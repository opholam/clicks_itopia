/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package com.hybris.addon.cockpits.components.liveedit;



import de.hybris.clicks.cockpits.cmscockpit.session.impl.DefaultLiveEditBrowserArea;

import org.zkoss.zul.Hbox;

import com.hybris.addon.cockpits.session.liveedit.impl.AddOnLiveEditBrowserModel;
import com.hybris.addon.cockpits.session.liveedit.impl.AddOnLiveEditContentBrowser;


/**
 * @author rmcotton
 * 
 */
public interface LiveEditCaptionButtonHandler
{
	void createButton(final DefaultLiveEditBrowserArea area, final AddOnLiveEditBrowserModel browserModel,
			final AddOnLiveEditContentBrowser contentBrowser, final Hbox buttonContainer);
}
