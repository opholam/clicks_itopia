package com.hybris.addon.cockpits.components.liveedit;

import de.hybris.platform.cmscockpit.components.liveedit.LiveEditView;


/**
 * author: dariusz.malachowski
 */
public interface RefreshContentHandler<V extends LiveEditView>
{

	void onRefresh(final V view);

}
