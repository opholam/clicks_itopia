package com.hybris.addon.cockpits.components.liveedit;

import de.hybris.platform.cmscockpit.components.liveedit.LiveEditView;

import java.util.List;


/**
 * author: dariusz.malachowski
 */
public interface RefreshContentHandlerRegistry<V extends LiveEditView>
{

	List<RefreshContentHandler<V>> getRefreshContentHandlers();

}
