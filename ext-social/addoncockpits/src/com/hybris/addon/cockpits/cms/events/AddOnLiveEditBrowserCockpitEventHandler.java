/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package com.hybris.addon.cockpits.cms.events;

import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.session.UIBrowserArea;


/**
 * @author rmcotton
 * 
 */
public interface AddOnLiveEditBrowserCockpitEventHandler<BA extends UIBrowserArea>
{
	void handleCockpitEvent(CockpitEvent event, BA browserArea);

	boolean canHandleEvent(CockpitEvent event, BA browserArea);
}
