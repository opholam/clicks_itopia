/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package com.hybris.addon.cockpits.cms.strategies;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Collection;


/**
 * For the given Content Catalog Version this suggest the counterpart product Catalog Versions (i.e. Content Catalog
 * Versin Staged -> Product Catalog Version Staged)
 * 
 * @author rmcotton
 * 
 */
public interface CounterpartProductCatalogVersionsStrategy
{
	Collection<CatalogVersionModel> getCounterpartProductCatalogVersions();
}
