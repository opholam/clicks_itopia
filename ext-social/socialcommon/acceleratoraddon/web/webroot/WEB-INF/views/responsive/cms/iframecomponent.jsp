<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="socialcommon" uri="/WEB-INF/tld/addons/socialcommon/socialcommontags.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="iframe-omponent responsive-component">
	<socialcommon:iframe iframe="${iframe}" />
</div>