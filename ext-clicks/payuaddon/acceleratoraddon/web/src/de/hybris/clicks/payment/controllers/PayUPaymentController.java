/**
 *
 */
package de.hybris.clicks.payment.controllers;

import de.hybris.clicks.fulfilmentprocess.stock.service.ClicksStockService;
import de.hybris.clicks.payment.exception.PayUIntegrationException;
import de.hybris.clicks.payment.facade.PayUPaymentFacade;
import de.hybris.platform.addonsupport.controllers.page.AbstractAddOnPageController;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import za.co.payu.www.GetTransactionResponseMessage.GetTransactionResponseMessage;
import za.co.payu.www.SetTransactionResponseMessage.SetTransactionResponseMessage;

import com.payjar.web.controller.api.soap.PayloadStatusEnum;


/**
 * @author Swapnil Desai
 *
 */
@Controller
@RequestMapping(value = "/payment")
public class PayUPaymentController extends AbstractAddOnPageController
{

	private static final Logger LOG = Logger.getLogger(PayUPaymentController.class);
	protected static final String REDIRECT_URL_ORDER_CONFIRMATION = REDIRECT_PREFIX + "/checkout/orderConfirmation/";
	private PayUPaymentFacade paymentFacade;

	private ConfigurationService configurationService;

	private ClicksStockService clicksStockService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @return the paymentFacade
	 */
	public PayUPaymentFacade getPaymentFacade()
	{
		return paymentFacade;
	}

	/**
	 * @param paymentFacade
	 *           the paymentFacade to set
	 */
	public void setPaymentFacade(final PayUPaymentFacade paymentFacade)
	{
		this.paymentFacade = paymentFacade;
	}

	@RequestMapping(value = "/payu/init", method = RequestMethod.GET)
	@RequireHardLogIn
	public String initiatePayment(final RedirectAttributes redirectModel)
	{
		LOG.info("PayUPaymentController -> initiatePayment");
		String redirect = "/checkout/multi/summary/view";
		SetTransactionResponseMessage setTransactionResponse;
		try
		{
			setTransactionResponse = paymentFacade.setTransaction();
			if (null != setTransactionResponse && StringUtils.isNotBlank(setTransactionResponse.getPayUReference())
					&& setTransactionResponse.isSuccessful())
			{
				final StringBuilder payuRedirectBuilder = new StringBuilder(configurationService.getConfiguration().getString(
						"payu.redirect"));
				payuRedirectBuilder.append(setTransactionResponse.getPayUReference());
				clicksStockService.reserveStockForCart();
				redirect = payuRedirectBuilder.toString();
				LOG.info("PayUPaymentController -> initiatePayment PayUReference " + setTransactionResponse.getPayUReference());
			}
			else if (null != setTransactionResponse && StringUtils.isNotBlank(setTransactionResponse.getResultCode())
					&& setTransactionResponse.getResultCode().startsWith("P"))
			{
				LOG.error("PayUPaymentController -> " + setTransactionResponse.getResultMessage() + " PayUReference "
						+ setTransactionResponse.getPayUReference());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						setTransactionResponse.getResultMessage() + ". " + setTransactionResponse.getDisplayMessage());
			}
		}
		catch (final PayUIntegrationException e)
		{
			LOG.error("PayU Error in PayUPaymentController -> ", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Payment could not be initiated. Please contact the merchant.");
		}
		catch (final InsufficientStockLevelException e)
		{
			LOG.error("Stock Error in PayUPaymentController -> ", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Sorry item(s) in your cart out of stock");
		}
		catch (final Exception e)
		{
			LOG.error("Payment could not be initiated. Please contact the merchant.", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Payment could not be initiated. Please contact the merchant.");
		}

		if (redirect.contains("summary"))
		{
			paymentFacade.enableBasketToEdit();
		}
		LOG.info("PayUPaymentController -> Redirecting to " + redirect);
		return REDIRECT_PREFIX + redirect;

	}

	@RequestMapping(value = "/payu/process", method = RequestMethod.GET)
	@RequireHardLogIn
	public String processPayment(final HttpServletRequest request, final RedirectAttributes redirectModel)
	{
		String payUReference = null;
		String redirect = "/checkout/multi/summary/view";
		if (null != request)
		{
			payUReference = request.getParameter("PayUReference");
		}
		LOG.info("PayUPaymentController -> processPayment" + payUReference);
		GetTransactionResponseMessage getTransactionResponse;
		try
		{
			getTransactionResponse = paymentFacade.getTransaction(payUReference);
			if (null != getTransactionResponse && "00".equals(getTransactionResponse.getResultCode())
					&& PayloadStatusEnum.SUCCESSFUL.equals(getTransactionResponse.getTransactionState()))
			{
				LOG.info("PayUPaymentController -> processed Payment for " + payUReference);
				redirect = "/checkout/multi/summary/placeOrder";
				clicksStockService.releaseStockForSessionCart();
				LOG.info("PayUPaymentController -> Stock released " + payUReference);
				sessionService.setAttribute("gaTrackingOderConfirmation", Boolean.TRUE);
				return FORWARD_PREFIX + redirect;
			}
			else if (null != getTransactionResponse && StringUtils.isNotBlank(getTransactionResponse.getResultCode())
					&& getTransactionResponse.getResultCode().startsWith("P"))
			{
				LOG.error("PayUPaymentController -> Failed Payment Process for " + payUReference + " Reason: "
						+ getTransactionResponse.getResultMessage());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						getTransactionResponse.getResultMessage() + ". " + getTransactionResponse.getDisplayMessage());
			}
		}
		catch (final Exception e)
		{
			if (e.getMessage().contains("DuplicatePayment_"))
			{
				final String orderCode = e.getMessage().split("_")[1];
				LOG.error("PayUPaymentController -> Duplicate Payment  for " + payUReference, e);
				return REDIRECT_URL_ORDER_CONFIRMATION + orderCode;
			}
			LOG.error("PayUPaymentController -> Error in Processing Payment  for " + payUReference, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"Payment could not be processed. Please contact the merchant.");
		}
		LOG.info("PayUPaymentController -> Redirecting to " + redirect);
		return REDIRECT_PREFIX + redirect;

	}

	@RequestMapping(value = "/payu/cancel", method = RequestMethod.GET)
	@RequireHardLogIn
	public String cancelPayment(final HttpServletRequest request, final RedirectAttributes redirectModel)
	{

		final String redirect = "/checkout/multi/summary/view";

		String payURef = null;
		if (null != request)
		{
			payURef = request.getParameter("payUReference");
		}
		paymentFacade.removePaymentTransaction(payURef);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "Payment has been cancelled");
		return REDIRECT_PREFIX + redirect;

	}

	/**
	 * @return the clicksStockService
	 */
	public ClicksStockService getClicksStockService()
	{
		return clicksStockService;
	}

	/**
	 * @param clicksStockService
	 *           the clicksStockService to set
	 */
	public void setClicksStockService(final ClicksStockService clicksStockService)
	{
		this.clicksStockService = clicksStockService;
	}
}
