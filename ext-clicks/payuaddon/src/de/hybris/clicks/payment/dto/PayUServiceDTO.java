/**
 *
 */
package de.hybris.clicks.payment.dto;

import com.payjar.web.controller.api.soap.AdditionalInfo;
import com.payjar.web.controller.api.soap.Basket;
import com.payjar.web.controller.api.soap.Customer;


/**
 * @author Swapnil Desai
 *
 */
public class PayUServiceDTO
{

	private AdditionalInfo additionalInfo;
	private Customer customer;
	private Basket basket;

	/**
	 * @return the additionalInfo
	 */
	public AdditionalInfo getAdditionalInfo()
	{
		return additionalInfo;
	}

	/**
	 * @param additionalInfo
	 *           the additionalInfo to set
	 */
	public void setAdditionalInfo(final AdditionalInfo additionalInfo)
	{
		this.additionalInfo = additionalInfo;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer()
	{
		return customer;
	}

	/**
	 * @param customer
	 *           the customer to set
	 */
	public void setCustomer(final Customer customer)
	{
		this.customer = customer;
	}

	/**
	 * @return the basket
	 */
	public Basket getBasket()
	{
		return basket;
	}

	/**
	 * @param basket
	 *           the basket to set
	 */
	public void setBasket(final Basket basket)
	{
		this.basket = basket;
	}




}
