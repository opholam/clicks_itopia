/**
 *
 */
package de.hybris.clicks.payment.facade;

import de.hybris.clicks.payment.exception.PayUIntegrationException;
import de.hybris.platform.core.model.order.CartModel;

import za.co.payu.www.GetTransactionResponseMessage.GetTransactionResponseMessage;
import za.co.payu.www.SetTransactionResponseMessage.SetTransactionResponseMessage;


/**
 * @author Swapnil Desai
 *
 */
public interface PayUPaymentFacade
{

	public SetTransactionResponseMessage setTransaction() throws PayUIntegrationException, Exception;

	public GetTransactionResponseMessage getTransaction(String payUReference) throws PayUIntegrationException, Exception;

	public boolean removePaymentTransaction(String payUReference);

	public void enableBasketToEdit();

	/**
	 * @param payUReference
	 * @param cartModel
	 * @return
	 */
	boolean isPaymentSuccess(String payUReference, CartModel cartModel);

}
