/**
 *
 */
package de.hybris.clicks.payment.facade.impl;

import de.hybris.clicks.payment.dto.PayUServiceDTO;
import de.hybris.clicks.payment.exception.IntegrationError;
import de.hybris.clicks.payment.exception.PayUIntegrationException;
import de.hybris.clicks.payment.facade.PayUPaymentFacade;
import de.hybris.clicks.payment.service.PayUPaymentService;
import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import za.co.payu.www.GetTransactionResponseMessage.GetTransactionResponseMessage;
import za.co.payu.www.SetTransactionResponseMessage.SetTransactionResponseMessage;

import com.payjar.web.controller.api.soap.AdditionalInfo;
import com.payjar.web.controller.api.soap.Basket;
import com.payjar.web.controller.api.soap.CreditCard;
import com.payjar.web.controller.api.soap.Customer;
import com.payjar.web.controller.api.soap.DebitCard;
import com.payjar.web.controller.api.soap.Eft;
import com.payjar.web.controller.api.soap.IPaymentMethod;
import com.payjar.web.controller.api.soap.PayloadStatusEnum;


/**
 * @author Swapnil Desai
 *
 */
public class PayUPaymentFacadeImpl extends DefaultAcceleratorCheckoutFacade implements PayUPaymentFacade
{

	private static final Logger LOG = Logger.getLogger(PayUPaymentFacadeImpl.class);

	private PayUPaymentService paymentService;
	private ConfigurationService configurationService;
	private KeyGenerator keyGenerator;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	/**
	 * @return the paymentService
	 */
	public PayUPaymentService getPaymentService()
	{
		return paymentService;
	}

	/**
	 * @param paymentService
	 *           the paymentService to set
	 */
	public void setPaymentService(final PayUPaymentService paymentService)
	{
		this.paymentService = paymentService;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @return the keyGenerator
	 */
	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * @param keyGenerator
	 *           the keyGenerator to set
	 */
	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}

	@Override
	public SetTransactionResponseMessage setTransaction() throws PayUIntegrationException, Exception
	{
		final CartData cartData = getCartFacade().getSessionCart();
		final PayUServiceDTO payUServiceData = getPaymentServiceDataForSetTransaction(cartData);
		SetTransactionResponseMessage setTransactionResponse = null;
		//		String redirect = "/checkout/multi/summary/view";
		try
		{
			setTransactionResponse = paymentService.setTransaction(payUServiceData);
			final CartModel cartModel = getCart();

			savePaymentTransactionsForSetTransaction(payUServiceData, cartData, cartModel, setTransactionResponse);


			//			if (null != setTransactionResponse && StringUtils.isNotBlank(setTransactionResponse.getPayUReference())
			//					&& setTransactionResponse.isSuccessful())
			//			{
			//				final StringBuilder payuRedirectBuilder = new StringBuilder(configurationService.getConfiguration().getString(
			//						"payu.redirect"));
			//				payuRedirectBuilder.append(setTransactionResponse.getPayUReference());
			//				redirect = payuRedirectBuilder.toString();
			//			}
			//			else if (null != setTransactionResponse && StringUtils.isNotBlank(setTransactionResponse.getResultCode())
			//					&& setTransactionResponse.getResultCode().startsWith("P"))
			//			{
			//				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
			//						setTransactionResponse.getResultMessage() + ". " + setTransactionResponse.getDisplayMessage());
			//			}
		}
		catch (final PayUIntegrationException ex)
		{
			if (IntegrationError.GSE_101.equals(ex.getErrorCode()))
			{
				LOG.error("SOAP Exception while set transaction : ", ex);
			}
			else if ((IntegrationError.RSE_101).equals(ex.getErrorCode()))
			{
				LOG.error("Remote Exception while set transaction : ", ex);
			}
			else if ((IntegrationError.SE_101).equals(ex.getErrorCode()))
			{
				LOG.error("Set Tranasction ", ex);
			}
			LOG.error("PayU set transaction exception ", ex);
			throw new PayUIntegrationException(IntegrationError.GE_101);
			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
			//					"Payment Could not be initiated. Please contact the Merchant.");
		}
		catch (final Exception ex)
		{
			LOG.error("Generic Exception in Set Transaction : ", ex);
			throw new PayUIntegrationException(IntegrationError.GE_101);
			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
			//					"Payment Could not be initiated. Please contact the Merchant.");
		}

		return setTransactionResponse;
	}

	/**
	 * @param payUServiceData
	 * @param cartData
	 * @param setTransactionResponse
	 */
	private void savePaymentTransactionsForSetTransaction(final PayUServiceDTO payUServiceData, final CartData cartData,
			final CartModel cartModel, final SetTransactionResponseMessage setTransactionResponse)
	{
		try
		{
			final PaymentTransactionModel paymentTransactionModel = modelService.create(PaymentTransactionModel.class);
			paymentTransactionModel.setCode(payUServiceData.getAdditionalInfo().getMerchantReference());
			paymentTransactionModel.setRequestId(payUServiceData.getAdditionalInfo().getMerchantReference());
			paymentTransactionModel.setPaymentProvider(configurationService.getConfiguration().getString("payment.provider"));
			paymentTransactionModel.setCurrency(getCommonI18NService().getCurrentCurrency());

			if (null != cartData.getTotalPrice())
			{
				paymentTransactionModel.setPlannedAmount(cartData.getTotalPrice().getValue());
			}

			if (null != setTransactionResponse)
			{
				paymentTransactionModel.setRequestId(setTransactionResponse.getPayUReference());
			}


			//			paymentTransactionModel.setInfo(cartModel.getPaymentInfo());
			paymentTransactionModel.setOrder(cartModel);
			//		modelService.refresh(paymentTransactionModel);
			modelService.save(paymentTransactionModel);


			final PaymentTransactionEntryModel paymentTransactionEntryModel = modelService
					.create(PaymentTransactionEntryModel.class);
			paymentTransactionEntryModel.setCode((String) keyGenerator.generate());
			paymentTransactionEntryModel.setType(PaymentTransactionType.CREATE_SUBSCRIPTION);
			if (null != cartData.getTotalPrice())
			{
				paymentTransactionEntryModel.setAmount(cartData.getTotalPrice().getValue());
			}
			paymentTransactionEntryModel.setCurrency(getCommonI18NService().getCurrentCurrency());
			paymentTransactionEntryModel.setRequestId(payUServiceData.getAdditionalInfo().getMerchantReference());
			if (null != setTransactionResponse)
			{
				paymentTransactionEntryModel.setTransactionStatus(setTransactionResponse.getResultCode());
				paymentTransactionEntryModel.setTransactionStatusDetails(setTransactionResponse.getResultMessage());
				paymentTransactionEntryModel.setSubscriptionID(setTransactionResponse.getPayUReference());
			}

			paymentTransactionEntryModel.setPaymentTransaction(paymentTransactionModel);

			//		modelService.refresh(paymentTransactionEntryModel);
			modelService.save(paymentTransactionEntryModel);
		}
		catch (final ModelSavingException ex)
		{
			LOG.error("Error in saving payment transaction model after calling set transaction : ", ex);
		}
		cartModel.setBasketEditable(Boolean.FALSE);
		getModelService().save(cartModel);
	}

	private PayUServiceDTO getPaymentServiceDataForSetTransaction(final CartData cartData)
	{

		final PayUServiceDTO payUServiceData = new PayUServiceDTO();

		final AdditionalInfo additionalInfo = new AdditionalInfo();
		additionalInfo.setCancelUrl(configurationService.getConfiguration().getString("payu.cancel.url"));
		additionalInfo.setDemoMode(configurationService.getConfiguration().getString("payu.demo.mode"));
		additionalInfo.setMerchantReference((String) keyGenerator.generate());
		additionalInfo.setNotificationUrl(configurationService.getConfiguration().getString("payu.notify.url"));
		additionalInfo.setReturnUrl(configurationService.getConfiguration().getString("payu.return.url"));
		additionalInfo.setSecure3D(configurationService.getConfiguration().getString("payu.secure3d"));
		additionalInfo.setSupportedPaymentMethods(configurationService.getConfiguration().getString("payu.paymentmethod"));
		//		additionalInfo.setShowBudget("true");

		payUServiceData.setAdditionalInfo(additionalInfo);

		final CustomerModel customerModel = getCurrentUserForCheckout();
		final AddressData addressData = cartData.getDeliveryAddress();
		final Customer customer = new Customer();
		customer.setMerchantUserId(customerModel.getCustomerID());
		customer.setEmail(StringUtils.isNotEmpty(customerModel.getEmailID()) ? customerModel.getEmailID()
				: null != addressData ? addressData.getEmail() : "");
		customer.setFirstName(StringUtils.isNotEmpty(customerModel.getFirstName()) ? customerModel.getFirstName()
				: null != addressData ? addressData.getFirstName() : "");
		customer.setLastName(StringUtils.isNotEmpty(customerModel.getLastName()) ? customerModel.getLastName()
				: null != addressData ? addressData.getLastName() : "");

		payUServiceData.setCustomer(customer);

		final Basket basket = new Basket();
		int cartValueInCents = 0;
		if (null != cartData.getTotalPrice() && null != cartData.getTotalPrice().getValue())
		{
			final BigDecimal cartValue = cartData.getTotalPrice().getValue();
			cartValueInCents = cartValue.multiply(new BigDecimal(100)).intValueExact();
		}
		basket.setAmountInCents(String.valueOf(cartValueInCents));
		basket.setCurrencyCode(getCommonI18NService().getCurrentCurrency().getIsocode());
		basket.setDescription(cartData.getDescription());

		payUServiceData.setBasket(basket);
		return payUServiceData;
	}

	@Override
	public boolean isPaymentSuccess(final String payUReference, final CartModel cartModel)
	{
		try
		{
			final GetTransactionResponseMessage getTransactionResponseMessage = getTransaction(payUReference);
			if (getTransactionResponseMessage.isSuccessful() && "00".equals(getTransactionResponseMessage.getResultCode())
					&& PayloadStatusEnum.SUCCESSFUL.equals(getTransactionResponseMessage.getTransactionState()))
			{
				savePaymentTransactionEntryForGetTransaction(getTransactionResponseMessage, cartModel);
				return true;
			}
		}
		catch (final Exception e)
		{
			//
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.payment.facade.PayUPaymentFacade#getTransaction()
	 */
	@Override
	public GetTransactionResponseMessage getTransaction(final String payUReference) throws PayUIntegrationException, Exception
	{
		LOG.info("****inside get transaction ****" + payUReference);
		//		final String redirect = "/checkout/multi/summary/view";
		GetTransactionResponseMessage getTransactionResponse = null;
		try
		{
			getTransactionResponse = paymentService.getTransaction(getPaymentServiceDataForGetTransaction(payUReference));
			LOG.info("after get transaction soap call-->>>" + payUReference);
			if (null != getTransactionResponse)
			{
				boolean duplicate = false;
				LOG.error("getTransactionResponse result code for PayUReference " + getTransactionResponse.getPayUReference() + " "
						+ getTransactionResponse.getResultCode());

				duplicate = checkDuplicatePaymentResponse(getTransactionResponse);
				final CartModel cartModel = getCart();
				if (null != cartModel && !duplicate)
				{
					cartModel.setBasketEditable(Boolean.TRUE);
					getModelService().save(cartModel);
					savePaymentTransactionEntryForGetTransaction(getTransactionResponse, cartModel);
				}
				else
				{
					OrderModel order = null;
					order = getOrder(getTransactionResponse, order);
					if (null == order)
					{
						Thread.sleep(Config.getLong("payu.duplicate.response.wait.time", 3000));
						order = getOrder(getTransactionResponse, order);
					}
					if (null != order && null != order.getUser())
					{
						String orderCode = "";
						if (CustomerType.GUEST.equals(((CustomerModel) order.getUser()).getType()))
						{
							orderCode = order.getGuid();
						}
						else
						{
							orderCode = StringUtils.isNotEmpty(order.getClicksOrderCode()) ? order.getClicksOrderCode() : order
									.getCode();
						}
						if (StringUtils.isNotEmpty(orderCode))
						{
							LOG.error("Duplicate payment transaction response received for payu "
									+ getTransactionResponse.getPayUReference() + " redirecting to order " + orderCode);
							throw new Exception("DuplicatePayment_" + orderCode);
						}
					}
					else if (null != cartModel)
					{
						cartModel.setBasketEditable(Boolean.TRUE);
						getModelService().save(cartModel);
						savePaymentTransactionEntryForGetTransaction(getTransactionResponse, cartModel);
					}

				}
				//				redirect = "/checkout/multi/summary/placeOrder";
			}
			//			else if (null != getTransactionResponse && StringUtils.isNotBlank(getTransactionResponse.getResultCode())
			//					&& getTransactionResponse.getResultCode().startsWith("P"))
			//			{
			////				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
			////						getTransactionResponse.getResultMessage() + ". " + getTransactionResponse.getDisplayMessage());
			//			}
		}
		catch (final PayUIntegrationException ex)
		{
			LOG.error("PayUIntegrationException: error code>>" + ex.getErrorCode() + " error message >> ", ex);
			if (IntegrationError.GSE_101.equals(ex.getErrorCode()))
			{
				LOG.error("SOAP Exception while get transaction : ", ex);
			}
			else if ((IntegrationError.RSE_101).equals(ex.getErrorCode()))
			{
				LOG.error("Remote Exception while get transaction : ", ex);
			}
			else if ((IntegrationError.SE_101).equals(ex.getErrorCode()))
			{
				LOG.error("Get Transaction : ", ex);
			}

			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
			//					"Payment Could not be processed. Please contact the Merchant.");
		}
		catch (final Exception ex)
		{
			if (ex.getMessage().contains("DuplicatePayment_"))
			{
				throw ex;
			}
			LOG.error("Generic Exception in Get Transaction : ", ex);

			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
			//					"Payment Could not be processed. Please contact the Merchant.");
		}
		return getTransactionResponse;
	}

	/**
	 * @param getTransactionResponse
	 * @param duplicate
	 * @return
	 */
	private boolean checkDuplicatePaymentResponse(final GetTransactionResponseMessage getTransactionResponse)
	{
		boolean duplicate = false;
		try
		{
			final Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put(PaymentTransactionEntryModel.SUBSCRIPTIONID, getTransactionResponse.getPayUReference());
			paramMap.put(PaymentTransactionEntryModel.TRANSACTIONSTATUS, TransactionStatus.ACCEPTED.name());
			paramMap.put(PaymentTransactionEntryModel.TYPE, PaymentTransactionType.AUTHORIZATION);
			final SearchResult<PaymentTransactionEntryModel> result = flexibleSearchService
					.search(new FlexibleSearchQuery(
							"select {pk} from {paymenttransactionentry} where {subscriptionID}=?subscriptionID and {transactionStatus}=?transactionStatus and {type}=?type",
							paramMap));
			if (null != result && CollectionUtils.isNotEmpty(result.getResult()))
			{
				duplicate = true;
				LOG.error("Duplicate payment transaction response received for payu " + getTransactionResponse.getPayUReference());
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		return duplicate;
	}

	/**
	 * @param getTransactionResponse
	 * @param order
	 * @return
	 */
	private OrderModel getOrder(final GetTransactionResponseMessage getTransactionResponse, OrderModel order)
	{
		try
		{
			final Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put(PaymentTransactionModel.REQUESTID, getTransactionResponse.getPayUReference());
			order = flexibleSearchService.searchUnique(new FlexibleSearchQuery(
					"select {o:pk} from {paymenttransaction as p join order as o on {p:order}={o:pk}} where {p:requestid}=?requestId",
					paramMap));
		}
		catch (final Exception e)
		{
			//
		}
		return order;
	}

	/**
	 * @param getTransactionResponse
	 * @param cartModel
	 */
	private void savePaymentTransactionEntryForGetTransaction(final GetTransactionResponseMessage getTransactionResponse,
			final CartModel cartModel)
	{
		try
		{
			final PaymentTransactionEntryModel paymentTransactionEntryModel = modelService
					.create(PaymentTransactionEntryModel.class);
			paymentTransactionEntryModel.setCode((String) keyGenerator.generate());
			paymentTransactionEntryModel.setType(PaymentTransactionType.AUTHORIZATION);
			PaymentInfoModel paymentInfoModel = null;
			LOG.info("savePaymentTransactionEntryForGetTransaction : Saving Payment Info");
			if (null != getTransactionResponse.getPaymentMethodsUsed() && getTransactionResponse.getPaymentMethodsUsed().length > 0)
			{
				final IPaymentMethod paymentMethod = getTransactionResponse.getPaymentMethodsUsed()[0];
				LOG.info("savePaymentTransactionEntryForGetTransaction : Receive Payment Method is " + paymentMethod);

				try
				{
					if (paymentMethod instanceof CreditCard)
					{
						final CreditCard creditCardPaymentMethod = (CreditCard) paymentMethod;
						final BigDecimal amountPaid = new BigDecimal(creditCardPaymentMethod.getAmountInCents());
						paymentTransactionEntryModel.setAmount(amountPaid.divide(new BigDecimal(100)));

						final CreditCardPaymentInfoModel ccPaymentInfoModel = modelService.create(CreditCardPaymentInfoModel.class);
						if (StringUtils.isNotBlank(creditCardPaymentMethod.getGatewayReference()))
						{
							ccPaymentInfoModel.setCode(creditCardPaymentMethod.getGatewayReference());
						}
						else
						{
							ccPaymentInfoModel.setCode(UUID.randomUUID().toString());
						}
						ccPaymentInfoModel.setNumber("0000");
						ccPaymentInfoModel.setType(getCardType(creditCardPaymentMethod));
						ccPaymentInfoModel.setCcOwner("*****");
						ccPaymentInfoModel.setValidToYear("****");
						ccPaymentInfoModel.setValidToMonth("***");
						ccPaymentInfoModel.setUser(null != cartModel.getUser() ? cartModel.getUser() : getCurrentUserForCheckout());
						modelService.save(ccPaymentInfoModel);
						paymentInfoModel = ccPaymentInfoModel;
					}
					if (paymentMethod instanceof DebitCard)
					{
						final DebitCard debitCard = (DebitCard) paymentMethod;
						final BigDecimal amountPaid = new BigDecimal(debitCard.getAmountInCents());
						paymentTransactionEntryModel.setAmount(amountPaid.divide(new BigDecimal(100)));
						final DebitPaymentInfoModel dbPaymentInfoModel = modelService.create(DebitPaymentInfoModel.class);
						dbPaymentInfoModel.setUser(null != cartModel.getUser() ? cartModel.getUser() : getCurrentUserForCheckout());
						dbPaymentInfoModel.setBank("DEBIT CARD");
						dbPaymentInfoModel.setCode(StringUtils.isNotEmpty(debitCard.getReference()) ? debitCard.getReference()
								: "DEBIT_CARD_" + paymentTransactionEntryModel.getCode());
						dbPaymentInfoModel.setAccountNumber("DEBIT CARD");
						dbPaymentInfoModel.setBankIDNumber("DEBIT CARD");
						dbPaymentInfoModel.setBaOwner("DEBIT CARD");
						modelService.save(dbPaymentInfoModel);
						paymentInfoModel = dbPaymentInfoModel;
					}
					if (paymentMethod instanceof Eft)
					{
						final Eft eft = (Eft) paymentMethod;
						final BigDecimal amountPaid = new BigDecimal(eft.getAmountInCents());
						paymentTransactionEntryModel.setAmount(amountPaid.divide(new BigDecimal(100)));

						final DebitPaymentInfoModel dbPaymentInfoModel = modelService.create(DebitPaymentInfoModel.class);
						dbPaymentInfoModel.setUser(null != cartModel.getUser() ? cartModel.getUser() : getCurrentUserForCheckout());
						dbPaymentInfoModel.setCode(StringUtils.isNotEmpty(eft.getReference()) ? eft.getReference() : "EFT_"
								+ paymentTransactionEntryModel.getCode());
						dbPaymentInfoModel.setBankIDNumber(StringUtils.isNotEmpty(eft.getBranchNumber()) ? eft.getBranchNumber()
								: "EFT");
						dbPaymentInfoModel.setBank(StringUtils.isNotEmpty(eft.getBankName()) ? eft.getBankName() : "EFT");
						dbPaymentInfoModel.setAccountNumber(StringUtils.isNotEmpty(eft.getAccountNumber()) ? eft.getAccountNumber()
								: "EFT");
						dbPaymentInfoModel.setBaOwner("EFT");
						modelService.save(dbPaymentInfoModel);
						paymentInfoModel = dbPaymentInfoModel;
					}
				}
				catch (final Exception e)
				{
					LOG.error("Exception while creating payment info for payu reference " + getTransactionResponse.getPayUReference(),
							e);
				}
			}
			if (null != getTransactionResponse.getBasket())
			{
				paymentTransactionEntryModel.setCurrency(getCommonI18NService().getCurrency(
						getTransactionResponse.getBasket().getCurrencyCode()));
			}

			paymentTransactionEntryModel.setRequestId(getTransactionResponse.getMerchantReference());
			paymentTransactionEntryModel.setTransactionStatus(getTransactionStatus(getTransactionResponse.getResultCode()));
			paymentTransactionEntryModel.setTransactionStatusDetails(getTransactionResponse.getResultMessage());
			paymentTransactionEntryModel.setSubscriptionID(getTransactionResponse.getPayUReference());

			if (CollectionUtils.isNotEmpty(cartModel.getPaymentTransactions()))
			{
				for (final PaymentTransactionModel paymentTransactionModel : cartModel.getPaymentTransactions())
				{
					if (paymentTransactionModel.getCode().equals(getTransactionResponse.getMerchantReference()))
					{
						if (null != paymentInfoModel)
						{
							LOG.info("savePaymentTransactionEntryForGetTransaction : Saving Payment info in transaction code:"
									+ paymentTransactionModel.getCode());
							paymentTransactionModel.setInfo(paymentInfoModel);
						}
						modelService.save(paymentTransactionModel);
						paymentTransactionEntryModel.setPaymentTransaction(paymentTransactionModel);
					}
				}
			}

			//		modelService.refresh(paymentTransactionEntryModel);
			modelService.save(paymentTransactionEntryModel);
		}
		catch (final ModelSavingException ex)
		{
			LOG.error("Error in saving payment transaction model after calling get transaction : ", ex);
		}


	}

	/**
	 * @param creditCardPaymentMethod
	 * @return
	 */
	private CreditCardType getCardType(final CreditCard creditCardPaymentMethod)
	{
		CreditCardType creditCardType = null;
		try
		{
			final String cardType = StringUtils.equalsIgnoreCase(creditCardPaymentMethod.getInformation(), "MASTERCARD") ? "MASTER"
					: creditCardPaymentMethod.getInformation();
			creditCardType = null != CreditCardType.valueOf(cardType) ? CreditCardType.valueOf(cardType) : CreditCardType.VISA;
		}
		catch (final Exception e)
		{
			creditCardType = CreditCardType.VISA;
		}
		return creditCardType;
	}

	private PayUServiceDTO getPaymentServiceDataForGetTransaction(final String payUReference)
	{

		final PayUServiceDTO payUServiceData = new PayUServiceDTO();
		final AdditionalInfo additionalInfo = new AdditionalInfo();
		additionalInfo.setPayUReference(payUReference);
		payUServiceData.setAdditionalInfo(additionalInfo);

		return payUServiceData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.payment.facade.PayUPaymentFacade#removePaymentTransaction(java.lang.String)
	 */
	@Override
	public boolean removePaymentTransaction(final String payUReference)
	{
		boolean remove = false;
		try
		{
			final CartModel cart = getCart();
			cart.setBasketEditable(Boolean.TRUE);
			getModelService().save(cart);
			if (StringUtils.isNotEmpty(payUReference) && CollectionUtils.isNotEmpty(cart.getPaymentTransactions()))
			{
				for (final PaymentTransactionModel paymentTransactionModel : cart.getPaymentTransactions())
				{
					if (StringUtils.isNotBlank(paymentTransactionModel.getRequestId())
							&& paymentTransactionModel.getRequestId().equalsIgnoreCase(payUReference))
					{
						for (final PaymentTransactionEntryModel entry : paymentTransactionModel.getEntries())
						{
							entry.setTransactionStatus("CANCELLED_BY_USER");
						}
						remove = true;
					}
				}
			}
		}
		catch (final ModelRemovalException ex)
		{
			LOG.error("Payment transaction model was not removed for payURefernce number : " + payUReference);
		}

		return remove;
	}

	private String getTransactionStatus(final String code)
	{
		if (StringUtils.isNotBlank(code) && code.equalsIgnoreCase("00"))
		{
			return TransactionStatus.ACCEPTED.name();
		}
		else
		{
			return TransactionStatus.ERROR.name();
		}
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public void enableBasketToEdit()
	{
		final CartModel cartModel = getCart();
		if (null != cartModel)
		{
			cartModel.setBasketEditable(Boolean.TRUE);
			modelService.save(cartModel);
		}
	}
}
