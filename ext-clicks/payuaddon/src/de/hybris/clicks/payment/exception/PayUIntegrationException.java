/**
 *
 */
package de.hybris.clicks.payment.exception;

/**
 * @author Swapnil Desai
 *
 */
public class PayUIntegrationException extends RuntimeException
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7845327879059273396L;

	/** The service name. */
	private final String serviceName;

	/** The error code. */
	private final String errorCode;

	/** The error message. */
	private final String errorMessage;

	/**
	 * Instantiates a new cintas integration exception.
	 *
	 * @param error
	 *           the IntegrationError
	 * @param cause
	 *           the Exception caught while calling external services
	 */
	public PayUIntegrationException(final IntegrationError error, final Throwable cause)
	{
		super(cause);
		this.errorCode = error.getCode();
		this.errorMessage = error.getMessage();
		this.serviceName = error.getServiceName();

	}

	/**
	 * Instantiates a new cintas integration exception.
	 *
	 * @param error
	 *           the error
	 */
	public PayUIntegrationException(final IntegrationError error)
	{
		this.errorCode = error.getCode();
		this.errorMessage = error.getMessage();
		this.serviceName = error.getServiceName();
	}

	/**
	 * Gets the error code.
	 *
	 * @return the errorCode
	 */
	public String getErrorCode()
	{
		return this.errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return this.errorMessage;
	}

	/**
	 * Gets the service name for which the error occured.
	 *
	 * @return the serviceName
	 */
	public String getServiceName()
	{
		return this.serviceName;
	}

}
