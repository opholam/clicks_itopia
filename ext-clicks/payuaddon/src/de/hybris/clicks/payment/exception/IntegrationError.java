package de.hybris.clicks.payment.exception;

/**
 * The Enum IntegrationError.
 *
 * @author Swapnil Desai
 */
public enum IntegrationError
{


	GE_101(IntegrationError.GE, null, null), STO_101(IntegrationError.STO, null, null), SE_101(IntegrationError.SE, "101",
			"Error in service binding"), GSE_101(IntegrationError.GSE, "101", "SOAP Exception while service creation"), RSE_101(
			IntegrationError.RSE, "101", null);

	public static final String GSE = "GenericSoapException"; // NOPMD

	public static final String GE = "GenericException"; // NOPMD

	/** The Constant STO. */
	public static final String STO = "SocketTimeout"; // NOPMD

	/** The Constant STO. */
	public static final String SE = "ServiceException"; // NOPMD

	/** The Constant RSE. */
	public static final String RSE = "RemoteServiceException"; // NOPMD

	/** The code. */
	private String code;

	/** The message. */
	private String message;

	/** The service name. */
	private String serviceName;

	/**
	 * Instantiates a new integration error.
	 *
	 * @param serviceName
	 *           the service name
	 * @param code
	 *           the code
	 * @param message
	 *           the message
	 */
	private IntegrationError(final String serviceName, final String code, final String message)
	{
		this.serviceName = serviceName;
		this.code = code;
		this.message = message;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage()
	{
		return this.message;
	}

	/**
	 * Gets the service name.
	 *
	 * @return the service name
	 */
	public String getServiceName()
	{
		return this.serviceName;
	}



}
