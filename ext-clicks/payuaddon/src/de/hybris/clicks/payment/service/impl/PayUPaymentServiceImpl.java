/**
 *
 */
package de.hybris.clicks.payment.service.impl;

import de.hybris.clicks.payment.dto.PayUServiceDTO;
import de.hybris.clicks.payment.exception.IntegrationError;
import de.hybris.clicks.payment.exception.PayUIntegrationException;
import de.hybris.clicks.payment.service.PayUPaymentService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;

import org.apache.axis.message.PrefixedQName;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.log4j.Logger;

import za.co.payu.www.GetTransactionResponseMessage.GetTransactionResponseMessage;
import za.co.payu.www.SetTransactionResponseMessage.SetTransactionResponseMessage;

import com.payjar.web.controller.api.soap.EnterpriseAPISoap;
import com.payjar.web.controller.api.soap.EnterpriseAPISoapService;
import com.payjar.web.controller.api.soap.EnterpriseAPISoapServiceLocator;
import com.payjar.web.controller.api.soap.EnterpriseAPISoapServiceSoapBindingStub;
import com.payjar.web.controller.api.soap.TransactionType;


/**
 * @author Swapnil Desai
 *
 */
public class PayUPaymentServiceImpl implements PayUPaymentService
{

	private static final Logger LOG = Logger.getLogger(PayUPaymentServiceImpl.class);

	private ConfigurationService configurationService;

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	private static final String SECURITY_QNAME = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String SECURITY_ELEMENT = "Security";
	private static final String SECURITY_PREFIX = "wsse";
	private static final String USERNAME_TOKEN = "UsernameToken";
	private static final String USERNAME_TOKEN_QNAME = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurityutility-1.0.xsd";
	private static final String USERNAME_TOKEN_NAMESPACE = "xmlns:wsu";
	private static final String USERNAME_ELEMENT = "Username";
	private static final String PASSWORD_ELEMENT = "Password";
	private static final String PASSWORD_ENCODING_TYPE_QNAME = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
	private static final String USERNAME_TOKEN_ID_KEY = "wsu:Id";
	private static final String USERNAME_TOKEN_ID_VALUE = "UsernameToken-9";

	private static final String PAYU_SOAP_API_VERSION = "ONE_ZERO";

	@Override
	public SetTransactionResponseMessage setTransaction(final PayUServiceDTO payUServiceDTO) throws PayUIntegrationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Locating service for Set Transaction call");
		}
		final EnterpriseAPISoap payUStub = getServiceLocator();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Calling Set Transaction service");
		}
		final SetTransactionResponseMessage setTransactionResponseMessage = callSetTransactionService(payUStub, payUServiceDTO);
		return setTransactionResponseMessage;
	}

	private EnterpriseAPISoap getServiceLocator()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Setting payu endpoint address.");
		}
		final EnterpriseAPISoapService payuService = new EnterpriseAPISoapServiceLocator();
		((EnterpriseAPISoapServiceLocator) payuService).setEnterpriseAPISoapPortEndpointAddress(configurationService
				.getConfiguration().getString("payu.settransaction.uri"));


		EnterpriseAPISoap serviceBinder;
		EnterpriseAPISoapServiceSoapBindingStub payUStub = null;
		try
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Binding Service.");
			}
			serviceBinder = payuService.getEnterpriseAPISoapPort();
			if (null != serviceBinder)
			{
				payUStub = (EnterpriseAPISoapServiceSoapBindingStub) serviceBinder;
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Constructing payu security header");
				}
				payUStub.setHeader(populateRequestSecurityHeader());
			}
		}
		catch (final ServiceException e)
		{
			LOG.error("Service Exception", e);
			throw new PayUIntegrationException(IntegrationError.SE_101, e.getCause());
		}
		catch (final SOAPException e)
		{
			LOG.error("Soap Exception in Security Header", e);
			throw new PayUIntegrationException(IntegrationError.GSE_101, e.getCause());
		}
		return payUStub;
	}

	/**
	 * @return
	 * @throws SOAPException
	 */
	private SOAPHeaderElement populateRequestSecurityHeader() throws SOAPException
	{

		//Set Header
		final SOAPHeaderElement wsseSecurity = new SOAPHeaderElement(new PrefixedQName(SECURITY_QNAME, SECURITY_ELEMENT,
				SECURITY_PREFIX));
		wsseSecurity.setMustUnderstand(true);
		wsseSecurity.setActor(null);

		//set userNameToken
		final SOAPElement userNameToken = wsseSecurity.addChildElement(USERNAME_TOKEN, SECURITY_PREFIX);
		userNameToken.setAttribute(USERNAME_TOKEN_ID_KEY, USERNAME_TOKEN_ID_VALUE);
		userNameToken.setAttribute(USERNAME_TOKEN_NAMESPACE, USERNAME_TOKEN_QNAME);

		//set username
		final SOAPElement userName = userNameToken.addChildElement(USERNAME_ELEMENT, SECURITY_PREFIX);
		userName.setValue(configurationService.getConfiguration().getString("payment.username"));

		//set password
		final SOAPElement password = userNameToken.addChildElement(PASSWORD_ELEMENT, SECURITY_PREFIX);
		password.setAttribute("Type", PASSWORD_ENCODING_TYPE_QNAME);
		password.setValue(configurationService.getConfiguration().getString("payment.password"));

		return wsseSecurity;
	}

	private SetTransactionResponseMessage callSetTransactionService(final EnterpriseAPISoap api,
			final PayUServiceDTO payUServiceDTO)
	{

		SetTransactionResponseMessage response = null;
		try
		{
			response = api.setTransaction(PAYU_SOAP_API_VERSION, configurationService.getConfiguration().getString("payu.safekey"),
					TransactionType.PAYMENT, Boolean.FALSE, payUServiceDTO.getAdditionalInfo(), payUServiceDTO.getCustomer(),
					payUServiceDTO.getBasket(), null, null, null, null, null, null, null, null, null, null, null, null);
		}
		catch (final RemoteException e)
		{
			LOG.error("Call to Set Transaction service failed : ", e);
			throw new PayUIntegrationException(IntegrationError.RSE_101, e.getCause());
		}

		if (null != response && LOG.isDebugEnabled())
		{
			LOG.debug("PayuReference from from Set Transaction response is  : " + response.getPayUReference());
		}

		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.payment.service.PayUPaymentService#getTransaction(de.hybris.clicks.payment.dto.PayUServiceDTO)
	 */
	@Override
	public GetTransactionResponseMessage getTransaction(final PayUServiceDTO payUServiceDTO) throws PayUIntegrationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Locating Service for Get Transaction service");
		}
		final EnterpriseAPISoap payUStub = getServiceLocator();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Calling Get Transaction service");
		}
		final GetTransactionResponseMessage getTransactionResponseMessage = callGetTransactionService(payUStub, payUServiceDTO);
		return getTransactionResponseMessage;
	}

	private GetTransactionResponseMessage callGetTransactionService(final EnterpriseAPISoap api,
			final PayUServiceDTO payUServiceDTO)
	{

		GetTransactionResponseMessage response = null;
		try
		{
			response = api.getTransaction(PAYU_SOAP_API_VERSION, configurationService.getConfiguration().getString("payu.safekey"),
					payUServiceDTO.getAdditionalInfo());
		}
		catch (final RemoteException e)
		{
			LOG.error("Call to get transaction service failed : ", e);
			throw new PayUIntegrationException(IntegrationError.RSE_101, e.getCause());
		}

		if (null != response && LOG.isDebugEnabled())
		{
			LOG.debug("PayuReference from Get Transaction Response is : " + response.getPayUReference());
		}

		return response;
	}

}
