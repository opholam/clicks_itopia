/**
 *
 */
package de.hybris.clicks.payment.service;

import de.hybris.clicks.payment.dto.PayUServiceDTO;
import de.hybris.clicks.payment.exception.PayUIntegrationException;

import za.co.payu.www.GetTransactionResponseMessage.GetTransactionResponseMessage;
import za.co.payu.www.SetTransactionResponseMessage.SetTransactionResponseMessage;


/**
 * @author Swapnil Desai
 *
 */
public interface PayUPaymentService
{

	public SetTransactionResponseMessage setTransaction(PayUServiceDTO payUServiceDTO) throws PayUIntegrationException;

	public GetTransactionResponseMessage getTransaction(PayUServiceDTO payUServiceDTO) throws PayUIntegrationException;

}
