/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.payment.service.impl;

import de.hybris.clicks.payment.facade.PayUPaymentFacade;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.strategies.impl.EventPublishingSubmitOrderStrategy;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


public class PayUConfirmationTaskRunner implements TaskRunner<TaskModel>
{
	private static final Logger LOG = Logger.getLogger(PayUConfirmationTaskRunner.class);
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "payuPaymentFacade")
	private PayUPaymentFacade payuPaymentFacade;
	@Resource(name = "commerceCheckoutService")
	private CommerceCheckoutService commerceCheckoutService;
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "eventPublishingSubmitOrderStrategy")
	private EventPublishingSubmitOrderStrategy eventPublishingSubmitOrderStrategy;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void handleError(final TaskService service, final TaskModel task, final Throwable fault)
	{
		LOG.error("Failed to run task '" + task + "' (context: " + task.getContext() + ").", fault);
	}

	@Override
	public void run(final TaskService service, final TaskModel task) throws RetryLaterException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Running on '" + task + "' (context: " + task.getContext() + "').");
		}
		try
		{
			sessionService.executeInLocalView(new SessionExecutionBody()
			{
				@Override
				public void executeWithoutResult()
				{
					try
					{
						final CartModel cartModel = (CartModel) task.getContextItem();
						modelService.refresh(cartModel);
						if (null != cartModel)
						{
							String payuReference = "";
							boolean isPaymentSuccess = false;
							for (final PaymentTransactionModel paymentTransactionModel : cartModel.getPaymentTransactions())
							{
								if (StringUtils.isNotEmpty(paymentTransactionModel.getRequestId()))
								{
									isPaymentSuccess = payuPaymentFacade.isPaymentSuccess(paymentTransactionModel.getRequestId(),
											cartModel);
									if (isPaymentSuccess)
									{
										payuReference = paymentTransactionModel.getRequestId();
										break;
									}
								}
							}
							if (isPaymentSuccess && Config.getBoolean("allow.order.placement.post.timeout", true))
							{
								cartModel.setBasketEditable(Boolean.TRUE);
								modelService.save(cartModel);
								final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
								parameter.setEnableHooks(true);
								parameter.setCart(cartModel);
								parameter.setSalesApplication(SalesApplication.WEB);
								final CommerceOrderResult commerceOrderResult = getCommerceCheckoutService().placeOrder(parameter);
								if (null != commerceOrderResult.getOrder())
								{
									LOG.error("Order placed for PayUreference " + payuReference + " order code "
											+ commerceOrderResult.getOrder().getClicksOrderCode());

									final OrderModel order = commerceOrderResult.getOrder();
									order.setStore(baseStoreService.getBaseStoreForUid("clicks"));
									order.setSite(baseSiteService.getBaseSiteForUID("clicks"));
									modelService.save(order);
									eventPublishingSubmitOrderStrategy.submitOrder(order);
									modelService.remove(cartModel);
								}
							}
							else
							{
								cartModel.setBasketEditable(Boolean.TRUE);
								modelService.save(cartModel);
							}
						}
					}
					catch (final Exception e)
					{
						//
					}
				}
			}, userService.getAdminUser());


		}
		catch (final Exception e)
		{
			//
		}
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the payuPaymentFacade
	 */
	public PayUPaymentFacade getPayuPaymentFacade()
	{
		return payuPaymentFacade;
	}

	/**
	 * @param payuPaymentFacade
	 *           the payuPaymentFacade to set
	 */
	public void setPayuPaymentFacade(final PayUPaymentFacade payuPaymentFacade)
	{
		this.payuPaymentFacade = payuPaymentFacade;
	}

	/**
	 * @return the commerceCheckoutService
	 */
	public CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}

	/**
	 * @param commerceCheckoutService
	 *           the commerceCheckoutService to set
	 */
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the eventPublishingSubmitOrderStrategy
	 */
	public EventPublishingSubmitOrderStrategy getEventPublishingSubmitOrderStrategy()
	{
		return eventPublishingSubmitOrderStrategy;
	}

	/**
	 * @param eventPublishingSubmitOrderStrategy
	 *           the eventPublishingSubmitOrderStrategy to set
	 */
	public void setEventPublishingSubmitOrderStrategy(final EventPublishingSubmitOrderStrategy eventPublishingSubmitOrderStrategy)
	{
		this.eventPublishingSubmitOrderStrategy = eventPublishingSubmitOrderStrategy;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
