/**
 *
 */
package de.hybris.clicks.integration.data.populators;

import de.hybris.clicks.clickscommercewebservices.dto.AccountsCBRWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.AddressWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.AfricaAccountsCBRWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.AfricaAccountsPSWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.BotswanaAccountsCBRWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.BotswanaAccountsPSWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.CBRtransactionWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.ClicksUserResponseWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.ContactDetailWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.CountryWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.LoyalityDetailsWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.MediaWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.PSPointWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.PSPointsWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.PStransactionWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.PStransactionsWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.PointsStatementWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.ProvinceWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.ResultWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.SegmentDetailsWsDTO;
import de.hybris.clicks.core.model.AccountsCBRModel;
import de.hybris.clicks.core.model.AccountsPSModel;
import de.hybris.clicks.core.model.CBRtransactionModel;
import de.hybris.clicks.core.model.ConsentModel;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.PS_PartnerPointModel;
import de.hybris.clicks.core.model.PS_PointsBucketModel;
import de.hybris.clicks.core.model.PStransactionModel;
import de.hybris.clicks.core.model.Type_consentModel;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;



/**
 * @author manish.bhargava
 *
 */
public class ClicksCustomerDataPopulator
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	public ClicksUserResponseWsDTO prepareUserResponse(final CustomerModel source, final ResultData result)
	{
		final ClicksUserResponseWsDTO clicksUserWsDTO = new ClicksUserResponseWsDTO();
		final LoyalityDetailsWsDTO loyalityDetailsWsDTO = new LoyalityDetailsWsDTO();
		try
		{
			// CustomerId
			if (StringUtils.isNotBlank(source.getCustomerID()))
			{
				clicksUserWsDTO.setCustomerID(source.getCustomerID());
			}

			if (StringUtils.isNotBlank(source.getName()))
			{
				clicksUserWsDTO.setPreferredName(source.getName());
			}
			if (StringUtils.isNotBlank(source.getFirstName()))
			{
				clicksUserWsDTO.setFirstName(source.getFirstName());
			}
			if (StringUtils.isNotBlank(source.getLastName()))
			{
				clicksUserWsDTO.setLastName(source.getLastName());
			}
			if (null != source.getGender() && StringUtils.isNotBlank(source.getGender().getCode()))
			{
				if ("MALE".equalsIgnoreCase(source.getGender().getCode()))
				{
					clicksUserWsDTO.setGender("1");
				}
				else
				{
					clicksUserWsDTO.setGender("2");
				}
			}

			if (null != source.getSAResident())
			{
				clicksUserWsDTO.setSAResident(source.getSAResident().booleanValue());
				if (source.getSAResident().booleanValue())
				{
					clicksUserWsDTO.setIdNumber(source.getRSA_ID());
				}
				if (null != source.getNonRSA_DOB())
				{
					clicksUserWsDTO.setDateOfBirth(new SimpleDateFormat("yyyy-MM-dd").format(source.getNonRSA_DOB()));
				}
			}

			if (StringUtils.isNotBlank(source.getEmailID()))
			{
				clicksUserWsDTO.setEmailAddress(source.getEmailID());
			}

			if (null != source.getProfilePicture())
			{
				final MediaWsDTO mediaWsDTO = new MediaWsDTO();
				mediaWsDTO.setUrl(source.getProfilePicture().getURL());
				mediaWsDTO.setCode(source.getProfilePicture().getCode());
				clicksUserWsDTO.setBarCodeImage(mediaWsDTO);
			}

			if (null != result)
			{
				final ResultWsDTO resultWsDTO = new ResultWsDTO();
				resultWsDTO.setCode(result.getCode().toString());
				resultWsDTO.setMessage(result.getMessage());
				clicksUserWsDTO.setResult(resultWsDTO);
			}
			/* Code changes for edit personal details starts */
			if (CollectionUtils.isNotEmpty(source.getAddresses()))
			{
				for (final AddressModel addressmodel : source.getAddresses())
				{
					if (null != addressmodel.getShippingAddress() && null != addressmodel.getBillingAddress()
							&& !addressmodel.getShippingAddress().booleanValue() && !addressmodel.getBillingAddress().booleanValue())
					{
						final AddressWsDTO addressWsDTO = new AddressWsDTO();
						if (StringUtils.isNotBlank(addressmodel.getBuilding()))
						{
							addressWsDTO.setAddressLine1(addressmodel.getBuilding());
						}
						if (StringUtils.isNotBlank(addressmodel.getStreetname()))
						{
							addressWsDTO.setAddressLine2(addressmodel.getStreetname());
						}
						if (StringUtils.isNotBlank(addressmodel.getStreetnumber()))
						{
							addressWsDTO.setSuburb(addressmodel.getStreetnumber());
						}
						if (StringUtils.isNotBlank(addressmodel.getTown()))
						{
							addressWsDTO.setTown(getTitleCase(addressmodel.getTown()));
						}
						if (StringUtils.isNotBlank(addressmodel.getProvince()))
						{
							final ProvinceWsDTO provinceWsDTO = new ProvinceWsDTO();
							provinceWsDTO.setCode(addressmodel.getProvince());
							addressWsDTO.setProvince(provinceWsDTO);
						}
						if (null != addressmodel.getCountry() && null != addressmodel.getCountry().getIsocode())
						{
							final CountryWsDTO countryWsDTO = new CountryWsDTO();
							countryWsDTO.setIsocode(addressmodel.getCountry().getIsocode());
							addressWsDTO.setCountry(countryWsDTO);
						}
						if (StringUtils.isNotBlank(addressmodel.getPostalcode()))
						{
							addressWsDTO.setPostalCode(addressmodel.getPostalcode());
						}
						clicksUserWsDTO.setAddressDetails(addressWsDTO);
						break;
					}
				}
			}
			if (CollectionUtils.isNotEmpty(source.getContactDetails()))
			{
				final List<ContactDetailWsDTO> contactDetailWsDTOList = new ArrayList<ContactDetailWsDTO>();
				for (final ContactDetailsModel contactModel : source.getContactDetails())
				{
					final ContactDetailWsDTO contactDetailWsDTO = new ContactDetailWsDTO();
					contactDetailWsDTO.setNumber(contactModel.getNumber());
					contactDetailWsDTO.setTypeID(contactModel.getTypeID());
					contactDetailWsDTOList.add(contactDetailWsDTO);
				}
				clicksUserWsDTO.setContactDetails(contactDetailWsDTOList);
			}

			if (StringUtils.isNotBlank(source.getMemberID()))
			{
				loyalityDetailsWsDTO.setClubCardNumber(source.getMemberID());
			}

			populateConsent(source, loyalityDetailsWsDTO);

			// Population CBR Statements
			if (null != source.getCBRstatement() && CollectionUtils.isNotEmpty(source.getCBRstatement().getAccountsCBRList()))
			{
				final AccountsCBRWsDTO accountsCBRWsDTO = new AccountsCBRWsDTO();
				for (final AccountsCBRModel accountsCBRModel : source.getCBRstatement().getAccountsCBRList())
				{
					if (null != accountsCBRModel.getAccountID() && accountsCBRModel.getAccountID().equals(new Integer(5000001)))
					{
						final BotswanaAccountsCBRWsDTO botswanaAccountsCBRWsDTO = new BotswanaAccountsCBRWsDTO();
						botswanaAccountsCBRWsDTO.setAccountID(accountsCBRModel.getAccountID().toString());
						botswanaAccountsCBRWsDTO.setCbrBalance(null != accountsCBRModel.getCBRbalance() ? accountsCBRModel
								.getCBRbalance().toString() : "");
						if (CollectionUtils.isNotEmpty(accountsCBRModel.getCBRtransactionList()))
						{
							botswanaAccountsCBRWsDTO.setCbrTransactionList(populateCbrTransactionList(accountsCBRModel
									.getCBRtransactionList()));
							accountsCBRWsDTO.setBotswanaCBRAccount(botswanaAccountsCBRWsDTO);
						}
					}

					if (null != accountsCBRModel.getAccountID() && accountsCBRModel.getAccountID().equals(new Integer(5000000)))
					{
						final AfricaAccountsCBRWsDTO africaAccountsCBRWsDTO = new AfricaAccountsCBRWsDTO();
						africaAccountsCBRWsDTO.setAccountID(accountsCBRModel.getAccountID().toString());
						africaAccountsCBRWsDTO.setCbrBalance(null != accountsCBRModel.getCBRbalance() ? accountsCBRModel
								.getCBRbalance().toString() : "");
						if (CollectionUtils.isNotEmpty(accountsCBRModel.getCBRtransactionList()))
						{
							africaAccountsCBRWsDTO.setCbrTransactionList(populateCbrTransactionList(accountsCBRModel
									.getCBRtransactionList()));
							accountsCBRWsDTO.setAfricaCBRAccount(africaAccountsCBRWsDTO);
						}
					}
				}
				loyalityDetailsWsDTO.setCbrStatement(accountsCBRWsDTO);
			}
			// Populating PointStatements
			if (null != source.getPointsStatement())
			{
				final PointsStatementWsDTO pointsStatementWsDTO = new PointsStatementWsDTO();
				pointsStatementWsDTO.setStartDate(source.getPointsStatement().getStartDate());
				pointsStatementWsDTO.setEndDate(source.getPointsStatement().getEndDate());

				if (CollectionUtils.isNotEmpty(source.getPointsStatement().getAccountsPSList()))
				{
					for (final AccountsPSModel accountsPSModel : source.getPointsStatement().getAccountsPSList())
					{
						if (null != accountsPSModel.getAccountID() && accountsPSModel.getAccountID().equals(new Integer(5000003)))
						{
							final BotswanaAccountsPSWsDTO botswanaAccountsPSWsDTO = new BotswanaAccountsPSWsDTO();

							botswanaAccountsPSWsDTO.setAccountID(accountsPSModel.getAccountID().toString());
							botswanaAccountsPSWsDTO.setPointsToQualify(null != accountsPSModel.getPointsToQualify() ? accountsPSModel
									.getPointsToQualify().toString() : "");
							botswanaAccountsPSWsDTO.setPointsBalance(null != accountsPSModel.getPointsBalance() ? accountsPSModel
									.getPointsBalance().toString() : "");
							botswanaAccountsPSWsDTO.setTotalSpent(null != accountsPSModel.getTotalSpent() ? accountsPSModel
									.getTotalSpent().toString() : "");
							botswanaAccountsPSWsDTO.setPsTransactions(populatePStransaction(accountsPSModel));
							botswanaAccountsPSWsDTO.setPs_PartnerPoints(populatePoints(accountsPSModel, "partnerPoints"));
							botswanaAccountsPSWsDTO.setPs_PointsBuckets(populatePoints(accountsPSModel, "bucketPoints"));
							pointsStatementWsDTO.setBotswanaAccount(botswanaAccountsPSWsDTO);
						}

						if (null != accountsPSModel.getAccountID() && accountsPSModel.getAccountID().equals(new Integer(5000002)))
						{
							final AfricaAccountsPSWsDTO africaAccountsPSWsDTO = new AfricaAccountsPSWsDTO();

							africaAccountsPSWsDTO.setAccountID(accountsPSModel.getAccountID().toString());
							africaAccountsPSWsDTO.setPointsToQualify(null != accountsPSModel.getPointsToQualify() ? accountsPSModel
									.getPointsToQualify().toString() : "");
							africaAccountsPSWsDTO.setPointsBalance(null != accountsPSModel.getPointsBalance() ? accountsPSModel
									.getPointsBalance().toString() : "");
							africaAccountsPSWsDTO.setTotalSpent(null != accountsPSModel.getTotalSpent() ? accountsPSModel
									.getTotalSpent().toString() : "");
							africaAccountsPSWsDTO.setPsTransactions(populatePStransaction(accountsPSModel));
							africaAccountsPSWsDTO.setPs_PartnerPoints(populatePoints(accountsPSModel, "partnerPoints"));
							africaAccountsPSWsDTO.setPs_PointsBuckets(populatePoints(accountsPSModel, "bucketPoints"));
							pointsStatementWsDTO.setAfricaAccount(africaAccountsPSWsDTO);
						}
					}
				}
				loyalityDetailsWsDTO.setPointsStatement(pointsStatementWsDTO);
			}
			clicksUserWsDTO.setLoyaltyDetails(loyalityDetailsWsDTO);
			if (CollectionUtils.isNotEmpty(source.getAllGroups()))
			{
				final List<SegmentDetailsWsDTO> segmentDetailsWsDTOList = new ArrayList<SegmentDetailsWsDTO>();
				for (final PrincipalGroupModel principalGroup : source.getAllGroups())
				{
					if (principalGroup instanceof UserGroupModel)
					{
						final UserGroupModel userGroup = (UserGroupModel) principalGroup;
						final SegmentDetailsWsDTO segmentDetailsWsDTO = new SegmentDetailsWsDTO();
						segmentDetailsWsDTOList.add(populateSegmentData(userGroup, segmentDetailsWsDTO));
					}

				}

				clicksUserWsDTO.setSegmentDetails(segmentDetailsWsDTOList);
			}
		}
		catch (final Exception e)
		{
			//
		}
		return clicksUserWsDTO;
	}

	/**
	 * @param source
	 * @param loyalityDetailsWsDTO
	 */
	private void populateConsent(final CustomerModel source, final LoyalityDetailsWsDTO loyalityDetailsWsDTO)
	{
		try
		{
			for (final ConsentModel consentModel : source.getMarketingConsent())
			{
				if ("ACCINFO".equalsIgnoreCase(consentModel.getConsentType()))
				{
					for (final Type_consentModel type : consentModel.getComms())
					{
						if (Integer.valueOf(1).equals(type.getTypeID()) && Boolean.TRUE.equals(type.getAccepted()))
						{
							loyalityDetailsWsDTO.setAccSMSConsentInfo(Boolean.TRUE.booleanValue());
						}
						else if ((Integer.valueOf(2).equals(type.getTypeID())) && Boolean.TRUE.equals(type.getAccepted()))
						{
							loyalityDetailsWsDTO.setAccEmailConsentInfo(Boolean.TRUE.booleanValue());
						}
					}

				}
				else if ("MARKETING".equalsIgnoreCase(consentModel.getConsentType()))
				{
					for (final Type_consentModel type : consentModel.getComms())
					{
						if (Integer.valueOf(1).equals(type.getTypeID()) && Boolean.TRUE.equals(type.getAccepted()))
						{
							loyalityDetailsWsDTO.setMarketingSMSConsentInfo(Boolean.TRUE.booleanValue());
						}
						else if ((Integer.valueOf(2).equals(type.getTypeID())) && Boolean.TRUE.equals(type.getAccepted()))
						{
							loyalityDetailsWsDTO.setMarketingEmailConsentInfo(Boolean.TRUE.booleanValue());
						}
					}

				}
			}
		}
		catch (final Exception e)
		{
			//
		}
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	private List<CBRtransactionWsDTO> populateCbrTransactionList(final List<CBRtransactionModel> cbrTransactionModelList)
	{
		final List<CBRtransactionWsDTO> cbrTransactionWsDTOList = new ArrayList<CBRtransactionWsDTO>();
		for (final CBRtransactionModel cbrTransactionModel : cbrTransactionModelList)
		{
			final CBRtransactionWsDTO cbrTransactionWsDTO = new CBRtransactionWsDTO();
			if (null != cbrTransactionModel.getAmountIssued())
			{
				cbrTransactionWsDTO.setAmountIssued(cbrTransactionModel.getAmountIssued().toString());
			}
			if (null != cbrTransactionModel.getAvailableAmount())
			{
				cbrTransactionWsDTO.setAvailableAmount(cbrTransactionModel.getAvailableAmount().toString());
			}
			if (null != cbrTransactionModel.getExpiryDate())
			{
				cbrTransactionWsDTO.setExpiryDate(cbrTransactionModel.getExpiryDate());
			}
			if (null != cbrTransactionModel.getIssueDate())
			{
				cbrTransactionWsDTO.setIssueDate(cbrTransactionModel.getIssueDate());
			}
			cbrTransactionWsDTOList.add(cbrTransactionWsDTO);
		}
		return cbrTransactionWsDTOList;
	}

	private PStransactionsWsDTO populatePStransaction(final AccountsPSModel accountsPSModel)
	{
		final PStransactionsWsDTO pstransactionsWsDTO = new PStransactionsWsDTO();
		if (null != accountsPSModel.getPStransactions())
		{
			pstransactionsWsDTO.setBalance(null != accountsPSModel.getPStransactions().getBalance() ? accountsPSModel
					.getPStransactions().getBalance().toString() : "");
			final List<PStransactionWsDTO> psTransactionWsDTOList = new ArrayList<PStransactionWsDTO>();

			for (final PStransactionModel pstransactionModel : accountsPSModel.getPStransactions().getPStransactionList())
			{
				final PStransactionWsDTO psTransactionWsDTO = new PStransactionWsDTO();
				if (null != pstransactionModel.getSpent())
				{
					psTransactionWsDTO.setSpent(pstransactionModel.getSpent().toString());
				}
				if (null != pstransactionModel.getEarned())
				{
					psTransactionWsDTO.setEarned(pstransactionModel.getEarned().toString());
				}
				if (null != pstransactionModel.getDate())
				{
					psTransactionWsDTO.setDate(pstransactionModel.getDate());
				}
				PointOfServiceModel posModel = new PointOfServiceModel();
				posModel.setName(pstransactionModel.getLocation());
				try
				{
					posModel = flexibleSearchService.getModelByExample(posModel);
					if (null != posModel)
					{
						psTransactionWsDTO.setLocation(posModel.getDisplayName());
					}
				}
				catch (final Exception e)
				{
					//
				}
				psTransactionWsDTOList.add(psTransactionWsDTO);
			}
			pstransactionsWsDTO.setPsTransactionList(psTransactionWsDTOList);
		}
		return pstransactionsWsDTO;
	}

	private PSPointsWsDTO populatePoints(final AccountsPSModel accountsPSModel, final String matchCase)
	{
		final PSPointsWsDTO psPointsWsDTO = new PSPointsWsDTO();
		final List<PSPointWsDTO> psPointWsDTOList = new ArrayList<PSPointWsDTO>();
		if (matchCase.equalsIgnoreCase("partnerPoints") && null != accountsPSModel.getPartnerPoints())
		{
			psPointsWsDTO.setBalance(null != accountsPSModel.getPartnerPoints().getBalance() ? accountsPSModel.getPartnerPoints()
					.getBalance().toString() : "");

			for (final PS_PartnerPointModel partnerModel : accountsPSModel.getPartnerPoints().getPS_PartnerPointList())
			{
				final PSPointWsDTO psPointWsDTO = new PSPointWsDTO();
				psPointWsDTO.setName(partnerModel.getName());
				psPointWsDTO.setValue(null != partnerModel.getValue() ? partnerModel.getValue().toString() : "");
				psPointWsDTOList.add(psPointWsDTO);
			}
			psPointsWsDTO.setPsPointList(psPointWsDTOList);
		}
		else if (matchCase.equalsIgnoreCase("bucketPoints") && null != accountsPSModel.getPointsBuckets())
		{
			psPointsWsDTO.setBalance(null != accountsPSModel.getPointsBuckets().getBalance() ? accountsPSModel.getPointsBuckets()
					.getBalance().toString() : "");

			for (final PS_PointsBucketModel bucketModel : accountsPSModel.getPointsBuckets().getPS_PointsBucketList())
			{
				final PSPointWsDTO psPointWsDTO = new PSPointWsDTO();
				psPointWsDTO.setName(bucketModel.getName());
				psPointWsDTO.setValue(null != bucketModel.getValue() ? bucketModel.getValue().toString() : "");
				psPointWsDTOList.add(psPointWsDTO);
			}
			psPointsWsDTO.setPsPointList(psPointWsDTOList);
		}
		return psPointsWsDTO;
	}


	/**
	 * @param userGroup
	 * @param segmentDetailsWsDTO
	 * @return
	 */
	private SegmentDetailsWsDTO populateSegmentData(final UserGroupModel userGroup, final SegmentDetailsWsDTO segmentDetailsWsDTO)
	{
		if (null != userGroup.getSegmentID())
		{
			segmentDetailsWsDTO.setSegmentID(userGroup.getSegmentID());
		}
		if (StringUtils.isNotBlank(userGroup.getSegmentDescription()))
		{
			segmentDetailsWsDTO.setSegmentDescription(userGroup.getSegmentDescription());
		}
		if (null != userGroup.getSegmentStatus())
		{
			segmentDetailsWsDTO.setSegmentStatus(userGroup.getSegmentStatus());
		}
		if (StringUtils.isNotBlank(userGroup.getSegmentType()))
		{
			segmentDetailsWsDTO.setSegmentType(userGroup.getSegmentType());
		}
		if (null != userGroup.getStartDate())
		{
			segmentDetailsWsDTO.setStartDate(userGroup.getStartDate());
		}
		if (null != userGroup.getEndDate())
		{
			segmentDetailsWsDTO.setEndDate(userGroup.getEndDate());
		}
		return segmentDetailsWsDTO;
	}

}
