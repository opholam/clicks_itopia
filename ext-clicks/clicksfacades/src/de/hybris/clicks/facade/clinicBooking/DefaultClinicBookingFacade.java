/**
 *
 */
package de.hybris.clicks.facade.clinicBooking;

import de.hybris.clicks.core.clinicBooking.ClinicBookingService;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author shruti.jhamb
 *
 */
public class DefaultClinicBookingFacade implements ClinicBookingFacade
{
	protected static final String CLINIC_BOOKING_EMAIL_PROCESS = "clinicBookingEmailProcess";



	@Resource(name = "clinicBookingService")
	ClinicBookingService clinicBookingService;

	private String adminEmail;


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facade.clinicBooking.ClinicBookingFacade#getProvincesList()
	 */
	@Override
	public Collection<ProvinceData> getProvincesList()
	{
		final Collection<ProvinceData> provinceList = clinicBookingService.getProvinceList();
		return provinceList;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facade.clinicBooking.ClinicBookingFacade#getPreferredClinicsList()
	 */
	@Override
	public List<PreferredClinicData> getPreferredClinicsList(final String code)
	{
		final List<PreferredClinicData> preferredClinicDataList = clinicBookingService.getpreferredClinicList(code);
		return preferredClinicDataList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.facade.clinicBooking.ClinicBookingFacade#processsBookingRequest(de.hybris.clicks.storefront.forms
	 * .ClinicBookingForm, org.springframework.validation.BindingResult, org.springframework.ui.Model,
	 * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public String processsBookingRequest(final ClinicBookingData bookingData)
	{
		bookingData.setProcessName(CLINIC_BOOKING_EMAIL_PROCESS);
		final boolean result = clinicBookingService.processClinicBooking(bookingData);

		if (result)
		{
			return "success";
		}
		else
		{
			return "failed";
		}
	}
}
