/**
 *
 */
package de.hybris.clicks.facade.clinicBooking;

import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;

import java.util.Collection;
import java.util.List;


/**
 * @author shruthi.jhamb
 *
 */
public interface ClinicBookingFacade
{

	/**
	 * Gets the provinces list.
	 *
	 * @return the provinces list
	 */
	Collection<ProvinceData> getProvincesList();

	/**
	 * Gets the preferred clinics list.
	 *
	 * @param code
	 *           the code
	 * @return the preferred clinics list
	 */
	List<PreferredClinicData> getPreferredClinicsList(String code);

	/**
	 * Processs booking request. submits the query to the clicks team
	 *
	 * @param bookingData
	 *           the booking data
	 * @return the string
	 */
	String processsBookingRequest(ClinicBookingData bookingData);

}
