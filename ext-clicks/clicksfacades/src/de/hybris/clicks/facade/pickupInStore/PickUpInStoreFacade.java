/**
 *
 */
package de.hybris.clicks.facade.pickupInStore;

import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.servicelayer.search.SearchResult;




/**
 * @author shruthi.jhamb
 *
 */
public interface PickUpInStoreFacade
{

	/**
	 * @param storeName
	 * @param productCode
	 * @param email
	 *
	 */
	void subscribeUserForStock(String email, String productCode, String storeName);

	public SearchResult<LinkModel> fetchStoreLocation(final String code);

}
