/**
 *
 */
package de.hybris.clicks.facade.pickupInStore;

import de.hybris.clicks.core.model.StockNotificationModel;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import org.apache.log4j.Logger;



/**
 * @author shruti.jhamb
 *
 */
public class PickUpInStoreFacadeImpl implements PickUpInStoreFacade
{
	protected static final Logger LOG = Logger.getLogger(PickUpInStoreFacadeImpl.class);

	@Resource
	private ModelService modelService;

	@Resource
	FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facade.pickupInStore.PickUpInStoreFacade#subscribeUserForStock(java.lang.String,
	 * java.lang.String, java.lang.String)
	 *
	 * saving the subscribe user,product and store details to a model
	 */
	@Override
	public void subscribeUserForStock(final String email, final String productCode, final String storeName)
	{
		final StockNotificationModel stockNotificationModel = modelService.create(StockNotificationModel.class);
		stockNotificationModel.setEmailID(email);
		stockNotificationModel.setProductCode(productCode);
		stockNotificationModel.setStockChangeFlag(true);
		stockNotificationModel.setStoreID(storeName);
		try
		{
			modelService.save(stockNotificationModel);
		}
		catch (final ModelSavingException savexc)
		{
			LOG.error(savexc);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facade.pickupInStore.PickUpInStoreFacade#fetchStoreLocation(java.lang.String) fetching store
	 * locations which are only stores(excluding clinics and pharmacies)
	 */
	@Override
	public SearchResult<LinkModel> fetchStoreLocation(final String code)
	{
		//use query param
		final String query = "select {pk} from {StoreLocation2StoreLocatorFeature as s join pointofservice as pos on {s:source}={pos:pk} join StoreLocatorFeature as f on {s:target}={f:pk}} where {f:Code} like '"
				+ code + "Str'";
		final SearchResult<LinkModel> result = flexibleSearchService.search(query);
		return result;
	}

}
