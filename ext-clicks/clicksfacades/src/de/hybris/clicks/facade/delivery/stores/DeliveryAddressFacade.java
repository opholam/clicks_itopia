/**
 *
 */
package de.hybris.clicks.facade.delivery.stores;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import org.springframework.ui.Model;






/**
 * @author shruthi.jhamb
 *
 */
public interface DeliveryAddressFacade
{
	/**
	 * @param sanitizedSearchQuery
	 * @param pageableData
	 * @return
	 */
	StoreFinderSearchPageData<PointOfServiceData> fetchStores(String sanitizedSearchQuery, PageableData pageableData);

	/**
	 * @param deliveryAdd
	 * @param cart
	 * @param tempAddr
	 * @param model
	 */
	void getCartData(final CartData cart, final Model model);

	/**
	 * @param current
	 *           fetch current customer shipping address list
	 */
	List<AddressModel> fetchCustAddressList(CustomerModel currentCheckoutCus, final List<AddressModel> customerAddressList);

	List<AddressModel> fetchStoreAddressList(final CustomerModel currentCheckoutCus, final List<AddressModel> customerAddressList);

	/**
	 * @param geoPoint
	 * @param pageableData
	 */
	StoreFinderSearchPageData<PointOfServiceData> fetchNearByStores(GeoPoint geoPoint, PageableData pageableData);

}
