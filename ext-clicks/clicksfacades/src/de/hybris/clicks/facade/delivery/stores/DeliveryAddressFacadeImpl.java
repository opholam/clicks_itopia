/**
 *
 */
package de.hybris.clicks.facade.delivery.stores;

import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorservices.store.data.UserLocationData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.storefinder.StoreFinderFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;



/**
 * @author shruti.jhamb
 *
 */
public class DeliveryAddressFacadeImpl implements DeliveryAddressFacade
{

	private static final Logger LOG = Logger.getLogger(DeliveryAddressFacadeImpl.class);

	@Resource(name = "storeFinderFacade")
	private StoreFinderFacade storeFinderFacade;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facade.delivery.stores.DeliveryAddressFacade#fetchStores(java.lang.String,
	 * de.hybris.platform.commerceservices.search.pagedata.PageableData)
	 */
	@Override
	public StoreFinderSearchPageData<PointOfServiceData> fetchStores(final String sanitizedSearchQuery,
			final PageableData pageableData)
	{
		StoreFinderSearchPageData<PointOfServiceData> searchResults = new StoreFinderSearchPageData<PointOfServiceData>();
		searchResults = setUpSearchResultsForLocationQuery(sanitizedSearchQuery, pageableData);
		return searchResults;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.facade.delivery.stores.DeliveryAddressFacade#fetchNearByStores(de.hybris.platform.commerceservices
	 * .store.data.GeoPoint, de.hybris.platform.commerceservices.search.pagedata.PageableData)
	 */
	@Override
	public StoreFinderSearchPageData<PointOfServiceData> fetchNearByStores(final GeoPoint geoPoint, final PageableData pageableData)
	{
		try
		{
			// Run the location search & populate the model
			final StoreFinderSearchPageData<PointOfServiceData> searchResult = storeFinderFacade.positionSearch(geoPoint,
					pageableData);
			final GeoPoint newGeoPoint = new GeoPoint();
			newGeoPoint.setLatitude(searchResult.getSourceLatitude());
			newGeoPoint.setLongitude(searchResult.getSourceLongitude());
			updateLocalUserPreferences(newGeoPoint, searchResult.getLocationText());
			return searchResult;
		}
		catch (final Exception exc)
		{
			LOG.error(exc);
			return null;
		}
	}


	private StoreFinderSearchPageData<PointOfServiceData> setUpSearchResultsForLocationQuery(final String locationQuery,
			final PageableData pageableData)
	{

		// Run the location search & populate the model
		final StoreFinderSearchPageData<PointOfServiceData> searchResult = storeFinderFacade.locationSearch(
				StringUtils.lowerCase(locationQuery), pageableData);

		final GeoPoint geoPoint = new GeoPoint();
		geoPoint.setLatitude(searchResult.getSourceLatitude());
		geoPoint.setLongitude(searchResult.getSourceLongitude());

		updateLocalUserPreferences(geoPoint, searchResult.getLocationText());
		//pickUpStoreForm = setUpPageData(pickUpStoreForm, searchResult);
		return searchResult;

	}


	/**
	 * @param geoPoint
	 * @param locationText
	 */
	protected void updateLocalUserPreferences(final GeoPoint geoPoint, final String location)
	{
		final UserLocationData userLocationData = new UserLocationData();
		if (StringUtils.isNotBlank(location) && null != geoPoint)
		{
			userLocationData.setSearchTerm(location);
			userLocationData.setPoint(geoPoint);
		}
		//customerLocationFacade.setUserLocationData(userLocationData);
		customerLocationService.setUserLocation(userLocationData);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facade.delivery.stores.DeliveryAddressFacade#getDeliveryAddressData(de.hybris.platform.
	 * commercefacades.user.data.AddressData, de.hybris.platform.commercefacades.order.data.CartData,
	 * de.hybris.platform.commercefacades.user.data.AddressData, org.springframework.ui.Model)
	 */
	@Override
	public void getCartData(final CartData cart, final Model model)
	{
		double subtotalVal = 0.00;
		if (null != cart.getSubTotal().getFormattedValue())
		{
			final String subTotalPrice = cart.getSubTotal().getFormattedValue();
			final String formattedPrice = subTotalPrice.substring(1);
			subtotalVal = Double.parseDouble(formattedPrice.replaceAll(",", ""));
			if (null != cart.getOrderDiscounts())
			{
				final double orderDiscount = Double.parseDouble(cart.getOrderDiscounts().getFormattedValue().substring(1)
						.replaceAll(",", ""));
				subtotalVal = subtotalVal - orderDiscount;
			}
			BigDecimal subtotal = BigDecimal.valueOf(subtotalVal);
			subtotal = subtotal.setScale(2, RoundingMode.CEILING);
			model.addAttribute("subtotal", subtotal);
		}
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.clicks.facade.delivery.stores.DeliveryAddressFacade#fetchCustAddressList(de.hybris.platform.core.model
	 * .user.CustomerModel)
	 */
	@Override
	public List<AddressModel> fetchCustAddressList(final CustomerModel currentCheckoutCus,
			final List<AddressModel> customerAddressList)
	{
		if (null != currentCheckoutCus.getAddresses())
		{
			for (final AddressModel address : currentCheckoutCus.getAddresses())
			{
				if (Boolean.TRUE.equals(address.getShippingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook()))
				{
					customerAddressList.add(address);
				}
			}
		}
		return customerAddressList;
	}

	@Override
	public List<AddressModel> fetchStoreAddressList(final CustomerModel currentCheckoutCus,
			final List<AddressModel> customerAddressList)
	{
		if (null != currentCheckoutCus.getAddresses())
		{
			for (final AddressModel address : currentCheckoutCus.getAddresses())
			{
				if (Boolean.TRUE.equals(address.getShippingAddress())
						&& Boolean.FALSE.equals(address.getVisibleInAddressBook() && Boolean.TRUE.equals(address.getIsStore())))
				{
					customerAddressList.add(address);
				}
			}
		}
		return customerAddressList;
	}
}