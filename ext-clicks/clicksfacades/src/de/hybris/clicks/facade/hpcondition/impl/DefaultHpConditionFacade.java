/**
 *
 */
package de.hybris.clicks.facade.hpcondition.impl;

import de.hybris.clicks.core.hpcondition.dao.HpConditionDao;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.facade.hpcondition.HpConditionFacade;
import de.hybris.clicks.facade.utility.HealthConditionDataComparator;
import de.hybris.platform.commercefacades.user.data.HealthConditionData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;




/**
 * @author anshul/Swapnil
 * 
 */
public class DefaultHpConditionFacade implements HpConditionFacade
{
	//private static final Logger LOG = Logger.getLogger(DefaultHpConditionFacade.class);

	@Resource(name = "hpConditionDao")
	private HpConditionDao hpConditionDao;

	private Converter<ConditionsModel, HealthConditionData> hpConditionConverter;

	@Required
	public void setHpConditionConverter(final Converter<ConditionsModel, HealthConditionData> hpConditionConverter)
	{
		this.hpConditionConverter = hpConditionConverter;
	}

	public Converter<ConditionsModel, HealthConditionData> getHpConditionConverter()
	{
		return hpConditionConverter;
	}

	@Override
	public List<HealthConditionData> findConditionsByType(final String conditionType)
	{

		final List<ConditionsModel> conditionModelList = hpConditionDao.findConditionsByType(conditionType);

		List<HealthConditionData> healthConditionDatas = Lists.newArrayList();
		if (CollectionUtils.isNotEmpty(conditionModelList))
		{
			healthConditionDatas = Converters.convertAll(conditionModelList, getHpConditionConverter());

			if (healthConditionDatas.size() > 0)
			{
				//			Collections.sort(healthConditionDatas, new HealthConditionDataComparator());
				Collections.sort(healthConditionDatas, (HealthConditionDataComparator.getComparator(
						HealthConditionDataComparator.CONDITIONS_PRIORITY, HealthConditionDataComparator.CONDITIONS_NAME)));
			}
		}
		return healthConditionDatas;
	}

	@Override
	public List<ConditionsModel> findAllConditions(final String type)
	{
		return hpConditionDao.findAllConditions(type);
	}

	@Override
	public <T extends AbstractItemModel> T getConditionsListByConditionId(final String id)
	{
		return hpConditionDao.getConditionsListByConditionId(id);
	}

}
