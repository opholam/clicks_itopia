/**
 *
 */
package de.hybris.clicks.facade.hpcondition;

import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.commercefacades.user.data.HealthConditionData;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.List;


/**
 * @author anshul/Swapnil
 *
 */
public interface HpConditionFacade
{

	List<ConditionsModel> findAllConditions(String type);

	List<HealthConditionData> findConditionsByType(String conditionType);

	<T extends AbstractItemModel> T getConditionsListByConditionId(String id);

}
