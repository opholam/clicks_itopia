/**
 *
 */
package de.hybris.clicks.facade.productpromotion.impl;

import de.hybris.clicks.core.dao.productpromotion.ProductPromotionSearchDao;
import de.hybris.clicks.facade.productpromotion.ProductPromotionSearchFacade;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Resource;


/**
 * @author mbhargava
 *
 */
public class ProductPromotionSearchFacadeImpl implements ProductPromotionSearchFacade
{
	@Resource(name = "productPromotionSearchDao")
	private ProductPromotionSearchDao productPromotionSearchDao;


	@Override
	public List<ProductModel> getPromotionsForCategory(final CategoryModel category)
	{

		return productPromotionSearchDao.getPromotionsForCategory(category);
	}

}
