/**
 *
 */
package de.hybris.clicks.facade.utility;

import de.hybris.platform.commercefacades.user.data.HealthConditionData;

import java.util.Comparator;


/**
 * @author Swapnil Desai
 *
 */
public enum HealthConditionDataComparator implements Comparator<HealthConditionData>
{
	CONDITIONS_PRIORITY
	{
		@Override
		public int compare(final HealthConditionData o1, final HealthConditionData o2)
		{
			if (o1.isPrioritize() == o2.isPrioritize())
			{
				return 0;
			}

			return (o1.isPrioritize() ? -1 : 1);
		}
	},
	CONDITIONS_NAME
	{
		@Override
		public int compare(final HealthConditionData o1, final HealthConditionData o2)
		{
			return o1.getConditionName().compareTo(o2.getConditionName());
		}
	};

	/*
	 * public static Comparator<HealthConditionData> decending(final Comparator<HealthConditionData> other) { return new
	 * Comparator<HealthConditionData>() { public int compare(final HealthConditionData o1, final HealthConditionData o2)
	 * { return -1 * other.compare(o1, o2); } }; }
	 */

	public static Comparator<HealthConditionData> getComparator(final HealthConditionDataComparator... multipleOptions)
	{
		return new Comparator<HealthConditionData>()
		{
			@Override
			public int compare(final HealthConditionData o1, final HealthConditionData o2)
			{
				for (final HealthConditionDataComparator option : multipleOptions)
				{
					final int result = option.compare(o1, o2);
					if (result != 0)
					{
						return result;
					}
				}
				return 0;
			}
		};
	}

	/*
	 * @Override public int compare(final HealthConditionData healthConditionData1, final HealthConditionData
	 * healthConditionData2) { final String conditionStageName1 = healthConditionData1.getConditionName(); final String
	 * conditionStageName2 = healthConditionData2.getConditionName(); return
	 * conditionStageName1.compareToIgnoreCase(conditionStageName2); }
	 */
}
