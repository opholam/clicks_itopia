/**
 *
 */
package de.hybris.clicks.facade.promotion;

import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;



/**
 * @author anshul
 * 
 */
public interface PromotionSearchFacade
{
	//
	public SolrSearchResult getPromotions(final String searchQuery, int pageSize, String sortCode);

	//public SolrSearchResult getPromotions(final Map<String, Object> facetMap);

	public SolrSearchResult getPromotions(String searchQuery, int pageSize, String sortCode, int offset);


}
