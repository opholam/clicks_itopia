/**
 *
 */
package de.hybris.clicks.facade.promotion.impl;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commercefacades.voucher.impl.DefaultVoucherFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author manikandan.gopalan
 *
 */
public class ClicksVoucherFacade extends DefaultVoucherFacade
{
	@Resource(name = "promotionsService")
	private PromotionsService promotionsService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Override
	public void releaseVoucher(final String voucherCode) throws VoucherOperationException
	{
		validateVoucherCodeParameter(voucherCode);
		final CartModel cartModel = getCartService().getSessionCart();
		final VoucherModel voucher = getVoucherModel(voucherCode);
		if (voucher != null && cartModel != null)
		{
			try
			{
				getVoucherService().releaseVoucher(voucherCode, cartModel);
				try
				{
					promotionsService.updatePromotions(getPromotionGroups(), cartModel);
				}
				catch (final Exception e)
				{
					//
				}
				return;
			}
			catch (final JaloPriceFactoryException e)
			{
				throw new VoucherOperationException("Couldn't release voucher: " + voucherCode);
			}
		}
	}

	@Override
	public void applyVoucher(final String voucherCode) throws VoucherOperationException
	{
		final CartModel cartModel = getCartService().getSessionCart();
		final Collection<String> appliedVouchers = getVoucherService().getAppliedVoucherCodes(cartModel);
		if (CollectionUtils.isEmpty(appliedVouchers) || Config.getBoolean("allow.voucher.stacking", false))
		{
			//super.applyVoucher(voucherCode);

			validateVoucherCodeParameter(voucherCode);
			if (!isVoucherCodeValid(voucherCode))
			{

				throw new VoucherOperationException("cart.voucher.notvalid");
			}

			//final CartModel cartModel = getCartService().getSessionCart();
			final VoucherModel voucher = getVoucherModel(voucherCode);
			if (!checkVoucherCanBeRedeemed(voucher, voucherCode))
			{
				throw new VoucherOperationException("cart.voucher.invalid.redeem");
			}
			else
			{
				try
				{
					if (!getVoucherService().redeemVoucher(voucherCode, cartModel))
					{
						throw new VoucherOperationException("cart.voucher.error");
					}
					//Important! Checking cart, if total amount <0, release this voucher
					checkCartAfterApply(voucherCode, voucher);
					return;
				}
				catch (final JaloPriceFactoryException e)
				{
					throw new VoucherOperationException("cart.voucher.error");
				}
			}

			//return;
		}
		throw new VoucherOperationException("cart.voucher.more.redemption");
	}

	@Override
	protected void checkCartAfterApply(final String lastVoucherCode, final VoucherModel lastVoucher)
			throws VoucherOperationException
	{
		final CartModel cartModel = getCartService().getSessionCart();
		try
		{
			promotionsService.updatePromotions(getPromotionGroups(), cartModel);
		}
		catch (final Exception e)
		{
			//
		}
		//Total amount in cart updated with delay... Calculating value of voucher regarding to order
		final double cartTotal = cartModel.getTotalPrice().doubleValue();
		final double voucherValue = lastVoucher.getValue().doubleValue();
		final double voucherCalcValue = (lastVoucher.getAbsolute().equals(Boolean.TRUE)) ? voucherValue
				: (cartTotal * voucherValue) / 100;
		if (null != cartModel.getSubtotal() && cartModel.getSubtotal() > 0)
		{
			if (cartModel.getSubtotal().doubleValue() - voucherCalcValue < 0)
			{
				releaseVoucher(lastVoucherCode);
				//Throw exception with specific information
				throw new VoucherOperationException("Voucher " + lastVoucherCode + " cannot be redeemed: total price exceeded");
			}

		}
	}

	protected Collection<PromotionGroupModel> getPromotionGroups()
	{
		final Collection<PromotionGroupModel> promotionGroupModels = new ArrayList<PromotionGroupModel>();
		if ((getBaseSiteService().getCurrentBaseSite() != null)
				&& (getBaseSiteService().getCurrentBaseSite().getDefaultPromotionGroup() != null))
		{
			promotionGroupModels.add(getBaseSiteService().getCurrentBaseSite().getDefaultPromotionGroup());
		}
		return promotionGroupModels;
	}

	@Override
	public void validateVoucherCodeParameter(final String voucherCode)
	{
		if (StringUtils.isBlank(voucherCode))
		{
			throw new IllegalArgumentException("cart.voucher.empty.message");
		}
	}

	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * @param promotionsService
	 *           the promotionsService to set
	 */
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}
