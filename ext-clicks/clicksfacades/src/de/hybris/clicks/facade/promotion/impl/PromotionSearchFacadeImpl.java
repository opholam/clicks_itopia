/**
 *
 */
package de.hybris.clicks.facade.promotion.impl;

import de.hybris.clicks.facade.promotion.PromotionSearchFacade;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.FacetSearchService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery.Operator;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;



/**
 * @author anshul
 *
 */
public class PromotionSearchFacadeImpl implements PromotionSearchFacade
{
	private static final Logger LOG = Logger.getLogger(PromotionSearchFacadeImpl.class);

	private static final String PROMOTION_TYPE = "clicksPromotionType";

	@Resource(name = "extFacetSearchService")
	private FacetSearchService facetSearchService;
	@Resource(name = "solrFacetSearchConfigSelectionStrategy")
	private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;
	@Resource(name = "facetSearchConfigService")
	private FacetSearchConfigService facetSearchConfigService;
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	/**
	 * @return the facetSearchService
	 */
	public FacetSearchService getFacetSearchService()
	{
		return facetSearchService;
	}

	/**
	 * @param facetSearchService
	 *           the facetSearchService to set
	 */
	public void setFacetSearchService(final FacetSearchService facetSearchService)
	{
		this.facetSearchService = facetSearchService;
	}

	protected Collection<CatalogVersionModel> getSessionProductCatalogVersions()
	{
		final BaseSiteModel currentSite = getBaseSiteService().getCurrentBaseSite();
		final List productCatalogs = getBaseSiteService().getProductCatalogs(currentSite);

		final Collection<CatalogVersionModel> sessionCatalogVersions = getCatalogVersionService().getSessionCatalogVersions();

		final Collection result = new ArrayList();
		for (final CatalogVersionModel sessionCatalogVersion : sessionCatalogVersions)
		{
			if (!productCatalogs.contains(sessionCatalogVersion.getCatalog()))
			{
				continue;
			}
			result.add(sessionCatalogVersion);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facade.promotion.PromotionSearchFacade#getPromotions()
	 */
	@Override
	public SolrSearchResult getPromotions(final String searchQuery, final int pageSize, final String sortCode, final int offset)
	{
		try
		{
			final SearchQuery query = getSolrQuery();
			query.setOffset(offset);
			query.setPageSize(pageSize);
			if (StringUtils.isNotBlank(sortCode))
			{
				final String[] sort = sortCode.split("-");
				query.addOrderField(sort[0], sortCode.contains("-desc") ? false : true);
			}
			else
			{
				query.addOrderField("startDate", false);
			}
			if (StringUtils.isNotBlank(searchQuery))
			{
				final String[] allFacets = searchQuery.split("&");
				String[] facet = null;
				String[] facetValues;
				for (final String facetString : allFacets)
				{
					if (StringUtils.isNotBlank(facetString)
							&& (facetString.contains("allCategories") || facetString.contains("promotionTypeID")))
					{
						facet = facetString.split("=");
						if (facet.length > 0)
						{
							facetValues = facet[1].split(",");
							for (final String facetValue : facetValues)
							{
								query.addFacetValue(facet[0], facetValue);
							}
						}
					}
				}
			}
			if (Config.getBoolean("show_valid_promotions_only", Boolean.TRUE))
			{
				//query.addFacetValue("enabled", "TRUE");
				query.addRawQuery(
						"enabled_boolean:TRUE AND productApprovalStatus_boolean:TRUE AND endDate_date:[NOW TO *] AND startDate_date:[* TO NOW]",
						Operator.AND);
				//query.addFacetValue("productApprovalStatus", "TRUE");
			}
			return (SolrSearchResult) facetSearchService.search(query);
		}
		catch (final Exception e)
		{
			LOG.error("Error in PromotionSearchFacadeImpl class and method is getPromotions() : " + e.getMessage());
		}
		return null;
	}

	//	@Override
	//	public SolrSearchResult getPromotions(final Map<String, Object> facetMap)
	//	{
	//		try
	//		{
	//			final SearchQuery query = getSolrQuery();
	//			for (final Entry<String, Object> facet : facetMap.entrySet())
	//			{
	//				query.addFacetValue(facet.getKey(), (String) facet.getValue());
	//			}
	//			if (Config.getBoolean("show_valid_promotions_only", Boolean.TRUE))
	//			{
	//				query.addFacetValue("enabled", "TRUE");
	//				query.addRawQuery("endDate_date:[NOW TO *] AND startDate_date:[* TO NOW]", Operator.AND);
	//			}
	//			return (SolrSearchResult) facetSearchService.search(query);
	//		}
	//		catch (final Exception e)
	//		{
	//			e.printStackTrace();
	//		}
	//		return null;
	//	}

	/**
	 * @return
	 * @throws NoValidSolrConfigException
	 * @throws FacetConfigServiceException
	 */
	private SearchQuery getSolrQuery() throws NoValidSolrConfigException, FacetConfigServiceException
	{
		final FacetSearchConfig facetSearchConfig = getFacetSearchConfig();
		final SearchQuery query = new SearchQuery(facetSearchConfig, getIndexedType(facetSearchConfig));
		//query.setCatalogVersions((List<CatalogVersionModel>) getSessionProductCatalogVersions());
		query.setCurrency(getCommonI18NService().getCurrentCurrency().getIsocode());
		query.setLanguage(getCommonI18NService().getCurrentLanguage().getIsocode());
		query.setUserQuery("*:*");
		query.setEnableSpellcheck(false);
		query.setCatalogVersions(new ArrayList<CatalogVersionModel>());
		return query;
	}

	protected IndexedType getIndexedType(final FacetSearchConfig config)
	{
		final IndexConfig indexConfig = config.getIndexConfig();

		final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
		if ((indexedTypes != null) && (!indexedTypes.isEmpty()))
		{
			for (final IndexedType index : indexedTypes)
			{
				if (PROMOTION_TYPE.equals(index.getIndexName()))
				{
					return index;
				}
			}
			return indexedTypes.iterator().next();
		}

		return null;
	}

	protected FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException, FacetConfigServiceException
	{
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = this.solrFacetSearchConfigSelectionStrategy
				.getCurrentSolrFacetSearchConfig();
		return this.facetSearchConfigService.getConfiguration(solrFacetSearchConfigModel.getName());
	}

	/**
	 * @return the solrFacetSearchConfigSelectionStrategy
	 */
	public SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy()
	{
		return solrFacetSearchConfigSelectionStrategy;
	}

	/**
	 * @param solrFacetSearchConfigSelectionStrategy
	 *           the solrFacetSearchConfigSelectionStrategy to set
	 */
	public void setSolrFacetSearchConfigSelectionStrategy(
			final SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy)
	{
		this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
	}

	/**
	 * @return the facetSearchConfigService
	 */
	public FacetSearchConfigService getFacetSearchConfigService()
	{
		return facetSearchConfigService;
	}

	/**
	 * @param facetSearchConfigService
	 *           the facetSearchConfigService to set
	 */
	public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService)
	{
		this.facetSearchConfigService = facetSearchConfigService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService
	 *           the catalogVersionService to set
	 */
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facade.promotion.PromotionSearchFacade#getPromotions(java.lang.String, int,
	 * java.lang.String)
	 */
	@Override
	public SolrSearchResult getPromotions(final String searchQuery, final int pageSize, final String sortCode)
	{
		return getPromotions(searchQuery, pageSize, sortCode, 0);
	}

}
