/**
 * 
 */
package de.hybris.clicks.facade.sitemap.stores;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;

import java.util.List;


/**
 * @author sridhar.reddy
 * 
 */
public interface SitemapStoresFacade
{
	public abstract List<PointOfServiceData> getSitemapStores();
}
