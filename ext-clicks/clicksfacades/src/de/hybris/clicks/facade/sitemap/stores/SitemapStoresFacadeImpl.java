/**
 * 
 */
package de.hybris.clicks.facade.sitemap.stores;

import de.hybris.clicks.core.sitemapStore.service.SitemapStoresService;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author sridhar.reddy
 * 
 */

public class SitemapStoresFacadeImpl implements SitemapStoresFacade
{
	@Resource(name = "sitemapStoresService")
	private SitemapStoresService sitemapStoresService;

	@Override
	public List<PointOfServiceData> getSitemapStores()
	{
		final List<PointOfServiceData> pointOfServiceData = new ArrayList();
		final List<PointOfServiceModel> pointOfService = sitemapStoresService.getSitemapStores();
		if (pointOfService != null && CollectionUtils.isNotEmpty(pointOfService))
		{
			for (final PointOfServiceModel pos : pointOfService)
			{
				final PointOfServiceData posData = new PointOfServiceData();
				posData.setDisplayName(pos.getDisplayName());
				posData.setUrl(getStoreUrl(pos));
				pointOfServiceData.add(posData);
			}
		}
		return pointOfServiceData;
	}


	public String getStoreUrl(final PointOfServiceModel posUrl)
	{
		final String urlPrefix = "/store/";
		//final String urlQueryParams = getStoreUrlQueryParams(target);

		String displayName = posUrl.getDisplayName();
		if (posUrl.getDisplayName().contains(" "))
		{
			displayName = posUrl.getDisplayName().replaceAll(" ", "-");
		}

		return urlPrefix + displayName + "/" + posUrl.getName();
	}
}
