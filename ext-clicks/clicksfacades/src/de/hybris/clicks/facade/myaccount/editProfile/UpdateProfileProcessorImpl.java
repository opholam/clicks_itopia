/**
 *
 */
package de.hybris.clicks.facade.myaccount.editProfile;

import de.hybris.clicks.core.editProfile.EditProfileService;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import javax.annotation.Resource;


/**
 * @author shruti.jhamb
 *
 */
public class UpdateProfileProcessorImpl implements UpdateProfileProcessor
{
	@Resource(name = "editProfileService")
	EditProfileService editProfileService;


	//saving non club card user updated details to customer model
	@Override
	public boolean saveUpdatedNonCCData(final CustomerData customer)
	{
		final boolean result = editProfileService.saveNonClubCardData(customer);
		return result;
	}

	//saving club card user updated details to customer model
	@Override
	public boolean saveUpdatedCCData(final CustomerData customer)
	{
		final boolean result = editProfileService.saveClubCardData(customer);
		return result;
	}
}
