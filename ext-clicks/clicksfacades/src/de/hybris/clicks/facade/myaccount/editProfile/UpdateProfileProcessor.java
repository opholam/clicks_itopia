/**
 *
 */
package de.hybris.clicks.facade.myaccount.editProfile;

import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 * @author shruti.jhamb
 *
 */

public interface UpdateProfileProcessor
{
	//saving club card user updated details to customer model
	public boolean saveUpdatedCCData(final CustomerData customer);

	//saving non club card user updated details to customer model
	public boolean saveUpdatedNonCCData(final CustomerData customer);
}
