/**
 *
 */
package de.hybris.clicks.facade.cartPage;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;


/**
 * @author shruthi.jhamb
 *
 */
public interface CartPageFacade
{

	/**
	 * Populate data.
	 *
	 * @param cartData
	 *           the cart data
	 * @param cartClubcardPoints
	 *           the cart clubcard points
	 * @return the cart data
	 */
	CartData populateData(final CartData cartData, final String cartClubcardPoints);

	/**
	 * @param cartmodel
	 * @return
	 */
	/*
	 * recalculates the cart along with promotions
	 */
	CartModel calculateCart(CartModel cartmodel);


	/**
	 * @param subTotalPrice
	 * @return
	 */
	/*
	 * calculating clubcard points according to the subtotal
	 */
	String calculateClubCardPoints(String subTotalPrice, String cartClubcardPoints);

}
