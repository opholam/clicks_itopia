/**
 *
 */
package de.hybris.clicks.facade.cartPage;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;


/**
 * @author manikandan.gopalan
 *
 */
public class ClicksCartFacade extends DefaultCartFacade
{
	@Resource
	private ModelService modelService;

	@Override
	public CartData getSessionCart()
	{
		final CartData cartData;
		if (hasSessionCart())
		{
			final CartModel cart = getCartService().getSessionCart();
			final Date currentDate = new Date();
			if (!DateUtils.isSameDay(cart.getDate(), currentDate))
			{
				cart.setDate(currentDate);
				modelService.save(cart);
			}
			cartData = getCartConverter().convert(cart);
		}
		else
		{
			cartData = createEmptyCart();
		}
		return cartData;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
	
}
