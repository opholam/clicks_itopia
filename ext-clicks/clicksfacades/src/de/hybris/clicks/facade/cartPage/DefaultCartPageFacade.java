/**
 *
 */
package de.hybris.clicks.facade.cartPage;

import de.hybris.clicks.core.cartPage.CartPageService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Resource;


/**
 * @author shruthi.jhamb
 *
 */
public class DefaultCartPageFacade implements CartPageFacade
{


	@Resource(name = "cartPageService")
	private CartPageService cartPageService;

	@Override
	public CartData populateData(CartData cartData, final String cartClubcardPoints)
	{
		cartData = cartPageService.populateData(cartData, cartClubcardPoints);
		return cartData;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.clicks.facade.cartPage.CartPageFacade#reCalculatePromotions(de.hybris.platform.core.model.order.CartModel
	 * )
	 */
	//recalculating cart along with promotions
	@Override
	public CartModel calculateCart(CartModel cartmodel)
	{
		cartmodel = cartPageService.calculateCart(cartmodel);
		return cartmodel;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * Calculation of club card points, cc points will be 20 percent of subtotal
	 * 
	 * @see de.hybris.clicks.facade.cartPage.CartPageFacade#calculateClubCardPoints(java.lang.String)
	 */
	@Override
	public String calculateClubCardPoints(final String price, String cartClubcardPoints)
	{
		double totalPriceVal = 0.00;
		final String formattedPrice = price.substring(1);
		totalPriceVal = Double.parseDouble(formattedPrice.replaceAll(",", ""));
		final int clubcardPoints = (int) ((20 * totalPriceVal) / 100);
		cartClubcardPoints = String.valueOf(clubcardPoints);
		return cartClubcardPoints;
	}
}
