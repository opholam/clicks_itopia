/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.PhoneListModel;
import de.hybris.clicks.core.model.StoreFeatureAttributeModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.converters.populator.ImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.storelocator.converters.populator.PointOfServicePopulator;
import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.storelocator.data.PhoneNumberListData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storelocator.data.StoreFeatureAttributeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.model.storelocator.StoreLocatorFeatureModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author abhaydh
 *
 */
public class StoreDataOpeningSchedulePopulator extends PointOfServicePopulator
{

	@Resource(name = "imagePopulator")
	private ImagePopulator imagePopulator;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Override
	public void populate(final PointOfServiceModel source, final PointOfServiceData target)
	{
		super.populate(source, target);

		AddressModel addressModel = null;
		AddressData addressData = null;

		if (null != source.getAddress())
		{
			addressModel = source.getAddress();
		}
		if (null != target.getAddress())
		{
			addressData = target.getAddress();
		}
		if (null != addressData && null != addressModel && null != addressModel.getDepartment())
		{
			addressData.setShopNumber(addressModel.getDepartment());
		}
		if (null != addressData && null != addressModel && null != addressModel.getAppartment())
		{
			addressData.setCenterName(addressModel.getAppartment());
		}
		if (null != addressData)
		{
			target.setAddress(addressData);
		}

		if (null != source.getOpenDate())
		{
			target.setOpenDtae(source.getOpenDate());
		}

		final List<OpeningScheduleData> openingScheduleDataList = new ArrayList<OpeningScheduleData>();
		if (null != source.getOpeningScheduleList() && source.getOpeningScheduleList().size() > 0)
		{
			for (final OpeningScheduleModel openingScheduleModel : source.getOpeningScheduleList())
			{
				OpeningScheduleData openingScheduleData = new OpeningScheduleData();
				openingScheduleData = getOpeningScheduleConverter().convert(openingScheduleModel);
				openingScheduleDataList.add(openingScheduleData);
			}
		}
		if (openingScheduleDataList.size() > 0)
		{
			target.setOpeningHoursList(openingScheduleDataList);
		}

		final List<PhoneNumberListData> phoneNumberList = new ArrayList<PhoneNumberListData>();

		if (null != source.getPhoneLists() && source.getPhoneLists().size() > 0)
		{
			for (final Iterator<PhoneListModel> iterator = source.getPhoneLists().iterator(); iterator.hasNext();)
			{
				final PhoneListModel phoneNumberListModel = iterator.next();
				final PhoneNumberListData phoneNumberListData = new PhoneNumberListData();

				phoneNumberListData.setCode(phoneNumberListModel.getCode());
				phoneNumberListData.setName(phoneNumberListModel.getName());
				final List<String> numbersList = new ArrayList<String>();
				if (null != phoneNumberListModel.getPhoneList() && phoneNumberListModel.getPhoneList().size() > 0)
				{
					for (final String phoneNumber : phoneNumberListModel.getPhoneList())
					{
						numbersList.add(phoneNumber);
					}
				}
				phoneNumberListData.setNumber(numbersList);
				phoneNumberList.add(phoneNumberListData);
			}
		}

		target.setPhoneNumberLists(phoneNumberList);

		final List<StoreFeatureAttributeData> FeatureAttributesList = new ArrayList<StoreFeatureAttributeData>();

		if (null != source.getFeatures() && source.getFeatures().size() > 0)
		{
			final Iterator<StoreLocatorFeatureModel> iterator = source.getFeatures().iterator();

			while (iterator.hasNext())
			{
				final StoreLocatorFeatureModel storeFeature = iterator.next();
				final StoreFeatureAttributeData storeFeatureData = new StoreFeatureAttributeData();
				storeFeatureData.setFeature(storeFeature.getName());
				if (StringUtils.equalsIgnoreCase(storeFeature.getName(), "Clinic"))
				{
					storeFeatureData.setClinic(Boolean.TRUE.booleanValue());
				}
				else if (StringUtils.equalsIgnoreCase(storeFeature.getName(), "Pharmacy"))
				{
					storeFeatureData.setPharmacy(Boolean.TRUE.booleanValue());
				}
				else if (StringUtils.equalsIgnoreCase(storeFeature.getName(), "Store"))
				{
					storeFeatureData.setStore(Boolean.TRUE.booleanValue());
				}
				final ImageData imageData = new ImageData();
				if (storeFeature.getIcon() != null)
				{
					imagePopulator.populate(storeFeature.getIcon(), imageData);
					storeFeatureData.setIcon(imageData);
				}
				else
				{
					try
					{
						final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion("clicksContentCatalog",
								"Staged");

						final MediaModel mediaModel = getMediaService().getMedia(catalogVersionModel, storeFeature.getName());
						imagePopulator.populate(mediaModel, imageData);
						storeFeatureData.setIcon(imageData);
					}
					catch (final Exception e)
					{
						//
					}
				}
				if (null != storeFeature.getAttributeList() && storeFeature.getAttributeList().size() > 0)
				{
					for (final StoreFeatureAttributeModel attribute : storeFeature.getAttributeList())
					{
						if (attribute.getId().equals("1"))
						{
							storeFeatureData.setId(attribute.getValue());
						}
						if (attribute.getId().equals("2"))
						{
							storeFeatureData.setValue(attribute.getValue());
						}
					}
				}

				FeatureAttributesList.add(storeFeatureData);
			}

		}
		if (FeatureAttributesList.size() > 0)
		{
			target.setFeatureAttrList(FeatureAttributesList);
		}

	}

}
