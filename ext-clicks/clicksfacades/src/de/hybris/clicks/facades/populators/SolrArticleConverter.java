/**
 * 
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.ArticleTagsModel;
import de.hybris.platform.commerceservices.search.facetdata.ArticleSearchPageData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.AbstractSolrConverter;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author siva.reddy
 * 
 */
public class SolrArticleConverter extends AbstractSolrConverter<ArticleSearchPageData> implements Serializable
{

	protected static final Logger LOG = Logger.getLogger(SolrArticleConverter.class);

	@Override
	public ArticleSearchPageData convert(final SolrResult solrResult, final ArticleSearchPageData target)
			throws ConversionException
	{
		try
		{
			target.setArticleTitle((String) getValue(solrResult, "articleTitle"));
			target.setTitleName((String) getValue(solrResult, "titleName"));
			target.setTitleContent((String) getValue(solrResult, "titleContent"));
			target.setArticleType((String) getValue(solrResult, "articleType"));
			target.setArticleTags((List<ArticleTagsModel>) getValue(solrResult, "articleTags"));
			target.setArticleImage((String) getValue(solrResult, "articleImage"));
		}
		catch (final Exception e)
		{
			LOG.error("error" + e);
		}
		return target;
	}

	@Override
	protected ArticleSearchPageData createDataObject()
	{
		return new ArticleSearchPageData();
	}

}
