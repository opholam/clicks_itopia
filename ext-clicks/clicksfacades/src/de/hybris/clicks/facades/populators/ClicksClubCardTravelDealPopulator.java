/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.TravelDealModel;
import de.hybris.clicks.facades.data.ClicksTravelDealData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author mBhargava
 *
 */
public class ClicksClubCardTravelDealPopulator implements Populator<TravelDealModel, ClicksTravelDealData>
{

	private Converter<MediaModel, ImageData> imageConverter;

	protected Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	@Required
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final TravelDealModel source, final ClicksTravelDealData target) throws ConversionException
	{
		if (StringUtils.isNotBlank(source.getCode()))
		{
			target.setCode(source.getCode());
		}
		if (StringUtils.isNotBlank(source.getName()))
		{
			target.setName(source.getName());
		}
		if (StringUtils.isNotBlank(source.getDescription()))
		{
			target.setDescription(source.getDescription());
		}
		if (StringUtils.isNotBlank(source.getSummary()))
		{
			target.setSummary(source.getSummary());
		}
		if (null != source.getStartDate())
		{
			target.setStartDate(source.getStartDate());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate());
		}
		if (StringUtils.isNotBlank(source.getReferenceNo()))
		{
			target.setReferenceNo(source.getReferenceNo());
		}
		if (null != source.getPicture())
		{
			target.setPicture(getImageConverter().convert(source.getPicture()));
		}
		if (StringUtils.isNotBlank(source.getTravelLocation()))
		{
			target.setTravelLocation(source.getTravelLocation());
		}
		if (StringUtils.isNotBlank(source.getFromPrice()))
		{
			target.setFromPrice(source.getFromPrice());
		}
		if (StringUtils.isNotBlank(source.getTravelTerms()))
		{
			target.setTravelTerms(source.getTravelTerms());
		}
	}

}
