/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.FormatFactory;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author mbhargava
 *
 */
public class ClicksOrderPopulator extends OrderPopulator
{
	@Resource(name = "formatFactory")
	protected FormatFactory formatFactory;

	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		// YTODO Auto-generated method stub
		super.populate(source, target);
		//		if (null != source.getTotalDiscounts())
		//		{
		//			final PriceData priceData = new PriceData();
		//			priceData.setFormattedValue(formatFactory.createCurrencyFormat().format(source.getTotalDiscounts()));
		//			priceData.setValue(BigDecimal.valueOf(source.getTotalDiscounts()));
		//			target.setTotalDiscounts(priceData);
		//		}

		if (StringUtils.isNotBlank(source.getClicksOrderCode()))
		{
			target.setClicksOrderCode(source.getClicksOrderCode());
		}
		if (null != source.getPaymentAddress())
		{
			final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
			ccPaymentInfoData.setBillingAddress(getAddressConverter().convert(source.getPaymentAddress()));
			target.setPaymentInfo(ccPaymentInfoData);
		}
		if (null != source.getSubtotal())
		{
			target.setClubcardPoints(String.valueOf(target.getSubTotal().getValue().intValue() / 5));
		}
		//target.getSubTotal().getValue().intValue() / 5;
	}
	//	@Override
	//	public void populate(final OrderModel source, final OrderData target) throws ConversionException
	//	{
	//		if (null != source.getPaymentAddress())
	//		{
	//			target.setBillingAddress(getAddressConverter().convert(source.getPaymentAddress()));
	//		}
	//	}

}
