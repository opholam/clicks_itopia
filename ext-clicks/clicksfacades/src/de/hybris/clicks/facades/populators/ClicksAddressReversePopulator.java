/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author mbhargava
 *
 */
public class ClicksAddressReversePopulator extends AddressReversePopulator
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator#populate(de.hybris.platform
	 * .commercefacades.user.data.AddressData, de.hybris.platform.core.model.user.AddressModel)
	 */
	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException
	{
		// YTODO Auto-generated method stub
		super.populate(addressData, addressModel);
		if (null != addressData.getSuburb())
		{
			addressModel.setSuburb(addressData.getSuburb());
		}
		else if (null != addressData.getTown())
		{
			//addressModel.setTown(addressData.getTown());
			addressModel.setSuburb(addressData.getTown());
		}
		if (null != addressData.getTown())
		{
			addressModel.setTown(addressData.getTown());
		}
		addressModel.setEmail(addressData.getEmail());
		addressModel.setStreetname(addressData.getLine1());
		addressModel.setStreetnumber(addressData.getLine2());
		addressModel.setProvince(addressData.getProvince());

		addressModel.setPhone2(addressData.getAlternatePhone());
		//addressModel.setDeliveryInstructions(addressData.getDeliveryInstructions());
		// 0- email , 1- sms
		if (null != addressData.getPreferredContactMethod() && addressData.getPreferredContactMethod().booleanValue())
		{
			addressModel.setPrefferredContactMethod("SMS");
		}
		else if (null != addressData.getPreferredContactMethod() && !addressData.getPreferredContactMethod().booleanValue())
		{
			addressModel.setPrefferredContactMethod("email");
		}
		if (null != addressData.getStoreId())
		{
			addressModel.setCompany(addressData.getStoreId());
		}

		if (null != addressData.getApartment())
		{
			addressModel.setAppartment(addressData.getApartment());
		}
		if (null != addressData.getDepartment())
		{
			addressModel.setDepartment(addressData.getDepartment());
		}
		//if (null != addressData.getStoreId())
		//{
		//addressModel.setFax(addressData.getStoreId());
		//}
		if (null != addressData.getIsStoreAddress())
		{
			addressModel.setIsStore(addressData.getIsStoreAddress());
		}
	}
}
