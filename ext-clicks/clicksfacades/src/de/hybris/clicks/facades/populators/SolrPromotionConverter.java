package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.AbstractSolrConverter;
import de.hybris.platform.solrfacetsearch.search.impl.SolrResult;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class SolrPromotionConverter extends AbstractSolrConverter<PromotionData> implements Serializable
{

	@Override
	public PromotionData convert(final SolrResult solrResult, final PromotionData target) throws ConversionException
	{
		try
		{
			target.setCode((String) getValue(solrResult, "code"));
			target.setTitle((String) getValue(solrResult, "title"));
			target.setDescription((String) getValue(solrResult, "description"));
			target.setCampaign((String) getValue(solrResult, "campaign"));
			target.setOffer((String) getValue(solrResult, "offer"));
			target.setDisclaimer((String) getValue(solrResult, "disclaimer"));
			target.setStickers((List<String>) getValue(solrResult, "stickers"));
			target.setStartDate((Date) getValue(solrResult, "startDate"));
			target.setEndDate((Date) getValue(solrResult, "endDate"));
			target.setEnabled((Boolean) getValue(solrResult, "enabled"));
			final Object val = getValue(solrResult, "promoCategoryType");
			if (null != val && val instanceof Integer)
			{
				target.setTypeId((Integer) val);
			}
			final ImageData productBanner = new ImageData();
			productBanner.setUrl(((String) getValue(solrResult, "imageUrl")));
			target.setProductBanner(productBanner);
			target.setStickerMediaUrls((List<String>) getValue(solrResult, "stickerMediaUrls"));
			target.setShopNowUrl((String) getValue(solrResult, "shopNowUrl"));
		}
		catch (final Exception e)
		{
			//
		}
		//target.setTitle((String) getValue(solrResult, "title"));
		//	target.setDescription((String) getValue(solrResult, "description"));
		return target;
	}

	@Override
	protected PromotionData createDataObject()
	{
		return new PromotionData();
	}
}