/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.i18n.FormatFactory;

import java.math.BigDecimal;

import javax.annotation.Resource;


/**
 * @author mbhargava
 *
 */
public class ClicksConsignmentEntryPopulator extends ConsignmentEntryPopulator
{
	@Resource(name = "formatFactory")
	protected FormatFactory formatFactory;

	@Override
	public void populate(final ConsignmentEntryModel source, final ConsignmentEntryData target)
	{
		// YTODO Auto-generated method stub
		super.populate(source, target);
		BigDecimal calculatedPrice = BigDecimal.ZERO;
		if (null != source.getOrderEntry().getQuantity() && null != source.getQuantity())
		{
			calculatedPrice = BigDecimal.valueOf((source.getOrderEntry().getTotalPrice() / source.getOrderEntry().getQuantity()
					.doubleValue())
					* source.getQuantity().doubleValue());
		}
		else
		{
			calculatedPrice = BigDecimal.valueOf(source.getOrderEntry().getTotalPrice());
		}
		final PriceData priceData = new PriceData();
		priceData.setFormattedValue(formatFactory.createCurrencyFormat().format(calculatedPrice));
		priceData.setValue(calculatedPrice);
		target.setPriceForQuantity(priceData);
	}
}
