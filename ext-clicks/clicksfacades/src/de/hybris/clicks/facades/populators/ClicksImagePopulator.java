/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;

import org.apache.commons.lang.StringUtils;


/**
 * @author mbhargava
 *
 */
public class ClicksImagePopulator extends ImagePopulator
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commercefacades.product.converters.populator.ImagePopulator#populate(de.hybris.platform.core
	 * .model.media.MediaModel, de.hybris.platform.commercefacades.product.data.ImageData)
	 */
	@Override
	public void populate(final MediaModel source, final ImageData target)
	{
		super.populate(source, target);
		if (StringUtils.isNotBlank(source.getDownloadURL()))
		{
			target.setDownloadURL(source.getDownloadURL());
		}
	}
}
