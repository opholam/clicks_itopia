/**
 * 
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;


/**
 * @author jayant.premchand
 * 
 */
public class ClicksWeekdayOpeningDayPopulator extends ClicksOpeningDayPopulator
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final WeekdayOpeningDayModel source, final WeekdayOpeningDayData target) throws ConversionException
	{
		populateBase(source, target);
		target.setWeekDay(getWeekDaySymbols().get(source.getDayOfWeek().ordinal()));

	}

}
