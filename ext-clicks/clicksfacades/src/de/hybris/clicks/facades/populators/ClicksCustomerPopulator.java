/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.AccountsCBRModel;
import de.hybris.clicks.core.model.AccountsPSModel;
import de.hybris.clicks.core.model.BabyClubModel;
import de.hybris.clicks.core.model.BenefitModel;
import de.hybris.clicks.core.model.CBRtransactionModel;
import de.hybris.clicks.core.model.ChildModel;
import de.hybris.clicks.core.model.ConsentModel;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.PS_PartnerPointModel;
import de.hybris.clicks.core.model.PS_PointsBucketModel;
import de.hybris.clicks.core.model.PStransactionModel;
import de.hybris.clicks.core.model.Type_consentModel;
import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.BabyClubData;
import de.hybris.platform.commercefacades.user.data.BenefitData;
import de.hybris.platform.commercefacades.user.data.BucketData;
import de.hybris.platform.commercefacades.user.data.CBRAccountData;
import de.hybris.platform.commercefacades.user.data.CBRtransactionData;
import de.hybris.platform.commercefacades.user.data.ChildData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.MediaData;
import de.hybris.platform.commercefacades.user.data.PS_AccountData;
import de.hybris.platform.commercefacades.user.data.PS_transactionData;
import de.hybris.platform.commercefacades.user.data.PS_transactionsData;
import de.hybris.platform.commercefacades.user.data.PartnerData;
import de.hybris.platform.commercefacades.user.data.PartnerPointsData;
import de.hybris.platform.commercefacades.user.data.PointStatementData;
import de.hybris.platform.commercefacades.user.data.PointsBucketsData;
import de.hybris.platform.commercefacades.user.data.SegmentData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author swapnil.desai
 *
 */
public class ClicksCustomerPopulator extends CustomerPopulator
{

	private static final Logger LOG = Logger.getLogger(ClicksCustomerPopulator.class);

	private static final String UNKNOWN_IDENTIFIER = "Unknown";

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}



	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		LOG.debug("Calling ClicksCustomerPopulator");
		super.populate(source, target);
		try
		{
			if (CollectionUtils.isNotEmpty(source.getContactDetails()))
			{
				final List<ContactDetail> cusContactDetailsList = new ArrayList<ContactDetail>();
				for (final ContactDetailsModel contactModel : source.getContactDetails())
				{
					final ContactDetail contactDetail = new ContactDetail();
					contactDetail.setNumber(contactModel.getNumber());
					contactDetail.setTypeID(contactModel.getTypeID());
					cusContactDetailsList.add(contactDetail);
				}
				target.setContactDetails(cusContactDetailsList);
			}
			if (StringUtils.isNotBlank(source.getMemberID()))
			{
				target.setMemberID(source.getMemberID());
			}

			// CustomerId
			if (StringUtils.isNotBlank(source.getCustomerID()))
			{
				target.setCustomerID(source.getCustomerID());
			}
			// Populating PointStatements
			LOG.debug("PointsStatement population starts");
			if (null != source.getPointsStatement())
			{
				final PointStatementData pointStatementData = new PointStatementData();
				pointStatementData.setStartDate(source.getPointsStatement().getStartDate());
				pointStatementData.setEndDate(source.getPointsStatement().getEndDate());

				if (null != source.getPointsStatement().getAccountsPSList())
				{
					final List<AccountsPSModel> accountsModel = source.getPointsStatement().getAccountsPSList();
					final List<PS_AccountData> accountsData = new ArrayList<PS_AccountData>(2);
					LOG.debug("PointsStatement Account population starts");
					for (final AccountsPSModel accountsPSModel : accountsModel)
					{
						final PS_AccountData ps_AccountData = new PS_AccountData();
						ps_AccountData.setAccountID(accountsPSModel.getAccountID());
						ps_AccountData.setPointsBalance(accountsPSModel.getPointsBalance());
						ps_AccountData.setPointsToQualify(accountsPSModel.getPointsToQualify());
						ps_AccountData.setTotalSpent(accountsPSModel.getTotalSpent());

						LOG.debug("PointsStatement Points Bucket population starts");
						if (null != accountsPSModel.getPointsBuckets())
						{
							final PointsBucketsData pointsBucketsData = new PointsBucketsData();
							pointsBucketsData.setBalance(accountsPSModel.getPointsBuckets().getBalance());

							final List<BucketData> bucketDataList = new ArrayList<BucketData>(2);
							for (final PS_PointsBucketModel bucketModel : accountsPSModel.getPointsBuckets().getPS_PointsBucketList())
							{
								final BucketData bucketData = new BucketData();
								bucketData.setName(bucketModel.getName());
								bucketData.setValue(bucketModel.getValue());
								bucketDataList.add(bucketData);
							}
							pointsBucketsData.setBuckets(bucketDataList);

							ps_AccountData.setClicksPoints(pointsBucketsData);
						}

						LOG.debug("PointsStatement Partner Points population starts");
						if (null != accountsPSModel.getPartnerPoints())
						{
							final PartnerPointsData partnerPointsData = new PartnerPointsData();
							partnerPointsData.setBalance(accountsPSModel.getPartnerPoints().getBalance());

							final List<PartnerData> partnerDataList = new ArrayList<PartnerData>(2);

							for (final PS_PartnerPointModel partnerModel : accountsPSModel.getPartnerPoints().getPS_PartnerPointList())
							{
								final PartnerData partnerData = new PartnerData();
								partnerData.setName(partnerModel.getName());
								partnerData.setValue(partnerModel.getValue());

								partnerDataList.add(partnerData);
							}

							partnerPointsData.setPartners(partnerDataList);
							ps_AccountData.setPartnerPoints(partnerPointsData);

						}

						LOG.debug("PointsStatement Transaction population starts");
						if (null != accountsPSModel.getPStransactions())
						{
							final List<PS_transactionData> ps_transactionDataList = new ArrayList<PS_transactionData>(2);

							final PS_transactionsData ps_transactionsData = new PS_transactionsData();
							ps_transactionsData.setPointsBalance(accountsPSModel.getPStransactions().getBalance());


							for (final PStransactionModel pstransactionModel : accountsPSModel.getPStransactions()
									.getPStransactionList())
							{
								final PS_transactionData ps_transactionData = new PS_transactionData();
								ps_transactionData.setDate(pstransactionModel.getDate());
								ps_transactionData.setEarned(pstransactionModel.getEarned());
								ps_transactionData.setLocation(UNKNOWN_IDENTIFIER);
								PointOfServiceModel posModel = new PointOfServiceModel();
								posModel.setName(pstransactionModel.getLocation());
								try
								{
									posModel = flexibleSearchService.getModelByExample(posModel);
									if (null != posModel)
									{
										ps_transactionData.setLocation(posModel.getDisplayName());
									}
								}
								catch (final Exception e)
								{
									LOG.debug("Error in getting Point of service Model for id : " + posModel.getName());
								}

								ps_transactionData.setSpent(pstransactionModel.getSpent());
								ps_transactionDataList.add(ps_transactionData);
							}
							ps_transactionsData.setTransactions(ps_transactionDataList);
							ps_AccountData.setTransactions(ps_transactionsData);
						}

						accountsData.add(ps_AccountData);
					}
					pointStatementData.setAccounts(accountsData);
				}

				target.setPointStatement(pointStatementData);
			}

			// Population CBR Statements
			if (null != source.getCBRstatement() && CollectionUtils.isNotEmpty(source.getCBRstatement().getAccountsCBRList()))
			{
				final List<CBRAccountData> cbrstatement = new ArrayList<CBRAccountData>(2);
				for (final AccountsCBRModel accountsCBRModel : source.getCBRstatement().getAccountsCBRList())
				{
					final CBRAccountData cbrAccountData = new CBRAccountData();
					if (null != accountsCBRModel.getAccountID())
					{
						cbrAccountData.setAccountID(accountsCBRModel.getAccountID());
					}
					if (null != accountsCBRModel.getCBRbalance())
					{
						cbrAccountData.setAvailableBalance(accountsCBRModel.getCBRbalance());
					}
					final List<CBRtransactionData> cbrTransactionDataList = new ArrayList<CBRtransactionData>(2);
					if (CollectionUtils.isNotEmpty(accountsCBRModel.getCBRtransactionList()))
					{
						for (final CBRtransactionModel cbrTransactionModel : accountsCBRModel.getCBRtransactionList())
						{
							final CBRtransactionData cbrtransactionData = new CBRtransactionData();
							if (null != cbrTransactionModel.getAmountIssued())
							{
								cbrtransactionData.setAmountIssued(cbrTransactionModel.getAmountIssued());
							}
							if (null != cbrTransactionModel.getAvailableAmount())
							{
								cbrtransactionData.setAvailableAmount(cbrTransactionModel.getAvailableAmount());
							}
							if (null != cbrTransactionModel.getExpiryDate())
							{
								cbrtransactionData.setExpiryDate(cbrTransactionModel.getExpiryDate());
							}
							if (null != cbrTransactionModel.getIssueDate())
							{
								cbrtransactionData.setIssueDate(cbrTransactionModel.getIssueDate());
							}

							cbrTransactionDataList.add(cbrtransactionData);
						}
						cbrAccountData.setCBRtransactions(cbrTransactionDataList);
					}
					cbrstatement.add(cbrAccountData);
				}
				target.setCBRstatement(cbrstatement);
			}

			// Populating Financial Sevices
			if (CollectionUtils.isNotEmpty(source.getFinancialServices()))
			{
				final List<BenefitData> financialServices = new ArrayList<BenefitData>(2);
				for (final BenefitModel benefitModel : source.getFinancialServices())
				{
					final BenefitData benefitData = new BenefitData();
					benefitData.setBenefitAmount(benefitModel.getBenefitAmount());
					benefitData.setBenefitDate(benefitModel.getBenefitDate());
					benefitData.setBenefitId(benefitModel.getBenefitId());
					benefitData.setBenefitStatus(benefitModel.getStatus());
					financialServices.add(benefitData);
				}

				target.setFinancialServices(financialServices);
			}

			if (CollectionUtils.isNotEmpty(source.getAllGroups()))
			{
				final List<SegmentData> segments = new ArrayList<SegmentData>(2);
				for (final PrincipalGroupModel principalGroup : source.getAllGroups())
				{
					if (principalGroup instanceof UserGroupModel)
					{
						final UserGroupModel userGroup = (UserGroupModel) principalGroup;

						final SegmentData segmentData = new SegmentData();
						segmentData.setSegmentID(userGroup.getSegmentID());
						segmentData.setSegmentDescription(userGroup.getSegmentDescription());
						segmentData.setSegmentStatus(userGroup.getSegmentStatus());
						segmentData.setSegmentType(userGroup.getSegmentType());
						segments.add(segmentData);
					}

				}

				target.setLoyalty_segments(segments);
			}

			if (null != source.getProfilePicture())
			{
				final MediaData mediaData = new MediaData();
				mediaData.setUrl(source.getProfilePicture().getURL());
				target.setProfilePicture(mediaData);
			}
			/* Code changes for edit personal details starts */
			if (CollectionUtils.isNotEmpty(source.getAddresses()))
			{
				final List<AddressData> address = new ArrayList<AddressData>();
				for (final AddressModel addressmodel : source.getAddresses())
				{
					addressmodel.setOwner(source);
					final AddressData addressData = new AddressData();
					if (null != addressmodel.getBuilding())
					{
						addressData.setLine1(addressmodel.getBuilding());
					}
					if (null != addressmodel.getStreetname())
					{
						addressData.setLine2(addressmodel.getStreetname());
					}
					if (null != addressmodel.getStreetnumber())
					{
						addressData.setSuburb(addressmodel.getStreetnumber());
					}
					if (null != addressmodel.getTown())
					{
						final String[] townSplit = getCustomerNameStrategy().splitName(addressmodel.getTown());
						if (townSplit != null)
						{
							addressData.setCity(townSplit[0]);
							addressData.setProvince(townSplit[1]);
						}
						else
						{
							addressData.setCity(addressmodel.getTown());
						}
					}
					if (null != addressmodel.getPostalcode())
					{
						addressData.setPostalCode(addressmodel.getPostalcode());
					}
					final CountryData country = new CountryData();
					if (null != addressmodel.getCountry() && null != addressmodel.getCountry().getIsocode())
					{
						country.setIsocode(addressmodel.getCountry().getIsocode());
						addressData.setCountry(country);
					}
					address.add(addressData);
				}
				target.setAddresses(address);
			}
			if (null != source.getName())
			{
				target.setPreferedName(source.getName());
			}
			if (null != source.getFirstName())
			{
				target.setFirstName(source.getFirstName());
			}
			if (null != source.getLastName())
			{
				target.setLastName(source.getLastName());
			}
			if (null != source.getGender() && null != source.getGender().getCode())
			{
				target.setGender(source.getGender().getCode());
			}
			if (CollectionUtils.isNotEmpty(source.getContactDetails()))
			{
				final List<ContactDetail> contactDataList = new ArrayList<ContactDetail>();
				for (final ContactDetailsModel contactDetailsModel : source.getContactDetails())
				{
					final ContactDetail contactData = new ContactDetail();
					contactData.setTypeID(contactDetailsModel.getTypeID());
					contactData.setNumber(contactDetailsModel.getNumber());
					contactDataList.add(contactData);
				}
				target.setContactDetails(contactDataList);
			}
			if (null != source.getEmailID())
			{
				target.setEmailID(source.getEmailID());
			}
			if (null != source.getSAResident())
			{
				target.setSAResident(source.getSAResident());
				if (source.getSAResident().booleanValue())
				{
					target.setIDnumber(source.getRSA_ID());
				}
				if (null != source.getNonRSA_DOB())
				{
					target.setNonRSA_DOB(source.getNonRSA_DOB());
				}
			}
			if (CollectionUtils.isNotEmpty(source.getMarketingConsent()))
			{
				populateConsent(source, target);
			}

			if (CollectionUtils.isNotEmpty(source.getBabyClub()))
			{
				getBabyClubDetails(source, target);
			}
			/* Code changes for edit personal details ends */
		}
		catch (final Exception e)
		{
			LOG.error("Error in populating customer data from model: " + e.getMessage());
		}

		LOG.debug("ClicksCustomerPopulator exits.");

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateConsent(final CustomerModel source, final CustomerData target)
	{
		try
		{
			for (final ConsentModel consentModel : source.getMarketingConsent())
			{
				if ("ACCINFO".equalsIgnoreCase(consentModel.getConsentType()))
				{
					for (final Type_consentModel type : consentModel.getComms())
					{
						if (Integer.valueOf(1).equals(type.getTypeID()))
						{
							target.setAccinfo_SMS(type.getAccepted());
						}
						else
						{
							target.setAccinfo_email(type.getAccepted());
						}
					}

				}
				else if ("MARKETING".equalsIgnoreCase(consentModel.getConsentType()))
				{
					for (final Type_consentModel type : consentModel.getComms())
					{
						if (Integer.valueOf(1).equals(type.getTypeID()))
						{
							target.setMarketingConsent_SMS(type.getAccepted());
						}
						else
						{
							target.setMarketingConsent_email(type.getAccepted());
						}
					}

				}
			}
		}
		catch (final Exception e)
		{
			//
		}
	}

	public void getBabyClubDetails(final CustomerModel source, final CustomerData target)
	{
		final Collection<BabyClubModel> babyClubList = source.getBabyClub();
		if (!CollectionUtils.isEmpty(babyClubList))
		{
			final BabyClubData babyClubData = new BabyClubData();
			for (final BabyClubModel babyClubModel : babyClubList)
			{
				if (StringUtils.isNotBlank(babyClubModel.getBabyClubDueDate()))
				{
					babyClubData.setBabyClubDueDate(babyClubModel.getBabyClubDueDate());
				}
				if (null != babyClubModel.getGender())
				{
					babyClubData.setGender(babyClubModel.getGender().name());
				}
				if (null != babyClubModel.getIsAlreadyPregnant())
				{
					babyClubData.setIsAlreadyPregnant(babyClubModel.getIsAlreadyPregnant());
				}

				if (CollectionUtils.isNotEmpty(babyClubModel.getChildren()))
				{
					final List<ChildData> childDataList = new ArrayList<ChildData>();
					babyClubModel.setIsAlreadyParent(new Boolean(true));
					babyClubData.setIsAlreadyParent(new Boolean(true));
					for (final ChildModel childModel : babyClubModel.getChildren())
					{
						final ChildData childData = new ChildData();
						if (StringUtils.isNotBlank(childModel.getChildDOB()))
						{
							childData.setChildDOB(childModel.getChildDOB());
						}
						if (StringUtils.isNotBlank(childModel.getFirstName()))
						{
							childData.setFirstName(childModel.getFirstName());
						}
						if (StringUtils.isNotBlank(childModel.getGender().name()))
						{
							childData.setGender(childModel.getGender().name());
						}
						if (StringUtils.isNotBlank(childModel.getLastName()))
						{
							childData.setLastName(childModel.getLastName());
						}
						childDataList.add(childData);
					}
					if (childDataList.size() > 0)
					{
						babyClubData.setChildren(childDataList);
					}
				}

			}
			target.setBabyClub(babyClubData);
		}
	}
}
