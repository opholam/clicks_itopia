/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author mbhargava
 *
 */
public class ClicksCartPopulator extends CartPopulator<CartData> implements Populator<CartModel, CartData>
{
	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final CartModel source, final CartData target) throws ConversionException
	{
		super.populate(source, target);
		target.setDeliveryInstruction(source.getDeliveryInstructions());
		target.setIsPaymentProcessing(null != source.getBasketEditable() ? !source.getBasketEditable().booleanValue() : false);
		try
		{
			final List<VoucherData> appliedVouchers = voucherFacade.getVouchersForCart();
			if (CollectionUtils.isNotEmpty(appliedVouchers))
			{
				for (final VoucherData voucherData : appliedVouchers)
				{
					System.out.println("applied vouchers " + voucherData.getVoucherCode() + "  -- " + voucherData.getCode());
				}
				target.setAppliedVouchers(appliedVouchers);
			}
		}
		catch (final Exception e)
		{
			//
		}
	}

	/**
	 * @return the voucherFacade
	 */
	public VoucherFacade getVoucherFacade()
	{
		return voucherFacade;
	}

	/**
	 * @param voucherFacade
	 *           the voucherFacade to set
	 */
	public void setVoucherFacade(final VoucherFacade voucherFacade)
	{
		this.voucherFacade = voucherFacade;
	}

}
