/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.ClubCardPartnerModel;
import de.hybris.clicks.facades.data.ClubCardPartnerData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;


/**
 * @author admin
 *
 */
public class ClicksClubCardPartnerPopulator implements Populator<ClubCardPartnerModel, ClubCardPartnerData>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final ClubCardPartnerModel source, final ClubCardPartnerData target) throws ConversionException
	{
		if (StringUtils.isNotBlank(source.getCode()))
		{
			target.setCode(source.getCode());
		}
		if (StringUtils.isNotBlank(source.getDescription()))
		{
			target.setDescription(source.getDescription());
		}
		if (StringUtils.isNotBlank(source.getPromotionalText()))
		{
			target.setPromotionalText(source.getPromotionalText());
		}
		if (StringUtils.isNotBlank(source.getTitle()))
		{
			target.setTitle(source.getTitle());
		}
		if (StringUtils.isNotBlank(source.getSummary()))
		{
			target.setSummary(source.getSummary());
		}
		if (null != source.getPartnerLogo())
		{
			final ImageData imgData = new ImageData();
			imgData.setUrl(source.getPartnerLogo().getURL());
			target.setPartnerLogo(imgData);
		}
	}


}
