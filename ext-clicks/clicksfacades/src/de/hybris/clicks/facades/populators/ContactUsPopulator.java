/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.ContactUsMessageModel;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author siva.reddy
 *
 */
public class ContactUsPopulator implements Populator<ContactUsMessageModel, ContactUsMessageData>
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource
	FlexibleSearchService flexibleSearchService;

	@Override
	public void populate(final ContactUsMessageModel source, final ContactUsMessageData target) throws ConversionException
	{
		if (null != source.getFirstName())
		{
			target.setFirstName(source.getFirstName());
		}
		if (null != source.getLastName())
		{
			target.setLastName(source.getLastName());
		}
		if (null != source.getEmailAddress())
		{
			target.setEmailAddress(source.getEmailAddress());
		}
		if (null != source.getContactNumber())
		{
			target.setContactNumber(source.getContactNumber());
		}
		if (null != source.getClubCardNumber())
		{
			target.setClubCardNumber(source.getClubCardNumber());
		}
		if (null != source.getMessage())
		{
			target.setMessage(source.getMessage());
		}
		if (null != source.getTopic())
		{
			target.setTopics(source.getTopic());
		}
		target.setCustomercare(source.isCustomerCare());

		if (StringUtils.containsIgnoreCase(source.getTopic(), "pharmacy")
				|| StringUtils.containsIgnoreCase(source.getTopic(), "script"))
		{
			if (StringUtils.isNotBlank(source.getCity()))
			{
				target.setCity(source.getCity());
			}
			if (StringUtils.isNotBlank(source.getSuburb()))
			{
				target.setSuburb(source.getSuburb());
			}
			if (StringUtils.isNotBlank(source.getStreet()))
			{
				target.setStreet(source.getStreet());
			}
			if (StringUtils.isNotBlank(source.getProvince()))
			{
				target.setProvince(source.getProvince());
			}
			if (StringUtils.isNotBlank(source.getPostalCode()))
			{
				target.setPostalCode(source.getPostalCode());
			}
		}
	}
}
