/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */
public class ClicksAddressPopulator implements Populator<AddressModel, AddressData>
{
	private static final Logger LOG = Logger.getLogger(ClicksAddressPopulator.class);

	@Resource(name = "clicksCheckoutFacade")
	ClicksCheckoutFacade clicksCheckoutFacade;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final AddressModel source, final AddressData target) throws ConversionException
	{
		// YTODO Auto-generated method stub
		if (null != source.getCellphone())
		{
			target.setMobile(source.getCellphone());
		}

		if (null != source.getLine1())
		{
			target.setStreetName(source.getLine1());
		}
		if (null != source.getLine2())
		{
			target.setStreetNumber(source.getLine2());
		}
		if (null != source.getSuburb())
		{
			target.setSuburb(source.getSuburb());
		}
		if (null != source.getProvince())
		{
			target.setProvince(source.getProvince());
		}
		if (null != source.getProvince())
		{
			target.setProvince(source.getProvince());
		}

		if (null != source.getPhone2())
		{
			target.setAlternatePhone(source.getPhone2());
		}
		if (null != source.getPrefferredContactMethod())
		{
			if (source.getPrefferredContactMethod().equalsIgnoreCase("email"))
			{
				target.setPreferredContactMethod(new Boolean(false));
			}
			if (source.getPrefferredContactMethod().equalsIgnoreCase("sms"))
			{
				target.setPreferredContactMethod(new Boolean(true));
			}

		}
		if (null != source.getCompany())
		{
			target.setStoreId(source.getCompany());
			//target.setCompanyName(source.getCompany());
		}
		if (null != source.getAppartment())
		{
			target.setApartment(source.getAppartment());
			;
			//target.setCompanyName(source.getCompany());
		}
		if (null != source.getDepartment())
		{
			target.setDepartment(source.getDepartment());
		}
		if (null != source.getTown())
		{
			target.setTown(source.getTown());
			;
		}
		if (null != target.getStoreId())
		{
			final PointOfServiceModel pos = clicksCheckoutFacade.getPosData(target.getStoreId());
			if (null != pos && null != pos.getDisplayName())
			{
				target.setCompanyName(pos.getDisplayName());
			}
		}
		if (null != source.getIsStore())
		{
			target.setIsStoreAddress(source.getIsStore());
		}
	}

}
