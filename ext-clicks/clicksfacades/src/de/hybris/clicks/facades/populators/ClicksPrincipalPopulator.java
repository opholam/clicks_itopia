/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.PrincipalPopulator;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;

import java.util.Iterator;

import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */
public class ClicksPrincipalPopulator extends PrincipalPopulator
{

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commercefacades.user.converters.populator.PrincipalPopulator#populate(de.hybris.platform.core
	 * .model.security.PrincipalModel, de.hybris.platform.commercefacades.user.data.PrincipalData)
	 */

	private static final Logger LOG = Logger.getLogger(ClicksPrincipalPopulator.class);

	@Override
	public void populate(final PrincipalModel source, final PrincipalData target)
	{
		super.populate(source, target);
		if (null != source && null != source.getProfilePicture() && null != source.getProfilePicture().getURL())
		{
			target.setProfilePicUrl(source.getProfilePicture().getURL());
		}
		if (null != source && null != source.getAllGroups())
		{
			final Iterator<PrincipalGroupModel> iterator = source.getAllGroups().iterator();
			iterator.hasNext();
			final PrincipalGroupModel groupModel = iterator.next();
			if (null != groupModel.getUid() && groupModel.getUid().contains("Expert"))
			{
				target.setIsExpertReview(true);
			}
		}

	}
}
