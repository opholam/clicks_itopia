/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 *
 */
public class PaymentInfoPopulator implements Populator<PaymentInfoModel, PaymentInfoData>
{
	private static final Logger LOG = Logger.getLogger(PaymentInfoPopulator.class);
	private Converter<AddressModel, AddressData> addressConverter;

	/**
	 * @return the addressConverter
	 */
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final PaymentInfoModel source, final PaymentInfoData target) throws ConversionException
	{
		//	LOG.info("Entered into PaymentInfoPopulator");

		if (null != source)
		{
			target.setCode(source.getCode());
			final AddressModel addressModel = source.getBillingAddress();
			if (addressModel != null)
			{
				final AddressData address = getAddressConverter().convert(addressModel);
				target.setBillingAddress(address);
			}
		}
	}

}
