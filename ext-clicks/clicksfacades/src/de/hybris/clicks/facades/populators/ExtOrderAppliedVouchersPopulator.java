/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.converters.populator.OrderAppliedVouchersPopulator;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;


/**
 * Populate the {@link de.hybris.platform.commercefacades.order.data.AbstractOrderData} with the vouchers applied to
 * {@link de.hybris.platform.core.model.order.AbstractOrderModel}
 */
public class ExtOrderAppliedVouchersPopulator extends OrderAppliedVouchersPopulator
{
	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		try
		{
			final List<VoucherData> appliedVouchers = voucherFacade.getVouchersForCart();
			for (final VoucherData voucher : appliedVouchers)
			{
				getAppliedVoucherPopulatorList().populate(source, voucher);
			}
			target.setAppliedVouchers(appliedVouchers);
		}
		catch (final Exception e)
		{
			super.populate(source, target);
		}
	}

	/**
	 * @return the voucherFacade
	 */
	public VoucherFacade getVoucherFacade()
	{
		return voucherFacade;
	}

	/**
	 * @param voucherFacade
	 *           the voucherFacade to set
	 */
	public void setVoucherFacade(final VoucherFacade voucherFacade)
	{
		this.voucherFacade = voucherFacade;
	}
}
