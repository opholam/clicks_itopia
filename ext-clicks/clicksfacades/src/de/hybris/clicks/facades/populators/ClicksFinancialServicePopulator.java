/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.FinancialServiceModel;
import de.hybris.clicks.facades.data.ClicksFinancialServiceData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author mbhargava
 *
 */
public class ClicksFinancialServicePopulator implements Populator<FinancialServiceModel, ClicksFinancialServiceData>
{

	private Converter<MediaModel, ImageData> imageConverter;

	protected Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	@Required
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final FinancialServiceModel source, final ClicksFinancialServiceData target) throws ConversionException
	{
		if (StringUtils.isNotBlank(source.getCode()))
		{
			target.setCode(source.getCode());
		}
		if (StringUtils.isNotBlank(source.getTitle()))
		{
			target.setTitle(source.getTitle());
		}
		if (StringUtils.isNotBlank(source.getName()))
		{
			target.setName(source.getName());
		}
		if (StringUtils.isNotBlank(source.getActivationLink()))
		{
			target.setActivationLink(source.getActivationLink());
		}

		if (null != source.getIsLogin())
		{
			target.setIsLogin(source.getIsLogin().booleanValue());
		}

		if (StringUtils.isNotBlank(source.getSummary()))
		{
			target.setSummary(source.getSummary());
		}

		if (StringUtils.isNotBlank(source.getDescription()))
		{
			target.setDescription(source.getDescription());
		}

		if (CollectionUtils.isNotEmpty(source.getDocuments()))
		{
			target.setDocuments(Converters.convertAll(source.getDocuments(), getImageConverter()));
		}
	}


}
