/**
 * 
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.storelocator.converters.populator.OpeningDayPopulator;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 * @author jayant.premchand
 * 
 */
public abstract class ClicksOpeningDayPopulator extends OpeningDayPopulator<WeekdayOpeningDayModel, WeekdayOpeningDayData>
{

	/**
	 * Override this method to retrieve Week Days in the Full form.
	 * 
	 **/
	@Override
	protected List<String> getWeekDaySymbols()
	{
		final List<String> notEmptyWeekDay = new ArrayList<String>();
		final DateFormatSymbols weekDaySymbols = new DateFormatSymbols(getCurrentLocale());
		for (final String anyWeekDay : weekDaySymbols.getWeekdays())
		{
			if (StringUtils.isNotBlank(anyWeekDay))
			{
				notEmptyWeekDay.add(anyWeekDay);
			}
		}

		return notEmptyWeekDay;
	}
}
