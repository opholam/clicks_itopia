/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang.StringUtils;


/**
 * @author admin
 *
 */
public class ClicksOrderHistoryPopulator extends OrderHistoryPopulator
{

	private Converter<AddressModel, AddressData> addressConverter;

	/**
	 * @return the addressConverter
	 */
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator#populate(de.hybris.platform
	 * .core.model.order.OrderModel, de.hybris.platform.commercefacades.order.data.OrderHistoryData)
	 */
	@Override
	public void populate(final OrderModel source, final OrderHistoryData target)
	{
		// YTODO Auto-generated method stub
		super.populate(source, target);
		if (null != source.getDeliveryAddress())
		{
			final AddressData address = getAddressConverter().convert(source.getDeliveryAddress());
			target.setDeliveryAddress(address);
		}
		if (StringUtils.isNotBlank(source.getClicksOrderCode()))
		{
			target.setClicksOrderCode(source.getClicksOrderCode());
		}
	}
}
