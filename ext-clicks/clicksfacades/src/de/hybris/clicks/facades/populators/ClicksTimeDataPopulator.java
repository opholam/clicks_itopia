/**
 *
 */
package de.hybris.clicks.facades.populators;

import de.hybris.platform.commercefacades.storelocator.converters.populator.TimeDataPopulator;
import de.hybris.platform.commercefacades.storelocator.data.TimeData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * @author jayant.premchand
 *
 */
public class ClicksTimeDataPopulator extends TimeDataPopulator
{

	/*
	 * Overridden this method to set Hours in the 24hrs format
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void populate(final Date source, final TimeData target)
	{
		if (source != null)
		{

			/*
			 * target.setHour((byte) source.getHours()); target.setMinute((byte) source.getMinutes());
			 * target.setFormattedHour(getDateFormat().format(source));
			 */

			final Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(source);
			target.setHour((byte) calendar.get(Calendar.HOUR_OF_DAY));
			target.setMinute((byte) calendar.get(Calendar.MINUTE));
			final SimpleDateFormat timeFormate = new SimpleDateFormat("HH.mm");
			target.setFormattedHour(timeFormate.format(source));
		}
	}

}
