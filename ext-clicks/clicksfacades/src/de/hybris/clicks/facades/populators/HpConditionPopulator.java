/**
 * 
 */
package de.hybris.clicks.facades.populators;

import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.commercefacades.user.data.HealthConditionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;


/**
 * @author sridhar.reddy
 * 
 */
public class HpConditionPopulator implements Populator<ConditionsModel, HealthConditionData>
{
	@Override
	public void populate(final ConditionsModel conditionModel, final HealthConditionData healthConditionData)
			throws ConversionException
	{
		if (StringUtils.isNotBlank(conditionModel.getConditionId()))
		{
			healthConditionData.setConditionId(conditionModel.getConditionId());
		}
		if (StringUtils.isNotBlank(conditionModel.getConditionName()))
		{
			healthConditionData.setConditionName(conditionModel.getConditionName());
		}
		healthConditionData.setPrioritize(conditionModel.isIsPrioritze());
	}
}
