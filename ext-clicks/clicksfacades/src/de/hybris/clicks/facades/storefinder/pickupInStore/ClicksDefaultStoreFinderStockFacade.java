/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.facades.storefinder.pickupInStore;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.storefinder.data.StoreFinderStockSearchPageData;
import de.hybris.platform.commercefacades.storefinder.impl.DefaultStoreFinderStockFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceStockData;
import de.hybris.platform.commercefacades.storelocator.data.StoreStockHolder;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.ui.Model;


/**
 * @author shruti.jhamb
 *
 */

public class ClicksDefaultStoreFinderStockFacade extends DefaultStoreFinderStockFacade
{

	ClicksDefaultStoreFinderStockFacade()
	{
		super();
	}

	@Override
	public StoreFinderStockSearchPageData productSearch(final String location, final ProductData productData,
			final PageableData pageableData)
	{
		final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = getStoreFinderService()
				.locationSearch(getBaseStoreService().getCurrentBaseStore(), location, pageableData);
		return getResultForPointOfServiceData(storeFinderSearchPageData, productData);
	}

	public StoreFinderStockSearchPageData productsSearch(final String location, final ProductData productData,
			final PageableData pageableData, final Model model)
	{
		final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData = getStoreFinderService()
				.locationSearch(getBaseStoreService().getCurrentBaseStore(), location, pageableData);
		return getResultsForPointOfServiceData(storeFinderSearchPageData, productData, model);
	}

	protected StoreFinderStockSearchPageData<PointOfServiceStockData> getResultForPointOfServiceData(
			final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData, final ProductData productData)
	{
		final List<PointOfServiceStockData> result = new ArrayList<PointOfServiceStockData>(storeFinderSearchPageData.getResults()
				.size());
		for (final PointOfServiceDistanceData distanceData : storeFinderSearchPageData.getResults())
		{
			if (null != distanceData.getPointOfService().getType()
					&& distanceData.getPointOfService().getType().getCode().equalsIgnoreCase("STORE"))
			{
				//if (warehouse.getCode().equalsIgnoreCase("9000"))
				//	{
				final StoreStockHolder storeStockHolder = createStoreStockHolder();
				storeStockHolder.setPointOfService(distanceData.getPointOfService());
				storeStockHolder.setProduct(getProductService().getProductForCode(productData.getCode()));
				//distanceData.getPointOfService().getWarehouses()

				final PointOfServiceStockData posStockData = (PointOfServiceStockData) getStoreStockConverter().convert(
						storeStockHolder);
				posStockData.setFormattedDistance(((PointOfServiceData) getPointOfServiceDistanceDataConverter()
						.convert(distanceData)).getFormattedDistance());
				result.add(posStockData);
				//}
				//}
			}
		}
		return createSearchResult(result, storeFinderSearchPageData.getPagination(), productData);
	}

	protected StoreFinderStockSearchPageData<PointOfServiceStockData> getResultsForPointOfServiceData(
			final StoreFinderSearchPageData<PointOfServiceDistanceData> storeFinderSearchPageData, final ProductData productData,
			final Model model)
	{
		final List<PointOfServiceStockData> result = new ArrayList<PointOfServiceStockData>(storeFinderSearchPageData.getResults()
				.size());
		for (final PointOfServiceDistanceData distanceData : storeFinderSearchPageData.getResults())
		{
			if (null != distanceData.getPointOfService().getType()
					&& distanceData.getPointOfService().getType().getCode().equalsIgnoreCase("STORE"))
			{
				//not required
				for (final WarehouseModel warehouse : distanceData.getPointOfService().getWarehouses())
				{
					warehouse.getModifiedtime().toString();
					model.addAttribute("modifiedTime", warehouse.getModifiedtime().toString());
				}
				//if (warehouse.getCode().equalsIgnoreCase("9000"))
				//	{
				final StoreStockHolder storeStockHolder = createStoreStockHolder();
				storeStockHolder.setPointOfService(distanceData.getPointOfService());
				storeStockHolder.setProduct(getProductService().getProductForCode(productData.getCode()));
				//distanceData.getPointOfService().getWarehouses()

				final PointOfServiceStockData posStockData = (PointOfServiceStockData) getStoreStockConverter().convert(
						storeStockHolder);
				posStockData.setFormattedDistance(((PointOfServiceData) getPointOfServiceDistanceDataConverter()
						.convert(distanceData)).getFormattedDistance());
				result.add(posStockData);
				//}
				//}
			}
		}
		return createSearchResult(result, storeFinderSearchPageData.getPagination(), productData);
	}
}
