/**
 *
 */
package de.hybris.clicks.facades.storefinder;

import de.hybris.platform.commercefacades.storefinder.impl.DefaultStoreFinderFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.store.BaseStoreModel;


/**
 * @author karthikeyan.n
 * 
 */
public class ClicksStorefinderFacade extends DefaultStoreFinderFacade
{
	@Override
	public StoreFinderSearchPageData<PointOfServiceData> positionSearch(final GeoPoint geoPoint, final PageableData pageableData)
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		final StoreFinderSearchPageData<PointOfServiceDistanceData> searchPageData = getStoreFinderService().positionSearch(
				currentBaseStore, geoPoint, pageableData, currentBaseStore.getMaxRadiusForPoSSearch());
		return getSearchPagePointOfServiceDistanceConverter().convert(searchPageData);
	}
}
