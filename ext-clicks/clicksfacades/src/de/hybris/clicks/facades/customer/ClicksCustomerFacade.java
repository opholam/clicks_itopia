/**
 *
 */
package de.hybris.clicks.facades.customer;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.clicks.core.model.ConsentModel;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.Type_consentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * @author abhaydh
 *
 */
public class ClicksCustomerFacade extends DefaultCustomerFacade
{

	protected static final Logger LOG = Logger.getLogger(ClicksCustomerFacade.class);
	protected static final String NEW_ACCOUNT_NEW_CLUB_CARD_EMAIL_PROCESS = "newAccountNewCCEmailProcess";
	protected static final String NON_CLUB_CARD_REGISTRATION = "customerRegistrationEmailProcess";
	boolean result = false;
	@Resource
	FlexibleSearchService flexibleSearchService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "baseStoreService")
	BaseStoreService baseStoreService;

	private ModelService modelService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;


	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}


	/**
	 * @return the modelService
	 */
	@Override
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		LOG.info("---in clicksCustomerFacade------");
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));

		if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
		{
			newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		}
		if (null == newCustomer.getName())
		{
			newCustomer.setName(registerData.getFirstName());
		}
		try
		{

			final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
			newCustomer.setTitle(title);
			setUidForRegister(registerData, newCustomer);
			newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
			newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
			// added for custom attribues populate
			//defaultCustomerAccountServiceImpl.register(newCustomer, registerData.getPassword());
			getCustomerAccountService().register(newCustomer, registerData.getPassword());
			newCustomer = populateCustomerModel(registerData, newCustomer);
			newCustomer.setCustomerID(registerData.getCustomerId());
			getModelService().save(newCustomer);
		}
		catch (final IllegalStateException illegalExc)
		{
			LOG.error("illegal state exception" + illegalExc);
		}
		catch (final ModelSavingException saveexc)
		{
			LOG.error("model saving exception" + saveexc);
		}
	}

	public void joinClubCard(final CustomerModel customerModel, final String processName)
	{
		LOG.info("---Join Club Card Facade------ ");
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		getModelService().save(customerModel);

		//processEmail(customerModel, processName);


	}

	/**
	 * @param newCustomer
	 *           processName
	 */
	public void processEmail(final CustomerModel newCustomer, final String processName)
	{
		//		final String process = processName;
		try
		{
			final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel;
			storeFrontCustomerProcessModel = (StoreFrontCustomerProcessModel) businessProcessService.createProcess(processName
					+ newCustomer.getUid() + "-" + System.currentTimeMillis(), processName);
			storeFrontCustomerProcessModel.setStore(baseStoreService.getCurrentBaseStore());
			storeFrontCustomerProcessModel.setCustomer(newCustomer);
			storeFrontCustomerProcessModel.setLanguage(commonI18NService.getCurrentLanguage());
			storeFrontCustomerProcessModel.setCurrency(commonI18NService.getCurrentCurrency());
			storeFrontCustomerProcessModel.setSite(cmsSiteService.getCurrentSite());
			getModelService().save(storeFrontCustomerProcessModel);
			businessProcessService.startProcess(storeFrontCustomerProcessModel);
		}
		catch (final Exception e)
		{
			LOG.warn("Exception while starting process for sending email : ", e);
		}
	}


	private CustomerModel populateCustomerModel(final RegisterData registerData, final CustomerModel newCustomer)
	{
		if (null != registerData.getMemberID())
		{
			newCustomer.setMemberID(registerData.getMemberID());
		}
		if (null != registerData.getPreferedName())
		{
			newCustomer.setName(registerData.getPreferedName());
		}
		else
		{
			newCustomer.setName(registerData.getFirstName());
		}
		if (null != registerData.getFirstName())
		{
			newCustomer.setFirstName(registerData.getFirstName());
		}
		if (null != registerData.getLastName())
		{
			newCustomer.setLastName(registerData.getLastName());
		}
		if (null != registerData.getGender())
		{
			if (registerData.getGender().intValue() == 1)
			{
				newCustomer.setGender(Gender.MALE);
			}
			if (registerData.getGender().intValue() == 2)
			{
				newCustomer.setGender(Gender.FEMALE);
			}
		}
		if (null != registerData.getSAResident())
		{
			newCustomer.setSAResident(registerData.getSAResident());
		}
		if (null != registerData.getIDNumber())
		{
			newCustomer.setRSA_ID(registerData.getIDNumber());
		}

		if (null != registerData.getContactDetails())
		{
			final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
			ContactDetailsModel contactModel;

			for (final ContactDetail contact : registerData.getContactDetails())
			{
				contactModel = getModelService().create(ContactDetailsModel.class);
				if (null != contact.getNumber())
				{
					contactModel.setNumber(contact.getNumber());
				}
				if (null != contact.getTypeID())
				{
					contactModel.setTypeID(contact.getTypeID());
					if (contact.getTypeID().equalsIgnoreCase("2"))
					{
						contactModel.setType("Mobile");
					}
					if (contact.getTypeID().equalsIgnoreCase("4"))
					{
						contactModel.setType("Other");
					}
				}
				contactModelList.add(contactModel);
			}
			if (contactModelList.size() > 0)
			{
				newCustomer.setContactDetails(contactModelList);
			}
		}

		try
		{
			if (null != registerData.getSA_DOB())
			{
				newCustomer.setNonRSA_DOB(new Date(registerData.getSA_DOB()));
			}
		}
		catch (final Exception e)
		{
			LOG.error(e);
		}

		if (null != registerData.getAddresses() && registerData.getAddresses().size() > 0)
		{
			final List<AddressModel> addressList = new ArrayList<AddressModel>();
			for (final AddressData address : registerData.getAddresses())
			{
				AddressModel addressModel = getModelService().create(AddressModel.class);
				addressModel = populateAddress(address, addressModel);
				addressModel.setOwner(newCustomer);
				addressList.add(addressModel);
			}
			if (addressList.size() > 0)
			{
				newCustomer.setAddresses(addressList);
			}
		}

		final List<ConsentModel> consentModelList = new ArrayList<ConsentModel>();
		List<Type_consentModel> typeModelList;
		ConsentModel consentModel;
		Type_consentModel typeModel;

		if (null != registerData.getAccinfo_email() || null != registerData.getAccinfo_SMS())
		{
			// To populate ACCINFO consent
			consentModel = getModelService().create(ConsentModel.class);
			consentModel.setConsentType("ACCINFO");

			typeModelList = new ArrayList<Type_consentModel>();

			typeModel = getModelService().create(Type_consentModel.class);
			typeModel.setTypeID(new Integer(1));
			typeModel.setAccepted(registerData.getAccinfo_SMS());
			typeModelList.add(typeModel);

			typeModel = getModelService().create(Type_consentModel.class);
			typeModel.setTypeID(new Integer(2));
			typeModel.setAccepted(registerData.getAccinfo_email());
			typeModelList.add(typeModel);
			consentModel.setComms(typeModelList);
			consentModelList.add(consentModel);
		}
		if (null != registerData.getMarketingConsent_SMS() || null != registerData.getMarketingConsent_email())
		{
			// To populate MARKETING consent
			typeModelList = new ArrayList<Type_consentModel>();
			consentModel = getModelService().create(ConsentModel.class);
			consentModel.setConsentType("MARKETING");

			if (null != registerData.getMarketingConsent_SMS())
			{
				typeModel = getModelService().create(Type_consentModel.class);
				typeModel.setTypeID(new Integer(1));
				typeModel.setAccepted(registerData.getMarketingConsent_SMS());
				typeModelList.add(typeModel);
			}

			typeModel = getModelService().create(Type_consentModel.class);
			typeModel.setTypeID(new Integer(2));
			typeModel.setAccepted(registerData.getMarketingConsent_email());
			typeModelList.add(typeModel);

			consentModel.setComms(typeModelList);

			consentModelList.add(consentModel);
		}
		if (consentModelList.size() > 0)
		{
			newCustomer.setMarketingConsent(consentModelList);
		}

		return newCustomer;
	}

	private AddressModel populateAddress(final AddressData source, final AddressModel target)
	{
		if (null != source.getTypeID())
		{
			target.setTypeID(source.getTypeID());
			if (Integer.parseInt(source.getTypeID()) == 1)
			{
				target.setType("Postal Address");
			}
			if (Integer.parseInt(source.getTypeID()) == 2)
			{
				target.setType("Physical Address");
			}
			if (Integer.parseInt(source.getTypeID()) == 3)
			{
				target.setType("Delivery Address");
			}
		}

		if (null != source.getLine1())
		{
			target.setBuilding(source.getLine1());
		}
		if (null != source.getLine2())
		{
			target.setStreetname(source.getLine2());
		}
		if (null != source.getSuburb())
		{
			target.setStreetnumber(source.getSuburb());
		}
		if (null != source.getPostalCode())
		{
			target.setPostalcode(source.getPostalCode());
		}
		if (null != source.getCity() && null != source.getProvince() && StringUtils.isNotEmpty(source.getCity())
				&& StringUtils.isNotEmpty(source.getProvince()))
		{
			target.setTown(source.getCity() + ", " + source.getProvince());
		}
		else if (null != source.getCity() && StringUtils.isNotEmpty(source.getCity()))
		{
			target.setTown(source.getCity());
		}
		else
		{
			target.setTown(source.getProvince());
		}
		try
		{
			if (null != source.getCountry())
			{
				CountryModel country = new CountryModel();
				getModelService().attach(country);
				country.setIsocode(source.getCountry().getIsocode());
				country = flexibleSearchService.getModelByExample(country);
				if (null != country)
				{
					target.setCountry(country);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error(e);
		}

		return target;
	}
}
