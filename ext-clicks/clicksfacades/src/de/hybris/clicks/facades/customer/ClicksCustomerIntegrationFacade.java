/**
 *
 */
package de.hybris.clicks.facades.customer;

import de.hybris.clicks.clickscommercewebservices.dto.ClicksUserResponseWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.LoyalityDetailsWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.ResultWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;
import de.hybris.clicks.integration.data.populators.ClicksCustomerDataPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;



/**
 * @author manish.bhargava
 *
 */
public class ClicksCustomerIntegrationFacade
{

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	@Resource(name = "clicksCustomerDataPopulator")
	ClicksCustomerDataPopulator clicksCustomerDataPopulator;

	@Resource
	ModelService modelService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	public RegisterData populateRegisterData(final ClicksUserRequestWsDTO form, final String requestType)
	{
		final String newID = (String) myNumberseriesGenerator.generate();
		final RegisterData data = new RegisterData();
		data.setCustomerId(newID);
		if (StringUtils.isNotBlank(form.getTitleCode()))
		{
			data.setTitleCode(populateTitleCode(form.getTitleCode()));
		}
		else
		{
			data.setTitleCode("mr");
		}
		if (StringUtils.isNotBlank(form.getFirstName()))
		{
			data.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (StringUtils.isNotBlank(form.getLastName()))
		{
			data.setLastName(getTitleCase(form.getLastName()));
		}
		if (StringUtils.isNotBlank(form.getEmailAddress()))
		{
			data.setLogin(getTitleCase(form.getEmailAddress()));
		}
		if (StringUtils.isNotBlank(form.getPassword()))
		{
			data.setPassword(form.getPassword());
		}
		if (StringUtils.isNotBlank(form.getPreferredName()))
		{
			data.setPreferedName(getTitleCase(form.getPreferredName()));
		}
		else
		{
			if (StringUtils.isNotBlank(form.getFirstName()))
			{
				data.setPreferedName(getTitleCase(form.getFirstName()));
			}
		}
		if ("registerCC".equalsIgnoreCase(requestType) && StringUtils.isNotBlank(form.getMemberID()))
		{
			data.setMemberID(form.getMemberID());
		}

		//Populate data for non cc user/enrollment
		if ("registerNonCC".equalsIgnoreCase(requestType) || "enroll".equalsIgnoreCase(requestType))
		{
			data.setSAResident(form.isSAResident());
			if (StringUtils.isNotBlank(form.getIdNumber()))
			{
				data.setIDNumber(form.getIdNumber());
			}
			if (StringUtils.isNotBlank(form.getSA_DOB_day()) && StringUtils.isNotBlank(form.getSA_DOB_month())
					&& StringUtils.isNotBlank(form.getSA_DOB_year()))
			{
				final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
				data.setSA_DOB(date);
			}
			data.setMarketingConsent_email(form.isMarketingEmailConsentInfo());
			data.setMarketingConsent_SMS(form.isMarketingSMSConsentInfo());
		}

		//Populate data only for enrollment
		if ("enroll".equalsIgnoreCase(requestType))
		{
			data.setGender(new Integer(form.getGender()));
			if (!form.isAccEmailConsentInfo() && !form.isAccSMSConsentInfo())
			{
				data.setAccinfo_email(Boolean.TRUE.booleanValue());
			}
			else
			{
				data.setAccinfo_email(form.isAccEmailConsentInfo());
				data.setAccinfo_SMS(form.isAccSMSConsentInfo());
			}

			if (StringUtils.isNotBlank(form.getAlternateNumber()) || StringUtils.isNotBlank(form.getCellNumber()))
			{
				final List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
				ContactDetail contact;
				if (StringUtils.isNotBlank(form.getAlternateNumber()))
				{
					contact = new ContactDetail();
					contact.setTypeID("4");
					contact.setNumber(form.getAlternateNumber());
					contactDetails.add(contact);
				}
				if (StringUtils.isNotBlank(form.getCellNumber()))
				{
					contact = new ContactDetail();
					contact.setTypeID("2");
					contact.setNumber(form.getCellNumber());
					contactDetails.add(contact);
				}
				if (contactDetails.size() > 0)
				{
					data.setContactDetails(contactDetails);
				}
			}

			final List<AddressData> addresses = new ArrayList<AddressData>();
			final AddressData address = new AddressData();

			address.setTypeID("1");
			if (StringUtils.isNotBlank(form.getAddressLine1()))
			{
				address.setLine1(getTitleCase(form.getAddressLine1()));
			}
			if (StringUtils.isNotBlank(form.getAddressLine2()))
			{
				address.setLine2(getTitleCase(form.getAddressLine2()));
			}
			if (StringUtils.isNotBlank(form.getSuburb()))
			{
				address.setSuburb(getTitleCase(form.getSuburb()));
			}
			if (StringUtils.isNotBlank(form.getTown()))
			{
				address.setCity(getTitleCase(form.getTown()));
			}

			if (StringUtils.isNotBlank(form.getCountry()))
			{
				final CountryData countryData = new CountryData();
				countryData.setIsocode(form.getCountry());
				address.setCountry(countryData);
			}

			if (StringUtils.isNotBlank(form.getPostalCode()))
			{
				address.setPostalCode(form.getPostalCode());
			}
			if (StringUtils.isNotBlank(form.getProvinceCode()))
			{
				address.setProvince(form.getProvinceCode());
			}
			addresses.add(address);
			if (addresses.size() > 0)
			{
				data.setAddresses(addresses);
			}
		}
		return data;

	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}


	public CustomerData populateCustomerDataToPublish(final ClicksUserRequestWsDTO form, final String requestType)
	{
		final CustomerData customer = new CustomerData();
		if (StringUtils.isNotBlank(form.getCustomerID()))
		{
			customer.setCustomerID(form.getCustomerID());
		}
		customer.setRetailerID_hybris("1");
		customer.setSAResident(new Boolean(form.isSAResident()));

		if (StringUtils.isNotBlank(form.getTitleCode()))
		{
			customer.setTitleCode(populateTitleCode(form.getTitleCode()));
		}
		else
		{
			customer.setTitleCode("mr");
		}

		if (StringUtils.isNotBlank(form.getEmailAddress()))
		{
			customer.setEmailID(getTitleCase(form.getEmailAddress()));
		}
		if (StringUtils.isNotBlank(form.getFirstName()))
		{
			customer.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (StringUtils.isNotBlank(form.getPreferredName()))
		{
			customer.setPreferedName(getTitleCase(form.getPreferredName()));
		}
		if (StringUtils.isNotBlank(form.getLastName()))
		{
			customer.setLastName(getTitleCase(form.getLastName()));
		}
		if ("registerCC".equalsIgnoreCase(requestType) && StringUtils.isNotBlank(form.getMemberID()))
		{
			customer.setMemberID(form.getMemberID());
		}

		if (StringUtils.isNotBlank(form.getIdNumber()))
		{
			customer.setIDnumber(form.getIdNumber());
		}
		if (StringUtils.isNotBlank(form.getGender()))
		{
			customer.setGender(form.getGender());
		}

		if (StringUtils.isNotBlank(form.getSA_DOB_day()) && StringUtils.isNotBlank(form.getSA_DOB_month())
				&& StringUtils.isNotBlank(form.getSA_DOB_year()))
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			customer.setNonRSA_DOB(new Date(date));
		}

		// Populate customer for non cc user/enrollment
		if ("registerNonCC".equalsIgnoreCase(requestType) || "enroll".equalsIgnoreCase(requestType))
		{
			customer.setMarketingConsent_email(form.isMarketingEmailConsentInfo());
			customer.setMarketingConsent_SMS(form.isMarketingSMSConsentInfo());
		}
		if ("enroll".equalsIgnoreCase(requestType))
		{
			if (!form.isAccEmailConsentInfo() && !form.isAccSMSConsentInfo())
			{
				customer.setAccinfo_email(Boolean.TRUE.booleanValue());
			}
			else
			{
				customer.setAccinfo_email(form.isAccEmailConsentInfo());
				customer.setAccinfo_SMS(form.isAccSMSConsentInfo());
			}

			final List<AddressData> addresses = new ArrayList<AddressData>();
			final AddressData address = new AddressData();

			if (StringUtils.isNotBlank(form.getAddressLine1()))
			{
				address.setLine1(getTitleCase(form.getAddressLine1()));
			}
			if (StringUtils.isNotBlank(form.getAddressLine2()))
			{
				address.setLine2(getTitleCase(form.getAddressLine2()));
			}

			if (StringUtils.isNotBlank(form.getSuburb()))
			{
				address.setSuburb(getTitleCase(form.getSuburb()));
			}
			if (StringUtils.isNotBlank(form.getTown()))
			{
				address.setCity(getTitleCase(form.getTown()));
			}

			if (StringUtils.isNotBlank(form.getCountry()))
			{
				final CountryData countryData = new CountryData();
				countryData.setIsocode(form.getCountry());
				address.setCountry(countryData);
			}

			if (StringUtils.isNotBlank(form.getPostalCode()))
			{
				address.setPostalCode(form.getPostalCode());
			}
			if (StringUtils.isNotBlank(form.getProvinceCode()))
			{
				address.setProvince(form.getProvinceCode());
			}

			addresses.add(address);
			if (addresses.size() > 0)
			{
				customer.setAddresses(addresses);
			}
		}

		if (StringUtils.isNotBlank(form.getCellNumber()) || StringUtils.isNotBlank(form.getAlternateNumber()))
		{
			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
			if (StringUtils.isNotBlank(form.getCellNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getCellNumber());
				contact.setTypeID("2");
				contactDetailList.add(contact);
			}
			if (StringUtils.isNotBlank(form.getAlternateNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getAlternateNumber());
				contact.setTypeID("4");
				contactDetailList.add(contact);
			}
			if (contactDetailList.size() > 0)
			{
				customer.setContactDetails(contactDetailList);
			}
		}
		return customer;
	}

	public ClicksUserResponseWsDTO prepareUserResponse(final CustomerModel customerModel, final ResultData result)
	{
		return clicksCustomerDataPopulator.prepareUserResponse(customerModel, result);
	}

	public void removeUser(final String userId)
	{
		try
		{
			String query;
			SearchResult<UserModel> result;
			query = "SELECT {PK} FROM {User} WHERE {uid}=?userId";
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			result = flexibleSearchService.search(query, params);
			if (result.getCount() > 0)
			{
				for (final UserModel user : result.getResult())
				{
					try
					{
						modelService.removeAll(user.getAddresses());
					}
					catch (final Exception e)
					{
						//
					}
					modelService.remove(user);
				}
			}
		}
		catch (final Exception e)
		{

		}
	}

	/**
	 * Populate for plastic card.
	 *
	 * @param form
	 *           the form
	 * @return the clicks register form
	 */
	public ClicksUserResponseWsDTO populateForPlasticCard(final ClicksUserRequestWsDTO user, final ResultData result)
	{
		final ClicksUserResponseWsDTO clicksUserWsDTO = new ClicksUserResponseWsDTO();
		final LoyalityDetailsWsDTO loyalityDetailsWsDTO = new LoyalityDetailsWsDTO();
		if (null != result)
		{
			final ResultWsDTO resultWsDTO = new ResultWsDTO();
			resultWsDTO.setCode(result.getCode().toString());
			resultWsDTO.setMessage(result.getMessage());
			clicksUserWsDTO.setResult(resultWsDTO);
		}
		if (null != user.getEmailAddress())
		{
			clicksUserWsDTO.setEmailAddress(user.getEmailAddress());
		}
		if (StringUtils.isNotBlank(user.getSA_DOB_day()) && StringUtils.isNotBlank(user.getSA_DOB_month())
				&& StringUtils.isNotBlank(user.getSA_DOB_year()))
		{
			final String date = user.getSA_DOB_year() + "-" + user.getSA_DOB_month() + "-" + user.getSA_DOB_day();
			clicksUserWsDTO.setDateOfBirth(date);
		}
		if (null != user.getIdNumber())
		{
			clicksUserWsDTO.setIdNumber(user.getIdNumber());
		}
		if (StringUtils.isNotBlank(user.getMemberID()))
		{
			loyalityDetailsWsDTO.setClubCardNumber(user.getMemberID());
			clicksUserWsDTO.setLoyaltyDetails(loyalityDetailsWsDTO);
		}
		clicksUserWsDTO.setSAResident(user.isSAResident());
		return clicksUserWsDTO;
	}

	public CustomerModel getCustomer(final String id)
	{
		if (StringUtils.isNotBlank(id))
		{
			String query = "";
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
			query = "SELECT {PK} FROM {Customer} WHERE {memberID}=?id OR {uid}=?id";
			final SearchResultImpl<CustomerModel> result = (SearchResultImpl) flexibleSearchService.search(query, params);
			if (CollectionUtils.isNotEmpty(result.getResult()))
			{
				return result.getResult().get(0);
			}
		}
		return null;
	}

	private String populateTitleCode(final String titleCode)
	{
		String str = "";
		switch (titleCode)
		{
			case "mr":
				str = titleCode;
				break;

			case "mrs":
				str = titleCode;
				break;

			case "miss":
				str = titleCode;
				break;

			case "dr":
				str = titleCode;
				break;

			case "rev":
				str = titleCode;
				break;
			default:
				str = "mr";
				break;
		}
		return str;
	}
}
