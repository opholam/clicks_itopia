/**
 *
 */
package de.hybris.clicks.facades.customer;

import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;
import de.hybris.clicks.core.editProfile.EditProfileService;
import de.hybris.clicks.facades.myaccount.MyAccountFacade;
import de.hybris.clicks.integration.data.populators.ClicksCustomerDataPopulator;
import de.hybris.clicks.webclient.customer.client.CustomerUpdateClient;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;



/**
 * @author manish.bhargava
 *
 */
public class ClicksCustomerUpdateIntegrationFacade
{

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	@Resource(name = "clicksCustomerDataPopulator")
	ClicksCustomerDataPopulator clicksCustomerDataPopulator;

	@Resource
	ModelService modelService;

	@Resource(name = "editProfileService")
	EditProfileService editProfileService;

	@Resource
	CustomerUpdateClient customerUpdateClient;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "myAccountFacade")
	private MyAccountFacade myAccountFacade;

	public ResultData ChangeUid(final ClicksUserRequestWsDTO user)
	{
		final ResultData result = new ResultData();
		if (StringUtils.isNotBlank(user.getPassword()))
		{
			try
			{
				myAccountFacade.changeUid(user.getEmailAddress(), user.getPassword());

				//				final String newUid = customerFacade.getCurrentCustomer().getUid();
				//				final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
				//				final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(newUid, null,
				//						oldAuthentication.getAuthorities());
				//				newAuthentication.setDetails(oldAuthentication.getDetails());
				//				SecurityContextHolder.getContext().setAuthentication(newAuthentication);
			}
			catch (final DuplicateUidException e)
			{
				result.setCode(400);
				result.setMessage("Duplicate Uid");
				return result;
			}
			catch (final de.hybris.platform.commerceservices.customer.PasswordMismatchException e)
			{
				result.setCode(400);
				result.setMessage("Password mismatch");
				return result;
			}
			catch (final Exception e)
			{
				result.setCode(400);
				result.setMessage(e.getMessage());
				return result;
			}
		}
		else
		{
			result.setCode(400);
			result.setMessage("Password Cannot be empty");
			return result;
		}
		//If UID change is successful
		result.setCode(786);
		return result;
	}

	public void populateCustomerDataToPublish(final ClicksUserRequestWsDTO form, final CustomerData currentCustomerData,
			final boolean isClubcardUser)
	{

		currentCustomerData.setSAResident(new Boolean(form.isSAResident()));

		if (form.isEmailIDEditable() && StringUtils.isNotBlank(form.getEmailAddress()))
		{
			currentCustomerData.setEmailID(getTitleCase(form.getEmailAddress()));
		}

		if (StringUtils.isNotBlank(form.getFirstName()))
		{
			currentCustomerData.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (StringUtils.isNotBlank(form.getPreferredName()))
		{
			currentCustomerData.setPreferedName(getTitleCase(form.getPreferredName()));
		}
		if (StringUtils.isNotBlank(form.getLastName()))
		{
			currentCustomerData.setLastName(getTitleCase(form.getLastName()));
		}

		if (StringUtils.isNotBlank(form.getIdNumber()))
		{
			currentCustomerData.setIDnumber(form.getIdNumber());
		}

		if (StringUtils.isNotBlank(form.getSA_DOB_day()) && StringUtils.isNotBlank(form.getSA_DOB_month())
				&& StringUtils.isNotBlank(form.getSA_DOB_year()))
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			currentCustomerData.setNonRSA_DOB(new Date(date));
		}

		//		if (StringUtils.isNotBlank(form.getCustomerID()))
		//		{
		//			currentCustomerData.setCustomerID(form.getCustomerID());
		//		}

		if (isClubcardUser)
		{
			if (StringUtils.isNotBlank(form.getGender()))
			{
				currentCustomerData.setGender(form.getGender());
			}

			if (!form.isAccEmailConsentInfo() && !form.isAccSMSConsentInfo())
			{
				currentCustomerData.setAccinfo_email(Boolean.TRUE.booleanValue());
			}
			else
			{
				currentCustomerData.setAccinfo_email(form.isAccEmailConsentInfo());
				currentCustomerData.setAccinfo_SMS(form.isAccSMSConsentInfo());
			}
			currentCustomerData.setMarketingConsent_email(form.isMarketingEmailConsentInfo());
			currentCustomerData.setMarketingConsent_SMS(form.isMarketingSMSConsentInfo());

			final List<AddressData> addresses = new ArrayList<AddressData>();
			final AddressData address = new AddressData();

			if (StringUtils.isNotBlank(form.getAddressLine1()))
			{
				address.setLine1(getTitleCase(form.getAddressLine1()));
			}
			if (StringUtils.isNotBlank(form.getAddressLine2()))
			{
				address.setLine2(getTitleCase(form.getAddressLine2()));
			}

			if (StringUtils.isNotBlank(form.getSuburb()))
			{
				address.setSuburb(getTitleCase(form.getSuburb()));
			}
			if (StringUtils.isNotBlank(form.getTown()))
			{
				address.setCity(getTitleCase(form.getTown()));
			}

			if (StringUtils.isNotBlank(form.getCountry()))
			{
				final CountryData countryData = new CountryData();
				countryData.setIsocode(form.getCountry());
				address.setCountry(countryData);
			}

			if (StringUtils.isNotBlank(form.getPostalCode()))
			{
				address.setPostalCode(form.getPostalCode());
			}
			if (StringUtils.isNotBlank(form.getProvinceCode()))
			{
				address.setProvince(form.getProvinceCode());
			}

			addresses.add(address);
			if (addresses.size() > 0)
			{
				currentCustomerData.setAddresses(addresses);
			}

			if (StringUtils.isNotBlank(form.getCellNumber()) || StringUtils.isNotBlank(form.getAlternateNumber()))
			{
				final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
				if (StringUtils.isNotBlank(form.getCellNumber()))
				{
					final ContactDetail contact = new ContactDetail();
					contact.setNumber(form.getCellNumber());
					contact.setTypeID("2");
					contactDetailList.add(contact);
				}
				if (StringUtils.isNotBlank(form.getAlternateNumber()))
				{
					final ContactDetail contact = new ContactDetail();
					contact.setNumber(form.getAlternateNumber());
					contact.setTypeID("4");
					contactDetailList.add(contact);
				}
				if (contactDetailList.size() > 0)
				{
					currentCustomerData.setContactDetails(contactDetailList);
				}
			}
		}
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	public ResultData saveUpdatedClubCardData(final CustomerData customerData)
	{
		ResultData resultData = null;
		try
		{
			resultData = customerUpdateClient.updateCustomer(customerData);
			if ((null != resultData && null != resultData.getCode() && resultData.getCode().intValue() == 0))
			{
				editProfileService.saveClubCardData(customerData);
			}
			else if (null == resultData || null == resultData.getCode())
			{
				resultData = new ResultData();
				resultData.setCode(400);
			}
		}
		catch (final Exception e)
		{
			if (null == resultData || null == resultData.getCode())
			{
				resultData = new ResultData();
				resultData.setCode(400);
			}
		}
		return resultData;
	}

	public ResultData saveUpdatedNonClubCardData(final CustomerData customerData)
	{
		ResultData resultData = null;
		try
		{
			resultData = customerUpdateClient.updateCustomer(customerData);
			if ((null != resultData && null != resultData.getCode() && resultData.getCode().intValue() == 0))
			{
				editProfileService.saveNonClubCardData(customerData);
				resultData.setCode(00);
				resultData.setMessage("Success");
			}
			else if (null == resultData || null == resultData.getCode())
			{
				resultData = new ResultData();
				resultData.setCode(400);
			}
		}
		catch (final Exception e)
		{
			if (null == resultData || null == resultData.getCode())
			{
				resultData = new ResultData();
				resultData.setCode(400);
			}
		}
		return resultData;
	}
}
