/**
 *
 */
package de.hybris.clicks.facades.magazine;

import de.hybris.clicks.core.magazine.service.MagazineLandingService;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author siva.reddy
 *
 */
public class MagazineLandingFacadeImpl implements MagazineLandingFacade
{

	protected static final Logger LOG = Logger.getLogger(MagazineLandingFacadeImpl.class);

	@Resource(name = "magazineLandingService")
	private MagazineLandingService magazineLandingService;

	@Override
	public List<ArticleModel> getAllArticlesToMagazinePage(final String tagId, final String pageType)
	{
		LOG.debug("in MagazineLandingFacade Impl " + tagId + " PageType: " + pageType);
		return magazineLandingService.getAllArticlesToMagazinePage(tagId, pageType);
	}

	@Override
	public List<ArticleTagsModel> getArticleTagsToMagazinePage()
	{
		LOG.debug("in MagazineLandingFacade Impl ");
		return magazineLandingService.getArticleTagsToMagazinePage();
	}


}
