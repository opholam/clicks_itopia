/**
 *
 */
package de.hybris.clicks.facades.magazine;

import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;

import java.util.List;


/**
 * @author siva.reddy
 *
 */
public interface MagazineLandingFacade
{

	public List<ArticleModel> getAllArticlesToMagazinePage(String tagId, String pageType);

	public List<ArticleTagsModel> getArticleTagsToMagazinePage();
}
