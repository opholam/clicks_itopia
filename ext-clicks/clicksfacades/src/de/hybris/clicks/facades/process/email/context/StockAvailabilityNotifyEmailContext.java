/**
 *
 */
package de.hybris.clicks.facades.process.email.context;

import de.hybris.clicks.core.model.NotifyStockProcessModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author shruti.jhamb
 *
 */

/**
 * Velocity context for sending updated stock information email to the subscribed customer.
 *
 */
public class StockAvailabilityNotifyEmailContext extends AbstractEmailContext<NotifyStockProcessModel>
{

	private static final Logger LOG = Logger.getLogger(StockAvailabilityNotifyEmailContext.class);
	@Resource
	private ConfigurationService configurationService;
	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;
	//@Resource
	//private ProductService productService;
	private Converter<UserModel, CustomerData> customerConverter;
	//private Converter<ProductModel, ProductData> productConverter;
	private CustomerData customerData;
	private ProductData productData;
	public String emailId;
	public String storeName;
	public String productName;
	public String productBrand;
	public String productUrl;
	public String productPrice;
	public String productImage;
	public String imageAltText;
	public String displayName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#init(de.hybris.platform.
	 * processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
	 */
	@Override
	public void init(final NotifyStockProcessModel notifyStockProcessModel, final EmailPageModel emailPageModel)
	{
		if (notifyStockProcessModel != null)
		{
			super.init(notifyStockProcessModel, emailPageModel);
			customerData = getCustomerConverter().convert(getCustomer(notifyStockProcessModel));
			if (null != customerData.getFirstName())
			{
				displayName = customerData.getFirstName();
			}
			final String contactUsEmailAddress_service = (String) configurationService.getConfiguration().getProperty(
					"contactus.customercare.service.email");
			if (null != notifyStockProcessModel)
			{
				if (null != notifyStockProcessModel.getProductCode())
				{
					productData = productFacade.getProductForCodeAndOptions(notifyStockProcessModel.getProductCode(),
							Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.GALLERY));
					//productData = getProductConverter().convert(getProduct(notifyStockProcessModel));
					productName = productData.getName();
					productUrl = productData.getUrl();
					productBrand = productData.getBrand();
					productPrice = productData.getPrice().getFormattedValue();
					if (null != productData.getImages())
					{
						for (final ImageData image : productData.getImages())
						{
							if (image.getFormat().equalsIgnoreCase("cartIcon"))
							{
								productImage = image.getUrl();
								imageAltText = image.getAltText();
							}
						}
					}
				}
				if (null != notifyStockProcessModel.getStoreName())
				{
					storeName = notifyStockProcessModel.getStoreName();
				}
				if (null != notifyStockProcessModel.getCustomer().getUid())
				{
					emailId = notifyStockProcessModel.getCustomer().getUid();
					put(EMAIL, emailId);
					put(FROM_EMAIL, contactUsEmailAddress_service);
					put(DISPLAY_NAME, "Clicks");
					put(FROM_DISPLAY_NAME, "The Clicks Customer Services Team");
				}
			}
		}
	}

	/**
	 * @return the imageAltText
	 */
	public String getImageAltText()
	{
		return imageAltText;
	}

	/**
	 * @param imageAltText
	 *           the imageAltText to set
	 */
	public void setImageAltText(final String imageAltText)
	{
		this.imageAltText = imageAltText;
	}

	/**
	 * @return the customerConverter
	 */
	public Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}


	/**
	 * @param customerConverter
	 *           the customerConverter to set
	 */
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	/**
	 * @return the customerData
	 */
	public CustomerData getCustomerData()
	{
		return customerData;
	}

	/**
	 * @param customerData
	 *           the customerData to set
	 */
	public void setCustomerData(final CustomerData customerData)
	{
		this.customerData = customerData;
	}


	/**
	 * @return the emailId
	 */
	public String getEmailId()
	{
		return emailId;
	}


	/**
	 * @param emailId
	 *           the emailId to set
	 */
	public void setEmailId(final String emailId)
	{
		this.emailId = emailId;
	}


	/**
	 * @return the storeName
	 */
	public String getStoreName()
	{
		return storeName;
	}


	/**
	 * @param storeName
	 *           the storeName to set
	 */
	public void setStoreName(final String storeName)
	{
		this.storeName = storeName;
	}


	/**
	 * @return the displayName
	 */
	@Override
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *           the displayName to set
	 */
	public void setDisplayName(final String displayName)
	{
		this.displayName = displayName;
	}

	/**
	 * @return the productData
	 */
	public ProductData getProductData()
	{
		return productData;
	}

	/**
	 * @param productData
	 *           the productData to set
	 */
	public void setProductData(final ProductData productData)
	{
		this.productData = productData;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}


	/**
	 * @param productName
	 *           the productName to set
	 */
	public void setProductName(final String productName)
	{
		this.productName = productName;
	}



	/**
	 * @return the productImage
	 */
	public String getProductImage()
	{
		return productImage;
	}

	/**
	 * @param productImage
	 *           the productImage to set
	 */
	public void setProductImage(final String productImage)
	{
		this.productImage = productImage;
	}

	@Override
	protected BaseSiteModel getSite(final NotifyStockProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getEmailLanguage(de.hybris.platform
	 * .processengine.model.BusinessProcessModel)
	 */
	@Override
	protected LanguageModel getEmailLanguage(final NotifyStockProcessModel businessProcessModel)
	{
		// YTODO Auto-generated method stub
		return getEmailLanguage();
	}


	/**
	 * @return the productBrand
	 */
	public String getProductBrand()
	{
		return productBrand;
	}

	/**
	 * @param productBrand
	 *           the productBrand to set
	 */
	public void setProductBrand(final String productBrand)
	{
		this.productBrand = productBrand;
	}

	/**
	 * @return the productUrl
	 */
	public String getProductUrl()
	{
		return productUrl;
	}

	/**
	 * @param productUrl
	 *           the productUrl to set
	 */
	public void setProductUrl(final String productUrl)
	{
		this.productUrl = productUrl;
	}

	/**
	 * @return the productPrice
	 */
	public String getProductPrice()
	{
		return productPrice;
	}

	/**
	 * @param productPrice
	 *           the productPrice to set
	 */
	public void setProductPrice(final String productPrice)
	{
		this.productPrice = productPrice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getCustomer(de.hybris.platform
	 * .processengine.model.BusinessProcessModel)
	 */
	@Override
	protected CustomerModel getCustomer(final NotifyStockProcessModel notifyStockProcessModel)
	{
		// YTODO Auto-generated method stub
		return notifyStockProcessModel.getCustomer();
	}

	/*
	 * protected ProductModel getProduct(final NotifyStockProcessModel notifyStockProcessModel) { final ProductModel
	 * productModel = productService.getProductForCode(notifyStockProcessModel.getProductCode()); // YTODO Auto-generated
	 * method stub return productModel; }
	 */
}
