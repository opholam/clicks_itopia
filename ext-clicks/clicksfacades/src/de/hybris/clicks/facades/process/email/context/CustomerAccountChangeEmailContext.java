/**
 *
 */
package de.hybris.clicks.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author manikandan.r
 *
 */
public class CustomerAccountChangeEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>
{
	private Converter<UserModel, CustomerData> customerConverter;
	//private Converter<ClinicBookingAppointmentModel, ClinicBookingData> bookingConverter;
	private ClinicBookingData clinicBookingData;
	private CustomerData customerData;
	private String firstName;
	public String name;
	private String lastName;
	private String contactNumber;
	private String region;
	private String preferredClinic;
	private String alternateClinic;
	@Resource(name = "myNumberseriesRefNumber")
	public PersistentKeyGenerator myNumberseriesRefNumber;

	String newID = new String();
	public String memberId;

	/**
	 * @return the memberId
	 */
	public String getMemberId()
	{
		return memberId;
	}

	/**
	 * @param memberId
	 *           the memberId to set
	 */
	public void setMemberId(final String memberId)
	{
		this.memberId = memberId;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}



	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		customerData = getCustomerConverter().convert(getCustomer(storeFrontCustomerProcessModel));
		//clinicBookingData =storeFrontCustomerProcessModel.getClinicBookingAppointment();
		if (null != storeFrontCustomerProcessModel.getCustomer().getFirstName())
		{
			name = storeFrontCustomerProcessModel.getCustomer().getFirstName();
		}
		else
		{
			name = storeFrontCustomerProcessModel.getCustomer().getName();
		}
		if (null != storeFrontCustomerProcessModel.getCustomer().getMemberID())
		{
			memberId = storeFrontCustomerProcessModel.getCustomer().getMemberID();
		}
		if (null != storeFrontCustomerProcessModel.getClinicBookingAppointment())
		{
			firstName = storeFrontCustomerProcessModel.getClinicBookingAppointment().getFirstName();
			lastName = storeFrontCustomerProcessModel.getClinicBookingAppointment().getLastName();
			contactNumber = storeFrontCustomerProcessModel.getClinicBookingAppointment().getContactNumber();
			region = storeFrontCustomerProcessModel.getClinicBookingAppointment().getClinicProvince();
			preferredClinic = storeFrontCustomerProcessModel.getClinicBookingAppointment().getPreferredClinic();
			alternateClinic = storeFrontCustomerProcessModel.getClinicBookingAppointment().getAlternativeClinic();
			if (null != storeFrontCustomerProcessModel.getCustomer().getMemberID())
			{
				memberId = storeFrontCustomerProcessModel.getCustomer().getMemberID();
			}
			newID = (String) myNumberseriesRefNumber.generate();
		}

		final CustomerModel customerModel = getCustomer(storeFrontCustomerProcessModel);
		if (customerModel != null && !customerModel.getOldUIDs().isEmpty())
		{
			put(EMAIL, customerModel.getOldUIDs().iterator().next());

		}
	}

	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel)
	{
		return storeFrontCustomerProcessModel.getCustomer();
	}

	protected Converter<UserModel, CustomerData> getCustomerConverter()
	{
		return customerConverter;
	}

	/**
	 * @return the alternateClinic
	 */
	public String getAlternateClinic()
	{
		return alternateClinic;
	}

	/**
	 * @param alternateClinic
	 *           the alternateClinic to set
	 */
	public void setAlternateClinic(final String alternateClinic)
	{
		this.alternateClinic = alternateClinic;
	}



	/**
	 * @return the preferredClinic
	 */
	public String getPreferredClinic()
	{
		return preferredClinic;
	}

	/**
	 * @param preferredClinic
	 *           the preferredClinic to set
	 */
	public void setPreferredClinic(final String preferredClinic)
	{
		this.preferredClinic = preferredClinic;
	}

	/**
	 * @return the region
	 */
	public String getRegion()
	{
		return region;
	}

	/**
	 * @param region
	 *           the region to set
	 */
	public void setRegion(final String region)
	{
		this.region = region;
	}

	/**
	 * @return the clinicBookingData
	 */
	public ClinicBookingData getClinicBookingData()
	{
		return clinicBookingData;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *           the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}


	/**
	 * @return the newID
	 */
	public String getNewID()
	{
		return newID;
	}

	/**
	 * @param newID
	 *           the newID to set
	 */
	public void setNewID(final String newID)
	{
		this.newID = newID;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @param clinicBookingData
	 *           the clinicBookingData to set
	 */
	public void setClinicBookingData(final ClinicBookingData clinicBookingData)
	{
		this.clinicBookingData = clinicBookingData;
	}

	@Required
	public void setCustomerConverter(final Converter<UserModel, CustomerData> customerConverter)
	{
		this.customerConverter = customerConverter;
	}

	public CustomerData getCustomer()
	{
		return customerData;
	}

	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}


}
