/**
 *
 */
package de.hybris.clicks.facades.process.email.context;

import de.hybris.clicks.core.model.ContactUsProcessModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;


/**
 * Velocity context for a contact us query email.
 *
 */
public class ContactUsEmailContext extends AbstractEmailContext<ContactUsProcessModel>
{

	private static final Logger LOG = Logger.getLogger(ContactUsEmailContext.class);
	private Converter contactUsConverter;
	private ContactUsMessageData contactUsMessageData;
	@Resource
	private ConfigurationService configurationService;
	public String name;
	public String lastname;
	public String contactNumber;
	public String emailId;
	public String topic;
	public String message;
	public String customerCareFlag;
	public String clubCard;
	public String strDate;

	/**
	 * @return the strDate
	 */
	public String getStrDate()
	{
		return strDate;
	}


	/**
	 * @param strDate
	 *           the strDate to set
	 */
	public void setStrDate(final String strDate)
	{
		this.strDate = strDate;
	}


	/**
	 * @return the clubCard
	 */
	public String getClubCard()
	{
		return clubCard;
	}


	/**
	 * @param clubCard
	 *           the clubCard to set
	 */
	public void setClubCard(final String clubCard)
	{
		this.clubCard = clubCard;
	}


	/**
	 * @return the customerCareFlag
	 */
	public String getCustomerCareFlag()
	{
		return customerCareFlag;
	}


	/**
	 * @param customerCareFlag
	 *           the customerCareFlag to set
	 */
	public void setCustomerCareFlag(final String customerCareFlag)
	{
		this.customerCareFlag = customerCareFlag;
	}


	@Resource(name = "modelService")
	private ModelService modelService;


	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}


	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}


	/**
	 * @return the topic
	 */
	public String getTopic()
	{
		return topic;
	}


	/**
	 * @param topic
	 *           the topic to set
	 */
	public void setTopic(final String topic)
	{
		this.topic = topic;
	}


	/**
	 * @return the emailId
	 */
	public String getEmailId()
	{
		return emailId;
	}


	/**
	 * @param emailId
	 *           the emailId to set
	 */
	public void setEmailId(final String emailId)
	{
		this.emailId = emailId;
	}


	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}


	/**
	 * @param contactNumber
	 *           the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}


	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}



	/**
	 * @return the lastname
	 */
	public String getLastname()
	{
		return lastname;
	}


	/**
	 * @param lastname
	 *           the lastname to set
	 */
	public void setLastname(final String lastname)
	{
		this.lastname = lastname;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#init(de.hybris.platform.
	 * processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
	 */
	@Override
	public void init(final ContactUsProcessModel contactUsProcessModel, final EmailPageModel emailPageModel)
	{
		if (contactUsProcessModel != null)
		{
			super.init(contactUsProcessModel, emailPageModel);
			contactUsMessageData = (ContactUsMessageData) contactUsConverter.convert(contactUsProcessModel.getContactUsMessage());
			if (null != contactUsProcessModel.getCustomerCareFlag())
			{
				/*
				 * validates the email to be triggered is to the customer or clicks team
				 */
				if (contactUsProcessModel.getCustomerCareFlag().booleanValue())
				{
					customerCareFlag = "true";
				}
				else if (!contactUsProcessModel.getCustomerCareFlag().booleanValue())
				{
					customerCareFlag = "false";
				}
				else
				{
					customerCareFlag = null;
				}
			}
			if (null != contactUsProcessModel.getContactUsMessage())
			{
				final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd-HH:mm");//dd/MM/yyyy
				final Date now = new Date();
				strDate = sdfDate.format(now);
				if (null != contactUsMessageData.getFirstName())
				{
					name = contactUsMessageData.getFirstName();
				}
				if (null != contactUsMessageData.getLastName())
				{
					lastname = contactUsMessageData.getLastName();
				}
				if (null != contactUsMessageData.getContactNumber())
				{
					contactNumber = contactUsMessageData.getContactNumber();
				}
				if (null != contactUsMessageData.getEmailAddress())
				{
					emailId = contactUsMessageData.getEmailAddress();
				}
				if (null != contactUsMessageData.getTopics())
				{
					topic = contactUsMessageData.getTopics();
				}
				if (null != contactUsMessageData.getMessage())
				{
					message = contactUsMessageData.getMessage();
				}
				if (null != contactUsMessageData.getClubCardNumber())
				{
					clubCard = contactUsMessageData.getClubCardNumber();
				}
			}
			/*
			 * email triggered to the clicks team
			 */
			if (null != customerCareFlag && customerCareFlag.equalsIgnoreCase("true"))
			{
				final String contactUsEmailAddress_service = (String) configurationService.getConfiguration().getProperty(
						"contactus.customercare.service.email");
				final String contactUsEmailAddress_clubcard = (String) configurationService.getConfiguration().getProperty(
						"contactus.customercare.clubcard.email");
				final String contactUsEmailAddress_script = (String) configurationService.getConfiguration().getProperty(
						"contactus.customercare.repeat.script.email");
				/*
				 * Checking the topic type
				 */
				if (null != contactUsEmailAddress_service && null != contactUsEmailAddress_clubcard)
				{
					/*
					 * checking query topic is related to clubcard or service
					 */
					if (contactUsMessageData.getTopics().equalsIgnoreCase("Forgotten clubcard or account details")
							|| contactUsMessageData.getTopics().equalsIgnoreCase("Clubcard")
							|| contactUsMessageData.getTopics().equalsIgnoreCase("Lost or Stolen Card")
							|| contactUsMessageData.getTopics().equalsIgnoreCase("ClubCard Query")
							|| contactUsMessageData.getTopics().contains("ClubCard"))
					{
						put(EMAIL, contactUsEmailAddress_clubcard);
					}
					else if (StringUtils.containsIgnoreCase(contactUsMessageData.getTopics(), "script"))
					{
						put(EMAIL, contactUsEmailAddress_script);
					}
					else
					{
						put(EMAIL, contactUsEmailAddress_service);
					}
					/*
					 * setting customer email as from email
					 */
					put(FROM_EMAIL, contactUsMessageData.getEmailAddress());
					put(DISPLAY_NAME, "The Clicks Customer Services Team");
				}
			}
			/*
			 * email treiggered to the customer
			 */
			else if (null != customerCareFlag && customerCareFlag.equalsIgnoreCase("false"))
			{
				final String contactUsEmailAddress_service = (String) configurationService.getConfiguration().getProperty(
						"contactus.customercare.service.email");
				final String contactUsEmailAddress_clubcard = (String) configurationService.getConfiguration().getProperty(
						"contactus.customercare.clubcard.email");
				if (null != contactUsEmailAddress_service && null != contactUsEmailAddress_clubcard)
				{
					if (contactUsMessageData.getTopics().equalsIgnoreCase("Forgotten clubcard or account details")
							|| contactUsMessageData.getTopics().equalsIgnoreCase("Clubcard")
							|| contactUsMessageData.getTopics().equalsIgnoreCase("Lost or Stolen Card")
							|| contactUsMessageData.getTopics().equalsIgnoreCase("ClubCard Query")
							|| contactUsMessageData.getTopics().contains("ClubCard"))
					{
						/*
						 * setting clubcard clicks team email as from email
						 */
						put(FROM_EMAIL, contactUsEmailAddress_clubcard);
						put(EMAIL, contactUsMessageData.getEmailAddress());
						put(DISPLAY_NAME, contactUsMessageData.getFirstName() + " " + contactUsMessageData.getLastName());
					}
					else
					{
						/*
						 * setting service clicks team email as from email
						 */
						put(FROM_EMAIL, contactUsEmailAddress_service);
						//put(FROM_DISPLAY_NAME, contactUsEmailAddress_service);
						put(EMAIL, contactUsMessageData.getEmailAddress());
						put(DISPLAY_NAME, contactUsMessageData.getFirstName() + " " + contactUsMessageData.getLastName());
						//modelService.save(emailPageModel);
					}
				}
			}
		}
	}

	/**
	 * @return the contactUsMessageData
	 */
	public ContactUsMessageData getContactUsMessageData()
	{
		return contactUsMessageData;
	}


	/**
	 * @param contactUsMessageData
	 *           the contactUsMessageData to set
	 */
	public void setContactUsMessageData(final ContactUsMessageData contactUsMessageData)
	{
		this.contactUsMessageData = contactUsMessageData;
	}


	/**
	 * @return the contactUsConverter
	 */
	public Converter getContactUsConverter()
	{
		return contactUsConverter;
	}


	/**
	 * @param contactUsConverter
	 *           the contactUsConverter to set
	 */
	public void setContactUsConverter(final Converter contactUsConverter)
	{
		this.contactUsConverter = contactUsConverter;
	}


	@Override
	protected BaseSiteModel getSite(final ContactUsProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}


	@Override
	protected CustomerModel getCustomer(final ContactUsProcessModel businessProcessModel)
	{
		return null;
	}


	@Override
	protected LanguageModel getEmailLanguage(final ContactUsProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

}
