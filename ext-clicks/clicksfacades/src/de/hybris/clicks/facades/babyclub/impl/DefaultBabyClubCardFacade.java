/**
 *
 */
package de.hybris.clicks.facades.babyclub.impl;


import de.hybris.clicks.core.babyclub.BabyClubCardService;
import de.hybris.clicks.facades.babyclub.BabyClubCardFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import javax.annotation.Resource;


/**
 * @author ashish.vyas
 *
 */
public class DefaultBabyClubCardFacade implements BabyClubCardFacade
{

	@Resource(name = "babyClubCardService")
	BabyClubCardService babyClubCardService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.facades.babyclub.BabyClubCardFacade#joinBabyClub(de.hybris.platform.commercefacades.user.data
	 * .CustomerData)
	 */
	@Override
	public void joinBabyClub(final CustomerData customerData) throws Exception
	{
		babyClubCardService.joinBabyClub(customerData);
	}

	@Override
	public CustomerData populateBabyClubForm()
	{
		CustomerData customerData = new CustomerData();
		customerData = babyClubCardService.getBabyClubDetails();
		return customerData;

	}
}
