/**
 *
 */
package de.hybris.clicks.facades.babyclub;

import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 * @author ashish.vyas
 *
 */
public interface BabyClubCardFacade
{
	public void joinBabyClub(final CustomerData customerData) throws Exception;

	public CustomerData populateBabyClubForm();
}
