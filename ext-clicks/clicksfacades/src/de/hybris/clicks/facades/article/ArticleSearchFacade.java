/**
 * 
 */
package de.hybris.clicks.facades.article;

import de.hybris.platform.commerceservices.search.facetdata.ArticleSearchPageData;

import java.util.List;

import org.springframework.ui.Model;


/**
 * @author siva.reddy
 * 
 */
public interface ArticleSearchFacade
{

	List<ArticleSearchPageData> searchArticles(final String searchText, final int count, final String isHealth, final Model model);
}
