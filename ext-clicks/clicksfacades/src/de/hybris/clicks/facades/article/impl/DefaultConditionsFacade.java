/**
 *
 */
package de.hybris.clicks.facades.article.impl;

import de.hybris.clicks.core.articles.ConditionService;
import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.core.model.ConditionsStageModel;
import de.hybris.clicks.facades.article.ConditionsFacade;


/**
 * @author baskar.lakshmanan
 *
 */
public class DefaultConditionsFacade implements ConditionsFacade
{

	private ConditionService conditionService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facades.article.ConditionsProcessor#getConditionDetail(java.lang.String)
	 */
	@Override
	public ConditionsModel getConditionDetail(final String conditionCode)
	{
		return conditionService.getConditonForCode(conditionCode);
	}

	/**
	 * @return the conditionService
	 */
	public ConditionService getConditionService()
	{
		return conditionService;
	}

	/**
	 * @param conditionService
	 *           the conditionService to set
	 */
	public void setConditionService(final ConditionService conditionService)
	{
		this.conditionService = conditionService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.article.ConditionsFacade#getConditionByAreaDetail(java.lang.String)
	 */
	@Override
	public ConditionsAreaModel getConditionByAreaDetail(final String conditionCode)
	{
		// YTODO Auto-generated method stub
		return conditionService.getConditionByAreaDetail(conditionCode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.article.ConditionsFacade#getConditionByStageDetail(java.lang.String)
	 */
	@Override
	public ConditionsStageModel getConditionByStageDetail(final String conditionCode)
	{
		// YTODO Auto-generated method stub
		return conditionService.getConditionByStageDetail(conditionCode);
	}
}
