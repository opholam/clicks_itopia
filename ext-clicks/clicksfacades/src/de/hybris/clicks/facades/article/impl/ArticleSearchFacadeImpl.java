/**
 * 
 */
package de.hybris.clicks.facades.article.impl;

import de.hybris.clicks.facades.article.ArticleSearchFacade;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.facetdata.ArticleSearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.enums.ConverterType;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.FacetSearchService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery.Operator;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;


/**
 * @author siva.reddy
 * 
 */
public class ArticleSearchFacadeImpl implements ArticleSearchFacade
{

	private static final Logger LOG = Logger.getLogger(ArticleSearchFacadeImpl.class);

	@Resource(name = "extFacetSearchService")
	private FacetSearchService facetSearchService;
	@Resource(name = "solrFacetSearchConfigSelectionStrategy")
	private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;
	@Resource(name = "facetSearchConfigService")
	private FacetSearchConfigService facetSearchConfigService;
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "productService")
	private ProductService productService;

	//article search
	@Override
	public List<ArticleSearchPageData> searchArticles(final String searchText, final int count, final String isHealth,
			final Model model)
	{
		final SolrSearchResult result = searchForArticles(searchText, count, isHealth);
		if (result != null)
		{
			final List<Object> searchArticleData = result.getResultData(ConverterType.STOREFRONT);
			final List<ArticleSearchPageData> articleSearchPageData = new ArrayList<ArticleSearchPageData>();
			for (final Object object : searchArticleData)
			{
				final ArticleSearchPageData aspd = (ArticleSearchPageData) object;
				articleSearchPageData.add(aspd);
			}
			if ("true".equalsIgnoreCase(isHealth))
			{
				model.addAttribute("totalNoOfHealthArticles", result.getTotalNumberOfResults());
			}
			else
			{
				model.addAttribute("totalNoOfInspirationArticles", result.getTotalNumberOfResults());
			}
			return articleSearchPageData;
		}
		return Collections.EMPTY_LIST;
	}

	protected SolrSearchResult searchForArticles(final String searchQuery, final int pageSize, final String isHealth)
	{
		try
		{
			final SearchQuery query = getArticleSolrQuery(searchQuery);
			query.setPageSize(pageSize);
			query.addFacetValue("isHealthArticle", isHealth);
			return (SolrSearchResult) facetSearchService.search(query);
		}
		catch (final Exception e)
		{
			LOG.error("Exception  while searching for solr articles " + e.getMessage());
		}
		return null;
	}


	protected SearchQuery getArticleSolrQuery(final String searchText) throws NoValidSolrConfigException,
			FacetConfigServiceException
	{
		final FacetSearchConfig facetSearchConfig = getFacetSearchConfig();
		final SearchQuery query = new SearchQuery(facetSearchConfig, getArticleIndexedType(facetSearchConfig));
		query.setCurrency(getCommonI18NService().getCurrentCurrency().getIsocode());
		query.setLanguage(getCommonI18NService().getCurrentLanguage().getIsocode());
		query.setUserQuery("*:*");
		//prepare fields to search in aticles
		query.searchInField("articleTitle_text", "*" + searchText + "*", Operator.OR);
		query.searchInField("titleName_text", "*" + searchText + "*", Operator.OR);
		query.searchInField("titleContent_text", "*" + searchText + "*", Operator.OR);

		query.setEnableSpellcheck(true);
		query.setCatalogVersions(new ArrayList<CatalogVersionModel>());
		return query;
	}

	private IndexedType getArticleIndexedType(final FacetSearchConfig config)
	{
		final IndexConfig indexConfig = config.getIndexConfig();
		final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
		if ((indexedTypes != null) && (!indexedTypes.isEmpty()))
		{
			for (final IndexedType index : indexedTypes)
			{
				if ("ArticleIndexType".equals(index.getIndexName()))
				{
					return index;
				}
			}
			return indexedTypes.iterator().next();
		}
		return null;
	}

	protected Collection<CatalogVersionModel> getSessionProductCatalogVersions()
	{
		final BaseSiteModel currentSite = getBaseSiteService().getCurrentBaseSite();
		final List productCatalogs = getBaseSiteService().getProductCatalogs(currentSite);
		final Collection<CatalogVersionModel> sessionCatalogVersions = getCatalogVersionService().getSessionCatalogVersions();
		final Collection result = new ArrayList();
		for (final CatalogVersionModel sessionCatalogVersion : sessionCatalogVersions)
		{
			if (!productCatalogs.contains(sessionCatalogVersion.getCatalog()))
			{
				continue;
			}
			result.add(sessionCatalogVersion);
		}
		return result;
	}

	public FacetSearchService getFacetSearchService()
	{
		return facetSearchService;
	}

	/**
	 * @param facetSearchService
	 *           the facetSearchService to set
	 */
	public void setFacetSearchService(final FacetSearchService facetSearchService)
	{
		this.facetSearchService = facetSearchService;
	}

	protected FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException, FacetConfigServiceException
	{
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = this.solrFacetSearchConfigSelectionStrategy
				.getCurrentSolrFacetSearchConfig();
		return this.facetSearchConfigService.getConfiguration(solrFacetSearchConfigModel.getName());
	}

	/**
	 * @return the solrFacetSearchConfigSelectionStrategy
	 */
	public SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy()
	{
		return solrFacetSearchConfigSelectionStrategy;
	}

	/**
	 * @param solrFacetSearchConfigSelectionStrategy
	 *           the solrFacetSearchConfigSelectionStrategy to set
	 */
	public void setSolrFacetSearchConfigSelectionStrategy(
			final SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy)
	{
		this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
	}

	/**
	 * @return the facetSearchConfigService
	 */
	public FacetSearchConfigService getFacetSearchConfigService()
	{
		return facetSearchConfigService;
	}

	/**
	 * @param facetSearchConfigService
	 *           the facetSearchConfigService to set
	 */
	public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService)
	{
		this.facetSearchConfigService = facetSearchConfigService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService
	 *           the catalogVersionService to set
	 */
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

}//class
