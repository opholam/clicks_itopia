/**
 *
 */
package de.hybris.clicks.facades.article;

import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ConditionsModel;

import java.util.List;


/**
 * @author siva.reddy
 *
 */
public interface ArticleFacade
{
	List<ArticleModel> getArticleByArticleCode(String articleCode);

	/**
	 * @param articleCode
	 * @return
	 */
	List<ConditionsModel> getRelatedConditionsForArticleCode(String articleTitle);

	List<ConditionsModel> getRelatedMedicinesForArticleCode(String articleTitle);
}
