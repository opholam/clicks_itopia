/**
 *
 */
package de.hybris.clicks.facades.article;

import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.core.model.ConditionsStageModel;



/**
 * @author baskar.lakshmanan
 *
 */
public interface ConditionsFacade
{
	ConditionsModel getConditionDetail(String conditionCode);

	/**
	 * @param conditionCode
	 * @return
	 */
	ConditionsAreaModel getConditionByAreaDetail(String conditionCode);

	/**
	 * @param conditionCode
	 * @return
	 */
	ConditionsStageModel getConditionByStageDetail(String conditionCode);
}
