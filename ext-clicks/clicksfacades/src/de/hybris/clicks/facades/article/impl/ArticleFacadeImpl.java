/**
 *
 */
package de.hybris.clicks.facades.article.impl;

import de.hybris.clicks.core.articles.service.ArticleService;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.facades.article.ArticleFacade;

import java.util.List;

import javax.annotation.Resource;


/**
 * @author siva.reddy/Swapnil
 *
 */
public class ArticleFacadeImpl implements ArticleFacade
{
	@Resource(name = "articleService")
	private ArticleService articleService;

	@Override
	public List<ArticleModel> getArticleByArticleCode(final String articleCode)
	{
		return articleService.getArticleByArticleCode(articleCode);
	}

	@Override
	public List<ConditionsModel> getRelatedConditionsForArticleCode(final String articleTitle)
	{
		return articleService.getRelatedConditionsForArticleCode(articleTitle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.article.ArticleFacade#getRelatedMedicinesForArticleCode(java.lang.String)
	 */
	@Override
	public List<ConditionsModel> getRelatedMedicinesForArticleCode(final String articleTitle)
	{
		return articleService.getRelatedMedicinesForArticleCode(articleTitle);
	}

}
