/**
 *
 */
package de.hybris.clicks.facades.customException;



import org.apache.log4j.Logger;


/**
 * @author manish.bhargava
 *
 */
public class ClicksOrderFraudException extends Exception
{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = Logger.getLogger(ClicksOrderFraudException.class);

	public ClicksOrderFraudException()
	{
	}

	public ClicksOrderFraudException(final String message)
	{
		super(message);
		LOG.error(message);
	}
}
