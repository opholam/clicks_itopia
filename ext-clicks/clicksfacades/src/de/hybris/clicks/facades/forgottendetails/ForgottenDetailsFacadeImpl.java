/**
 * 
 */
package de.hybris.clicks.facades.forgottendetails;

import de.hybris.clicks.core.forgottendetails.ForgottenDetailsService;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Date;

import javax.annotation.Resource;



/**
 * @author siva.reddy
 * 
 */
public class ForgottenDetailsFacadeImpl implements ForgottenDetailsFacade
{

	@Resource(name = "forgottenDetailsService")
	private ForgottenDetailsService forgottenDetailsService;

	@Override
	public CustomerModel checkClubcardResidentExists(final String clubCardNumber, final String residentNumber)
	{
		return forgottenDetailsService.checkClubcardResidentExists(clubCardNumber, residentNumber);
	}

	@Override
	public CustomerModel checkClubcardDOBExists(final String clubCardNumber, final Date dob)
	{
		return forgottenDetailsService.checkClubcardDOBExists(clubCardNumber, dob);
	}

	@Override
	public CustomerModel checkResidentExists(final String clubcardNumber, final String residentNumber)
	{
		return forgottenDetailsService.checkResidentExists(clubcardNumber, residentNumber);
	}

}
