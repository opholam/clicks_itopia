/**
 * 
 */
package de.hybris.clicks.facades.forgottendetails;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Date;


/**
 * @author siva.reddy
 * 
 */
public interface ForgottenDetailsFacade
{

	CustomerModel checkClubcardResidentExists(final String clubCardNumber, final String residentNumber);

	CustomerModel checkClubcardDOBExists(final String clubCardNumber, final Date dob);

	CustomerModel checkResidentExists(String clubcardNumber, String residentNumber);
}
