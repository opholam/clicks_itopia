/**
 *
 */
package de.hybris.clicks.facades.checkout;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.clicks.core.dao.checkout.ClicksCheckoutDao;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.DeliveryAreaModel;
import de.hybris.clicks.facades.customException.ClicksOrderFraudException;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author mbhargava
 *
 */
public class ClicksCheckoutFacade extends DefaultAcceleratorCheckoutFacade
{

	private CustomerEmailResolutionService customerEmailResolutionService;

	private Converter<PaymentInfoModel, PaymentInfoData> paymentInfoConverter;
	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource
	private CartService cartService;

	public SearchResult<DeliveryAreaModel> fetchResults(final String postcode)
	{
		final SearchResult<DeliveryAreaModel> result = clicksCheckoutDao.fetchResults(postcode);
		return result;
	}

	/**
	 * @return the paymentInfoConverter
	 */
	public Converter<PaymentInfoModel, PaymentInfoData> getPaymentInfoConverter()
	{
		return paymentInfoConverter;
	}

	/**
	 * @param paymentInfoConverter
	 *           the paymentInfoConverter to set
	 */
	public void setPaymentInfoConverter(final Converter<PaymentInfoModel, PaymentInfoData> paymentInfoConverter)
	{
		this.paymentInfoConverter = paymentInfoConverter;
	}

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	/**
	 * @return the customerEmailResolutionService
	 */
	public CustomerEmailResolutionService getCustomerEmailResolutionService()
	{
		return customerEmailResolutionService;
	}

	/**
	 * @param customerEmailResolutionService
	 *           the customerEmailResolutionService to set
	 */
	public void setCustomerEmailResolutionService(final CustomerEmailResolutionService customerEmailResolutionService)
	{
		this.customerEmailResolutionService = customerEmailResolutionService;
	}

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	ClicksCheckoutDao clicksCheckoutDao;

	public AddressData getDeliveryAreaDetails(final String postcode)
	{
		AddressData addressData = null;
		addressData = clicksCheckoutDao.getDeliveryAreaDetails(postcode);
		return addressData;
	}

	@Override
	public CCPaymentInfoData createPaymentSubscription(final CCPaymentInfoData paymentInfoData)
	{
		//validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
		final AddressData billingAddressData = paymentInfoData.getBillingAddress();
		//validateParameterNotNullStandardMessage("billingAddress", billingAddressData);

		if (checkIfCurrentUserIsTheCartUser())
		{
			final CustomerModel customerModel = getCustomerModel();
			//CreditCardPaymentInfoModel paymentInfoModel = modelService.create(CreditCardPaymentInfoModel.class);
			createPaymentSubscription(customerModel, billingAddressData, billingAddressData.getTitleCode(), getPaymentProvider(),
					paymentInfoData.isSaved());
			return new CCPaymentInfoData();
		}

		return null;
		//return super.createPaymentSubscription(paymentInfoData);
	}

	private void createPaymentSubscription(final CustomerModel customerModel, final AddressData billingAddressData,
			final String titleCode, final String paymentProvider, final boolean saveInAccount)
	{

		AddressModel billingAddress = null;
		billingAddress = addBillingAddress(billingAddressData);
		if (null != billingAddress)
		{
			saveBillingAddressInCart(billingAddress.getPk().toString(), billingAddressData.getDeliveryInstructions());
		}

	}

	public void setPaymentDetailsForCart(final CustomerModel customerModel, final AddressModel addressModel)
	{
		//validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);

		if (checkIfCurrentUserIsTheCartUser())
		{
			final CartModel cart = getCart();
			customerModel.setDefaultPaymentAddress(addressModel);
			getModelService().save(customerModel);
			final PaymentInfoModel paymentInfoModel = getModelService().create(PaymentInfoModel.class);
			paymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
			paymentInfoModel.setUser(customerModel);
			//paymentInfoModel.setBillingAddress(addressModel);

			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cart);
			parameter.setPaymentInfo(paymentInfoModel);
			getCommerceCheckoutService().setPaymentInfo(parameter);
			getModelService().refresh(customerModel);
		}
	}

	public CustomerModel getCustomerModel()
	{
		return getCurrentUserForCheckout();
	}

	public AddressModel saveBillingAddressInCart(final String pk, final String deliveryInstruction)
	{
		final CustomerModel customerModel = getCustomerModel();
		final CartModel cart = getCart();
		AddressModel addressModel = getModelService().create(AddressModel.class);
		for (final AddressModel address : customerModel.getAddresses())
		{
			if (address.getPk().toString().equals(pk))
			{
				addressModel = address;
				break;
			}
		}
		if (null != addressModel)
		{
			cart.setPaymentAddress(addressModel);
			if (StringUtils.isNotBlank(deliveryInstruction))
			{
				cart.setDeliveryInstructions(deliveryInstruction);
			}
			getModelService().save(cart);
			getModelService().refresh(cart);
			setPaymentDetailsForCart(customerModel, addressModel);
		}
		return addressModel;
	}

	public boolean saveDeliveryAsBillingAddress()
	{
		final CustomerModel customerModel = getCustomerModel();
		final CartModel cart = getCart();

		final AddressModel addressModel = cart.getDeliveryAddress();
		AddressModel billingAddress = getModelService().create(AddressModel.class);
		if (null != addressModel)
		{
			billingAddress.setFirstname(addressModel.getFirstname());
			billingAddress.setLastname(addressModel.getLastname());
			billingAddress.setLine1(addressModel.getLine1());
			billingAddress.setLine2(addressModel.getLine2());
			billingAddress.setTown(addressModel.getTown());
			billingAddress.setStreetname(addressModel.getStreetname());
			billingAddress.setStreetnumber(addressModel.getStreetnumber());
			billingAddress.setSuburb(addressModel.getSuburb());
			billingAddress.setProvince(addressModel.getProvince());
			billingAddress.setPostalcode(addressModel.getPostalcode());
			billingAddress.setCountry(addressModel.getCountry());
			billingAddress.setVisibleInAddressBook(Boolean.TRUE);
			billingAddress.setBillingAddress(Boolean.TRUE);
			billingAddress.setEmail(addressModel.getEmail());
			billingAddress.setOwner(customerModel);
			try
			{
				final List<AddressModel> list = flexibleSearchService.getModelsByExample(billingAddress);
				if (CollectionUtils.isNotEmpty(list))
				{
					billingAddress = list.get(0);
				}
			}
			catch (final Exception e)
			{
				//
			}
			getCustomerAccountService().saveAddressEntry(customerModel, billingAddress);
			getModelService().save(billingAddress);
			cart.setPaymentAddress(billingAddress);
			getModelService().save(cart);
			setPaymentDetailsForCart(customerModel, billingAddress);
			if (cartService.hasSessionCart() && null != cartService.getSessionCart().getUser())
			{
				if (getCheckoutCustomerStrategy().isAnonymousCheckout() && null != billingAddress)
				{
					final CustomerModel customer = (CustomerModel) cartService.getSessionCart().getUser();
					customer.setEmailID(billingAddress.getEmail());
					if (null != cartService.getSessionCart().getDeliveryAddress()
							&& null != cartService.getSessionCart().getDeliveryAddress().getCellphone())
					{
						final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
						final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
						contactModel.setNumber(cartService.getSessionCart().getDeliveryAddress().getCellphone());
						contactModel.setTypeID("2");
						contactModelList.add(contactModel);
						customer.setContactDetails(contactModelList);
					}
					modelService.save(customer);
				}
				else if (null != customerModel.getUid() && !getCheckoutCustomerStrategy().isAnonymousCheckout())
				{
					customerModel.setEmailID(customerModel.getUid());
					modelService.save(customerModel);
				}
			}
			customerModel.setDefaultPaymentAddress(addressModel);
			getModelService().save(customerModel);
			getModelService().refresh(customerModel);
			return true;
		}
		return false;
	}

	public AddressModel getBillingAddressForCode(final String editAddressCode)
	{
		final CustomerModel customerModel = getCustomerModel();
		return getCustomerAccountService().getAddressForCode(customerModel, editAddressCode);
	}

	public AddressModel addBillingAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);

		final CustomerModel currentCustomer = getCurrentUserForCheckout();

		final boolean makeThisAddressTheDefault = addressData.isDefaultAddress()
				|| (currentCustomer.getDefaultShipmentAddress() == null && addressData.isVisibleInAddressBook());

		// Create the new address model
		AddressModel newAddress = getModelService().create(AddressModel.class);
		getAddressReversePopulator().populate(addressData, newAddress);
		try
		{
			final List<AddressModel> list = flexibleSearchService.getModelsByExample(newAddress);
			if (CollectionUtils.isNotEmpty(list))
			{
				newAddress = list.get(0);
			}
		}
		catch (final Exception e)
		{
			//
		}
		// Store the address against the user
		getCustomerAccountService().saveAddressEntry(currentCustomer, newAddress);

		// Update the address ID in the newly created address
		addressData.setId(newAddress.getPk().toString());

		if (makeThisAddressTheDefault)
		{
			getCustomerAccountService().setDefaultAddressEntry(currentCustomer, newAddress);
		}
		return newAddress;
	}

	public AddressModel editBillingAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final CartModel cart = getCart();
		cart.setDeliveryInstructions(addressData.getDeliveryInstructions());
		getModelService().save(cart);
		getModelService().refresh(cart);
		final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
		//addressModel.setRegion(null);

		getModelService().refresh(addressModel);

		getAddressReversePopulator().populate(addressData, addressModel);
		getModelService().save(addressModel);

		getCustomerAccountService().saveAddressEntry(currentCustomer, addressModel);
		if (addressData.isDefaultAddress())
		{
			getCustomerAccountService().setDefaultAddressEntry(currentCustomer, addressModel);
		}
		else if (addressModel.equals(currentCustomer.getDefaultShipmentAddress()))
		{
			getCustomerAccountService().clearDefaultAddressEntry(currentCustomer);
		}
		return addressModel;
	}

	public AddressModel editDeliveryAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);
		if (cartFacade.hasSessionCart())
		{
			final CartModel cart = getCart();
			cart.setDeliveryInstructions(addressData.getDeliveryInstructions());
			getModelService().save(cart);
			getModelService().refresh(cart);
		}
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
		addressModel.setRegion(null);
		getModelService().refresh(addressModel);

		getAddressReversePopulator().populate(addressData, addressModel);
		getModelService().save(addressModel);

		if (addressData.isDefaultAddress())
		{
			getCustomerAccountService().setDefaultAddressEntry(currentCustomer, addressModel);
		}
		else if (addressModel.equals(currentCustomer.getDefaultShipmentAddress()))
		{
			getCustomerAccountService().clearDefaultAddressEntry(currentCustomer);
		}
		return addressModel;

	}

	@Override
	public CartData getCheckoutCart()
	{
		final CartData cartData = getCartFacade().getSessionCart();
		if (cartData != null)
		{
			cartData.setDeliveryAddress(getDeliveryAddress());
			cartData.setDeliveryMode(getDeliveryMode());
			cartData.setRootPaymentInfo(getPaymentInfoDetails());
		}

		return cartData;
	}

	protected PaymentInfoData getPaymentInfoDetails()
	{
		final CartModel cart = getCart();
		if (cart != null)
		{
			final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
			final PaymentInfoData paymentInfoData = paymentInfoConverter.convert(paymentInfo);
			if (paymentInfoData.getBillingAddress() == null && cart.getPaymentAddress() != null)
			{
				paymentInfoData.setBillingAddress(getAddressConverter().convert(cart.getPaymentAddress()));
			}
			return paymentInfoData;
		}

		return null;
	}

	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutCart();
		return (cartData == null || cartData.getRootPaymentInfo() == null);
	}

	public String getDeliveryInstructionFromCart()
	{
		final CartModel cart = getCart();
		if (null != cart)
		{
			return cart.getDeliveryInstructions();
		}
		return null;
	}

	public String getDeliveryModeFromCart()
	{
		final CartModel cart = getCart();
		return cart.getDeliveryMode() == null ? null : cart.getDeliveryMode().getCode();
	}

	public void saveDeliveryInstructionsForDelivery(final String deliveryInstruction)
	{
		if (StringUtils.isNotBlank(deliveryInstruction))
		{
			final CartModel cart = getCart();
			cart.setDeliveryInstructions(deliveryInstruction);
			getModelService().save(cart);
			getModelService().refresh(cart);
		}
	}

	@Override
	public boolean authorizePayment(final String securityCode)
	{
		final CartModel cartModel = getCart();
		if (checkIfCurrentUserIsTheCartUser())
		{
			if (CollectionUtils.isNotEmpty(cartModel.getPaymentTransactions()))
			{
				for (final PaymentTransactionModel paymentTransactionModel : cartModel.getPaymentTransactions())
				{
					if (CollectionUtils.isNotEmpty(paymentTransactionModel.getEntries()))
					{
						for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionModel.getEntries())
						{
							if (StringUtils.isNotBlank(paymentTransactionEntryModel.getTransactionStatus())
									&& TransactionStatus.ACCEPTED.name().equals(paymentTransactionEntryModel.getTransactionStatus()))
							{
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public void checkForFraudOrder() throws ClicksOrderFraudException
	{
		final CartModel cartModel = getCart();
		if (CollectionUtils.isNotEmpty(cartModel.getPaymentTransactions()))
		{
			for (final PaymentTransactionModel paymentTransactionModel : cartModel.getPaymentTransactions())
			{
				if (CollectionUtils.isNotEmpty(paymentTransactionModel.getEntries()))
				{
					for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionModel.getEntries())
					{
						if (StringUtils.isNotBlank(paymentTransactionEntryModel.getTransactionStatus())
								&& TransactionStatus.ACCEPTED.name().equals(paymentTransactionEntryModel.getTransactionStatus())
								&& "Successful".equalsIgnoreCase(paymentTransactionEntryModel.getTransactionStatusDetails()))
						{
							if (paymentTransactionEntryModel.getAmount().doubleValue() - cartModel.getTotalPrice() < 0)
							{
								paymentTransactionEntryModel.setTransactionStatus(TransactionStatus.REJECTED.name());
								modelService.save(paymentTransactionEntryModel);
								cartModel.setBasketEditable(Boolean.TRUE);
								modelService.save(cartModel);
								modelService.refresh(cartModel);
								throw new ClicksOrderFraudException();
							}
						}
					}
				}
			}
		}
	}

	public List<AddressData> getDeliveryAddressList(final List<AddressModel> addresses)
	{
		//final ArrayList<PromotionResultModel> promotionResultModels = getModelService().getAll(promotionsResults,
		//	new ArrayList<PromotionResultModel>());
		return Converters.convertAll(addresses, getAddressConverter());
	}


	/*
	 * public List<PointOfServiceData> getConsolidatedPickupOptions( final StoreFinderSearchPageData<PointOfServiceData>
	 * searchResults, final CartModel cart) { return
	 * Converters.convertAll(clicksCheckoutService.getConsolidatedPickupOptions(searchResults, cart),
	 * getPointOfServiceDistanceConverter()); }
	 */

	public PointOfServiceModel getPosData(final String pickupPointOfServiceName)
	{
		return getPointOfServiceService().getPointOfServiceForName(pickupPointOfServiceName);
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @param provinceList
	 * @return
	 */
	public Collection<ProvinceData> getProvinceList(Collection<ProvinceData> provinceList)
	{
		provinceList = clicksCheckoutDao.getProvinceList(provinceList);
		return provinceList;
	}

	//This condition will execute only for Click & Collect functionality
	public void saveCustomerNameInDeliveryAddress(final String firstName, final String lastName)
	{
		final CartData cartData = getCheckoutCart();
		if (null != cartData.getDeliveryAddress() && null != cartData.getDeliveryAddress().getIsStoreAddress()
				&& cartData.getDeliveryAddress().getIsStoreAddress().booleanValue())
		{
			final AddressData cartDeliveryAddress = cartData.getDeliveryAddress();
			cartDeliveryAddress.setFirstName(firstName);
			cartDeliveryAddress.setLastName(lastName);
			editDeliveryAddress(cartDeliveryAddress);
		}
	}
}
