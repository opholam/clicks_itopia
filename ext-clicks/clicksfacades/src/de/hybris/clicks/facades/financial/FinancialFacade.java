/**
 *
 */
package de.hybris.clicks.facades.financial;

import de.hybris.clicks.facades.data.ClicksFinancialServiceData;

import java.util.List;


/**
 * @author ashish.vyas
 *
 */
public interface FinancialFacade
{
	public List<ClicksFinancialServiceData> getFinancialService();
}
