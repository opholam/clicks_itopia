/**
 *
 */
package de.hybris.clicks.facades.financial.impl;

import de.hybris.clicks.core.financial.FinancialService;
import de.hybris.clicks.core.model.FinancialServiceModel;
import de.hybris.clicks.facades.data.ClicksFinancialServiceData;
import de.hybris.clicks.facades.financial.FinancialFacade;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author ashish.vyas
 *
 */
public class DefaultFinancialFacade implements FinancialFacade
{

	@Resource(name = "financialService")
	FinancialService financialService;

	private Converter<FinancialServiceModel, ClicksFinancialServiceData> clicksFinancialServiceConverter;

	/**
	 * @return the clicksFinancialServiceConverter
	 */
	public Converter<FinancialServiceModel, ClicksFinancialServiceData> getClicksFinancialServiceConverter()
	{
		return clicksFinancialServiceConverter;
	}

	/**
	 * @param clicksFinancialServiceConverter
	 *           the clicksFinancialServiceConverter to set
	 */
	@Required
	public void setClicksFinancialServiceConverter(
			final Converter<FinancialServiceModel, ClicksFinancialServiceData> clicksFinancialServiceConverter)
	{
		this.clicksFinancialServiceConverter = clicksFinancialServiceConverter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.financial.FinancialFacade#getFinancialService()
	 */
	@Override
	public List<ClicksFinancialServiceData> getFinancialService()
	{
		final List<FinancialServiceModel> financialServiceModelList = financialService.getFinancialService();
		if (CollectionUtils.isNotEmpty(financialServiceModelList))
		{
			return Converters.convertAll(financialServiceModelList, getClicksFinancialServiceConverter());
		}
		return (List<ClicksFinancialServiceData>) CollectionUtils.EMPTY_COLLECTION;
	}
}
