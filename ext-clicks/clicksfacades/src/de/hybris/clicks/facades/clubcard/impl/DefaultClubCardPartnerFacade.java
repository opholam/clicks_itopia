/**
 *
 */
package de.hybris.clicks.facades.clubcard.impl;

import de.hybris.clicks.core.clubcard.ClubCardPartnerService;
import de.hybris.clicks.core.model.ClubCardPartnerModel;
import de.hybris.clicks.facades.clubcard.ClubCardPartnerFacade;
import de.hybris.clicks.facades.data.ClubCardPartnerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author ashish.vyas
 *
 */
public class DefaultClubCardPartnerFacade implements ClubCardPartnerFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultClubCardPartnerFacade.class);

	@Resource(name = "clubCardPartnerService")
	ClubCardPartnerService clubCardPartnerService;

	private Converter<ClubCardPartnerModel, ClubCardPartnerData> clubCardPartnerConverter;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facades.clubcard.ClubCardPartnerFacade#getClubCardPartners()
	 */
	/**
	 * @return the clubCardPartnerConverter
	 */
	public Converter<ClubCardPartnerModel, ClubCardPartnerData> getClubCardPartnerConverter()
	{
		return clubCardPartnerConverter;
	}

	/**
	 * @param clubCardPartnerConverter
	 *           the clubCardPartnerConverter to set
	 */
	@Required
	public void setClubCardPartnerConverter(final Converter<ClubCardPartnerModel, ClubCardPartnerData> clubCardPartnerConverter)
	{
		this.clubCardPartnerConverter = clubCardPartnerConverter;
	}

	@Override
	public List<ClubCardPartnerData> getClubCardPartners()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("In getClubCardPartners() of DefaultClubCardPartnerFacade");
		}
		final List<ClubCardPartnerModel> clubCardPartnerModelList = clubCardPartnerService.getClubCardPartners();
		if (CollectionUtils.isNotEmpty(clubCardPartnerModelList))
		{
			return Converters.convertAll(clubCardPartnerModelList, getClubCardPartnerConverter());
		}
		return (List<ClubCardPartnerData>) CollectionUtils.EMPTY_COLLECTION;
	}
}
