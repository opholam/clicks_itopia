/**
 *
 */
package de.hybris.clicks.facades.clubcard.impl;

import de.hybris.clicks.core.clubcard.ClubCardTravelDealService;
import de.hybris.clicks.core.jalo.TravelDeal;
import de.hybris.clicks.core.model.TravelDealModel;
import de.hybris.clicks.facades.clubcard.ClubCardTravelDealFacade;
import de.hybris.clicks.facades.data.ClicksTravelDealData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author siva.reddy
 *
 */
public class ClubCardTravelDealFacadeImpl implements ClubCardTravelDealFacade
{
	@Resource(name = "clubCardTravelDealService")
	private ClubCardTravelDealService clubCardTravelDealService;

	private Converter<TravelDealModel, ClicksTravelDealData> clicksclubCardTravelDealConverter;



	/**
	 * @return the clicksclubCardTravelDealConverter
	 */
	public Converter<TravelDealModel, ClicksTravelDealData> getClicksclubCardTravelDealConverter()
	{
		return clicksclubCardTravelDealConverter;
	}

	/**
	 * @param clicksclubCardTravelDealConverter
	 *           the clicksclubCardTravelDealConverter to set
	 */
	@Required
	public void setClicksclubCardTravelDealConverter(
			final Converter<TravelDealModel, ClicksTravelDealData> clicksclubCardTravelDealConverter)
	{
		this.clicksclubCardTravelDealConverter = clicksclubCardTravelDealConverter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.clubcard.ClubCardTravelDealFacade#getTravelDeals(java.util.Map)
	 */
	@Override
	public List<ClicksTravelDealData> getTravelDeals(final Map<String, Object> params)
	{
		final List<TravelDealModel> travelDealModelList = clubCardTravelDealService.getTravelDeals(params);
		if (CollectionUtils.isNotEmpty(travelDealModelList))
		{
			return Converters.convertAll(travelDealModelList, getClicksclubCardTravelDealConverter());
		}
		return (List<ClicksTravelDealData>) CollectionUtils.EMPTY_COLLECTION;
	}

	/**
	 * @return the clubCardTravelDealService
	 */
	public ClubCardTravelDealService getClubCardTravelDealService()
	{
		return clubCardTravelDealService;
	}

	/**
	 * @param clubCardTravelDealService
	 *           the clubCardTravelDealService to set
	 */
	public void setClubCardTravelDealService(final ClubCardTravelDealService clubCardTravelDealService)
	{
		this.clubCardTravelDealService = clubCardTravelDealService;
	}

	@Override
	public void sendEnquiryEmail(final TravelDeal travelCode, final CustomerData customerData)
	{
		//clubCardTravelDealService.sendEnquiryEmail(params);
	}
}
