/**
 *
 */
package de.hybris.clicks.facades.clubcard;

import de.hybris.clicks.facades.data.ClubCardPartnerData;

import java.util.List;


/**
 * @author ashish.vyas
 *
 */
public interface ClubCardPartnerFacade
{
	public List<ClubCardPartnerData> getClubCardPartners();
}
