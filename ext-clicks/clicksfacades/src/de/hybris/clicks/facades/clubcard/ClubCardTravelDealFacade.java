/**
 *
 */
package de.hybris.clicks.facades.clubcard;

import de.hybris.clicks.core.jalo.TravelDeal;
import de.hybris.clicks.facades.data.ClicksTravelDealData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.List;
import java.util.Map;



/**
 * @author manikandan.gopalan
 *
 */
public interface ClubCardTravelDealFacade
{
	List<ClicksTravelDealData> getTravelDeals(Map<String, Object> params);

	void sendEnquiryEmail(final TravelDeal travelCode, final CustomerData customerData);

}
