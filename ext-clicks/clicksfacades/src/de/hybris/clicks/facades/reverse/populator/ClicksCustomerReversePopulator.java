/**
 *
 */
package de.hybris.clicks.facades.reverse.populator;

import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.platform.commercefacades.user.converters.populator.CustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 *
 */
public class ClicksCustomerReversePopulator extends CustomerReversePopulator
{

	private static final Logger LOG = Logger.getLogger(ClicksCustomerReversePopulator.class);

	@Resource
	FlexibleSearchService flexibleSearchService;

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public void populate(final CustomerData source, final CustomerModel target) throws ConversionException
	{
		try
		{

			super.populate(source, target);
			if (StringUtils.isNotEmpty(source.getUid()))
			{
				target.setUid(source.getUid());
			}

			if (StringUtils.isNotEmpty(source.getFirstName()))
			{
				target.setFirstName(source.getFirstName());
			}

			if (StringUtils.isNotEmpty(source.getPreferedName()))
			{
				target.setName(source.getPreferedName());
			}

			if (StringUtils.isNotEmpty(source.getLastName()))
			{
				target.setLastName(source.getLastName());
			}

			if (null != source.getNonRSA_DOB())
			{
				target.setDateofbirth(source.getNonRSA_DOB());
			}


			final List<AddressModel> addresses = new ArrayList<AddressModel>(2);
			for (final AddressData addressData : source.getAddresses())
			{
				final AddressModel addressModel = new AddressModel();
				populateAddress(addressData, addressModel);
				addressModel.setOwner(target);
				addresses.add(addressModel);
			}

			if (CollectionUtils.isNotEmpty(target.getAddresses()))
			{
				addresses.addAll(target.getAddresses());
			}

			target.setAddresses(addresses);

			if (null != source.getGender())
			{
				if (source.getGender().equals("1"))
				{
					target.setGender(Gender.MALE);
				}
				if (source.getGender().equals("2"))
				{
					target.setGender(Gender.FEMALE);
				}
			}
			if (null != source.getSAResident())
			{
				target.setSAResident(source.getSAResident());
			}
			if (null != source.getRSA_ID())
			{
				target.setRSA_ID(source.getRSA_ID());
			}

			if (null != source.getContactDetails())
			{
				final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
				ContactDetailsModel contactModel;

				for (final ContactDetail contact : source.getContactDetails())
				{
					contactModel = getModelService().create(ContactDetailsModel.class);
					if (null != contact.getNumber())
					{
						contactModel.setNumber(contact.getNumber());
					}
					if (null != contact.getTypeID())
					{
						contactModel.setTypeID(contact.getTypeID());
						if (contact.getTypeID().equalsIgnoreCase("2"))
						{
							contactModel.setType("Mobile");
						}
						if (contact.getTypeID().equalsIgnoreCase("4"))
						{
							contactModel.setType("Other");
						}
					}
					contactModelList.add(contactModel);
				}
				if (contactModelList.size() > 0)
				{
					target.setContactDetails(contactModelList);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in reverse populating customer : " + e.getMessage());
		}


	}

	private AddressModel populateAddress(final AddressData source, final AddressModel target)
	{
		if (null != source.getTypeID())
		{
			target.setTypeID(source.getTypeID());
			if (Integer.parseInt(source.getTypeID()) == 1)
			{
				target.setType("Postal Address");
			}
			if (Integer.parseInt(source.getTypeID()) == 2)
			{
				target.setType("Physical Address");
			}
			if (Integer.parseInt(source.getTypeID()) == 3)
			{
				target.setType("Delivery Address");
			}
		}

		if (null != source.getLine1())
		{
			target.setBuilding(source.getLine1());
		}
		if (null != source.getLine2())
		{
			target.setStreetname(source.getLine2());
		}
		if (null != source.getSuburb())
		{
			target.setStreetnumber(source.getSuburb());
		}
		if (null != source.getPostalCode())
		{
			target.setPostalcode(source.getPostalCode());
		}
		if (null != source.getCity() && null != source.getProvince())
		{
			target.setTown(source.getCity() + ", " + source.getProvince());
		}
		else if (null != source.getCity())
		{
			target.setTown(source.getCity());
		}
		else
		{
			target.setTown(source.getProvince());
		}
		try
		{
			if (null != source.getCountry())
			{
				CountryModel country = new CountryModel();
				getModelService().attach(country);
				country.setIsocode(source.getCountry().getIsocode());
				country = flexibleSearchService.getModelByExample(country);
				if (null != country)
				{
					target.setCountry(country);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in Clicks Reverse populator : " + e.getMessage());
		}

		return target;
	}

}
