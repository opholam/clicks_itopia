/**
 *
 */
package de.hybris.clicks.facades.myaccount.impl;

import de.hybris.clicks.clickscommercewebservices.dto.ContactUs.ClicksContactUsRequestWsDTO;
import de.hybris.clicks.core.account.service.AccountPageService;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.myaccount.MyAccountFacade;
import de.hybris.clicks.facades.product.data.CountryData;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;
import de.hybris.platform.commercefacades.user.data.TopicsData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author ashish.vyas
 *
 */
public class DefaultMyAccountFacade implements MyAccountFacade
{
	private static final Logger LOG = Logger.getLogger(DefaultMyAccountFacade.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "customerAccountService")
	private CustomerAccountService customerAccountService;

	@Resource(name = "accountPageService")
	private AccountPageService accountPageService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource
	FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.myaccount.MyAccountFacade#getRecentOrders()
	 */
	@Override
	public List<OrderHistoryData> getRecentOrders(final int count)
	{
		final OrderStatus[] statuses = {};
		int endIndex = 0;
		final List<OrderHistoryData> ordHis = orderFacade.getOrderHistoryForStatuses(statuses);
		List<OrderHistoryData> orderHiSubsList = new ArrayList<OrderHistoryData>();
		final Comparator<OrderHistoryData> cmp = new Comparator<OrderHistoryData>()
		{
			@Override
			public int compare(final OrderHistoryData o1, final OrderHistoryData o2)
			{
				return o2.getPlaced().compareTo(o1.getPlaced());
			}
		};
		Collections.sort(ordHis, cmp);
		if (CollectionUtils.isNotEmpty(ordHis))
		{
			ordHis.get(0).setTotalOrderCount(String.valueOf(ordHis.size()));
		}
		if (ordHis.size() - count > 0)
		{
			endIndex = count;
		}
		else
		{
			endIndex = ordHis.size();
		}
		try
		{
			orderHiSubsList = ordHis.subList(0, endIndex);
			LOG.info("+++++++++++++Total number of Order returned for load more ----->   " + orderHiSubsList.size()
					+ "+++++++++++++++++");
			return orderHiSubsList;
		}
		catch (final Exception e)
		{
			LOG.error("+++++++++++++Execption thrown while executing sublist function on Order history list+++++++++++++++++");
			return orderHiSubsList;
		}

	}

	public List<OrderHistoryData> loadOrders(final int count)
	{
		LOG.info("+++++++++++++Entered into DefaultMyAccountFacade for loadOrders+++++++++++++++++");
		//Count represents the number of orders already loaded on Order history page
		final OrderStatus[] statuses = {};

		//loadOrdersCount will define how many orders needs to be loaded
		final int loadOrdersCount = Integer.parseInt(Config.getParameter("orderHistory.loadorders.count"));
		List<OrderHistoryData> orderHiSubsList = new ArrayList<OrderHistoryData>();
		int endIndex = 0;
		final List<OrderHistoryData> ordHis = orderFacade.getOrderHistoryForStatuses(statuses);
		LOG.info("+++++++++++++Total number of Order found ----->   " + ordHis.size() + "+++++++++++++++++");
		final Comparator<OrderHistoryData> cmp = new Comparator<OrderHistoryData>()
		{
			@Override
			public int compare(final OrderHistoryData o1, final OrderHistoryData o2)
			{
				return o2.getPlaced().compareTo(o1.getPlaced());
			}
		};
		Collections.sort(ordHis, cmp);
		if (ordHis.size() - (count + loadOrdersCount) > 0)
		{
			endIndex = count + loadOrdersCount;
		}
		else
		{
			endIndex = ordHis.size();
		}
		try
		{
			orderHiSubsList = ordHis.subList(count, endIndex);
			LOG.info("+++++++++++++Total number of Order returned for load more ----->   " + orderHiSubsList.size()
					+ "+++++++++++++++++");
			return orderHiSubsList;
		}
		catch (final Exception e)
		{
			LOG.error("+++++++++++++Execption thrown while executing sublist function on Order history list+++++++++++++++++");
			return orderHiSubsList;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.myaccount.MyAccountFacade#changeUid(java.lang.String, java.lang.String)
	 */
	@Override
	public void changeUid(final String newUid, final String currentPassword) throws DuplicateUidException,
			PasswordMismatchException
	{
		customerAccountService.changeUid(newUid, currentPassword);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.facades.myaccount.MyAccountFacade#fetchCountryData(de.hybris.clicks.facades.product.data.CountryData
	 * )
	 */
	@Override
	public Collection<CountryData> fetchCountryData(Collection<de.hybris.clicks.facades.product.data.CountryData> countryList)
	{
		// YTODO Auto-generated method stub
		countryList = accountPageService.fetchCountryData(countryList);
		return countryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.myaccount.MyAccountFacade#fetchTopicData(java.util.Collection)
	 */
	@Override
	public Collection<TopicsData> fetchTopicData(Collection<TopicsData> topicList, final boolean isMobileAppReq)
	{
		topicList = accountPageService.fetchTopicData(topicList, isMobileAppReq);
		return topicList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.myaccount.MyAccountFacade#fetchProvinceData(java.util.Collection)
	 */
	@Override
	public Collection<ProvinceData> fetchProvinceData(Collection<ProvinceData> provinceList)
	{
		provinceList = accountPageService.fetchProvinceData(provinceList);
		return provinceList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facades.myaccount.MyAccountFacade#populdateContactUsQueryDetails(de.hybris.clicks.
	 * clickscommercewebservices.dto.ContactUs.ClicksContactUsRequestWsDTO)
	 */
	@Override
	public ContactUsMessageData populateContactUsMessageData(final ClicksContactUsRequestWsDTO clicksContactUsRequestWsDTO)
	{
		final ContactUsMessageData contactUsData = new ContactUsMessageData();
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getFirstName()))
		{
			contactUsData.setFirstName(clicksContactUsRequestWsDTO.getFirstName());
		}
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getLastName()))
		{
			contactUsData.setLastName(clicksContactUsRequestWsDTO.getLastName());
		}
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getEmailAddress()))
		{
			contactUsData.setEmailAddress(clicksContactUsRequestWsDTO.getEmailAddress());
		}
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getContactNumber()))
		{
			contactUsData.setContactNumber(clicksContactUsRequestWsDTO.getContactNumber());
		}
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getMemberID()))
		{
			contactUsData.setClubCardNumber(clicksContactUsRequestWsDTO.getMemberID());
		}
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getMessage()))
		{
			contactUsData.setMessage(clicksContactUsRequestWsDTO.getMessage());
		}
		if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getTopicCode()))
		{
			contactUsData.setTopics(clicksContactUsRequestWsDTO.getTopicCode());
		}

		if (clicksContactUsRequestWsDTO.isPharmacyQuery())
		{
			if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getCity()))
			{
				contactUsData.setCity(clicksContactUsRequestWsDTO.getCity());
			}
			if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getSuburb()))
			{
				contactUsData.setSuburb(clicksContactUsRequestWsDTO.getSuburb());
			}
			if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getStreet()))
			{
				contactUsData.setStreet(clicksContactUsRequestWsDTO.getStreet());
			}
			if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getProvinceCode()))
			{
				ProvinceModel provinceModel = modelService.create(ProvinceModel.class);
				provinceModel.setCode(clicksContactUsRequestWsDTO.getProvinceCode());
				try
				{
					provinceModel = flexibleSearchService.getModelByExample(provinceModel);
					contactUsData.setProvince(provinceModel.getName());
				}
				catch (final Exception e)
				{
					LOG.info("Exception while searching province code :" + clicksContactUsRequestWsDTO.getProvinceCode() + "\n" + e);
				}
			}
			if (StringUtils.isNotBlank(clicksContactUsRequestWsDTO.getPostalCode()))
			{
				contactUsData.setPostalCode(clicksContactUsRequestWsDTO.getPostalCode());
			}
		}
		return contactUsData;
	}
}
