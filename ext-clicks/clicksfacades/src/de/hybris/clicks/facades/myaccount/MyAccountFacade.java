/**
 *
 */
package de.hybris.clicks.facades.myaccount;

import de.hybris.clicks.clickscommercewebservices.dto.ContactUs.ClicksContactUsRequestWsDTO;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;
import de.hybris.platform.commercefacades.user.data.TopicsData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;

import java.util.Collection;
import java.util.List;



/**
 * @author ashish.vyas
 *
 */
public interface MyAccountFacade
{
	List<OrderHistoryData> getRecentOrders(int count);

	public void changeUid(String newUid, String currentPassword) throws DuplicateUidException, PasswordMismatchException;

	public Collection<de.hybris.clicks.facades.product.data.CountryData> fetchCountryData(
			Collection<de.hybris.clicks.facades.product.data.CountryData> countryList);

	public Collection<TopicsData> fetchTopicData(Collection<TopicsData> topicList, boolean isMobileAppReq);

	public Collection<ProvinceData> fetchProvinceData(Collection<ProvinceData> provinceList);

	List<OrderHistoryData> loadOrders(int count);

	public ContactUsMessageData populateContactUsMessageData(ClicksContactUsRequestWsDTO clicksContactUsRequestWsDTO);
}
