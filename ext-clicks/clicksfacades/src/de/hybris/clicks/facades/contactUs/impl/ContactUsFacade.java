/**
 *
 */
package de.hybris.clicks.facades.contactUs.impl;

import de.hybris.clicks.core.model.ContactUsMessageModel;
import de.hybris.clicks.core.model.ContactUsProcessModel;
import de.hybris.clicks.core.model.TopicModel;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author shruti.jhamb
 *
 */
public class ContactUsFacade
{

	private static final Logger LOG = Logger.getLogger(ContactUsFacade.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource
	FlexibleSearchService flexibleSearchService;

	@Resource(name = "baseStoreService")
	BaseStoreService baseStoreService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "emailService")
	private EmailService emailService;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "userService")
	private UserService userService;

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Resource
	private ConfigurationService configurationService;



	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * Process contact us.
	 *
	 * @param contactUsData
	 *           the contact us data
	 * @return true, if successful
	 */
	public boolean processContactUs(final ContactUsMessageData contactUsData)
	{
		boolean result = false;
		ContactUsMessageModel contactUsModel = modelService.create(ContactUsMessageModel.class);
		try
		{
			contactUsModel = populateContactUsModel(contactUsData, contactUsModel);
			modelService.save(contactUsModel);
			/*
			 * process for sending email to clicks team
			 */
			processEmail(contactUsModel, contactUsData.getProcessName(), true);
			//processEmail(contactUsModel, contactUsData.getProcessName());
			//processEmail(contactUsModel, contactUsData.getProcessName(), false);
			result = true;
		}
		catch (final ModelSavingException modelexc)
		{
			LOG.error(modelexc);
		}
		catch (final NullPointerException nullexc)
		{
			LOG.error(nullexc);
		}
		return result;
	}



	/**
	 * Process email for starting email process.
	 *
	 * @param contactUsMessageModel
	 *           the contact us message model
	 * @param processName
	 *           the process name
	 * @param customerCare
	 *           the customer care
	 */
	private void processEmail(final ContactUsMessageModel contactUsMessageModel, final String processName,
			final boolean customerCare)
	{
		final String process = processName;
		try
		{
			final ContactUsProcessModel contactUsProcessModel = (ContactUsProcessModel) businessProcessService.createProcess(
					"process" + System.currentTimeMillis(), process);
			contactUsProcessModel.setContactUsMessage(contactUsMessageModel);
			contactUsProcessModel.setLanguage(commonI18NService.getCurrentLanguage());
			contactUsProcessModel.setCurrency(commonI18NService.getCurrentCurrency());
			contactUsProcessModel.setStore(baseStoreService.getCurrentBaseStore());
			contactUsProcessModel.setSite(cmsSiteService.getCurrentSite());
			contactUsProcessModel.setCustomerCareFlag(customerCare);
			modelService.save(contactUsProcessModel);
			businessProcessService.startProcess(contactUsProcessModel);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while starting process for conatct us sending email : " + e);
		}
	}

	/**
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @return the contact us message model
	 */
	/*
	 * Populating and saving data from contact us data to contact us model
	 */
	private ContactUsMessageModel populateContactUsModel(final ContactUsMessageData source, final ContactUsMessageModel target)
	{
		if (null != source.getFirstName())
		{
			target.setFirstName(source.getFirstName());
		}
		if (null != source.getLastName())
		{
			target.setLastName(source.getLastName());
		}
		try
		{
			if (null != source.getEmailAddress())
			{
				target.setEmailAddress(source.getEmailAddress());
			}
		}
		catch (final Exception e)
		{
			LOG.error(e);
		}
		if (null != source.getContactNumber())
		{
			target.setContactNumber(source.getContactNumber());
		}
		if (null != source.getClubCardNumber())
		{
			target.setClubCardNumber(source.getClubCardNumber());
		}
		if (null != source.getMessage())
		{
			target.setMessage(source.getMessage());
		}

		if (StringUtils.isNotBlank(source.getTopics()))
		{
			fetchTopic(source, target);
		}
		target.setCustomerCare(source.isCustomercare());
		/*
		 * if (null != source.getCity()) { target.setCityofappointment(source.getCity()); }
		 */

		//Section that needs to be populated for Mobile APP Pharmacy/Script Topic begins
		if (StringUtils.isNotBlank(source.getCity()))
		{
			target.setCity(source.getCity());
		}
		if (StringUtils.isNotBlank(source.getSuburb()))
		{
			target.setSuburb(source.getSuburb());
		}
		if (StringUtils.isNotBlank(source.getStreet()))
		{
			target.setStreet(source.getStreet());
		}
		if (StringUtils.isNotBlank(source.getProvince()))
		{
			target.setProvince(source.getProvince());
		}
		if (StringUtils.isNotBlank(source.getPostalCode()))
		{
			target.setPostalCode(source.getPostalCode());
		}
		//Section that needs to be populated for Mobile APP Pharmacy/Script Topic ends
		return target;

	}

	private void fetchTopic(final ContactUsMessageData source, final ContactUsMessageModel target)
	{
		String query = "";
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", source.getTopics());
		query = "SELECT {PK} FROM {Topic} where {code} = ?code";
		final SearchResultImpl<TopicModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<TopicModel> result = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(result))
		{
			target.setTopic(result.get(0).getName());
		}
	}
}
