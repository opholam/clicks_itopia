/**
 *
 */
package de.hybris.clicks.address.service;

import de.hybris.platform.commerceservices.address.AddressErrorCode;
import de.hybris.platform.commerceservices.address.AddressFieldType;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.address.AddressVerificationService;
import de.hybris.platform.commerceservices.address.data.AddressFieldErrorData;
import de.hybris.platform.commerceservices.address.data.AddressVerificationResultData;
import de.hybris.platform.commerceservices.address.util.AddressVerificationResultUtils;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author admin
 *
 */
public class ClicksAddressVerificationService implements
		AddressVerificationService<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>>
{
	private BaseStoreService baseStoreService;
	public static final String ACCEPT = "accept";
	public static final String REJECT = "reject";
	public static final String TITLE_CODE = "titleCode";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String ADDRESS_LINE_1 = "addressline1";
	public static final String ADDRESS_LINE_2 = "addressline2";
	public static final String REGION = "region";
	public static final String ZIP_CODE = "zipcode";
	public static final String CITY = "city";
	public static final String COUNTRY = "country";
	public static final String MISSING = "missing";
	public static final String INVALID = "invalid";

	@Override
	public AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> verifyAddress(
			final AddressModel addressModel)
	{
		final AddressVerificationResultData acceptedResult = createVerificationResult();

		validateAddressFields(acceptedResult, addressModel);
		if (AddressVerificationResultUtils.requiresErrorHandling(acceptedResult))
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup("reject"));
		}
		else
		{
			if (StringUtils.isNotBlank(addressModel.getTown()) && addressModel.getTown().equals("review"))
			{
				acceptedResult.setDecision(AddressVerificationDecision.REVIEW);
				final List suggestedAddresses = new ArrayList();
				addressModel.setLine1(String.format("%s corrected", new Object[]
				{ addressModel.getLine1() }));
				suggestedAddresses.add(addressModel);
				acceptedResult.setSuggestedAddresses(suggestedAddresses);
				return acceptedResult;
			}
			acceptedResult.setDecision(AddressVerificationDecision.lookup("accept"));
		}
		return acceptedResult;
	}


	@Override
	public boolean isCustomerAllowedToIgnoreSuggestions()
	{
		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		return ((baseStore != null) && (baseStore.isCustomerAllowedToIgnoreSuggestions()));
	}








	protected void validateAddressFields(
			final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> result,
			final AddressModel address)
	{
		final List errorList = new ArrayList();
		if ((address.getTitle() == null) || ((address.getTitle() != null) && (address.getTitle().getCode() == null)))
		{
			addErrorToVerificationResult("titleCode", "missing", errorList);
		}
		else if (StringUtils.length(address.getTitle().getCode()) > 255)
		{
			addErrorToVerificationResult("titleCode", "invalid", errorList);
		}

		if (StringUtils.isEmpty(address.getFirstname()))
		{
			addErrorToVerificationResult("firstName", "missing", errorList);
		}
		else if (StringUtils.length(address.getFirstname()) > 255)
		{
			addErrorToVerificationResult("firstName", "invalid", errorList);
		}

		if (StringUtils.isEmpty(address.getLastname()))
		{
			addErrorToVerificationResult("lastName", "missing", errorList);
		}
		else if (StringUtils.length(address.getLastname()) > 255)
		{
			addErrorToVerificationResult("lastName", "invalid", errorList);
		}

		if (StringUtils.isEmpty(address.getLine1()))
		{
			addErrorToVerificationResult("addressline1", "missing", errorList);
		}
		else if (StringUtils.length(address.getLine1()) > 255)
		{
			addErrorToVerificationResult("addressline1", "invalid", errorList);
		}

		if ((StringUtils.isNotEmpty(address.getLine2())) && (StringUtils.length(address.getLine2()) > 255))
		{
			addErrorToVerificationResult("addressline2", "invalid", errorList);
		}

		//		if (StringUtils.isEmpty(address.getTown()))
		//		{
		//			addErrorToVerificationResult("city", "missing", errorList);
		//		}
		//		else if (StringUtils.length(address.getTown()) > 255)
		//		{
		//			addErrorToVerificationResult("city", "invalid", errorList);
		//		}

		if ((address.getRegion() != null) && (address.getRegion().getIsocode() == null))
		{
			addErrorToVerificationResult("region", "missing", errorList);
		}
		else if ((address.getRegion() != null) && (StringUtils.length(address.getRegion().getIsocode()) > 255))
		{
			addErrorToVerificationResult("region", "invalid", errorList);
		}

		if (StringUtils.isEmpty(address.getPostalcode()))
		{
			addErrorToVerificationResult("zipcode", "missing", errorList);
		}
		else if (StringUtils.length(address.getPostalcode()) > 10)
		{
			addErrorToVerificationResult("zipcode", "invalid", errorList);
		}

		if ((address.getCountry() == null) || ((address.getCountry() != null) && (address.getCountry().getIsocode() == null)))
		{
			addErrorToVerificationResult("country", "missing", errorList);
		}
		else if (StringUtils.length(address.getCountry().getIsocode()) > 255)
		{
			addErrorToVerificationResult("country", "invalid", errorList);
		}

		result.setFieldErrors(errorList);
	}


	protected void addErrorToVerificationResult(final String titleCode, final String missing,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errors)
	{
		final AddressFieldErrorData errorData = createFieldError();
		errorData.setFieldType(AddressFieldType.lookup(titleCode));
		errorData.setErrorCode(AddressErrorCode.lookup(missing));
		errors.add(errorData);
	}

	protected AddressFieldErrorData<AddressFieldType, AddressErrorCode> createFieldError()
	{
		return new AddressFieldErrorData();
	}

	protected AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> createVerificationResult()
	{
		return new AddressVerificationResultData();
	}

	protected BaseStoreService getBaseStoreService()
	{
		return this.baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}
}
