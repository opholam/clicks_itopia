<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/clickscheckoutaddon/desktop/checkout/multi" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/desktop/address" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>


<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
	<div id="loadMoreStores">
										<script id="loadMoreStoresTemplate" type="text/x-jsrender">
										<div class="store-search-content">
										<div class="store-search-innercontent1">
										<div class="fetch_stores" id="showStores{{:name}}">
										<div class="fetch_stores_item radioOuter pull-left">
										<span class="fetch_stores_head">{{:displayName}}</span>
										<br/>
										{{:address.line1}},{{:address.line2}}
										{{:address.town}},{{:address.postalCode}}
										</div>
										</div>
										<div class="price">
										{{:formattedDistance}},
										</div>
										</div>
										<div class="store-search-innercontent1">
										<div class="storeLinks" id="storeLinks{{:name}}">
										<div class="selectedStore" id="selectedStoreData{{:name}}">
											<a href="#checkoutContentPanel" id="{{:name}}" class="selectStore">Collect from this store</a>
										<div class="changeStore" id="{{:name}}"></div>
										<div class="showStore" id="{{:name}}"></div>
										</div>
										</div>
                    				<div class="storeInfo" id="storeInfo{{:name}}">
								<a href="javascript:void(0);" id="{{:description}}_{{:name}}" class="selectedStoreInfo" data-toggle="modal" data-target="#storeInfo">
								Show more details</a>
								</div>
								</div>
                    		
									</script>
									</div>
	<div id="storeInfo" class="modal fade" role="dialog">
	<script id="storeDetails" type="text/x-jsrender">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
<button type="button" class="btn btn-default close-btn" data-dismiss="modal">X</button>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
	{{:displayName}}
	<p>
		{{if features}}
		{{props features }}
		{{if prop ==  "Store" || if prop=="Clinic" || if prop=="Pharmacy" }} 
		<strong>Facilities</strong>
		{{/if}}
		{{/props}}
				{{/if}} 
	{{if featureAttrList}}
	{{for featureAttrList}}
	{{if icon}}
	<img src="{{:icon.url}}" />
	<span class="blue">{{:feature}} </span>
	{{/if}}
	{{/for}}
	{{/if}}
</p>
<p class="header3">
{{if address}}
				<strong>Address</strong>
				 {{if address.shopNumber}}{{:address.shopNumber}},&nbsp;{{/if}}
					{{if address.centerName}}{{:address.centerName}},&nbsp;{{/if}}
					{{if address.line1}}{{:address.line1}},&nbsp;{{/if}}
					{{if address.line2}}{{:address.line2}},&nbsp;{{/if}}
					{{if address.town}}{{:address.town}},&nbsp;{{/if}}
					{{if address.postalCode}}{{:address.postalCode}}{{/if}}.
				
			{{/if}}
</p>
	</h4>
	</div> 
	<div class="modal-body">
	<!-- <div class="col-md-6 store-map">
				<div class="map-container">
				{{if geoPoint}}
				{{if geoPoint.latitude && geoPoint.longitude}}
				<div class="map active" id="map_canvas" data-latitude = '{{:geoPoint.latitude}}'
			data-longitude = '{{:geoPoint.longitude}}'
			data-stores= '{"id":"0","latitude":"{{:geoPoint.latitude}}","longitude":"{{:geoPoint.longitude}}","name":"<div class=strong>{{:displayName}}</div><div>{{:address.line1}}</div><div>{{:address.line2}}</div><div>{{:address.town}}</div><div>{{:address.postalCode}}</div><div>{{:address.country.name}}</div>"}'></div>
				{{/if}}
				{{/if}}
				</div>
	</div> -->
	<div class="store-hours">
		{{if features}}
		{{props features }}
		{{if prop ==  "Clinic" }} 
		<!-- <h2 class="green">Clinic</h2> -->
		{{else prop ==  "Store" || else prop ==  "Pharmacy" || else prop ==  "Click&Collect"}} 
		<h2 class="blue">Store</h2>
		{{/if}}
		{{/props}}
		{{/if}}
	<div class="store-info">
	<div class="store-individual">
	{{if prop ==  "Clinic"}} 
	{{else}}
	<h3 class="header4">Opening hours</h3>
	<div class="row openingHoursWrap">
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
	{{for openingHoursList}}
	{{if name=='Str' || if name=='Ph' || if name='CC'}}
	{{if weekDayOpeningList}}
	{{for weekDayOpeningList}}
	<dl><dt>{{:weekDay}}</dt>
	<dd>	
		{{if openingTime.formattedHour && closingTime.formattedHour}}
		{{:openingTime.formattedHour}} - {{:closingTime.formattedHour}}
		{{else}}
		Closed
		{{/if}}			
	</dd></dl>
	{{/for}}
	{{/if}}
	{{if specialDayOpeningList}}
	{{for specialDayOpeningList}}
    </div>
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'> 
	<dl><dt>{{:name}}</dt>
	<dd>	
		{{if openingTime.formattedHour && closingTime.formattedHour}}
		{{:openingTime.formattedHour}} - {{:closingTime.formattedHour}}
		{{else}}
		Closed
		{{/if}}			
	</dd></dl>
	{{/for}}
	{{/if}}
	{{/if}}
	{{/for}}
    </div>

    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
	<em>Call to confirm</em>
    </div></div>
    <div class="row openingHoursWrap">
	{{if phoneNumberLists}}
    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
	<h3 class="header4">Contact Details</h3>
    </div>
	{{for phoneNumberLists}}
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
	<div class="contactStore">
	{{if name=='StrTel'}}
	{{if number}}
	{{for number}}	
  <!--  Shop telephone {{:#getIndex()+1}}  {{:#data}} -->
	<div class="contact-num">
	 Shop telephone {{:#getIndex()+1}} {{:#data}} 
	</div>
	{{/for}}
	{{/if}}
	{{/if}}
	{{if name=='StrFax'}}
	{{if number}}
	{{for number}}
  <!--  Shop telephone {{:#getIndex()+1}}  {{:#data}} -->
	<div class="contact-num">
	 Shop Fax {{:#getIndex()+1}} {{:#data}} 
	</div>
	{{/for}}
	{{/if}}
	{{/if}}
	{{if name=='PhTel'}}
	{{if number}}
	{{for number}}
  <!--  Pharmacy telephone {{:#getIndex()+1}}  {{:#data}} -->
	<div class="contact-num">
	 Shop Fax {{:#getIndex()+1}} {{:#data}} 
	</div>
	{{/for}}
	{{/if}}
	{{/if}}
	{{if name=='PhFax'}}
	{{if number}}
	{{for number}}
  <!--  Shop telephone {{:#getIndex()+1}}  {{:#data}} -->
	<div class="contact-num">
	 Pharmacy Fax {{:#getIndex()+1}} {{:#data}} 
	<div>
	{{/for}}
	{{/if}}
	{{/if}}
	</div>
	</div>
	{{/for}}
	{{/if}}
	</div>
	{{/if}}
</div>
</div>
</div>
</div>
	</div>
	
	
	</div>
	</div>
	</script>
	</div>
<!-- <div id="loadDelStores">
hiii
</div>  -->
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
 <jsp:attribute name="pageScripts">
		<sec:authorize ifAllGranted="ROLE_ANONYMOUS">
						         	
		<script type="text/javascript">
		 $(document).ready(function(){
             <c:if test="${editMode != true and edit != true}"> 
 
				$(".deliver_areaForm").css("display", "none");
	    		
	    		$('#addressForm').attr('action', contextPathURL + '/checkout/multi/delivery-address/add');
	    	</c:if>
	    	  <c:if test="${editMode eq true or edit eq true}"> 
	    	  
				$(".deliver_areaForm").css("display", "block");
	    		
	    	</c:if>
    			$(".deliver_areaSearch").slideDown();

         })		
         </script>
         </sec:authorize>
     <sec:authorize ifNotGranted="ROLE_ANONYMOUS">
		<script type="text/javascript">
		 $(document).ready(function(){
             <c:if test="${empty deliveryAddresses}"> 
 
				$(".deliver_areaForm").css("display", "none");
				$(".deliver_areaSearch").slideDown();

	    		$('#addressForm').attr('action', contextPathURL + '/checkout/multi/delivery-address/add');
	    	</c:if>
	    	  <c:if test="${edit eq true}"> 
	    	  
				$(".deliver_areaForm").css("display", "block");
				$(".deliver_areaSearch").slideDown();

	    	</c:if>
	    	<c:if test="${editMode eq true}"> 
			$(".deliver_areaSearch").slideDown();

			$(".deliver_areaForm").css("display", "block");
			$('#addressBook').css("display", "none");
    	</c:if>
         })		
         </script>
         </sec:authorize>
	</jsp:attribute>
	
           <jsp:body>
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<section class="checkoutTop">
		<div class="container">
			<div class="row">
				<%-- <div class="col-md-6 col-sm-7">
					<div class="acc-heading1">
						<div class="secureCheck">Secure checkout</div>
						<h1>Delivery <span>options</span></h1>
					</div>
				</div>
				<div class="col-md-6 col-sm-5">
					<div class="pull-right">
					 <multi-checkout:checkoutProgressBar steps="${checkoutSteps}" progressBarId="${progressBarId}"/> 
					 <!--
						<ul class="checkoutSteps">
						 <li><a class="active" href="#">1</a><div class="checkoutStepsC">Delivery</div></li>
						<li><a href="#">2</a><div class="checkoutStepsC">Payment</div></li>
						<li><a href="#">3</a><div class="checkoutStepsC">Confirmation</div></li> 
						</ul>
					-->
					</div>
				</div> --%>
				<multi-checkout:checkoutProgressBar stepName="Delivery options" steps="${checkoutSteps}" progressBarId="${progressBarId}"/>
			</div>
		</div>
	</section>	
	<c:if test="${gaTrakingCheckout}">
		<input id="gaTrackingChechout" type="hidden" name="gaTrakingCheckout" value="${gaTrakingCheckout}">
	</c:if>
	<!-- Google Analytics tracking Data for delivery page starts -->
			<script>
			<c:forEach items="${cartData.entries}" var="entry">
			<c:set value="${entry.product}" var="product"/>
				ga('ec:addProduct', {                 
				  'id': '${product.code}',                    
				  'name': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" +"${product.sizeVariantTitle}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''), 
				  'brand': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
				  'price': "${product.price.value}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
				  'quantity': "${entry.quantity}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'')
				 });
				</c:forEach>
				ga('ec:setAction', 'checkout', {'step': 2}); 
				ga('send', 'pageview');
			</script>
<!-- Google Analytics tracking Data for delivery page ends -->
	<section class="main-container">
	 <div class="bg-gray clearfix">
	  <div class="container checkout-stage">
	   <div class="row">
	    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	     <div class="bg-white clearfix checkout-confirm-left">
            <div  id="checkoutContentPanel" class="pad-spacing form-wrap clearfix">
            <c:set var="deliveryName" value="${fn:split(deliveryNameList, ',')}" />
			<c:set var="standard" value="${deliveryName[0]}" />
			<c:set var="CC" value="${deliveryName[1]}" />
            <c:choose>
            <c:when test="${not empty deliveryList}">
           <label> Please select your preferred delivery/collection method for this order.</label>
             <c:forEach items="${deliveryList}" var="delivery">
			 <c:if test="${delivery.name eq CC}">
			  <div class="pick-store-box">
			 <cms:pageSlot position="CCDeliveryMethodSlot" var="feature" >
				<cms:component component="${feature}" element="div" class="border-full"/>
				</cms:pageSlot>
				</div>
			 </c:if>
			 <c:if test="${delivery.name eq standard}">
			 <div class="standard-courier-box">
			  <cms:pageSlot position="StandardDeliveryMethodSlot" var="feature" >
				<cms:component component="${feature}" element="div" class="border-full"/>
				</cms:pageSlot>
				</div>
			 </c:if>
			 <div class="standard-courier box">
    		<c:if test="${delivery.name eq standard}">
    		<input id="selected.addr" name="selectedDeliveryMode" type="hidden" value='${delivery.name}'/>
            <div id="addressBook">
            	<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
            
              <c:if test="${not empty deliveryAddresses}"> 
                <c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
                 <c:if test="${not empty deliveryAddress.id and deliveryAddress.visibleInAddressBook eq true and deliveryAddress.id ne editAddressCode}"> 
                 <div class="del-method checkout-slider">
                	<form action="${request.contextPath}/checkout/multi/delivery-address/select" method="GET">
                    <h2>Delivery Address</h2>
                      <div class="checkout-content">
                        <input type="hidden" name="selectedAddressCode" value="${deliveryAddress.id}"/>
                         <input type="hidden" class="selectDeliveryMode" name="selectedDeliveryMode" value="${delivery.code}"/>
                         <input type="hidden" id="editDeliveryForm" value="${edit}"/>
                        <div class="confirm-content"><strong>${fn:escapeXml(deliveryAddress.title)}&nbsp; ${fn:escapeXml(deliveryAddress.firstName)}&nbsp; ${fn:escapeXml(deliveryAddress.lastName)}</strong></div>
                        <div class="confirm-content">${fn:escapeXml(deliveryAddress.line1)}&nbsp; , ${fn:escapeXml(deliveryAddress.line2)}</div>
                        <div class="confirm-content">${fn:escapeXml(deliveryAddress.town)}&nbsp; ${fn:escapeXml(deliveryAddress.postalCode)} &nbsp;,
                        	${fn:escapeXml(deliveryAddress.country.name)}
                          <c:if test="${not empty deliveryAddress.region.name}">&nbsp; ${fn:escapeXml(deliveryAddress.region.name)}</c:if>
                        </div>
                        <button type="submit" class="edit btn positive left" tabindex="${status.count + 21}" onsubmit="$('.selectDeliveryMode').val($('input[ name=\'selectedDeliveryMode\' ]:checked', '#addressForm').val())">
                        <spring:theme code="checkout.multi.deliveryAddress.useThisAddress" text="Use this delivery address"/>
                        </button>
                      </div>
                    </form>
                   <!-- <form:form action="${request.contextPath}/checkout/multi/delivery-address/remove" method="POST">
                      <input type="hidden" name="addressCode" value="${deliveryAddress.id}"/>
                      <button type="submit" class="edit btn negative remove-payment-item left" tabindex="${status.count + 22}">
                      <spring:theme code="checkout.multi.deliveryAddress.remove" text="Remove"/>
                      </button>
                    </form:form> -->
                    <form:form action="${request.contextPath}/checkout/multi/delivery-address/edit" method="GET">
                      <input type="hidden" name="editAddressCode" value="${deliveryAddress.id}"/>
                      <button type="submit" class="edit btn negative remove-payment-item left" tabindex="${status.count + 22}">
                      <spring:theme code="checkout.multi.deliveryAddress.edit" text="Edit"/>
                      </button>
                    </form:form>
                  </div>
                  </c:if>
                </c:forEach>
             
                <a href="javascript:void(0)" class="addNew">Add new delivery address</a> </c:if>   
                
                </sec:authorize>         
                </div>
                <a href="javascript:void(0)" style="display: none" class="addNew">Add new delivery address</a> 
			
    		<div class="deliver_areaSearch">
              <h2 style=${editMode eq true?'display:none':'display:block'}>Delivery Area</h2>
              <p style=${editMode eq true?'display:none':'display:block'}>Enter your postcode to check whether we deliver to your area.</p>
              <address:addressFormSelector supportedCountries="${countries}"
			                             regions="${regions}"
			                             cancelUrl="${currentStepUrl}"
			                             country="${country}"/>
            </div>
            </c:if>
    		</div>
    		<div class="pickup-store box">
    		<c:if test="${delivery.name eq CC}">
    		<input id="selected.store" name="deliveryMode" type="hidden" value='${delivery.name}'/>
				<div class="store_areaSearch delivery" >
						<form id="delAreaForm" onsubmit="return false;" >
								<h3>Choose store to collect order from</h3>
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 paddingNone">								
								<input type="hidden" id="storelocator-latitude" name="latitude" value="${pickUpStoreForm.latitude}"/>
								<input type="hidden" id="storelocator-longitude" name="longitude" value="${pickUpStoreForm.longitude}"/>
								 <input type="hidden" id="storelocator-selectedStore" name="selectedStore" value="${pickUpStoreForm.selectedStore}"/>
								<!--  <input type="hidden" id="storelocator-selectedDeliveryMode" name="selectedDeliveryMode" value="${pickUpStoreForm.selectedDeliveryMode}"/> -->
								<button  id="nearbyStoreSearchButton" class="btn icon_target" type="button"></button>
								<input type="text" class="text  deliversearch" placeholder="Store name, suburb or postcode" name="q" id="storelocator-q" value="${pickUpStoreForm.q}">
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 paddingNone">
								<button type="button" id="storeLocatorSearchButton" class="btn btn-save-change" value="q"> search</button>
								</div>
								</form>
								<div  id="isStoreDiv">
								
								</div>
								<div  id="isSelectedStore">
								
								</div>
								<div id="storeListDiv">
									<c:if test="${not empty deliveryList}">
									<div id="storeListOuterDiv" ><div class="pickupStores">
									<!-- <input type="hidden" id="storelocator-pos" name="posStockList" value="${pickUpStoreForm.pointOfServiceData}"/>  -->
									<c:forEach items="${pickUpStoreForm.pointOfServiceData}" var="pickupStore">
									<div class="fetch_stores">
									<!-- <div class="fetch_stores_item radioOuter pull-left"> -->	
											<!--<form:radiobutton path="selectedStore" cssClass="radio-hide" value="${pickupStore.name}"/> -->
										<div class="resultName">${pickupStore.displayName}</div>
											<div class="resultDistance"><span>${pickupStore.formattedDistance}</span></div>
											<div class="clear_fix address">
												<div class="resultLine1"><c:if test="${not empty pickupStore.address.department}">
											${pickupStore.address.department}
											</c:if>
											<c:if test="${not empty pickupStore.address.apartment}">
											, ${pickupStore.address.apartment}
											</c:if><br/>
											${pickupStore.address.line1}</div>
												<div class="resultLine2">${pickupStore.address.line2}</div>
												<div class="resultTown">${pickupStore.address.town}</div>
												<div class="resultUip">${pickupStore.address.postalCode}</div> 
											</div>	
											<div class="storeInfo">
											<a href="javascript:void(0);" data-toggle="modal" data-target="#storeInfo"></a>
											</div>
											 <div class="loadDelStores">
											<a href="#" class="loadDelStores btn" data-target="loadDelStores"></a>
											</div>  
									</div>
									<div id="selectedStoreData${pickupStore.name}">
									
									</div>
									</c:forEach>
										</div>
										</div>
									</c:if> 
								</div>
								<!-- <div class="loadDelStores" id="loadDelStores_${locationQuery}"><a class="btn">LOAD MORE ${locationQuery}</a></div>  -->
								<div id="isStoreNameSelected">
								<div class='alert negative'>
								Please select a store
								</div>
						</div>
							<div id="storeDeliveryAddressSubmit" class="storeDeliveryAddressSubmit">
							<form:form action="${request.contextPath}/checkout/multi/delivery-address/getStores" method="POST" commandName="pickUpStoreForm">
                      	<div class="checkout-content">
                      	<form:hidden path="posName" />
                      	<form:hidden path="selectedDeliveryMode" value="${CC}"/>
                      <!-- 	<form:hidden path="selectedDeliveryMode" /> -->
                       <!-- <input type="hidden" id="selected-store" value="${selectedStoreId}"/>  -->	
                       	<div class="form-group clearfix"><formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" mandatory="true" /></div>
						<div class="form-group clearfix">	
						<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="text" mandatory="true"/>
						</div>
						<div class="form-group clearfix">
						<formElement:formInputBox idKey="address.alternatePhone" labelKey="address.alternatePhone" path="alternatePhone" inputCSS="text"/>
						</div>
						 <button type="submit" class="delAddressSubmit btn">
						 Continue
                        </button> 
                      </div>
                    </form:form>
							</div>
							</div>
					</c:if>			
    		</div>
    		 </c:forEach>
    		 </c:when>
    		 <c:when test="${not empty storeAddresses}">
    		<div id="addressBook">
                </div>     
                <div class="store_areaSearch delivery" >
               	<form id="delAreaForm" onsubmit="return false;" >
						<h2>Deliver to a store</h2>
								<h3>Choose store to collect order from</h3>
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 paddingNone">								
								<input type="hidden" id="storelocator-latitude" name="latitude" value="${pickUpStoreForm.latitude}"/>
								<input type="hidden" id="storelocator-longitude" name="longitude" value="${pickUpStoreForm.longitude}"/>
								<input type="hidden" id="contextPath" value="${request.contextPath}"/>
								<input id="selected.store" name="deliveryMode" type="hidden" value='${pickUpStoreForm.selectedDeliveryMode}'/>
								 <input type="hidden" id="storelocator-selectedStore" name="selectedStore" value="${pickUpStoreForm.posName}"/>
								<!--  <input type="hidden" id="storelocator-selectedDeliveryMode" name="selectedDeliveryMode" value="${pickUpStoreForm.selectedDeliveryMode}"/> -->
								<button  id="nearbyStoreSearchButton" class="btn icon_target" type="submit"></button>
								<input type="text" class="text  deliversearch" placeholder="Store name, suburb or postcode" name="q" id="storelocator-q" value="${pickUpStoreForm.postcode}">
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 paddingNone">
								<button type="submit" id="storeLocatorSearchButton" class="btn btn-save-change" value="${pickUpStoreForm.posName}"> search</button>
								</div>
					</form>
					<div  id="isStoreDiv">
								
					</div>
					<div id="storeListDiv">
					</div>
					<div  id="isSelectedStore">
								
					</div>
						<div id="storeDeliveryAddressSubmit" class="storeDeliveryAddressSubmit">
							<form:form action="${request.contextPath}/checkout/multi/delivery-address/getStores" method="POST" commandName="pickUpStoreForm">
                      	<div class="checkout-content">
                      	<form:hidden path="posName" />
                      <!-- 	<form:hidden path="selectedDeliveryMode" /> -->
                       <!-- <input type="hidden" id="selected-store" value="${selectedStoreId}"/>  -->	
                       	<div class="form-group clearfix"><formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" mandatory="true" /></div>
						<div class="form-group clearfix">	
						<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="text" mandatory="true"/>
						</div>
						<div class="form-group clearfix">
						<formElement:formInputBox idKey="address.alternatePhone" labelKey="address.alternatePhone" path="alternatePhone" inputCSS="text"/>
						</div>
						 <button type="submit" id="delAddressSubmit" class="delAddressSubmit btn">
						 Continue
                        </button> 
                      </div>
                    </form:form>
							</div>
					<div id="storeDeliveryAddress" class="storeDeliveryAddress">
					<form:form action="${request.contextPath}/checkout/multi/delivery-address/getStores" method="POST" commandName="pickUpStoreForm">
					<form:hidden path="posName" />
                      	<div class="checkout-content">
                      		<div class="fetch_stores">
									<!-- <div class="fetch_stores_item radioOuter pull-left"> -->	
											<!--<form:radiobutton path="selectedStore" cssClass="radio-hide" value="${pickupStore.name}"/> -->
											<div class="del-method">
											<div class="confirm-content">
										<div class="resultName">${pickUpStoreForm.storeDisplayName}</div>
											<div class="clear_fix address">
												<div class="resultLine1"><c:if test="${not empty pickUpStoreForm.department}">
											${pickUpStoreForm.department}
											</c:if>
											<c:if test="${not empty pickUpStoreForm.apartment}">
											, ${pickUpStoreForm.apartment}
											</c:if> <br/>
											${pickUpStoreForm.line1}</div>
												<div class="resultLine2">${pickUpStoreForm.line2}</div>
												<!-- <div class="resultDistance"><span>${pickupStore.formattedDistance}</span></div>  -->
												<div class="resultTown">${pickUpStoreForm.postcode}&nbsp;,${pickUpStoreForm.countryName}</div>
												<div class="resultUip"> 
												<c:if test="${not empty pickUpStoreForm.regionIso}">&nbsp; ${pickUpStoreForm.regionIso}
												</c:if>
												</div> 
											</div>	
											</div>
											</div>
											<input type="hidden" class="text  deliversearch" placeholder="Store name, suburb or postcode" name="q" id="storelocator-q" value="${pickUpStoreForm.postcode}">
									</div>
									<div class="del-option-content1">
									<div class="del-option-content1-left">
									<label class="storeCheckBox"><input type="checkbox" checked="checked" name="myCheckbox" class="myCheckbox" disabled="disabled"/>Collect from this store</label>
									<!-- <div class="showAllStores" id="${posName}">  -->
									<a type="submit" id="changeStoreButton">change</a>
									<div id="selectedStoreData${posName}">
									
							</div></div><div class="del-option-content1-right">
							
											<a href="javascript:void(0);" id="${pickUpStoreForm.q}_${pickUpStoreForm.posName}" class="storePopUp" data-toggle="modal" data-target="#storeInfo">Show more details
											</a></div></div>
											<div id="storeInfo" class="modal fade" role="dialog">
											</div>
                      <!-- 	<form:hidden path="selectedDeliveryMode" /> -->
                       <!--  <input type="hidden" id="selected-store" value="${selectedStoreId}"/>  	 -->
                       	<div class="form-group clearfix"><formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" mandatory="true" /></div>
						<div class="form-group clearfix">	
						<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="text" mandatory="true"/>
						</div>
						<div class="form-group clearfix">
						<formElement:formInputBox idKey="address.alternatePhone" labelKey="address.alternatePhone" path="alternatePhone" inputCSS="text"/>
						</div>
						 <button type="submit" id="delAddressSubmit" class="delAddressSubmit btn">
						 Continue
                        </button> 
                      </div>
                    </form:form>
                    </div>
                    </div>
    		 </c:when>
    		 <c:otherwise>
    		  <div id="addressBook">
            	<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
            
              <c:if test="${not empty deliveryAddresses}"> 
                <c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
                 <c:if test="${not empty deliveryAddress.id and deliveryAddress.visibleInAddressBook eq true and deliveryAddress.id ne editAddressCode}"> 
                 <div class="del-method checkout-slider">
                	<form action="${request.contextPath}/checkout/multi/delivery-address/select" method="GET">
                    <h2>Delivery Address</h2>
                      <div class="checkout-content">
                        <input type="hidden" name="selectedAddressCode" value="${deliveryAddress.id}"/>
                         <input type="hidden" class="selectDeliveryMode" name="selectedDeliveryMode" value="${addressForm.selectedDeliveryMode}"/>
                         <input type="hidden" id="editDeliveryForm" value="${edit}"/>
                         <div class="confirm-content">
											<c:if test="${not empty deliveryAddress.department}">
											${fn:escapeXml(deliveryAddress.department)}
											</c:if>
											<c:if test="${not empty deliveryAddress.apartment}">
											, ${fn:escapeXml(deliveryAddress.apartment)}
											</c:if>
											</div>
                        <div class="confirm-content"><strong>${fn:escapeXml(deliveryAddress.title)}&nbsp; ${fn:escapeXml(deliveryAddress.firstName)}&nbsp; ${fn:escapeXml(deliveryAddress.lastName)}</strong></div>
                        <div class="confirm-content">${fn:escapeXml(deliveryAddress.line1)}&nbsp; , ${fn:escapeXml(deliveryAddress.line2)}</div>
                        <div class="confirm-content">${fn:escapeXml(deliveryAddress.town)}&nbsp; ${fn:escapeXml(deliveryAddress.postalCode)} &nbsp;,
                        	${fn:escapeXml(deliveryAddress.country.name)}
                          <c:if test="${not empty deliveryAddress.region.name}">&nbsp; ${fn:escapeXml(deliveryAddress.region.name)}</c:if>
                        </div>
                        <button type="submit" class="edit btn positive left" tabindex="${status.count + 21}" onsubmit="$('.selectDeliveryMode').val($('input[ name=\'selectedDeliveryMode\' ]:checked', '#addressForm').val())">
                        <spring:theme code="checkout.multi.deliveryAddress.useThisAddress" text="Use this delivery address"/>
                        </button>
                      </div>
                    </form>
                   <!-- <form:form action="${request.contextPath}/checkout/multi/delivery-address/remove" method="POST">
                      <input type="hidden" name="addressCode" value="${deliveryAddress.id}"/>
                      <button type="submit" class="edit btn negative remove-payment-item left" tabindex="${status.count + 22}">
                      <spring:theme code="checkout.multi.deliveryAddress.remove" text="Remove"/>
                      </button>
                    </form:form> -->
                  <!--   <form:form action="${request.contextPath}/checkout/multi/delivery-address/edit" method="GET">
                      <input type="hidden" name="editAddressCode" value="${deliveryAddress.id}"/>
                      <button type="submit" class="edit btn negative remove-payment-item left" tabindex="${status.count + 22}">
                      <spring:theme code="checkout.multi.deliveryAddress.edit" text="Edit"/>
                      </button>
                    </form:form>  -->
                  </div>
                  </c:if>
                </c:forEach>
             
                <a href="javascript:void(0)" class="addNew">Add new delivery address</a> </c:if>   
                
                </sec:authorize>    
                </div>     
                <a href="javascript:void(0)" style="display: none" class="addNew">Add new delivery address</a> 
			<div>
    		<div class="deliver_areaSearch">
              <h2 style=${editMode eq true?'display:none':'display:block'}>Delivery Area</h2>
              <p style=${editMode eq true?'display:none':'display:block'}>Enter your postcode to check whether we deliver to your area.</p>
              <address:addressFormSelector supportedCountries="${countries}"
			                             regions="${regions}"
			                             cancelUrl="${currentStepUrl}"
			                             country="${country}"/>
            </div>
                </div>
    		 </c:otherwise>
    		 </c:choose>
   			 </div>
          </div>
          <address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select"/>
	    </div>
	    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="bg-white clearfix orderSummOuter">
            <multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="true"/>
            <cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
              <cms:component component="${feature}"/>
            </cms:pageSlot>
          </div>
        </div>
	   </div>
	  </div>
	 </div>
	</section>
</jsp:body>

</template:page>




<%-- 
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

	<div id="globalMessages">
		<common:globalMessages/>
	</div>

	<multi-checkout:checkoutProgressBar steps="${checkoutSteps}" progressBarId="${progressBarId}"/>
	<div class="span-14 append-1">
		<div id="checkoutContentPanel" class="clearfix">

			<div class="headline"><spring:theme code="checkout.multi.addressDetails" text="Address Details"/></div>
			<div class="required right"><spring:theme code="form.required" text="Fields marked * are required"/></div>
			<div class="description"><spring:theme code="checkout.multi.addEditform" text="Please use this form to add/edit an address."/></div>

			<address:addressFormSelector supportedCountries="${countries}"
			                             regions="${regions}"
			                             cancelUrl="${currentStepUrl}"
			                             country="${country}"/>

			<c:if test="${not empty deliveryAddresses}">
				<div id="savedAddressListHolder" class="clear">
					<div id="savedAddressList" class="summaryOverlay clearfix">
						<div class="headline">
							<spring:theme code="checkout.multi.deliveryAddress.addressBook" text="Address Book"/>
						</div>
						<div class="addressList">
							<c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
								<div class="addressEntry">
									<form action="${request.contextPath}/checkout/multi/delivery-address/select" method="GET">
										<input type="hidden" name="selectedAddressCode" value="${deliveryAddress.id}"/>
										<ul>
											<li>${fn:escapeXml(deliveryAddress.title)}&nbsp; ${fn:escapeXml(deliveryAddress.firstName)}&nbsp; ${fn:escapeXml(deliveryAddress.lastName)}</li>
											<li>${fn:escapeXml(deliveryAddress.line1)}</li>
											<li>${fn:escapeXml(deliveryAddress.line2)}</li>
											<li>${fn:escapeXml(deliveryAddress.town)}&nbsp; ${fn:escapeXml(deliveryAddress.postalCode)}</li>
											<li>${fn:escapeXml(deliveryAddress.country.name)}<c:if test="${not empty deliveryAddress.region.name}">&nbsp; ${fn:escapeXml(deliveryAddress.region.name)}</c:if></li>
										</ul>
										<button type="submit" class="positive left" tabindex="${status.count + 21}">
											<spring:theme code="checkout.multi.deliveryAddress.useThisAddress" text="Use this delivery address"/>
										</button>
									</form>
									<form:form action="${request.contextPath}/checkout/multi/delivery-address/remove" method="POST">
										<input type="hidden" name="addressCode" value="${deliveryAddress.id}"/>
										<button type="submit" class="negative remove-payment-item left" tabindex="${status.count + 22}">
											<spring:theme code="checkout.multi.deliveryAddress.remove" text="Remove"/>
										</button>
									</form:form>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</div>
		
	</div>
	<multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="false"/>
	<cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot>

</template:page> --%>
