<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/addons/clickscheckoutaddon/desktop/checkout" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/clickscheckoutaddon/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:url value="/payment/payu/init" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>


<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

	<div id="globalMessages">
		<common:globalMessages/>
	</div>

	<section class="checkoutTop">
		<div class="container">
			<multi-checkout:checkoutProgressBar stepName="Confirmation" steps="${checkoutSteps}" progressBarId="${progressBarId}"/>
		</div>
	</section>
	
	<!-- Google Analytics tracking Data for confirmation order summary page starts -->
					<script>
					<c:forEach items="${cartData.entries}" var="entry">
					<c:set value="${entry.product}" var="product"/>
						ga('ec:addProduct', {                 
						  'id': '${product.code}',                    
						  'name': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" +"${product.sizeVariantTitle}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''), 
						  'brand': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
						  'price': "${product.price.value}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
						  'quantity': "${entry.quantity}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'')
						 });
						</c:forEach>
						ga('ec:setAction','checkout', {'step': 4});
					    ga('send', 'pageview');
					</script>	
<!-- Google Analytics tracking Data for confirmation order summary page ends -->

	<!-- <div class="span-14 append-1"> -->
	<section class="main-container">
		<div class="bg-gray clearfix">
			<div class="container checkout-stage">	
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="bg-white clearfix checkout-confirm-left">
						<div class="pad-spacing clearfix">
		<multi-checkout:summaryFlow deliveryAddress="${cartData.deliveryAddress}" deliveryMode="${deliveryMode}" paymentInfo="${paymentInfo}" requestSecurityCode="${requestSecurityCode}" cartData="${cartData}"/>
		<cart:cartPromotions cartData="${cartData}"/>
		<div class="orderSummOuter payMethod">
			<cms:pageSlot position="PaymentTextSlot" var="feature" >
				<cms:component component="${feature}" element="div" class="border-full"/>
				</cms:pageSlot>
				<div class="paymentBtns">
				<cms:pageSlot position="PaymentLogosSlot" var="feature" >
				<cms:component component="${feature}" element="div" class="border-full"/>
				</cms:pageSlot>
								<%-- <div class="border-full"><img src="${themeResourcePath}/clicks/image/checkout/ucount.jpg"></div>
								<div class="border-full"><img src="${themeResourcePath}/clicks/image/checkout/visa.jpg"></div>
								<div class="border-full"><img src="${themeResourcePath}/clicks/image/checkout/master.jpg"></div>
								<div class="border-full"><img src="${themeResourcePath}/clicks/image/checkout/paypal.jpg"></div>
								<div class="border-full"><img src="${themeResourcePath}/clicks/image/checkout/eft.jpg"></div> --%>
				</div>
				</div>
		<form:form action="${placeOrderUrl}" id="placeOrderForm1" commandName="placeOrderForm" method="Get">
			<c:if test="${requestSecurityCode}">
				<form:input type="hidden" class="securityCodeClass" path="securityCode"/>
				<button type="submit" class="positive right pad_right place-order placeOrderWithSecurityCode">
					<spring:theme code="checkout.summary.placeOrder"/>
				</button>
			</c:if>

			<%-- <c:if test="${not requestSecurityCode}">
				<button type="submit" class="positive right place-order">
					<spring:theme code="checkout.summary.placeOrder"/>
				</button>
			</c:if> --%>
			<%-- <div class="terms">
				<form:checkbox id="Terms1" path="termsCheck"/>
				<label for="Terms1"><spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" arguments="${getTermsAndConditionsUrl}"/></label>
			</div> --%>
			<p class="terms">By placing this order, I confirm that I agree with the Clicks '<a href="https://clicks.co.za/legalHelp" target="_blank">Terms &amp; Conditions</a>'.</p>
			<p>You will now be redirected to the secure PayU payment system to complete your order.</p>
			<button class="btn btn-save-change">Confirm &amp; pay now</button>
		</form:form>
		</div>
		</div>
		</div>
	<!-- </div> -->
	
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="bg-white clearfix orderSummOuter">
			<multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="true"/>
		</div>
	</div>	
			

	<cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	</div>
	</div>
	</div>
	</section>
<!-- <script>
ga('ec:setAction','checkout', {'step': 4});
</script> -->
</template:page>
