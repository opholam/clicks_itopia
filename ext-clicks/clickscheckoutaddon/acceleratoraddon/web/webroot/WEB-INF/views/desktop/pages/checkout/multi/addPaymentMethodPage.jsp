<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/clickscheckoutaddon/desktop/checkout/multi" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:url value="${currentStepUrl}" var="choosePaymentMethodUrl"/>
<c:url value="/checkout/multi/payment-method/add" var="nextStepURL"/>
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
 <jsp:attribute name="pageScripts">
<sec:authorize ifAllGranted="ROLE_ANONYMOUS">
						
<script type="text/javascript">

$(document).ready(function(){
	$('#saveBillingAdddress').removeAttr('disabled');
	$(this).is(":checked") ?		$("#paymentDetailsBillingForm").css("display", "none") : 		$("#paymentDetailsBillingForm").css("display", "block");
						
});

</script>
</sec:authorize>
	<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
								<c:choose>
								<c:when test="${ empty customerAddressList}">
<script type="text/javascript">

$(document).ready(function(){
	$('#saveBillingAdddress').removeAttr('disabled');
	$('#billingAddress').is(":checked") ?		$("#paymentDetailsBillingForm").css("display", "none") : 		$("#paymentDetailsBillingForm").css("display", "block");

	$('#billingAddress').click(function(){
		 $(this).is(":checked") ? $("#paymentDetailsBillingForm").css("display", "block"): $("#paymentDetailsBillingForm").css("display", "none")
	})
});

</script>
								</c:when>
								<c:otherwise>
								
<script type="text/javascript">

$(document).ready(function(){

	<c:if test="${customerAddressList.size() eq 1 and edit != true}">	
	$('#saveBillingAdddress').removeAttr('disabled');
	<c:forEach items="${customerAddressList}" var="customerAddress">
	<c:if test="${customerAddress.billingAddress}">	
	$('#${customerAddress.pk}_useBillingAddressDetails').css("display","none");
	$('#billingAddress').prop('checked' , false);
	$('#saveBillingAdddress').click(function(e){
		if($("#paymentDetailsBillingForm").is(':visible')==false){
		e.preventDefault();
		$('#${customerAddress.pk}_useBillingAddressDetails').click();
		}
	})
	</c:if>
	</c:forEach>
	</c:if>
});

</script>
								</c:otherwise>
								</c:choose>
								</sec:authorize>
	</jsp:attribute>
	
           <jsp:body>
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
<div id="wrapper">
	<header> 
		<div class="header">
		<div class="topColor"></div>
			<div class="logo-block logo-checkout">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
					
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<%-- <div class="pull-right backToLink">
							<c:choose>
							<c:when test="${not empty returnToShopping}">
							<a href="${returnToShopping}">Back to shopping</a>
							</c:when>
							<c:otherwise>
							<a href="${contextPath}">Back to shopping</a>
							</c:otherwise>
							</c:choose>
							</div> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- <div class="clearfix bg-white editprofiletop order-top-space">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1>Edit your account details</h1>
				</div>
			</div>
		</div>
	</div>  -->
	<section class="checkoutTop">
		<div class="container">
		
				<div class="row">
<!-- 						<div class="col-md-6 col-sm-7"> -->
<!-- 							<div class="acc-heading1"> -->
<!-- 								<div class="secureCheck">Secure checkout</div> -->
<!-- 								<h1>Payment</h1> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="col-md-6 col-sm-5"> -->
<!-- 							<div class="pull-right"> -->
<!-- 								<ul class="checkoutSteps"> -->
<!-- 								<li><a href="#">1</a><div class="checkoutStepsC">Delivery</div></li> -->
<!-- 								<li><a class="active" href="#">2</a><div class="checkoutStepsC">Payment</div></li> -->
<!-- 								<li><a  href="#">3</a><div class="checkoutStepsC">Confirmation</div></li> -->
<!-- 								</ul> -->
<!-- 							</div> -->
<!-- 						</div> -->
					<multi-checkout:checkoutProgressBar stepName="Billing address" steps="${checkoutSteps}" progressBarId="${progressBarId}"/>

					</div>
		</div>
	</section>	
	<!-- Google Analytics tracking Data for billing page starts -->
					<script>
					<c:forEach items="${cartData.entries}" var="entry">
					<c:set value="${entry.product}" var="product"/>
						ga('ec:addProduct', {                 
						  'id': '${product.code}',                    
						  'name': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" +"${product.sizeVariantTitle}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''), 
						  'brand': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
						  'price': "${product.price.value}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
						  'quantity': "${entry.quantity}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'')
						 });
						</c:forEach>
						ga('ec:setAction','checkout', {'step': 3});
						ga('send', 'pageview');
					</script>
<!-- Google Analytics tracking Data for billing page ends -->
	<section class="main-container">
	<input type="hidden" value="${contextPath}" id="contextPath">
		<div class="bg-gray clearfix">
			<div class="container checkout-stage">	
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<input type="hidden" id="deliveryInstruction" value="${deliveryInstruction}">
									<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
				<c:if test="${not empty customerAddressList}">
					<div id='availableBillingAddress' class="bg-white clearfix checkout-confirm-left">
					<input type="hidden" id="editForm" value="${edit}">
					<input type="hidden" id="hasErrors" value="${hasErrors}">
						<div class="pad-spacing clearfix">
						<h2>Billing Address</h2>
						<c:forEach items="${customerAddressList}" var="customerAddress">
						<c:if test="${customerAddress.billingAddress}">						
							<div class="del-method checkout-slider">
								<div class="checkout-content">
								<div class="confirm-content"><strong>${customerAddress.firstname}&nbsp;${customerAddress.lastname}</strong>
								<input type="hidden" id="${customerAddress.pk}_firstname" value="${customerAddress.firstname}">
								<input type="hidden" id="${customerAddress.pk}_lastname" value="${customerAddress.lastname}">
								<input type="hidden" id="${customerAddress.pk}_email" value="${customerAddress.email}">
								<input type="hidden" id="${customerAddress.pk}_province" value="${customerAddress.province}">							
								</div>
									<div class="confirm-content">
									<c:if test="${not empty customerAddress.streetname}">${customerAddress.streetname},
									<input type="hidden" id="${customerAddress.pk}_streetname" value="${customerAddress.streetname}">
									</c:if> 
									
									<c:if test="${not empty customerAddress.streetnumber}">${customerAddress.streetnumber},
									<input type="hidden" id="${customerAddress.pk}_streetnumber" value="${customerAddress.streetnumber}">
									</c:if> 
									<c:if test="${not empty customerAddress.suburb}">${customerAddress.suburb},
									<input type="hidden" id="${customerAddress.pk}_suburb" value="${customerAddress.suburb}">
									</c:if>
									<c:if test="${not empty customerAddress.town}">${customerAddress.town}
									<input type="hidden" id="${customerAddress.pk}_town" value="${customerAddress.town}">
									</c:if>
									</div>
									<c:if test="${not empty customerAddress.postalcode}">
									<div class="confirm-content">${customerAddress.postalcode}
									<input type="hidden" id="${customerAddress.pk}_postalcode" value="${customerAddress.postalcode}">
									</div>
									</c:if>
									<a href="#" id="${customerAddress.pk}_editBillingAddressDetails">Edit these details</a>
									<a href="#" id="${customerAddress.pk}_useBillingAddressDetails" class="edit btn">Use this address & continue</a>
								</div>
							</div>
							</c:if>
							</c:forEach>
							<a id="displayPaymentBillingAddForm" href="#">Add new billing address</a>
						</div> 
					</div>
					</c:if>
					</sec:authorize>
					<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
								<c:choose>
								<c:when test="${not empty customerAddressList}">
								<c:set var="displayUseAs" value="NO"></c:set>
							</c:when></c:choose>
					</sec:authorize>
					<div class="bg-white clearfix checkout-left">
						<div class="pad-spacing form-wrap clearfix">
							<c:if test="${empty displayUseAs}">
							<c:if test="${cartData.deliveryAddress.visibleInAddressBook}">
							<div class="form-group clearfix">
								<input type="checkbox" name="billingAddress" class="check-hide" id="billingAddress"> 
								<label for="billingAddress" class="check-box">
									<span> Use the delivery address as my billing address </span>
								</label>
							</div>
							</c:if>
							<div class="billinAvailable">
								<div class="del-method">
 									<div id="deliveryName" class="confirm-content">
									<c:if test="${not empty cartData.deliveryAddress.firstName}">
									${cartData.deliveryAddress.firstName}
									</c:if>
									<c:if test="${not empty cartData.deliveryAddress.lastName}">
									&nbsp;${cartData.deliveryAddress.lastName}
									</c:if>
									</div> 
									<c:if test="${not empty cartData.deliveryAddress.streetName}">
									<div id="deliveryLine1" class="confirm-content">${cartData.deliveryAddress.streetName}</div>
									</c:if>
									<c:if test="${not empty cartData.deliveryAddress.streetNumber}">
									<div id="deliveryLine2" class="confirm-content">${cartData.deliveryAddress.streetNumber}</div>
									</c:if>
									<c:if test="${not empty cartData.deliveryAddress.suburb}">
									<div id="deliverySuburb" class="confirm-content">${cartData.deliveryAddress.suburb}</div>
									</c:if>
									<c:if test="${not empty cartData.deliveryAddress.town}">
									<div id="deliveryCity" class="confirm-content">${cartData.deliveryAddress.town}</div>
									</c:if>
									<c:if test="${not empty cartData.deliveryAddress.region && not empty cartData.deliveryAddress.region.name}">
									<div id="deliveryRegion" class="confirm-content">${cartData.deliveryAddress.region.name}</div>
									</c:if>
									<c:if test="${not empty cartData.deliveryAddress.postalCode}">
									<div id="deliveryPostCode" class="confirm-content">${cartData.deliveryAddress.postalCode}</div>
									</c:if>
								</div>
							</div>
							</c:if>
								<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
							
							<c:if test="${(not empty displayUseAs && empty customerAddressList) }">
							<h2>Billing Address</h2>
							</c:if>
							</sec:authorize>
							<sec:authorize ifAllGranted="ROLE_ANONYMOUS">
							<c:if test="${edit eq true or not empty displayUseAs}">
							<h2>Billing Address</h2>
							</c:if>
							</sec:authorize>
							<div <c:if test="${!edit}">style="display:none"</c:if> id="paymentDetailsBillingForm">
							<c:if test="${not empty billingFormTitle}">
							<label>${billingFormTitle}</label>
							</c:if><br/>
							<form:form  method="post" action="${nextStepURL}" commandName="paymentDetailsForm" class="create_update_payment_form">
							<form:hidden path="newBillingAddress"/>
<%-- 							<form:hidden path="paymentId" class="create_update_payment_id"/> --%>
							<form:hidden path="billingAddress.addressId" class="create_update_address_id"/> 
							<div class="nobillinAvailable">
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billingAddress.firstName" inputCSS="text" mandatory="true" tabindex="11"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.surname" labelKey="address.lastName" path="billingAddress.lastName" inputCSS="text" mandatory="true" tabindex="12"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.email" labelKey="address.email" path="billingAddress.email" inputCSS="text" mandatory="true" tabindex="14"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="billingAddress.line1" inputCSS="text" mandatory="true" tabindex="14"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="billingAddress.line2" inputCSS="text" mandatory="false" tabindex="15"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.suburb" labelKey="address.Suburb" path="billingAddress.suburb" inputCSS="text" mandatory="true" tabindex="16"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.townCity" labelKey="address.billing.city" path="billingAddress.townCity" inputCSS="text" mandatory="false" tabindex="16"/>
							</div>
							<div class="form-group clearfix">
								<formElement:formSelectBox idKey="address.billing.province" labelKey="address.billing.province" path="billingAddress.province"  skipBlankMessageKey="form.select.empty"  mandatory="false" items="${province}" />
							</div>
							<div class="form-group clearfix">
								<formElement:formInputBox idKey="address.postcode" labelKey="address.billing.postcode" path="billingAddress.postcode" inputCSS="text" mandatory="true" tabindex="17"/>
							</div>
						
					<form:hidden path="billingAddress.shippingAddress"/>
					<form:hidden path="billingAddress.billingAddress"/>
							</div>
							</form:form>
							</div>
							<c:choose>
							<c:when test="${empty customerAddressList or buttonVal ne 'Continue' or fn:length(customerAddressList) eq 1}"> 
							<c:set var="showButton" value="true" />
							<input type="hidden" value="${showButton}" id="showButtonOnBilling">
							</c:when>
							<c:otherwise>
							<c:set var="showButton" value="false" />
							<input type="hidden" value="${showButton}" id="showButtonOnBilling">
							</c:otherwise>
							</c:choose>
						<button type="submit" class="btn btn-save-change" id="saveBillingAdddress" disabled="disabled">
						<c:choose>
						<c:when test="${buttonVal eq 'Continue'}">Continue</c:when>
						<c:otherwise>Update</c:otherwise>
						</c:choose>
						</button>
						
							<!-- <p>We will create an account so you can checkout faster next time you shop with us.</p>
							<button class="btn btn-save-change">Continue</button> -->
						</div> 
					</div>
				</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="bg-white clearfix orderSummOuter">
			<multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="true"/>
		</div>
	</div>
				</div>
			</div>
		</div>
	</section>
		
<!--<script>
ga('ec:setAction','checkout', {'step': 3});
</script>  -->
</div>
<%-- <div class="span-14 append-1">
		<div id="checkoutContentPanel" class="clea	rfix">
			<div class="headline"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.paymentCard"/></div>
			<div class="required right"><spring:theme code="form.required"/></div>
			<div class="description"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.enterYourCardDetails"/></div>


			<form:form method="post" commandName="paymentDetailsForm" class="create_update_payment_form">
				<div class="cardForm">
					<form:hidden path="paymentId" class="create_update_payment_id"/>
					<formElement:formSelectBox idKey="cardType" labelKey="payment.cardType" path="cardTypeCode" mandatory="true" skipBlank="false" skipBlankMessageKey="payment.cardType.pleaseSelect" items="${cardTypes}" tabindex="1"/>
					<formElement:formInputBox idKey="nameOnCard" labelKey="payment.nameOnCard" path="nameOnCard" inputCSS="text" mandatory="true" tabindex="2"/>
					<formElement:formInputBox idKey="cardNumber" labelKey="payment.cardNumber" path="cardNumber" inputCSS="text" mandatory="true" tabindex="3"/>
					<fieldset id="startDate" class="cardDate">
						<legend><spring:theme code="payment.startDate"/></legend>
						<formElement:formSelectBox idKey="StartMonth" labelKey="payment.month" path="startMonth" mandatory="true" skipBlank="false" skipBlankMessageKey="" items="${months}" tabindex="4"/>
						<formElement:formSelectBox idKey="StartYear" labelKey="payment.year" path="startYear" mandatory="true" skipBlank="false" skipBlankMessageKey="" items="${startYears}" tabindex="5"/>
					</fieldset>
					<fieldset class="cardDate">
						<legend><spring:theme code="payment.expiryDate"/></legend>
						<formElement:formSelectBox idKey="ExpiryMonth" labelKey="payment.month" path="expiryMonth" mandatory="true" skipBlank="false" skipBlankMessageKey="" items="${months}" tabindex="6"/>
						<formElement:formSelectBox idKey="ExpiryYear" labelKey="payment.year" path="expiryYear" mandatory="true" skipBlank="false" skipBlankMessageKey="" items="${expiryYears}" tabindex="7"/>
					</fieldset>
					<div id="issueNum">
						<formElement:formInputBox idKey="payment.issueNumber" labelKey="payment.issueNumber" path="issueNumber" inputCSS="text" mandatory="false" tabindex="8"/>
					</div>
				</div>

				<div class="headline clear"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.billingAddress"/></div>
				<div class="description"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.billingAddressDiffersFromDeliveryAddress"/></div>


				<div>
					<c:if test="${cartData.deliveryItemsQuantity > 0}">
						<form:checkbox id="differentAddress" path="newBillingAddress" tabindex="9"/>
						<label for="differentAddress"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.enterDifferentBillingAddress"/></label>
					</c:if>
				</div>


				<div id="newBillingAddressFields" class="cardForm">
					<form:hidden path="billingAddress.addressId" class="create_update_address_id"/>
					<formElement:formSelectBox idKey="address.title" labelKey="address.title" path="billingAddress.titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}" tabindex="10"/>
					<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billingAddress.firstName" inputCSS="text" mandatory="true" tabindex="11"/>
					<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billingAddress.lastName" inputCSS="text" mandatory="true" tabindex="12"/>
					<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="billingAddress.line1" inputCSS="text" mandatory="true" tabindex="14"/>
					<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="billingAddress.line2" inputCSS="text" mandatory="false" tabindex="15"/>
					<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="billingAddress.townCity" inputCSS="text" mandatory="true" tabindex="16"/>
					<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="billingAddress.postcode" inputCSS="text" mandatory="true" tabindex="17"/>
					<formElement:formSelectBox idKey="address.country" labelKey="address.country" path="billingAddress.countryIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectCountry" items="${billingCountries}" itemValue="isocode" tabindex="18"/>
					<form:hidden path="billingAddress.shippingAddress"/>
					<form:hidden path="billingAddress.billingAddress"/>
				</div>

				<div class="save_payment_details">
					<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
						<form:checkbox id="SaveDetails" path="saveInAccount" tabindex="19"/>
						<label for="SaveDetails"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.savePaymentDetailsInAccount"/></label>
					</sec:authorize>
				</div>


				<div class="form-actions">
					<c:if test="${not hasNoPaymentInfo}">
						<a class="button" href="${choosePaymentMethodUrl}"><spring:theme code="checkout.multi.cancel" text="Cancel"/></a>
					</c:if>
					<ycommerce:testId code="editPaymentMethod_savePaymentMethod_button">
						<button class="positive" tabindex="20" id="lastInTheForm" type="submit">
							<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useThesePaymentDetails"/>
						</button>
					</ycommerce:testId>
				</div>
			</form:form>
		</div>
	</div>
	<multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="true"/>

	<cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot> --%>
</jsp:body>
</template:page>