<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="paymentInfo" required="true" type="de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData" %>
<%@ attribute name="requestSecurityCode" required="true" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set value="${not empty paymentInfo and not empty paymentInfo.billingAddress}" var="billingAddressOk"/>
<%-- <spring:theme code="checkout.summary.paymentMethod.securityCode.whatIsThis.description" var="securityWhatText"/>

<div class="summaryPayment clearfix"  data-security-what-text="${securityWhatText}">
    <ycommerce:testId code="checkout_paymentDetails_text">
            <div class="column append-1">
				 <strong><spring:theme code="checkout.summary.paymentMethod.header" htmlEscape="false"/></strong>
                <ul>
                    <li>${fn:escapeXml(paymentInfo.accountHolderName)}</li>
                    <li>${fn:escapeXml(paymentInfo.cardNumber)}</li>
                    <li>${fn:escapeXml(paymentInfo.cardTypeData.name)}</li>
                    <li><spring:theme code="checkout.summary.paymentMethod.paymentDetails.expires" arguments="${paymentInfo.expiryMonth},${paymentInfo.expiryYear}"/></li>
                </ul>
                <c:if test="${requestSecurityCode}">
				 <form>
					 <div class="control-group security">
						 <label for="SecurityCode"><spring:theme code="checkout.summary.paymentMethod.securityCode"/>*</label>
						 <div class="controls">
							<input type="text" class="text security" id="SecurityCode"/>
							<a href="#" class="security_code_what"><spring:theme code="checkout.summary.paymentMethod.securityCode.whatIsThis"/></a>
						 </div>
					 </div>
				 </form>
                </c:if>
            </div>
 --%>		
            <!-- <div class="column"> -->
                <!-- <ul> -->
                <div class="del-method">
                    <c:if test="${billingAddressOk}">
                        <h2><spring:theme code="checkout.summary.paymentMethod.billingAddress.header"/></h2>
                        <div class="confirm-content">
                                ${fn:escapeXml(paymentInfo.billingAddress.firstName)}&nbsp;${fn:escapeXml(paymentInfo.billingAddress.lastName)}
                        </div>
                        <div class="confirm-content">${fn:escapeXml(paymentInfo.billingAddress.line1)}</div>
                        <div class="confirm-content">${fn:escapeXml(paymentInfo.billingAddress.line2)}</div>
                        <div class="confirm-content">${fn:escapeXml(paymentInfo.billingAddress.suburb)}</div>
                        <div class="confirm-content">${fn:escapeXml(paymentInfo.billingAddress.town)}</div>
                        <c:if test="${not empty paymentInfo.billingAddress.province}">
                        <div class="confirm-content">
                 			<c:forEach items="${province}" var="provinceVal">
							<c:if test="${provinceVal.code eq paymentInfo.billingAddress.province}">
							${fn:escapeXml(provinceVal.name)}
							</c:if>
							</c:forEach>
                        </div>
                        </c:if>
                        <div class="confirm-content">${fn:escapeXml(paymentInfo.billingAddress.postalCode)}</div>
						<div class="confirm-content">${fn:escapeXml(paymentInfo.billingAddress.phone)}</div>                        
                        <ycommerce:testId code="checkout_changePayment_element">
						    <c:url value="/checkout/multi/payment-method/edit?editAddressCode=${paymentInfo.billingAddress.id}" var="addPaymentMethodUrl"/>
					        <a href="${addPaymentMethodUrl}" class="edit"><spring:theme code="checkout.summary.billingAddress.edit" text="Edit Billing Address"/></a>
					    </ycommerce:testId>
                    </c:if>
                     
				</div>                  
                <!-- </ul> -->
            <!-- </div> -->
  <%--   </ycommerce:testId> --%>
   <%--  <ycommerce:testId code="checkout_changePayment_element">
	    <c:url value="/checkout/multi/payment-method/add" var="addPaymentMethodUrl"/>
        <a href="${addPaymentMethodUrl}" class="button positive editButton"><spring:theme code="checkout.summary.billingAddress.edit"/></a>
    </ycommerce:testId> --%>
<!-- </div> -->
