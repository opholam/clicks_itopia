<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="steps" required="true" type="java.util.List" %>
<%@ attribute name="stepName" type="java.lang.String" %>

<%@ attribute name="progressBarId" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="acc-heading1">
						<div class="secureCheck">Secure checkout</div>
						<h1>${stepName}</h1>
					</div>
				</div>
				
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<c:url value="${returnToShopping}" var="homePageURL"/>
<div class="pull-right backToLink">
							<a href="${homePageURL}">Back to shopping</a>
							</div>
<ul data-role="navbar" id="checkoutProgress" class="checkoutSteps">
	<c:forEach items="${steps}" var="checkoutStep" varStatus="status" begin="${checkoutForm? 0 : 1}">
		<c:url value="#" var="stepUrl"/>
		<c:choose>
			<c:when test="${progressBarId eq checkoutStep.progressBarId}">
				<c:set scope="page"  var="currentStepActive"  value="${checkoutStep.stepNumber}"/>
				<li class="active">
					<a href="${stepUrl}" class="active"><spring:theme code="clicks.checkout.multi.${checkoutStep.progressBarId}"/></a>
					<%-- <div class="checkoutStepsC"><spring:theme code="clicks.checkout.multi.${checkoutStep.progressBarId}"/></div> --%>
				</li>
			</c:when>
			<c:when test="${checkoutStep.stepNumber > currentStepActive }">
				<li>
					<a href="${stepUrl}"><spring:theme code="clicks.checkout.multi.${checkoutStep.progressBarId}"/></a>
					<%-- <div class="checkoutStepsC"><spring:theme code="clicks.checkout.multi.${checkoutStep.progressBarId}"/></div> --%>
				</li>
			</c:when>
			<c:otherwise>
				<li>
					<a href="${stepUrl}"><spring:theme code="clicks.checkout.multi.${checkoutStep.progressBarId}"/></a>
					<%-- <div class="checkoutStepsC"><spring:theme code="clicks.checkout.multi.${checkoutStep.progressBarId}"/></div> --%>
				</li>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</ul>
</div>

<%-- <ul data-role="navbar" id="checkoutProgress" class="steps-${fn:length(steps)}">
	<c:forEach items="${steps}" var="checkoutStep" varStatus="status">
		<c:url value="${checkoutStep.url}" var="stepUrl"/>
		<c:choose>
			<c:when test="${progressBarId eq checkoutStep.progressBarId}">
				<c:set scope="page"  var="currentStepActive"  value="${checkoutStep.stepNumber}"/>
				<li class="step active">
					<a href="${stepUrl}">
						<spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
					</a>
				</li>
			</c:when>
			<c:when test="${checkoutStep.stepNumber > currentStepActive }">
				<li class="step disabled">
					<a href="${stepUrl}">
						<spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
					</a>
				</li>
			</c:when>
			<c:otherwise>
				<li class="step visited">
					<a href="${stepUrl}">
						<spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
					</a>
				</li>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</ul> --%>
