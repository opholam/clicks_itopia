<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryMode" required="true" type="de.hybris.platform.commercefacades.order.data.DeliveryModeData" %>
<%@ attribute name="cartData" required="false" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="hasShippedItems" value="${cartData.deliveryItemsQuantity > 0}" />

<div class="del-method">
	<ycommerce:testId code="checkout_deliveryModeData_text">
		<c:if test="${cartData.deliveryItemsQuantity > 0}">
			<!-- <div class="column append-1"> -->
				<h2><spring:theme code="checkout.summary.deliveryMode.header" htmlEscape="false"/></h2>
				<div class="confirm-content">${deliveryMode.name}</div>
				<div class="confirm-content">${deliveryMode.description}</div>
				<div class="confirm-content">${deliveryMode.deliveryCost.formattedValue}</div>
				<ycommerce:testId code="checkout_changeAddress_element">
				<input type="hidden" id="deliveryModeName" name="deliveryModeName" value="${deliveryMode.name}"/>
				    <c:url value="/checkout/multi/delivery-address/editMode" var="editDeliveryModeUrl" />
					<a href="${editDeliveryModeUrl}" class="selectStore edit"><spring:theme code="checkout.summary.deliveryAddress.editDeliveryModeButton" text="Edit delivery mode"/></a>
				</ycommerce:testId>
			<!-- </div> -->
		</c:if>
	</ycommerce:testId>
		
	<c:if test="${cartData.pickupItemsQuantity > 0}">
		<div class="column">
			<strong>&nbsp;</strong>
			<ul>
				<li><spring:theme code="checkout.pickup.items.to.pickup" arguments="${cartData.pickupItemsQuantity}"/></li>
				<li><spring:theme code="checkout.pickup.store.destinations" arguments="${fn:length(cartData.pickupOrderGroups)}"/></li>
			</ul>
		</div>
	</c:if>

	<%-- <c:if test="${cartData.deliveryItemsQuantity > 0}">
		<ycommerce:testId code="checkout_changeDeliveryMode_element">
			<a href="<c:url value="/checkout/multi/delivery-method/choose"/>" class="button positive editButton"><spring:theme code="checkout.summary.edit"/></a>
		</ycommerce:testId>
	</c:if> --%>
</div>