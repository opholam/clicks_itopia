<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryAddress" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="cartData" required="false" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="hasShippedItems" value="${cartData.deliveryItemsQuantity > 0}" />

<!-- <div class="summaryDeliveryAddress"> -->
<div class="del-method">
	<ycommerce:testId code="checkout_deliveryAddressData_text">
		<c:if test="${not hasShippedItems}">
			<spring:theme code="checkout.pickup.no.delivery.required"/>
		</c:if>
		
		<c:if test="${hasShippedItems}">
		<c:choose>
				<c:when test="${deliveryAddress.isStoreAddress eq true}">
					<h2><spring:theme code="checkout.summary.deliveryAddress.header" htmlEscape="false"/></h2>
				<div class="confirm-content">
				<div class="confirm-content">Clicks - ${fn:escapeXml(deliveryAddress.companyName)}</div>
				<c:if test="${deliveryAddress.firstName ne deliveryAddress.companyName}">
				${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
				</c:if></div>
				<div class="confirm-content">
				<c:if test="${not empty deliveryAddress.department}">
					${fn:escapeXml(deliveryAddress.department)}&nbsp;
				</c:if>
				<c:if test="${not empty deliveryAddress.apartment}">
				 ${fn:escapeXml(deliveryAddress.apartment)}
				</c:if>
				</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.line1)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.line2)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.suburb)}</div>
				<c:if test="${not empty deliveryAddress.province}">
				<div class="confirm-content">
				<c:forEach items="${province}" var="provinceVal">
				<c:if test="${provinceVal.code eq deliveryAddress.province}">
				${fn:escapeXml(provinceVal.name)}
				</c:if>
				</c:forEach>
				</div>
				</c:if>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.country.name)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.postalCode)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.phone)}</div>
				
				
				<ycommerce:testId code="checkout_changeAddress_element">
				    <c:url value="/checkout/multi/delivery-address/edit" var="editAddressUrl" />
					<a href="${editAddressUrl}/?editAddressCode=${deliveryAddress.id}" id="${fn:escapeXml(deliveryAddress.storeId)}" class="selectStore edit">Change selected store</a>
				</ycommerce:testId>
				</c:when>
				<c:otherwise>
					<h2><spring:theme code="checkout.summary.deliveryAddress.header" htmlEscape="false"/></h2>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.line1)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.line2)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.suburb)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.town)}</div>
				<c:if test="${not empty deliveryAddress.province}">
				<div class="confirm-content">
				<c:forEach items="${province}" var="provinceVal">
				<c:if test="${provinceVal.code eq deliveryAddress.province}">
				${fn:escapeXml(provinceVal.name)}
				</c:if>
				</c:forEach>
				</div>
				</c:if>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.postalCode)}</div>
				<div class="confirm-content">${fn:escapeXml(deliveryAddress.phone)}</div>
				<ycommerce:testId code="checkout_changeAddress_element">
				    <c:url value="/checkout/multi/delivery-address/edit" var="editAddressUrl" />
					<a href="${editAddressUrl}/?editAddressCode=${deliveryAddress.id}" class="edit"><spring:theme code="checkout.summary.deliveryAddress.editDeliveryAddressButton"/></a>
				</ycommerce:testId>
				</c:otherwise>
				</c:choose>
		</c:if>
	</ycommerce:testId>

	<%-- <c:if test="${hasShippedItems}">
		<ycommerce:testId code="checkout_changeAddress_element">
		    <c:url value="/checkout/multi/delivery-address/edit" var="editAddressUrl" />
			<a href="${editAddressUrl}/?editAddressCode=${deliveryAddress.id}" class="button positive editButton"><spring:theme code="checkout.summary.edit"/></a>
		</ycommerce:testId>
	</c:if> --%>
</div>
<!-- </div> -->