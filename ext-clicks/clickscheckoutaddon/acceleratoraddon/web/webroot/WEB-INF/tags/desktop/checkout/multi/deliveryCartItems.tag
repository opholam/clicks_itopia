<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean" %>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


	<%-- <h2><spring:theme code="checkout.pickup.order.summary"/></h2> --%>
	
		<c:forEach items="${cartData.entries}" var="entry">
			<c:if test="${entry.deliveryPointOfService == null}">
				<c:url value="${entry.product.url}" var="productUrl"/>
			<li>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 paddingNone">	
						<div class="ord-summary-img">
							<a href="${productUrl}">
								<product:productPrimaryImage product="${entry.product}" format="cartIcon"/>
							</a>
						</div>
			<div class="ord-summary-content">
				<h4>${entry.product.brand}</h4>
				<c:if test="${not empty entry.product.name}">
					<p><a  href="${contextPath}${entry.product.url}">${entry.product.name}</a></p>
				</c:if>
				 <c:set var="price" value="${entry.product.price.grossPriceWithPromotionApplied}" />
				 <c:if test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
 						<fmt:formatNumber minFractionDigits="2" var="promoPrice" pattern="####.##" value="${entry.product.price.grossPriceWithPromotionApplied}" />
 					<c:set var="price" value="R${promoPrice}" />
 		</c:if>
		<c:set var="dec" value="${fn:split(price, '.')}" />
		<c:set var="dec1" value="${dec[0]}" />
		<c:set var="dec2" value="${dec[1]}" />

			<c:if test="${empty entry.product.potentialPromotions[0] || entry.product.price.grossPriceWithPromotionApplied eq 0}">
				<c:set var="blue" value="blue" />
			</c:if>


			<small class="blue">${entry.quantity} x </small>
			<c:if test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
			<small class="red">
			 ${dec1}<sup>${dec2}</sup>
			 <c:set var="wasValue" value="Was&nbsp;" />
			</small>
			</c:if>
			<small class="blue">
			<c:if test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
	 		${wasValue}
			</c:if>
			<dateFormat:dateFormat conversionType="rand" input="${entry.product.price.formattedValue}"/></small>
			</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
				
					<%-- <format:price priceData="${entry.basePrice}" displayFreeForZero="true"/> --%>
					<%-- <spring:theme code="basket.page.qty"/>:  ${entry.quantity} --%>
					<div class="ord-summary-content">
					<format:orderPrice product="${entry.product}" priceData="${entry.totalPrice}" displayFreeForZero="true"/>
					</div>
				</div>
			</li>
			
			</c:if>
			
			<script>
				ga('ec:addProduct', {
					'id': "${entry.product.code}",
					'name': "${entry.product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${entry.product.name}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''),
					'brand': "${entry.product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
					'price': "${fn:replace(entry.product.price.formattedValue,',','')}",
					'quantity': "${entry.quantity}"
				});
			</script>
		
		</c:forEach>


<%-- 
<table class="deliveryCartItems">
	<thead>
		<tr>
			<td colspan="4"><spring:theme code="checkout.pickup.items.to.be.delivered"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${cartData.entries}" var="entry">
			<c:if test="${entry.deliveryPointOfService == null}">
				<c:url value="${entry.product.url}" var="productUrl"/>
				<tr>
					<td rowspan="2" class="thumb">
						<a href="${productUrl}">
							<product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
						</a>
					</td>
					<td colspan="3" class="desc">
						<div class="name"><a href="${productUrl}">${entry.product.name}</a></div>
						
						
						
						<c:forEach items="${entry.product.baseOptions}" var="option">
							<c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
								<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
									<div>${selectedOption.name}: ${selectedOption.value}</div>
								</c:forEach>
							</c:if>
						</c:forEach>
						
						
						<c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber) && showPotentialPromotions}">
							<ul class="cart-promotions">
								<c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
									<c:set var="displayed" value="false"/>
									<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
										<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
											<c:set var="displayed" value="true"/>
											<li class="cart-promotions-potential"><span>${promotion.description}</span></li>
										</c:if>
									</c:forEach>
								</c:forEach>
							</ul>
						</c:if>
						
						
						<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
							<ul class="cart-promotions">
								<c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
									<c:set var="displayed" value="false"/>
									<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
										<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
											<c:set var="displayed" value="true"/>
											<li class="cart-promotions-applied"><span>${promotion.description}</span></li>
										</c:if>
									</c:forEach>
								</c:forEach>
							</ul>
						</c:if>
					
					
					
					</td>
				</tr>
				<tr>
					<td class="priceRow"><format:price priceData="${entry.basePrice}" displayFreeForZero="true"/></td>
					<td class="priceRow"><spring:theme code="basket.page.qty"/>:  ${entry.quantity}</td>
					<td class="priceRow"><format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/></td>
				</tr>
			</c:if>
		</c:forEach>
	</thead>
</table> --%>


