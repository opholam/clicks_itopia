/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.checkout.controllers.pages.checkout.steps;

import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.platform.clicksstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(value = "/login/checkout/register")
public class RegisterCustomerCheckoutStepController extends AbstractCheckoutStepController
{
	private final static String REGISTER_CUSTOMER = "register";

	@Resource
	ClicksCheckoutFacade clicksCheckoutFacade;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	@ModelAttribute("checkoutForm")
	public Boolean checkoutForm()
	{
		return Boolean.TRUE;
	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	@PreValidateCheckoutStep(checkoutStep = REGISTER_CUSTOMER)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		sessionService.setAttribute("registration", Boolean.TRUE);

		return "forward:/register/registerCustomer";
	}

	@RequestMapping(value = "/newcustomer", method = RequestMethod.POST)
	public String doRegister(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
					throws CMSItemNotFoundException
	{
		return "forward:/register/newcustomer";
	}


	// -------- Enroll for clubcard-------------
	@RequestMapping(value = "/registerCustomer", method = RequestMethod.GET)
	public String getCustomerLayout(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		return "forward:/register/registerCustomer";
	}



	@RequestMapping(value = "/getEnrollForm", method = RequestMethod.GET)
	String enrollRequest(final Model model, final HttpServletRequest request, final ClicksRegisterForm form,
			final boolean isJoinCC) throws CMSItemNotFoundException
	{
		return "forward:/register/getEnrollForm";
	}


	@SuppressWarnings("boxing")
	@RequestMapping(value = "/joincc", method = RequestMethod.GET)
	@RequireHardLogIn
	String joinClubCardRequestForm(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		model.addAttribute("checkoutForm", Boolean.TRUE);
		return "forward:/register/joincc";
	}

	@RequestMapping(value = "/joincc", method = RequestMethod.POST)
	public String joinClubCardSubmitForm(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
					throws CMSItemNotFoundException
	{

		return "forward:/register/joincc";
	}


	@RequestMapping(value = "/createAccount", method = RequestMethod.GET)
	public String createAccount(final Model model) throws CMSItemNotFoundException
	{
		return "forward:/register/createAccount";
	}


	@RequestMapping(value = "/enroll", method = RequestMethod.POST)
	public String enrollCustomer(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
					throws CMSItemNotFoundException
	{
		return "forward:/register/enroll";
	}

	// -------- Register Clubcard customer-------------
	@RequestMapping(value = "/registerCC", method = RequestMethod.POST)
	public String registerClubCardCustomer(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
					throws CMSItemNotFoundException
	{
		model.addAttribute("checkoutForm", Boolean.TRUE);
		return "forward:/register/registerCC";
	}

	// -------- Register Non Clubcard customer -------------
	@RequestMapping(value = "/registerNonCC", method = RequestMethod.POST)
	public String registerNonClubCardCustomer(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
					throws CMSItemNotFoundException
	{
		model.addAttribute("checkoutForm", Boolean.TRUE);
		return "forward:/register/registerNonCC";
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}


	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(REGISTER_CUSTOMER);
	}

}
