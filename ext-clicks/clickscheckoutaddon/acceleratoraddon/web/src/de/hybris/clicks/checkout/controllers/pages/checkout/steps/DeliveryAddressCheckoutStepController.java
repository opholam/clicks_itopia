/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.checkout.controllers.pages.checkout.steps;

import de.hybris.clicks.checkout.controllers.ControllerConstants;
import de.hybris.clicks.facade.cartPage.CartPageFacade;
import de.hybris.clicks.facade.delivery.stores.DeliveryAddressFacade;
import de.hybris.clicks.facades.populators.ClicksAddressReversePopulator;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.clicks.facades.storefinder.pickupInStore.ClicksDefaultStoreFinderStockFacade;
import de.hybris.clicks.storefront.controllers.validators.PickUpStoreAddressValidator;
import de.hybris.clicks.storefront.forms.PickUpStoreForm;
import de.hybris.clicks.storefront.store.populator.ClicksDeliveryAddressPopulator;
import de.hybris.platform.acceleratorfacades.customerlocation.CustomerLocationFacade;
import de.hybris.platform.clicksstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.AddressForm;
import de.hybris.platform.clicksstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;
import de.hybris.platform.commercefacades.storefinder.StoreFinderStockFacade;
import de.hybris.platform.commercefacades.storefinder.data.StoreFinderStockSearchPageData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceStockData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.StoreFinderService;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class DeliveryAddressCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOG = Logger.getLogger(DeliveryAddressCheckoutStepController.class);
	private final static String DELIVERY_ADDRESS = "delivery-address";

	public static final int MAX_PAGE_LIMIT = 100;


	public static enum ShowMode
	{
		Page, All
	}


	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "clicksAddressReversePopulator")
	ClicksAddressReversePopulator clicksAddressReversePopulator;

	@Resource(name = "pickUpStoreAddressValidator")
	private PickUpStoreAddressValidator pickUpStoreAddressValidator;

	@Resource(name = "customerLocationFacade")
	private CustomerLocationFacade customerLocationFacade;

	@Resource
	private ModelService modelService;

	@Resource(name = "storeFinderStockFacade")
	protected StoreFinderStockFacade<PointOfServiceStockData, StoreFinderStockSearchPageData<PointOfServiceStockData>> storeFinderStockFacade;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource(name = "deliveryAddressFacade")
	private DeliveryAddressFacade deliveryAddressFacade;

	@Resource
	private StoreFinderService<PointOfServiceDistanceData, StoreFinderSearchPageData<PointOfServiceDistanceData>> storeFinderService;

	@Resource
	private CartService cartService;

	@Resource
	private ClicksDeliveryAddressPopulator clicksDeliveryAddressPopulator;

	@Resource(name = "cartPageFacade")
	private CartPageFacade cartPageFacade;

	@Resource(name = "clicksDefaultStoreFinderStockFacade")
	private ClicksDefaultStoreFinderStockFacade clicksDefaultStoreFinderStockFacade;

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		final StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringtrimmer);
	}

	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		provinceList = clicksCheckoutFacade.getProvinceList(provinceList);
		return provinceList;
	}

	//private static final String EDIT_MODE_PATH_VARIABLE_PATTERN = "{deliveryModeName:.*}";

	/*
	 * Enter step for delivery method selection page
	 */
	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		//String editDelMode=
		final String deliveryNameVal = Config.getParameter("delivery.name.value");
		//final String[] deliveryNameList = deliveryNameVal.split(",");
		model.addAttribute("deliveryNameList", deliveryNameVal);
		PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();
		if (cartService.hasSessionCart())
		{
			final CartData cart = clicksCheckoutFacade.getCheckoutCart();
			final AddressData deliveryAdd = cart.getDeliveryAddress();
			AddressData tempAddr = null;
			List<ZoneDeliveryModeData> deliveryList = new ArrayList<ZoneDeliveryModeData>();
			if (deliveryAdd == null)
			{
				/*
				 * Creating a temporary address
				 */
				tempAddr = new AddressData();
				final CountryData country = new CountryData();
				country.setIsocode("ZA");
				country.setName("South Africa");
				tempAddr.setCountry(country);
				tempAddr.setShippingAddress(true);
				tempAddr.setBillingAddress(false);
				tempAddr.setDefaultAddress(true);
				tempAddr.setVisibleInAddressBook(false);
				getUserFacade().addAddress(tempAddr);
				getCheckoutFacade().setDeliveryAddress(tempAddr);
				cart.setDeliveryAddress(tempAddr);
			}
			/*
			 * fetch the delivery modes
			 */
			deliveryList = (List<ZoneDeliveryModeData>) getCheckoutFacade().getSupportedDeliveryModes();

			if (deliveryList.size() > 0)
			{
				model.addAttribute("deliveryList", deliveryList);
				model.addAttribute("standardDelivery", Config.getParameter("delivery.standard.mode"));
				model.addAttribute("ClickCollect", Config.getParameter("delivery.pickup.mode"));
				if ((deliveryAdd != null && deliveryAdd.getId() != null) && null != cart.getDeliveryMode()
						&& deliveryAdd.isVisibleInAddressBook())
				{
					return getCheckoutStep().nextStep();
				}
				//else if (cart.getDeliveryMode() == null)
				//{
				//model.addAttribute("deliveryFeeMessage", Config.getParameter("delivery.options.delivery.fee.message"));
				//}
				model.addAttribute("deliveryFeeMessage", Config.getParameter("delivery.options.delivery.fee.message"));
				deliveryAddressFacade.getCartData(cart, model);
				model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
				final CartData cartData = cart;
				model.addAttribute("cartData", cartData);
				//removing temporary address
				if (null != tempAddr)
				{
					getUserFacade().removeAddress(tempAddr);
				}
				/*
				 * fetch current customer shipping address list
				 */
				final CustomerModel currentCheckoutCus = clicksCheckoutFacade.getCustomerModel();
				final List<AddressModel> customerAddressList = new ArrayList<>();
				deliveryAddressFacade.fetchCustAddressList(currentCheckoutCus, customerAddressList);
				model.addAttribute("deliveryAddresses", clicksCheckoutFacade.getDeliveryAddressList(customerAddressList));
				model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));

				/*
				 * populate pick up form
				 */
				pickUpStoreForm = clicksDeliveryAddressPopulator.populatePickUpInStoreForm(pickUpStoreForm, cartData);
				model.addAttribute("pickUpStoreForm", pickUpStoreForm);
				/*
				 * populate address form
				 */
				AddressForm addressForm = new AddressForm();
				addressForm = clicksDeliveryAddressPopulator.populateAddressForm(addressForm, cartData);
				model.addAttribute("addressForm", addressForm);
				model.addAttribute("showSaveToAddressBook", Boolean.TRUE);
				prepareDataForPage(model);
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				model.addAttribute(WebConstants.BREADCRUMBS_KEY,
						getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
				model.addAttribute("metaRobots", "noindex,nofollow");
				setCheckoutStepLinksForModel(model, getCheckoutStep());
				model.addAttribute("gaTrakingCheckout", sessionService.getAttribute("gaTrakingCR"));
				sessionService.removeAttribute("gaTrakingCR");


			}

		}
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/editMode", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String redirectEditDeliveryMode(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		//String editDelMode=
		final String deliveryNameVal = Config.getParameter("delivery.name.value");
		//final String[] deliveryNameList = deliveryNameVal.split(",");
		model.addAttribute("deliveryNameList", deliveryNameVal);
		PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();

		if (cartService.hasSessionCart())
		{
			final CartData cart = clicksCheckoutFacade.getCheckoutCart();
			final AddressData deliveryAdd = cart.getDeliveryAddress();
			AddressData tempAddr = null;
			List<ZoneDeliveryModeData> deliveryList = new ArrayList<ZoneDeliveryModeData>();
			if (deliveryAdd == null)
			{
				/*
				 * Creating a temporary address
				 */
				tempAddr = new AddressData();
				final CountryData country = new CountryData();
				country.setIsocode("ZA");
				country.setName("South Africa");
				tempAddr.setCountry(country);
				tempAddr.setShippingAddress(true);
				tempAddr.setBillingAddress(false);
				tempAddr.setDefaultAddress(true);
				tempAddr.setVisibleInAddressBook(false);
				getUserFacade().addAddress(tempAddr);
				getCheckoutFacade().setDeliveryAddress(tempAddr);
				cart.setDeliveryAddress(tempAddr);
			}
			/*
			 * fetch the delivery modes
			 */
			deliveryList = (List<ZoneDeliveryModeData>) getCheckoutFacade().getSupportedDeliveryModes();

			if (deliveryList.size() > 0)
			{
				model.addAttribute("deliveryList", deliveryList);
				model.addAttribute("standardDelivery", Config.getParameter("delivery.standard.mode"));
				model.addAttribute("ClickCollect", Config.getParameter("delivery.pickup.mode"));
				//if ((deliveryAdd != null && deliveryAdd.getId() != null) && null != cart.getDeliveryMode())
				//{
				//return getCheckoutStep().nextStep();
				//}
				if (cart.getDeliveryMode() == null)
				{
					model.addAttribute("deliveryFeeMessage", Config.getParameter("delivery.options.delivery.fee.message"));
				}
				deliveryAddressFacade.getCartData(cart, model);
				model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
				final CartData cartData = cart;
				model.addAttribute("cartData", cartData);
				//removing temporary address
				if (null != tempAddr)
				{
					getUserFacade().removeAddress(tempAddr);
				}
				/*
				 * fetch current customer shipping address list
				 */
				final CustomerModel currentCheckoutCus = clicksCheckoutFacade.getCustomerModel();
				final List<AddressModel> customerAddressList = new ArrayList<>();
				deliveryAddressFacade.fetchCustAddressList(currentCheckoutCus, customerAddressList);
				model.addAttribute("deliveryAddresses", clicksCheckoutFacade.getDeliveryAddressList(customerAddressList));
				model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));

				/*
				 * populate pick up form
				 */
				pickUpStoreForm = clicksDeliveryAddressPopulator.populatePickUpInStoreForm(pickUpStoreForm, cartData);
				model.addAttribute("pickUpStoreForm", pickUpStoreForm);
				/*
				 * populate address form
				 */
				AddressForm addressForm = new AddressForm();
				addressForm = clicksDeliveryAddressPopulator.populateAddressForm(addressForm, cartData);
				model.addAttribute("addressForm", addressForm);
				model.addAttribute("showSaveToAddressBook", Boolean.TRUE);
				prepareDataForPage(model);
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				model.addAttribute(WebConstants.BREADCRUMBS_KEY,
						getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
				model.addAttribute("metaRobots", "noindex,nofollow");
				setCheckoutStepLinksForModel(model, getCheckoutStep());
				model.addAttribute("gaTrakingCheckout", sessionService.getAttribute("gaTrakingCR"));
				sessionService.removeAttribute("gaTrakingCR");


			}

		}
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	/*
	 * Adding a delivery address for home delivery
	 */

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();
		model.addAttribute("pickUpStoreForm", pickUpStoreForm);

		getAddressValidator().validate(addressForm, bindingResult);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		this.prepareDataForPage(model);
		if (StringUtils.isNotBlank(addressForm.getCountryIso()) && StringUtils.isAlpha(addressForm.getCountryIso()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
		}

		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("showSaveToAddressBook", Boolean.TRUE);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
			model.addAttribute("edit", Boolean.TRUE);
			model.addAttribute("addressForm", addressForm);
			model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}
		AddressData newAddress = new AddressData();
		/*
		 * populate address data
		 */
		newAddress = clicksDeliveryAddressPopulator.populateAddressData(newAddress, addressForm);

		if (addressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
			if (addressForm.getSaveInAddressBook().booleanValue() && getUserFacade().isAddressBookEmpty())
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
		// Verify the address data.
		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		if (addressRequiresReview)
		{
			model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		getUserFacade().addAddress(newAddress);

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{
			// removing temporary address
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		// Set the new address as the selected checkout delivery address
		clicksCheckoutFacade.saveDeliveryInstructionsForDelivery(addressForm.getDeliveryInstructions());
		if (Boolean.TRUE.equals(addressForm.getBillingAddress()))
		{
			clicksCheckoutFacade.saveDeliveryAsBillingAddress();
		}
		return getCheckoutStep().nextStep();
	}

	/*
	 * modify or change delivery address in the cart
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode") final String editAddressCode, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}

		AddressData addressData = null;
		boolean hasAddressData = false;

		if (StringUtils.isNotEmpty(editAddressCode))
		{
			hasAddressData = true;

			addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
		}
		else
		{
			addressData = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		}
		if (null != addressData.getIsStoreAddress() && addressData.getIsStoreAddress().booleanValue())
		{
			/*
			 * populating form data for pick up in store delivery
			 */
			PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();
			pickUpStoreForm = clicksDeliveryAddressPopulator.populateInstoreForm(pickUpStoreForm, addressData);
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			final List<ZoneDeliveryModeData> zoneDeliveryList = (List<ZoneDeliveryModeData>) getCheckoutFacade()
					.getSupportedDeliveryModes();
			model.addAttribute("deliveryModes", zoneDeliveryList);
			if (!zoneDeliveryList.isEmpty())
			{
				pickUpStoreForm.setSelectedDeliveryMode(clicksCheckoutFacade.getDeliveryModeFromCart() == null ? zoneDeliveryList
						.iterator().next().getCode() : clicksCheckoutFacade.getDeliveryModeFromCart());

			}
			model.addAttribute("cartData", cartData);
			final CustomerModel currentCheckoutCus = clicksCheckoutFacade.getCustomerModel();
			final List<AddressModel> customerAddressList = new ArrayList<>();
			for (final AddressModel address : currentCheckoutCus.getAddresses())
			{
				if (Boolean.TRUE.equals(address.getShippingAddress()) && Boolean.FALSE.equals(address.getVisibleInAddressBook()))
				{
					customerAddressList.add(address);
				}
			}
			model.addAttribute("storeAddresses", clicksCheckoutFacade.getDeliveryAddressList(customerAddressList));
			if (StringUtils.isNotBlank(pickUpStoreForm.getCountryIso()))
			{
				model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(pickUpStoreForm.getCountryIso()));
				model.addAttribute("country", pickUpStoreForm.getCountryIso());
			}
			model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			model.addAttribute("edit", Boolean.valueOf(hasAddressData));
			model.addAttribute("editMode", Boolean.valueOf(!hasAddressData));
			model.addAttribute("editAddressCode", editAddressCode);
			model.addAttribute("pickUpStoreForm", pickUpStoreForm);

		}
		else
		{
			/*
			 * populating form data for home delivery
			 */
			AddressForm addressForm = new AddressForm();
			addressForm = clicksDeliveryAddressPopulator.editPopulateAddressForm(addressForm, addressData);
			if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
			{
				addressForm.setRegionIso(addressData.getRegion().getIsocode());
			}

			final List<ZoneDeliveryModeData> zoneDeliveryList = (List<ZoneDeliveryModeData>) getCheckoutFacade()
					.getSupportedDeliveryModes();

			model.addAttribute("deliveryModes", zoneDeliveryList);
			if (!zoneDeliveryList.isEmpty())
			{
				addressForm.setSelectedDeliveryMode(clicksCheckoutFacade.getDeliveryModeFromCart() == null ? zoneDeliveryList
						.iterator().next().getCode() : clicksCheckoutFacade.getDeliveryModeFromCart());

			}
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute("cartData", cartData);
			final CustomerModel currentCheckoutCus = clicksCheckoutFacade.getCustomerModel();
			final List<AddressModel> customerAddressList = new ArrayList<>();
			for (final AddressModel address : currentCheckoutCus.getAddresses())
			{
				if (Boolean.TRUE.equals(address.getShippingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook()))
				{
					customerAddressList.add(address);
				}
			}
			model.addAttribute("deliveryAddresses", clicksCheckoutFacade.getDeliveryAddressList(customerAddressList));
			if (StringUtils.isNotBlank(addressForm.getCountryIso()))
			{
				model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
				model.addAttribute("country", addressForm.getCountryIso());
			}
			model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			model.addAttribute("edit", Boolean.valueOf(hasAddressData));
			model.addAttribute("editMode", Boolean.valueOf(!hasAddressData));
			model.addAttribute("editAddressCode", editAddressCode);
			model.addAttribute("addressForm", addressForm);
		}

		model.addAttribute("showSaveToAddressBook", Boolean.valueOf(!addressData.isVisibleInAddressBook()));

		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String edit(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (addressForm.getAddressId() != null)
		{
			getAddressValidator().validate(addressForm, bindingResult);
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute("cartData", cartData);
			model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
			if (StringUtils.isNotBlank(addressForm.getCountryIso()))
			{
				model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
				model.addAttribute("country", addressForm.getCountryIso());
			}
			model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			if (bindingResult.hasErrors())
			{
				GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
				List<ZoneDeliveryModeData> zoneDeliveryList = new ArrayList<ZoneDeliveryModeData>();
				zoneDeliveryList = (List<ZoneDeliveryModeData>) getCheckoutFacade().getSupportedDeliveryModes();
				model.addAttribute("deliveryModes", zoneDeliveryList);
				model.addAttribute("edit", Boolean.TRUE);
				model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
			}

			AddressData newAddress = new AddressData();
			newAddress = clicksDeliveryAddressPopulator.editPopulateAddressData(newAddress, addressForm);
			newAddress.setDefaultAddress(getUserFacade().isAddressBookEmpty() || getUserFacade().getAddressBook().size() == 1
					|| Boolean.TRUE.equals(addressForm.getDefaultAddress()));

			// Verify the address data.
			final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
					.verifyAddressData(newAddress);
			final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
					model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
					"checkout.multi.address.updated");

			if (addressRequiresReview)
			{
				if (StringUtils.isNotBlank(addressForm.getCountryIso()))
				{
					model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
					model.addAttribute("country", addressForm.getCountryIso());
				}
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));

				if (StringUtils.isNotEmpty(addressForm.getAddressId()))
				{
					final AddressData addressData = getCheckoutFacade().getDeliveryAddressForCode(addressForm.getAddressId());
					if (addressData != null)
					{
						model.addAttribute("showSaveToAddressBook", Boolean.valueOf(!addressData.isVisibleInAddressBook()));
						model.addAttribute("edit", Boolean.TRUE);
					}
				}

				return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
			}
			clicksCheckoutFacade.editDeliveryAddress(newAddress);
			clicksCheckoutFacade.setDeliveryAddress(newAddress);
		}
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/remove", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@RequestParam("addressCode") final String addressCode, final RedirectAttributes redirectModel,
			final Model model) throws CMSItemNotFoundException
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		getUserFacade().removeAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute("addressForm", new AddressForm());

		return getCheckoutStep().currentStep();
	}


	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils.commaDelimitedListToSet(Config
				.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = new AddressData();
		selectedAddress.setId(addressForm.getAddressId());
		selectedAddress.setTitleCode(addressForm.getTitleCode());
		selectedAddress.setFirstName(addressForm.getFirstName());
		selectedAddress.setLastName(addressForm.getLastName());
		selectedAddress.setLine1(addressForm.getLine1());
		selectedAddress.setLine2(addressForm.getLine2());
		selectedAddress.setTown(addressForm.getTownCity());
		selectedAddress.setPostalCode(addressForm.getPostcode());
		selectedAddress.setBillingAddress(false);
		selectedAddress.setShippingAddress(true);
		final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
		selectedAddress.setCountry(countryData);

		if (resolveCountryRegions.contains(countryData.getIsocode()) && addressForm.getRegionIso() != null
				&& !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			selectedAddress.setRegion(regionData);
		}

		if (addressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}
		// set delivery method
		if (StringUtils.isNotEmpty(addressForm.getSelectedDeliveryMode()))
		{
			getCheckoutFacade().setDeliveryMode(addressForm.getSelectedDeliveryMode());
		}
		// end - set delivery method
		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook()
				&& previousSelectedAddress.getId() != null && !previousSelectedAddress.getId().equals(selectedAddress.getId()))
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryAddress(@RequestParam("selectedAddressCode") final String selectedAddressCode,
			@RequestParam("selectedDeliveryMode") final String selectedDeliveryMode, final RedirectAttributes redirectAttributes)
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
			final boolean hasSelectedAddressData = selectedAddressData != null;
			if (hasSelectedAddressData)
			{
				final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
				if (isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData))
				{
					getCheckoutFacade().setDeliveryAddress(selectedAddressData);
					getCheckoutFacade().setDeliveryMode(selectedDeliveryMode);
					if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook())
					{
						// temporary address should be removed
						getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
					}
				}
			}
		}
		return getCheckoutStep().currentStep();
	}


	@RequestMapping(value = "/getDeliveryArea", method = RequestMethod.POST)
	public @ResponseBody AddressForm getDeliveryArea(@RequestParam("postcode") final String postcode,
			@RequestParam("selectedDeliveryMode") final String selectedDeliveryMode) throws CMSItemNotFoundException
	{
		AddressForm addressForm = new AddressForm();
		addressForm = checkDeliveryArea(postcode, selectedDeliveryMode);
		return addressForm;
	}

	/*
	 * fetch the nearby stores with postcode, store code,store name or location
	 */
	@RequestMapping(value = "/getStores", method = RequestMethod.GET)
	public @ResponseBody PickUpStoreForm getStores(@RequestParam(value = "q", required = false) final String locationQuery,
			@RequestParam(value = "feature", required = false) final String feature,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "latitude", required = false) final Double latitude,
			@RequestParam(value = "longitude", required = false) final Double longitude,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "posId", required = false) final String posId,
			@RequestParam(value = "deliveryMode", required = false) final String selectedDeliveryMode, final Model model)
			throws CMSItemNotFoundException
	{
		final PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		/*
		 * store results
		 */
		pickUpStoreForm.setIsDeliverable(Boolean.FALSE);
		StoreFinderSearchPageData<PointOfServiceData> searchResults = new StoreFinderStockSearchPageData<PointOfServiceData>();
		searchResults = getNearbyStores(locationQuery, latitude, longitude, feature, page, showMode, sortCode, searchResults);
		final List<PointOfServiceData> posList = new ArrayList<PointOfServiceData>();
		if (CollectionUtils.isNotEmpty(posList))
		{
			posList.clear();
		}
		if (null != searchResults && CollectionUtils.isNotEmpty(searchResults.getResults()))
		{
			pickUpStoreForm.setIsDeliverable(Boolean.TRUE);
			/*
			 * fetching selected store
			 */
			if (posId != null)
			{
				for (final PointOfServiceData pos : searchResults.getResults())
				{
					if (pos.getName().equalsIgnoreCase(posId))
					{
						posList.add(pos);
					}
				}
				pickUpStoreForm.setPointOfServiceData(posList);
				pickUpStoreForm.setPosName(posId);
			}
			else
			{
				/*
				 * fetching all the nearby stores
				 */
				/*
				 * final List<ZoneDeliveryModeData> zoneDeliveryMode = (List<ZoneDeliveryModeData>) getCheckoutFacade()
				 * .getSupportedDeliveryModes(); for (final ZoneDeliveryModeData deliveryMode : zoneDeliveryMode) { if
				 * (deliveryMode.getName().equalsIgnoreCase(selectedDeliveryMode) ||
				 * deliveryMode.getCode().equalsIgnoreCase(selectedDeliveryMode)) {
				 * getCheckoutFacade().setDeliveryMode(deliveryMode.getCode()); cartData.setDeliveryMode(deliveryMode);
				 * cartData.setDeliveryCost(deliveryMode.getDeliveryCost());
				 * pickUpStoreForm.setSelectedDeliveryMode(selectedDeliveryMode); } }
				 */
				pickUpStoreForm.setPointOfServiceData(searchResults.getResults());
			}
			model.addAttribute("pointOfServiceDataList", searchResults.getResults());
		}
		if (StringUtils.isNotBlank(locationQuery))
		{
			pickUpStoreForm.setQ(locationQuery);
			model.addAttribute("locationQuery", pickUpStoreForm.getQ());
		}
		final CartData cart = clicksCheckoutFacade.getCheckoutCart();
		deliveryAddressFacade.getCartData(cart, model);
		//	getCheckoutFacade().setDeliveryMode(pickUpStoreForm.getSelectedDeliveryMode());
		AddressData tempAddr = null;
		if (cartData.getDeliveryAddress() == null)
		{

			/*
			 * Creating a temporary address
			 */
			tempAddr = new AddressData();
			final CountryData country = new CountryData();
			country.setIsocode("ZA");
			country.setName("South Africa");
			tempAddr.setCountry(country);
			tempAddr.setShippingAddress(true);
			tempAddr.setBillingAddress(false);
			tempAddr.setDefaultAddress(true);
			tempAddr.setVisibleInAddressBook(true);
			getUserFacade().addAddress(tempAddr);
			getCheckoutFacade().setDeliveryAddress(tempAddr);
			cart.setDeliveryAddress(tempAddr);
		}
		/*
		 * final List<ZoneDeliveryModeData> zoneDeliveryMode = (List<ZoneDeliveryModeData>) getCheckoutFacade()
		 * .getSupportedDeliveryModes(); if (zoneDeliveryMode.size() > 0) { for (final ZoneDeliveryModeData deliveryMode :
		 * zoneDeliveryMode) { if (deliveryMode.getCode().equalsIgnoreCase(pickUpStoreForm.getSelectedDeliveryMode()) ||
		 * deliveryMode.getName().equalsIgnoreCase(pickUpStoreForm.getSelectedDeliveryMode())) {
		 * cart.setDeliveryMode(deliveryMode); //cart.setDeliveryCost(deliveryMode.getDeliveryCost()); } } }
		 */
		model.addAttribute("deliveryFeeMessage", null);
		model.addAttribute("cartData", cartData);
		if (null != tempAddr)
		{
			getUserFacade().removeAddress(tempAddr);
		}
		model.addAttribute("pickUpStoreForm", pickUpStoreForm);
		return pickUpStoreForm;
	}

	public String getStoreUrl(final PointOfServiceModel posUrl)
	{
		final String urlPrefix = "/store/";
		String displayName = posUrl.getDisplayName();
		if (posUrl.getDisplayName().contains(" "))
		{
			displayName = posUrl.getDisplayName().replaceAll(" ", "-");
		}

		return urlPrefix + displayName + "/" + posUrl.getName();
	}

	/*
	 * submitting selected store address as delivery address along with customer details
	 */
	@RequestMapping(value = "/getStores", method = RequestMethod.POST)
	@RequireHardLogIn
	public String getSelectedStore(@RequestParam(value = "locationText", required = false) final String locationQuery,
			PickUpStoreForm pickUpStoreForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel)
	{
		if (null != pickUpStoreForm.getPosName() && StringUtils.isNotEmpty(pickUpStoreForm.getPosName()))
		{
			final PointOfServiceModel pos = clicksCheckoutFacade.getPosData(pickUpStoreForm.getPosName());
			pickUpStoreForm = clicksDeliveryAddressPopulator.populatePickUpForm(pos, pickUpStoreForm);
		}
		pickUpStoreAddressValidator.validate(pickUpStoreForm, bindingResult);
		try
		{
			this.prepareDataForPage(model);
		}
		catch (final CMSItemNotFoundException e2)
		{
			// YTODO Auto-generated catch block
			//e2.printStackTrace();
		}
		if (StringUtils.isNotBlank(pickUpStoreForm.getCountryIso()) && StringUtils.isAlpha(pickUpStoreForm.getCountryIso()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(pickUpStoreForm.getCountryIso()));
			model.addAttribute("country", pickUpStoreForm.getCountryIso());
		}

		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("showSaveToAddressBook", Boolean.TRUE);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("pickUpStoreForm", pickUpStoreForm);
			model.addAttribute("addressForm", new AddressForm());
			model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
			GlobalMessages.addErrorMessage(model, "address.error.store.entry.invalid");
			try
			{
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			}
			catch (final CMSItemNotFoundException e)
			{
				// YTODO Auto-generated catch block
				//e.printStackTrace();
			}
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}
		AddressData newAddress = new AddressData();
		if (!getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			final CustomerData currentCustomerData = getCustomerFacade().getCurrentCustomer();
			newAddress.setFirstName(currentCustomerData.getFirstName());
			newAddress.setLastName(currentCustomerData.getLastName());
		}
		else
		{
			newAddress.setFirstName(pickUpStoreForm.getStoreDisplayName());
			newAddress.setLastName(pickUpStoreForm.getPosName());
		}
		newAddress = clicksDeliveryAddressPopulator.populateStoreDelAddress(model, pickUpStoreForm, newAddress);
		// start - added for personal details
		newAddress.setTitleCode("mr");
		if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(false);
		}
		newAddress.setIsStoreAddress(Boolean.TRUE);
		// Verify the address data.
		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		final List<ZoneDeliveryModeData> zoneDeliveryMode = (List<ZoneDeliveryModeData>) getCheckoutFacade()
				.getSupportedDeliveryModes();
		for (final ZoneDeliveryModeData deliveryMode : zoneDeliveryMode)
		{
			if (deliveryMode.getName().equalsIgnoreCase(pickUpStoreForm.getSelectedDeliveryMode())
					|| deliveryMode.getCode().equalsIgnoreCase(pickUpStoreForm.getSelectedDeliveryMode()))
			{
				getCheckoutFacade().setDeliveryMode(deliveryMode.getCode());
				cartData.setDeliveryMode(deliveryMode);
				cartData.setDeliveryCost(deliveryMode.getDeliveryCost());
				//pickUpStoreForm.setSelectedDeliveryMode(pickUpStoreForm.getSelectedDeliveryMode());
			}
		}
		cartData.setDeliveryAddress(newAddress);
		if (null != cartData.getRootPaymentInfo() && null != cartData.getRootPaymentInfo().getBillingAddress())
		{
			newAddress.setFirstName(cartData.getRootPaymentInfo().getBillingAddress().getFirstName());
			newAddress.setLastName(cartData.getRootPaymentInfo().getBillingAddress().getLastName());
		}
		if (addressRequiresReview)
		{
			try
			{
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			}
			catch (final CMSItemNotFoundException e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		getUserFacade().addAddress(newAddress);
		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);
		model.addAttribute("deliveryFeeMessage", null);
		model.addAttribute("cartData", cartData);
		return getCheckoutStep().nextStep();


	}

	/*
	 * load more stores in delivery page
	 */
	@RequestMapping(value = "/loadMore", method = RequestMethod.POST)
	public @ResponseBody PickUpStoreForm getMoreStores(@RequestParam(value = "count", defaultValue = "0") final int count,
			@RequestParam(value = "feature", required = false) final String feature,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "latitude", required = false) final Double latitude,
			@RequestParam(value = "longitude", required = false) final Double longitude,
			@RequestParam(value = "sort", required = false) final String sortCode, @RequestParam("q") final String locationQuery,
			final Model model) throws CMSItemNotFoundException
	{
		final PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();
		//pickUpStoreForm.setCount(count);
		StoreFinderSearchPageData<PointOfServiceData> searchResults = new StoreFinderStockSearchPageData<PointOfServiceData>();
		searchResults = getNearbyStores(locationQuery, latitude, longitude, feature, page, showMode, sortCode, searchResults);
		final List<PointOfServiceData> result = new ArrayList<PointOfServiceData>();
		for (int i = count; i < searchResults.getResults().size(); i++)
		{
			try
			{
				searchResults.getResults().get(i).setDescription(locationQuery);
				result.add(searchResults.getResults().get(i));
			}
			catch (final ArrayIndexOutOfBoundsException exc)
			{
				return pickUpStoreForm;
				//LOG.error(exc);
			}
			//count++;
		}
		pickUpStoreForm.setPointOfServiceData(result);
		pickUpStoreForm.setQ(locationQuery);
		model.addAttribute("q", locationQuery);
		pickUpStoreForm.setCount(count);
		return pickUpStoreForm;
	}




	/**
	 * @param locationQuery
	 * @param longitude
	 * @param latitude
	 * @param showMode
	 * @param sortCode
	 * @return
	 */
	private StoreFinderSearchPageData<PointOfServiceData> getNearbyStores(final String locationQuery, final Double latitude,
			final Double longitude, final String feature, final int page, final ShowMode showMode, final String sortCode,
			StoreFinderSearchPageData<PointOfServiceData> searchResults)
	{

		final String sanitizedSearchQuery = XSSFilterUtil.filter(locationQuery);

		/*
		 * Search for stores with location query
		 */
		if (StringUtils.isNotBlank(sanitizedSearchQuery))
		{
			final PageableData pageableData = createPageableData(page, getStoreLocatorPageSize(), sortCode, showMode);
			pageableData.setFeature(feature);
			searchResults = deliveryAddressFacade.fetchStores(sanitizedSearchQuery, pageableData);
		}
		/*
		 * Search for stores with current location
		 */
		else if (latitude != null && longitude != null)
		{
			final GeoPoint geoPoint = new GeoPoint();
			geoPoint.setLatitude(latitude.doubleValue());
			geoPoint.setLongitude(longitude.doubleValue());
			final PageableData pageableData = createPageableData(page, getStoreLocatorPageSize(), sortCode, showMode);
			pageableData.setFeature(feature);
			searchResults = deliveryAddressFacade.fetchNearByStores(geoPoint, pageableData);
		}
		if (null != searchResults && CollectionUtils.isNotEmpty(searchResults.getResults()))
		{
			final List<PointOfServiceData> result = new ArrayList<PointOfServiceData>(searchResults.getResults().size());
			for (final PointOfServiceData posData : searchResults.getResults())
			{
				if (null != posData.getFeatures())
				{
					for (final Entry<String, String> entry : posData.getFeatures().entrySet())
					{
						if (entry.getKey().equalsIgnoreCase(posData.getName() + Config.getParameter("store.feature.service.type")))
						{
							result.add(posData);
						}
					}
					//}
				}
			}
			//	if (CollectionUtils.isNotEmpty(result))
			{
				searchResults.setResults(result);
			}
		}
		return searchResults;
	}

	/**
	 * @return
	 */
	protected int getStoreLocatorPageSize()
	{
		return getSiteConfigService().getInt("storefront.storelocator.pageSize", 0);
	}

	/**
	 * @param page
	 * @param storeLocatorPageSize
	 * @param sortCode
	 * @param showMode
	 * @return
	 */
	private PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode,
			final ShowMode showMode)
	{

		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(pageNumber);
		pageableData.setSort(sortCode);

		if (ShowMode.All == showMode)
		{
			pageableData.setPageSize(MAX_PAGE_LIMIT);
		}
		else
		{
			pageableData.setPageSize(pageSize);
		}
		return pageableData;

	}

	/*
	 * fetch address form for home delivery as per the postal code
	 */
	public AddressForm checkDeliveryArea(final String postcode, final String selectedDeliveryMode)
	{
		final AddressForm addressForm = new AddressForm();
		final AddressData addressData = clicksCheckoutFacade.getDeliveryAreaDetails(postcode);
		if (null != addressData)
		{
			addressForm.setIsDeliverable(new Boolean(true));
			if (null != addressData.getCountry() && null != addressData.getCountry().getName())
			{
				addressForm.setCountryName(addressData.getCountry().getName());
				addressForm.setCountryIso(addressData.getCountry().getIsocode());
			}
			if (null != addressData.getSuburb())
			{
				addressForm.setSuburb(getTitleCase(addressData.getSuburb()));
			}
			if (null != addressData.getPostalCode())
			{
				addressForm.setPostcode(addressData.getPostalCode());
			}
			addressData.setVisibleInAddressBook(true);
			addressData.setShippingAddress(true);
			getUserFacade().addAddress(addressData);
			getCheckoutFacade().setDeliveryAddress(addressData);
			List<ZoneDeliveryModeData> deliveryList = new ArrayList<ZoneDeliveryModeData>();
			deliveryList = (List<ZoneDeliveryModeData>) getCheckoutFacade().getSupportedDeliveryModes();
			final List<ZoneDeliveryModeData> standardDeliveryList = new ArrayList<ZoneDeliveryModeData>();
			if (deliveryList.size() > 0)
			{
				for (final ZoneDeliveryModeData deliveryMode : deliveryList)
				{
					if (deliveryMode.getName().equalsIgnoreCase(selectedDeliveryMode)
							|| deliveryMode.getCode().equalsIgnoreCase(selectedDeliveryMode))
					{
						standardDeliveryList.add(deliveryMode);
						getCheckoutFacade().setDeliveryMode(deliveryMode.getCode());
					}
				}
			}
			addressForm.setDeliveryModes(standardDeliveryList);
			getUserFacade().removeAddress(addressData);

		}
		else
		{
			addressForm.setIsDeliverable(new Boolean(false));
		}
		return addressForm;
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}


	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}


}
