/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.checkout.controllers.pages.checkout.steps;

import de.hybris.clicks.checkout.controllers.ControllerConstants;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facade.cartPage.CartPageFacade;
import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.facades.customException.ClicksOrderFraudException;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.clicksstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{
	private final static String SUMMARY = "summary";

	private final static Logger LOG = Logger.getLogger(SummaryCheckoutStepController.class);

	@Resource(name = "cartPageFacade")
	private CartPageFacade cartPageFacade;

	@Resource
	CartService cartService;


	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "clicksCheckoutFacade")
	ClicksCheckoutFacade clicksCheckoutFacade;

	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		final Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		if (cartService.hasSessionCart())
		{
			//CartModel cartmodel = cartService.getSessionCart();
			//cartmodel = cartPageFacade.calculateCart(cartmodel);
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
			{
				for (final OrderEntryData entry : cartData.getEntries())
				{
					final String productCode = entry.getProduct().getCode();
					final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode,
							Arrays.asList(ProductOption.BASIC, ProductOption.PRICE));
					entry.setProduct(product);
				}
			}

			double subtotalVal = 0.00;
			if (null != cartData.getSubTotal().getFormattedValue())
			{
				final String subTotalPrice = cartData.getSubTotal().getFormattedValue();
				final String formattedPrice = subTotalPrice.substring(1);
				subtotalVal = Double.parseDouble(formattedPrice.replaceAll(",", ""));
				if (null != cartData.getOrderDiscounts())
				{
					final double orderDiscount = Double
							.parseDouble(cartData.getOrderDiscounts().getFormattedValue().substring(1).replaceAll(",", ""));
					subtotalVal = subtotalVal - orderDiscount;
				}
				BigDecimal subtotal = BigDecimal.valueOf(subtotalVal);
				subtotal = subtotal.setScale(2, RoundingMode.CEILING);
				model.addAttribute("subtotal", subtotal);
			}
			model.addAttribute("cartData", cartData);
			model.addAttribute("allItems", cartData.getEntries());
			model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
			model.addAttribute("deliveryMode", cartData.getDeliveryMode());
			//		model.addAttribute("paymentInfo", cartData.getPaymentInfo());
			model.addAttribute("paymentInfo", cartData.getRootPaymentInfo());

			// Only request the security code if the SubscriptionPciOption is set to Default.
		}
		else
		{
			model.addAttribute("cartEmpty", Boolean.TRUE);
			GlobalMessages.addErrorMessage(model, "checkout.error.order.cart.notfound");
			LOG.error("cart is empty");
		}
		final boolean requestSecurityCode = (CheckoutPciOptionEnum.DEFAULT
				.equals(getCheckoutFlowFacade().getSubscriptionPciOption()));
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());
		model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}

	@RequestMapping(value = "/placeOrder")
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel)
					throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		LOG.info("SummaryCheckoutController -> Proceeding to place order");

		if (!cartService.hasSessionCart())
		{
			LOG.error("Session Cart is empty or null");
			return enterStep(model, redirectModel);
		}
		if (validateOrderForm(placeOrderForm, model))
		{
			LOG.error("Order form validation failed");
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			LOG.error("cart validation failed");
			// Invalid cart. Bounce back to the cart page.
			LOG.error("Invalid cart. Bounce back to the cart page.");
			return REDIRECT_PREFIX + "/cart";
		}

		// authorize, if failure occurs don't allow to place the order
		boolean isPaymentUthorized = false;
		try
		{
			clicksCheckoutFacade.checkForFraudOrder();
		}
		catch (final ClicksOrderFraudException e1)
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.order.fraud.failed");
			LOG.error("clicks order fraud exception," + e1);
			return enterStep(model, redirectModel);
		}
		try
		{
			isPaymentUthorized = getCheckoutFacade().authorizePayment(placeOrderForm.getSecurityCode());
		}
		catch (final AdapterException ae)
		{
			// handle a case where a wrong paymentProvider configurations on the store see getCommerceCheckoutService().getPaymentProvider()
			LOG.error(ae.getMessage(), ae);
		}
		if (!isPaymentUthorized)
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
			LOG.error("payment authorization failed");
			return enterStep(model, redirectModel);
		}

		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
		}
		catch (final Exception e)
		{
			LOG.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return enterStep(model, redirectModel);
		}

		return redirectToOrderConfirmationPage(orderData);
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model)
	{
		//		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			LOG.error("delivery address for your order is empty");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			LOG.error("method of delivery for order is not selected");
			invalid = true;
		}

		//		if (getCheckoutFlowFacade().hasNoPaymentInfo())
		//		{
		//			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
		//			invalid = true;
		//		}

		if (getCheckoutFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			LOG.error("payment details for order not provided");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			//			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
			//					&& StringUtils.isBlank(securityCode))
			//			{
			//				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
			//				invalid = true;
			//			}
		}

		//		if (!placeOrderForm.isTermsCheck())
		//		{
		//			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
		//			invalid = true;
		//			return invalid;
		//		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		//		if (!getCheckoutFacade().containsTaxValues())
		//		{
		//			LOG.error(String.format(
		//					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
		//					cartData.getCode()));
		//			GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
		//			invalid = true;
		//		}

		if (!cartData.isCalculated())
		{
			LOG.error(
					String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}
		LOG.info("Order Form is not Valid for " + cartData.getCode() + " ? : " + invalid);
		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}


}
