/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.checkout.steps.validation.impl;


import de.hybris.platform.clicksstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;

import javax.annotation.Resource;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public class DefaultRegisterCustomerStepCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			return userFacade.isAnonymousUser() ? ValidationResults.SUCCESS : ValidationResults.REDIRECT_TO_REGISTER_CUSTOMER;
		}
		LOG.info("Missing, empty or unsupported cart");
		return ValidationResults.FAILED;
	}
}
