/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.checkout.controllers.pages.checkout.steps;


import de.hybris.clicks.checkout.controllers.ControllerConstants;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.clicksstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.clicksstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.AddressForm;
import de.hybris.platform.clicksstorefrontcommons.forms.PaymentDetailsForm;
import de.hybris.platform.clicksstorefrontcommons.forms.SopPaymentDetailsForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final Map<String, String> cybersourceSopCardTypes = new HashMap<String, String>();
	private final static String PAYMENT_METHOD = "payment-method";

	@Resource(name = "clicksCheckoutFacade")
	ClicksCheckoutFacade clicksCheckoutFacade;
	@Resource(name = "userService")
	UserService userService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource
	private CartService cartService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@ModelAttribute("cardTypes")
	public Collection<CardTypeData> getCardTypes()
	{
		return getCheckoutFacade().getSupportedCardTypes();
	}

	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		final List<SelectOption> months = new ArrayList<SelectOption>();

		months.add(new SelectOption("1", "01"));
		months.add(new SelectOption("2", "02"));
		months.add(new SelectOption("3", "03"));
		months.add(new SelectOption("4", "04"));
		months.add(new SelectOption("5", "05"));
		months.add(new SelectOption("6", "06"));
		months.add(new SelectOption("7", "07"));
		months.add(new SelectOption("8", "08"));
		months.add(new SelectOption("9", "09"));
		months.add(new SelectOption("10", "10"));
		months.add(new SelectOption("11", "11"));
		months.add(new SelectOption("12", "12"));

		return months;
	}

	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		final Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		final List<SelectOption> startYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 6); i--)
		{
			startYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return startYears;
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < (calender.get(Calendar.YEAR) + 11); i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		final StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringtrimmer);
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		setupAddPaymentPage(model);
		final PaymentInfoData paymentInfoData = clicksCheckoutFacade.getCheckoutCart().getRootPaymentInfo();

		if (paymentInfoData != null && paymentInfoData.getBillingAddress() != null)
		{
			return getCheckoutStep().nextStep();
		}
		// Use the checkout PCI strategy for getting the URL for creating new subscriptions.
		//final CheckoutPciOptionEnum subscriptionPciOption = getCheckoutFlowFacade().getSubscriptionPciOption();
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		// If not using HOP or SOP we need to build up the payment details form
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		//cartData.getDeliveryAddress().isVisibleInAddressBook()
		//if (null != cartData.getDeliveryAddress().getIsStoreAddress())
		//{
		//final PickUpStoreForm pickUpStoreForm = new PickUpStoreForm();
		//model.addAttribute("isStoreAddress", cartData.getDeliveryAddress().getIsStoreAddress());
		//}
		final AddressForm addressForm = new AddressForm();
		addressForm.setBillingAddress(Boolean.TRUE);
		addressForm.setDeliveryInstructions(clicksCheckoutFacade.getDeliveryInstructionFromCart());
		if (!getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			final CustomerData currentCustomerData = getCustomerFacade().getCurrentCustomer();
			addressForm.setEmail(currentCustomerData.getUid());
			addressForm.setFirstName(currentCustomerData.getFirstName());
			addressForm.setLastName(currentCustomerData.getLastName());
			addressForm.setPhone(currentCustomerData.getContactNumber());
		}
		model.addAttribute("deliveryInstruction", clicksCheckoutFacade.getDeliveryInstructionFromCart());
		paymentDetailsForm.setBillingAddress(addressForm);
		paymentDetailsForm.setNewBillingAddress(Boolean.TRUE);
		model.addAttribute(paymentDetailsForm);
		model.addAttribute("buttonVal", "Continue");
		if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			model.addAttribute("isAnonymousCheckout", "true");
		}
		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();
		final List<AddressModel> customerAddressList = new ArrayList<>();
		for (final AddressModel address : currentCustomer.getAddresses())
		{
			if (Boolean.TRUE.equals(address.getBillingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook()))
			{
				customerAddressList.add(address);
			}
		}
		model.addAttribute("customerAddressList", customerAddressList);
		model.addAttribute("billingFormTitle", Config.getParameter("billing.form.title.message"));

		model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	@RequestMapping(value =
	{ "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, @Valid final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);

		if (bindingResult.hasErrors())
		{
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute("cartData", cartData);
			final CustomerModel currentCheckoutCus = clicksCheckoutFacade.getCustomerModel();
			if (getCheckoutCustomerStrategy().isAnonymousCheckout())
			{
				model.addAttribute("isAnonymousCheckout", "true");
			}
			final List<AddressModel> customerAddressList = new ArrayList<>();
			for (final AddressModel address : currentCheckoutCus.getAddresses())
			{
				if (Boolean.TRUE.equals(address.getBillingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook()))
				{
					customerAddressList.add(address);
				}
			}
			model.addAttribute("customerAddressList", customerAddressList);

			model.addAttribute("edit", Boolean.TRUE);
			model.addAttribute("hasErrors", Boolean.TRUE);
			model.addAttribute("buttonVal", "Continue");
			model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		final CCPaymentInfoData paymentInfoData = new CCPaymentInfoData();
		paymentInfoData.setId(paymentDetailsForm.getPaymentId());

		if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			paymentInfoData.setSaved(true);
		}
		final AddressData addressData;

		if (Boolean.FALSE.equals(paymentDetailsForm.getNewBillingAddress()))
		{
			addressData = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
			if (addressData == null)
			{
				GlobalMessages.addErrorMessage(model,
						"checkout.multi.paymentMethod.createSubscription.billingAddress.noneSelectedMsg");
				return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
			}

			addressData.setBillingAddress(true);
			clicksCheckoutFacade.saveDeliveryAsBillingAddress();
			//This condition will execute only for Click & Collect functionality
			clicksCheckoutFacade.saveCustomerNameInDeliveryAddress(paymentDetailsForm.getBillingAddress().getFirstName(),
					paymentDetailsForm.getBillingAddress().getLastName());
			if (cartService.hasSessionCart() && null != cartService.getSessionCart().getUser())
			{
				final CustomerModel customerModel = (CustomerModel) cartService.getSessionCart().getUser();
				if (null != getCheckoutFacade().getCheckoutCart() && getCheckoutCustomerStrategy().isAnonymousCheckout()
						&& null != paymentDetailsForm.getBillingAddress())
				{
					customerModel.setEmailID(paymentDetailsForm.getBillingAddress().getEmail());
					if (null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress()
							&& null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone())
					{
						final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
						final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
						contactModel.setNumber(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
						contactModel.setCode(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
						contactModel.setTypeID("2");
						contactModelList.add(contactModel);
						customerModel.setContactDetails(contactModelList);
						modelService.save(contactModel);
					}
					modelService.save(customerModel);
				}
				else if (null != customerModel.getUid() && !getCheckoutCustomerStrategy().isAnonymousCheckout())
				{
					customerModel.setEmailID(customerModel.getUid());
					if (null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress()
							&& null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone())
					{
						final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
						final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
						contactModel.setNumber(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
						contactModel.setCode(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
						contactModel.setTypeID("2");
						contactModelList.add(contactModel);
						modelService.save(contactModel);
						if (null != customerModel.getContactDetails())
						{
							final List<ContactDetailsModel> currentContactList = customerModel.getContactDetails();
							final List<ContactDetailsModel> listFinal = new ArrayList<ContactDetailsModel>();
							listFinal.addAll(contactModelList);
							listFinal.addAll(currentContactList);
							customerModel.setContactDetails(listFinal);
						}
						else
						{
							customerModel.setContactDetails(contactModelList);
						}
					}
					modelService.save(customerModel);
				}
			}
			return getCheckoutStep().nextStep();
		}
		else
		{
			final AddressForm addressForm = paymentDetailsForm.getBillingAddress();
			addressData = new AddressData();
			if (null != addressForm)
			{
				addressData.setId(addressForm.getAddressId());
				addressData.setTitleCode(addressForm.getTitleCode());
				addressData.setFirstName(addressForm.getFirstName());
				addressData.setLastName(addressForm.getLastName());
				addressData.setEmail(addressForm.getEmail());
				addressData.setLine1(addressForm.getLine1());
				addressData.setLine2(addressForm.getLine2());
				addressData.setSuburb(addressForm.getSuburb());
				addressData.setTown(addressForm.getTownCity());
				addressData.setProvince(addressForm.getProvince());
				addressData.setPostalCode(addressForm.getPostcode());
				addressData.setVisibleInAddressBook(Boolean.TRUE.booleanValue());
				//addressData.setCountry("ZA");
				//	if (addressForm.getRegionIso() != null)
				//{
				//		addressData.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
				//	}

				addressData.setShippingAddress(Boolean.TRUE.equals(addressForm.getShippingAddress()));
				addressData.setBillingAddress(Boolean.TRUE.equals(addressForm.getBillingAddress()));
				paymentInfoData.setBillingAddress(addressData);
				getCheckoutFacade().createPaymentSubscription(paymentInfoData);
				setCheckoutStepLinksForModel(model, getCheckoutStep());
				//This condition will execute only for Click & Collect functionality
				clicksCheckoutFacade.saveCustomerNameInDeliveryAddress(addressForm.getFirstName(), addressForm.getLastName());
				if (cartService.hasSessionCart() && null != cartService.getSessionCart().getUser())
				{
					final CustomerModel customerModel = (CustomerModel) cartService.getSessionCart().getUser();
					if (null != getCheckoutFacade().getCheckoutCart() && getCheckoutCustomerStrategy().isAnonymousCheckout()
							&& null != paymentInfoData.getBillingAddress())
					{
						customerModel.setEmailID(paymentInfoData.getBillingAddress().getEmail());
						if (null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress()
								&& null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone())
						{
							final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
							final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
							contactModel.setNumber(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
							contactModel.setCode(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
							contactModel.setTypeID("2");
							contactModelList.add(contactModel);
							customerModel.setContactDetails(contactModelList);
							modelService.save(contactModel);
						}
						modelService.save(customerModel);
					}
					else if (null != customerModel.getUid() && !getCheckoutCustomerStrategy().isAnonymousCheckout())
					{
						customerModel.setEmailID(customerModel.getUid());
						if (null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress()
								&& null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone())
						{
							final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
							final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
							contactModel.setNumber(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
							contactModel.setCode(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
							contactModel.setTypeID("2");
							contactModelList.add(contactModel);
							modelService.save(contactModel);
							if (null != customerModel.getContactDetails())
							{
								final List<ContactDetailsModel> currentContactList = customerModel.getContactDetails();
								final List<ContactDetailsModel> listFinal = new ArrayList<ContactDetailsModel>();
								listFinal.addAll(contactModelList);
								listFinal.addAll(currentContactList);
								customerModel.setContactDetails(listFinal);
							}
							else
							{
								customerModel.setContactDetails(contactModelList);
							}

						}
						modelService.save(customerModel);
					}
				}
				return getCheckoutStep().nextStep();
			}
		}
		GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	@RequireHardLogIn
	public String remove(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getUserFacade().unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return getCheckoutStep().currentStep();
	}

	/**
	 * This method gets called when the "Use These Payment Details" button is clicked. It sets the selected payment
	 * method on the checkout facade and reloads the page highlighting the selected payment method.
	 *
	 * @param selectedPaymentMethodId
	 *           - the id of the payment method to use.
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId)
	{
		if (StringUtils.isNotBlank(selectedPaymentMethodId))
		{
			getCheckoutFacade().setPaymentDetails(selectedPaymentMethodId);
		}
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CardTypeData createCardTypeData(final String code, final String name)
	{
		final CardTypeData cardTypeData = new CardTypeData();
		cardTypeData.setCode(code);
		cardTypeData.setName(name);
		return cardTypeData;
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	protected void setupSilentOrderPostPage(final SopPaymentDetailsForm sopPaymentDetailsForm, final Model model)
	{
		try
		{
			final PaymentData silentOrderPageData = getPaymentFacade().beginSopCreateSubscription("/checkout/multi/sop/response",
					"/integration/merchant_callback");
			model.addAttribute("silentOrderPageData", silentOrderPageData);
			sopPaymentDetailsForm.setParameters(silentOrderPageData.getParameters());
			model.addAttribute("paymentFormUrl", silentOrderPageData.getPostUrl());
		}
		catch (final IllegalArgumentException e)
		{
			model.addAttribute("paymentFormUrl", "");
			model.addAttribute("silentOrderPageData", null);
			LOG.warn("Failed to set up silent order post page " + e.getMessage());
			GlobalMessages.addErrorMessage(model, "checkout.multi.sop.globalError");
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("silentOrderPostForm", new PaymentDetailsForm());
		model.addAttribute("cartData", cartData);
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("sopPaymentDetailsForm", sopPaymentDetailsForm);
		model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));
		model.addAttribute("sopCardTypes", getSopCardTypes());
		if (StringUtils.isNotBlank(sopPaymentDetailsForm.getBillTo_country()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(sopPaymentDetailsForm.getBillTo_country()));
			model.addAttribute("country", sopPaymentDetailsForm.getBillTo_country());
		}
	}

	protected Collection<CardTypeData> getSopCardTypes()
	{
		final Collection<CardTypeData> sopCardTypes = new ArrayList<CardTypeData>();

		final List<CardTypeData> supportedCardTypes = getCheckoutFacade().getSupportedCardTypes();
		for (final CardTypeData supportedCardType : supportedCardTypes)
		{
			// Add credit cards for all supported cards that have mappings for cybersource SOP
			if (cybersourceSopCardTypes.containsKey(supportedCardType.getCode()))
			{
				sopCardTypes.add(createCardTypeData(cybersourceSopCardTypes.get(supportedCardType.getCode()),
						supportedCardType.getName()));
			}
		}
		return sopCardTypes;
	}

	@RequireHardLogIn
	@RequestMapping(value = "/useExistingBillingAddress", method = RequestMethod.POST)
	public String useExistingBillingAddress(@ModelAttribute final PaymentDetailsForm paymentDetailsForm)
	{
		try
		{
			if (null != paymentDetailsForm.getBillingAddress()
					&& StringUtils.isNotBlank(paymentDetailsForm.getBillingAddress().getAddressId()))
			{
				final AddressModel savedBillingAddressModel = clicksCheckoutFacade.saveBillingAddressInCart(paymentDetailsForm
						.getBillingAddress().getAddressId(), null);
				if (null != savedBillingAddressModel)
				{
					//This condition will execute only for Click & Collect functionality
					clicksCheckoutFacade.saveCustomerNameInDeliveryAddress(savedBillingAddressModel.getFirstname(),
							savedBillingAddressModel.getLastname());
					if (cartService.hasSessionCart() && null != cartService.getSessionCart().getUser())
					{
						final CustomerModel customerModel = (CustomerModel) cartService.getSessionCart().getUser();
						if (null != getCheckoutFacade().getCheckoutCart() && getCheckoutCustomerStrategy().isAnonymousCheckout()
								&& null != paymentDetailsForm.getBillingAddress())
						{
							customerModel.setEmailID(paymentDetailsForm.getBillingAddress().getEmail());
							if (null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress()
									&& null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone())
							{
								final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
								final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
								contactModel.setNumber(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
								contactModel.setCode(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
								contactModel.setTypeID("2");
								contactModelList.add(contactModel);
								modelService.save(contactModel);
								customerModel.setContactDetails(contactModelList);
							}
							modelService.save(customerModel);
						}
						else if (null != customerModel.getUid() && !getCheckoutCustomerStrategy().isAnonymousCheckout())
						{
							customerModel.setEmailID(customerModel.getUid());
							if (null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress()
									&& null != getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone())
							{
								final List<ContactDetailsModel> contactModelList = new ArrayList<ContactDetailsModel>();
								final ContactDetailsModel contactModel = modelService.create(ContactDetailsModel.class);
								contactModel.setNumber(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
								contactModel.setCode(getCheckoutFacade().getCheckoutCart().getDeliveryAddress().getPhone());
								contactModel.setTypeID("2");
								contactModelList.add(contactModel);
								modelService.save(contactModel);
								if (null != customerModel.getContactDetails())
								{
									final List<ContactDetailsModel> currentContactList = customerModel.getContactDetails();
									final List<ContactDetailsModel> listFinal = new ArrayList<ContactDetailsModel>();
									listFinal.addAll(contactModelList);
									listFinal.addAll(currentContactList);
									customerModel.setContactDetails(listFinal);
								}
								else
								{
									customerModel.setContactDetails(contactModelList);
								}
							}
							modelService.save(customerModel);
						}
					}
					return getCheckoutStep().nextStep();
				}
			}
			return getCheckoutStep().currentStep();
		}
		catch (final Exception e)
		{
			return getCheckoutStep().currentStep();
		}
	}

	@RequireHardLogIn
	@RequestMapping(value = "/useDeliveryAsBillingAddress", method = RequestMethod.POST)
	public String useDeliveryAsBillingAddress(final RedirectAttributes redirectAttributes)
	{
		try
		{
			if (clicksCheckoutFacade.saveDeliveryAsBillingAddress())
			{
				return getCheckoutStep().nextStep();
			}
			return getCheckoutStep().currentStep();
		}
		catch (final Exception e)
		{
			return getCheckoutStep().currentStep();
		}
	}


	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode") final String editAddressCode, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		setupAddPaymentPage(model);

		// Use the checkout PCI strategy for getting the URL for creating new subscriptions.
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		AddressModel addressModel = null;
		if (StringUtils.isNotEmpty(editAddressCode))
		{
			addressModel = clicksCheckoutFacade.getBillingAddressForCode(editAddressCode);
		}
		if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			model.addAttribute("isAnonymousCheckout", "true");
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		final AddressForm addressForm = new AddressForm();
		final boolean hasAddressModel = addressModel != null;
		if (hasAddressModel)
		{
			addressForm.setAddressId(addressModel.getPk().getLongValueAsString());
			addressForm.setFirstName(addressModel.getFirstname());
			addressForm.setLastName(addressModel.getLastname());
			addressForm.setEmail(addressModel.getEmail());
			addressForm.setLine1(addressModel.getStreetname());
			addressForm.setLine2(addressModel.getStreetnumber());
			addressForm.setSuburb(addressModel.getSuburb());
			addressForm.setTownCity(addressModel.getTown());
			addressForm.setProvince(addressModel.getProvince());
			addressForm.setPostcode(addressModel.getPostalcode());
			addressForm.setBillingAddress(Boolean.TRUE);
			addressForm.setEditAddress(Boolean.TRUE);
			addressForm.setDeliveryInstructions(clicksCheckoutFacade.getDeliveryInstructionFromCart());
		}

		model.addAttribute("deliveryInstruction", clicksCheckoutFacade.getDeliveryInstructionFromCart());
		model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
		model.addAttribute("edit", Boolean.valueOf(hasAddressModel));
		paymentDetailsForm.setBillingAddress(addressForm);
		model.addAttribute(paymentDetailsForm);

		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();
		final List<AddressModel> customerAddressList = new ArrayList<>();
		for (final AddressModel address : currentCustomer.getAddresses())
		{
			if (Boolean.TRUE.equals(address.getBillingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook())
					&& !editAddressCode.equalsIgnoreCase(address.getPk().getLongValueAsString()))
			{
				customerAddressList.add(address);
			}
		}
		model.addAttribute("customerAddressList", customerAddressList);
		model.addAttribute("billingFormTitle", Config.getParameter("billing.form.title.message"));

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String editBillingAddress(final Model model, @Valid final PaymentDetailsForm paymentDetailsForm,
			final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("buttonVal", "Update");
			model.addAttribute("edit", Boolean.TRUE);
			model.addAttribute("returnToShopping", getSessionService().getAttribute("returnToShopping"));
			model.addAttribute("hasErrors", Boolean.TRUE);
			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute("cartData", cartData);
			final CustomerModel currentCheckoutCus = clicksCheckoutFacade.getCustomerModel();
			final List<AddressModel> customerAddressList = new ArrayList<>();
			for (final AddressModel address : currentCheckoutCus.getAddresses())
			{
				if (Boolean.TRUE.equals(address.getBillingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook())
						&& !paymentDetailsForm.getBillingAddress().getAddressId().equals(address.getPk().getLongValueAsString()))
				{
					customerAddressList.add(address);
				}
			}
			model.addAttribute("customerAddressList", customerAddressList);

			if (getCheckoutCustomerStrategy().isAnonymousCheckout())
			{
				model.addAttribute("isAnonymousCheckout", "true");
			}
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}
		final AddressData addressData = new AddressData();
		final AddressForm addressForm = paymentDetailsForm.getBillingAddress();
		if (null != addressForm)
		{
			addressData.setId(addressForm.getAddressId());
			addressData.setFirstName(addressForm.getFirstName());
			addressData.setLastName(addressForm.getLastName());
			addressData.setEmail(addressForm.getEmail());
			addressData.setLine1(addressForm.getLine1());
			addressData.setLine2(addressForm.getLine2());
			addressData.setSuburb(addressForm.getSuburb());
			addressData.setTown(addressForm.getTownCity());
			addressData.setProvince(addressForm.getProvince());
			addressData.setPostalCode(addressForm.getPostcode());
			addressData.setDeliveryInstructions(addressForm.getDeliveryInstructions());
			addressData.setShippingAddress(Boolean.FALSE.equals(addressForm.getShippingAddress()));
			addressData.setBillingAddress(Boolean.TRUE.equals(addressForm.getBillingAddress()));
			addressData.setVisibleInAddressBook(true);
			clicksCheckoutFacade.editBillingAddress(addressData);
			return useExistingBillingAddress(paymentDetailsForm);
		}
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}

	static
	{
		// Map hybris card type to Cybersource SOP credit card
		cybersourceSopCardTypes.put("visa", "001");
		cybersourceSopCardTypes.put("master", "002");
		cybersourceSopCardTypes.put("amex", "003");
		cybersourceSopCardTypes.put("diners", "005");
		cybersourceSopCardTypes.put("maestro", "024");
	}
}
