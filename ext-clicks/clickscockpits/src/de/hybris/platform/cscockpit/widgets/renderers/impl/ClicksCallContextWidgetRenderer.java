/**
 * 
 */
package de.hybris.platform.cscockpit.widgets.renderers.impl;

import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Toolbarbutton;

import de.hybris.platform.cockpit.events.CockpitEventAcceptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.session.impl.OrderedConfigurableBrowserArea;
import de.hybris.platform.cscockpit.utils.CssUtils;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CallContextController;
import de.hybris.platform.cscockpit.widgets.models.impl.CallContextWidgetModel;

/**
 * @author manish.bhargava
 *
 */
public class ClicksCallContextWidgetRenderer extends CallContextWidgetRenderer{

	@Override
	protected HtmlBasedComponent createContentInternal(
			Widget<CallContextWidgetModel, CallContextController> widget,
			HtmlBasedComponent rootContainer) {
	    CockpitEventAcceptor addressNotificationEventAcceptor = createAddressNotificationEventAcceptor(widget);
	    OrderedConfigurableBrowserArea browserArea = (OrderedConfigurableBrowserArea)UISessionUtils.getCurrentSession()
	      .getCurrentPerspective().getBrowserArea();
	    browserArea.addNotificationListener(widget.getWidgetCode(), addressNotificationEventAcceptor);

	    Div content = new Div();

	    content.appendChild(createSiteContent(widget));
	    content.appendChild(createCustomerContent(widget));
	    content.appendChild(createOrderContent(widget));
	  //  content.appendChild(createTicketContent(widget));
	   // content.appendChild(createCurrencyContent(widget));
	   // content.appendChild(createEndCallContent(widget));

	    return content;
	}
	
	@Override
	protected void createNewCustomerButton(
			Widget<CallContextWidgetModel, CallContextController> widget,
			Div container) {
		//Not required as per requirement so overridden this method and left it empty.
	}

}
