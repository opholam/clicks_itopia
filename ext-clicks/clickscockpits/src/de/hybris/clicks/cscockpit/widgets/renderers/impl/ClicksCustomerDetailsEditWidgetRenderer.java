package de.hybris.clicks.cscockpit.widgets.renderers.impl;

import de.hybris.platform.cockpit.components.StyledDiv;
import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.events.CockpitEventAcceptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.widgets.InputWidget;
import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.commerceservices.constants.GeneratedCommerceServicesConstants.Enumerations.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.CustomerController;
import de.hybris.platform.cscockpit.widgets.models.impl.CustomerItemWidgetModel;
import de.hybris.platform.cscockpit.widgets.renderers.details.WidgetDetailRenderer;
import de.hybris.platform.cscockpit.widgets.renderers.impl.CustomerDetailsEditWidgetRenderer;

import java.util.Collections;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;



public class ClicksCustomerDetailsEditWidgetRenderer extends CustomerDetailsEditWidgetRenderer

{
	@Override
	protected HtmlBasedComponent createContentInternal(InputWidget<CustomerItemWidgetModel, CustomerController> widget,HtmlBasedComponent rootContainer) {
		Div content = new Div();

		TypedObject customer = ((CustomerItemWidgetModel) widget
				.getWidgetModel()).getCustomer();
		if ((customer != null)
				&& (customer.getObject() instanceof CustomerModel)) {
			loadAndCreateEditors(widget, content, customer);

			Div row = new Div();
			row.setParent(content);
			row.setSclass("csCustomerFooterRow");
			row.setStyle("padding-bottom: 50px");
			renderFooter(row, widget);

			Div right = new Div();
			right.setParent(row);
			right.setSclass("csCustomerRefreshButton");

			createRefreshButton(widget, right, "refresh");
		} else {
			content.appendChild(new Label(LabelUtils.getLabel(widget,
					"noCustomerSelected", new Object[0])));
		}
		return content;
	}
	
	@Override
	protected void renderFooter(HtmlBasedComponent content,
			InputWidget<CustomerItemWidgetModel, CustomerController> widget) {
		Div footerContent = new Div();
		footerContent.setSclass("csCustomerFooter");
		footerContent.setStyle("padding-bottom: 50px");
		footerContent.setParent(content);

		TypedObject customer = ((CustomerItemWidgetModel) widget
				.getWidgetModel()).getCustomer();
		if ((customer == null)
				|| (!(customer.getObject() instanceof CustomerModel)))
			return;
		HtmlBasedComponent detailContent = getFooterRenderer().createContent(
				null, customer, widget);
		if (detailContent == null)
			return;
		footerContent.appendChild(detailContent);
	}

}
