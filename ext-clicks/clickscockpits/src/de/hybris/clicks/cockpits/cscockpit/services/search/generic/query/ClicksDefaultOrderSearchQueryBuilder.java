/**
 * 
 */
package de.hybris.clicks.cockpits.cscockpit.services.search.generic.query;

import org.apache.commons.lang.StringUtils;

import de.hybris.platform.cscockpit.services.search.generic.query.DefaultOrderSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

/**
 * @author manish.bhargava
 *
 */
public class ClicksDefaultOrderSearchQueryBuilder extends DefaultOrderSearchQueryBuilder 
{
	@Override
	protected FlexibleSearchQuery buildFlexibleSearchQuery(
			DefaultCsTextSearchCommand command) {
		  String orderId = command.getText(TextField.OrderId);
		    String customerName = command.getText(TextField.CustomerName);

		    boolean searchOrderId = StringUtils.isNotEmpty(orderId);
		    boolean searchCustomerName = StringUtils.isNotEmpty(customerName);

		    StringBuilder query = new StringBuilder(300);


		    query.append("SELECT DISTINCT {o:pk}, {o:code}, {o:clicksOrderCode} ");


		    query.append("FROM {Order AS o ");

		    if (searchCustomerName)

		    {
		      query.append("LEFT JOIN Customer AS c ON {c:pk}={o:user} ");

		    }

		    query.append(" } ");


		    query.append(" WHERE {o:originalVersion} IS NULL ");

		    if ((searchOrderId) || (searchCustomerName))
		    {
		      query.append(" AND ( ");
		    }

		    if (searchOrderId)
		    {
		      query.append(" {o:clicksOrderCode} LIKE ?orderId OR {o:code} LIKE ?orderId ");
		    }

		    if (searchCustomerName)
		    {
		      if (searchOrderId)
		      {
		        query.append(" OR ");
		      }

		      if (isCaseInsensitive())

		      {
		        query.append(" LOWER({c:name}) LIKE LOWER(?customerName) ");

		      }
		      else
		      {
		        query.append(" {c:name} LIKE ?customerName ");
		      }
		    }

		    if ((searchOrderId) || (searchCustomerName))
		    {
		      query.append(" ) ");
		    }

		    query.append(" ORDER BY {o:code} ASC ");

		    FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());


		    if (searchOrderId)
		    {
		      searchQuery.addQueryParameter("orderId", "%" + orderId.trim() + "%");
		    }
		    if (searchCustomerName)
		    {
		      searchQuery.addQueryParameter("customerName", "%" + customerName.trim() + "%");
		    }

		    return searchQuery;
	}
}
