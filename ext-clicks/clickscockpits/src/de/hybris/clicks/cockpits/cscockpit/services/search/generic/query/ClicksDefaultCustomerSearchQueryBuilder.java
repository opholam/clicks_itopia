package de.hybris.clicks.cockpits.cscockpit.services.search.generic.query;

import org.apache.commons.lang.StringUtils;

import de.hybris.platform.cscockpit.services.search.generic.query.DefaultCustomerSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

public class ClicksDefaultCustomerSearchQueryBuilder extends DefaultCustomerSearchQueryBuilder{
	
	@Override
	protected FlexibleSearchQuery buildFlexibleSearchQuery(DefaultCsTextSearchCommand command)
	  {
	    String name = command.getText(TextField.Name);
	    String postcode = command.getText(TextField.Postcode);

	    boolean searchName = StringUtils.isNotEmpty(name);
	    boolean searchPostcode = StringUtils.isNotEmpty(postcode);

	    StringBuilder query = new StringBuilder(300);


	    query.append("SELECT DISTINCT {c:pk}, {c:name}, {c:uid} ");


	    query.append("FROM {Customer AS c ");

	    if (searchPostcode)

	    {
	      query.append("LEFT JOIN Address AS a ON {c:pk}={a:owner} ");

	    }

	    query.append("} ");


	    query.append("WHERE ({c:name}!='anonymous' OR {c:name} IS NULL) ");

	    if (searchName)
	    {
	      if (isCaseInsensitive())

	      {
	        query.append("AND (LOWER({c:name}) LIKE LOWER(?customerName) OR LOWER({c:firstName}) LIKE LOWER(?customerName) OR LOWER({c:lastName}) LIKE LOWER(?customerName)  OR {c:uid} LIKE ?customerName  OR {c:memberID} LIKE ?customerName)");

	      }
	      else
	      {
	        query.append("AND ({c:name} LIKE ?customerName OR {c:firstName} LIKE ?customerName OR {c:lastName} LIKE ?customerName OR {c:uid} LIKE ?customerName OR {c:memberID} LIKE ?customerName) ");
	      }
	    }

	    if (searchPostcode)
	    {
	      if (isCaseInsensitive())

	      {
	        query.append("AND LOWER({a:postalcode}) LIKE LOWER(?postcode) ");

	      }
	      else
	      {
	        query.append("AND {a:postalcode} LIKE ?postcode ");
	      }

	    }

	    query.append("ORDER BY {c:name}, {c:uid} ");

	    FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());


	    if (searchName)
	    {
	      searchQuery.addQueryParameter("customerName", "%" + name.trim() + "%");
	    }
	    if (searchPostcode)
	    {
	      searchQuery.addQueryParameter("postcode", "%" + postcode.trim() + "%");
	    }

	    return searchQuery;
	  }

}
