/**
 * 
 */
package de.hybris.advancedexport;

import de.hybris.advancedexport.jalo.AdvancedTypeExportConfiguration;
import de.hybris.advancedexport.model.AdvancedTypeExportCronJobModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ExportConfig;
import de.hybris.platform.servicelayer.impex.ExportConfig.ValidationMode;
import de.hybris.platform.servicelayer.impex.ExportResult;
import de.hybris.platform.servicelayer.impex.ExportService;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.util.CSVConstants;

import java.io.StringBufferInputStream;

import org.apache.log4j.Logger;


/**
 * @author brendan
 * 
 */
public class AdvancedTypeExportJob extends AbstractJobPerformable<AdvancedTypeExportCronJobModel>
{

	//private ModelService modelService;
	private ExportService exportService;

	private static final Logger LOG = Logger.getLogger(AdvancedTypeExportJob.class);

	@Override
	public PerformResult perform(final AdvancedTypeExportCronJobModel cron)
	{

		final String exportString = generateExportScript(cron);

		if (LOG.isInfoEnabled())
		{
			LOG.info("***** Exporting types ********");
			LOG.info("Using script.......");
			LOG.info(exportString);
		}

		final ImpExResource exportResource = new StreamBasedImpExResource(new StringBufferInputStream(exportString),
				CSVConstants.HYBRIS_ENCODING);

		final ExportConfig exportConfig = new ExportConfig();
		exportConfig.setFailOnError(false);
		exportConfig.setValidationMode(ValidationMode.RELAXED);
		exportConfig.setScript(exportResource);
		exportConfig.setSingleFile(true);

		cron.setExportScript(exportResource.getMedia());
		modelService.save(cron);

		final ExportResult result = exportService.exportData(exportConfig);
		cron.setExportedData(result.getExportedData());
		cron.setExportedMedia(result.getExportedMedia());
		modelService.save(cron);

		return result.isSuccessful() ? new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED) : new PerformResult(
				CronJobResult.FAILURE, CronJobStatus.FINISHED);
	}

	/**
	 * Use a custom export script generator to dynamically create an export script
	 * 
	 * @param cron
	 * @return
	 */
	protected String generateExportScript(final AdvancedTypeExportCronJobModel cron)
	{
		final AdvancedTypeExportScriptGenerator generator = new AdvancedTypeExportScriptGenerator(
				(AdvancedTypeExportConfiguration) modelService.getSource(cron.getExportConfiguration()));
		final String exportString = generator.generateScript();
		return exportString;
	}

	/**
	 * @param exportService
	 *           the exportService to set
	 */
	public void setExportService(final ExportService exportService)
	{
		this.exportService = exportService;
	}


}
