/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.RetryLaterException.Method;
import de.hybris.platform.util.Config;

import org.apache.log4j.Logger;

import com.clicks.integration.service.OrderIntegrationService;


/**
 * This action implements payment authorization using {@link CreditCardPaymentInfoModel}. Any other payment model could
 * be implemented here, or in a separate action, if the process flow differs.
 */
public class ExportOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private final static Logger LOG = Logger.getLogger(ExportOrderAction.class);
	private OrderIntegrationService orderIntegrationService;
	private BusinessProcessService businessProcessService;

	/**
	 * @return the orderIntegrationService
	 */
	public OrderIntegrationService getOrderIntegrationService()
	{
		return orderIntegrationService;
	}

	/**
	 * @param orderIntegrationService
	 *           the orderIntegrationService to set
	 */
	public void setOrderIntegrationService(final OrderIntegrationService orderIntegrationService)
	{
		this.orderIntegrationService = orderIntegrationService;
	}

	@Override
	public Transition executeAction(final OrderProcessModel process) throws RetryLaterException
	{
		final OrderModel order = process.getOrder();

		if (order != null)
		{
			if (!ExportStatus.EXPORTED.equals(order.getExportStatus()))
			{
				try
				{
					orderIntegrationService.publishNewOrder(order);
				}
				catch (final Exception e)
				{
					LOG.error("Order Export failed", e);
				}
				getModelService().refresh(order);
				if (ExportStatus.EXPORTED.equals(order.getExportStatus()))
				{
					return Transition.OK;
				}

				BusinessProcessParameterModel parameterModel = null;

				if (getProcessParameterHelper().containsParameter(process, "retryCount"))
				{
					parameterModel = getProcessParameterHelper().getProcessParameterByName(process, "retryCount");
				}

				Integer retryCount = (null != parameterModel && null != parameterModel.getValue()) ? (Integer) parameterModel
						.getValue() : 1;

				if (null != retryCount && retryCount.intValue() <= Config.getInt("esb.order.retry.max.count", 5))
				{
					retryCount = retryCount + 1;
					getProcessParameterHelper().setProcessParameter(process, "retryCount", retryCount);
					final RetryLaterException retry = new RetryLaterException();
					retry.setDelay(1000L * 60L * Config.getLong("esb.order.retry.interval", 60));
					LOG.info("retry delay " + retry.getDelay());
					retry.setMethod(Method.LINEAR);
					retry.setRollBack(false);
					throw retry;
				}
				else
				{
					setOrderStatus(order, OrderStatus.PROCESSING_ERROR);
				}

			}
		}
		return Transition.NOK;
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
