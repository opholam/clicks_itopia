/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.fulfilmentprocess.actions.order;

import de.hybris.clicks.fulfilmentprocess.stock.service.ClicksStockService;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


public class SendOrderPlacedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendOrderPlacedNotificationAction.class);

	private EventService eventService;

	private ClicksStockService clicksStockService;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		getEventService().publishEvent(new OrderPlacedEvent(process));
		LOG.info("Process: " + process.getCode() + " in step " + getClass());
		clicksStockService.releaseAndDetuctStockForOrder(process.getOrder());
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the clicksStockService
	 */
	public ClicksStockService getClicksStockService()
	{
		return clicksStockService;
	}

	/**
	 * @param clicksStockService
	 *           the clicksStockService to set
	 */
	public void setClicksStockService(final ClicksStockService clicksStockService)
	{
		this.clicksStockService = clicksStockService;
	}
}
