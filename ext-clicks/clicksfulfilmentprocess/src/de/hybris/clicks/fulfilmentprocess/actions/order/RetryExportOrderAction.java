/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.util.Config;

import java.util.Date;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;


/**
 * This action implements payment authorization using {@link CreditCardPaymentInfoModel}. Any other payment model could
 * be implemented here, or in a separate action, if the process flow differs.
 */
public class RetryExportOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private final static Logger LOG = Logger.getLogger(RetryExportOrderAction.class);
	private BusinessProcessService businessProcessService;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();
		if (order != null && !ExportStatus.EXPORTED.equals(order.getExportStatus()))
		{
			BusinessProcessParameterModel parameterModel = CollectionUtils.isNotEmpty(process.getContextParameters()) ? (BusinessProcessParameterModel) CollectionUtils
					.find(process.getContextParameters(), new BeanPropertyValueEqualsPredicate("name", "retryCount", true)) : null;
			if (null == parameterModel)
			{
				parameterModel = new BusinessProcessParameterModel();
				parameterModel.setName("retryCount");
				parameterModel.setValue(1);
				parameterModel.setProcess(process);
				getModelService().save(parameterModel);
			}
			Integer retryCount = (Integer) parameterModel.getValue();

			if (null != retryCount && retryCount.intValue() <= Config.getInt("esb.order.retry.max.count", 5))
			{
				LOG.error("Retrying order export to ESB for " + retryCount + " time for order " + order.getClicksOrderCode());
				businessProcessService.triggerEvent("ExportOrderEvent_" + order.getClicksOrderCode(),
						DateUtils.addMinutes(new Date(), Config.getInt("esb.order.retry.interval", 60)));
				retryCount = retryCount + 1;
				parameterModel.setValue(retryCount);
				getModelService().save(parameterModel);
				return Transition.OK;
			}
		}
		else
		{
			LOG.error("Order already exported for process code " + process.getCode());
			return Transition.NOK;
		}
		order.setStatus(OrderStatus.PROCESSING_ERROR);
		getModelService().save(order);
		return Transition.NOK;
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
