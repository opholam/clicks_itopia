/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.fulfilmentprocess.actions.order;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.apache.log4j.Logger;


/**
 *
 */
public class SubprocessesCompletedAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SubprocessesCompletedAction.class);

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		if (process.getOrder().getDeliveryAddress().getIsStore().booleanValue())
		{
			for (final ConsignmentModel consign : process.getOrder().getConsignments())
			{
				if (ConsignmentStatus.CANCELLED.equals(consign.getStatus()))
				{
					continue;
				}
				if (!ConsignmentStatus.PICKUP_COMPLETE.equals(consign.getStatus()))
				{
					return Transition.NOK;
				}
			}
			return Transition.OK;
		}
		else
		{
			for (final ConsignmentModel consign : process.getOrder().getConsignments())
			{
				if (ConsignmentStatus.CANCELLED.equals(consign.getStatus()))
				{
					continue;
				}
				if (!ConsignmentStatus.DELIVERED.equals(consign.getStatus()))
				{
					return Transition.NOK;
				}
			}
			return Transition.OK;
		}
	}
}
