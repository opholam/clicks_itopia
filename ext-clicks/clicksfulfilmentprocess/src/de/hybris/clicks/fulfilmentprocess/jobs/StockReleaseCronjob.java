/**
 *
 */
package de.hybris.clicks.fulfilmentprocess.jobs;

import de.hybris.clicks.fulfilmentprocess.stock.service.ClicksStockService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskService;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author manikandan r
 *
 */
public class StockReleaseCronjob extends AbstractJobPerformable<CronJobModel>
{

	private static final Logger LOG = Logger.getLogger(StockReleaseCronjob.class);
	private ConfigurationService configurationService;
	private ClicksStockService clicksStockService;
	@Resource(name = "taskService")
	private TaskService taskService;

	@Override
	public PerformResult perform(final CronJobModel job)
	{
		final String cartId = job.getCode();
		final long sleep = configurationService.getConfiguration().getLong("stock.reserve.duration", 180000l);

		try
		{
			job.setStatus(CronJobStatus.PAUSED);
			modelService.save(job);
			Thread.sleep(sleep);
		}
		catch (final InterruptedException e)
		{
			LOG.error("Stock release thread interrupted");
		}
		job.setStatus(CronJobStatus.RUNNING);
		modelService.save(job);
		try
		{
			final TaskModel taskModel = new TaskModel();
			taskModel.setRunnerBean("payUConfirmationTaskRunner");
			CartModel cartModel = new CartModel();
			cartModel.setCode(cartId);
			cartModel = flexibleSearchService.getModelByExample(cartModel);
			taskModel.setContextItem(cartModel);
			taskModel.setExecutionDate(new Date());
			modelService.save(taskModel);
			taskService.scheduleTask(taskModel);
			modelService.refresh(cartModel);
			if (null != cartModel)
			{
				clicksStockService.releaseStockForCart(cartId);
			}
		}
		catch (final Exception e)
		{
			//clicksStockService.releaseStockForCart(cartId);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @return the clicksStockService
	 */
	public ClicksStockService getClicksStockService()
	{
		return clicksStockService;
	}

	/**
	 * @param clicksStockService
	 *           the clicksStockService to set
	 */
	public void setClicksStockService(final ClicksStockService clicksStockService)
	{
		this.clicksStockService = clicksStockService;
	}

	/**
	 * @return the taskService
	 */
	public TaskService getTaskService()
	{
		return taskService;
	}

	/**
	 * @param taskService
	 *           the taskService to set
	 */
	public void setTaskService(final TaskService taskService)
	{
		this.taskService = taskService;
	}


}
