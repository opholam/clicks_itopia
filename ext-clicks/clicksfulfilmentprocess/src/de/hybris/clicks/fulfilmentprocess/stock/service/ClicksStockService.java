/**
 *
 */
package de.hybris.clicks.fulfilmentprocess.stock.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;


/**
 * @author manikandan.r
 *
 */
public interface ClicksStockService
{

	/**
	 * @param order
	 */
	void releaseAndDetuctStockForOrder(OrderModel order);

	/**
	 * @param cartId
	 */
	void releaseStockForCart(String cartId);


	/**
	 * @throws InsufficientStockLevelException
	 */
	void reserveStockForCart() throws InsufficientStockLevelException;

	/**
	 *
	 */
	void releaseStockForSessionCart();

}
