/**
 *
 */
package de.hybris.clicks.fulfilmentprocess.stock.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.stock.exception.InsufficientStockLevelException;
import de.hybris.platform.stock.impl.StockLevelDao;
import de.hybris.platform.stock.strategy.StockLevelProductStrategy;

import org.apache.log4j.Logger;


/**
 * @author manikandan.r
 * 
 */
public class ClicksStockServiceImpl implements ClicksStockService
{

	private StockService stockService;
	private WarehouseService warehouseService;
	private CartService cartService;
	private StockLevelDao stockLevelDao;
	private StockLevelProductStrategy stockLevelProductStrategy;
	private static final Logger LOG = Logger.getLogger(ClicksStockServiceImpl.class);
	private FlexibleSearchService flexibleSearchService;
	private CronJobService cronJobService;
	private ModelService modelService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facade.stock.ClicksStockFacade#reserveStockForCart()
	 */
	@Override
	public void reserveStockForCart() throws InsufficientStockLevelException
	{
		if (cartService.hasSessionCart())
		{
			final CartModel cart = cartService.getSessionCart();
			final WarehouseModel warehouseModel = warehouseService.getWarehouseForCode("9000");
			CronJobModel job = null;
			try
			{
				job = cronJobService.getCronJob(cart.getCode());
			}
			catch (final Exception e)
			{
				//LOG.info("");
			}
			if (job != null)
			{
				if (cronJobService.isPaused(job))
				{
					LOG.info("Stock reserved already");
					modelService.remove(job);
					job = createStockReleaseJob(cart);
					cronJobService.performCronJob(job);
					return;
				}
			}
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				stockService.reserve(entry.getProduct(), warehouseModel, entry.getQuantity().intValue(), "Stock reserved for "
						+ entry.getProduct().getCode());
			}
			job = createStockReleaseJob(cart);
			cronJobService.performCronJob(job);
		}
	}

	private CronJobModel createStockReleaseJob(final CartModel cart)
	{
		final CronJobModel job = modelService.create(CronJobModel.class);
		job.setCode(cart.getCode());
		job.setJob(cronJobService.getJob("stockReleaseJob"));
		job.setRemoveOnExit(Boolean.TRUE);
		job.setRetry(Boolean.FALSE);
		modelService.save(job);
		modelService.refresh(job);
		return job;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.facade.stock.ClicksStockFacade#releaseStockForCart()
	 */
	@Override
	public void releaseStockForCart(final String cartId)
	{
		if (cartId != null)
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {pk} FROM {AbstractOrder} where {code}=?cartId");
			query.addQueryParameter("cartId", cartId);
			try
			{
				query.setDisableSearchRestrictions(true);
				final AbstractOrderModel cart = flexibleSearchService.searchUnique(query);
				final WarehouseModel warehouseModel = warehouseService.getWarehouseForCode("9000");
				for (final AbstractOrderEntryModel entry : cart.getEntries())
				{
					try
					{
						stockService.release(entry.getProduct(), warehouseModel, entry.getQuantity().intValue(), "Stock released for "
								+ entry.getProduct().getCode());
					}
					catch (final Exception e)
					{
						LOG.error("Stock released failed", e);
					}
				}
			//	cart.setBasketEditable(Boolean.TRUE);
			//	getModelService().save(cart);
			}
			catch (final Exception e)
			{
				LOG.error("Stock released failed", e);
			}
		}
	}

	@Override
	public void releaseStockForSessionCart()
	{
		if (cartService.hasSessionCart())
		{
			final AbstractOrderModel cart = cartService.getSessionCart();
			final WarehouseModel warehouseModel = warehouseService.getWarehouseForCode("9000");
			CronJobModel job = null;
			try
			{
				job = cronJobService.getCronJob(cart.getCode());
				if (cronJobService.isPaused(job))
				{
					modelService.remove(job);
				}
			}
			catch (final Exception e)
			{
				//LOG.info("");
			}
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				try
				{
					stockService.release(entry.getProduct(), warehouseModel, entry.getQuantity().intValue(), "Stock released for "
							+ entry.getProduct().getCode());
				}
				catch (final Exception e)
				{
					LOG.error("Stock released failed", e);
				}
			}
		}
	}


	@Override
	public void releaseAndDetuctStockForOrder(final OrderModel order)
	{
		if (order != null)
		{
			final WarehouseModel warehouseModel = warehouseService.getWarehouseForCode("9000");
			for (final AbstractOrderEntryModel entry : order.getEntries())
			{
				try
				{
					//					stockService.release(entry.getProduct(), warehouseModel, entry.getQuantity().intValue(),
					//							"Stock released for " + entry.getProduct().getCode());
					final int actual = checkAndGetStockLevel(entry.getProduct(), warehouseModel);
					stockService.updateActualStockLevel(entry.getProduct(), warehouseModel, actual - entry.getQuantity().intValue(),
							"Stock deducted for " + entry.getProduct().getCode());
				}
				catch (final Exception e)
				{
					LOG.error("Stock released and deduct failed", e);
				}
			}
		}
	}

	private int checkAndGetStockLevel(final ProductModel product, final WarehouseModel warehouse)
	{
		final StockLevelModel stockLevel = this.stockLevelDao.findStockLevel(this.stockLevelProductStrategy.convert(product),
				warehouse);
		if (stockLevel != null)
		{
			return stockLevel.getAvailable();
		}
		return 0;
	}

	/**
	 * @return the stockService
	 */
	public StockService getStockService()
	{
		return stockService;
	}

	/**
	 * @param stockService
	 *           the stockService to set
	 */
	public void setStockService(final StockService stockService)
	{
		this.stockService = stockService;
	}

	/**
	 * @return the warehouseService
	 */
	public WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	/**
	 * @param warehouseService
	 *           the warehouseService to set
	 */
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the stockLevelDao
	 */
	public StockLevelDao getStockLevelDao()
	{
		return stockLevelDao;
	}

	/**
	 * @param stockLevelDao
	 *           the stockLevelDao to set
	 */
	public void setStockLevelDao(final StockLevelDao stockLevelDao)
	{
		this.stockLevelDao = stockLevelDao;
	}

	/**
	 * @return the stockLevelProductStrategy
	 */
	public StockLevelProductStrategy getStockLevelProductStrategy()
	{
		return stockLevelProductStrategy;
	}

	/**
	 * @param stockLevelProductStrategy
	 *           the stockLevelProductStrategy to set
	 */
	public void setStockLevelProductStrategy(final StockLevelProductStrategy stockLevelProductStrategy)
	{
		this.stockLevelProductStrategy = stockLevelProductStrategy;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the cronJobService
	 */
	public CronJobService getCronJobService()
	{
		return cronJobService;
	}

	/**
	 * @param cronJobService
	 *           the cronJobService to set
	 */
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


}
