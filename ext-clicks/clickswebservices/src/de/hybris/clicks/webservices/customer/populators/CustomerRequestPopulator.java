/**
 *
 */
package de.hybris.clicks.webservices.customer.populators;

import de.hybris.clicks.webservices.customer.schemas.Customer;
import de.hybris.clicks.webservices.customer.schemas.Customer.CoreDetails;
import de.hybris.clicks.webservices.customer.schemas.Customer.CoreDetails.Addresses;
import de.hybris.clicks.webservices.customer.schemas.Customer.CoreDetails.Addresses.Address;
import de.hybris.clicks.webservices.customer.schemas.Customer.CoreDetails.ContactDetails;
import de.hybris.clicks.webservices.customer.schemas.Customer.CoreDetails.ContactDetails.ContactDetail;
import de.hybris.clicks.webservices.customer.schemas.Customer.Hybris;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns.MarketingConsent;
import de.hybris.clicks.webservices.customer.schemas.PublishCustomer;
import de.hybris.clicks.webservices.customer.schemas.PublishCustomer.Body;
import de.hybris.clicks.webservices.customer.schemas.PublishCustomer.Header;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;




/**
 * @author abhaydh
 *
 */
public class CustomerRequestPopulator
{

	public PublishCustomer populate(final CustomerData customerData)
	{
		final PublishCustomer publishCustomer = new PublishCustomer();
		final Body body = new Body();
		final Header header = new Header();
		final Customer customer = new Customer();
		final CoreDetails coreDetails = new CoreDetails();
		final Hybris customerHybris = new Hybris();
		final Loyalty customerLoyalty = new Loyalty();

		header.setEventCode("1");
		publishCustomer.setHeader(header);

		if (null != customerData.getSAResident())
		{
			coreDetails.setSAResident(customerData.getSAResident());
		}
		if (null != customerData.getIDnumber())
		{
			coreDetails.setIDNumber(customerData.getIDnumber());
		}
		if (null != customerData.getFirstName())
		{
			coreDetails.setFirstName(customerData.getFirstName());
		}
		if (null != customerData.getLastName())
		{
			coreDetails.setLastName(customerData.getLastName());
		}
		if (null != customerData.getEmailID())
		{
			coreDetails.setEMailAddress(customerData.getEmailID());
		}
		if (null != customerData.getPreferedName())
		{
			coreDetails.setPreferedName(customerData.getPreferedName());
		}
		if (null != customerData.getGender())
		{
			if (customerData.getGender().equals("1"))
			{
				coreDetails.setGender(new Integer(1));
			}
			if (customerData.getGender().equals("2"))
			{
				coreDetails.setGender(new Integer(2));
			}
		}

		final Addresses addressList = new Addresses();
		final Address addresses = new Address();

		for (final AddressData address : customerData.getAddresses())
		{
			if (null != address.getLine1())
			{
				addresses.setAddressLine1(address.getLine1());
			}
			if (null != address.getTypeID())
			{
				addresses.setAddressTypeId(address.getTypeID());
			}
			if (null != address.getCity())
			{
				addresses.setCity(address.getCity());
			}
			if (null != address.getCountry())
			{
				addresses.setCountry(address.getCountry().getIsocode());
			}
			if (null != address.getPostalCode())
			{
				addresses.setPostalCode(address.getPostalCode());
			}
			if (null != address.getProvince())
			{
				addresses.setProvince(address.getProvince());
			}
			if (null != address.getSuburb())
			{
				addresses.setSuburb(address.getSuburb());
			}
			addressList.getAddress().add(addresses);

		}

		coreDetails.setAddresses(addressList);

		// array size for contacts here should be exactly same as number of contacts. extra size will cause error in esb webservice
		final ContactDetails contactsArray = new ContactDetails();

		final ContactDetail contact1 = new ContactDetail();
		contact1.setTypeId("2");
		contact1.setNumber("0820413023");
		contactsArray.getContactDetail().add(contact1);

		final ContactDetail contact2 = new ContactDetail();
		contact2.setTypeId("4");
		contact2.setNumber("0119434912");
		contactsArray.getContactDetail().add(contact2);

		coreDetails.setContactDetails(contactsArray);
		customer.setAction("I");
		customer.setCoreDetails(coreDetails);



		if (null != customerData.getRetailerID_hybris())
		{
			customerHybris.setRetailerId(customerData.getRetailerID_hybris());
		}
		if (null != customerData.getCustomerID())
		{
			customerHybris.setCustomerId(customerData.getCustomerID());
		}
		customer.setHybris(customerHybris);



		final OptIns optIns = new OptIns();
		final MarketingConsent marketingConsent = new MarketingConsent();


		//		if (null != customerData.getMarketingConsent_email())
		//		{
		//			marketingConsent.setEmail(customerData.getMarketingConsent_email().booleanValue());
		//		}
		//		if (null != customerData.getMarketingConsent_SMS())
		//		{
		//			marketingConsent.setSMS(customerData.getMarketingConsent_SMS().booleanValue());
		//		}
		optIns.setMarketingConsent(marketingConsent);

		//		if (null != customerData.getBabyClub())
		//		{
		//			final BabyClub babyClub = new BabyClub();
		//			final AlreadyParent alreadyParent = new AlreadyParent();
		//			final Children children = new Children();
		//			final Child child = new Child();
		//
		//			final BabyClubData babyClubData = customerData.getBabyClub();
		//
		//			if (null != babyClubData.getChildDOB())
		//			{
		//				child.setDateOfBirth(babyClubData.getChildDOB());
		//			}
		//			if (null != babyClubData.getFirstName())
		//			{
		//				child.setFirstName(babyClubData.getFirstName());
		//			}
		//			if (null != babyClubData.getGender())
		//			{
		//				if (babyClubData.getGender().equals("1"))
		//				{
		//					child.setGender(new Integer(1));
		//				}
		//				if (babyClubData.getGender().equals("2"))
		//				{
		//					child.setGender(new Integer(2));
		//				}
		//			}
		//			if (null != babyClubData.getLastName())
		//			{
		//				child.setSurname(babyClubData.getLastName());
		//			}
		//
		//			children.setChild(child);
		//			alreadyParent.setChildren(children);
		//			babyClub.setAlreadyParent(alreadyParent);
		//			optIns.setBabyClub(babyClub);
		//		}

		customerLoyalty.setOptIns(optIns);
		customer.setLoyalty(customerLoyalty);

		body.getCustomer().add(customer);
		publishCustomer.setBody(body);

		return publishCustomer;
	}

}
