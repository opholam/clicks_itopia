/**
 *
 */
package de.hybris.clicks.webservices.customer.populators;

import de.hybris.clicks.webservices.customer.schemas.Customer.NonLoyalty;
import de.hybris.clicks.webservices.customer.schemas.Customer.NonLoyalty.Segments.Segment;
import de.hybris.clicks.webservices.customer.schemas.Legend;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class CustomerNonLoyaltyDetailsPopulator
{

	private static final Logger LOG = Logger.getLogger(CustomerNonLoyaltyDetailsPopulator.class);

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;


	/**
	 * @param nonLoyalty
	 * @param customerModel
	 * @return
	 */
	public CustomerModel populate(final NonLoyalty nonLoyalty, final CustomerModel customerModel, final Legend legend)
	{

		if (null != nonLoyalty.getRetailerId())
		{
			customerModel.setRetailerID_NonLoyalty(nonLoyalty.getRetailerId());
		}

		if (null != nonLoyalty.getSegments() && null != nonLoyalty.getSegments().getSegment()
				&& nonLoyalty.getSegments().getSegment().size() > 0)
		{
			final List<UserGroupModel> segmentModelList = new ArrayList<UserGroupModel>();
			List<UserGroupModel> segmentModelList2 = new ArrayList<UserGroupModel>();
			if (null != customerModel.getNonLoyalty_segments())
			{
				segmentModelList2 = customerModel.getNonLoyalty_segments();
				segmentModelList.addAll(segmentModelList2);
			}
			UserGroupModel userGroupModel = null;
			boolean isUpdate = false;
			for (final Segment segment : nonLoyalty.getSegments().getSegment())
			{
				userGroupModel = modelService.create(UserGroupModel.class);
				isUpdate = false;
				if (!CollectionUtils.isEmpty(segmentModelList2))
				{
					for (final UserGroupModel userModel : segmentModelList2)
					{
						if (userModel.getUid().equals(new Integer(segment.getSegmentId()).toString()))
						{
							isUpdate = true;
						}
					}
				}
				if (segment.getSegmentId() != 0)
				{
					userGroupModel.setUid(new Integer(segment.getSegmentId()).toString());
				}
				try
				{
					userGroupModel = flexibleSearchService.getModelByExample(userGroupModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				userGroupModel = populateSegments(segment, userGroupModel, legend);
				if (!isUpdate)
				{
					segmentModelList.add(userGroupModel);
				}
			}

			if (segmentModelList.size() > 0)
			{
				customerModel.setNonLoyalty_segments(segmentModelList);
			}

		}
		modelService.save(customerModel);
		return customerModel;
	}


	private UserGroupModel populateSegments(final Segment source, final UserGroupModel target, final Legend legend)
	{

		try
		{
			if (null != legend && null != source && null != legend.getSegments()
					&& CollectionUtils.isNotEmpty(legend.getSegments().getSegment()))
			{
				de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment segmentObj = new de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment();

				segmentObj = (de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment) CollectionUtils.find(legend
						.getSegments().getSegment(), new BeanPropertyValueEqualsPredicate("segmentId", source.getSegmentId(), true));

				if (StringUtils.isNotBlank(segmentObj.getSegmentDescription()))
				{
					target.setName(segmentObj.getSegmentDescription());
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
		}

		if (source.getSegmentId() != 0)
		{
			target.setUid(new Integer(source.getSegmentId()).toString());
		}

		if (null != source.getStatus())
		{
			target.setSegmentStatus(source.getStatus());
		}
		if (null != source.getStartDate())
		{
			target.setStartDate(source.getStartDate().toGregorianCalendar().getTime());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate().toGregorianCalendar().getTime());
		}

		return target;
	}

}
