/**
 *
 */
package de.hybris.clicks.webservices.customer.populators;

import de.hybris.clicks.core.model.AccountsCBRModel;
import de.hybris.clicks.core.model.AccountsPSModel;
import de.hybris.clicks.core.model.BabyClubModel;
import de.hybris.clicks.core.model.BenefitModel;
import de.hybris.clicks.core.model.CBRstatementModel;
import de.hybris.clicks.core.model.CBRtransactionModel;
import de.hybris.clicks.core.model.ChildModel;
import de.hybris.clicks.core.model.ConsentModel;
import de.hybris.clicks.core.model.PS_PartnerPointModel;
import de.hybris.clicks.core.model.PS_PartnerPointsModel;
import de.hybris.clicks.core.model.PS_PointsBucketModel;
import de.hybris.clicks.core.model.PS_PointsBucketsModel;
import de.hybris.clicks.core.model.PStransactionModel;
import de.hybris.clicks.core.model.PStransactionsModel;
import de.hybris.clicks.core.model.PointsStatementsModel;
import de.hybris.clicks.core.model.Type_consentModel;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.CBRStatement.Accounts.Account;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.CBRStatement.Accounts.Account.Transactions.Transaction;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.Coupons.Coupon;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns.BabyClub;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns.BabyClub.AlreadyParent.Children.Child;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns.FinancialServices.Benefit;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns.MarketingConsent.Consent;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.OptIns.MarketingConsent.Consent.Comms.Type;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.PointsStatement.Accounts.Account.PartnerPoints.Partner;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.PointsStatement.Accounts.Account.PointsBuckets.Bucket;
import de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.Segments.Segment;
import de.hybris.clicks.webservices.customer.schemas.Legend;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;




/**
 * @author abhaydh
 *
 */
public class CustomerLoyaltyDetailsPopulator
{

	private static final Logger LOG = Logger.getLogger(CustomerLoyaltyDetailsPopulator.class);

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource(name = "myNumberseriesGenerator2")
	private PersistentKeyGenerator myNumberseriesGenerator2;

	public CustomerModel populate(final Loyalty loyalty, final CustomerModel customerModel, final Legend legend)
	{

		if (null != loyalty.getRetailerId())
		{
			customerModel.setRetailerID_Loyalty(loyalty.getRetailerId());
		}
		if (null != loyalty.getHouseholdId())
		{
			customerModel.setHouseholdID(loyalty.getHouseholdId());
		}
		//		if (null != loyalty.getMemberId())
		//		{
		//			customerModel.setCustomerID(loyalty.getMemberId());
		//		}
		if (null != loyalty.getMemberId())
		{
			customerModel.setMemberID(loyalty.getMemberId());
		}
		if (null != loyalty.getIsMainMember())
		{
			customerModel.setIsMainMember(loyalty.getIsMainMember());
		}

		if (null != loyalty.getOptIns())
		{

			if (null != loyalty.getOptIns().getMarketingConsent() && null != loyalty.getOptIns().getMarketingConsent().getConsent()
					&& loyalty.getOptIns().getMarketingConsent().getConsent().size() > 0)
			{
				final List<ConsentModel> consentModelList = new ArrayList<ConsentModel>();
				for (final Consent consent : loyalty.getOptIns().getMarketingConsent().getConsent())
				{
					final ConsentModel consentModel = new ConsentModel();
					if (null != consent.getConsentType())
					{
						consentModel.setConsentType(consent.getConsentType());
					}

					if (null != consent.getComms() && null != consent.getComms().getType() && consent.getComms().getType().size() > 0)
					{
						final List<Type_consentModel> typeModelList = new ArrayList<Type_consentModel>();
						for (final Type type : consent.getComms().getType())
						{
							final Type_consentModel typeModel = new Type_consentModel();
							if (type.getTypeId() != 0)
							{
								typeModel.setTypeID(new Integer(type.getTypeId()));
							}
							if (type.isAccepted())
							{
								typeModel.setAccepted(new Boolean(type.isAccepted()));
							}
							typeModelList.add(typeModel);
						}
						consentModel.setComms(typeModelList);
					}
					consentModelList.add(consentModel);
				}

				customerModel.setMarketingConsent(consentModelList);
				modelService.save(customerModel);
			}

			if (null != loyalty.getOptIns().getFinancialServices()
					&& null != loyalty.getOptIns().getFinancialServices().getBenefit()
					&& loyalty.getOptIns().getFinancialServices().getBenefit().size() > 0)
			{
				final List<BenefitModel> benefitModelList = new ArrayList<BenefitModel>();
				BenefitModel benefitModel;
				for (final Benefit benefit : loyalty.getOptIns().getFinancialServices().getBenefit())
				{
					benefitModel = modelService.create(BenefitModel.class);
					if (null != benefit.getBenefitId())
					{
						benefitModel.setBenefitId(benefit.getBenefitId());
					}
					benefitModel.setStatus(new Integer(benefit.getStatus()));
					if (null != benefit.getBenefitAmount())
					{
						benefitModel.setBenefitAmount(new Double(benefit.getBenefitAmount().doubleValue()));
					}
					if (null != benefit.getBenefitDate())
					{
						benefitModel.setBenefitDate(benefit.getBenefitDate().toGregorianCalendar().getTime());
					}
					benefitModelList.add(benefitModel);
				}
				if (benefitModelList.size() > 0)
				{
					customerModel.setFinancialServices(benefitModelList);
				}
			}

		}
		if (null != loyalty.getMagazine())
		{
			customerModel.setMagazineStatus(new Integer(loyalty.getMagazine().getStatus()));
		}

		modelService.save(customerModel);


		if (null != loyalty.getCBRStatement())
		{
			final CBRstatementModel cBRstatementModel = modelService.create(CBRstatementModel.class);
			if (null != loyalty.getCBRStatement().getAccounts())
			{
				if (null != loyalty.getCBRStatement().getAccounts().getAccount()
						&& loyalty.getCBRStatement().getAccounts().getAccount().size() > 0)
				{
					final List<AccountsCBRModel> accountsCBRModelList = new ArrayList<AccountsCBRModel>();
					for (final Account account : loyalty.getCBRStatement().getAccounts().getAccount())
					{
						final AccountsCBRModel accountsCBRModel = modelService.create(AccountsCBRModel.class);

						if (account.getAccountId() != 0)
						{
							accountsCBRModel.setAccountID(new Integer(account.getAccountId()));
						}
						if (null != account.getCBRAvailableBalance())
						{
							accountsCBRModel.setCBRbalance(new Double(account.getCBRAvailableBalance().doubleValue()));
						}
						if (null != account.getTransactions())
						{
							if (null != account.getTransactions().getTransaction()
									&& account.getTransactions().getTransaction().size() > 0)
							{
								final List<CBRtransactionModel> cBRtransactionList = new ArrayList<CBRtransactionModel>();

								for (final Transaction transaction : account.getTransactions().getTransaction())
								{
									final CBRtransactionModel cBRtransactionModel = modelService.create(CBRtransactionModel.class);

									if (null != transaction.getAmountIssued())
									{
										cBRtransactionModel.setAmountIssued(new Double(transaction.getAmountIssued().doubleValue()));
									}
									if (null != transaction.getAvailableAmount())
									{
										cBRtransactionModel.setAvailableAmount(new Double(transaction.getAvailableAmount().doubleValue()));
									}
									if (null != transaction.getExpiryDate())
									{
										cBRtransactionModel.setExpiryDate(transaction.getExpiryDate().toGregorianCalendar().getTime());
									}
									if (null != transaction.getIssueDate())
									{
										cBRtransactionModel.setIssueDate(transaction.getIssueDate().toGregorianCalendar().getTime());
									}
									cBRtransactionModel.setOwner(customerModel);
									cBRtransactionList.add(cBRtransactionModel);
								}
								accountsCBRModel.setCBRtransactionList(cBRtransactionList);
							}
						}
						accountsCBRModelList.add(accountsCBRModel);
					}
					cBRstatementModel.setOwner(customerModel);
					cBRstatementModel.setAccountsCBRList(accountsCBRModelList);
				}
			}
			modelService.save(cBRstatementModel);
			customerModel.setCBRstatement(cBRstatementModel);
			modelService.save(customerModel);
		}
		try
		{
			if (null != loyalty.getPointsStatement())
			{
				final PointsStatementsModel pointsStatementModel = modelService.create(PointsStatementsModel.class);
				if (null != loyalty.getPointsStatement().getEndDate())
				{
					pointsStatementModel.setEndDate(loyalty.getPointsStatement().getEndDate().toGregorianCalendar().getTime());
				}
				if (null != loyalty.getPointsStatement().getStartDate())
				{
					pointsStatementModel.setStartDate(loyalty.getPointsStatement().getStartDate().toGregorianCalendar().getTime());
				}
				if (null != loyalty.getPointsStatement().getAccounts())
				{
					if (null != loyalty.getPointsStatement().getAccounts().getAccount()
							&& loyalty.getPointsStatement().getAccounts().getAccount().size() > 0)
					{
						final List<AccountsPSModel> accountsPSModelList = new ArrayList<AccountsPSModel>();
						for (final de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.PointsStatement.Accounts.Account account : loyalty
								.getPointsStatement().getAccounts().getAccount())
						{
							final AccountsPSModel accountsPSModel = modelService.create(AccountsPSModel.class);

							if (account.getAccountId() != 0)
							{
								accountsPSModel.setAccountID(new Integer(account.getAccountId()));
							}
							if (null != account.getPointsBalance())
							{
								accountsPSModel.setPointsBalance(account.getPointsBalance());
							}
							if (null != account.getPointsToQualify())
							{
								accountsPSModel.setPointsToQualify(account.getPointsToQualify());
							}
							if (null != account.getTotalSpend())
							{
								accountsPSModel.setTotalSpent(new Double(account.getTotalSpend().doubleValue()));
							}

							if (null != account.getTransactions())
							{
								final PStransactionsModel pStransactionsModel = modelService.create(PStransactionsModel.class);
								if (null != account.getTransactionsPointsBalance())
								{
									pStransactionsModel.setBalance(account.getTransactionsPointsBalance());
								}
								if (null != account.getTransactions().getTransaction()
										&& account.getTransactions().getTransaction().size() > 0)
								{
									final List<PStransactionModel> transactionList = new ArrayList<PStransactionModel>();
									for (final de.hybris.clicks.webservices.customer.schemas.Customer.Loyalty.PointsStatement.Accounts.Account.Transactions.Transaction transaction : account
											.getTransactions().getTransaction())
									{
										final PStransactionModel pStransactionModel = modelService.create(PStransactionModel.class);
										if (null != transaction.getEarned())
										{
											pStransactionModel.setEarned(transaction.getEarned());
										}
										if (null != transaction.getDate())
										{
											pStransactionModel.setDate(transaction.getDate().toGregorianCalendar().getTime());
										}
										if (null != transaction.getLocation())
										{
											pStransactionModel.setLocation(transaction.getLocation());
										}
										if (null != transaction.getSpent())
										{
											pStransactionModel.setSpent(new Double(transaction.getSpent().doubleValue()));
										}
										modelService.save(pStransactionModel);
										transactionList.add(pStransactionModel);
									}
									pStransactionsModel.setOwner(customerModel);
									pStransactionsModel.setPStransactionList(transactionList);
									modelService.save(pStransactionsModel);
								}
								accountsPSModel.setPStransactions(pStransactionsModel);
								modelService.save(accountsPSModel);
							}

							if (null != account.getPartnerPoints())
							{
								final PS_PartnerPointsModel partnerPointsModel = modelService.create(PS_PartnerPointsModel.class);
								if (null != account.getPartnerPointsBalance())
								{
									partnerPointsModel.setBalance(account.getPartnerPointsBalance());
								}
								if (null != account.getPartnerPoints().getPartner() && account.getPartnerPoints().getPartner().size() > 0)
								{
									final List<PS_PartnerPointModel> pointsList = new ArrayList<PS_PartnerPointModel>();
									for (final Partner partnerPoint : account.getPartnerPoints().getPartner())
									{
										final PS_PartnerPointModel partnerPointModel = modelService.create(PS_PartnerPointModel.class);

										if (null != partnerPoint.getName())
										{
											partnerPointModel.setName(partnerPoint.getName());
										}
										if (null != partnerPoint.getValue())
										{
											partnerPointModel.setValue(partnerPoint.getValue());
										}
										modelService.save(partnerPointModel);
										pointsList.add(partnerPointModel);
									}
									partnerPointsModel.setPS_PartnerPointList(pointsList);
								}
								modelService.save(partnerPointsModel);
								accountsPSModel.setPartnerPoints(partnerPointsModel);
							}

							if (null != account.getPointsBuckets())
							{

								final PS_PointsBucketsModel pointsBucketsModel = modelService.create(PS_PointsBucketsModel.class);
								if (null != account.getPointsBucketsBalance())
								{
									pointsBucketsModel.setBalance(account.getPointsBucketsBalance());
								}
								if (null != account.getPointsBuckets().getBucket() && account.getPointsBuckets().getBucket().size() > 0)
								{
									final List<PS_PointsBucketModel> pointsBucketList = new ArrayList<PS_PointsBucketModel>();
									for (final Bucket pointsBucket : account.getPointsBuckets().getBucket())
									{
										final PS_PointsBucketModel pointsBucketModel = modelService.create(PS_PointsBucketModel.class);

										if (null != pointsBucket.getName())
										{
											pointsBucketModel.setName(pointsBucket.getName());
										}
										if (null != pointsBucket.getValue())
										{
											pointsBucketModel.setValue(pointsBucket.getValue());
										}
										modelService.save(pointsBucketModel);
										pointsBucketList.add(pointsBucketModel);
									}
									pointsBucketsModel.setPS_PointsBucketList(pointsBucketList);
								}
								modelService.save(pointsBucketsModel);
								accountsPSModel.setPointsBuckets(pointsBucketsModel);

							}

							modelService.save(accountsPSModel);
							accountsPSModelList.add(accountsPSModel);
						}
						pointsStatementModel.setAccountsPSList(accountsPSModelList);
					}

				}

				modelService.save(pointsStatementModel);
				customerModel.setPointsStatement(pointsStatementModel);
				modelService.save(customerModel);
			}
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		if (null != loyalty.getOptIns() && null != loyalty.getOptIns().getBabyClub())
		{
			final List<BabyClubModel> babyClubList = new ArrayList<BabyClubModel>();
			BabyClubModel babyClubModel = new BabyClubModel();
			babyClubModel = populateBabyClub(loyalty.getOptIns().getBabyClub(), babyClubModel);
			babyClubList.add(babyClubModel);
			customerModel.setBabyClub(babyClubList);
		}

		if (null != loyalty.getCoupons() && null != loyalty.getCoupons().getCoupon())
		{
			final List<VoucherModel> voucherList = new ArrayList<VoucherModel>();
			PromotionVoucherModel voucher = modelService.create(PromotionVoucherModel.class);
			voucher = populateVoucher(loyalty.getCoupons().getCoupon(), voucher);
			voucherList.add(voucher);
			if (voucherList.size() > 0)
			{
				customerModel.setCoupons(voucherList);
			}
		}


		final Set<PrincipalGroupModel> userGroups = new HashSet<PrincipalGroupModel>();

		if (null != loyalty.getSegments() && null != loyalty.getSegments().getSegment()
				&& loyalty.getSegments().getSegment().size() > 0)
		{
			if (null != customerModel.getGroups())
			{
				Set<PrincipalGroupModel> userGroups2 = new HashSet<PrincipalGroupModel>();
				userGroups2 = customerModel.getGroups();
				userGroups.addAll(userGroups2);
			}

			for (final Iterator<Segment> iterator = loyalty.getSegments().getSegment().iterator(); iterator.hasNext();)
			{
				final Segment segment = iterator.next();
				UserGroupModel userGroupModel = modelService.create(UserGroupModel.class);
				if (segment.getSegmentId() != 0)
				{
					userGroupModel.setUid(new Integer(segment.getSegmentId()).toString());
				}
				try
				{
					userGroupModel = flexibleSearchService.getModelByExample(userGroupModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}

				userGroupModel = populateSegments(segment, userGroupModel, legend);
				modelService.save(userGroupModel);
				try
				{
					if (Integer.valueOf(1).equals(segment.getStatus()))
					{
						userGroups.add(userGroupModel);
					}
					else if (Integer.valueOf(2).equals(segment.getStatus()) && userGroups.contains(userGroupModel))
					{
						userGroups.remove(userGroupModel);
					}
				}
				catch (final Exception e)
				{
					//
				}
			}
		}
		if (userGroups.size() > 0)
		{
			customerModel.setGroups(userGroups);
		}

		modelService.save(customerModel);
		return customerModel;
	}

	/**
	 * @param coupon
	 * @param voucher
	 * @return
	 */
	private PromotionVoucherModel populateVoucher(final Coupon source, final PromotionVoucherModel target)
	{
		final String newID = (String) myNumberseriesGenerator2.generate();
		target.setCode(newID);

		if (null != source.getType())
		{
			if (source.getType().intValue() == 1)
			{
				target.setCouponType("InStore");
			}
			if (source.getType().intValue() == 2)
			{
				target.setCouponType("Online");
			}
		}
		if (null != source.getCode())
		{
			target.setCouponCode(source.getCode());
		}
		if (null != source.getDescription())
		{
			target.setDescription(source.getDescription());
		}
		if (null != source.getExpiryDate())
		{
			target.setExpiryDate(source.getExpiryDate().toGregorianCalendar().getTime());
		}
		if (null != source.getValue())
		{
			target.setValue(new Double(source.getValue().doubleValue()));
		}
		return target;
	}

	/**
	 * @param segment
	 * @param userGroupModel
	 * @return
	 */
	private UserGroupModel populateSegments(final Segment source, final UserGroupModel target, final Legend legend)
	{
		try
		{
			if (null != legend && null != source && null != legend.getSegments()
					&& CollectionUtils.isNotEmpty(legend.getSegments().getSegment()))
			{
				de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment segmentObj = new de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment();

				segmentObj = (de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment) CollectionUtils.find(legend
						.getSegments().getSegment(), new BeanPropertyValueEqualsPredicate("segmentId", source.getSegmentId(), true));

				if (StringUtils.isNotBlank(segmentObj.getSegmentDescription()))
				{
					target.setName(segmentObj.getSegmentDescription());
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
		}
		if (source.getSegmentId() != 0)
		{
			target.setSegmentID(new Integer(source.getSegmentId()));
		}

		if (null != source.getSegmentTypeId())
		{
			if (source.getSegmentTypeId().equals("1"))
			{
				target.setSegmentType("Household");
			}
			if (source.getSegmentTypeId().equals("2"))
			{
				target.setSegmentType("Member");
			}
		}

		if (null != source.getStatus())
		{
			target.setSegmentStatus(source.getStatus());
		}
		if (null != source.getStartDate())
		{
			target.setStartDate(source.getStartDate().toGregorianCalendar().getTime());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate().toGregorianCalendar().getTime());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate().toGregorianCalendar().getTime());
		}

		return target;
	}

	/**
	 * @param babyClub
	 * @param babyClubModel
	 * @return
	 */
	private BabyClubModel populateBabyClub(final BabyClub source, final BabyClubModel target)
	{
		if (null != source.getAlreadyPregnant())
		{
			target.setIsAlreadyPregnant(new Boolean(true));
			if (null != source.getAlreadyPregnant().getGender() && source.getAlreadyPregnant().getGender().intValue() == 1)
			{
				target.setGender(Gender.MALE);
			}
			if (null != source.getAlreadyPregnant().getGender() && source.getAlreadyPregnant().getGender().intValue() == 2)
			{
				target.setGender(Gender.FEMALE);
			}
			if (null != source.getAlreadyPregnant().getGender() && source.getAlreadyPregnant().getGender().intValue() == 3)
			{
				target.setGender(Gender.UNKNOWN);
			}
			if (null != source.getAlreadyPregnant().getDueDate())
			{
				target.setBabyClubDueDate(source.getAlreadyPregnant().getDueDate());
			}

		}

		if (null != source.getAlreadyParent())
		{
			target.setIsAlreadyParent(new Boolean(true));

			final List<ChildModel> childList = new ArrayList<ChildModel>();

			if (null != source.getAlreadyParent().getChildren() && null != source.getAlreadyParent().getChildren().getChild()
					&& source.getAlreadyParent().getChildren().getChild().size() > 0)
			{
				for (final Child child : source.getAlreadyParent().getChildren().getChild())
				{
					final ChildModel childModel = modelService.create(ChildModel.class);
					if (null != child.getDateOfBirth())
					{
						childModel.setChildDOB(child.getDateOfBirth());
					}

					if (null != child.getFirstName())
					{
						childModel.setFirstName(child.getFirstName());
					}

					if (null != child.getSurname())
					{
						childModel.setLastName(child.getSurname());
					}
					if (null != child.getGender())
					{
						if (child.getGender().intValue() == 2)
						{
							childModel.setGender(Gender.FEMALE);
						}
						if (child.getGender().intValue() == 1)
						{
							childModel.setGender(Gender.MALE);
						}
						if (child.getGender().intValue() == 3)
						{
							childModel.setGender(Gender.UNKNOWN);
						}
					}
					childList.add(childModel);
				}
			}
			if (childList.size() > 0)
			{
				target.setChildren(childList);
			}

		}

		return target;
	}
}
