/**
 *
 */
package de.hybris.clicks.webservices.customer.impl;

import de.hybris.clicks.core.model.MemberAccountModel;
import de.hybris.clicks.webservices.customer.CustomerImportEndpoint;
import de.hybris.clicks.webservices.customer.populators.CustomerCoreDetailsPopulator;
import de.hybris.clicks.webservices.customer.populators.CustomerHeaderPopulator;
import de.hybris.clicks.webservices.customer.populators.CustomerLoyaltyDetailsPopulator;
import de.hybris.clicks.webservices.customer.populators.CustomerNonLoyaltyDetailsPopulator;
import de.hybris.clicks.webservices.customer.schemas.Customer;
import de.hybris.clicks.webservices.customer.schemas.Legend.MemberAccounts.MemberAccount;
import de.hybris.clicks.webservices.customer.schemas.Legend.Segments.Segment;
import de.hybris.clicks.webservices.customer.schemas.PublishCustomer;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


/**
 * @author abhaydh
 *
 */
@Endpoint
public class DefaultCustomerImportEndpoint implements CustomerImportEndpoint
{
	protected static final Logger LOG = Logger.getLogger(DefaultCustomerImportEndpoint.class);
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/Customer";
	public static final String GET_PERSONS_REQUEST = "PublishCustomer";

	@Resource
	CustomerLoyaltyDetailsPopulator customerLoyaltyDetailsPopulator;
	@Resource
	CustomerHeaderPopulator customerHeaderPopulator;
	@Resource
	CustomerCoreDetailsPopulator customerCoreDetailsPopulator;
	@Resource
	CustomerNonLoyaltyDetailsPopulator customerNonLoyaltyDetailsPopulator;

	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	@PayloadRoot(localPart = "PublishCustomer", namespace = "http://Clicks.BizTalk.Omni/Schema/Customer")
	@ResponsePayload
	public void getCustomer(@RequestPayload final PublishCustomer publishCustomer)
	{
		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				try
				{
					importCustomer(publishCustomer);
				}
				catch (final Exception e)
				{
					LOG.error("Exception in importing customer" + e.getMessage());
				}
			}
		});
		thread.start();
	}

	private void importCustomer(final PublishCustomer publishCustomer)
	{
		if (null != publishCustomer.getHeader())
		{
			customerHeaderPopulator.populate(publishCustomer.getHeader());
		}

		// for customer section
		if (null != publishCustomer.getBody().getCustomer())
		{
			for (final Customer customer : publishCustomer.getBody().getCustomer())
			{

				if (null != customer.getCoreDetails())
				{
					if (!publishCustomer.getHeader().getEventCode().equals("BICUSTPUB"))
					{
						CustomerModel customerModel = modelService.create(CustomerModel.class);
						if (null != customer.getCoreDetails().getEMailAddress())
						{
							try
							{
								if (null != customer.getLoyalty() && null != customer.getLoyalty().getMemberId())
								{
									SearchResult<CustomerModel> result = null;
									String query = "SELECT {PK} FROM {Customer} WHERE {memberID}='" + customer.getLoyalty().getMemberId()
											+ "'";
									result = flexibleSearchService.search(query);
									if (null != result && result.getCount() > 0)
									{
										for (final CustomerModel cust : result.getResult())
										{
											customerModel = cust;
										}
									}
									else if (null != customer.getHybris() && StringUtils.isNotEmpty(customer.getHybris().getCustomerId()))
									{
										query = "SELECT {PK} FROM {Customer} WHERE {customerid}='" + customer.getHybris().getCustomerId()
												+ "'";
										result = flexibleSearchService.search(query);
										if (null != result && result.getCount() > 0)
										{
											for (final CustomerModel cust : result.getResult())
											{
												customerModel = cust;
											}
										}
										else
										{
											LOG.info("---Can not find customer with MemberID --->" + customer.getLoyalty().getMemberId());
											customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails()
													.getEMailAddress());
										}
									}
									else
									{
										LOG.info("---Can not find customer with MemberID --->" + customer.getLoyalty().getMemberId());
										customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails()
												.getEMailAddress());
									}
								}
								else
								{
									customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails().getEMailAddress());
								}
							}
							catch (final Exception e)
							{
								customerModel.setUid(customer.getCoreDetails().getEMailAddress());
								e.printStackTrace();
							}
						}
						if (null != customer.getHybris())
						{
							if (null != customer.getHybris().getRetailerId())
							{
								customerModel.setRetailerID_hybris(customer.getHybris().getRetailerId());
							}
							if (null != customer.getHybris().getCustomerId()
									&& StringUtils.isNotEmpty(customer.getHybris().getCustomerId()))
							{
								customerModel.setCustomerID(customer.getHybris().getCustomerId());
							}
							else if (StringUtils.isEmpty(customerModel.getCustomerID()))
							{
								//customerModel.setCustomerID((String) myNumberseriesGenerator.generate());
							}
						}
						if (null != customer.getPharmacy() && null != customer.getPharmacy().getMarconiId())
						{
							customerModel.setMarconiId(customer.getPharmacy().getMarconiId());
						}
						if (null != customer.getCoreDetails())
						{
							customerModel = customerCoreDetailsPopulator.populate(customer.getCoreDetails(), customerModel);
						}
						if (null != customer.getLoyalty())
						{
							customerModel = customerLoyaltyDetailsPopulator.populate(customer.getLoyalty(), customerModel,
									publishCustomer.getBody().getLegend());
						}
						if (null != customer.getNonLoyalty())
						{
							customerModel = customerNonLoyaltyDetailsPopulator.populate(customer.getNonLoyalty(), customerModel,
									publishCustomer.getBody().getLegend());
						}
						modelService.save(customerModel);
					}
					else
					{

						if (null != customer.getCoreDetails().getEMailAddress())
						{
							try
							{
								CustomerModel customerModel = modelService.create(CustomerModel.class);
								if (null != customer.getLoyalty() && null != customer.getLoyalty().getMemberId())
								{
									SearchResult<CustomerModel> result = null;
									final String query = "SELECT {PK} FROM {Customer} WHERE {memberID}='"
											+ customer.getLoyalty().getMemberId() + "'";
									result = flexibleSearchService.search(query);
									if (null != result && result.getCount() > 0)
									{
										for (final CustomerModel cust : result.getResult())
										{
											customerModel = cust;
										}
									}
									else
									{
										LOG.info("---Can not find customer with MemberID --->" + customer.getLoyalty().getMemberId());
										customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails()
												.getEMailAddress());
									}
								}
								else
								{
									customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails().getEMailAddress());
								}
								if (null != customer.getNonLoyalty())
								{
									customerModel = customerNonLoyaltyDetailsPopulator.populate(customer.getNonLoyalty(), customerModel,
											publishCustomer.getBody().getLegend());
								}
								modelService.save(customerModel);
							}
							catch (final Exception e)
							{
								e.printStackTrace();
							}
						}

					}
				}
				else
				{
					if (null != customer.getLoyalty() && (null != customer.getLoyalty().getMemberId()))
					{

						try
						{
							CustomerModel customerModel = null;

							SearchResult<CustomerModel> result = null;
							final String query = "SELECT {PK} FROM {Customer} WHERE {memberID}='" + customer.getLoyalty().getMemberId()
									+ "'";
							result = flexibleSearchService.search(query);
							if (null != result && result.getCount() > 0)
							{
								for (final CustomerModel cust : result.getResult())
								{
									customerModel = cust;
								}
							}
							else
							{
								LOG.error("---Can not find customer with memberID --->" + customer.getLoyalty().getMemberId());
							}
							if (null != customerModel)
							{
								customerModel = customerLoyaltyDetailsPopulator.populate(customer.getLoyalty(), customerModel,
										publishCustomer.getBody().getLegend());
								modelService.save(customerModel);
							}
						}
						catch (final Exception e)
						{
							e.printStackTrace();
						}

					}
					else if (null != customer.getLoyalty() && StringUtils.isNotBlank(customer.getLoyalty().getHouseholdId()))
					{
						try
						{
							SearchResult<CustomerModel> result = null;
							final String query = "SELECT {PK} FROM {Customer} WHERE {householdid}='"
									+ customer.getLoyalty().getHouseholdId() + "'";
							result = flexibleSearchService.search(query);
							if (null != result && CollectionUtils.isNotEmpty(result.getResult()))
							{
								for (CustomerModel cust : result.getResult())
								{
									if (null != cust)
									{
										cust = customerLoyaltyDetailsPopulator.populate(customer.getLoyalty(), cust, publishCustomer
												.getBody().getLegend());
										modelService.save(cust);
									}
								}
							}
							else
							{
								LOG.info("---Can not find customers with HouseholdId --->" + customer.getLoyalty().getHouseholdId());
							}

						}
						catch (final Exception e)
						{
							e.printStackTrace();
						}
					}

				}
			}
		}
		// for Legends section
		if (null != publishCustomer.getBody().getLegend())
		{
			if (null != publishCustomer.getBody().getLegend().getSegments()
					&& null != publishCustomer.getBody().getLegend().getSegments().getSegment()
					&& publishCustomer.getBody().getLegend().getSegments().getSegment().size() > 0)
			{
				for (final Segment segment : publishCustomer.getBody().getLegend().getSegments().getSegment())
				{
					UserGroupModel userGroupModel = modelService.create(UserGroupModel.class);
					if (segment.getSegmentId() != 0)
					{
						userGroupModel.setUid(new Integer(segment.getSegmentId()).toString());
					}
					try
					{
						userGroupModel = flexibleSearchService.getModelByExample(userGroupModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}

					userGroupModel = populateLegendSegments(segment, userGroupModel);
					modelService.save(userGroupModel);
				}
			}
			if (null != publishCustomer.getBody().getLegend().getMemberAccounts()
					&& null != publishCustomer.getBody().getLegend().getMemberAccounts().getMemberAccount()
					&& publishCustomer.getBody().getLegend().getMemberAccounts().getMemberAccount().size() > 0)
			{
				for (final MemberAccount account : publishCustomer.getBody().getLegend().getMemberAccounts().getMemberAccount())
				{
					MemberAccountModel memberAccountModel = modelService.create(MemberAccountModel.class);
					if (account.getAccountId() != 0)
					{
						memberAccountModel.setAccountId(new Integer(account.getAccountId()));
					}
					try
					{
						memberAccountModel = flexibleSearchService.getModelByExample(memberAccountModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}

					memberAccountModel = populateMemberAccount(account, memberAccountModel);
					modelService.save(memberAccountModel);
				}
			}
		}
	}

	private MemberAccountModel populateMemberAccount(final MemberAccount source, final MemberAccountModel target)
	{
		if (null != source.getAccountName())
		{
			target.setAccountName(source.getAccountName());
		}
		if (null != source.getBalanceType())
		{
			target.setBalanceType(source.getBalanceType());
		}
		if (null != source.getCurrency())
		{
			target.setCurrency(source.getCurrency());
		}

		return target;
	}

	private UserGroupModel populateLegendSegments(final Segment source, final UserGroupModel target)
	{
		if (source.getSegmentId() != 0)
		{
			target.setSegmentID(new Integer(source.getSegmentId()));
		}
		if (null != source.getStartDate())
		{
			target.setStartDate(source.getStartDate().toGregorianCalendar().getTime());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate().toGregorianCalendar().getTime());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate().toGregorianCalendar().getTime());
		}
		if (null != source.getSegmentDescription())
		{
			target.setSegmentDescription(source.getSegmentDescription());
			target.setDescription(source.getSegmentDescription());
			target.setLocName(source.getSegmentDescription(), Locale.ENGLISH);
		}
		modelService.save(target);
		return target;
	}

	/**
	 * @return the myNumberseriesGenerator
	 */
	public PersistentKeyGenerator getMyNumberseriesGenerator()
	{
		return myNumberseriesGenerator;
	}

	/**
	 * @param myNumberseriesGenerator
	 *           the myNumberseriesGenerator to set
	 */
	public void setMyNumberseriesGenerator(final PersistentKeyGenerator myNumberseriesGenerator)
	{
		this.myNumberseriesGenerator = myNumberseriesGenerator;
	}
}
