package de.hybris.clicks.webservices.product.services;

import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.clicks.webservices.product.schemas.stock.StockCount;
import de.hybris.clicks.webservices.product.schemas.stock.StockCount.Body.Items.Item;
import de.hybris.clicks.webservices.product.schemas.stock.StockCount.Body.Items.Item.Store;
import de.hybris.clicks.webservices.product.schemas.stock.StockCount.Header;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;


@Endpoint
public class StockCountImportEndpoint
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/StockCount";
	public static final String GET_PERSONS_REQUEST = "StockCount";
	protected static final Logger LOG = Logger.getLogger(StockCountImportEndpoint.class);
	@Resource
	private UserService userService;
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;
	@Resource
	private WarehouseService warehouseService;
	@Resource
	private StockService stockService;
	@Resource
	private ProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@PayloadRoot(localPart = "StockCount", namespace = "http://Clicks.BizTalk.Omni/Schema/StockCount")
	public void updateStockCount(@RequestPayload final StockCount stock)
	{

		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				userService.setCurrentUser(userService.getAdminUser());
				if (null != stock && null != stock.getHeader())
				{
					populateHeaderDetails(stock.getHeader());
				}
				if (null != stock && null != stock.getBody() && null != stock.getBody().getItems())
				{
					updateStock(stock);
				}
				//
			}

			private void populateHeaderDetails(final Header source)
			{
				try
				{
					final ProdHeaderDetailsModel target = modelService.create(ProdHeaderDetailsModel.class);

					if (null != source.getDataSource())
					{
						target.setDataSource(source.getDataSource());
					}
					if (null != source.getEventCode())
					{
						target.setEventCode(source.getEventCode());
					}
					if (null != source.getEventLogId())
					{
						target.setEventLogId(source.getEventLogId());
					}
					if (null != source.getEventCode())
					{
						target.setFeedType(source.getEventCode());
					}
					if (null != source.getCreatedDateTime())
					{
						target.setCreatedDateTime(source.getCreatedDateTime().toGregorianCalendar().getTime());
					}

					modelService.save(target);
				}
				catch (final Exception e)
				{
					//
				}
			}

			/**
			 * @param stock
			 */
			private void updateStock(final StockCount stock)
			{
				StringBuffer buffer = null;
				WarehouseModel warehouseModel = null;
				final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion("clicksProductCatalog",
						"Staged");
				if (CollectionUtils.isNotEmpty(stock.getBody().getItems().getItem()))
				{
					for (final Item item : stock.getBody().getItems().getItem())
					{
						try
						{
							final ProductModel productModel = productService.getProductForCode(catalogVersionModel, item.getSKU());
							if (null != productModel)
							{
								for (final Store store : item.getStore())
								{
									try
									{
										buffer = new StringBuffer();
										buffer.append(item.getSKU()).append(" store" + store.getId()).append(" Available > ")
												.append(store.getAvailable()).append(" Reserved").append(store.getReserved());
										try
										{
											warehouseModel = warehouseService.getWarehouseForCode(store.getId());
										}
										catch (final Exception e)
										{
											warehouseModel = null;
										}
										if (null == warehouseModel)
										{
											warehouseModel = modelService.create(WarehouseModel.class);
											warehouseModel.setName(store.getId());
											warehouseModel.setCode(store.getId());
											VendorModel vendorModel = modelService.create(VendorModel.class);
											vendorModel.setCode("default");
											try
											{
												vendorModel = flexibleSearchService.getModelByExample(vendorModel);
											}
											catch (final Exception e)
											{
												LOG.error("Ecxeption while getting vendor model for stock update of " + item.getSKU()
														+ " store " + store.getId() + e.getMessage());
											}
											if (null != vendorModel.getName())
											{
												warehouseModel.setVendor(vendorModel);
											}

											PointOfServiceModel posModel = modelService.create(PointOfServiceModel.class);
											posModel.setName(store.getId());
											try
											{
												posModel = flexibleSearchService.getModelByExample(posModel);
											}
											catch (final Exception e)
											{
												//
											}
											if (null != posModel.getDisplayName())
											{
												final List<PointOfServiceModel> posList = new ArrayList<PointOfServiceModel>();
												posList.add(posModel);
												warehouseModel.setPointsOfService(posList);
											}
											modelService.save(warehouseModel);
										}
										if (null != warehouseModel)
										{
											try
											{
												stockService.updateActualStockLevel(productModel, warehouseModel, store.getAvailable(),
														buffer.toString());
											}
											catch (final Exception e)
											{
												LOG.error("Ecxeption while updating stock for " + item.getSKU() + " store " + store.getId()
														+ e.getMessage());
											}
										}
									}
									catch (final Exception e)
									{
										LOG.error("Ecxeption while updating stock for " + item.getSKU() + " store " + store.getId()
												+ e.getMessage());
									}
								}
							}
						}
						catch (final Exception e)
						{
							LOG.error("Ecxeption while updating stock for " + item.getSKU() + e.getMessage());
						}
					}
				}
			}
		});
		thread.start();

	}
}
