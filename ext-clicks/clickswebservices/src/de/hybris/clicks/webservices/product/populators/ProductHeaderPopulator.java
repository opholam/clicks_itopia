/**
 *
 */
package de.hybris.clicks.webservices.product.populators;

import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class ProductHeaderPopulator
{

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	public void populate(final ProductCatalogue source) throws ConversionException
	{

		final ProdHeaderDetailsModel target = modelService.create(ProdHeaderDetailsModel.class);

		if (null != source.getHeader().getDataSource())
		{
			target.setDataSource(source.getHeader().getDataSource());
		}
		if (null != source.getHeader().getCreatedDateTime())
		{
			target.setCreatedDateTime(source.getHeader().getCreatedDateTime().toGregorianCalendar().getTime());
		}
		if (null != source.getHeader().getEventCode())
		{
			target.setEventCode(source.getHeader().getEventCode());
		}
		if (null != source.getHeader().getEventLogId())
		{
			target.setEventLogId(source.getHeader().getEventLogId());
		}
		if (null != source.getHeader().getEventCode())
		{
			target.setFeedType(source.getHeader().getEventCode());
		}
		if (null != source.getBody().getRetailerId())
		{
			target.setRetailerId(source.getBody().getRetailerId());
		}
		if (null != source.getBody().getCreationDate())
		{
			target.setEventDate(source.getBody().getCreationDate().toGregorianCalendar().getTime());
		}

		modelService.save(target);

	}

}
