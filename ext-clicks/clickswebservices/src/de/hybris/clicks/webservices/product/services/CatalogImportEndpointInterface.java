/**
 *
 */
package de.hybris.clicks.webservices.product.services;

import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.springframework.ws.server.endpoint.annotation.RequestPayload;


/**
 * @author abhaydh
 *
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface CatalogImportEndpointInterface
{
	@WebMethod
	public void getCatalogue(@RequestPayload final Catalogue catalog);

}
