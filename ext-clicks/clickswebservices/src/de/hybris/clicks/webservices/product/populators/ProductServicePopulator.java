package de.hybris.clicks.webservices.product.populators;



import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.BarcodeModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.clicks.core.model.PartnerModel;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand.Products.Product;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand.Products.Product.Variants.Variant;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand.Products.Product.Variants.Variant.Items.Item;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand.Products.Product.Variants.Variant.Items.Item.Barcodes.Barcode;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand.Products.Product.Variants.Variant.Items.Item.DepartmentHierarchy.HierarchyLink;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue.Body.Brands.Brand.Products.Product.Variants.Variant.Items.Item.PartnerRewards.Partner;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.product.VariantsService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class ProductServicePopulator
{

	protected static final Logger LOG = Logger.getLogger(ProductServicePopulator.class);

	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CatalogService catalogService;

	@Resource
	private CategoryService categoryService;

	@Resource
	private VariantsService variantsService;

	@Resource
	private UserService userService;

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	String newID;

	public void populate(final ProductCatalogue source) throws ConversionException
	{
		CategoryModel categoryModel;
		CategoryModel superCategoryModel;
		ProductModel productModel;
		VariantTypeModel variantTypeModel;
		CategoryModel deptCategoryModel;
		String prodName = "";
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
		//final CatalogVersion version = CatalogManager.getInstance().getCatalog("testCatalog").getCatalogVersion("Staged");

		userService.setCurrentUser(userService.getAdminUser());
		PrincipalModel principalModel = modelService.create(PrincipalModel.class);
		principalModel.setUid("customergroup");
		principalModel = flexibleSearchService.getModelByExample(principalModel);
		final List<PrincipalModel> pricipalList = new ArrayList<PrincipalModel>();
		pricipalList.add(principalModel);


		//deptCategoryModel.setCode("Departments");
		//deptCategoryModel = flexibleSearchService.getModelByExample(deptCategoryModel);

		final String query = "SELECT {PK} FROM {Category} WHERE {code}='Departments'";
		final SearchResult<CategoryModel> result = flexibleSearchService.search(query);

		if (result.getCount() == 0)
		{
			deptCategoryModel = modelService.create(CategoryModel.class);
			deptCategoryModel.setCode("Departments");
			deptCategoryModel.setName("Departments");
			//deptCategoryModel.setAllowedPrincipals(pricipalList);
			deptCategoryModel.setCatalogVersion(catalogVersionModel);
			modelService.save(deptCategoryModel);

		}

		for (final Iterator<Brand> iterator = source.getBody().getBrands().getBrand().iterator(); iterator.hasNext();)
		{
			final Brand brand = iterator.next();
			final List<CategoryModel> productSuperCategories = new ArrayList<CategoryModel>();
			final List<CategoryModel> superCategories = new ArrayList<CategoryModel>();
			superCategoryModel = modelService.create(CategoryModel.class);
			try
			{
				superCategoryModel = getCategoryService().getCategoryForCode(catalogVersionModel, "brands");
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
			if (null == superCategoryModel.getCode())
			{
				superCategoryModel.setCode("brands");
				superCategoryModel.setName("brands");
				superCategoryModel.setAllowedPrincipals(pricipalList);
				superCategoryModel.setCatalogVersion(catalogVersionModel);
				modelService.save(superCategoryModel);
			}

			superCategories.add(superCategoryModel);

			categoryModel = modelService.create(CategoryModel.class);
			categoryModel.setName(brand.getName());
			categoryModel.setCatalogVersion(catalogVersionModel);
			try
			{
				final List<CategoryModel> categoryModels = flexibleSearchService.getModelsByExample(categoryModel);
				if (CollectionUtils.isNotEmpty(categoryModels))
				{
					for (final CategoryModel categoryModel2 : categoryModels)
					{
						if (CollectionUtils.exists(categoryModel2.getSupercategories(), new BeanPropertyValueEqualsPredicate(
								CategoryModel.CODE, "brands", true)))
						{
							categoryModel = categoryModel2;
							break;
						}
					}
				}
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}

			if (null == categoryModel.getCode())
			{
				categoryModel = modelService.create(CategoryModel.class);
				newID = (String) myNumberseriesGenerator.generate();
				categoryModel.setCode(newID);
			}

			categoryModel.setName(brand.getName());
			categoryModel.setAllowedPrincipals(pricipalList);
			categoryModel.setCatalogVersion(catalogVersionModel);
			categoryModel.setSupercategories(superCategories);
			modelService.save(categoryModel);
			productSuperCategories.add(categoryModel);


			for (final Iterator<Product> productIterator = brand.getProducts().getProduct().iterator(); productIterator.hasNext();)
			{
				final Product product = productIterator.next();

				productModel = modelService.create(ProductModel.class);
				if (StringUtils.isNotBlank(product.getCollection()))
				{
					prodName = (product.getName() + " - " + product.getCollection());
				}
				else
				{
					prodName = product.getName();
				}
				productModel.setName(prodName);
				try
				{
					//productModel = flexibleSearchService.getModelByExample(productModel);

					String productquery;
					SearchResult<ProductModel> productresult = null;
					//productquery = "SELECT {PK} FROM {Product} WHERE {name}=?variantname and {catalogVersion} = ?catalogVersion";
					productquery = "SELECT {PK} FROM {Product as p join VariantType as v on {p:varianttype}={v:pk}} WHERE {p:name}=?variantname and {p:catalogVersion} = ?catalogVersion and {v:code} ='ApparelStyleVariantProduct'";

					final Map<String, Object> params = new HashMap<String, Object>();
					params.put("variantname", prodName);
					params.put("catalogVersion", catalogVersionModel);
					productresult = flexibleSearchService.search(productquery, params);
					if (productresult.getCount() > 0)
					{
						for (final ProductModel productModelTemp : productresult.getResult())
						{
							for (final CategoryModel superCategory : productModelTemp.getSupercategories())
							{
								if (null != brand.getName() && null != superCategory.getName())
								{
									if (brand.getName().equalsIgnoreCase(superCategory.getName()))
									{
										productModel = productModelTemp;
									}
								}
							}
						}
					}

				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}

				if (null == productModel.getCode())
				{
					productModel = modelService.create(ProductModel.class);
					productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
					newID = (String) myNumberseriesGenerator.generate();
					productModel.setCode(newID);
				}


				variantTypeModel = variantsService.getVariantTypeForCode("ApparelStyleVariantProduct");

				productModel.setName(prodName);
				if (null != product.getCollection())
				{
					productModel.setCollection(product.getCollection());
				}
				productModel.setCatalogVersion(catalogVersionModel);
				productModel.setVariantType(variantTypeModel);
				if (CollectionUtils.isNotEmpty(productModel.getSupercategories()))
				{
					for (final CategoryModel categoryModel2 : productModel.getSupercategories())
					{
						if (categoryModel2.getCode().startsWith("OH"))
						{
							productSuperCategories.add(categoryModel2);
						}
					}
				}
				productModel.setSupercategories(productSuperCategories);
				modelService.save(productModel);

				productModel.setVariants(populateStyleVariants(product, productModel));

				modelService.save(productModel);
			}


		}
	}

	private Collection<VariantProductModel> populateStyleVariants(final Product product, final ProductModel productModel)
	{
		boolean hasVariants = false;
		List<VariantProductModel> variantsList = new ArrayList<VariantProductModel>();
		List<VariantProductModel> newVariantsList = new ArrayList<VariantProductModel>();
		if (null != productModel.getVariants() && productModel.getVariants().size() > 0)
		{
			variantsList = (List<VariantProductModel>) productModel.getVariants();
			newVariantsList.addAll(variantsList);
			hasVariants = true;
		}
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
		final VariantTypeModel variantTypeModel = null;
		boolean toAddVariant = false;
		for (final Iterator<Variant> variantIterator = product.getVariants().getVariant().iterator(); variantIterator.hasNext();)
		{
			final Variant variant = variantIterator.next();
			boolean alreayExist = false;
			if (hasVariants)
			{
				for (final VariantProductModel loopVariant : newVariantsList)
				{
					if (null != variant.getName())
					{

						if (loopVariant.getName().equalsIgnoreCase(variant.getName()))
						{
							alreayExist = true;
							toAddVariant = false;
							newVariantsList = addStyleVariant(variant, catalogVersionModel, variantTypeModel, productModel,
									newVariantsList, toAddVariant);
						}
					}
				}
				if (!alreayExist)
				{
					toAddVariant = true;
					newVariantsList = addStyleVariant(variant, catalogVersionModel, variantTypeModel, productModel, newVariantsList,
							toAddVariant);
				}
			}
			else
			{
				toAddVariant = true;
				newVariantsList = addStyleVariant(variant, catalogVersionModel, variantTypeModel, productModel, newVariantsList,
						toAddVariant);
			}
		}
		return newVariantsList;
	}



	private List<VariantProductModel> addStyleVariant(final Variant variant, final CatalogVersionModel catalogVersionModel,
			VariantTypeModel variantTypeModel, final ProductModel productModel, final List<VariantProductModel> newVariantsList,
			final boolean toAddVariant)
	{
		ApparelStyleVariantProductModel styleVariantModel;
		styleVariantModel = modelService.create(ApparelStyleVariantProductModel.class);
		if (null != variant.getName())
		{
			styleVariantModel.setName(variant.getName());
		}
		else
		{
			if (null != variant.getTitle())
			{
				styleVariantModel.setName(variant.getTitle());
			}
		}
		try
		{

			String query;
			SearchResult<ApparelStyleVariantProductModel> result;
			//query = "SELECT {PK} FROM {ApparelStyleVariantProduct} WHERE {name}=?variantname and {catalogVersion} = ?catalogVersion";
			query = "SELECT {PK} FROM {Product as p join VariantType as v on {p:varianttype}={v:pk}} WHERE {p:name}=?variantname and {p:catalogVersion} = ?catalogVersion and {v:code} ='ClicksApparelSizeVariant'";
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("catalogVersion", catalogVersionModel);
			if (null != variant.getName())
			{
				params.put("variantname", variant.getName());
			}
			else
			{
				params.put("variantname", variant.getTitle());
			}
			result = flexibleSearchService.search(query, params);
			if (result.getCount() > 0)
			{
				for (final ApparelStyleVariantProductModel styleVariantModelTemp : result.getResult())
				{

					if (productModel.getName().equalsIgnoreCase(styleVariantModelTemp.getBaseProduct().getName()))
					{
						for (final CategoryModel superCategory : productModel.getSupercategories())
						{
							for (final CategoryModel superCategory2 : styleVariantModelTemp.getBaseProduct().getSupercategories())
							{
								if (null != superCategory.getName() && null != superCategory2.getName())
								{
									if (superCategory.getName().equalsIgnoreCase(superCategory2.getName()))
									{
										styleVariantModel = styleVariantModelTemp;
									}
								}
							}
						}
					}

				}
			}


		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		if (null == styleVariantModel.getCode())
		{
			styleVariantModel = modelService.create(ApparelStyleVariantProductModel.class);
			styleVariantModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
			newID = (String) myNumberseriesGenerator.generate();
			styleVariantModel.setCode(newID);
		}
		variantTypeModel = variantsService.getVariantTypeForCode("ClicksApparelSizeVariant");
		if (null != variant.getName())
		{
			styleVariantModel.setName(variant.getName());
		}
		else
		{
			if (null != variant.getTitle())
			{
				styleVariantModel.setName(variant.getTitle());
			}
		}
		if (null != variant.getName())
		{
			styleVariantModel.setStyle(variant.getName());
		}
		else
		{
			if (null != variant.getTitle())
			{
				styleVariantModel.setStyle(variant.getTitle());
			}
		}
		styleVariantModel.setCatalogVersion(catalogVersionModel);
		styleVariantModel.setBaseProduct(productModel);
		styleVariantModel.setVariantType(variantTypeModel);
		styleVariantModel.setStyleVariantTitle(variant.getTitle());
		modelService.save(styleVariantModel);

		styleVariantModel.setVariants(populateSizeVariants(variant, styleVariantModel));
		modelService.save(styleVariantModel);

		if (toAddVariant)
		{
			newVariantsList.add(styleVariantModel);
		}
		return newVariantsList;
	}

	private Collection<VariantProductModel> populateSizeVariants(final Variant variant,
			final ApparelStyleVariantProductModel styleVariantModel)
	{
		boolean hasItems = false;
		List<VariantProductModel> getVariantsList = new ArrayList<VariantProductModel>();
		List<VariantProductModel> variantsList = new ArrayList<VariantProductModel>();
		if (null != styleVariantModel.getVariants() && styleVariantModel.getVariants().size() > 0)
		{
			getVariantsList = (List<VariantProductModel>) styleVariantModel.getVariants();
			variantsList.addAll(getVariantsList);
			hasItems = true;
		}

		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
		for (final Iterator<Item> itemIterator = variant.getItems().getItem().iterator(); itemIterator.hasNext();)
		{
			boolean toAdd = false;
			final Item sizeVariant = itemIterator.next();
			boolean alreayExist = false;
			if (hasItems)
			{
				for (final VariantProductModel loopVariant : variantsList)
				{
					if (loopVariant.getCode().equalsIgnoreCase(sizeVariant.getSKU()))
					{
						alreayExist = true;
						//variantsList.remove(loopVariant);
						toAdd = false;
						variantsList = populateItem(sizeVariant, catalogVersionModel, variantsList, styleVariantModel, toAdd);
					}
				}
				if (!alreayExist)
				{
					toAdd = true;
					variantsList = populateItem(sizeVariant, catalogVersionModel, variantsList, styleVariantModel, toAdd);
				}
			}
			else
			{
				toAdd = true;
				variantsList = populateItem(sizeVariant, catalogVersionModel, variantsList, styleVariantModel, toAdd);
			}
		}

		return variantsList;
	}




	private List<VariantProductModel> populateItem(final Item sizeVariant, CatalogVersionModel catalogVersionModel,
			final List<VariantProductModel> variantsList, final ApparelStyleVariantProductModel styleVariantModel,
			final boolean toAdd)
	{
		ClicksApparelSizeVariantModel sizeVariantModel;
		catalogVersionModel = catalogVersionService.getCatalogVersion(Config.getParameter("clicks.productcatalog.name"),
				Config.getParameter("clicks.catalog.staged.version"));
		sizeVariantModel = new ClicksApparelSizeVariantModel();
		sizeVariantModel.setCode(sizeVariant.getSKU());
		sizeVariantModel.setCatalogVersion(catalogVersionModel);
		try
		{
			sizeVariantModel = flexibleSearchService.getModelByExample(sizeVariantModel);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
			try
			{
				sizeVariantModel = flexibleSearchService.getModelByExample(sizeVariantModel);
			}
			catch (final Exception e2)
			{
				LOG.error(e2.getMessage());
			}
		}
		sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.CHECK);
		if (null == sizeVariantModel.getPk())
		{
			sizeVariantModel = modelService.create(ClicksApparelSizeVariantModel.class);
			sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.LOADED);
			newID = (String) myNumberseriesGenerator.generate();
			sizeVariantModel.setCode(newID);
		}


		CategoryModel deptCategoryModel;

		sizeVariantModel.setCode(sizeVariant.getSKU());
		if (null != sizeVariant.getQuantity())
		{
			sizeVariantModel.setProductQuantity(sizeVariant.getQuantity());
		}
		if (null != sizeVariant.getSize())
		{
			sizeVariantModel.setSize(sizeVariant.getSize());
		}
		sizeVariantModel.setBaseProduct(styleVariantModel);
		sizeVariantModel.setCatalogVersion(catalogVersionModel);

		final List<CategoryModel> categoryList = new ArrayList<CategoryModel>();
		final List<CategoryModel> superCategoryList = new ArrayList<CategoryModel>();
		deptCategoryModel = getCategoryService().getCategoryForCode(catalogVersionModel, "Departments");
		superCategoryList.add(deptCategoryModel);
		PrincipalModel principalModel = modelService.create(PrincipalModel.class);
		principalModel.setUid("customergroup");
		principalModel = flexibleSearchService.getModelByExample(principalModel);
		final List<PrincipalModel> pricipalList = new ArrayList<PrincipalModel>();
		pricipalList.add(principalModel);

		String query;
		SearchResult<CategoryModel> result;

		if (null != sizeVariant.getDepartmentHierarchy() && sizeVariant.getDepartmentHierarchy().getHierarchyLink().size() > 0)
		{
			for (final Iterator<HierarchyLink> deptIterator = sizeVariant.getDepartmentHierarchy().getHierarchyLink().iterator(); deptIterator
					.hasNext();)
			{
				final HierarchyLink hierarchyLink = deptIterator.next();
				CategoryModel categoryModel = modelService.create(CategoryModel.class);

				query = "SELECT {PK} FROM {Category} WHERE {code}='" + hierarchyLink.getCategoryId() + "'";
				result = flexibleSearchService.search(query);
				if (result.getCount() == 0)
				{
					categoryModel.setCode(hierarchyLink.getCategoryId());
					categoryModel.setSupercategories(superCategoryList);
					//categoryModel.setAllowedPrincipals(pricipalList);
					categoryModel.setCatalogVersion(catalogVersionModel);
					modelService.save(categoryModel);
				}
				else
				{
					categoryModel.setCode(hierarchyLink.getCategoryId());
					categoryModel.setCatalogVersion(catalogVersionModel);
					categoryModel = flexibleSearchService.getModelByExample(categoryModel);
				}

				categoryList.add(categoryModel);
			}
		}
		if (categoryList.size() > 0)
		{
			if (CollectionUtils.isNotEmpty(sizeVariantModel.getSupercategories()))
			{
				for (final CategoryModel categoryModel2 : sizeVariantModel.getSupercategories())
				{
					try
					{
						if ((CollectionUtils.isNotEmpty(categoryModel2.getSupercategories()) && CollectionUtils.exists(
								categoryModel2.getSupercategories(), new BeanPropertyValueEqualsPredicate("code", "collections", true))))
						{
							categoryList.add(categoryModel2);
						}
					}
					catch (final Exception e)
					{
						//
					}
					if (categoryModel2.getCode().startsWith("OH"))
					{
						categoryList.add(categoryModel2);
					}
				}
			}
			sizeVariantModel.setSupercategories(categoryList);
		}

		if (null != sizeVariant.getQuantityRestriction())
		{
			sizeVariantModel.setMaxOrderQuantity(sizeVariant.getQuantityRestriction());
		}
		if (null != sizeVariant.getTaxRate())
		{
			sizeVariantModel.setTaxRate(new Double(sizeVariant.getTaxRate().doubleValue()));
			try
			{
				final ProductTaxGroup productTaxGroup = ProductTaxGroup.valueOf("ZATaxRate" + sizeVariant.getTaxRate());
				if (modelService.isNew(productTaxGroup))
				{
					modelService.save(productTaxGroup);
				}
				sizeVariantModel.setEurope1PriceFactory_PTG(productTaxGroup);
			}
			catch (final Exception e)
			{
				//
			}
		}

		if (null != sizeVariant.getBrandType())
		{
			sizeVariantModel.setBrandType(sizeVariant.getBrandType());
		}
		if (null != sizeVariant.getTitle())
		{
			sizeVariantModel.setSizeVariantTitle(sizeVariant.getTitle());
		}

		if (sizeVariant.getAction().equalsIgnoreCase("I"))
		{
			//sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		}
		if (sizeVariant.getAction().equalsIgnoreCase("D"))
		{
			sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
		}

		if (null != sizeVariant.isNewness() && sizeVariant.isNewness().booleanValue())
		{
			sizeVariantModel.setNewness(new Boolean(true));
		}

		if (null != sizeVariant.isSeasonal() && sizeVariant.isSeasonal().booleanValue())
		{
			sizeVariantModel.setSeasonal(new Boolean(true));
		}
		if (null != sizeVariant.isOnlineOnly() && sizeVariant.isOnlineOnly().booleanValue())
		{
			sizeVariantModel.setOnlineOnly(new Boolean(true));
		}
		if (null != sizeVariant.getProductType())
		{
			sizeVariantModel.setProductType(sizeVariant.getProductType());
		}
		if (null != sizeVariant.getPharmacyProduct())
		{
			sizeVariantModel.setSchedulingStatus(new Integer(sizeVariant.getPharmacyProduct().getSchedulingStatus()));
		}
		if (null != sizeVariant.isDisplayOnline())
		{
			sizeVariantModel.setDisplayOnline(sizeVariant.isDisplayOnline());
			if (!sizeVariant.isDisplayOnline().booleanValue())
			{
				sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.OFFLINE);
			}
			//			else if (null != sizeVariantModel.getPk() && ArticleApprovalStatus.OFFLINE.equals(sizeVariantModel.getApprovalStatus()))
			//			{
			//				sizeVariantModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
			//			}
		}
		if (null != sizeVariant.getTitle())
		{
			sizeVariantModel.setName(sizeVariant.getTitle());
		}

		final List<PartnerModel> partnerlist = new ArrayList<PartnerModel>();

		if (null != sizeVariant.getPartnerRewards() && sizeVariant.getPartnerRewards().getPartner().size() > 0)
		{
			boolean isVITHC = false;
			for (final Iterator<Partner> partnerIterator = sizeVariant.getPartnerRewards().getPartner().iterator(); partnerIterator
					.hasNext();)
			{
				final Partner partner = partnerIterator.next();
				final PartnerModel PartnerModel = modelService.create(PartnerModel.class);
				PartnerModel.setPartnerId(partner.getPartnerId());
				partnerlist.add(PartnerModel);
				if (partner.getPartnerId().equalsIgnoreCase(Config.getParameter("clicks.property.vithc")))
				{
					isVITHC = true;
				}
			}


			// add category to supercategory list
			String categoryVITCH = "OH2_VITHC";
			if (Config.getParameter("product.vitality.health.care.categoryCode") != null)
			{
				categoryVITCH = Config.getParameter("product.vitality.health.care.categoryCode");
			}

			CategoryModel categoryModel = modelService.create(CategoryModel.class);
			query = "SELECT {PK} FROM {Category} WHERE {code}='" + categoryVITCH + "'";
			result = flexibleSearchService.search(query);
			if (result.getCount() == 0)
			{
				categoryModel.setCode(categoryVITCH);
				categoryModel.setSupercategories(superCategoryList);
				//categoryModel.setAllowedPrincipals(pricipalList);
				categoryModel.setCatalogVersion(catalogVersionModel);
				modelService.save(categoryModel);
			}
			else
			{
				categoryModel.setCode(categoryVITCH);
				categoryModel.setCatalogVersion(catalogVersionModel);
				categoryModel = flexibleSearchService.getModelByExample(categoryModel);
			}

			List<CategoryModel> superCatList = new ArrayList<CategoryModel>();
			final List<CategoryModel> superCatList2 = new ArrayList<CategoryModel>();
			superCatList = (List<CategoryModel>) sizeVariantModel.getSupercategories();
			superCatList2.addAll(superCatList);
			boolean alreadyInList = false;
			for (final CategoryModel superCat : sizeVariantModel.getSupercategories())
			{
				if (superCat.getCode().equalsIgnoreCase(categoryVITCH))
				{
					alreadyInList = true;
				}
			}
			if (isVITHC)
			{
				if (!alreadyInList)
				{
					superCatList2.add(categoryModel);
				}
			}
			else
			{
				if (alreadyInList)
				{
					superCatList2.remove(categoryModel);
				}
			}
			sizeVariantModel.setSupercategories(superCatList2);
		}


		if (partnerlist.size() > 0)
		{
			sizeVariantModel.setPartnerRewards(partnerlist);
		}

		final List<BarcodeModel> barcodelist = new ArrayList<BarcodeModel>();
		if (null != sizeVariant.getBarcodes() && sizeVariant.getBarcodes().getBarcode().size() > 0)
		{
			for (final Iterator<Barcode> barcodeIterator = sizeVariant.getBarcodes().getBarcode().iterator(); barcodeIterator
					.hasNext();)
			{
				final Barcode barcode = barcodeIterator.next();
				final BarcodeModel barcodemodel = modelService.create(BarcodeModel.class);
				barcodemodel.setItemId(barcode.getItemId());
				barcodelist.add(barcodemodel);
			}
		}
		if (barcodelist.size() > 0)
		{
			sizeVariantModel.setBarcodeList(barcodelist);
		}

		modelService.save(sizeVariantModel);
		if (null != sizeVariantModel.getCode())
		{
			LOG.info("SKU---->" + sizeVariantModel.getCode());
		}
		if (null != sizeVariantModel.getName())
		{
			LOG.info("SKU---->" + sizeVariantModel.getName());
		}


		if (toAdd)
		{
			variantsList.add(sizeVariantModel);
		}

		return variantsList;
	}

}
