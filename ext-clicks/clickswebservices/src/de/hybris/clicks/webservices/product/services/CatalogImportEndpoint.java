package de.hybris.clicks.webservices.product.services;

import de.hybris.clicks.webservices.product.populators.ProductStockImportPopulator;
import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue;
import de.hybris.platform.core.Registry;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.cxf.jaxws.context.WebServiceContextImpl;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;


@WebService(endpointInterface = "de.hybris.clicks.webservices.product.services.CatalogImportEndpointInterface")
@Endpoint
public class CatalogImportEndpoint implements CatalogImportEndpointInterface
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/Catalogue";
	public static final String GET_PERSONS_REQUEST = "Catalogue";

	@Resource
	ProductStockImportPopulator productStockImportPopulator;
	@Resource
	WebServiceContext webServiceContext = new WebServiceContextImpl();


	public WebServiceContext getWebServiceContext()
	{
		return webServiceContext;
	}

	public void setWebServiceContext(final WebServiceContext webServiceContext)
	{
		this.webServiceContext = webServiceContext;
	}

	@Override
	@PayloadRoot(localPart = "Catalogue", namespace = "http://Clicks.BizTalk.Omni/Schema/Catalogue")
	public void getCatalogue(@RequestPayload final Catalogue catalog)
	{
		/*
		 * 
		 * final Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
		 * 
		 * 
		 * final List userList = (List) http_headers.get("Username"); final List passList = (List)
		 * http_headers.get("Password");
		 * 
		 * String username = ""; String password = "";
		 * 
		 * if (userList != null) { username = userList.get(0).toString(); } if (passList != null) { password =
		 * passList.get(0).toString(); }
		 */
		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				final boolean result = productStockImportPopulator.populate(catalog);
			}
		});
		thread.start();


	}
}
