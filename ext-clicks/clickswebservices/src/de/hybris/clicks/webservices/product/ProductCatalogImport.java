/**
 *
 */
package de.hybris.clicks.webservices.product;

import de.hybris.clicks.webservices.product.schemas.ProductCatalogue;


/**
 * @author abhaydh
 *
 */
public interface ProductCatalogImport
{
	boolean importProductCatalogData(ProductCatalogue productData);

}
