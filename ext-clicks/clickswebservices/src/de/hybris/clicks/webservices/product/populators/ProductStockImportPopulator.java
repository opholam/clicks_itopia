/**
 *
 */
package de.hybris.clicks.webservices.product.populators;

import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue;
import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue.Body.GeneralTables.DepartmentHierarchy.ItemHierarchy;
import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue.Body.Items.Item;
import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue.Body.Items.Item.LinkedItems.LinkedItem;
import de.hybris.clicks.webservices.product.schemas.catalog.Catalogue.Body.Items.Item.StockAvailability.Stock;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class ProductStockImportPopulator
{
	protected static final Logger LOG = Logger.getLogger(ProductStockImportPopulator.class);
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	ProductService productService;

	@Resource
	private StockService stockService;

	@Resource
	private UserService userService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CategoryService categoryService;

	/**
	 * @param catalog
	 * @return
	 */
	public boolean populate(final Catalogue catalog)
	{

		userService.setCurrentUser(userService.getAdminUser());
		if (null != catalog.getBody().getItems() && catalog.getBody().getItems().getItem().size() > 0)
		{
			for (final Iterator<Item> itemIterator = catalog.getBody().getItems().getItem().iterator(); itemIterator.hasNext();)
			{
				userService.setCurrentUser(userService.getAdminUser());
				final Item item = itemIterator.next();

				LOG.info("--------stock import for SKU ID ----->" + item.getSKU());

				String productquery;
				SearchResult<ProductModel> productresult = null;
				productquery = "SELECT {PK} FROM {Product} WHERE {code}=?skuID";
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put("skuID", item.getSKU());
				productresult = flexibleSearchService.search(productquery, params);
				if (productresult.getCount() > 0)
				{
					for (final ProductModel product : productresult.getResult())
					{
						try
						{
							if (null != item.getStockAvailability() && item.getStockAvailability().getStock().size() > 0)
							{
								for (final Iterator<Stock> stockIterator = item.getStockAvailability().getStock().iterator(); stockIterator
										.hasNext();)
								{
									final Stock stock = stockIterator.next();

									WarehouseModel warehouseModel = new WarehouseModel();
									warehouseModel.setCode(stock.getStoreId());
									try
									{
										warehouseModel = flexibleSearchService.getModelByExample(warehouseModel);
									}
									catch (final Exception e)
									{
										warehouseModel = modelService.create(WarehouseModel.class);
									}

									if (null == warehouseModel.getName())
									{
										warehouseModel.setName(stock.getStoreId());
										warehouseModel.setCode(stock.getStoreId());


										VendorModel vendorModel = modelService.create(VendorModel.class);
										vendorModel.setCode("default");
										try
										{
											vendorModel = flexibleSearchService.getModelByExample(vendorModel);
										}
										catch (final Exception e)
										{
											e.printStackTrace();
										}
										if (null != vendorModel.getName())
										{
											warehouseModel.setVendor(vendorModel);
										}

										PointOfServiceModel posModel = modelService.create(PointOfServiceModel.class);
										posModel.setName(stock.getStoreId());
										try
										{
											posModel = flexibleSearchService.getModelByExample(posModel);
										}
										catch (final Exception e)
										{
											e.printStackTrace();
										}
										if (null != posModel.getDisplayName())
										{
											final List<PointOfServiceModel> posList = new ArrayList<PointOfServiceModel>();
											posList.add(posModel);
											warehouseModel.setPointsOfService(posList);
										}


										modelService.save(warehouseModel);
									}

									int availableStock = -1;

									try
									{
										availableStock = stockService.getStockLevelAmount(product, warehouseModel);
									}
									catch (final Exception e)
									{
										e.printStackTrace();
									}

									if (availableStock == -1)
									{
										final StockLevelModel stockLevelModel = modelService.create(StockLevelModel.class);
										final List<ProductModel> productList = new ArrayList<ProductModel>();
										productList.add(product);

										stockLevelModel.setProduct(product);
										stockLevelModel.setProductCode(product.getCode());
										stockLevelModel.setProducts(productList);
										stockLevelModel.setWarehouse(warehouseModel);
										stockLevelModel.setAvailable(stock.getBalance().intValue());

										modelService.save(stockLevelModel);

									}
									else
									{
										StockLevelModel stockLevelModel = modelService.create(StockLevelModel.class);
										stockLevelModel = stockService.getStockLevel(product, warehouseModel);
										if (item.getAction().equalsIgnoreCase("I"))
										{
											stockLevelModel.setAvailable(stock.getBalance().intValue());
											modelService.save(stockLevelModel);
										}
										else
										{
											modelService.remove(stockLevelModel);
										}

									}
								}
							}


							List<ProductReferenceModel> LinkedItemsList = new ArrayList<ProductReferenceModel>();
							final List<ProductReferenceModel> LinkedItemsList2 = new ArrayList<ProductReferenceModel>();
							if (product.getProductReferences().size() > 0)
							{
								LinkedItemsList = (List<ProductReferenceModel>) product.getProductReferences();
							}

							if (null != item.getLinkedItems() && item.getLinkedItems().getLinkedItem().size() > 0)
							{
								for (final Iterator<LinkedItem> linkediterator = item.getLinkedItems().getLinkedItem().iterator(); linkediterator
										.hasNext();)
								{
									final LinkedItem linkedItem = linkediterator.next();

									if (linkedItem.getAction().equals("I") || linkedItem.getAction().equals("U"))
									{
										ProductReferenceModel linkedItemModel = modelService.create(ProductReferenceModel.class);
										if (linkedItem.getAction().equals("U"))
										{
											ProductReferenceModel refProduct2 = modelService.create(ProductReferenceModel.class);
											for (final Iterator<ProductReferenceModel> refIterator = LinkedItemsList.iterator(); refIterator
													.hasNext();)
											{
												refProduct2 = refIterator.next();
												ProductModel productModel = modelService.create(ProductModel.class);
												productModel = refProduct2.getTarget();
												if (linkedItem.getItemId().equals(productModel.getCode()))
												{
													linkedItemModel = refProduct2;
												}
											}
										}

										linkedItemModel.setPreselected(Boolean.TRUE);
										if (linkedItem.getAction().equals("I"))
										{
											linkedItemModel.setSource(product);
										}


										if (linkedItem.getType() == 1)
										{
											linkedItemModel.setReferenceType(ProductReferenceTypeEnum.CROSSELLING);
										}
										if (linkedItem.getType() == 2)
										{
											linkedItemModel.setReferenceType(ProductReferenceTypeEnum.UPSELLING);
										}
										if (linkedItem.getStatus() == 1)
										{
											linkedItemModel.setActive(Boolean.TRUE);
										}
										if (linkedItem.getStatus() == 2)
										{
											linkedItemModel.setActive(Boolean.FALSE);
										}
										ProductModel refProduct = modelService.create(ProductModel.class);
										try
										{
											refProduct = productService.getProductForCode(linkedItem.getItemId());
										}
										catch (final Exception e)
										{
											e.printStackTrace();
										}
										if (null != refProduct.getCode())
										{
											linkedItemModel.setTarget(refProduct);
										}
										if (linkedItem.getAction().equals("I"))
										{
											LinkedItemsList2.add(linkedItemModel);
										}
										else
										{
											modelService.save(linkedItemModel);
										}

									}
									if (linkedItem.getAction().equals("D"))
									{
										ProductReferenceModel refProduct2 = modelService.create(ProductReferenceModel.class);
										for (final Iterator<ProductReferenceModel> refIterator = LinkedItemsList.iterator(); refIterator
												.hasNext();)
										{
											refProduct2 = refIterator.next();
											ProductModel productModel = modelService.create(ProductModel.class);
											productModel = refProduct2.getTarget();

											if (linkedItem.getItemId().equals(productModel.getCode()))
											{
												modelService.remove(refProduct2);
											}
											modelService.save(productModel);

										}

									}
								}
							}

							if (LinkedItemsList.size() > 0)
							{
								LinkedItemsList2.addAll(LinkedItemsList);
							}
							if (LinkedItemsList2.size() > 0)
							{
								product.setProductReferences(LinkedItemsList2);
							}
							modelService.save(product);
						}
						catch (final Exception e)
						{
							e.printStackTrace();
						}
					}
				}

			}
		}

		if (null != catalog.getBody().getGeneralTables() && null != catalog.getBody().getGeneralTables().getDepartmentHierarchy()
				&& catalog.getBody().getGeneralTables().getDepartmentHierarchy().getItemHierarchy().size() > 0)
		{
			final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
					Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));

			CategoryModel deptCategoryModel;
			final String query = "SELECT * FROM {Category} WHERE {code}='Departments' and {catalogVersion} = ?catalogVersion";
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("catalogVersion", catalogVersionModel);
			final SearchResult<CategoryModel> result = flexibleSearchService.search(query, params);
			if (result.getCount() == 0)
			{
				deptCategoryModel = modelService.create(CategoryModel.class);
				deptCategoryModel.setCode("Departments");
				deptCategoryModel.setName("Departments");
				//deptCategoryModel.setAllowedPrincipals(pricipalList);
				deptCategoryModel.setCatalogVersion(catalogVersionModel);
				modelService.save(deptCategoryModel);
			}
			else
			{
				deptCategoryModel = getCategoryService().getCategoryForCode(catalogVersionModel, "Departments");
			}


			PrincipalModel principalModel = modelService.create(PrincipalModel.class);
			principalModel.setUid("customergroup");
			principalModel = flexibleSearchService.getModelByExample(principalModel);
			final List<PrincipalModel> pricipalList = new ArrayList<PrincipalModel>();
			pricipalList.add(principalModel);


			for (final Iterator<ItemHierarchy> itemiterator = catalog.getBody().getGeneralTables().getDepartmentHierarchy()
					.getItemHierarchy().iterator(); itemiterator.hasNext();)
			{
				final ItemHierarchy itemHierarchy = itemiterator.next();
				if (itemHierarchy.getAction().equals("I") || itemHierarchy.getAction().equals("U"))
				{
					final List<CategoryModel> superCategoryList = new ArrayList<CategoryModel>();
					CategoryModel categoryModel = modelService.create(CategoryModel.class);
					categoryModel.setCode(itemHierarchy.getCategoryId());
					categoryModel.setCatalogVersion(catalogVersionModel);
					try
					{
						categoryModel = flexibleSearchService.getModelByExample(categoryModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}

					//					if (null == categoryModel.getCatalogVersion())
					//					{
					//						// creating new category - insert action
					//						//categoryModel.setAllowedPrincipals(pricipalList);
					//						categoryModel.setCatalogVersion(catalogVersionModel);
					//					}
					categoryModel.setLevel(new Integer(itemHierarchy.getLevel().intValue()));
					categoryModel.setLevelName(itemHierarchy.getLevelName());
					categoryModel.setDescription(itemHierarchy.getCategoryDescription());
					categoryModel.setName(itemHierarchy.getCategoryDescription());

					CategoryModel parentCategoryModel = modelService.create(CategoryModel.class);
					parentCategoryModel.setCode(itemHierarchy.getParentCategoryId());
					try
					{
						parentCategoryModel = flexibleSearchService.getModelByExample(parentCategoryModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
					if (itemHierarchy.getParentCategoryId().equalsIgnoreCase("0"))
					{
						superCategoryList.add(deptCategoryModel);
						categoryModel.setSupercategories(superCategoryList);
					}
					if (null != parentCategoryModel.getCatalogVersion())
					{
						superCategoryList.add(parentCategoryModel);
						categoryModel.setSupercategories(superCategoryList);
					}

					modelService.save(categoryModel);
				}
				if (itemHierarchy.getAction().equals("D"))
				{
					CategoryModel categoryModel = modelService.create(CategoryModel.class);
					categoryModel.setCode(itemHierarchy.getCategoryId());
					try
					{
						categoryModel = flexibleSearchService.getModelByExample(categoryModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}

					if (null != categoryModel.getCatalogVersion())
					{
						modelService.remove(categoryModel);
					}
				}

			}
		}
		return true;
	}

	/**
	 * @return the categoryService
	 */
	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	/**
	 * @param categoryService
	 *           the categoryService to set
	 */
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}
}
