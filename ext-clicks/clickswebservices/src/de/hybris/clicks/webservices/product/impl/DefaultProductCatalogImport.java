/**
 *
 */
package de.hybris.clicks.webservices.product.impl;

import de.hybris.clicks.webservices.product.ProductCatalogImport;
import de.hybris.clicks.webservices.product.populators.ProductHeaderPopulator;
import de.hybris.clicks.webservices.product.populators.ProductServicePopulator;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class DefaultProductCatalogImport implements ProductCatalogImport
{

	@Resource
	private ModelService modelService;

	@Resource
	ProductHeaderPopulator productHeaderPopulator;

	@Resource
	ProductServicePopulator productServicePopulator;

	@Override
	public boolean importProductCatalogData(final ProductCatalogue productData)
	{
		productHeaderPopulator.populate(productData);
		productServicePopulator.populate(productData);
		return false;
	}

}
