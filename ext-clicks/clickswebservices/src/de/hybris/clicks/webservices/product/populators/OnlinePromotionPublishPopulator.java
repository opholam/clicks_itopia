/**
 *
 */
package de.hybris.clicks.webclient.product.populator;

import de.hybris.clicks.core.model.MediaItemModel;
import de.hybris.platform.acceleratorservices.model.promotions.AcceleratorProductBOGOFPromotionModel;
import de.hybris.platform.acceleratorservices.model.promotions.AcceleratorProductMultiBuyPromotionModel;
import de.hybris.platform.b2bacceleratorservices.model.promotions.OrderThresholdDiscountPercentagePromotionModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.model.promotions.PromotionOrderRestrictionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.AbstractPromotionRestrictionModel;
import de.hybris.platform.promotions.model.OrderThresholdChangeDeliveryModePromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdFreeGiftPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdFreeVoucherPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductBOGOFPromotionModel;
import de.hybris.platform.promotions.model.ProductBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductFixedPricePromotionModel;
import de.hybris.platform.promotions.model.ProductOneToOnePerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductPercentageDiscountPromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.ProductSteppedMultiBuyPromotionModel;
import de.hybris.platform.promotions.model.PromotionPriceRowModel;
import de.hybris.platform.promotions.model.PromotionProductRestrictionModel;
import de.hybris.platform.promotions.model.PromotionQuantityAndPricesRowModel;
import de.hybris.platform.promotions.model.PromotionUserRestrictionModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import catalog.omni.biztalk.clicks.schema.promotion.MonetaryValue;
import catalog.omni.biztalk.clicks.schema.promotion.Money;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Buckets;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Buckets.Bucket;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Media;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Media.Item;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Messages;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Messages.Message;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Properties;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Stickers;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Header;

import com.thoughtworks.xstream.XStream;






/**
 * @author abhaydh
 * 
 */
public class OnlinePromotionPublishPopulator
{
	protected static final Logger LOG = Logger.getLogger(OnlinePromotionPublishPopulator.class);
	public final String RETAILERID = "1";
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource
	private CategoryService categoryService;

	@Resource
	public PersistentKeyGenerator myNumberseriesClient;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	@Resource
	private HashMap<String, Integer> promoTypeMap;

	public PromotionMessage populate(final PromotionMessage promotionMessage)
	{
		//--- SET Header information---
		userService.setCurrentUser(userService.getAdminUser());
		final Header header = new Header();
		final GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		XMLGregorianCalendar date = null;
		try
		{
			date = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		}
		catch (final DatatypeConfigurationException e1)
		{
			//
		}

		header.setCreatedDateTime(date);
		header.setDataSource("Hybris");
		header.setEventCode("OMNIHIER");

		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		promotionMessage.setHeader(header);
		//--- end Header information---
		final Body body = new Body();
		promotionMessage.setBody(body);
		final Promotions promos = new Promotions();
		body.setPromotions(promos);
		final SearchResult<AbstractPromotionModel> promoSearchResult = flexibleSearchService.search("SELECT {"
				+ AbstractPromotionModel.PK + "} FROM {" + AbstractPromotionModel._TYPECODE + "}");
		final List<AbstractPromotionModel> allPromotions = promoSearchResult.getResult();
		if (CollectionUtils.isNotEmpty(allPromotions))
		{
			Messages messages = null;
			Promotion promo = null;
			Properties properties = null;
			for (final AbstractPromotionModel abstractPromotionModel : allPromotions)
			{
				messages = new Messages();
				try
				{
					promo = new Promotion();
					promo.setIdentifier(abstractPromotionModel.getCode());
					promo.setAction("I");

					properties = new Properties();
					properties.setCampaign(abstractPromotionModel.getCampaign());
					properties
							.setCorrelationId(StringUtils.isNotEmpty(abstractPromotionModel.getCorrelationId()) ? abstractPromotionModel
									.getCorrelationId() : abstractPromotionModel.getPk().toString());
					properties.setDescription(abstractPromotionModel.getDescription());
					properties.setDisclaimer(abstractPromotionModel.getDisclaimer());
					properties.setEnabled(abstractPromotionModel.getEnabled());
					properties.setEndDate(new SimpleDateFormat("yyyy-MM-dd").format(abstractPromotionModel.getEndDate()));
					properties.setOffer(abstractPromotionModel.getOffer());
					properties.setPriority(abstractPromotionModel.getPriority());
					properties.setPromotionType(promoTypeMap.get(abstractPromotionModel.getItemtype()));
					properties.setStartDate(new SimpleDateFormat("yyyy-MM-dd").format(abstractPromotionModel.getStartDate()));
					properties.setTitle(abstractPromotionModel.getTitle());

					promo.setProperties(properties);
					final Triggers triggers = new Triggers();
					Trigger trigger = new Trigger();
					final Rewards rewards = new Rewards();

					final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets triggerBuckets = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets();

					if (abstractPromotionModel instanceof ProductPromotionModel)
					{
						final Buckets buckets = new Buckets();
						final ProductPromotionModel productPromotionModel = (ProductPromotionModel) abstractPromotionModel;
						if (CollectionUtils.isNotEmpty(productPromotionModel.getProducts()))
						{
							final Bucket bucket = new Bucket();
							bucket.setBucketId("1");
							bucket.setItemTypeId(1);
							for (final ProductModel product : productPromotionModel.getProducts())
							{
								bucket.getItemId().add(product.getCode());
							}
							buckets.getBucket().add(bucket);
						}
						if (CollectionUtils.isNotEmpty(productPromotionModel.getCategories()))
						{
							final Bucket bucket = new Bucket();
							bucket.setBucketId("1");
							bucket.setItemTypeId(2);

							for (final CategoryModel category : productPromotionModel.getCategories())
							{
								bucket.getItemId().add(category.getCode());
							}
							buckets.getBucket().add(bucket);
						}
						final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket bucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket();
						bucket.setBucketId("1");
						triggerBuckets.getBucket().add(bucket);
						trigger.setBuckets(triggerBuckets);


						if (abstractPromotionModel instanceof ProductBOGOFPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							trigger.setQualifyingCount(((ProductBOGOFPromotionModel) abstractPromotionModel).getQualifyingCount());
							reward.setFreeCount(((ProductBOGOFPromotionModel) abstractPromotionModel).getFreeCount());
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((ProductBOGOFPromotionModel) abstractPromotionModel).getMessageFired(),
									((ProductBOGOFPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(), messages);
						}
						else if (abstractPromotionModel instanceof ProductFixedPricePromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((ProductFixedPricePromotionModel) abstractPromotionModel)
									.getProductFixedUnitPrice();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((ProductFixedPricePromotionModel) abstractPromotionModel).getMessageFired(), "", messages);
						}
						else if (abstractPromotionModel instanceof AcceleratorProductMultiBuyPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							trigger.setQualifyingCount(((AcceleratorProductMultiBuyPromotionModel) abstractPromotionModel)
									.getQualifyingCount());
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((AcceleratorProductMultiBuyPromotionModel) abstractPromotionModel)
									.getBundlePrices();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((AcceleratorProductMultiBuyPromotionModel) abstractPromotionModel).getMessageFired(),
									((AcceleratorProductMultiBuyPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						else if (abstractPromotionModel instanceof ProductSteppedMultiBuyPromotionModel)
						{
							final Collection<PromotionQuantityAndPricesRowModel> quantityAndPricesRowModels = ((ProductSteppedMultiBuyPromotionModel) abstractPromotionModel)
									.getQualifyingCountsAndBundlePrices();
							if (CollectionUtils.isNotEmpty(quantityAndPricesRowModels))
							{
								int count = 0;
								for (final PromotionQuantityAndPricesRowModel promotionQuantityAndPricesRowModel : quantityAndPricesRowModels)
								{
									count++;
									trigger = new Trigger();
									trigger.setBuckets(triggerBuckets);
									trigger.setTriggerOrdinal(count);
									trigger.setQualifyingCount(promotionQuantityAndPricesRowModel.getQuantity().intValue());
									final Reward reward = new Reward();
									final MonetaryValue monetaryValue = new MonetaryValue();
									final Money money = new Money();
									final Collection<PromotionPriceRowModel> prices = promotionQuantityAndPricesRowModel.getPrices();
									if (CollectionUtils.isNotEmpty(prices))
									{
										final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
												new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

										if (null != priceRow)
										{
											money.setCurrencyId(priceRow.getCurrency().getIsocode());
											money.setPrice(priceRow.getPrice());
											monetaryValue.getPrice().add(money);
											reward.setPrices(monetaryValue);
										}
									}
									reward.setRewardId(String.valueOf(count));
									rewards.getReward().add(reward);
									trigger.setRewardId(String.valueOf(count));
									triggers.getTrigger().add(trigger);
								}
							}
							setMessage(((ProductSteppedMultiBuyPromotionModel) abstractPromotionModel).getMessageFired(),
									((ProductSteppedMultiBuyPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(), messages);
						}
						else if (abstractPromotionModel instanceof ProductOneToOnePerfectPartnerPromotionModel)
						{
							final Bucket baseProductBucket = new Bucket();
							baseProductBucket.setBucketId("1");
							baseProductBucket.setItemTypeId(1);
							baseProductBucket.getItemId().add(
									((ProductOneToOnePerfectPartnerPromotionModel) abstractPromotionModel).getBaseProduct().getCode());
							buckets.getBucket().add(baseProductBucket);
							final Bucket partnerProductBucket = new Bucket();
							partnerProductBucket.setBucketId("2");
							partnerProductBucket.setItemTypeId(1);
							partnerProductBucket.getItemId().add(
									((ProductOneToOnePerfectPartnerPromotionModel) abstractPromotionModel).getPartnerProduct().getCode());
							buckets.getBucket().add(partnerProductBucket);
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket partnerBucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket();
							bucket.setBucketId("2");
							triggerBuckets.getBucket().add(partnerBucket);
							trigger.setBuckets(triggerBuckets);
							trigger.setRewardId("1");
							trigger.setTriggerOrdinal(1);
							triggers.getTrigger().add(trigger);
							final Reward reward = new Reward();
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((ProductOneToOnePerfectPartnerPromotionModel) abstractPromotionModel)
									.getBundlePrices();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							reward.setRewardId("1");
							rewards.getReward().add(reward);
							setMessage(((ProductOneToOnePerfectPartnerPromotionModel) abstractPromotionModel).getMessageFired(),
									((ProductOneToOnePerfectPartnerPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						else if (abstractPromotionModel instanceof ProductPerfectPartnerPromotionModel)
						{
							final Bucket partnerProductBucket = new Bucket();
							partnerProductBucket.setBucketId("2");
							partnerProductBucket.setItemTypeId(1);
							for (final ProductModel partnerProduct : ((ProductPerfectPartnerPromotionModel) abstractPromotionModel)
									.getPartnerProducts())
							{
								partnerProductBucket.getItemId().add(partnerProduct.getCode());
							}
							buckets.getBucket().add(partnerProductBucket);
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket partnerBucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket();
							bucket.setBucketId("2");
							triggerBuckets.getBucket().add(partnerBucket);
							trigger.setBuckets(triggerBuckets);
							trigger.setRewardId("1");
							trigger.setTriggerOrdinal(1);
							triggers.getTrigger().add(trigger);
							final Reward reward = new Reward();
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((ProductPerfectPartnerPromotionModel) abstractPromotionModel)
									.getPartnerPrices();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							reward.setRewardId("1");
							rewards.getReward().add(reward);
							setMessage(((ProductPerfectPartnerPromotionModel) abstractPromotionModel).getMessageFired(),
									((ProductPerfectPartnerPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(), messages);
						}
						else if (abstractPromotionModel instanceof ProductPerfectPartnerBundlePromotionModel)
						{
							final Bucket baseProductBucket = new Bucket();
							baseProductBucket.setBucketId("1");
							baseProductBucket.setItemTypeId(1);
							baseProductBucket.getItemId().add(
									((ProductPerfectPartnerBundlePromotionModel) abstractPromotionModel).getBaseProduct().getCode());
							buckets.getBucket().add(baseProductBucket);
							final Bucket partnerProductBucket = new Bucket();
							partnerProductBucket.setBucketId("2");
							partnerProductBucket.setItemTypeId(1);
							for (final ProductModel partnerProduct : ((ProductPerfectPartnerBundlePromotionModel) abstractPromotionModel)
									.getPartnerProducts())
							{
								partnerProductBucket.getItemId().add(partnerProduct.getCode());
							}
							buckets.getBucket().add(partnerProductBucket);
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket partnerBucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket();
							bucket.setBucketId("2");
							triggerBuckets.getBucket().add(partnerBucket);
							trigger.setBuckets(triggerBuckets);
							trigger.setRewardId("1");
							trigger.setTriggerOrdinal(1);
							triggers.getTrigger().add(trigger);
							final Reward reward = new Reward();
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((ProductPerfectPartnerBundlePromotionModel) abstractPromotionModel)
									.getBundlePrices();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							reward.setRewardId("1");
							rewards.getReward().add(reward);
							setMessage(((ProductPerfectPartnerBundlePromotionModel) abstractPromotionModel).getMessageFired(),
									((ProductPerfectPartnerBundlePromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						else if (abstractPromotionModel instanceof ProductBundlePromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							trigger.setRewardId("1");
							triggers.getTrigger().add(trigger);
							final Reward reward = new Reward();
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((ProductBundlePromotionModel) abstractPromotionModel)
									.getBundlePrices();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));

								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							reward.setRewardId("1");
							rewards.getReward().add(reward);
							setMessage(((ProductBundlePromotionModel) abstractPromotionModel).getMessageFired(),
									((ProductBundlePromotionModel) abstractPromotionModel).getMessageCouldHaveFired(), messages);
						}
						else if (abstractPromotionModel instanceof ProductPercentageDiscountPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							trigger.setRewardId("1");
							final Reward reward = new Reward();
							reward.setPercentageDiscount(((ProductPercentageDiscountPromotionModel) abstractPromotionModel)
									.getPercentageDiscount());
							reward.setRewardId("1");
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((ProductPercentageDiscountPromotionModel) abstractPromotionModel).getMessageFired(), "",
									messages);
						}
						else if (abstractPromotionModel instanceof AcceleratorProductBOGOFPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							trigger.setQualifyingCount(((AcceleratorProductBOGOFPromotionModel) abstractPromotionModel)
									.getQualifyingCount());
							reward.setFreeCount(((AcceleratorProductBOGOFPromotionModel) abstractPromotionModel).getFreeCount());
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((AcceleratorProductBOGOFPromotionModel) abstractPromotionModel).getMessageFired(),
									((AcceleratorProductBOGOFPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(), messages);
						}

						promo.setBuckets(buckets);
						promo.setMessages(messages);

					}
					else
					{
						//
						if (abstractPromotionModel instanceof OrderThresholdChangeDeliveryModePromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							try
							{
								reward.setDeliveryMode(Integer
										.parseInt(((OrderThresholdChangeDeliveryModePromotionModel) abstractPromotionModel)
												.getDeliveryMode().getCode()));
							}
							catch (final Exception e)
							{
								reward.setDeliveryMode(1);
							}
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((OrderThresholdChangeDeliveryModePromotionModel) abstractPromotionModel)
									.getThresholdTotals();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));
								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									trigger.setOrderThreshold(monetaryValue);
								}
							}
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((OrderThresholdChangeDeliveryModePromotionModel) abstractPromotionModel).getMessageFired(),
									((OrderThresholdChangeDeliveryModePromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						else if (abstractPromotionModel instanceof OrderThresholdDiscountPercentagePromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((OrderThresholdDiscountPercentagePromotionModel) abstractPromotionModel)
									.getThresholdTotals();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));
								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									trigger.setOrderThreshold(monetaryValue);
								}
							}
							reward.setPercentageDiscount(((OrderThresholdDiscountPercentagePromotionModel) abstractPromotionModel)
									.getPercentageDiscount());
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							setMessage(((OrderThresholdDiscountPercentagePromotionModel) abstractPromotionModel).getMessageFired(),
									((OrderThresholdDiscountPercentagePromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						else if (abstractPromotionModel instanceof OrderThresholdFreeGiftPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((OrderThresholdFreeGiftPromotionModel) abstractPromotionModel)
									.getThresholdTotals();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));
								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									trigger.setOrderThreshold(monetaryValue);
								}
							}
							final Bucket giftProductBucket = new Bucket();
							giftProductBucket.setBucketId("1");
							giftProductBucket.setItemTypeId(1);
							giftProductBucket.getItemId().add(
									((OrderThresholdFreeGiftPromotionModel) abstractPromotionModel).getGiftProduct().getCode());
							final Buckets buckets = new Buckets();
							buckets.getBucket().add(giftProductBucket);
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets rewardBuckets = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets();
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket rewardBucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket();
							rewardBucket.setBucketId("1");
							rewardBuckets.getBucket().add(rewardBucket);
							reward.setBuckets(rewardBuckets);
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							promo.setBuckets(buckets);
							setMessage(((OrderThresholdFreeGiftPromotionModel) abstractPromotionModel).getMessageFired(),
									((OrderThresholdFreeGiftPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(), messages);
						}
						else if (abstractPromotionModel instanceof OrderThresholdFreeVoucherPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((OrderThresholdFreeVoucherPromotionModel) abstractPromotionModel)
									.getThresholdTotals();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));
								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									trigger.setOrderThreshold(monetaryValue);
								}
							}
							final Bucket giftProductBucket = new Bucket();
							giftProductBucket.setBucketId("1");
							giftProductBucket.setItemTypeId(3);
							giftProductBucket.getItemId().add(
									((OrderThresholdFreeVoucherPromotionModel) abstractPromotionModel).getFreeVoucher().getCode());
							final Buckets buckets = new Buckets();
							buckets.getBucket().add(giftProductBucket);
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets rewardBuckets = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets();
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket rewardBucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket();
							rewardBucket.setBucketId("1");
							rewardBuckets.getBucket().add(rewardBucket);
							reward.setBuckets(rewardBuckets);
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							promo.setBuckets(buckets);
							setMessage(((OrderThresholdFreeVoucherPromotionModel) abstractPromotionModel).getMessageFired(),
									((OrderThresholdFreeVoucherPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						else if (abstractPromotionModel instanceof OrderThresholdPerfectPartnerPromotionModel)
						{
							trigger.setTriggerOrdinal(1);
							final Reward reward = new Reward();
							reward.setRewardId("1");
							trigger.setRewardId("1");
							final MonetaryValue monetaryValue = new MonetaryValue();
							final Money money = new Money();
							final Collection<PromotionPriceRowModel> prices = ((OrderThresholdPerfectPartnerPromotionModel) abstractPromotionModel)
									.getThresholdTotals();
							if (CollectionUtils.isNotEmpty(prices))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(prices,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));
								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									trigger.setOrderThreshold(monetaryValue);
								}
							}
							trigger.setIncludeDiscountedPriceInThreshold(String
									.valueOf(((OrderThresholdPerfectPartnerPromotionModel) abstractPromotionModel)
											.getIncludeDiscountedPriceInThreshold()));

							final Collection<PromotionPriceRowModel> rewardPrice = ((OrderThresholdPerfectPartnerPromotionModel) abstractPromotionModel)
									.getProductPrices();
							if (CollectionUtils.isNotEmpty(rewardPrice))
							{
								final PromotionPriceRowModel priceRow = (PromotionPriceRowModel) CollectionUtils.find(rewardPrice,
										new BeanPropertyValueEqualsPredicate("currency.isocode", "ZAR", true));
								if (null != priceRow)
								{
									money.setCurrencyId(priceRow.getCurrency().getIsocode());
									money.setPrice(priceRow.getPrice());
									monetaryValue.getPrice().add(money);
									reward.setPrices(monetaryValue);
								}
							}
							final Bucket giftProductBucket = new Bucket();
							giftProductBucket.setBucketId("1");
							giftProductBucket.setItemTypeId(1);
							giftProductBucket.getItemId().add(
									((OrderThresholdPerfectPartnerPromotionModel) abstractPromotionModel).getDiscountProduct().getCode());
							final Buckets buckets = new Buckets();
							buckets.getBucket().add(giftProductBucket);
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets rewardBuckets = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets();
							final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket rewardBucket = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket();
							rewardBucket.setBucketId("1");
							rewardBuckets.getBucket().add(rewardBucket);
							reward.setBuckets(rewardBuckets);
							rewards.getReward().add(reward);
							triggers.getTrigger().add(trigger);
							promo.setBuckets(buckets);
							setMessage(((OrderThresholdPerfectPartnerPromotionModel) abstractPromotionModel).getMessageFired(),
									((OrderThresholdPerfectPartnerPromotionModel) abstractPromotionModel).getMessageCouldHaveFired(),
									messages);
						}
						promo.setMessages(messages);
					}


					promo.setTriggers(triggers);

					promo.setRewards(rewards);

					final Media media = new Media();
					for (final MediaItemModel medisItem : abstractPromotionModel.getMediaItems())
					{
						final Item item = new Item();
						item.setType(medisItem.getType());
						item.setValue(medisItem.getValue());
						media.getItem().add(item);
					}

					promo.setMedia(media);

					if (CollectionUtils.isNotEmpty(abstractPromotionModel.getStickers()))
					{
						final Stickers stickers = new Stickers();
						promo.setStickers(stickers);
						for (final String stickeId : abstractPromotionModel.getStickers())
						{
							stickers.getStickerId().add(stickeId);
						}
					}

					if (CollectionUtils.isNotEmpty(abstractPromotionModel.getRestrictions()))
					{
						final Restrictions restrictions = new Restrictions();

						for (final AbstractPromotionRestrictionModel restrictionModel : abstractPromotionModel.getRestrictions())
						{
							final Restriction restriction = new Restriction();
							if (restrictionModel instanceof PromotionUserRestrictionModel)
							{
								restriction.setRestrictionType("1");
								for (final PrincipalModel principal : ((PromotionUserRestrictionModel) restrictionModel).getUsers())
								{
									final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item restrictionItem = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item();
									restrictionItem
											.setFilter(((PromotionUserRestrictionModel) restrictionModel).getPositive() ? "I" : "E");
									restrictionItem.setValue(principal.getUid());
									restriction.getItem().add(restrictionItem);
								}
							}
							else if (restrictionModel instanceof PromotionProductRestrictionModel)
							{
								restriction.setRestrictionType("2");
								for (final ProductModel product : ((PromotionProductRestrictionModel) restrictionModel).getProducts())
								{
									final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item restrictionItem = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item();
									restrictionItem.setFilter("I");
									restrictionItem.setValue(product.getCode());
									restriction.getItem().add(restrictionItem);
								}
							}
							else if (restrictionModel instanceof PromotionOrderRestrictionModel)
							{
								restriction.setRestrictionType("3");
								for (final AbstractOrderModel order : ((PromotionOrderRestrictionModel) restrictionModel).getOrders())
								{
									final catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item restrictionItem = new catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item();
									restrictionItem.setFilter("I");
									restrictionItem.setValue(order.getCode());
									restriction.getItem().add(restrictionItem);
								}
							}
							restrictions.getRestriction().add(restriction);
						}
						promo.setRestrictions(restrictions);
					}
					promos.getPromotion().add(promo);
				}
				catch (final Exception e)
				{
					LOG.error("Error at OnlinePromotionPublishPopulator class at populate()" + e.getMessage());
				}
			}
		}
		FileOutputStream fout2 = null;
		ObjectOutputStream oos2 = null;
		try
		{
			final XStream xStream = new XStream();
			System.out.println(xStream.toXML(promotionMessage));
			fout2 = new FileOutputStream("E:/home/born/promomsg.txt");
			oos2 = new ObjectOutputStream(fout2);
			oos2.writeObject(xStream.toXML(promotionMessage));

		}
		catch (final Exception e)
		{
			//
		}
		finally
		{
			if (null != fout2)
			{
				IOUtils.closeQuietly(fout2);
			}
			if (null != oos2)
			{
				IOUtils.closeQuietly(oos2);
			}
		}
		return promotionMessage;
	}

	private void setMessage(final String messageFired, final String messageCouldHaveFired, final Messages messages)
	{
		Message message = null;
		if (StringUtils.isNotEmpty(messageFired))
		{
			message = new Message();
			message.setType(1);
			message.setValue(messageFired);
			messages.getMessage().add(message);
		}
		if (StringUtils.isNotEmpty(messageCouldHaveFired))
		{
			message = new Message();
			message.setType(2);
			message.setValue(messageCouldHaveFired);
			messages.getMessage().add(message);
		}
	}

	/**
	 * @return the promoTypeMap
	 */
	public HashMap<String, Integer> getPromoTypeMap()
	{
		return promoTypeMap;
	}

	/**
	 * @param promoTypeMap
	 *           the promoTypeMap to set
	 */
	public void setPromoTypeMap(final HashMap<String, Integer> promoTypeMap)
	{
		this.promoTypeMap = promoTypeMap;
	}
}
