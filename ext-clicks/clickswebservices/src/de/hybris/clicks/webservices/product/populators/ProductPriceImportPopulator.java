/**
 *
 */
package de.hybris.clicks.webservices.product.populators;

import de.hybris.clicks.core.model.PriceHeaderDetailsModel;
import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.clicks.webservices.product.schemas.price.ProductPrice;
import de.hybris.clicks.webservices.product.schemas.price.ProductPrice.Body.Batch;
import de.hybris.clicks.webservices.product.schemas.price.ProductPrice.Body.Batch.Item;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class ProductPriceImportPopulator
{
	protected static final Logger LOG = Logger.getLogger(ProductPriceImportPopulator.class);

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ProductService productService;

	@SuppressWarnings("deprecation")
	public boolean populate(final ProductPrice price)
	{
		final ProdHeaderDetailsModel headerModel = modelService.create(ProdHeaderDetailsModel.class);
		populateHeader(price, headerModel);
		modelService.save(headerModel);
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel stagedVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
		final CatalogVersionModel onlineVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.online.version"));


		for (final Iterator<Batch> batchIterator = price.getBody().getBatch().iterator(); batchIterator.hasNext();)
		{
			final Batch batch = batchIterator.next();
			List<ProductModel> productModelList;
			for (final Iterator<Item> itemIterator = batch.getItem().iterator(); itemIterator.hasNext();)
			{
				final Item item = itemIterator.next();
				ProductModel productModel = null;
				productModelList = new ArrayList<ProductModel>();
				try
				{
					productModel = productService.getProductForCode(stagedVersionModel, item.getSKU());
					productModelList.add(productModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				try
				{
					productModel = productService.getProductForCode(onlineVersionModel, item.getSKU());
					productModelList.add(productModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}

				for (final ProductModel product : productModelList)
				{
					if (null != product)
					{
						for (final PriceRowModel priceRow : product.getEurope1Prices())
						{
							if (batch.getDueDate().toGregorianCalendar().getTime().compareTo(priceRow.getStartTime()) == 0)
							{
								modelService.remove(priceRow);
							}
						}
						final PriceRowModel priceRowModel = modelService.create(PriceRowModel.class);
						UnitModel unitModel = modelService.create(UnitModel.class);
						CurrencyModel currencyModel = modelService.create(CurrencyModel.class);

						unitModel.setCode("pieces");
						unitModel = flexibleSearchService.getModelByExample(unitModel);
						priceRowModel.setUnit(unitModel);

						currencyModel.setIsocode("ZAR");
						currencyModel = flexibleSearchService.getModelByExample(currencyModel);
						priceRowModel.setCurrency(currencyModel);
						if (null != batch.getDueDate())
						{
							priceRowModel.setStartTime(batch.getDueDate().toGregorianCalendar().getTime());
							Calendar endDate = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
							endDate.set(Calendar.HOUR_OF_DAY, endDate.get(Calendar.HOUR_OF_DAY) + 2);
							endDate = batch.getDueDate().toGregorianCalendar();
							endDate.add(Calendar.YEAR, 10);
							priceRowModel.setEndTime(endDate.getTime());
						}

						priceRowModel.setPrice(new Double(item.getPrice().doubleValue()));
						priceRowModel.setProduct(product);

						modelService.save(priceRowModel);
						modelService.refresh(product);
						final List<PriceRowModel> priceModelList = new ArrayList<PriceRowModel>();
						if (!product.getEurope1Prices().isEmpty())
						{
							priceModelList.addAll(product.getEurope1Prices());
							Collections.sort(priceModelList, new Comparator<PriceRowModel>()
							{
								@Override
								public int compare(final PriceRowModel o1, final PriceRowModel o2)
								{
									return o1.getStartTime().compareTo(o2.getStartTime());
								}

							});
						}
						for (int i = 0; i < priceModelList.size(); i++)
						{
							if ((i + 1) != priceModelList.size())
							{
								priceModelList.get(i).setEndTime(priceModelList.get(i + 1).getStartTime());
							}
						}
						modelService.saveAll(priceModelList);
					}
				}
			}

		}

		return true;
	}

	private void populateHeader(final ProductPrice source, final ProdHeaderDetailsModel target)
	{
		if (null != source.getHeader().getDataSource())
		{
			target.setDataSource(source.getHeader().getDataSource());
		}
		if (null != source.getHeader().getEventCode())
		{
			target.setEventCode(source.getHeader().getEventCode());
		}
		if (null != source.getHeader().getEventLogId())
		{
			target.setEventLogId(source.getHeader().getEventLogId());
		}
		if (null != source.getHeader().getEventCode())
		{
			target.setFeedType(source.getHeader().getEventCode());
		}
		if (null != source.getBody().getTargetStore())
		{
			target.setRetailerId(source.getBody().getTargetStore());
		}

		final List<PriceHeaderDetailsModel> createList = new ArrayList<PriceHeaderDetailsModel>();
		int index = 0;
		for (final Iterator<Batch> batchIterator = source.getBody().getBatch().iterator(); batchIterator.hasNext();)
		{
			final Batch batch = batchIterator.next();
			index++;
			final PriceHeaderDetailsModel headerModel = modelService.create(PriceHeaderDetailsModel.class);
			headerModel.setBatch("Batch" + index);
			headerModel.setCreatedDate(batch.getCreatedDateTime().toGregorianCalendar().getTime());
			createList.add(headerModel);
		}
		target.setCreationDateList(createList);
	}

}
