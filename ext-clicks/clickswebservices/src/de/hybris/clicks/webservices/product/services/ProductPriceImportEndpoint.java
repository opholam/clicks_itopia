package de.hybris.clicks.webservices.product.services;

import de.hybris.clicks.webservices.product.populators.ProductPriceImportPopulator;
import de.hybris.clicks.webservices.product.schemas.price.ProductPrice;
import de.hybris.platform.core.Registry;

import javax.annotation.Resource;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;


@Endpoint
public class ProductPriceImportEndpoint
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/ProductPrice";
	public static final String GET_PERSONS_REQUEST = "ProductPrice";

	@Resource
	ProductPriceImportPopulator productPriceImportPopulator;

	@PayloadRoot(localPart = "ProductPrice", namespace = "http://Clicks.BizTalk.Omni/Schema/ProductPrice")
	public void getProductPrice(@RequestPayload final ProductPrice price)
	{
		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				final boolean result = productPriceImportPopulator.populate(price);
			}
		});
		thread.start();
	}
}
