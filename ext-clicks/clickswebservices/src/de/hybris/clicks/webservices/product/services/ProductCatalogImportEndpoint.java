package de.hybris.clicks.webservices.product.services;

import de.hybris.clicks.webservices.product.ProductCatalogImport;
import de.hybris.clicks.webservices.product.schemas.ProductCatalogue;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;


@Endpoint
public class ProductCatalogImportEndpoint
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/ProductCatalogue";
	public static final String GET_PERSONS_REQUEST = "ProductCatalogue";

	@Resource
	ProductCatalogImport productCatalogImport;
	@Resource
	private UserService userService;

	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;

	@PayloadRoot(localPart = "ProductCatalogue", namespace = "http://Clicks.BizTalk.Omni/Schema/ProductCatalogue")
	public void getProductCatalogue(@RequestPayload final ProductCatalogue product)
	{
		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				userService.setCurrentUser(userService.getAdminUser());

				if (product.getHeader().getDataSource().equalsIgnoreCase("deleteAllProducts"))
				{
					try
					{
						String productquery;
						SearchResult<ProductModel> productresult = null;
						productquery = "SELECT {PK} FROM {Product}";
						productresult = flexibleSearchService.search(productquery);
						if (productresult.getCount() > 0)
						{
							for (final ProductModel productModelTemp : productresult.getResult())
							{
								try
								{
									modelService.remove(productModelTemp);
								}
								catch (final Exception e)
								{
									e.printStackTrace();
								}
							}
						}
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					final boolean result = productCatalogImport.importProductCatalogData(product);
				}
			}
		});
		thread.start();

	}
}
