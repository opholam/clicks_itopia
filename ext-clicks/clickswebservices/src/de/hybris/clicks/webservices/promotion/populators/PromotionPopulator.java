/**
 *
 */
package de.hybris.clicks.webservices.promotion.populators;

import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion;
import de.hybris.platform.acceleratorservices.model.promotions.AcceleratorProductBOGOFPromotionModel;
import de.hybris.platform.acceleratorservices.model.promotions.AcceleratorProductMultiBuyPromotionModel;
import de.hybris.platform.b2bacceleratorservices.model.promotions.OrderThresholdDiscountPercentagePromotionModel;
import de.hybris.platform.promotions.jalo.OrderThresholdDiscountPromotion;
import de.hybris.platform.promotions.model.OrderThresholdChangeDeliveryModePromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdDiscountPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdFreeGiftPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdFreeVoucherPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductBOGOFPromotionModel;
import de.hybris.platform.promotions.model.ProductBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductFixedPricePromotionModel;
import de.hybris.platform.promotions.model.ProductOneToOnePerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductPercentageDiscountPromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductSteppedMultiBuyPromotionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class PromotionPopulator
{
	protected static final Logger LOG = Logger.getLogger(PromotionPopulator.class);
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;
	@Resource
	PromotionGenericPopulator promotionGenericPopulator;

	public void populate(final Promotion promotion)
	{
		String query = null;
		switch (promotion.getProperties().getPromotionType())
		{
			case 0:
				ProductFixedPricePromotionModel productFixedPricePromotion = modelService
						.create(ProductFixedPricePromotionModel.class);
				final SearchResult<ProductFixedPricePromotionModel> ProductFixedPricePromotionresult;
				try
				{
					query = "SELECT {PK} FROM {productFixedPricePromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					ProductFixedPricePromotionresult = flexibleSearchService.search(query);
					if (ProductFixedPricePromotionresult.getCount() > 0)
					{
						for (final ProductFixedPricePromotionModel itemModel : ProductFixedPricePromotionresult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productFixedPricePromotion = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productFixedPricePromotion = (ProductFixedPricePromotionModel) promotionGenericPopulator.populate(
						productFixedPricePromotion, promotion);
				break;

			case 1:
				ProductBOGOFPromotionModel productBOGOFPromotionModel;
				productBOGOFPromotionModel = modelService.create(ProductBOGOFPromotionModel.class);

				final SearchResult<ProductBOGOFPromotionModel> productBOGOFPromotionModelresult;
				try
				{
					query = "SELECT {PK} FROM {ProductBOGOFPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					productBOGOFPromotionModelresult = flexibleSearchService.search(query);
					if (productBOGOFPromotionModelresult.getCount() > 0)
					{
						for (final ProductBOGOFPromotionModel itemModel : productBOGOFPromotionModelresult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productBOGOFPromotionModel = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}

				productBOGOFPromotionModel = (ProductBOGOFPromotionModel) promotionGenericPopulator.populate(
						productBOGOFPromotionModel, promotion);
				break;

			case 2:
				ProductFixedPricePromotionModel productFixedPricePromotionModel;
				productFixedPricePromotionModel = modelService.create(ProductFixedPricePromotionModel.class);
				final SearchResult<ProductFixedPricePromotionModel> ProductFixedPricePromotionModelresult;
				try
				{
					query = "SELECT {PK} FROM {productFixedPricePromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					ProductFixedPricePromotionModelresult = flexibleSearchService.search(query);
					if (ProductFixedPricePromotionModelresult.getCount() > 0)
					{
						for (final ProductFixedPricePromotionModel itemModel : ProductFixedPricePromotionModelresult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productFixedPricePromotionModel = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productFixedPricePromotionModel = (ProductFixedPricePromotionModel) promotionGenericPopulator.populate(
						productFixedPricePromotionModel, promotion);
				break;

			case 3:
				AcceleratorProductMultiBuyPromotionModel acceleratorProductMultiBuy;
				acceleratorProductMultiBuy = modelService.create(AcceleratorProductMultiBuyPromotionModel.class);
				final SearchResult<AcceleratorProductMultiBuyPromotionModel> acceleratorProductMultiBuyPromotionModelresult;
				try
				{
					query = "SELECT {PK} FROM {AcceleratorProductMultiBuyPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					acceleratorProductMultiBuyPromotionModelresult = flexibleSearchService.search(query);
					if (acceleratorProductMultiBuyPromotionModelresult.getCount() > 0)
					{
						for (final AcceleratorProductMultiBuyPromotionModel itemModel : acceleratorProductMultiBuyPromotionModelresult
								.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								acceleratorProductMultiBuy = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				acceleratorProductMultiBuy = (AcceleratorProductMultiBuyPromotionModel) promotionGenericPopulator.populate(
						acceleratorProductMultiBuy, promotion);
				break;

			case 4:
				ProductSteppedMultiBuyPromotionModel productSteppedMultiBuy;
				productSteppedMultiBuy = modelService.create(ProductSteppedMultiBuyPromotionModel.class);
				final SearchResult<ProductSteppedMultiBuyPromotionModel> productSteppedMultiBuyPromotionModelresult;
				try
				{
					query = "SELECT {PK} FROM {ProductSteppedMultiBuyPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					productSteppedMultiBuyPromotionModelresult = flexibleSearchService.search(query);
					if (productSteppedMultiBuyPromotionModelresult.getCount() > 0)
					{
						for (final ProductSteppedMultiBuyPromotionModel itemModel : productSteppedMultiBuyPromotionModelresult
								.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productSteppedMultiBuy = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productSteppedMultiBuy = (ProductSteppedMultiBuyPromotionModel) promotionGenericPopulator.populate(
						productSteppedMultiBuy, promotion);
				break;

			case 5:
				ProductOneToOnePerfectPartnerPromotionModel productOneToOnePerfectPartner;
				productOneToOnePerfectPartner = modelService.create(ProductOneToOnePerfectPartnerPromotionModel.class);
				final SearchResult<ProductOneToOnePerfectPartnerPromotionModel> productOneToOnePerfectPartnerresult;
				try
				{
					query = "SELECT {PK} FROM {ProductOneToOnePerfectPartnerPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					productOneToOnePerfectPartnerresult = flexibleSearchService.search(query);
					if (productOneToOnePerfectPartnerresult.getCount() > 0)
					{
						for (final ProductOneToOnePerfectPartnerPromotionModel itemModel : productOneToOnePerfectPartnerresult
								.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productOneToOnePerfectPartner = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productOneToOnePerfectPartner = (ProductOneToOnePerfectPartnerPromotionModel) promotionGenericPopulator.populate(
						productOneToOnePerfectPartner, promotion);
				break;

			case 6:
				ProductPerfectPartnerPromotionModel productPerfectPartner;
				productPerfectPartner = modelService.create(ProductPerfectPartnerPromotionModel.class);
				final SearchResult<ProductPerfectPartnerPromotionModel> perfectPartnerPromotionResult;
				try
				{
					query = "SELECT {PK} FROM {ProductPerfectPartnerPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					perfectPartnerPromotionResult = flexibleSearchService.search(query);
					if (perfectPartnerPromotionResult.getCount() > 0)
					{
						for (final ProductPerfectPartnerPromotionModel itemModel : perfectPartnerPromotionResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productPerfectPartner = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productPerfectPartner = (ProductPerfectPartnerPromotionModel) promotionGenericPopulator.populate(
						productPerfectPartner, promotion);
				break;

			case 7:
				ProductPerfectPartnerBundlePromotionModel productPerfectPartnerBundle;
				productPerfectPartnerBundle = modelService.create(ProductPerfectPartnerBundlePromotionModel.class);
				final SearchResult<ProductPerfectPartnerBundlePromotionModel> perfectPartnerbundleResult;
				try
				{
					query = "SELECT {PK} FROM {ProductPerfectPartnerBundlePromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					perfectPartnerbundleResult = flexibleSearchService.search(query);
					if (perfectPartnerbundleResult.getCount() > 0)
					{
						for (final ProductPerfectPartnerBundlePromotionModel itemModel : perfectPartnerbundleResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productPerfectPartnerBundle = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productPerfectPartnerBundle = (ProductPerfectPartnerBundlePromotionModel) promotionGenericPopulator.populate(
						productPerfectPartnerBundle, promotion);
				break;

			case 8:
				ProductBundlePromotionModel productBundle;
				productBundle = modelService.create(ProductBundlePromotionModel.class);
				final SearchResult<ProductBundlePromotionModel> bundleResult;
				try
				{
					query = "SELECT {PK} FROM {ProductBundlePromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					bundleResult = flexibleSearchService.search(query);
					if (bundleResult.getCount() > 0)
					{
						for (final ProductBundlePromotionModel itemModel : bundleResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productBundle = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productBundle = (ProductBundlePromotionModel) promotionGenericPopulator.populate(productBundle, promotion);
				break;

			case 9:
				ProductPercentageDiscountPromotionModel productPercentageDiscount;
				productPercentageDiscount = modelService.create(ProductPercentageDiscountPromotionModel.class);
				final SearchResult<ProductPercentageDiscountPromotionModel> percentageDiscountResult;
				try
				{
					query = "SELECT {PK} FROM {ProductPercentageDiscountPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					percentageDiscountResult = flexibleSearchService.search(query);
					if (percentageDiscountResult.getCount() > 0)
					{
						for (final ProductPercentageDiscountPromotionModel itemModel : percentageDiscountResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								productPercentageDiscount = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				productPercentageDiscount = (ProductPercentageDiscountPromotionModel) promotionGenericPopulator.populate(
						productPercentageDiscount, promotion);
				break;

			case 10:
				AcceleratorProductBOGOFPromotionModel acceleratorProductBOGOF;
				acceleratorProductBOGOF = modelService.create(AcceleratorProductBOGOFPromotionModel.class);
				final SearchResult<AcceleratorProductBOGOFPromotionModel> acceleratorProductBOGOFresult;
				try
				{
					query = "SELECT {PK} FROM {AcceleratorProductBOGOFPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					acceleratorProductBOGOFresult = flexibleSearchService.search(query);
					if (acceleratorProductBOGOFresult.getCount() > 0)
					{
						for (final AcceleratorProductBOGOFPromotionModel itemModel : acceleratorProductBOGOFresult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								acceleratorProductBOGOF = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				acceleratorProductBOGOF = (AcceleratorProductBOGOFPromotionModel) promotionGenericPopulator.populate(
						acceleratorProductBOGOF, promotion);
				break;

			case 11:
				OrderThresholdChangeDeliveryModePromotionModel orderThresholdChangeDeliveryMode;
				orderThresholdChangeDeliveryMode = modelService.create(OrderThresholdChangeDeliveryModePromotionModel.class);
				final SearchResult<OrderThresholdChangeDeliveryModePromotionModel> orderThresholdChangeDeliveryModeResult;
				try
				{
					query = "SELECT {PK} FROM {OrderThresholdChangeDeliveryModePromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					orderThresholdChangeDeliveryModeResult = flexibleSearchService.search(query);
					if (orderThresholdChangeDeliveryModeResult.getCount() > 0)
					{
						for (final OrderThresholdChangeDeliveryModePromotionModel itemModel : orderThresholdChangeDeliveryModeResult
								.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								orderThresholdChangeDeliveryMode = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				orderThresholdChangeDeliveryMode = (OrderThresholdChangeDeliveryModePromotionModel) promotionGenericPopulator
						.populate(orderThresholdChangeDeliveryMode, promotion);
				break;

			case 12:
				/* OrderThresholdfixed Promotion */
				OrderThresholdDiscountPromotionModel orderThresholdFixedDiscount;
				orderThresholdFixedDiscount = modelService.create(OrderThresholdDiscountPromotion.class);
				final SearchResult<OrderThresholdDiscountPromotionModel> orderThresholdFixedDiscountResult;
				try
				{
					query = "SELECT {PK} FROM {OrderThresholdDiscountPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					orderThresholdFixedDiscountResult = flexibleSearchService.search(query);
					if (orderThresholdFixedDiscountResult.getCount() > 0)
					{
						for (final OrderThresholdDiscountPromotionModel discPromoModel : orderThresholdFixedDiscountResult.getResult())
						{
							if (StringUtils.isBlank(discPromoModel.getImmutableKey())
									&& StringUtils.isBlank(discPromoModel.getImmutableKeyHash()))
							{
								orderThresholdFixedDiscount = discPromoModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}


			case 13:
				OrderThresholdFreeGiftPromotionModel orderThresholdFreeGift;
				orderThresholdFreeGift = modelService.create(OrderThresholdFreeGiftPromotionModel.class);
				final SearchResult<OrderThresholdFreeGiftPromotionModel> orderThresholdFreeGiftResult;
				try
				{
					query = "SELECT {PK} FROM {OrderThresholdFreeGiftPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					orderThresholdFreeGiftResult = flexibleSearchService.search(query);
					if (orderThresholdFreeGiftResult.getCount() > 0)
					{
						for (final OrderThresholdFreeGiftPromotionModel itemModel : orderThresholdFreeGiftResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								orderThresholdFreeGift = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				orderThresholdFreeGift = (OrderThresholdFreeGiftPromotionModel) promotionGenericPopulator.populate(
						orderThresholdFreeGift, promotion);
				break;

			case 14:
				OrderThresholdFreeVoucherPromotionModel orderThresholdFreeVoucher;
				orderThresholdFreeVoucher = modelService.create(OrderThresholdFreeVoucherPromotionModel.class);
				final SearchResult<OrderThresholdFreeVoucherPromotionModel> orderThresholdFreeVoucherResult;
				try
				{
					query = "SELECT {PK} FROM {OrderThresholdFreeVoucherPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					orderThresholdFreeVoucherResult = flexibleSearchService.search(query);
					if (orderThresholdFreeVoucherResult.getCount() > 0)
					{
						for (final OrderThresholdFreeVoucherPromotionModel itemModel : orderThresholdFreeVoucherResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								orderThresholdFreeVoucher = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				orderThresholdFreeVoucher = (OrderThresholdFreeVoucherPromotionModel) promotionGenericPopulator.populate(
						orderThresholdFreeVoucher, promotion);
				break;

			case 15:
				OrderThresholdPerfectPartnerPromotionModel orderThresholdPerfectPartner;
				orderThresholdPerfectPartner = modelService.create(OrderThresholdPerfectPartnerPromotionModel.class);
				final SearchResult<OrderThresholdPerfectPartnerPromotionModel> orderThresholdPerfectPartnerResult;
				try
				{
					query = "SELECT {PK} FROM {OrderThresholdPerfectPartnerPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					orderThresholdPerfectPartnerResult = flexibleSearchService.search(query);
					if (orderThresholdPerfectPartnerResult.getCount() > 0)
					{
						for (final OrderThresholdPerfectPartnerPromotionModel itemModel : orderThresholdPerfectPartnerResult
								.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								orderThresholdPerfectPartner = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				orderThresholdPerfectPartner = (OrderThresholdPerfectPartnerPromotionModel) promotionGenericPopulator.populate(
						orderThresholdPerfectPartner, promotion);
				break;

			case 16:
				OrderThresholdDiscountPercentagePromotionModel orderThresholdDiscount;
				orderThresholdDiscount = modelService.create(OrderThresholdDiscountPercentagePromotionModel.class);
				final SearchResult<OrderThresholdDiscountPercentagePromotionModel> orderThresholdDiscountResult;
				try
				{
					query = "SELECT {PK} FROM {OrderThresholdDiscountPercentagePromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
							+ promotion.getIdentifier() + "'";
					orderThresholdDiscountResult = flexibleSearchService.search(query);
					if (orderThresholdDiscountResult.getCount() > 0)
					{
						for (final OrderThresholdDiscountPercentagePromotionModel itemModel : orderThresholdDiscountResult.getResult())
						{
							if (StringUtils.isBlank(itemModel.getImmutableKey()) && StringUtils.isBlank(itemModel.getImmutableKeyHash()))
							{
								orderThresholdDiscount = itemModel;
								break;
							}
						}
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				orderThresholdDiscount = (OrderThresholdDiscountPercentagePromotionModel) promotionGenericPopulator.populate(
						orderThresholdDiscount, promotion);
				break;
		}

	}

}
