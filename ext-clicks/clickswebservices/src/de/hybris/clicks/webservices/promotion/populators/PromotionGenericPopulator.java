/**
 *
 */
package de.hybris.clicks.webservices.promotion.populators;

import de.hybris.clicks.core.model.MediaItemModel;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Media;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Properties;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item;
import de.hybris.platform.acceleratorservices.model.promotions.AcceleratorProductBOGOFPromotionModel;
import de.hybris.platform.acceleratorservices.model.promotions.AcceleratorProductMultiBuyPromotionModel;
import de.hybris.platform.b2bacceleratorservices.model.promotions.OrderThresholdDiscountPercentagePromotionModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.model.promotions.PromotionOrderRestrictionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.AbstractPromotionRestrictionModel;
import de.hybris.platform.promotions.model.OrderThresholdChangeDeliveryModePromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdDiscountPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdFreeGiftPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdFreeVoucherPromotionModel;
import de.hybris.platform.promotions.model.OrderThresholdPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductBOGOFPromotionModel;
import de.hybris.platform.promotions.model.ProductBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductFixedPricePromotionModel;
import de.hybris.platform.promotions.model.ProductOneToOnePerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductPercentageDiscountPromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductSteppedMultiBuyPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.promotions.model.PromotionProductRestrictionModel;
import de.hybris.platform.promotions.model.PromotionUserRestrictionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author abhaydh
 * 
 */
public class PromotionGenericPopulator<T extends AbstractPromotionModel>
{
	@Resource
	ModelService modelService;
	@Resource
	FlexibleSearchService flexibleSearchService;
	@Resource
	private UserService userService;
	@Resource
	ProductBOGOFPromotionPopulator productBOGOFPromotionPopulator;

	@Resource
	ProductFixedPricePopulator productFixedPricePopulator;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	AcceleratorProductMultiBuyPopulator acceleratorProductMultiBuyPopulator;
	@Resource
	ProductSteppedMultiBuyPopulator productSteppedMultiBuyPopulator;
	@Resource
	ProductOneToOnePerfectPartnerPopulator productOneToOnePerfectPartnerPopulator;
	@Resource
	ProductPerfectPartnerPopulator productPerfectPartnerPopulator;

	@Resource
	ProductPerfectPartnerBundlePopulator productPerfectPartnerBundlePopulator;
	@Resource
	ProductBundlePopulator productBundlePopulator;
	@Resource
	ProductPercentageDiscountPopulator productPercentageDiscountPopulator;
	@Resource
	AcceleratorProductBOGOFPopulator acceleratorProductBOGOFPopulator;

	@Resource
	OrderThresholdChangeDeliveryModePopulator orderThresholdChangeDeliveryModePopulator;
	@Resource
	OrderThresholdDiscountPopulator orderThresholdDiscountPopulator;

	@Resource
	OrderThresholdFixedDiscountPopulator orderThresholdFixedDiscountPopulator;

	@Resource
	OrderThresholdFreeGiftPopulator orderThresholdFreeGiftPopulator;

	@Resource
	OrderThresholdFreeVoucherPopulator orderThresholdFreeVoucherPopulator;
	@Resource
	OrderThresholdPerfectPartnerPopulator orderThresholdPerfectPartnerPopulator;



	public T populate(T promotionModel, final Promotion promotion)
	{
		userService.setCurrentUser(userService.getAdminUser());

		PromotionGroupModel promotionGroup = modelService.create(PromotionGroupModel.class);
		promotionGroup.setIdentifier("clicksPromoGrp");
		try
		{
			promotionGroup = flexibleSearchService.getModelByExample(promotionGroup);
			promotionModel.setPromotionGroup(promotionGroup);
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		if (null != promotion.getIdentifier())
		{
			promotionModel.setCode(promotion.getIdentifier());
		}
		if (null != promotion.getProperties())
		{
			promotionModel = populateProperties(promotionModel, promotion.getProperties());
		}
		if (null != promotion.getMedia())
		{
			promotionModel = populateMedia(promotionModel, promotion.getMedia());
		}
		if (null != promotion.getStickers())
		{
			if (null != promotion.getStickers().getStickerId() && promotion.getStickers().getStickerId().size() > 0)
			{
				promotionModel.setStickers(promotion.getStickers().getStickerId());
			}
		}
		modelService.save(promotionModel);
		if (null != promotion.getRestrictions() && null != promotion.getRestrictions().getRestriction()
				&& promotion.getRestrictions().getRestriction().size() > 0)
		{
			promotionModel = populateRestrictions(promotionModel, promotion.getRestrictions().getRestriction());
		}


		//case 1
		if (promotionModel.getItemtype().equals(ProductBOGOFPromotionModel._TYPECODE))
		{
			promotionModel = (T) productBOGOFPromotionPopulator.populate((ProductBOGOFPromotionModel) promotionModel, promotion);
		}

		//case 2
		if (promotionModel instanceof ProductFixedPricePromotionModel)
		{
			promotionModel = (T) productFixedPricePopulator.populate((ProductFixedPricePromotionModel) promotionModel, promotion);
		}

		//case 3
		if (promotionModel instanceof AcceleratorProductMultiBuyPromotionModel)
		{
			promotionModel = (T) acceleratorProductMultiBuyPopulator.populate(
					(AcceleratorProductMultiBuyPromotionModel) promotionModel, promotion);
		}

		//case 4
		if (promotionModel instanceof ProductSteppedMultiBuyPromotionModel)
		{
			promotionModel = (T) productSteppedMultiBuyPopulator.populate((ProductSteppedMultiBuyPromotionModel) promotionModel,
					promotion);
		}

		//case 5
		if (promotionModel instanceof ProductOneToOnePerfectPartnerPromotionModel)
		{
			promotionModel = (T) productOneToOnePerfectPartnerPopulator.populate(
					(ProductOneToOnePerfectPartnerPromotionModel) promotionModel, promotion);
		}

		//case 6
		if (promotionModel instanceof ProductPerfectPartnerPromotionModel)
		{
			promotionModel = (T) productPerfectPartnerPopulator.populate((ProductPerfectPartnerPromotionModel) promotionModel,
					promotion);
		}

		//case 7
		if (promotionModel instanceof ProductPerfectPartnerBundlePromotionModel)
		{
			promotionModel = (T) productPerfectPartnerBundlePopulator.populate(
					(ProductPerfectPartnerBundlePromotionModel) promotionModel, promotion);
		}

		//case 8
		if (promotionModel instanceof ProductBundlePromotionModel)
		{
			promotionModel = (T) productBundlePopulator.populate((ProductBundlePromotionModel) promotionModel, promotion);
		}

		//case 9
		if (promotionModel instanceof ProductPercentageDiscountPromotionModel)
		{
			promotionModel = (T) productPercentageDiscountPopulator.populate(
					(ProductPercentageDiscountPromotionModel) promotionModel, promotion);
		}

		//case 10
		if (promotionModel instanceof AcceleratorProductBOGOFPromotionModel)
		{
			promotionModel = (T) acceleratorProductBOGOFPopulator.populate((AcceleratorProductBOGOFPromotionModel) promotionModel,
					promotion);
		}

		//case 11
		if (promotionModel instanceof OrderThresholdChangeDeliveryModePromotionModel)
		{
			promotionModel = (T) orderThresholdChangeDeliveryModePopulator.populate(
					(OrderThresholdChangeDeliveryModePromotionModel) promotionModel, promotion);
		}

		//case 12
		if (promotionModel instanceof OrderThresholdDiscountPromotionModel)
		{
			promotionModel = (T) orderThresholdFixedDiscountPopulator.populate(
					(OrderThresholdDiscountPromotionModel) promotionModel, promotion);
		}

		//case 13
		if (promotionModel instanceof OrderThresholdFreeGiftPromotionModel)
		{
			promotionModel = (T) orderThresholdFreeGiftPopulator.populate((OrderThresholdFreeGiftPromotionModel) promotionModel,
					promotion);
		}

		//case 14
		if (promotionModel instanceof OrderThresholdFreeVoucherPromotionModel)
		{
			promotionModel = (T) orderThresholdFreeVoucherPopulator.populate(
					(OrderThresholdFreeVoucherPromotionModel) promotionModel, promotion);
		}

		//case 15
		if (promotionModel instanceof OrderThresholdPerfectPartnerPromotionModel)
		{
			promotionModel = (T) orderThresholdPerfectPartnerPopulator.populate(
					(OrderThresholdPerfectPartnerPromotionModel) promotionModel, promotion);
		}
		//case 16
		if (promotionModel instanceof OrderThresholdDiscountPercentagePromotionModel)
		{
			promotionModel = (T) orderThresholdDiscountPopulator.populate(
					(OrderThresholdDiscountPercentagePromotionModel) promotionModel, promotion);
		}


		modelService.save(promotionModel);
		return promotionModel;

	}



	private T populateMedia(final T promotionModel, final Media media)
	{
		if (null != media.getItem() && media.getItem().size() > 0)
		{
			final List<MediaItemModel> mediaList = new ArrayList<MediaItemModel>();
			for (final de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Media.Item item : media
					.getItem())
			{
				final MediaItemModel mediaItem = modelService.create(MediaItemModel.class);
				if (item.getType() != 0)
				{
					mediaItem.setType(new Integer(item.getType()));
				}
				if (null != item.getValue())
				{
					mediaItem.setValue(item.getValue());
				}

				mediaList.add(mediaItem);
			}
			if (mediaList.size() > 0)
			{
				promotionModel.setMediaItems(mediaList);
			}
		}

		return promotionModel;
	}



	private T populateRestrictions(final T promotionModel, final List<Restriction> restrictions)
	{
		final List<AbstractPromotionRestrictionModel> restrictionsList = new ArrayList<AbstractPromotionRestrictionModel>();
		for (final Restriction restriction : restrictions)
		{
			if (null != restriction.getRestrictionType() && null != restriction.getItem() && null != restriction.getItem()
					&& restriction.getItem().size() > 0)
			{
				if (restriction.getRestrictionType().equalsIgnoreCase("1"))
				{
					final PromotionUserRestrictionModel restrictionModel = modelService.create(PromotionUserRestrictionModel.class);
					final List<PrincipalModel> principalModelList = new ArrayList<PrincipalModel>();

					for (final Item item : restriction.getItem())
					{
						boolean flag = false;
						PrincipalModel itemModel = modelService.create(PrincipalModel.class);
						itemModel.setUid(item.getValue());
						try
						{
							itemModel = flexibleSearchService.getModelByExample(itemModel);
						}
						catch (final Exception e)
						{
							flag = true;
							e.printStackTrace();
						}
						if (!flag)
						{
							principalModelList.add(itemModel);
						}
						if (null != item.getFilter())
						{
							if (item.getFilter().equalsIgnoreCase("I"))
							{
								restrictionModel.setPositive(new Boolean(true));
							}
							else
							{
								restrictionModel.setPositive(new Boolean(false));
							}
						}
					}
					restrictionModel.setPromotion(promotionModel);
					restrictionModel.setUsers(principalModelList);
					restrictionsList.add(restrictionModel);
				}

				if (restriction.getRestrictionType().equalsIgnoreCase("2"))
				{
					final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion("clicksProductCatalog",
							"Online");
					final PromotionProductRestrictionModel restrictionModel = modelService
							.create(PromotionProductRestrictionModel.class);
					final List<ProductModel> productModelList = new ArrayList<ProductModel>();

					for (final Item item : restriction.getItem())
					{
						boolean flag = false;
						ProductModel itemModel = new ProductModel();
						itemModel.setCode(item.getValue());

						itemModel.setCatalogVersion(catalogVersionModel);
						//itemModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
						try
						{
							itemModel = flexibleSearchService.getModelByExample(itemModel);
						}
						catch (final Exception e)
						{
							flag = true;
							e.printStackTrace();
						}
						if (!flag)
						{
							productModelList.add(itemModel);
						}

					}
					restrictionModel.setPromotion(promotionModel);
					restrictionModel.setProducts(productModelList);
					restrictionsList.add(restrictionModel);
				}
				if (restriction.getRestrictionType().equalsIgnoreCase("3"))
				{
					final PromotionOrderRestrictionModel restrictionModel = modelService.create(PromotionOrderRestrictionModel.class);
					final List<AbstractOrderModel> orderModelList = new ArrayList<AbstractOrderModel>();

					for (final Item item : restriction.getItem())
					{
						boolean flag = false;
						AbstractOrderModel itemModel = modelService.create(AbstractOrderModel.class);
						itemModel.setCode(item.getValue());
						try
						{
							itemModel = flexibleSearchService.getModelByExample(itemModel);
						}
						catch (final Exception e)
						{
							flag = true;
							e.printStackTrace();
						}
						if (!flag)
						{
							orderModelList.add(itemModel);
						}

					}
					restrictionModel.setPromotion(promotionModel);
					restrictionModel.setOrders(orderModelList);
					restrictionsList.add(restrictionModel);
				}
			}

		}

		if (restrictionsList.size() > 0)
		{
			promotionModel.setRestrictions(restrictionsList);
		}
		return promotionModel;
	}



	private T populateProperties(final T promotionModel, final Properties properties)
	{

		if (null != properties.getCorrelationId())
		{
			promotionModel.setCorrelationId(properties.getCorrelationId());
		}
		if (properties.getPromotionType() >= 0 && properties.getPromotionType() <= 15)
		{
			promotionModel.setPromotionTypeID(new Integer(properties.getPromotionType()));
		}
		if (null != properties.getCampaign())
		{
			promotionModel.setCampaign(properties.getCampaign());
		}
		if (null != properties.getOffer())
		{
			promotionModel.setOffer(properties.getOffer());
		}
		if (null != properties.getTitle())
		{
			promotionModel.setTitle(properties.getTitle());
		}
		if (null != properties.getDescription())
		{
			promotionModel.setDescription(properties.getDescription());
		}
		if (null != properties.getDisclaimer())
		{
			promotionModel.setDisclaimer(properties.getDisclaimer());
		}
		promotionModel.setEnabled(new Boolean(properties.isEnabled()));

		if (null != properties.getStartDate())
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
			final String dateInString = properties.getStartDate();
			Date date = getDate(formatter, dateInString);
			if (null == date)
			{
				formatter = new SimpleDateFormat("yyyy-MM-dd");
				date = getDate(formatter, dateInString);
			}
			if (null == date)
			{
				date = new Date();
			}
			promotionModel.setStartDate(date);
		}
		if (null != properties.getEndDate())
		{
			SimpleDateFormat endDateformatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
			final String endDateString = properties.getEndDate();
			Date endDate = getDate(endDateformatter, endDateString);
			if (null == endDate)
			{
				endDateformatter = new SimpleDateFormat("yyyy-MM-dd");
				endDate = getDate(endDateformatter, endDateString);
			}
			if (null == endDate)
			{
				endDate = new Date();
			}
			promotionModel.setEndDate(endDate);
		}
		promotionModel.setPriority(new Integer(properties.getPriority()));

		return promotionModel;
	}

	/**
	 * @param formatter
	 * @param dateInString
	 * @return
	 */
	private Date getDate(final SimpleDateFormat formatter, final String dateInString)
	{
		Date date = null;
		try
		{
			date = formatter.parse(dateInString);
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		return date;
	}
}
