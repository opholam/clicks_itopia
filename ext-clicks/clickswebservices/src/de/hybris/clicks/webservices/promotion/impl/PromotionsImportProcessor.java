/**
 *
 */
package de.hybris.clicks.webservices.promotion.impl;

import de.hybris.clicks.webservices.promotion.populators.PromotionPopulator;
import de.hybris.clicks.webservices.promotion.populators.PromotionsHeaderPopulator;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class PromotionsImportProcessor
{
	@Resource
	PromotionsHeaderPopulator promotionsHeaderPopulator;
	@Resource
	PromotionPopulator promotionPopulator;
	@Resource
	ModelService modelService;
	@Resource
	FlexibleSearchService flexibleSearchService;

	public void processPromotions(final PromotionMessage promotions)
	{
		if (null != promotions.getHeader())
		{
			promotionsHeaderPopulator.populate(promotions.getHeader());
		}
		if (null != promotions.getBody() && null != promotions.getBody().getPromotions()
				&& null != promotions.getBody().getPromotions().getPromotion()
				&& promotions.getBody().getPromotions().getPromotion().size() > 0)
		{
			for (final Promotion promotion : promotions.getBody().getPromotions().getPromotion())
			{
				if (promotion.getAction().equalsIgnoreCase("I"))
				{
					promotionPopulator.populate(promotion);
				}
				if (promotion.getAction().equalsIgnoreCase("D"))
				{
					String query = null;
					SearchResult<AbstractPromotionModel> result;
					try
					{
						query = "SELECT {PK} FROM {AbstractPromotion} WHERE {immutableKeyHash} IS NULL AND {code}='"
								+ promotion.getIdentifier() + "'";
						result = flexibleSearchService.search(query);
						if (result.getCount() > 0)
						{
							for (final AbstractPromotionModel itemModel : result.getResult())
							{
								modelService.remove(itemModel);
							}
						}
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
				}
			}

		}
	}
}
