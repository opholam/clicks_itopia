/**
 *
 */
package de.hybris.clicks.webservices.promotion.populators;

import de.hybris.clicks.webservices.promotion.schemas.Money;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Messages.Message;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.promotions.model.OrderThresholdChangeDeliveryModePromotionModel;
import de.hybris.platform.promotions.model.PromotionPriceRowModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class OrderThresholdChangeDeliveryModePopulator
{

	@Resource
	ModelService modelService;
	@Resource
	FlexibleSearchService flexibleSearchService;
	@Resource
	private UserService userService;
	@Resource
	private DeliveryModeService deliveryModeService;

	public OrderThresholdChangeDeliveryModePromotionModel populate(
			final OrderThresholdChangeDeliveryModePromotionModel promotionModel, final Promotion promotion)
	{
		userService.setCurrentUser(userService.getAdminUser());

		if (null != promotion.getMessages() && null != promotion.getMessages().getMessage()
				&& promotion.getMessages().getMessage().size() > 0)
		{
			for (final Message message : promotion.getMessages().getMessage())
			{
				if (message.getType() == 1)
				{
					promotionModel.setMessageFired(message.getValue());
				}
				if (message.getType() == 2)
				{
					promotionModel.setMessageCouldHaveFired(message.getValue());
				}
				if (message.getType() == 3)
				{
					promotionModel.setMessageCouldHaveFired(message.getValue());
				}
			}
		}

		if (null != promotion.getTriggers() && null != promotion.getTriggers().getTrigger()
				&& promotion.getTriggers().getTrigger().size() > 0)
		{
			for (final Trigger trigger : promotion.getTriggers().getTrigger())
			{
				//promotionModel.setTriggerOrdinal(new Integer(trigger.getTriggerOrdinal()));

				if (null != trigger.getOrderThreshold() && null != trigger.getOrderThreshold().getPrice()
						&& trigger.getOrderThreshold().getPrice().size() > 0)
				{
					final List<PromotionPriceRowModel> priceRowModelList = new ArrayList<PromotionPriceRowModel>();
					for (final Money price : trigger.getOrderThreshold().getPrice())
					{

						final PromotionPriceRowModel rowModel = modelService.create(PromotionPriceRowModel.class);
						CurrencyModel currencyModel = modelService.create(CurrencyModel.class);
						currencyModel.setIsocode(price.getCurrencyId());
						try
						{
							currencyModel = flexibleSearchService.getModelByExample(currencyModel);
						}
						catch (final Exception e)
						{
							e.printStackTrace();
							currencyModel.setIsocode("ZAR");
							currencyModel = flexibleSearchService.getModelByExample(currencyModel);
						}
						rowModel.setCurrency(currencyModel);
						rowModel.setPrice(new Double(price.getPrice()));

						priceRowModelList.add(rowModel);
					}
					if (priceRowModelList.size() > 0)
					{
						promotionModel.setThresholdTotals(priceRowModelList);
					}

				}

				if (null != trigger.getRewardId())
				{
					if (null != promotion.getRewards() && null != promotion.getRewards().getReward()
							&& promotion.getRewards().getReward().size() > 0)
					{
						for (final Reward reward : promotion.getRewards().getReward())
						{
							if (null != reward.getRewardId())
							{
								if (trigger.getRewardId().equalsIgnoreCase(reward.getRewardId()))
								{
									promotionModel.setDeliveryMode(deliveryModeService.getDeliveryModeForCode("free-standard-shipping"));
								}
							}
						}
					}
				}

			}
		}


		return promotionModel;
	}

}
