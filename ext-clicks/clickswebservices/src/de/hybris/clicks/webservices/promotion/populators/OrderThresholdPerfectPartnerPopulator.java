/**
 *
 */
package de.hybris.clicks.webservices.promotion.populators;

import de.hybris.clicks.webservices.promotion.schemas.Money;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Messages.Message;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Rewards.Reward;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.OrderThresholdPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.PromotionPriceRowModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class OrderThresholdPerfectPartnerPopulator
{
	@Resource
	ModelService modelService;
	@Resource
	FlexibleSearchService flexibleSearchService;
	@Resource
	private UserService userService;
	@Resource
	private CatalogVersionService catalogVersionService;

	public OrderThresholdPerfectPartnerPromotionModel populate(final OrderThresholdPerfectPartnerPromotionModel promotionModel,
			final Promotion promotion)
	{
		userService.setCurrentUser(userService.getAdminUser());

		if (null != promotion.getMessages() && null != promotion.getMessages().getMessage()
				&& promotion.getMessages().getMessage().size() > 0)
		{
			for (final Message message : promotion.getMessages().getMessage())
			{
				if (message.getType() == 1)
				{
					promotionModel.setMessageFired(message.getValue());
				}
				if (message.getType() == 2)
				{
					promotionModel.setMessageCouldHaveFired(message.getValue());
				}
				if (message.getType() == 3)
				{
					(promotionModel).setMessageCouldHaveFired(message.getValue());
				}
			}
		}

		if (null != promotion.getTriggers() && null != promotion.getTriggers().getTrigger()
				&& promotion.getTriggers().getTrigger().size() > 0)
		{
			for (final Trigger trigger : promotion.getTriggers().getTrigger())
			{
				//promotionModel.setTriggerOrdinal(new Integer(trigger.getTriggerOrdinal()));

				if (null != trigger.getIncludeDiscountedPriceInThreshold()
						&& trigger.getIncludeDiscountedPriceInThreshold().equalsIgnoreCase("true"))
				{
					promotionModel.setIncludeDiscountedPriceInThreshold(new Boolean(true));
				}
				if (null != trigger.getIncludeDiscountedPriceInThreshold()
						&& trigger.getIncludeDiscountedPriceInThreshold().equalsIgnoreCase("false"))
				{
					promotionModel.setIncludeDiscountedPriceInThreshold(new Boolean(false));
				}

				if (null != trigger.getOrderThreshold() && null != trigger.getOrderThreshold().getPrice()
						&& trigger.getOrderThreshold().getPrice().size() > 0)
				{
					final List<PromotionPriceRowModel> priceRowModelList = new ArrayList<PromotionPriceRowModel>();
					for (final Money price : trigger.getOrderThreshold().getPrice())
					{

						final PromotionPriceRowModel rowModel = modelService.create(PromotionPriceRowModel.class);
						CurrencyModel currencyModel = modelService.create(CurrencyModel.class);
						currencyModel.setIsocode(price.getCurrencyId());
						try
						{
							currencyModel = flexibleSearchService.getModelByExample(currencyModel);
						}
						catch (final Exception e)
						{
							e.printStackTrace();
							currencyModel.setIsocode("ZAR");
							currencyModel = flexibleSearchService.getModelByExample(currencyModel);
						}
						rowModel.setCurrency(currencyModel);
						rowModel.setPrice(new Double(price.getPrice()));

						priceRowModelList.add(rowModel);
					}
					if (priceRowModelList.size() > 0)
					{
						promotionModel.setThresholdTotals(priceRowModelList);
					}

				}

				if (null != trigger.getRewardId())
				{
					if (null != promotion.getRewards() && null != promotion.getRewards().getReward()
							&& promotion.getRewards().getReward().size() > 0)
					{
						for (final Reward reward : promotion.getRewards().getReward())
						{
							if (null != reward.getRewardId())
							{
								if (trigger.getRewardId().equalsIgnoreCase(reward.getRewardId()))
								{
									if (null != reward.getPrices() && null != reward.getPrices().getPrice()
											&& reward.getPrices().getPrice().size() > 0)
									{
										final List<PromotionPriceRowModel> priceRowModelList = new ArrayList<PromotionPriceRowModel>();
										for (final Money price : reward.getPrices().getPrice())
										{
											final PromotionPriceRowModel rowModel = modelService.create(PromotionPriceRowModel.class);
											CurrencyModel currencyModel = modelService.create(CurrencyModel.class);
											currencyModel.setIsocode(price.getCurrencyId());
											try
											{
												currencyModel = flexibleSearchService.getModelByExample(currencyModel);
											}
											catch (final Exception e)
											{
												e.printStackTrace();
												currencyModel.setIsocode("ZAR");
												currencyModel = flexibleSearchService.getModelByExample(currencyModel);
											}
											rowModel.setCurrency(currencyModel);
											rowModel.setPrice(new Double(price.getPrice()));

											priceRowModelList.add(rowModel);
										}
										if (priceRowModelList.size() > 0)
										{
											promotionModel.setProductPrices(priceRowModelList);
										}
									}
								}
							}
						}
					}
				}

				if (null != promotion.getBuckets() && null != promotion.getBuckets().getBucket()
						&& promotion.getBuckets().getBucket().size() > 0)
				{
					for (final de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Body.Promotions.Promotion.Buckets.Bucket bucket : promotion
							.getBuckets().getBucket())
					{
						if (bucket.getItemTypeId() == 1)
						{
							//final List<ProductModel> productModelList = new ArrayList<ProductModel>();
							for (final String item : bucket.getItemId())
							{
								boolean flag = false;
								ProductModel itemModel = modelService.create(ProductModel.class);
								itemModel.setCode(item);
								itemModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
								final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
										Config.getParameter("clicks.productcatalog.name"),
										Config.getParameter("clicks.catalog.online.version"));
								itemModel.setCatalogVersion(catalogVersionModel);
								try
								{
									itemModel = flexibleSearchService.getModelByExample(itemModel);
								}
								catch (final Exception e)
								{
									flag = true;
									e.printStackTrace();
								}
								if (!flag)
								{
									promotionModel.setDiscountProduct(itemModel);
								}
							}
						}

					}
				}

			}
		}

		return promotionModel;
	}

}
