/**
 *
 */
package de.hybris.clicks.webservices.promotion.impl;

import de.hybris.clicks.webservices.promotion.PromotionImportEndpoint;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage;
import de.hybris.platform.core.Registry;

import javax.annotation.Resource;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;


/**
 * @author abhaydh
 * 
 */
@Endpoint
public class DefaultPromotionImportEndpoint implements PromotionImportEndpoint
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/Promotion";
	public static final String GET_PERSONS_REQUEST = "PromotionMessage";

	@Resource
	PromotionsImportProcessor promotionsImportProcessor;

	@PayloadRoot(localPart = "PromotionMessage", namespace = "http://Clicks.BizTalk.Omni/Schema/Promotion")
	public void getPromotions(@RequestPayload final PromotionMessage promotion)
	{
		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				promotionsImportProcessor.processPromotions(promotion);
			}
		});
		thread.start();


	}

}
