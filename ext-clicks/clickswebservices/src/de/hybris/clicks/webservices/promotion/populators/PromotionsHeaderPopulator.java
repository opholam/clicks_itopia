/**
 *
 */
package de.hybris.clicks.webservices.promotion.populators;

import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.clicks.webservices.promotion.schemas.PromotionMessage.Header;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class PromotionsHeaderPopulator
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	public void populate(final Header source)
	{

		final ProdHeaderDetailsModel target = modelService.create(ProdHeaderDetailsModel.class);

		if (null != source.getDataSource())
		{
			target.setDataSource(source.getDataSource());
		}
		if (null != source.getEventCode())
		{
			target.setEventCode(source.getEventCode());
		}
		if (null != source.getEventLogId())
		{
			target.setEventLogId(source.getEventLogId());
		}
		if (null != source.getEventCode())
		{
			target.setFeedType(source.getEventCode());
		}
		if (null != source.getCreatedDateTime())
		{
			target.setCreatedDateTime(source.getCreatedDateTime().toGregorianCalendar().getTime());
		}

		modelService.save(target);

	}
}
