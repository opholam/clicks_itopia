/**
 *
 */
package de.hybris.clicks.webservices.storelocator.populators;

import de.hybris.clicks.core.model.StoreFeatureAttributeModel;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store.Services.Service;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store.Services.Service.Attributes.Attribute;
import de.hybris.clicks.webservices.storelocator.schemas.TradingHoursType.Day;
import de.hybris.platform.basecommerce.enums.WeekDay;
import de.hybris.platform.commerceservices.model.storelocator.StoreLocatorFeatureModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.model.SpecialOpeningDayModel;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author abhaydh
 *
 */
public class StoreOpeningSchedulePopulator implements Populator<Store, PointOfServiceModel>
{
	private static final String PUBLIC_HOLIDAY = "Public Holidays";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	boolean isUpdate;


	/**
	 * @return the isUpdate
	 */
	public boolean isUpdate()
	{
		return isUpdate;
	}

	/**
	 * @param isUpdate
	 *           the isUpdate to set
	 */
	public void setUpdate(final boolean isUpdate)
	{
		this.isUpdate = isUpdate;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void populate(final Store source, final PointOfServiceModel target) throws ConversionException
	{
		Date futureDate;
		final List<OpeningScheduleModel> openingScheduleList = new ArrayList<OpeningScheduleModel>();
		OpeningScheduleModel openingScheduleModel;

		Collection<OpeningDayModel> openingDayList = new ArrayList<OpeningDayModel>();

		Collection<WeekdayOpeningDayModel> weekdayList = new ArrayList<WeekdayOpeningDayModel>();
		WeekdayOpeningDayModel weekdayOpeningDayModel;
		Collection<SpecialOpeningDayModel> specialDayList = new ArrayList<SpecialOpeningDayModel>();
		SpecialOpeningDayModel specialOpeningDayModel;

		final Map<String, StoreLocatorFeatureModel> featureMap = new HashMap<String, StoreLocatorFeatureModel>();
		if (isUpdate)
		{
			if (CollectionUtils.isNotEmpty(target.getFeatures()))
			{
				modelService.removeAll(target.getFeatures());
			}
		}

		StoreLocatorFeatureModel feature = modelService.create(StoreLocatorFeatureModel.class);
		feature.setCode(target.getName() + "Str");
		feature.setName("Store");
		feature = getStoreLocatorFeature(feature);
		featureMap.put(feature.getCode(), feature);

		if (isUpdate)
		{
			openingScheduleModel = modelService.create(OpeningScheduleModel.class);
			openingScheduleModel.setCode(target.getName() + "Str");
			try
			{
				openingScheduleModel = flexibleSearchService.getModelByExample(openingScheduleModel);
			}
			catch (final Exception e)
			{
				//
			}
			if (null != openingScheduleModel.getName())
			{
				for (final Iterator<OpeningDayModel> iterator = openingScheduleModel.getOpeningDays().iterator(); iterator.hasNext();)
				{
					final OpeningDayModel openDay = iterator.next();
					modelService.remove(openDay);
				}
			}

		}
		else
		{
			openingScheduleModel = modelService.create(OpeningScheduleModel.class);
			openingScheduleModel.setCode(target.getName() + "Str");
		}
		openingScheduleModel.setName("Str");
		modelService.save(openingScheduleModel);
		if (null != source.getTradingHours() && null != source.getTradingHours().getDay())
		{
			removeExistingOpeningdays(openingScheduleModel);
			for (final Iterator<Day> iterator = source.getTradingHours().getDay().iterator(); iterator.hasNext();)
			{
				final Day day = iterator.next();
				final String weekday = day.getDayIndicator();

				switch (weekday)
				{
					case "Mon":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.MONDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;

					case "Tue":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.TUESDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;

					case "Wed":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.WEDNESDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;

					case "Thu":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.THURSDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;

					case "Fri":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.FRIDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;

					case "Sat":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.SATURDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;

					case "Sun":
						weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
						weekdayOpeningDayModel.setDayOfWeek(WeekDay.SUNDAY);
						weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						weekdayList.add(weekdayOpeningDayModel);
						break;


					case "Pub":
						specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
						futureDate = new Date();
						futureDate.setYear(futureDate.getYear() + 40);
						specialOpeningDayModel.setDate(futureDate);
						specialOpeningDayModel.setName(PUBLIC_HOLIDAY);
						specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						modelService.save(specialOpeningDayModel);
						specialDayList.add(specialOpeningDayModel);
						break;

					case "Td1":
						specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
						futureDate = new Date();
						futureDate.setYear(futureDate.getYear() + 41);
						specialOpeningDayModel.setDate(futureDate);
						specialOpeningDayModel.setName("Monday to Friday");
						specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						modelService.save(specialOpeningDayModel);
						specialDayList.add(specialOpeningDayModel);
						break;

					case "Td2":
						specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
						futureDate = new Date();
						futureDate.setYear(futureDate.getYear() + 42);
						specialOpeningDayModel.setDate(futureDate);
						specialOpeningDayModel.setName("1st & last Saturday of each month");
						specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						modelService.save(specialOpeningDayModel);
						specialDayList.add(specialOpeningDayModel);
						break;


					default:
						specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
						futureDate = new Date();
						futureDate.setYear(futureDate.getYear() + 43);
						specialOpeningDayModel.setDate(futureDate);
						specialOpeningDayModel.setName(weekday);
						specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
						if (null != day.getOpenTime())
						{
							specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
						}
						if (null != day.getCloseTime())
						{
							specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
						}
						modelService.save(specialOpeningDayModel);
						specialDayList.add(specialOpeningDayModel);
						break;

				}


			} // for loop ends
		}

		openingDayList.addAll(weekdayList);
		openingDayList.addAll(specialDayList);
		openingScheduleModel.setOpeningDays(openingDayList);
		modelService.save(openingScheduleModel);
		openingScheduleList.add(openingScheduleModel);
		boolean isClickAndCollectEnabled = false;
		if (null != source.getServices() && source.getServices().getService().size() > 0)
		{
			for (final Service service : source.getServices().getService())
			{
				if (service.getType().equalsIgnoreCase("OC"))
				{
					isClickAndCollectEnabled = true;
					continue;
				}

				feature = modelService.create(StoreLocatorFeatureModel.class);
				feature.setCode(target.getName() + service.getType());
				if (service.getType().equalsIgnoreCase("Ph"))
				{
					feature.setName("Pharmacy");
				}
				if (service.getType().equalsIgnoreCase("Cl"))
				{
					feature.setName("Clinic");
				}
				feature = getStoreLocatorFeature(feature);
				if (null != service.getOpeningDate())
				{
					final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					final String dateInString = service.getOpeningDate();
					Date date = null;
					try
					{
						date = formatter.parse(dateInString);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
					feature.setOpeningDate(date);
				}
				final List<StoreFeatureAttributeModel> attributeList = new ArrayList<StoreFeatureAttributeModel>();

				if (null != service.getAttributes() && null != service.getAttributes().getAttribute()
						&& service.getAttributes().getAttribute().size() > 0)
				{

					for (final Attribute attribute : service.getAttributes().getAttribute())
					{
						final StoreFeatureAttributeModel storeFeatureAttributeModel = modelService
								.create(StoreFeatureAttributeModel.class);
						storeFeatureAttributeModel.setId(attribute.getId().toString());
						storeFeatureAttributeModel.setValue(attribute.getValue());
						attributeList.add(storeFeatureAttributeModel);
					}

				}
				if (attributeList.size() > 0)
				{
					feature.setAttributeList(attributeList);
				}
				featureMap.put(feature.getCode(), feature);

				openingScheduleModel = modelService.create(OpeningScheduleModel.class);

				openingDayList = new ArrayList<OpeningDayModel>();
				weekdayList = new ArrayList<WeekdayOpeningDayModel>();
				specialDayList = new ArrayList<SpecialOpeningDayModel>();

				if (isUpdate)
				{
					openingScheduleModel = modelService.create(OpeningScheduleModel.class);
					openingScheduleModel.setCode(target.getName() + service.getType());
					try
					{
						openingScheduleModel = flexibleSearchService.getModelByExample(openingScheduleModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
					if (null != openingScheduleModel.getName())
					{
						for (final Iterator<OpeningDayModel> iterator = openingScheduleModel.getOpeningDays().iterator(); iterator
								.hasNext();)
						{
							final OpeningDayModel openDay = iterator.next();
							modelService.remove(openDay);
						}
					}
				}
				else
				{
					openingScheduleModel.setCode(target.getName() + service.getType());
				}

				openingScheduleModel.setName(service.getType());

				if (null != service.getTradingHours() && null != service.getTradingHours().getDay()
						&& service.getTradingHours().getDay().size() > 0)
				{
					removeExistingOpeningdays(openingScheduleModel);
					for (final Iterator<Day> iterator = service.getTradingHours().getDay().iterator(); iterator.hasNext();)
					{
						final Day day = iterator.next();
						final String weekday = day.getDayIndicator();

						switch (weekday)
						{

							case "Mon":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.MONDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Tue":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.TUESDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Wed":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.WEDNESDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Thu":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.THURSDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Fri":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.FRIDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Sat":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.SATURDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Sun":
								weekdayOpeningDayModel = modelService.create(WeekdayOpeningDayModel.class);
								weekdayOpeningDayModel.setDayOfWeek(WeekDay.SUNDAY);
								weekdayOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									weekdayOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									weekdayOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								weekdayList.add(weekdayOpeningDayModel);
								break;

							case "Pub":
								specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
								futureDate = new Date();
								futureDate.setYear(futureDate.getYear() + 44);
								specialOpeningDayModel.setDate(futureDate);
								specialOpeningDayModel.setName(PUBLIC_HOLIDAY);
								specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								modelService.save(specialOpeningDayModel);
								specialDayList.add(specialOpeningDayModel);
								break;

							case "Td1":
								specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
								futureDate = new Date();
								futureDate.setYear(futureDate.getYear() + 45);
								specialOpeningDayModel.setDate(futureDate);
								specialOpeningDayModel.setName("Monday to Friday");
								specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								modelService.save(specialOpeningDayModel);
								specialDayList.add(specialOpeningDayModel);
								break;

							case "Td2":
								specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
								futureDate = new Date();
								futureDate.setYear(futureDate.getYear() + 46);
								specialOpeningDayModel.setDate(futureDate);
								specialOpeningDayModel.setName("1st & last Saturday of each month");
								specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								if (null != day.getOpenTime())
								{
									specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								modelService.save(specialOpeningDayModel);
								specialDayList.add(specialOpeningDayModel);
								break;

							default:
								specialOpeningDayModel = modelService.create(SpecialOpeningDayModel.class);
								futureDate = new Date();
								futureDate.setYear(futureDate.getYear() + 47);
								specialOpeningDayModel.setDate(futureDate);
								specialOpeningDayModel.setOpeningSchedule(openingScheduleModel);
								specialOpeningDayModel.setName(weekday);
								if (null != day.getOpenTime())
								{
									specialOpeningDayModel.setOpeningTime(day.getOpenTime().toGregorianCalendar().getTime());
								}
								if (null != day.getCloseTime())
								{
									specialOpeningDayModel.setClosingTime(day.getCloseTime().toGregorianCalendar().getTime());
								}
								modelService.save(specialOpeningDayModel);
								specialDayList.add(specialOpeningDayModel);
								break;

						}

					}
				}

				openingDayList.addAll(weekdayList);
				openingDayList.addAll(specialDayList);
				openingScheduleModel.setOpeningDays(openingDayList);
				modelService.save(openingScheduleModel);
				openingScheduleList.add(openingScheduleModel);
			}
		}
		if (isClickAndCollectEnabled)
		{
			StoreLocatorFeatureModel CCfeature = modelService.create(StoreLocatorFeatureModel.class);
			CCfeature.setCode(target.getName() + "OC");
			CCfeature.setName("Click & Collect");
			CCfeature = getStoreLocatorFeature(CCfeature);
			featureMap.put(CCfeature.getCode(), CCfeature);
		}
		target.setFeatures(new HashSet<StoreLocatorFeatureModel>(featureMap.values()));
		target.setOpeningScheduleList(openingScheduleList);

	}

	/**
	 * @param feature
	 */
	private StoreLocatorFeatureModel getStoreLocatorFeature(StoreLocatorFeatureModel feature)
	{
		try
		{
			final List<StoreLocatorFeatureModel> list = flexibleSearchService.getModelsByExample(feature);
			if (CollectionUtils.isNotEmpty(list))
			{
				feature = list.get(0);
			}
		}
		catch (final Exception e)
		{
			//
		}
		return feature;
	}

	/**
	 * @param openingScheduleModel
	 */
	private void removeExistingOpeningdays(final OpeningScheduleModel openingScheduleModel)
	{
		try
		{
			final List<WeekdayOpeningDayModel> wkList = new ArrayList<WeekdayOpeningDayModel>();
			final List<SpecialOpeningDayModel> spList = new ArrayList<SpecialOpeningDayModel>();
			final WeekdayOpeningDayModel weekdayModel = new WeekdayOpeningDayModel();
			weekdayModel.setOpeningSchedule(openingScheduleModel);
			wkList.addAll(flexibleSearchService.getModelsByExample(weekdayModel));
			modelService.removeAll(wkList);
			final SpecialOpeningDayModel specialModel = new SpecialOpeningDayModel();
			specialModel.setOpeningSchedule(openingScheduleModel);
			spList.addAll(flexibleSearchService.getModelsByExample(specialModel));
			modelService.removeAll(spList);
		}
		catch (final Exception e)
		{
			//
		}
	}

}