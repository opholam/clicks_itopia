/**
 *
 */
package de.hybris.clicks.webservices.storelocator.impl;


import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.clicks.webservices.storelocator.StoreLocationImport;
import de.hybris.clicks.webservices.storelocator.populators.StoreContactNumbersPopulator;
import de.hybris.clicks.webservices.storelocator.populators.StoreLocationAddressPopulator;
import de.hybris.clicks.webservices.storelocator.populators.StoreOpeningSchedulePopulator;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commerceservices.model.storelocator.StoreLocatorFeatureModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author abhaydh
 *
 */
public class DefaultStoreLocationImport implements StoreLocationImport
{


	@Resource
	private ModelService modelService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private StoreLocationAddressPopulator storeLocationAddressPopulator;

	@Resource
	private StoreOpeningSchedulePopulator storeOpeningSchedulePopulator;

	@Resource
	private StoreContactNumbersPopulator storeContactNumbersPopulator;

	@Override
	public boolean importStoreLocationData(final StoreLocation storeData)
	{
		final List<PointOfServiceModel> posModelList = new ArrayList<PointOfServiceModel>();
		PointOfServiceModel posModel;
		AddressModel addressModel;
		boolean isUpdate = false;
		Double latitude = 0.0d;
		Double longitude = 0.0d;

		// start - storing header data
		final ProdHeaderDetailsModel headerModel = modelService.create(ProdHeaderDetailsModel.class);
		populateHeader(storeData, headerModel);
		// end - storing header data

		for (final Store store : storeData.getBody().getStores().getStore())
		{
			isUpdate = false;
			if (store.getAction().equalsIgnoreCase("D"))
			{
				posModel = new PointOfServiceModel();
				posModel.setName(store.getId());
				try
				{
					posModel = flexibleSearchService.getModelByExample(posModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}

				if (null != posModel.getFeatures())
				{
					for (final StoreLocatorFeatureModel feature : posModel.getFeatures())
					{
						modelService.remove(feature);
					}
				}
				if (null != posModel.getOpeningScheduleList())
				{
					for (final OpeningScheduleModel openingScheduleRemove : posModel.getOpeningScheduleList())
					{
						for (final Iterator<OpeningDayModel> iterator = openingScheduleRemove.getOpeningDays().iterator(); iterator
								.hasNext();)
						{
							final OpeningDayModel openDay = iterator.next();
							modelService.remove(openDay);
						}
						modelService.remove(openingScheduleRemove);
					}
				}

				if (null != posModel.getDisplayName())
				{
					modelService.remove(posModel);
				}
			}
			else
			{
				posModel = modelService.create(PointOfServiceModel.class);
				posModel.setName(store.getId());
				try
				{
					posModel = flexibleSearchService.getModelByExample(posModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				if (null != posModel.getDisplayName())
				{
					isUpdate = true;
				}
				else
				{
					posModel.setName(store.getId());
				}

				addressModel = modelService.create(AddressModel.class);
				if (null != store.getCoords())
				{
					if (StringUtils.isNotEmpty(store.getCoords().getLatitude()))
					{
						latitude = Double.valueOf(store.getCoords().getLatitude());
					}
					if (StringUtils.isNotEmpty(store.getCoords().getLongitude()))
					{
						longitude = Double.valueOf(store.getCoords().getLongitude());
					}
				}
				posModel.setType(PointOfServiceTypeEnum.STORE);
				posModel.setDisplayName(store.getName());
				posModel.setLatitude(latitude);
				posModel.setLongitude(longitude);
				posModel = this.setDefaultStoreData(posModel);

				modelService.save(posModel);
				if (!isUpdate)
				{
					modelService.attach(addressModel);
				}

				if (null != store.getOpeningDate())
				{
					final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					final String dateInString = store.getOpeningDate();
					Date date = null;
					try
					{

						date = formatter.parse(dateInString);

					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
					posModel.setOpenDate(date);
				}

				if (null != store.getAddress())
				{
					storeLocationAddressPopulator.populate(store.getAddress(), addressModel);
					addressModel.setOwner(posModel);
					posModel.setAddress(addressModel);
				}



				storeOpeningSchedulePopulator.setUpdate(isUpdate);
				if (null != store.getTradingHours() || null != store.getServices())
				{
					storeOpeningSchedulePopulator.populate(store, posModel);
				}
				if (null != store.getContactNumbers() || null != store.getServices())
				{
					storeContactNumbersPopulator.populate(store, posModel);
				}


				posModelList.add(posModel);
				modelService.save(posModel);
			}



		}

		return false;
	}

	public PointOfServiceModel setDefaultStoreData(final PointOfServiceModel posModel)
	{

		BaseStoreModel basestore = new BaseStoreModel();

		basestore.setUid("clicks");
		basestore = flexibleSearchService.getModelByExample(basestore);
		posModel.setBaseStore(basestore);

		return posModel;

	}

	private void populateHeader(final StoreLocation storeData, final ProdHeaderDetailsModel target)
	{
		if (null != storeData.getHeader().getDataSource())
		{
			target.setDataSource(storeData.getHeader().getDataSource());
		}
		if (null != storeData.getHeader().getEventCode())
		{
			target.setEventCode(storeData.getHeader().getEventCode());
		}
		if (null != storeData.getHeader().getEventLogId())
		{
			target.setEventLogId(storeData.getHeader().getEventLogId());
		}
		if (null != storeData.getHeader().getEventCode())
		{
			target.setFeedType(storeData.getHeader().getEventCode());
		}
		if (null != storeData.getHeader().getCreatedDateTime())
		{
			target.setCreatedDateTime(storeData.getHeader().getCreatedDateTime().toGregorianCalendar().getTime());
		}
		modelService.save(target);
	}

}
