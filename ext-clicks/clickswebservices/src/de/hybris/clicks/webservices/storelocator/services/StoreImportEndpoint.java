package de.hybris.clicks.webservices.storelocator.services;

import de.hybris.clicks.webservices.storelocator.StoreLocationImport;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation;
import de.hybris.platform.core.Registry;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;



@Endpoint
public class StoreImportEndpoint
{
	public final static String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/StoreLocation";
	public final static String GET_PERSONS_REQUEST = "StoreLocation";

	@Resource
	StoreLocationImport storeLocationImport;

	@Resource
	WebServiceContext webServiceContext;

	/**
	 * 
	 * @param storeData
	 * @return
	 */
	@PayloadRoot(localPart = GET_PERSONS_REQUEST, namespace = NAMESPACE)
	public @ResponsePayload
	void getProducts(@RequestPayload final StoreLocation storeData)
	{
		final Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Registry.activateMasterTenant();
				storeLocationImport.importStoreLocationData(storeData);
			}
		});
		thread.start();
	}

}
