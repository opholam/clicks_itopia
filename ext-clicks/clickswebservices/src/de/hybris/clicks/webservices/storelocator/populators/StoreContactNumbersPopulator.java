/**
 *
 */
package de.hybris.clicks.webservices.storelocator.populators;

import de.hybris.clicks.core.model.PhoneListModel;
import de.hybris.clicks.webservices.storelocator.schemas.ContactNumberType.ContactNumber;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store;
import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store.Services.Service;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class StoreContactNumbersPopulator implements Populator<Store, PointOfServiceModel>
{

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Override
	public void populate(final Store source, final PointOfServiceModel target) throws ConversionException
	{

		final List<PhoneListModel> phoneNumbersList = new ArrayList<PhoneListModel>();

		PhoneListModel phoneNumberListModelTel = modelService.create(PhoneListModel.class);
		PhoneListModel phoneNumberListModelFax = modelService.create(PhoneListModel.class);

		List<String> phoneListTel = new ArrayList<String>();
		List<String> phoneListFax = new ArrayList<String>();

		phoneNumberListModelTel.setCode(target.getName() + "Str" + "Tel");
		phoneNumberListModelTel.setName("Str" + "Tel");
		phoneNumberListModelFax.setCode(target.getName() + "Str" + "Fax");
		phoneNumberListModelFax.setName("Str" + "Fax");

		if (null != source.getContactNumbers() && source.getContactNumbers().getContactNumber().size() > 0)
		{
			for (final ContactNumber contactNumber : source.getContactNumbers().getContactNumber())
			{
				if (contactNumber.getType().equals("Tel"))
				{
					phoneListTel.add(contactNumber.getNumber());
				}
				else
				{
					phoneListFax.add(contactNumber.getNumber());
				}

			}
		}

		if (phoneListTel.size() > 0)
		{
			phoneNumberListModelTel.setPhoneList(phoneListTel);
			phoneNumbersList.add(phoneNumberListModelTel);
		}
		if (phoneListFax.size() > 0)
		{
			phoneNumberListModelFax.setPhoneList(phoneListFax);
			phoneNumbersList.add(phoneNumberListModelFax);
		}

		if (null != source.getServices() && source.getServices().getService().size() > 0)
		{
			for (final Service service : source.getServices().getService())
			{

				phoneNumberListModelTel = modelService.create(PhoneListModel.class);
				phoneNumberListModelFax = modelService.create(PhoneListModel.class);
				phoneListTel = new ArrayList<String>();
				phoneListFax = new ArrayList<String>();

				phoneNumberListModelTel.setCode(target.getName() + service.getType() + "Tel");
				phoneNumberListModelTel.setName(service.getType() + "Tel");
				phoneNumberListModelFax.setCode(target.getName() + service.getType() + "Fax");
				phoneNumberListModelFax.setName(service.getType() + "Fax");


				if (null != service.getContactNumbers() && service.getContactNumbers().getContactNumber().size() > 0)
				{
					for (final ContactNumber contactNumber : service.getContactNumbers().getContactNumber())
					{
						if (contactNumber.getType().equals("Tel"))
						{
							phoneListTel.add(contactNumber.getNumber());
						}
						else
						{
							phoneListFax.add(contactNumber.getNumber());
						}
					}

				}

				if (phoneListTel.size() > 0)
				{
					phoneNumberListModelTel.setPhoneList(phoneListTel);
					phoneNumbersList.add(phoneNumberListModelTel);
				}
				if (phoneListFax.size() > 0)
				{
					phoneNumberListModelFax.setPhoneList(phoneListFax);
					phoneNumbersList.add(phoneNumberListModelFax);
				}


			}
		}

		target.setPhoneLists(phoneNumbersList);
	}
}