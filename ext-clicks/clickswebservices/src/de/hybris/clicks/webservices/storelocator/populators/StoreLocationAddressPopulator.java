/**
 *
 */
package de.hybris.clicks.webservices.storelocator.populators;

import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation.Body.Stores.Store.Address;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class StoreLocationAddressPopulator implements Populator<Address, AddressModel>
{

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private FlexibleSearchService flexibleSearchService;


	@Resource
	private ModelService modelService;


	@Override
	public void populate(final Address source, final AddressModel target) throws ConversionException
	{


		if (null != source.getAddress2())
		{
			target.setBuilding(source.getAddress2());
		}

		if (null != source.getCentreName())
		{
			target.setAppartment(source.getCentreName());
		}

		if (null != source.getShopNumber())
		{
			target.setDepartment(source.getShopNumber());
		}
		if (null != source.getAddress1())
		{
			target.setStreetname(source.getAddress1());
		}
		if (null != source.getSuburb())
		{
			target.setStreetnumber(source.getSuburb());
		}
		if (null != source.getPostalCode())
		{
			target.setPostalcode(source.getPostalCode());
		}

		if (null != source.getCity() && null != source.getProvince())
		{
			target.setTown(source.getCity() + ", " + source.getProvince());
		}
		else if (null != source.getCity())
		{
			target.setTown(source.getCity());
		}
		else
		{
			target.setTown(source.getProvince());
		}


		if (null != source.getCountry())
		{
			//final CountryModel country = commonI18NService.getCountry(source.getCountry());
			CountryModel country = new CountryModel();
			modelService.attach(country);
			country.setName(source.getCountry());
			country = flexibleSearchService.getModelByExample(country);
			if (null != country)
			{
				target.setCountry(country);
			}
		}

	}
}
