package de.hybris.clicks.webservices.storelocator.unmarshall;





import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;


public class UnmarshallStoreLocation
{

	public StoreLocation doUnmarshallStores() throws JAXBException, FileNotFoundException
	{



		final JAXBContext jaxbContext = JAXBContext.newInstance(StoreLocation.class);
		final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();


		final File f = new File(
				"D:/clicks2/hybris/bin/ext-clicks/ext-clicks/clickswebservices/resources/xsd/storelocator/Stores_data.xml");


		if (!f.exists())
		{
			throw new FileNotFoundException();
		}

		final StoreLocation data = (StoreLocation) jaxbUnmarshaller.unmarshal(f);

		return data;
	}
}
