/**
 *
 */
package de.hybris.clicks.webservices.storelocator;

import de.hybris.clicks.webservices.storelocator.schemas.StoreLocation;





/**
 * @author abhaydh
 *
 */
public interface StoreLocationImport
{

	boolean importStoreLocationData(StoreLocation storeData);
}
