package de.hybris.clicks.webservices.ordertracking.services;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.clicks.webservices.orderclosed.schemas.OrderClosed;
import de.hybris.clicks.webservices.orderclosed.schemas.OrderClosed.Body.Items.Item;
import de.hybris.clicks.webservices.ordertracking.schemas.OrderTracking;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class OrderTrackingEndpoint
{
	public final static String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/OrderTracking";
	public final static String GET_PERSONS_REQUEST = "OrderTracking";

	@Resource
	CustomerAccountService customerAccountService;

	@Resource
	ModelService modelService;

	@Resource
	BaseStoreService baseStoreService;

	@Resource
	WarehouseService warehouseService;

	@Resource
	BusinessProcessService businessProcessService;

	@Resource
	private Map<String, String> parcelStatusMap;

	@Resource
	private Map<String, String> storeDeliveryParcelStatusMap;

	private static final Logger LOG = Logger.getLogger(OrderTrackingEndpoint.class);

	/**
	 *
	 * @param order
	 */
	@PayloadRoot(localPart = GET_PERSONS_REQUEST, namespace = NAMESPACE)
	public @ResponsePayload void updateOrderTrackingInfo(@RequestPayload final OrderTracking order)
	{
		try
		{
			OrderModel orderModel = null;
			try
			{
				orderModel = customerAccountService.getOrderForCode(order.getBody().getOrderNumber(),
						baseStoreService.getBaseStoreForUid("clicks"));
			}
			catch (final Exception e)
			{
				// suppress
			}
			validateParameterNotNull(orderModel, "Order not found for order number " + order.getBody().getOrderNumber());
			if (OrderStatus.COMPLETED.equals(orderModel.getStatus()))
			{
				LOG.error("OrderTracking message ignored. Reason Order status is completed for " + order.getBody().getOrderNumber());
				return;
			}
			final boolean isStoreDelivery = null != orderModel.getDeliveryAddress().getIsStore() ? orderModel.getDeliveryAddress()
					.getIsStore().booleanValue() : false;
			boolean triggerDeliveryEmail = false;
			for (final ConsignmentModel consignmentModel : orderModel.getConsignments())
			{

				if (consignmentModel.getCode().equalsIgnoreCase(order.getBody().getContainer()))
				{
					consignmentModel.setTrackingID(order.getBody().getParcelId());
					final int parcelStatusId = order.getBody().getStatusId();
					final ConsignmentStatus consignmentStatus = getParcelStatus(parcelStatusId, isStoreDelivery);
					if (ConsignmentStatus.CANCELLED.equals(consignmentModel.getStatus()))
					{
						consignmentModel.setStatus(ConsignmentStatus.CANCELLED);
					}
					else
					{
						//consignmentModel.setStatus(ConsignmentStatus.valueOf(String.valueOf(order.getBody().getStatusId())));
						if (null != consignmentStatus)
						{
							if (!ConsignmentStatus.SHIPPED.equals(consignmentModel.getStatus())
									&& ConsignmentStatus.SHIPPED.equals(consignmentStatus))
							{
								triggerDeliveryEmail = true;
							}
							consignmentModel.setStatus(consignmentStatus);
						}
						consignmentModel.setShippingDate(order.getBody().getStatusDate().toGregorianCalendar().getTime());

						for (final ConsignmentEntryModel entry : consignmentModel.getConsignmentEntries())
						{
							entry.setShippedQuantity(entry.getQuantity());
						}
					}
					modelService.saveAll();
					if (triggerDeliveryEmail)
					{
						try
						{
							final OrderProcessModel orderProcessModel = businessProcessService.createProcess("sendDeliveryEmailProcess-"
									+ orderModel.getClicksOrderCode() + "-" + System.currentTimeMillis(), "sendDeliveryEmailProcess");
							orderProcessModel.setOrder(orderModel);
							modelService.save(orderProcessModel);
							businessProcessService.startProcess(orderProcessModel);
						}
						catch (final Exception e)
						{
							LOG.error("Error while sending delivery shipped email for " + order.getBody().getOrderNumber());
						}
					}
					for (final OrderProcessModel processModel : orderModel.getOrderProcess())
					{
						businessProcessService.triggerEvent("ConsignmentShippedEvent_" + processModel.getCode());
					}
					return;
				}


			}
			validateParameterNotNull(null, "Container " + order.getBody().getContainer() + " not found for order number "
					+ order.getBody().getOrderNumber());
		}
		catch (final Exception e)
		{
			LOG.error("++++++++++Order Tracking++++++++++++  ", e);
		}
	}

	private ConsignmentStatus getParcelStatus(final int statusId, final boolean isStoreDelivery)
	{
		final Map<String, String> stautsMap = isStoreDelivery ? storeDeliveryParcelStatusMap : parcelStatusMap;
		try
		{
			for (final Entry<String, String> entry : stautsMap.entrySet())
			{
				if (StringUtils.isNotEmpty(entry.getValue())
						&& Arrays.asList(StringUtils.split(entry.getValue(), ",")).contains(String.valueOf(statusId)))
				{
					return ConsignmentStatus.valueOf(entry.getKey());
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("++++++++++Order Tracking++++++++++++error while setting parcel status for status id " + statusId
					+ e.getMessage());
		}
		LOG.error("++++++++++Order Tracking++++++++++++No parcel status mapping found for status id " + statusId);
		return null;
	}

	/**
	 *
	 * @param order
	 */
	@PayloadRoot(localPart = "OrderClosed", namespace = "http://Clicks.BizTalk.Omni/Schema/OrderClosed")
	public @ResponsePayload void updateOrderClosedInfo(@RequestPayload final OrderClosed order)
	{
		try
		{
			final OrderModel orderModel = customerAccountService.getOrderForCode(order.getBody().getOrderNumber(),
					baseStoreService.getBaseStoreForUid("clicks"));
			validateParameterNotNull(orderModel, "Order not found for order number " + order.getBody().getOrderNumber());
			if (OrderStatus.COMPLETED.equals(orderModel.getStatus()))
			{
				LOG.error("OrderClosed message ignored. Reason Order status is completed for " + order.getBody().getOrderNumber());
				return;
			}
			final Map<String, ConsignmentModel> consignments = new HashMap<String, ConsignmentModel>();
			final Map<String, AbstractOrderEntryModel> orderEntries = new HashMap<String, AbstractOrderEntryModel>();

			String containerKey;
			if (orderModel.getConsignments().isEmpty())
			{
				orderModel.setConsignments(new HashSet<ConsignmentModel>());
			}
			else
			{
				modelService.removeAll(orderModel.getConsignments());
				orderModel.setConsignments(new HashSet<ConsignmentModel>());
			}

			for (final ConsignmentModel consignmentModel : orderModel.getConsignments())
			{
				consignments.put(consignmentModel.getCode(), consignmentModel);
				consignmentModel.setConsignmentEntries(new HashSet<ConsignmentEntryModel>(consignmentModel.getConsignmentEntries()));

			}
			ConsignmentModel consignment;
			for (final AbstractOrderEntryModel orderEntryModel : orderModel.getEntries())
			{
				orderEntries.put(orderEntryModel.getProduct().getCode(), orderEntryModel);
			}

			for (final Item item : order.getBody().getItems().getItem())
			{
				containerKey = item.getContainer();
				if (consignments.containsKey(containerKey))
				{
					consignment = consignments.get(containerKey);
				}
				else
				{
					consignment = modelService.create(ConsignmentModel.class);
					consignment.setCode(containerKey);
					consignment.setOrder(orderModel);
					consignment.setStatus(ConsignmentStatus.READY);
					consignment
							.setWarehouse(warehouseService.getWarehouseForCode(Config.getParameter("clickswebclient.order.storeid")));
					consignment.setDeliveryMode(orderModel.getDeliveryMode());
					consignment.setShippingAddress(orderModel.getDeliveryAddress());
					consignment.setConsignmentEntries(new HashSet<ConsignmentEntryModel>());
				}
				final ConsignmentEntryModel consignmentEntryModel = modelService.create(ConsignmentEntryModel.class);
				consignmentEntryModel.setOrderEntry(orderEntries.get(item.getSKU()));
				consignmentEntryModel.setConsignment(consignment);
				if ("Y".equalsIgnoreCase(order.getBody().getCompleteFulfilment()))
				{
					consignmentEntryModel.setQuantity(consignmentEntryModel.getOrderEntry().getQuantity());
				}
				else if (item.getQuantityFulfilled() > 0
						&& item.getQuantityFulfilled() <= consignmentEntryModel.getOrderEntry().getQuantity())
				{
					consignmentEntryModel.setQuantity(Long.valueOf(item.getQuantityFulfilled()));
				}
				else
				{
					validateParameterNotNull(null, "Container " + containerKey + " has unsupported quantityfulfiled for product sku "
							+ item.getSKU() + " Order:" + order.getBody().getOrderNumber());
				}
				consignment.getConsignmentEntries().add(consignmentEntryModel);
				consignments.put(containerKey, consignment);
				orderModel.setStatus(OrderStatus.ORDER_SPLIT);
			}
			modelService.saveAll();
		}
		catch (final Exception e)
		{
			LOG.error("++++++++++Order Closing++++++++++++  ", e);
		}
	}

	/**
	 * @return the parcelStatusMap
	 */
	public Map<String, String> getParcelStatusMap()
	{
		return parcelStatusMap;
	}

	/**
	 * @param parcelStatusMap
	 *           the parcelStatusMap to set
	 */
	public void setParcelStatusMap(final Map<String, String> parcelStatusMap)
	{
		this.parcelStatusMap = parcelStatusMap;
	}

	/**
	 * @return the storeDeliveryParcelStatusMap
	 */
	public Map<String, String> getStoreDeliveryParcelStatusMap()
	{
		return storeDeliveryParcelStatusMap;
	}

	/**
	 * @param storeDeliveryParcelStatusMap
	 *           the storeDeliveryParcelStatusMap to set
	 */
	public void setStoreDeliveryParcelStatusMap(final Map<String, String> storeDeliveryParcelStatusMap)
	{
		this.storeDeliveryParcelStatusMap = storeDeliveryParcelStatusMap;
	}

}
