/**
 *
 */
package de.hybris.clicks.webservices.address.populators;

import de.hybris.clicks.core.model.AddresslocationModel;
import de.hybris.clicks.core.model.DeliveryCostModel;
import de.hybris.clicks.core.model.PostalCodeModel;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.webservices.address.schemas.AddressLocation.Body;
import de.hybris.clicks.webservices.address.schemas.AddressLocation.Body.Legend.Provinces.Province;
import de.hybris.clicks.webservices.address.schemas.AddressLocation.Body.Suburbs.Item;
import de.hybris.clicks.webservices.address.schemas.AddressLocation.Body.Suburbs.Item.DeliveryCosts.Cost;
import de.hybris.clicks.webservices.address.schemas.AddressLocation.Body.Suburbs.Item.PostalCode.Code;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class AddressPublishPopulator
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;


	public void populate(final Body addressBody)
	{
		if (null != addressBody.getLegend() && null != addressBody.getLegend().getProvinces()
				&& null != addressBody.getLegend().getProvinces().getProvince()
				&& addressBody.getLegend().getProvinces().getProvince().size() > 0)
		{
			for (final Province province : addressBody.getLegend().getProvinces().getProvince())
			{

				ProvinceModel provinceModel = modelService.create(ProvinceModel.class);
				if (null != province.getCode())
				{
					provinceModel.setCode(province.getCode());
				}
				try
				{
					provinceModel = flexibleSearchService.getModelByExample(provinceModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				if (province.getAction().equalsIgnoreCase("D"))
				{
					modelService.remove(provinceModel);
				}
				else
				{
					if (null != province.getName())
					{
						provinceModel.setName(province.getName());
					}
					modelService.save(provinceModel);
				}
			}
		}

		if (null != addressBody.getSuburbs() && null != addressBody.getSuburbs().getItem()
				&& addressBody.getSuburbs().getItem().size() > 0)
		{
			for (final Item item : addressBody.getSuburbs().getItem())
			{

				AddresslocationModel addressModel = modelService.create(AddresslocationModel.class);
				if (null != item.getId().toString())
				{
					addressModel.setCode(item.getId().toString());
				}
				try
				{
					addressModel = flexibleSearchService.getModelByExample(addressModel);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}

				if (item.getAction().equalsIgnoreCase("I"))
				{
					addressModel = populateItem(item, addressModel);
				}
				if (item.getAction().equalsIgnoreCase("D"))
				{
					try
					{
						modelService.remove(addressModel);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}

	}

	AddresslocationModel populateItem(final Item item, final AddresslocationModel addressModel)
	{
		if (null != item.getId().toString())
		{
			addressModel.setCode(item.getId().toString());
		}
		if (null != item.getSuburb())
		{
			addressModel.setSuburb(item.getSuburb());
		}
		if (null != item.getArea())
		{
			addressModel.setArea(item.getArea());
		}
		if (null != item.getProvince())
		{
			ProvinceModel provinceModel = modelService.create(ProvinceModel.class);
			provinceModel.setCode(item.getProvince());
			try
			{
				provinceModel = flexibleSearchService.getModelByExample(provinceModel);
				addressModel.setProvince(provinceModel);
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
		}

		if (null != item.getPostalCode() && null != item.getPostalCode().getCode() && item.getPostalCode().getCode().size() > 0)
		{
			final List<PostalCodeModel> codeModelList = new ArrayList<PostalCodeModel>();
			for (final Code postalCode : item.getPostalCode().getCode())
			{
				final PostalCodeModel codeModel = modelService.create(PostalCodeModel.class);

				if (null != postalCode.getType())
				{
					codeModel.setType(postalCode.getType());

					if (postalCode.getType().intValue() == 1)
					{
						codeModel.setCode("PO Box delivery");
					}
					if (postalCode.getType().intValue() == 2)
					{
						codeModel.setCode("Street Delivery");
					}
				}
				if (null != postalCode.getValue())
				{
					codeModel.setValue(postalCode.getValue());
				}
				codeModelList.add(codeModel);
			}
			addressModel.setPostalCodeList(codeModelList);
		}
		if (null != item.getDeliveryCosts() && null != item.getDeliveryCosts().getCost()
				&& item.getDeliveryCosts().getCost().size() > 0)
		{
			final List<DeliveryCostModel> costModelList = new ArrayList<DeliveryCostModel>();
			for (final Cost deliveryCost : item.getDeliveryCosts().getCost())
			{
				final DeliveryCostModel deliveryCostModel = modelService.create(DeliveryCostModel.class);
				if (null != deliveryCost.getType())
				{
					deliveryCostModel.setType(deliveryCost.getType());
				}
				if (null != deliveryCost.getValue())
				{
					deliveryCostModel.setValue(new Double(deliveryCost.getValue().doubleValue()));
				}
				costModelList.add(deliveryCostModel);
			}
			addressModel.setDeliveryCostList(costModelList);
		}
		modelService.save(addressModel);
		return addressModel;
	}
}
