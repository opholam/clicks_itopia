/**
 *
 */
package de.hybris.clicks.webservices.address.populators;

import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.clicks.webservices.address.schemas.AddressLocation.Header;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class AddressPublishHeaderPopulator
{

	@Resource
	private ModelService modelService;

	public void populate(final Header header)
	{

		final ProdHeaderDetailsModel target = modelService.create(ProdHeaderDetailsModel.class);

		if (null != header.getDataSource())
		{
			target.setDataSource(header.getDataSource());
		}
		if (null != header.getEventCode())
		{
			target.setEventCode(header.getEventCode());
		}
		if (null != header.getEventLogId())
		{
			target.setEventLogId(header.getEventLogId());
		}
		if (null != header.getEventCode())
		{
			target.setFeedType(header.getEventCode());
		}
		if (null != header.getCreatedDateTime())
		{
			target.setCreatedDateTime(header.getCreatedDateTime().toGregorianCalendar().getTime());
		}

		modelService.save(target);

	}
}
