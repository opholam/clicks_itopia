/**
 *
 */
package de.hybris.clicks.webservices.address.impl;

import de.hybris.clicks.webservices.address.AddressImportEndpoint;
import de.hybris.clicks.webservices.address.populators.AddressPublishHeaderPopulator;
import de.hybris.clicks.webservices.address.populators.AddressPublishPopulator;
import de.hybris.clicks.webservices.address.schemas.AddressLocation;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


/**
 * @author abhaydh
 *
 */
@Endpoint
public class DefaultAddressImportEndpoint implements AddressImportEndpoint
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/AddressLocation";
	public static final String GET_PERSONS_REQUEST = "AddressLocation";


	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	AddressPublishHeaderPopulator addressPublishHeaderPopulator;
	@Resource
	AddressPublishPopulator addressPublishPopulator;

	@PayloadRoot(localPart = "AddressLocation", namespace = "http://Clicks.BizTalk.Omni/Schema/AddressLocation")
	@ResponsePayload
	public void getCustomer(@RequestPayload final AddressLocation addressLocation)
	{

		if (null != addressLocation.getHeader())
		{
			addressPublishHeaderPopulator.populate(addressLocation.getHeader());
		}
		if (null != addressLocation.getBody())
		{
			addressPublishPopulator.populate(addressLocation.getBody());
		}
	}


}
