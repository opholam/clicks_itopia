package de.hybris.clicks.webservices.testOnline.testService;

import de.hybris.clicks.webclient.customer.client.CustomerDetailsRequestClient;
import de.hybris.clicks.webclient.customer.client.CustomerEnrollmentClient;
import de.hybris.clicks.webclient.customer.client.CustomerRegistrationClient;
import de.hybris.clicks.webclient.customer.client.CustomerUpdateClient;
import de.hybris.clicks.webclient.product.client.OnlineCatalogueClient;
import de.hybris.clicks.webservices.testOnline.schemas.OnlineCatalogue;

import javax.annotation.Resource;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class TestOnlineCatalogEndpoint
{
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue";
	public static final String GET_PERSONS_REQUEST = "OnlineCatalogue";

	@Resource
	OnlineCatalogueClient onlineCatalogueClient;
	@Resource
	CustomerRegistrationClient customerRegistrationClient;
	@Resource
	CustomerEnrollmentClient customerEnrollmentClient;
	@Resource
	CustomerUpdateClient customerUpdateClient;
	@Resource
	CustomerDetailsRequestClient customerDetailsRequestClient;

	@PayloadRoot(localPart = "OnlineCatalogue", namespace = "http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue")
	@ResponsePayload
	public void getOnlineCatalogue(@RequestPayload final OnlineCatalogue onlineCatalogue)
	{

		if (onlineCatalogue.getHeader().getEventLogId().equals("12"))
		{
			//onlineCatalogueClient.productCatalogueClient();
		}
		if (onlineCatalogue.getHeader().getEventLogId().equals("13"))
		{
			//customerRegistrationClient.registerCCcustomer(new CustomerData());
			//customerRegistrationClient.registerNonCCcustomer(new CustomerData());
		}
		if (onlineCatalogue.getHeader().getEventLogId().equals("14"))
		{
			//customerEnrollmentClient.enrollCustomer(new CustomerData());
		}
		if (onlineCatalogue.getHeader().getEventLogId().equals("15"))
		{
			//customerUpdateClient.updateCustomer();
		}
		if (onlineCatalogue.getHeader().getEventLogId().equals("16"))
		{
			//customerDetailsRequestClient.getCustomerDetails();
		}

	}
}
