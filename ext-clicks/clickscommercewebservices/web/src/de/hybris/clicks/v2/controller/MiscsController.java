/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.v2.controller;

import de.hybris.clicks.clickscommercewebservices.dto.ResultWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.ContactUs.ClicksContactUsRequestWsDTO;
import de.hybris.clicks.facades.contactUs.impl.ContactUsFacade;
import de.hybris.clicks.facades.myaccount.MyAccountFacade;
import de.hybris.clicks.facades.product.data.CountryDataList;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.clicks.facades.product.data.ProvinceDataList;
import de.hybris.clicks.facades.product.data.TopicsDataList;
import de.hybris.clicks.order.data.CardTypeDataList;
import de.hybris.clicks.storesession.data.CurrencyDataList;
import de.hybris.clicks.storesession.data.LanguageDataList;
import de.hybris.clicks.user.data.TitleDataList;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;
import de.hybris.platform.commercefacades.user.data.TopicsData;
import de.hybris.platform.commercewebservicescommons.cache.CacheControl;
import de.hybris.platform.commercewebservicescommons.cache.CacheControlDirective;
import de.hybris.platform.commercewebservicescommons.dto.ClicksContactUsTopicsListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CardTypeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.storesession.CurrencyListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.storesession.LanguageListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.ClicksProvinceListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.CountryListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.TitleListWsDTO;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Misc Controller
 */
@Controller
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 1800)
public class MiscsController extends BaseController
{
	protected static final String CONTACT_US_EMAIL_PROCESS = "contactUsEmailProcess";

	@Resource(name = "myAccountFacade")
	private MyAccountFacade myAccountFacade;
	@Resource(name = "userFacade")
	private UserFacade userFacade;
	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;
	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;
	@Resource(name = "contactUsDTOValidator")
	private Validator contactUsDTOValidator;
	@Resource(name = "contactUsFacade")
	ContactUsFacade contactUsFacade;

	/**
	 * Lists all available languages (all languages used for a particular store). If the list of languages for a base
	 * store is empty, a list of all languages available in the system will be returned.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List of languages
	 */
	@RequestMapping(value = "/{baseSiteId}/languages", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getLanguages',#fields)")
	@ResponseBody
	public LanguageListWsDTO getLanguages(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final LanguageDataList dataList = new LanguageDataList();
		dataList.setLanguages(storeSessionFacade.getAllLanguages());
		return dataMapper.map(dataList, LanguageListWsDTO.class, fields);
	}

	/**
	 * Lists all available currencies (all usable currencies for the current store).If the list of currencies for stores
	 * is empty, a list of all currencies available in the system is returned.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List of currencies
	 */
	@RequestMapping(value = "/{baseSiteId}/currencies", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getCurrencies',#fields)")
	@ResponseBody
	public CurrencyListWsDTO getCurrencies(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final CurrencyDataList dataList = new CurrencyDataList();
		dataList.setCurrencies(storeSessionFacade.getAllCurrencies());
		return dataMapper.map(dataList, CurrencyListWsDTO.class, fields);
	}

	/**
	 * Lists all supported delivery countries for the current store. The list is sorted alphabetically.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List supported delivery countries.
	 */
	@RequestMapping(value = "/{baseSiteId}/deliverycountries", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getDeliveryCountries',#fields)")
	@ResponseBody
	public CountryListWsDTO getDeliveryCountries(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final List<de.hybris.clicks.facades.product.data.CountryData> countryList = new ArrayList<de.hybris.clicks.facades.product.data.CountryData>();
		final CountryDataList countryDTOList = new CountryDataList();
		countryDTOList.setCountries(myAccountFacade.fetchCountryData(countryList));
		return dataMapper.map(countryDTOList, CountryListWsDTO.class, fields);
	}

	/**
	 * Lists all localized titles.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List of titles
	 */
	@RequestMapping(value = "/{baseSiteId}/titles", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getTitles',#fields)")
	@ResponseBody
	public TitleListWsDTO getTitles(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final TitleDataList dataList = new TitleDataList();
		dataList.setTitles(userFacade.getTitles());
		return dataMapper.map(dataList, TitleListWsDTO.class, fields);
	}

	/**
	 * Lists supported payment card types.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List of card types
	 */
	@RequestMapping(value = "/{baseSiteId}/cardtypes", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getCardTypes',#fields)")
	@ResponseBody
	public CardTypeListWsDTO getCardTypes(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final CardTypeDataList dataList = new CardTypeDataList();
		dataList.setCardTypes(checkoutFacade.getSupportedCardTypes());
		return dataMapper.map(dataList, CardTypeListWsDTO.class, fields);
	}

	/**
	 * Lists all supported provinces for the current store. The list is sorted alphabetically.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List supported provinces.
	 */
	@RequestMapping(value = "/{baseSiteId}/province", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getProvinceList',#fields)")
	@ResponseBody
	public ClicksProvinceListWsDTO geProvinceList(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final List<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		final ProvinceDataList provinceDataList = new ProvinceDataList();
		provinceDataList.setProvinces(myAccountFacade.fetchProvinceData(provinceList));
		return dataMapper.map(provinceDataList, ClicksProvinceListWsDTO.class, fields);
	}

	/**
	 * Lists all supported provinces for the current store. The list is sorted alphabetically.
	 *
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 * @return List supported provinces.
	 */
	@RequestMapping(value = "/{baseSiteId}/contactUsTopics", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getContactUsTopics',#fields)")
	@ResponseBody
	public ClicksContactUsTopicsListWsDTO geContactUsTopics(@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final List<TopicsData> topicsData = new ArrayList<TopicsData>();
		final TopicsDataList topicsDataList = new TopicsDataList();
		topicsDataList.setTopics(myAccountFacade.fetchTopicData(topicsData, Boolean.TRUE.booleanValue()));
		return dataMapper.map(topicsDataList, ClicksContactUsTopicsListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/contactUsQuery", method = RequestMethod.POST)
	@ResponseBody
	public ResultWsDTO contactUsSendQueryMessage(@RequestBody final ClicksContactUsRequestWsDTO contactUsDetails)
	{
		validate(contactUsDetails, "contactUsDetails", contactUsDTOValidator);
		final ContactUsMessageData contactUsData = myAccountFacade.populateContactUsMessageData(contactUsDetails);
		final ResultWsDTO resultWsDTO = new ResultWsDTO();
		/*
		 * setting the process for email process
		 */
		contactUsData.setProcessName(CONTACT_US_EMAIL_PROCESS);
		if (contactUsFacade.processContactUs(contactUsData))
		{
			resultWsDTO.setCode("0");
			resultWsDTO.setMessage("Success");
		}
		else
		{
			resultWsDTO.setCode("400");
			resultWsDTO.setMessage("Exception");
		}
		return resultWsDTO;
	}
}
