/**
 *
 */
package de.hybris.clicks.validator;

import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author manish.bhargava
 *
 */
public class MemberIDValidator implements Validator
{
	private String fieldPath;
	private String errorMessageID;
	private String regularExpression;

	/**
	 * @return the regularExpression
	 */
	public String getRegularExpression()
	{
		return regularExpression;
	}

	/**
	 * @param regularExpression
	 *           the regularExpression to set
	 */
	public void setRegularExpression(final String regularExpression)
	{
		this.regularExpression = regularExpression;
	}

	/**
	 * @return the errorMessageID
	 */
	public String getErrorMessageID()
	{
		return errorMessageID;
	}

	/**
	 * @param errorMessageID
	 *           the errorMessageID to set
	 */
	public void setErrorMessageID(final String errorMessageID)
	{
		this.errorMessageID = errorMessageID;
	}

	/**
	 * @return the fieldPath
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * @param fieldPath
	 *           the fieldPath to set
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return ClicksUserRequestWsDTO.class.equals(paramClass);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object paramObject, final Errors errors)
	{
		final ClicksUserRequestWsDTO clicksUserRequestWsDTO = (ClicksUserRequestWsDTO) paramObject;
		if (clicksUserRequestWsDTO.isClubCard())
		{
			if (StringUtils.isNotBlank(clicksUserRequestWsDTO.getMemberID()))
			{
				final Pattern p = Pattern.compile(getRegularExpression());//. represents single character
				Assert.notNull(errors, "Errors object must not be null");
				final String fieldValue = (String) errors.getFieldValue(getFieldPath());
				final Matcher m = p.matcher(fieldValue);
				if (!m.matches())
				{
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
			else
			{
				errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
				{ getFieldPath() }, null);
			}

		}
	}
}
