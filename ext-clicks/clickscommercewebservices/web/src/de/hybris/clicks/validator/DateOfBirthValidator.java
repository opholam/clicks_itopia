/**
 *
 */
package de.hybris.clicks.validator;

import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author manish.bhargava
 *
 */
public class DateOfBirthValidator implements Validator
{
	private String fieldPath;
	private String errorMessageID;

	/**
	 * @return the errorMessageID
	 */
	public String getErrorMessageID()
	{
		return errorMessageID;
	}

	/**
	 * @param errorMessageID
	 *           the errorMessageID to set
	 */
	public void setErrorMessageID(final String errorMessageID)
	{
		this.errorMessageID = errorMessageID;
	}

	/**
	 * @return the fieldPath
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * @param fieldPath
	 *           the fieldPath to set
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return ClicksUserRequestWsDTO.class.equals(paramClass);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object paramObject, final Errors errors)
	{
		final ClicksUserRequestWsDTO clicksUserRequestWsDTO = (ClicksUserRequestWsDTO) paramObject;
		if (StringUtils.isNotBlank(clicksUserRequestWsDTO.getSA_DOB_day())
				&& StringUtils.isNotBlank(clicksUserRequestWsDTO.getSA_DOB_month())
				&& StringUtils.isNotBlank(clicksUserRequestWsDTO.getSA_DOB_year())
				&& Integer.parseInt(clicksUserRequestWsDTO.getSA_DOB_day()) <= 31
				&& Integer.parseInt(clicksUserRequestWsDTO.getSA_DOB_month()) <= 12)
		{
			final String date = clicksUserRequestWsDTO.getSA_DOB_month() + "/" + clicksUserRequestWsDTO.getSA_DOB_day() + "/"
					+ clicksUserRequestWsDTO.getSA_DOB_year();
			try
			{
				final Date dobDate = new Date(date);
				final Date compareDate = new Date();
				compareDate.setYear(new Date().getYear() - 18);
				if (dobDate.after(compareDate))
				{
					// show error msg here -- age should be more than 18
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
			catch (final Exception e)
			{
				errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
				{ getFieldPath() }, null);
			}
		}
		else
		{
			errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
			{ getFieldPath() }, null);
		}

	}
}
