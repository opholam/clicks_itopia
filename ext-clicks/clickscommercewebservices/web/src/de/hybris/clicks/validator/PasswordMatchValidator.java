/**
 *
 */
package de.hybris.clicks.validator;

import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author manish.bhargava
 *
 */
public class PasswordMatchValidator implements Validator
{
	private String fieldPath;
	private String errorMessageID;

	/**
	 * @return the errorMessageID
	 */
	public String getErrorMessageID()
	{
		return errorMessageID;
	}

	/**
	 * @param errorMessageID
	 *           the errorMessageID to set
	 */
	public void setErrorMessageID(final String errorMessageID)
	{
		this.errorMessageID = errorMessageID;
	}

	/**
	 * @return the fieldPath
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * @param fieldPath
	 *           the fieldPath to set
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return ClicksUserRequestWsDTO.class.equals(paramClass);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object paramObject, final Errors errors)
	{
		final ClicksUserRequestWsDTO clicksUserRequestWsDTO = (ClicksUserRequestWsDTO) paramObject;
		if (!StringUtils.equals(clicksUserRequestWsDTO.getPassword(), clicksUserRequestWsDTO.getConfirmPassword()))
		{
			errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
			{ getFieldPath() }, null);
		}
	}
}
