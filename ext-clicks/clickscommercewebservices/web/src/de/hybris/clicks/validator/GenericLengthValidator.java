/**
 *
 */
package de.hybris.clicks.validator;

import de.hybris.clicks.clickscommercewebservices.dto.ContactUs.ClicksContactUsRequestWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author manish.bhargava
 *
 */
public class GenericLengthValidator implements Validator
{
	private String fieldPath;
	private String errorMessageID;
	private String length;

	/**
	 * @return the length
	 */
	public String getLength()
	{
		return length;
	}

	/**
	 * @param length
	 *           the length to set
	 */
	public void setLength(final String length)
	{
		this.length = length;
	}

	/**
	 * @return the errorMessageID
	 */
	public String getErrorMessageID()
	{
		return errorMessageID;
	}

	/**
	 * @param errorMessageID
	 *           the errorMessageID to set
	 */
	public void setErrorMessageID(final String errorMessageID)
	{
		this.errorMessageID = errorMessageID;
	}

	/**
	 * @return the fieldPath
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * @param fieldPath
	 *           the fieldPath to set
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object paramObject, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		if (paramObject instanceof ClicksUserRequestWsDTO)
		{
			final ClicksUserRequestWsDTO clicksUserRequestWsDTO = (ClicksUserRequestWsDTO) paramObject;
			if (clicksUserRequestWsDTO.isJoinClubCard() && StringUtils.isBlank(clicksUserRequestWsDTO.getMemberID()))
			{
				final String fieldValue = (String) errors.getFieldValue(getFieldPath());
				if ("addressLine2".equalsIgnoreCase(getFieldPath()) || "town".equalsIgnoreCase(getFieldPath()))
				{
					if (StringUtils.length(fieldValue) > Integer.parseInt(getLength()))
					{
						errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
						{ getFieldPath() }, null);
					}
				}
				else if (StringUtils.isBlank(fieldValue) || StringUtils.length(fieldValue) == 0
						|| StringUtils.length(fieldValue) > Integer.parseInt(getLength()))
				{
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
		}
		else if (paramObject instanceof ClicksContactUsRequestWsDTO)
		{
			final ClicksContactUsRequestWsDTO clicksContactUsRequestWsDTO = (ClicksContactUsRequestWsDTO) paramObject;
			final String fieldValue = (String) errors.getFieldValue(getFieldPath());
			if ("city".equalsIgnoreCase(getFieldPath()))
			{
				if (StringUtils.length(fieldValue) > Integer.parseInt(getLength()))
				{
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
			else if ("message".equalsIgnoreCase(getFieldPath()) || clicksContactUsRequestWsDTO.isPharmacyQuery())
			{
				if (StringUtils.isBlank(fieldValue) || StringUtils.length(fieldValue) == 0
						|| StringUtils.length(fieldValue) > Integer.parseInt(getLength()))
				{
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
		}
	}
}
