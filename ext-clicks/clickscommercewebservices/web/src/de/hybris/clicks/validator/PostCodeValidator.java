/**
 *
 */
package de.hybris.clicks.validator;

import de.hybris.clicks.clickscommercewebservices.dto.ContactUs.ClicksContactUsRequestWsDTO;
import de.hybris.clicks.clickscommercewebservices.dto.user.ClicksUserRequestWsDTO;
import de.hybris.clicks.core.model.DeliveryAreaModel;
import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author manish.bhargava
 *
 */
public class PostCodeValidator implements Validator
{
	@Resource(name = "clicksCheckoutFacade")
	private ClicksCheckoutFacade clicksCheckoutFacade;

	private String fieldPath;
	private String errorMessageID;
	private String regularExpression;

	/**
	 * @return the regularExpression
	 */
	public String getRegularExpression()
	{
		return regularExpression;
	}

	/**
	 * @param regularExpression
	 *           the regularExpression to set
	 */
	public void setRegularExpression(final String regularExpression)
	{
		this.regularExpression = regularExpression;
	}

	/**
	 * @return the errorMessageID
	 */
	public String getErrorMessageID()
	{
		return errorMessageID;
	}

	/**
	 * @param errorMessageID
	 *           the errorMessageID to set
	 */
	public void setErrorMessageID(final String errorMessageID)
	{
		this.errorMessageID = errorMessageID;
	}

	/**
	 * @return the fieldPath
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * @param fieldPath
	 *           the fieldPath to set
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object paramObject, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		if (paramObject instanceof ClicksUserRequestWsDTO)
		{
			final ClicksUserRequestWsDTO clicksUserRequestWsDTO = (ClicksUserRequestWsDTO) paramObject;
			if (clicksUserRequestWsDTO.isJoinClubCard() && StringUtils.isBlank(clicksUserRequestWsDTO.getMemberID()))
			{
				final String fieldValue = (String) errors.getFieldValue(getFieldPath());
				if (StringUtils.isNotBlank(fieldValue))
				{
					final Pattern p = Pattern.compile(getRegularExpression());//. represents single character
					final Matcher m = p.matcher(fieldValue);
					if (!m.matches())
					{
						errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
						{ getFieldPath() }, null);
					}
					else
					{
						try
						{
							final SearchResult<DeliveryAreaModel> result = clicksCheckoutFacade.fetchResults(fieldValue);
							if (null != result && result.getCount() == 0)
							{
								errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
								{ getFieldPath() }, null);
							}
						}
						catch (final Exception exp)
						{
							errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
							{ getFieldPath() }, null);
						}
					}
				}
				else
				{
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
		}
		else if (paramObject instanceof ClicksContactUsRequestWsDTO)
		{
			final String fieldValue = (String) errors.getFieldValue(getFieldPath());
			if (StringUtils.isNotBlank(fieldValue))
			{
				final Pattern p = Pattern.compile(getRegularExpression());//. represents single character
				final Matcher m = p.matcher(fieldValue);
				if (!m.matches())
				{
					errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
					{ getFieldPath() }, null);
				}
			}
			else
			{
				errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[]
				{ getFieldPath() }, null);
			}
		}
	}
}
