/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.clicksstorefrontcommons.forms;

/**
 * @author shruthi.jhamb
 *
 */

public class CartPageCalForm
{

	private String clubcardPoints;

	/**
	 * @return the clubcardPoints
	 */
	public String getClubcardPoints()
	{
		return clubcardPoints;
	}

	/**
	 * @param clubcardPoints
	 *           the clubcardPoints to set
	 */
	public void setClubcardPoints(final String clubcardPoints)
	{
		this.clubcardPoints = clubcardPoints;
	}


}
