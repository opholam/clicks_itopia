package de.hybris.platform.clicksstorefrontcommons.forms;

public class RegisterCustomerForm
{
	private String firstName;
	private String preferedName;
	private String lastName;
	private String eMailAddress;
	private int gender;
	private String IDNumber;
	private String alternateNumber;
	private String contactNumber;

	private String hybrisRetailerId;
	private String customerId;

	private String password;
	private String confirmPassword;
	private int isClubCard = 1;

	private int isSAResident = 1;
	private Boolean joinClubCard;
	private Boolean result;

	private String clubcardNumber;

	private String SA_DOB_day;
	private String SA_DOB_month;
	private String SA_DOB_year;

	private String SA_DOB;

	private boolean marketingEmail;
	private boolean marketingSMS;
	private boolean accinfoEmail;
	private boolean accinfoSMS;



	/**
	 * @return the result
	 */
	public Boolean getResult()
	{
		return result;
	}

	/**
	 * @param result
	 *           the result to set
	 */
	public void setResult(final Boolean result)
	{
		this.result = result;
	}

	/**
	 * @return the marketingEmail
	 */
	public boolean isMarketingEmail()
	{
		return marketingEmail;
	}

	/**
	 * @param marketingEmail
	 *           the marketingEmail to set
	 */
	public void setMarketingEmail(final boolean marketingEmail)
	{
		this.marketingEmail = marketingEmail;
	}

	/**
	 * @return the marketingSMS
	 */
	public boolean isMarketingSMS()
	{
		return marketingSMS;
	}

	/**
	 * @param marketingSMS
	 *           the marketingSMS to set
	 */
	public void setMarketingSMS(final boolean marketingSMS)
	{
		this.marketingSMS = marketingSMS;
	}

	/**
	 * @return the accinfoEmail
	 */
	public boolean isAccinfoEmail()
	{
		return accinfoEmail;
	}

	/**
	 * @param accinfoEmail
	 *           the accinfoEmail to set
	 */
	public void setAccinfoEmail(final boolean accinfoEmail)
	{
		this.accinfoEmail = accinfoEmail;
	}

	/**
	 * @return the accinfoSMS
	 */
	public boolean isAccinfoSMS()
	{
		return accinfoSMS;
	}

	/**
	 * @param accinfoSMS
	 *           the accinfoSMS to set
	 */
	public void setAccinfoSMS(final boolean accinfoSMS)
	{
		this.accinfoSMS = accinfoSMS;
	}

	/**
	 * @return the alternateNumber
	 */
	public String getAlternateNumber()
	{
		return alternateNumber;
	}

	/**
	 * @param alternateNumber
	 *           the alternateNumber to set
	 */
	public void setAlternateNumber(final String alternateNumber)
	{
		this.alternateNumber = alternateNumber;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *           the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword()
	{
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *           the confirmPassword to set
	 */
	public void setConfirmPassword(final String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the preferedName
	 */
	public String getPreferedName()
	{
		return preferedName;
	}

	/**
	 * @param preferedName
	 *           the preferedName to set
	 */
	public void setPreferedName(final String preferedName)
	{
		this.preferedName = preferedName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the eMailAddress
	 */
	public String geteMailAddress()
	{
		return eMailAddress;
	}

	/**
	 * @param eMailAddress
	 *           the eMailAddress to set
	 */
	public void seteMailAddress(final String eMailAddress)
	{
		this.eMailAddress = eMailAddress;
	}

	/**
	 * @return the gender
	 */
	public int getGender()
	{
		return gender;
	}

	/**
	 * @param gender
	 *           the gender to set
	 */
	public void setGender(final int gender)
	{
		this.gender = gender;
	}

	/**
	 * @return the iDNumber
	 */
	public String getIDNumber()
	{
		return IDNumber;
	}

	/**
	 * @param iDNumber
	 *           the iDNumber to set
	 */
	public void setIDNumber(final String iDNumber)
	{
		IDNumber = iDNumber;
	}


	/**
	 * @return the hybrisRetailerId
	 */
	public String getHybrisRetailerId()
	{
		return hybrisRetailerId;
	}

	/**
	 * @param hybrisRetailerId
	 *           the hybrisRetailerId to set
	 */
	public void setHybrisRetailerId(final String hybrisRetailerId)
	{
		this.hybrisRetailerId = hybrisRetailerId;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *           the password to set
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * @return the isClubCard
	 */
	public int getIsClubCard()
	{
		return isClubCard;
	}

	/**
	 * @param isClubCard
	 *           the isClubCard to set
	 */
	public void setIsClubCard(final int isClubCard)
	{
		this.isClubCard = isClubCard;
	}

	/**
	 * @return the isSAResident
	 */
	public int getIsSAResident()
	{
		return isSAResident;
	}

	/**
	 * @param isSAResident
	 *           the isSAResident to set
	 */
	public void setIsSAResident(final int isSAResident)
	{
		this.isSAResident = isSAResident;
	}

	/**
	 * @return the joinClubCard
	 */
	public Boolean getJoinClubCard()
	{
		return joinClubCard;
	}

	/**
	 * @param joinClubCard
	 *           the joinClubCard to set
	 */
	public void setJoinClubCard(final Boolean joinClubCard)
	{
		this.joinClubCard = joinClubCard;
	}

	/**
	 * @return the clubcardNumber
	 */
	public String getClubcardNumber()
	{
		return clubcardNumber;
	}

	/**
	 * @param clubcardNumber
	 *           the clubcardNumber to set
	 */
	public void setClubcardNumber(final String clubcardNumber)
	{
		this.clubcardNumber = clubcardNumber;
	}


	/**
	 * @return the sA_DOB_day
	 */
	public String getSA_DOB_day()
	{
		return SA_DOB_day;
	}

	/**
	 * @param sA_DOB_day
	 *           the sA_DOB_day to set
	 */
	public void setSA_DOB_day(final String sA_DOB_day)
	{
		SA_DOB_day = sA_DOB_day;
	}

	/**
	 * @return the sA_DOB_month
	 */
	public String getSA_DOB_month()
	{
		return SA_DOB_month;
	}

	/**
	 * @param sA_DOB_month
	 *           the sA_DOB_month to set
	 */
	public void setSA_DOB_month(final String sA_DOB_month)
	{
		SA_DOB_month = sA_DOB_month;
	}

	/**
	 * @return the sA_DOB_year
	 */
	public String getSA_DOB_year()
	{
		return SA_DOB_year;
	}

	/**
	 * @param sA_DOB_year
	 *           the sA_DOB_year to set
	 */
	public void setSA_DOB_year(final String sA_DOB_year)
	{
		SA_DOB_year = sA_DOB_year;
	}

	/**
	 * @return the sA_DOB
	 */
	public String getSA_DOB()
	{
		return SA_DOB;
	}

	/**
	 * @param sA_DOB
	 *           the sA_DOB to set
	 */
	public void setSA_DOB(final String sA_DOB)
	{
		SA_DOB = sA_DOB;
	}



}
