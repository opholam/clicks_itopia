/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.clicksstorefrontcommons.forms.validation;

import de.hybris.platform.clicksstorefrontcommons.forms.PaymentDetailsForm;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component("paymentDetailsValidator")
public class PaymentDetailsValidator implements Validator
{
	public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final int MAX_FIELD_LENGTH = 30;
	private static final int MAX_ADDRESS_LENGTH = 50;
	private static final int MAX_POSTCODE_LENGTH = 10;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return PaymentDetailsForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final PaymentDetailsForm form = (PaymentDetailsForm) object;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.firstName", "address.firstName.invalid");
		if (StringUtils.length(form.getBillingAddress().getFirstName()) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("billingAddress.firstName", "address.field.length.invalid");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.lastName", "address.lastName.invalid");
		if (StringUtils.length(form.getBillingAddress().getLastName()) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("billingAddress.lastName", "address.field.length.invalid");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.line1", "address.line1.invalid");
		if (StringUtils.length(form.getBillingAddress().getLine1()) > MAX_ADDRESS_LENGTH)
		{
			errors.rejectValue("billingAddress.line1", "address.length.invalid");
		}
		if (StringUtils.length(form.getBillingAddress().getLine2()) > MAX_ADDRESS_LENGTH)
		{
			errors.rejectValue("billingAddress.line2", "address.length.invalid");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.suburb", "address.suburb.invalid");
		if (StringUtils.length(form.getBillingAddress().getSuburb()) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("billingAddress.suburb", "address.field.length.invalid");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.postcode", "address.postcode.invalid");
		if (StringUtils.length(form.getBillingAddress().getPostcode()) > MAX_POSTCODE_LENGTH)
		{
			errors.rejectValue("billingAddress.postcode", "address.postcode.length.invalid");
		}
		if (StringUtils.length(form.getBillingAddress().getTownCity()) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("billingAddress.townCity", "address.field.length.invalid");
		}
		if (!validateRegex(EMAIL_REGEX, form.getBillingAddress().getEmail()))
		{
			errors.rejectValue("billingAddress.email", "address.email.invalid");
		}
		else if (StringUtils.length(form.getBillingAddress().getEmail()) > MAX_ADDRESS_LENGTH)
		{
			errors.rejectValue("billingAddress.email", "address.length.invalid");
		}
	}

	protected Calendar parseDate(final String month, final String year)
	{
		if (StringUtils.isNotBlank(month) && StringUtils.isNotBlank(year))
		{
			final Integer yearInt = getIntegerForString(year);
			final Integer monthInt = getIntegerForString(month);

			if (yearInt != null && monthInt != null)
			{
				final Calendar date = getCalendarResetTime();
				date.set(Calendar.YEAR, yearInt.intValue());
				date.set(Calendar.MONTH, monthInt.intValue() - 1);
				date.set(Calendar.DAY_OF_MONTH, 1);
				return date;
			}
		}
		return null;
	}

	protected Calendar getCalendarResetTime()
	{
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	/**
	 * Common method to convert a String to an Integer.
	 *
	 * @param value
	 *           - the String value to be converted.
	 * @return - an Integer object.
	 */
	protected Integer getIntegerForString(final String value)
	{
		if (value != null && !value.isEmpty())
		{
			try
			{
				return Integer.valueOf(value);
			}
			catch (final Exception ignore)
			{
				// Ignore
			}
		}

		return null;
	}

	public static boolean validateRegex(final String regex, final String text)
	{
		if (StringUtils.isEmpty(text))
		{
			return false;
		}

		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}

}
