/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.clicksstorefrontcommons.controllers.pages;

import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.customer.ClicksCustomerFacade;
import de.hybris.clicks.facades.process.email.context.ForgottenPasswordEmailContext;
import de.hybris.clicks.facades.product.data.DayData;
import de.hybris.clicks.facades.product.data.MonthData;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.clicks.facades.product.data.YearData;
import de.hybris.clicks.facades.reverse.populator.ClicksCustomerReversePopulator;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.clicksstorefrontcommons.forms.ForgottenPwdForm;
import de.hybris.platform.clicksstorefrontcommons.forms.GuestForm;
import de.hybris.platform.clicksstorefrontcommons.forms.LoginForm;
import de.hybris.platform.clicksstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.clicksstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.clicksstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public abstract class AbstractRegisterPageController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(AbstractRegisterPageController.class);

	protected abstract AbstractPageModel getCmsPage() throws CMSItemNotFoundException;

	protected abstract String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response);

	protected static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";
	protected static final String NEW_ACCOUNT_NEW_CLUB_CARD_EMAIL_PROCESS = "newAccountNewCCEmailProcess";
	protected static final String NEW_ACCOUNT_EXISTING_CLUB_CARD_EMAIL_PROCESS = "newAccountExistingCCEmailProcess";
	protected static final String CLUB_CARD_LINKED_TO_ACCOUNT_EMAIL_PROCESS = "clubcardLinkedToAccountEmailProcess";

	protected abstract String getView();

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource
	ModelService modelService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "registrationValidator")
	private Validator registrationValidator;

	@Resource(name = "customerReversePopulator")
	ClicksCustomerReversePopulator clicksCustomerReversePopulator;

	@Resource(name = "forgottenPasswordEmailContext")
	ForgottenPasswordEmailContext forgottenPasswordEmailContext;

	/**
	 * @return the registrationValidator
	 */
	protected Validator getRegistrationValidator()
	{
		return registrationValidator;
	}

	/**
	 * @return the autoLoginStrategy
	 */
	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}

	/**
	 * @return the userFacade
	 */
	protected UserFacade getUserFacade()
	{
		return userFacade;
	}

	/**
	 *
	 * @return GUIDCookieStrategy
	 */
	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	// added for country and province
	@ModelAttribute("country")
	public Collection<de.hybris.clicks.facades.product.data.CountryData> getCountryList()
	{
		final Collection<de.hybris.clicks.facades.product.data.CountryData> countryList = new ArrayList<de.hybris.clicks.facades.product.data.CountryData>();
		de.hybris.clicks.facades.product.data.CountryData country;
		final String query = "SELECT {PK} FROM {Country}";
		final SearchResult<CountryModel> result = flexibleSearchService.search(query);
		if (result.getCount() > 0)
		{
			for (final CountryModel countryModel : result.getResult())
			{
				country = new de.hybris.clicks.facades.product.data.CountryData();
				country.setCode(countryModel.getIsocode());
				country.setName(countryModel.getName());
				countryList.add(country);
			}
		}

		return countryList;
	}

	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		final Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

	// end -added for country and province

	@ModelAttribute("days")
	public Collection<DayData> getDays()
	{
		final Collection<DayData> days = new ArrayList<DayData>();
		DayData day;
		for (int i = 1; i <= 31; i++)
		{
			day = new DayData();
			day.setCode(new Integer(i).toString());
			day.setName(new Integer(i).toString());
			days.add(day);
		}
		return days;
	}

	@ModelAttribute("years")
	public Collection<YearData> getYears()
	{
		final Collection<YearData> years = new ArrayList<YearData>();
		YearData year;
		final Calendar now = Calendar.getInstance();
		final int currentYear = now.get(Calendar.YEAR);
		for (int i = currentYear - 100; i <= currentYear; i++)
		{
			year = new YearData();
			year.setCode(new Integer(i).toString());
			year.setName(new Integer(i).toString());
			years.add(year);
		}
		return years;
	}

	@ModelAttribute("months")
	public Collection<MonthData> getMonths()
	{
		final Collection<MonthData> months = new ArrayList<MonthData>();
		MonthData month;
		String monthString = null;
		for (int i = 1; i <= 12; i++)
		{
			switch (i)
			{
				case 1:
					monthString = "Jan";
					break;
				case 2:
					monthString = "Feb";
					break;
				case 3:
					monthString = "Mar";
					break;
				case 4:
					monthString = "Apr";
					break;
				case 5:
					monthString = "May";
					break;
				case 6:
					monthString = "Jun";
					break;
				case 7:
					monthString = "Jul";
					break;
				case 8:
					monthString = "Aug";
					break;
				case 9:
					monthString = "Sep";
					break;
				case 10:
					monthString = "Oct";
					break;
				case 11:
					monthString = "Nov";
					break;
				case 12:
					monthString = "Dec";
					break;
			}
			month = new MonthData();
			month.setCode(new Integer(i).toString());
			month.setName(monthString);
			months.add(month);
		}

		return months;
	}



	protected String getDefaultRegistrationPage(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#", getMessageSource().getMessage("header.link.login", null,
				getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));
		model.addAttribute(new RegisterForm());
		return getView();
	}

	/**
	 * This method takes data from the registration form and create a new customer account and attempts to log in using
	 * the credentials of this new user.
	 *
	 * @return true if there are no binding errors or the account does not already exists.
	 * @throws CMSItemNotFoundException
	 */
	protected String processRegisterUserRequest(final String referer, final ClicksRegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return handleRegistrationError(model);
		}

		RegisterData data = new RegisterData();
		data = populateRegisterData(form, data);
		form.setCustomerId(data.getCustomerId());

		try
		{
			//data.setProcessName(NEW_ACCOUNT_EXISTING_CLUB_CARD_EMAIL_PROCESS);
			getCustomerFacade().register(data);

			// sending mail


			//getAutoLoginStrategy().login(form.geteMailAddress().toLowerCase(), form.getPassword(), request, response);
			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
			//					"registration.confirmation.message.title");
		}
		catch (final DuplicateUidException e)
		{
			LOG.warn("registration failed: " + e);
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			bindingResult.rejectValue("eMailAddress", "registration.error.account.exists.title");
			GlobalMessages.addErrorMessage(model, "registration.error.account.exists.title");
			return handleRegistrationError(model);
		}

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}

	protected String processRegisterCCUserRequest(final String referer, final ClicksRegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return handleRegistrationError(model);
		}

		RegisterData data = new RegisterData();
		data = populateRegisterCCData(form, data);
		form.setCustomerId(data.getCustomerId());

		try
		{
			//data.setProcessName(NEW_ACCOUNT_NEW_CLUB_CARD_EMAIL_PROCESS);
			getCustomerFacade().register(data);
			//getAutoLoginStrategy().login(form.geteMailAddress().toLowerCase(), form.getPassword(), request, response);
			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
			//					"registration.confirmation.message.title");
		}
		catch (final DuplicateUidException e)
		{
			LOG.warn("registration failed: " + e);
			form.setDuplicateCheck(true);
			final String duplicateCheck = Boolean.toString(form.isDuplicateCheck());
			model.addAttribute(form);
			model.addAttribute("duplicateCheck", duplicateCheck);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			final ForgottenPwdForm forgottenPwdForm = new ForgottenPwdForm();
			model.addAttribute("forgottenPwdForm", forgottenPwdForm);
			forgottenPwdForm.setEmail(form.geteMailAddress());
			model.addAttribute("pwdEmail", forgottenPwdForm.getEmail());
			bindingResult.rejectValue("eMailAddress", "registration.error.account.exists.title");
			GlobalMessages.addErrorMessage(model, "registration.error.account.exists.title");
			return handleRegistrationError(model);
		}
		catch (final ModelSavingException e)
		{
			LOG.warn("registration failed: " + e);
			model.addAttribute(form);
			bindingResult.rejectValue("memberID", "register.CC.memberID.duplicate");
			GlobalMessages.addErrorMessage(model, "register.CC.memberID.duplicate");
			return handleRegistrationError(model);
		}

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}

	protected String processRegisterEnrollUserRequest(final String referer, final ClicksRegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		LOG.info("in processRegisterEnrollUserRequest");
		if (bindingResult.hasErrors())
		{
			LOG.info("in processRegisterEnrollUserRequest has error" + bindingResult.getAllErrors());
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return handleRegistrationError(model);
		}

		RegisterData data = new RegisterData();
		data = populateRegisterEnrollData(form, data);
		form.setCustomerId(data.getCustomerId());

		try
		{
			//data.setProcessName(CLUB_CARD_LINKED_TO_ACCOUNT_EMAIL_PROCESS);
			getCustomerFacade().register(data);


			//getAutoLoginStrategy().login(form.geteMailAddress().toLowerCase(), form.getPassword(), request, response);
			//			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
			//					"registration.confirmation.message.title");
		}
		catch (final DuplicateUidException e)
		{
			LOG.warn("registration failed: " + e);
			form.setDuplicateCheck(true);
			final String duplicateCheck = Boolean.toString(form.isDuplicateCheck());
			model.addAttribute(form);
			model.addAttribute("duplicateCheck", duplicateCheck);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			final ForgottenPwdForm forgottenPwdForm = new ForgottenPwdForm();
			model.addAttribute("forgottenPwdForm", forgottenPwdForm);
			forgottenPwdForm.setEmail(form.geteMailAddress());
			model.addAttribute("pwdEmail", forgottenPwdForm.getEmail());
			bindingResult.rejectValue("eMailAddress", "registration.error.account.exists.title");
			GlobalMessages.addErrorMessage(model, "registration.error.account.exists.title");
			return handleRegistrationError(model);
		}

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}


	/**
	 * Anonymous checkout process.
	 *
	 * Creates a new guest customer and updates the session cart with this user. The session user will be anonymous and
	 * it's never updated with this guest user.
	 *
	 * If email is required, grab the email from the form and set it as uid with "guid|email" format.
	 *
	 * @throws de.hybris.platform.cms2.exceptions.CMSItemNotFoundException
	 */
	protected String processAnonymousCheckoutUserRequest(final GuestForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		try
		{
			if (bindingResult.hasErrors())
			{
				model.addAttribute(form);
				model.addAttribute(new LoginForm());
				model.addAttribute(new RegisterForm());
				GlobalMessages.addErrorMessage(model, "form.global.error");
				return handleRegistrationError(model);
			}

			getCustomerFacade().createGuestUserForAnonymousCheckout(form.getEmail(),
					getMessageSource().getMessage("text.guest.customer", null, getI18nService().getCurrentLocale()));
			getGuidCookieStrategy().setCookie(request, response);
			getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
		}
		catch (final DuplicateUidException e)
		{
			LOG.warn("guest registration failed: " + e);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return handleRegistrationError(model);
		}

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}

	protected String handleRegistrationError(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		return getView();
	}



	private RegisterData populateRegisterData(final ClicksRegisterForm form, final RegisterData data)
	{
		final String newID = (String) myNumberseriesGenerator.generate();
		data.setCustomerId(newID);
		if (null != form.getTitle())
		{
			data.setTitleCode(form.getTitle());
		}
		else
		{
			data.setTitleCode("mr");
		}
		if (null != form.getFirstName())
		{
			data.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getLastName())
		{
			data.setLastName(getTitleCase(form.getLastName()));
		}
		if (null != form.geteMailAddress())
		{
			data.setLogin(getTitleCase(form.geteMailAddress()));
		}
		if (null != form.getPassword())
		{
			data.setPassword(form.getPassword());
		}
		//added
		if (null != form.getPreferedName() && !StringUtils.isEmpty(form.getPreferedName()))
		{
			data.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		else
		{
			data.setPreferedName(getTitleCase(form.getFirstName()));
		}

		data.setGender(new Integer(form.getGender()));
		data.setMarketingConsent_email(new Boolean(form.isMarketingEmailNonCC()));

		if (null != form.getIDNumber())
		{
			data.setIDNumber(form.getIDNumber());
		}
		if (form.getIsSAResident() == 1)
		{
			data.setSAResident(Boolean.TRUE);
		}
		if (form.getIsSAResident() == 0)
		{
			data.setSAResident(Boolean.FALSE);
		}

		if (null != form.getSA_DOB_day() && null != form.getSA_DOB_month() && null != form.getSA_DOB_year())
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			data.setSA_DOB(date);
		}

		return data;
	}

	private RegisterData populateRegisterCCData(final ClicksRegisterForm form, final RegisterData data)
	{
		final String newID = (String) myNumberseriesGenerator.generate();
		data.setCustomerId(newID);
		if (null != form.getTitle())
		{
			data.setTitleCode(form.getTitle());
		}
		else
		{
			data.setTitleCode("mr");
		}
		if (null != form.getFirstName())
		{
			data.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getLastName())
		{
			data.setLastName(getTitleCase(form.getLastName()));
		}
		if (null != form.geteMailAddress())
		{
			data.setLogin(getTitleCase(form.geteMailAddress()));
		}
		if (null != form.getPassword())
		{
			data.setPassword(form.getPassword());
		}
		if (null != form.getPreferedName() && !StringUtils.isEmpty(form.getPreferedName()))
		{
			data.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		else
		{
			data.setPreferedName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getMemberID())
		{
			data.setMemberID(form.getMemberID());
		}
		// added

		//		data.setAccinfo_email(new Boolean(form.isAccinfoEmail()));
		//		data.setAccinfo_SMS(new Boolean(form.isAccinfoSMS()));
		//		data.setMarketingConsent_email(new Boolean(form.isMarketingEmail()));
		//		data.setMarketingConsent_SMS(new Boolean(form.isMarketingSMS()));
		//		data.setGender(new Integer(form.getGender()));
		//		data.setSAResident(new Boolean(form.isSAResident()));
		//		if (null != form.getIDNumber())
		//		{
		//			data.setIDNumber(form.getIDNumber());
		//		}
		//		if (null != form.getSA_DOB_day() && null != form.getSA_DOB_month() && null != form.getSA_DOB_year())
		//		{
		//			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
		//			data.setSA_DOB(date);
		//		}
		//
		//		if (null != form.getAlternateNumber() || null != form.getContactNumber())
		//		{
		//			final List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
		//			ContactDetail contact;
		//			if (null != form.getAlternateNumber())
		//			{
		//				contact = new ContactDetail();
		//				contact.setTypeID("4");
		//				contact.setNumber(form.getAlternateNumber());
		//				contactDetails.add(contact);
		//			}
		//			if (null != form.getContactNumber())
		//			{
		//				contact = new ContactDetail();
		//				contact.setTypeID("2");
		//				contact.setNumber(form.getContactNumber());
		//				contactDetails.add(contact);
		//			}
		//			if (contactDetails.size() > 0)
		//			{
		//				data.setContactDetails(contactDetails);
		//			}
		//		}
		//
		//		final List<AddressData> addresses = new ArrayList<AddressData>();
		//		final AddressData address = new AddressData();
		//
		//		address.setTypeID("1");
		//		if (null != form.getAddressLine1())
		//		{
		//			address.setLine1(getTitleCase(form.getAddressLine1()));
		//		}
		//		if (null != form.getAddressLine2())
		//		{
		//			address.setLine2(getTitleCase(form.getAddressLine2()));
		//		}
		//		if (null != form.getSuburb())
		//		{
		//			address.setSuburb(getTitleCase(form.getSuburb()));
		//		}
		//		if (null != form.getCity())
		//		{
		//			address.setCity(getTitleCase(form.getCity()));
		//		}
		//
		//		if (null != form.getCountry())
		//		{
		//			final CountryData countryData = new CountryData();
		//			countryData.setIsocode(form.getCountry());
		//			address.setCountry(countryData);
		//		}
		//
		//		if (null != form.getPostalCode())
		//		{
		//			address.setPostalCode(form.getPostalCode());
		//		}
		//		if (null != form.getProvince())
		//		{
		//			address.setProvince(form.getProvince());
		//		}
		//		addresses.add(address);
		//		if (addresses.size() > 0)
		//		{
		//			data.setAddresses(addresses);
		//		}

		return data;

	}

	private RegisterData populateRegisterEnrollData(final ClicksRegisterForm form, final RegisterData data)
	{
		final String newID = (String) myNumberseriesGenerator.generate();
		data.setCustomerId(newID);
		if (null != form.getTitle())
		{
			data.setTitleCode(form.getTitle());
		}
		else
		{
			data.setTitleCode("mr");
		}
		if (null != form.getFirstName())
		{
			data.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getLastName())
		{
			data.setLastName(getTitleCase(form.getLastName()));
		}
		if (null != form.geteMailAddress())
		{
			data.setLogin(getTitleCase(form.geteMailAddress()));
		}
		if (null != form.getPassword())
		{
			data.setPassword(form.getPassword());
		}
		if (null != form.getMemberID())
		{
			data.setMemberID(form.getMemberID());
		}
		if (null != form.getMemberIDPlasticCard())
		{
			data.setMemberID(form.getMemberIDPlasticCard());
		}

		// added

		data.setAccinfo_email(new Boolean(form.isAccinfoEmail()));
		data.setAccinfo_SMS(new Boolean(form.isAccinfoSMS()));
		data.setMarketingConsent_email(new Boolean(form.isMarketingEmail()));
		data.setMarketingConsent_SMS(new Boolean(form.isMarketingSMS()));

		if (null != form.getPreferedName() && !StringUtils.isEmpty(form.getPreferedName()))
		{
			data.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		else
		{
			data.setPreferedName(getTitleCase(form.getFirstName()));
		}
		data.setGender(new Integer(form.getGender()));
		data.setSAResident(new Boolean(form.isSAResident()));
		if (null != form.getIDNumber())
		{
			data.setIDNumber(form.getIDNumber());
		}
		if (null != form.getSA_DOB_day() && null != form.getSA_DOB_month() && null != form.getSA_DOB_year())
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			data.setSA_DOB(date);
		}

		if (null != form.getAlternateNumber() || null != form.getContactNumber())
		{
			final List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
			ContactDetail contact;
			if (null != form.getAlternateNumber() && StringUtils.isNotEmpty(form.getAlternateNumber()))
			{
				contact = new ContactDetail();
				contact.setTypeID("4");
				contact.setNumber(form.getAlternateNumber());
				contactDetails.add(contact);
			}
			if (null != form.getContactNumber() && StringUtils.isNotEmpty(form.getContactNumber()))
			{
				contact = new ContactDetail();
				contact.setTypeID("2");
				contact.setNumber(form.getContactNumber());
				contactDetails.add(contact);
			}
			if (contactDetails.size() > 0)
			{
				data.setContactDetails(contactDetails);
			}
		}

		final List<AddressData> addresses = new ArrayList<AddressData>();
		final AddressData address = new AddressData();

		address.setTypeID("1");
		if (null != form.getAddressLine1())
		{
			address.setLine1(getTitleCase(form.getAddressLine1()));
		}
		if (null != form.getAddressLine2())
		{
			address.setLine2(getTitleCase(form.getAddressLine2()));
		}
		if (null != form.getSuburb())
		{
			address.setSuburb(getTitleCase(form.getSuburb()));
		}
		if (null != form.getCity())
		{
			address.setCity(getTitleCase(form.getCity()));
		}

		if (null != form.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setIsocode(form.getCountry());
			address.setCountry(countryData);
		}

		if (null != form.getPostalCode())
		{
			address.setPostalCode(form.getPostalCode());
		}
		if (null != form.getProvince())
		{
			address.setProvince(form.getProvince());
		}
		addresses.add(address);
		if (addresses.size() > 0)
		{
			data.setAddresses(addresses);
		}

		return data;
	}

	/**
	 * @param referer
	 * @param form
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @param redirectModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	protected String processJoinCCRequest(final String referer, final ClicksRegisterForm form, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel,
			final CustomerModel customerModel, final boolean escapeErrors) throws CMSItemNotFoundException
	{

		/*
		 * if (!escapeErrors) { model.addAttribute(form); model.addAttribute(new LoginForm()); model.addAttribute(new
		 * GuestForm()); GlobalMessages.addErrorMessage(model, "form.global.error"); return
		 * handleRegistrationError(model); }
		 */

		final CustomerData customerData = new CustomerData();
		populateEnrollData(form, customerData);
		customerData.setUid(customerModel.getUid());
		clicksCustomerReversePopulator.populate(customerData, customerModel);

		if (getCustomerFacade() instanceof ClicksCustomerFacade)
		{
			final ClicksCustomerFacade custFacade = (ClicksCustomerFacade) getCustomerFacade();
			custFacade.joinClubCard(customerModel, CLUB_CARD_LINKED_TO_ACCOUNT_EMAIL_PROCESS);
		}
		return "success";
	}

	private CustomerData populateEnrollData(final ClicksRegisterForm form, final CustomerData customer)
	{

		final List<AddressData> addresses = new ArrayList<AddressData>();
		final AddressData address = new AddressData();

		if (null != form.getAddressLine1())
		{
			address.setLine1(getTitleCase(form.getAddressLine1()));
		}
		if (null != form.getAddressLine2())
		{
			address.setLine2(getTitleCase(form.getAddressLine2()));
		}
		if (null != form.getAddressTypeId())
		{
			address.setTypeID(getTitleCase(form.getAddressTypeId()));
		}
		if (null != form.getSuburb())
		{
			address.setSuburb(getTitleCase(form.getSuburb()));
		}
		if (null != form.getCity())
		{
			address.setCity(getTitleCase(form.getCity()));
		}

		if (null != form.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setIsocode(form.getCountry());
			address.setCountry(countryData);
		}

		if (null != form.getPostalCode())
		{
			address.setPostalCode(form.getPostalCode());
		}
		if (null != form.getProvince())
		{
			address.setProvince(form.getProvince());
		}

		addresses.add(address);
		if (addresses.size() > 0)
		{
			customer.setAddresses(addresses);
		}
		if (null != form.getTitle())
		{
			customer.setTitleCode(form.getTitle());
		}
		if (null != form.getFirstName())
		{
			customer.setFirstName(getTitleCase(form.getFirstName()));
		}
		else
		{
			customer.setTitleCode("mr");
		}
		if (null != form.getPreferedName() && !StringUtils.isEmpty(form.getPreferedName()))
		{
			customer.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		else
		{
			customer.setPreferedName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getLastName())
		{
			customer.setLastName(getTitleCase(form.getLastName()));
		}
		customer.setGender(new Integer(form.getGender()).toString());
		if (null != form.geteMailAddress())
		{
			customer.setEmailID(getTitleCase(form.geteMailAddress()));
		}
		customer.setSAResident(new Boolean(form.isSAResident()));
		if (null != form.getIDNumber())
		{
			customer.setRSA_ID(form.getIDNumber());
		}

		if (null != form.getHybrisRetailerId())
		{
			customer.setRetailerID_hybris(form.getHybrisRetailerId());
		}
		if (null != form.getCustomerId())
		{
			customer.setCustomerID(form.getCustomerId());
		}

		if (null != form.getSA_DOB_day() && null != form.getSA_DOB_month() && null != form.getSA_DOB_year())
		{
			final String date = form.getSA_DOB_day() + "/" + form.getSA_DOB_month() + "/" + form.getSA_DOB_year();
			customer.setNonRSA_DOB(new Date(date));
		}

		customer.setMarketingConsent_email(new Boolean(form.isMarketingEmail()));
		customer.setMarketingConsent_SMS(new Boolean(form.isMarketingSMS()));
		customer.setAccinfo_email(new Boolean(form.isAccinfoEmail()));
		customer.setAccinfo_SMS(new Boolean(form.isAccinfoSMS()));

		if (StringUtils.isNotBlank(form.getContactNumber()) || StringUtils.isNotBlank(form.getAlternateNumber()))
		{
			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
			if (StringUtils.isNotBlank(form.getContactNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getContactNumber());
				contact.setTypeID("2");
				contactDetailList.add(contact);
			}
			if (StringUtils.isNotBlank(form.getAlternateNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getAlternateNumber());
				contact.setTypeID("4");
				contactDetailList.add(contact);
			}
			if (contactDetailList.size() > 0)
			{
				customer.setContactDetails(contactDetailList);
			}
		}
		return customer;
	}


	public void removeUser(final String userId)
	{
		try
		{
			String query;
			SearchResult<UserModel> result;
			query = "SELECT {PK} FROM {User} WHERE {uid}=?userId";
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			result = flexibleSearchService.search(query, params);
			if (result.getCount() > 0)
			{
				for (final UserModel user : result.getResult())
				{
					try
					{
						modelService.removeAll(user.getAddresses());
					}
					catch (final Exception e)
					{
						LOG.error("Error in AbstractRegisterPageController class at removeUser() method for user address : "
								+ e.getMessage());
					}
					modelService.remove(user);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in AbstractRegisterPageController class at removeUser() method in search result : " + e.getMessage());
		}
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

}
