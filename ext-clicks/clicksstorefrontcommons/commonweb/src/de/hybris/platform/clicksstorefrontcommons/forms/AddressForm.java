/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.clicksstorefrontcommons.forms;

import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;


/**
 */
public class AddressForm
{
	private String addressId;
	private String titleCode;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String alternatePhone;
	private String line1;
	private String line2;
	private String townCity;
	private String regionIso;
	private String postcode;
	private String province;
	private String countryIso = "ZA";
	private String countryName;
	private String suburb;
	private String selectedDeliveryMode;
	private String deliveryInstructions;
	private Boolean saveInAddressBook;
	private Boolean preferredContactMethod;
	private Boolean defaultAddress;
	private Boolean shippingAddress;
	private Boolean billingAddress;
	private Boolean editAddress;
	private Boolean isDeliverable;
	List<ZoneDeliveryModeData> deliveryModes;


	/**
	 * @return the province
	 */
	public String getProvince()
	{
		return province;
	}

	/**
	 * @param province
	 *           the province to set
	 */
	public void setProvince(final String province)
	{
		this.province = province;
	}

	/**
	 * @return the selectedDeliveryMode
	 */
	public String getSelectedDeliveryMode()
	{
		return selectedDeliveryMode;
	}

	/**
	 * @param selectedDeliveryMode
	 *           the selectedDeliveryMode to set
	 */
	public void setSelectedDeliveryMode(final String selectedDeliveryMode)
	{
		this.selectedDeliveryMode = selectedDeliveryMode;
	}

	/**
	 * @return the preferredContactMethod
	 */
	public Boolean getPreferredContactMethod()
	{
		return preferredContactMethod;
	}

	/**
	 * @param preferredContactMethod
	 *           the preferredContactMethod to set
	 */
	public void setPreferredContactMethod(final Boolean preferredContactMethod)
	{
		this.preferredContactMethod = preferredContactMethod;
	}

	/**
	 * @return the deliveryInstructions
	 */
	@NotNull(message = "{address.deliveryInstructions.length.invalid}")
	@Size(min = 1, max = 255, message = "{address.deliveryInstructions.length.invalid}")
	public String getDeliveryInstructions()
	{
		return deliveryInstructions;
	}

	/**
	 * @param deliveryInstructions
	 *           the deliveryInstructions to set
	 */
	public void setDeliveryInstructions(final String deliveryInstructions)
	{
		this.deliveryInstructions = deliveryInstructions;
	}

	/**
	 * @return the email
	 */
	@NotNull(message = "{address.email.invalid}")
	@Size(min = 1, max = 255, message = "{address.email.invalid}")
	@Email(message = "{address.email.invalid}")
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	/**
	 * @return the alternatePhone
	 */
	public String getAlternatePhone()
	{
		return alternatePhone;
	}

	/**
	 * @param alternatePhone
	 *           the alternatePhone to set
	 */
	public void setAlternatePhone(final String alternatePhone)
	{
		this.alternatePhone = alternatePhone;
	}

	/**
	 * @return the deliveryModes
	 */
	public List<ZoneDeliveryModeData> getDeliveryModes()
	{
		return deliveryModes;
	}

	/**
	 * @param deliveryModes
	 *           the deliveryModes to set
	 */
	public void setDeliveryModes(final List<ZoneDeliveryModeData> deliveryModes)
	{
		this.deliveryModes = deliveryModes;
	}

	/**
	 * @return the isDeliverable
	 */
	public Boolean getIsDeliverable()
	{
		return isDeliverable;
	}

	/**
	 * @param isDeliverable
	 *           the isDeliverable to set
	 */
	public void setIsDeliverable(final Boolean isDeliverable)
	{
		this.isDeliverable = isDeliverable;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName()
	{
		return countryName;
	}

	/**
	 * @param countryName
	 *           the countryName to set
	 */
	public void setCountryName(final String countryName)
	{
		this.countryName = countryName;
	}

	/**
	 * @return the suburb
	 */
	@NotNull(message = "{address.suburb.invalid}")
	@Size(min = 1, max = 255, message = "{address.suburb.invalid}")
	public String getSuburb()
	{
		return suburb;
	}

	/**
	 * @param suburb
	 *           the suburb to set
	 */
	public void setSuburb(final String suburb)
	{
		this.suburb = suburb;
	}

	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}



	public String getTitleCode()
	{
		return titleCode;
	}

	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	@NotNull(message = "{address.firstName.invalid}")
	@Size(min = 1, max = 255, message = "{address.firstName.invalid}")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	@NotNull(message = "{address.lastName.invalid}")
	@Size(min = 1, max = 255, message = "{address.lastName.invalid}")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	@NotNull(message = "{address.line1.invalid}")
	@Size(min = 1, max = 255, message = "{address.line1.invalid}")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	@NotNull(message = "{address.townCity.invalid}")
	@Size(min = 0, max = 255, message = "{address.townCity.invalid}")
	public String getTownCity()
	{
		return townCity;
	}

	public void setTownCity(final String townCity)
	{
		this.townCity = townCity;
	}

	public String getRegionIso()
	{
		return regionIso;
	}

	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}

	@NotNull(message = "{address.postcode.invalid}")
	@Size(min = 4, max = 10, message = "{address.postcode.invalid}")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@NotNull(message = "{address.country.invalid}")
	@Size(min = 1, max = 255, message = "{address.country.invalid}")
	public String getCountryIso()
	{
		return countryIso;
	}

	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	public Boolean getSaveInAddressBook()
	{
		return saveInAddressBook;
	}

	public void setSaveInAddressBook(final Boolean saveInAddressBook)
	{
		this.saveInAddressBook = saveInAddressBook;
	}

	public Boolean getDefaultAddress()
	{
		return defaultAddress;
	}

	public void setDefaultAddress(final Boolean defaultAddress)
	{
		this.defaultAddress = defaultAddress;
	}

	public Boolean getShippingAddress()
	{
		return shippingAddress;
	}

	public void setShippingAddress(final Boolean shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}

	public Boolean getBillingAddress()
	{
		return billingAddress;
	}

	public void setBillingAddress(final Boolean billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	public Boolean getEditAddress()
	{
		return editAddress;
	}

	public void setEditAddress(final Boolean editAddress)
	{
		this.editAddress = editAddress;
	}
}
