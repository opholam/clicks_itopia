/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.clicksstorefrontcommons.forms;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceStockData;

import java.util.List;


public class StoreFinderForm
{
	public String q; //NOPMD
	public String email;
	public List<PointOfServiceStockData> posList;
	public String storeName;
	public String productCode;
	public String message;
	public Boolean flag;










	/**
	 * @return the flag
	 */
	public Boolean getFlag()
	{
		return flag;
	}

	/**
	 * @param flag
	 *           the flag to set
	 */
	public void setFlag(final Boolean flag)
	{
		this.flag = flag;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * @return the storeName
	 */
	public String getStoreName()
	{
		return storeName;
	}

	/**
	 * @param storeName
	 *           the storeName to set
	 */
	public void setStoreName(final String storeName)
	{
		this.storeName = storeName;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode
	 *           the productCode to set
	 */
	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the posList
	 */
	public List<PointOfServiceStockData> getPosList()
	{
		return posList;
	}

	/**
	 * @param posList
	 *           the posList to set
	 */
	public void setPosList(final List<PointOfServiceStockData> posList)
	{
		this.posList = posList;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getQ()
	{
		return q;
	}

	public void setQ(final String q) //NOPMD
	{
		this.q = q;
	}
}
