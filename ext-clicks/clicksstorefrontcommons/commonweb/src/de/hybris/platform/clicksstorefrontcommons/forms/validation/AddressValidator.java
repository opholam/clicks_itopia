/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.clicksstorefrontcommons.forms.validation;

import de.hybris.clicks.core.model.DeliveryAreaModel;
import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.platform.clicksstorefrontcommons.forms.AddressForm;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validator for address forms. Enforces the order of validation
 */
@Component("addressValidator")
public class AddressValidator implements Validator
{
	private static final int MAX_DELIVERYINSTRUCTIONS_FIELD_LENGTH = 128;
	private static final int MAX_FIELD_LENGTH = 30;
	private static final int MAX_ADDRESS_LENGTH = 50;
	//	private static final int MAX_POSTCODE_LENGTH = 10;
	static Logger LOG = Logger.getLogger(AddressValidator.class.getName());

	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;
	@Resource(name = "clicksCheckoutFacade")
	private ClicksCheckoutFacade clicksCheckoutFacade;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddressForm addressForm = (AddressForm) object;
		final String postcode = addressForm.getPostcode();
		validateStandardFields(addressForm, errors);
		validateCountrySpecificFields(addressForm, errors);
		validatePostcode(postcode, errors);
	}

	/**
	 * @param postcode
	 * @param errors
	 */
	private void validatePostcode(final String postcode, final Errors errors)
	{
		final SearchResult<DeliveryAreaModel> result = clicksCheckoutFacade.fetchResults(postcode);
		try
		{
			if (null != result && result.getCount() == 0)
			{
				errors.rejectValue("postcode", "register.enroll.postalCode.invalid");
			}
			/*
			 * else if (!postcode.matches("^(0|[1-9][0-9]*)$")) { errors.rejectValue("postcode",
			 * "register.enroll.postalCode.invalid"); }
			 */
		}
		catch (final Exception exp)
		{
			errors.rejectValue("postcode", "register.enroll.postalCode.invalid");
		}
	}


	protected void validateStandardFields(final AddressForm addressForm, final Errors errors)
	{
		//validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_ADDRESS_LENGTH, errors);
		validateStringFieldForLength(addressForm.getLine2(), AddressField.LINE2, MAX_ADDRESS_LENGTH, errors);
		validateStringField(addressForm.getSuburb(), AddressField.SUBURB, MAX_FIELD_LENGTH, errors);
		validateStringFieldForLength(addressForm.getTownCity(), AddressField.TOWNCITY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getCountryName(), AddressField.COUNTRYNAME, MAX_FIELD_LENGTH, errors);
		validateStringFieldForLength(addressForm.getDeliveryInstructions(), AddressField.DELIVERYINSTRUCTIONS,
				MAX_DELIVERYINSTRUCTIONS_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);

		//validateStringField(addressForm.getPostcode(), AddressField.EMAIL, MAX_POSTCODE_LENGTH, errors);
		validateStringField(addressForm.getEmail(), AddressField.EMAIL, MAX_ADDRESS_LENGTH, errors);
		//validateStringField(addressForm.getPhone(), AddressField.PHONE, MAX_FIELD_LENGTH, errors);
	}

	protected void validateCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{
		//		final String isoCode = addressForm.getCountryIso();
		//		if (isoCode != null)
		//		{
		//			switch (CountryCode.lookup(isoCode))
		//			{
		//				case CHINA:
		//					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		//					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
		//					break;
		//				case CANADA:
		//					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		//					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
		//					break;
		//				case USA:
		//					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		//					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
		//					break;
		//				case JAPAN:
		//					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
		//					validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
		//					break;
		//				default:
		//					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		//					break;
		//			}
		//		}
	}

	protected static void validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}

		else if (StringUtils.length(addressField) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), "address.field.length.invalid");
		}
	}


	protected static void validateStringFieldForLength(final String addressField, final AddressField fieldType,
			final int maxFieldLength, final Errors errors)
	{
		if (StringUtils.length(addressField) > maxFieldLength)
		{
			if (fieldType.getFieldKey().equalsIgnoreCase("deliveryInstructions"))
			{
				errors.rejectValue(fieldType.getFieldKey(), "address.deliveryInstructions.length.invalid");
			}
			else
			{
				errors.rejectValue(fieldType.getFieldKey(), "address.field.length.invalid");
			}
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		USA("US"), CANADA("CA"), JAPAN("JP"), CHINA("CN"), BRITAIN("GB"), GERMANY("DE"), DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName",
				"address.lastName.invalid"), LINE1("line1", "address.line1.invalid"), LINE2("line2", "address.line2.invalid"), POSTCODE(
				"postcode", "address.postcode.invalid"), REGION("regionIso", "address.regionIso.invalid"), COUNTRY("countryIso",
				"address.country.invalid"), EMAIL("email", "address.email.invalid"), PHONE("phone", "address.phone.invalid"), COUNTRYNAME(
				"countryName", "address.country.invalid"), SUBURB("suburb", "address.suburb.invalid"), TOWNCITY("townCity",
				"address.townCity.invalid"), DELIVERYINSTRUCTIONS("deliveryInstructions",
				"address.deliveryInstructions.length.invalid");

		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
