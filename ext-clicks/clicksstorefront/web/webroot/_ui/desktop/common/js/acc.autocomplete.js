ACC.autocomplete = {
    bindAll: function() {
        this.bindSearchAutocomplete();
    },
    bindSearchAutocomplete: function() {
        $.widget("custom.yautocomplete", $.ui.autocomplete, {
            _create: function() {
                var option = this.element.data("options");
                this._setOptions({
                    minLength: option.minCharactersBeforeRequest,
                    displayProductImages: option.displayProductImages,
                    delay: option.waitTimeBeforeRequest,
                    autocompleteUrl: option.autocompleteUrl,
                    source: this.source
                });
                $.ui.autocomplete.prototype._create.call(this);
            },
            options: {
                cache: {},
                focus: function() {
                    return false;
                },
                select: function(event, ui) {
                    window.location.href = ui.item.url;
                }
            },
            _renderItem: function(ul, item) {
                if (item.type == "autoSuggestion") {
                	
                	var autoSuggestionOutput = String(item.value).replace(new RegExp(this.term, "gi"),"<span class='ui-state-highlight'>$&</span>");
                    var renderHtml = "<a href='?q=" + item.value + "' class='clearfix'>" + autoSuggestionOutput + "</a>";
                    return $("<li class=''>").data("item.autocomplete", item).append(renderHtml).appendTo(ul);
                } else if (item.type == "productResult") {
                	var productResultOutput = String(item.value).replace(new RegExp(this.term, "gi"),"<span class='ui-state-highlight'>$&</span>");
                    var renderHtml = "<a href='" + ACC.config.contextPath + item.url + "?from=slp' class='product clearfix'>";
                    if (item.image != null) {}
                    renderHtml += "<span class='desc clearfix'>";
                    renderHtml += "<span class='title'>" + productResultOutput + "</span>";
                    renderHtml += "</span>";
                    renderHtml += "</a>";
                    return $("<li class='product'>").data("item.autocomplete", item).append(renderHtml).appendTo(ul);
                } else if (item.type == "articleResult") {
                	var articleResultOutput = String(item.value).replace(new RegExp(this.term, "gi"),"<span class='ui-state-highlight'>$&</span>");
                    var renderHtml = "<a href='" + ACC.config.contextPath + item.url + "' class='product clearfix'>";
                    renderHtml += "<span class='desc clearfix'>";
                    renderHtml += "<span class='title'>" + articleResultOutput + "</span>";
                    return $("<li class='product'>").data("item.autocomplete", item).append(renderHtml).appendTo(ul);
                } else if (item.type == "seeAllLink") {
                    var renderHtml = "<a class='seeAllButton clearfix' data='" + item.term + "' >";
                    renderHtml += "<span class='desc clearfix'>";
                    renderHtml += "<span class='title'>SEE ALL RESULTS</span>";
                    renderHtml += "</span>";
                    renderHtml += "</a>";
                    return $("<li class='seeAll'>").data("item.autocomplete", item).append(renderHtml).appendTo(ul);
                }
            },
            source: function(request, response) {
                var self = this;
                var term = request.term.toLowerCase();
                if (term in self.options.cache) {
                    return response(self.options.cache[term]);
                }
                $.getJSON(self.options.autocompleteUrl, {
                    term: request.term
                }, function(data) {
                    var autoSearchData = [];
                    if (data.suggestions != null) {
                        $.each(data.suggestions, function(i, obj) {
                            autoSearchData.push({
                                value: obj.term,
                                url: ACC.config.contextPath + "/search?text=" + obj.term,
                                type: "autoSuggestion"
                            });
                        });
                    }
                    if (data.products != null) { 
                        $.each(data.products, function(i, obj) {
                        	var val = (obj.brand === typeof 'undefined')?obj.sizeVariantTitle:obj.brand +" "+obj.sizeVariantTitle;
                            autoSearchData.push({
                                value: val,
                                code: obj.code,
                                desc: obj.description,
                                manufacturer: obj.manufacturer,
                                url: ACC.config.contextPath + obj.url + "?ss=" + request.term,
                                type: "productResult",
                                image: (obj.images != null && self.options.displayProductImages) ? obj.images[0].url : null
                            });
                        });
                    }
                    if (data.articles != null) {
                        $.each(data.articles, function(i, obj) {
                            var url;
                            if (obj.articleType == "MAGAZINES") {
                                url = ACC.config.contextPath + "/clubcard-magazine/article-view/" + obj.articleTitle + "?ss=" + request.term;
                            } else if (obj.articleType == "HEALTH") {
                                url = ACC.config.contextPath + "/health/article-view/" + obj.articleTitle + "?ss=" + request.term;
                            } else if (obj.articleType == "CONDITIONS") {
                                url = ACC.config.contextPath + "/health/conditions/article-view/" + obj.articleTitle + "?ss=" + request.term;
                            } else if (obj.articleType == "MEDICINES") {
                                url = ACC.config.contextPath + "/health/medicines/article-view/" + obj.articleTitle + "?ss=" + request.term;
                            } else if (obj.articleType == "VITAMINS") {
                                url = ACC.config.contextPath + "/health/supplements/article-view/" + obj.articleTitle + "?ss=" + request.term;
                            }
                            autoSearchData.push({
                                title: obj.articleTitle,
                                type: "articleResult",
                                url: url,
                                articleType: obj.articleType,
                                image: (obj.articleImage != null) ? obj.articleImage : null,
                                value: obj.articleTitle
                            });
                        });
                    }
                    autoSearchData.push({
                        type: "seeAllLink",
                        term: request.term
                    })
                    self.options.cache[term] = autoSearchData;
                    return response(autoSearchData);
                });
            }
        });
        $search = $(".siteSearchInput");
        if ($search.length > 0) {
            $search.yautocomplete()
        }
    }
};
$(document).ready(function() {
    ACC.autocomplete.bindAll();
});
