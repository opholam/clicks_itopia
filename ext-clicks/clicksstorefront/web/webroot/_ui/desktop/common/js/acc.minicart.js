ACC.minicart = {
	$layer : $('#miniCartLayer'),
	$moblayer : $('#mobminiCartLayer'),
	bindMiniCart : function() {
		$(document).on('mouseenter', '.mini-basket-icon', function(){
			ACC.minicart.showMiniCart();
			ACC.cartremoveitem.bindAll();
			$(".dropdown-list.mini-basket").slideDown(function(){
				if($(window).width() >= 1024) {
					$("#div-overlay").css({"display": "block", "position": "fixed"});
				}else {
					$("#menu-overlay").css({"display": "block"});
				}
				
				$(this).parents("li").addClass("open").siblings("li").removeClass("open");
				$(this).hover(function(){
					$(this).css("display","block");
					ACC.cartremoveitem.bindAll();
					$("#div-overlay").css({"display": "block", "position": "fixed"});
				}, function(){
					$(".mini-basket").slideUp();
					if($(window).width() >= 1024) {
						$("#div-overlay").css({"display": "none", "position": "absolute"});
					}else {
						$("#menu-overlay").css({"display": "none"});
					}
					$(this).parents("li").removeClass("open");
				});
			});			
		});
		$('.basket').bind('click', function(){
			ACC.minicart.showMiniCart();				
		});
	},
	showMiniCart : function() {
		if (ACC.minicart.$layer.data("needRefresh") != false) {
			ACC.minicart.getMiniCartData(function() {
				ACC.minicart.$layer.fadeIn(function() {
					ACC.minicart.$layer.data("hover", true);
					ACC.minicart.$layer.data("needRefresh", false);
					ACC.cartremoveitem.bindAll();
				});
			})
		}
		ACC.minicart.$layer.fadeIn(function() {
			ACC.minicart.$layer.data("hover", true);
		});
	},
	hideMiniCart : function() {
		ACC.minicart.$layer.fadeOut(function() {
			ACC.minicart.$layer.data("hover", false);
		});
	},
	getMiniCartData : function(callback) {
		$.ajax({
			url : ACC.minicart.$layer.attr("data-rolloverPopupUrl"),
			cache : false,
			type : 'GET',
			success : function(result) {
				ACC.minicart.$layer.html(result);
				ACC.minicart.$moblayer.html(result);
				if(($(window).width()) <= 768){
					$(".basket-blk .mini-basket-product-list").bxSlider({
						controls : false,
						infiniteLoop: false,
						onSliderLoad: function(){
							var screenWidth = $(window).width();
							var sliderWidth = parseInt(screenWidth - 30)
							$(".mini-basket-product-list li.popupCartItem").each(function(){
								$(this).css("width", sliderWidth);
							});
							ACC.cartremoveitem.bindAll();
						  }
					});	
				}

				callback();
			}
		});
	},
	refreshMiniCartCount : function() {
		$.ajax({
			dataType : "json",
			url : ACC.minicart.$layer.attr("data-refreshMiniCartUrl")
					+ Math.floor(Math.random() * 101) * (new Date().getTime()),
			success : function(data) {
				$(".mini_basket_head .pull-right").html(data.miniCartCount);
				$(".mini-basket-count").html(data.cartCount);
				ACC.minicart.$layer.data("needRefresh", true);
			}
		});
	}
};
$(document).ready(function() {
	ACC.minicart.bindMiniCart();
});