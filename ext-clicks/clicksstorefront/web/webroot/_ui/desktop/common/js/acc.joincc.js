$(document)
		.ready(
				function() {
					$("#section2, #section3, #section4").addClass(
							"form-disabled");
					$(
							"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
							.attr("disabled", "disabled");
					var haveSaridLength = $("#bb");
					var dateValueCheck = $(".dateCheck");
					var monthValueCheck = $(".monthCheck");
					var yearValueCheck = $(".yearCheck");
					var empty_flds = 0;
					$("#section1 input.mandatory").each(function() {
						if (!$.trim($(this).val())) {
							empty_flds++;
						}
					});
					if (((!empty_flds) && (haveSaridLength.val() != ""))
							|| ((!empty_flds) && ((dateValueCheck.val() != "")
									&& (monthValueCheck.val() != "") && (yearValueCheck
									.val() != "")))) {
						if (!$("#section1 input, #section1 select").hasClass(
								"error")) {
							$("#section2").removeClass("form-disabled");
							$("#section2 input").removeAttr("disabled");
							$("#section1 .successTick").css("display", "block");
						} else {
							$("#section2, #section3, #section4").addClass(
									"form-disabled");
							$(
									"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
									.attr("disabled", "disabled");
							$("#section1 .successTick").css("display", "none");
						}
					} else {
						$("#section2").addClass("form-disabled");
						$("#section2 input").attr("disabled", "disabled");
						$("#section1 .successTick").css("display", "none");
					}
					$("#section2 input.mandatory").each(function() {
						if (!$.trim($(this).val())) {
							empty_flds++;
						}
					});
					if (empty_flds) {
						$("#section3, #section4").addClass("form-disabled");
						$(
								"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
								.attr("disabled", "disabled");
						$("#section2 .successTick").css("display", "none");
					} else {
						if (!$("#section2 input.mandatory").hasClass("error")) {
							$("#section3").removeClass("form-disabled");
							$("#section3 input, #section3 select").removeAttr(
									"disabled");
							$("#section2 .successTick").css("display", "block");
						} else {
							$("#section3, #section4").addClass("form-disabled");
							$(
									"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
									.attr("disabled", "disabled");
							$("#section2 .successTick").css("display", "none");
						}
					}
					$("#section3 input.mandatory").each(function() {
						if (!$.trim($(this).val())) {
							empty_flds++;
						}
					});
					if (empty_flds) {
						$("#section4").addClass("form-disabled");
						$(
								"#section4 .form-disabled input, #section4 .form-disabled select, #section4 .form-disabled button")
								.attr("disabled", "disabled");
						$("#section4 .successTick").css("display", "none");
					} else {
						if (!$("#section3 input.mandatory").hasClass("error")) {
							$("#section4").removeClass("form-disabled");
							$(
									"#section4 input, #section4 select, #section4 button")
									.removeAttr("disabled");
							$("#section3 .successTick").css("display", "block");
							$("#section4 .successTick").css("display", "block");
						} else {
							$("#section4").addClass("form-disabled");
							$(
									"#section4 .form-disabled input, #section4 .form-disabled select, #section4 .form-disabled button")
									.attr("disabled", "disabled");
							$("#section3 .successTick").css("display", "none");
							$("#section4 .successTick").css("display", "none");
						}
					}
					$('#section1 input.mandatory')
							.bind(
									"change keyup blur",
									function() {
										var empty_flds = 0;
										$("#section1 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if ($("#isSAResident1").is(":checked")) {
											$("#createAccountJoinCC #bb")
													.removeAttr("disabled");
											if ((!empty_flds)
													&& (haveSaridLength.val() != "")) {
												if (!$(
														"#section1 input, #section1 select")
														.hasClass("error")) {
													$("#section2").removeClass(
															"form-disabled");
													$("#section2 input")
															.removeAttr(
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"block");
												} else {
													$(
															"#section2, #section3, #section4")
															.addClass(
																	"form-disabled");
													$(
															"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
															.attr("disabled",
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"none");
												}
											} else {
												$(
														"#section2, #section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section1 .successTick")
														.css("display", "none");
											}
										}
										if ($("#isSAResident2").is(":checked")) {
											$("#createAccountJoinCC #bb").attr(
													"disabled", "disabled")
													.removeClass("error").next(
															"label.error").css(
															"display", "none");
											$("#createAccountJoinCC #cc").css(
													"display", "block");
											if ((!empty_flds)
													&& ((dateValueCheck.val() != "")
															&& (monthValueCheck
																	.val() != "") && (yearValueCheck
															.val() != ""))) {
												if (!$(
														"#section1 input, #section1 select")
														.hasClass("error")) {
													$("#section2").removeClass(
															"form-disabled");
													$("#section2 input")
															.removeAttr(
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"block");
												} else {
													$(
															"#section2, #section3, #section4")
															.addClass(
																	"form-disabled");
													$(
															"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
															.attr("disabled",
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"none");
												}
											} else {
												$(
														"#section2, #section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section1 .successTick")
														.css("display", "none");
											}
										}
									});
					$('#bb')
							.bind(
									"change keyup blur",
									function() {
										var empty_flds = 0;
										$("#section1 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if ((!empty_flds)
												&& (haveSaridLength.val() != "")) {
											if (!$(
													"#section1 input, #section1 select")
													.hasClass("error")) {
												$("#section2").removeClass(
														"form-disabled");
												$("#section2 input")
														.removeAttr("disabled");
												$("#section1 .successTick")
														.css("display", "block");
											} else {
												$(
														"#section2, #section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section1 .successTick")
														.css("display", "none");
											}
										} else {
											$("#section2, #section3, #section4")
													.addClass("form-disabled");
											$(
													"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
													.attr("disabled",
															"disabled");
											$("#section1 .successTick").css(
													"display", "none");
										}
									});
					$("#createAccountJoinCC .saResident input[type=radio]")
							.change(
									function() {
										var empty_flds = 0;
										$("#section1 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if ($("#isSAResident1").is(":checked")) {
											$("#createAccountJoinCC #bb")
													.removeAttr("disabled");
											if ((!empty_flds)
													&& (haveSaridLength.val() != "")) {
												if (!$(
														"#section1 input, #section1 select")
														.hasClass("error")) {
													$("#section2").removeClass(
															"form-disabled");
													$("#section2 input")
															.removeAttr(
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"block");
												} else {
													$(
															"#section2, #section3, #section4")
															.addClass(
																	"form-disabled");
													$(
															"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
															.attr("disabled",
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"none");
												}
											} else {
												$(
														"#section2, #section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section1 .successTick")
														.css("display", "none");
											}
										}
										if ($("#isSAResident2").is(":checked")) {
											$("#createAccountJoinCC #bb").attr(
													"disabled", "disabled")
													.removeClass("error").next(
															"label.error").css(
															"display", "none");
											$("#createAccountJoinCC #cc").css(
													"display", "block");
											if ((!empty_flds)
													&& ((dateValueCheck.val() != "")
															&& (monthValueCheck
																	.val() != "") && (yearValueCheck
															.val() != ""))) {
												if (!$(
														"#section1 input, #section1 select")
														.hasClass("error")) {
													$("#section2").removeClass(
															"form-disabled");
													$("#section2 input")
															.removeAttr(
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"block");
												} else {
													$(
															"#section2, #section3, #section4")
															.addClass(
																	"form-disabled");
													$(
															"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
															.attr("disabled",
																	"disabled");
													$("#section1 .successTick")
															.css("display",
																	"none");
												}
											} else {
												$(
														"#section2, #section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section1 .successTick")
														.css("display", "none");
											}
										}
									});
					$(".dobSaOuterFirst select")
							.change(
									function() {
										var empty_flds = 0;
										$("#section1 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if (((dateValueCheck.val() != "")
												&& (monthValueCheck.val() != "") && (yearValueCheck
												.val() != ""))
												&& (!empty_flds)) {
											var email=document.getElementById('customer.enroll.EMailAddress').value;
											if(null!=email && email!=undefined && email!="")
											{
												var address=document.getElementById('customer.enroll.AddressLine1').value;
												if(address!=undefined && address!="" )
												{
													$("#cR-3").removeClass("form-disabled");
													$("#cR-3 input, #cR-3 select, #cR-3 button").removeAttr("disabled");
													$("#cR-3 .successTick").css("display", "block");
													$("#cR-3 .successTick").css("display", "block");
												}
												$("#cR-4").removeClass("form-disabled");
												$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
												$("#cR-3 .successTick").css("display", "block");
												$("#cR-4 .successTick").css("display", "block");
											}
											if (!$(
													"#section1 input, #section1 select")
													.hasClass("error")) {
												$("#section2").removeClass(
														"form-disabled");
												$("#section2 input")
														.removeAttr("disabled");
												$("#section1 .successTick")
														.css("display", "block");
											} else {
												$(
														"#section2, #section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section1 .successTick")
														.css("display", "none");
											}
										} else {
											$("#section2, #section3, #section4")
													.addClass("form-disabled");
											$(
													"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
													.attr("disabled",
															"disabled");
											$("#section1 .successTick").css(
													"display", "none");
										}
									});
					$('#section2 input.mandatory')
							.bind(
									"change keyup blur",
									function() {
										var empty_flds = 0;
										$("#section2 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if (empty_flds) {
											$("#section3, #section4").addClass(
													"form-disabled");
											$(
													"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
													.attr("disabled",
															"disabled");
											$("#section2 .successTick").css(
													"display", "none");
										} else {
											if (!$("#section2 input.mandatory")
													.hasClass("error")) {
												$("#section3").removeClass(
														"form-disabled");
												$(
														"#section3 input, #section3 select")
														.removeAttr("disabled");
												$("#section2 .successTick")
														.css("display", "block");
											} else {
												$("#section3, #section4")
														.addClass(
																"form-disabled");
												$(
														"#createAccountJoinCC .form-disabled input, #createAccountJoinCC .form-disabled select, #createAccountJoinCC .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section2 .successTick")
														.css("display", "none");
											}
										}
										$("#section3 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if (empty_flds) {
											$("#section4").addClass(
													"form-disabled");
											$(
													"#section4 .form-disabled input, #section4 .form-disabled select, #section4 .form-disabled button")
													.attr("disabled",
															"disabled");
											$("#section3 .successTick").css(
													"display", "none");
											$("#section4 .successTick").css(
													"display", "none");
										} else {
											if (!$("#section3 input.mandatory")
													.hasClass("error")) {
												$("#section4").removeClass(
														"form-disabled");
												$(
														"#section4 input, #section4 select, #section4 button")
														.removeAttr("disabled");
												$("#section3 .successTick")
														.css("display", "block");
												$("#section4 .successTick")
														.css("display", "block");
											} else {
												$("#section4").addClass(
														"form-disabled");
												$(
														"#section4 .form-disabled input, #section4 .form-disabled select, #section4 .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section3 .successTick")
														.css("display", "none");
												$("#section4 .successTick")
														.css("display", "none");
											}
										}
									});
					$('#section3 input.mandatory')
							.bind(
									"change keyup blur",
									function() {
										var empty_flds = 0;
										$("#section3 input.mandatory")
												.each(
														function() {
															if (!$.trim($(this)
																	.val())) {
																empty_flds++;
															}
														});
										if (empty_flds) {
											$("#section4").addClass(
													"form-disabled");
											$(
													"#section4 .form-disabled input, #section4 .form-disabled select, #section4 .form-disabled button")
													.attr("disabled",
															"disabled");
											$("#section3 .successTick").css(
													"display", "none");
											$("#section4 .successTick").css(
													"display", "none");
										} else {
											if (!$("#section3 input.mandatory")
													.hasClass("error")) {
												$("#section4").removeClass(
														"form-disabled");
												$(
														"#section4 input, #section4 select, #section4 button")
														.removeAttr("disabled");
												$("#section3 .successTick")
														.css("display", "block");
												$("#section4 .successTick")
														.css("display", "block");
											} else {
												$("#section4").addClass(
														"form-disabled");
												$(
														"#section4 .form-disabled input, #section4 .form-disabled select, #section4 .form-disabled button")
														.attr("disabled",
																"disabled");
												$("#section3 .successTick")
														.css("display", "none");
												$("#section4 .successTick")
														.css("display", "none");
											}
										}
									});
				});