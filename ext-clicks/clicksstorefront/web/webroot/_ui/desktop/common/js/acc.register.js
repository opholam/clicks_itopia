$(document).ready(function (){

	$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
	$("#createAccount .form-disabled input, #createAccount .form-disabled select, #createAccount .form-disabled button").attr("disabled","disabled");
	$('.successTick').css("display", "none");
	$(".join-club-card-inactive").addClass("dontShow");
		
	var haveValueLength = $("#haveValue");
	var haveSaidLength = $(".inputSAIDPlaceholder input");
	var dateValueCheck = $(".dateCheck");
	var monthValueCheck = $(".monthCheck");
	var yearValueCheck = $(".yearCheck");
	
	if($("#haveYes").is(":checked")){
		
		$("#secondBlock, #cR-3, #cR-4").removeClass("Show").addClass("dontShow");
		$("#haveValue").prop("disabled", false);
		$("#cR-1 .successTick").css("display", "none");
		if(((haveValueLength.val() != "") && (haveSaidLength.val() != "")) || ((haveValueLength.val() != "") && ((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")))) {
			if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
			
			
		}else {}
	}
	
	if($("#haveNo").is(":checked")){
	    $("#block1emailpwd .emailPwd").remove().clone().appendTo('#block2emailpwd');
        $("#JoinCC").prop("checked", true).css("visibility", "hidden");        
		$("#secondBlock, #cR-3, #cR-4").removeClass("dontShow").addClass("Show");		
		$("#haveValue").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
		$("#termsOuter").addClass("dontShow");
		$("#cR-1 .form-actions").removeClass("Show").addClass("dontShow");
		$(".scenarioHide input").removeClass("mandatory");
		$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
		$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
		$(".acc-col-55 .cashback-msg").addClass("form-content-dis");   
		$(".comuni-reg3").addClass("form-content-hide").removeClass("form-content-dis");
	    $(".comuni-reg4").addClass("form-content-dis").removeClass("form-content-hide");
	    $(".wo").removeClass("dontShow").addClass("Show");
	    $("#cR-1 .bg-white, #secondBlock .bg-white").removeClass("wo-h");
		if($("#saResiYes").is(":checked")){
			if((haveSaidLength.val() != "")) {				
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
					$("#secondBlock").removeClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
					$("#cR-1 .successTick").css("display", "block");
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}else {
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
			}
		}
		if($("#saResiNo").is(":checked")){
			if((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) {	
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
					$("#secondBlock").removeClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
					$("#cR-1 .successTick").css("display", "block");
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}else {
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
			}			
		}	
		$(".saResident input[type=radio]").change(function(){
			if($("#saResiYes").is(":checked")){
				$(".inputSAIDPlaceholder input").prop("disabled", false);
				dateValueCheck.val("");
				monthValueCheck.val("");
				yearValueCheck.val("");
				$(".dobSaOuterFirst select").removeClass("error").next("label.error").css("display", "none");
				$(".dobSaOuterFirst").removeClass("Show");
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
				$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
					if((haveSaidLength.val() != "")) {
						if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
							$("#secondBlock").removeClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
							$("#cR-1 .successTick").css("display", "block");
						}else {
							$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
							$("#cR-1 .successTick").css("display", "none");
						}
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				});
			}
			if($("#saResiNo").is(":checked")){
				$(".inputSAIDPlaceholder input").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
				$(".dobSaOuterFirst").addClass("Show");	
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
				$(".dobSaOuterFirst select").change(function(){
					if(((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != ""))) {
						if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
							$("#secondBlock").removeClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
							$("#cR-1 .successTick").css("display", "block");
						}else {
							$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
							$("#cR-1 .successTick").css("display", "none");
						}
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				});
			}
		});
		
		if($("#JoinCC").is(":checked")){			
			$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("dontShow").addClass("Show");
			$(".join-club-card-inactive").removeClass("Show").addClass("dontShow");
			$(".scenarioHide input").addClass("mandatory");
		}else{			
			$(".scenarioHide input").removeClass("mandatory");
			$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
			$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
		}
		
		var email=document.getElementById('customer.enroll.EMailAddress').value;
		var number=document.getElementById('customer.enroll.cell.Number').value;
		if(null!=email && email!=undefined && email!="" && null!=number && number!=undefined && number!="")
		{
			$("#cR-3").removeClass("form-disabled");
			$("#cR-3 input, #cR-3 select, #cR-3 button").removeAttr("disabled");
			$("#cR-3 .successTick").css("display", "block");
			$("#cR-3 .successTick").css("display", "block");
			var postCode=document.getElementById('customer.enroll.PostalCode').value;
			var address=document.getElementById('customer.enroll.AddressLine1').value;
			if(null!=address && address!=undefined && address!="" && null!=postCode && postCode!="" && postCode!=undefined)
			{
				$("#cR-4").removeClass("form-disabled");
				$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
				$("#cR-3 .successTick").css("display", "block");
				$("#cR-4 .successTick").css("display", "block");
			}
		}
	}
	if($("#withoutcc").is(":checked")){	
		
        $("#block1emailpwd .emailPwd").remove().clone().appendTo('#block2emailpwd');
        $("#JoinCC").prop("checked", true).css("visibility", "hidden");
		$("#secondBlock, #cR-4").removeClass("dontShow").addClass("Show");
		$("#cR-3").removeClass("Show").addClass("dontShow");
		$("#haveValue").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
		$("#termsOuter").addClass("dontShow");
		$("#cR-1 .form-actions").removeClass("Show").addClass("dontShow");
		$(".scenarioHide input").removeClass("mandatory");
		$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
		$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
        $(".comuni-reg4, .cashback-msg").addClass("form-content-hide").removeClass("form-content-dis"); 
        $(".acc-col-55 .cashback-msg").addClass("form-content-hide");
        $(".comuni-reg4").addClass("form-content-hide").removeClass("form-content-dis");
        $(".comuni-reg3").addClass("form-content-dis").removeClass("form-content-hide");  
        $(".wo").removeClass("Show").addClass("dontShow");
        $("#cR-1 .bg-white, #secondBlock .bg-white").addClass("wo-h");
        
		if($("#saResiYes").is(":checked")){
			if((haveSaidLength.val() != "")) {					
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
					$("#secondBlock").removeClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
					$("#cR-1 .successTick").css("display", "block");
					
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}else {
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");				
			}
		}
		if($("#saResiNo").is(":checked")){
			if((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) {					
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
					$("#secondBlock").removeClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
					$("#cR-1 .successTick").css("display", "block");
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}else {
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
			}			
		}	
		$(".saResident input[type=radio]").change(function(){			
			if($("#saResiYes").is(":checked")){
				$(".inputSAIDPlaceholder input").prop("disabled", false);
				dateValueCheck.val("");
				monthValueCheck.val("");
				yearValueCheck.val("");
				$(".dobSaOuterFirst select").removeClass("error").next("label.error").css("display", "none");
				$(".dobSaOuterFirst").removeClass("Show");
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
				$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
					if((haveSaidLength.val() != "")) {
						if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
							$("#secondBlock").removeClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
							$("#cR-1 .successTick").css("display", "block");
						}else {
							$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
							$("#cR-1 .successTick").css("display", "none");
						}
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				});
			}
			if($("#saResiNo").is(":checked")){
				$(".inputSAIDPlaceholder input").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
				$(".dobSaOuterFirst").addClass("Show");	
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
				$(".dobSaOuterFirst select").change(function(){
					if(((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != ""))) {
						if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
							$("#secondBlock").removeClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
							$("#cR-1 .successTick").css("display", "block");
						}else {
							$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
							$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
							$("#cR-1 .successTick").css("display", "none");
						}
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				});
			}
		});
		
		if($("#JoinCC").is(":checked")){			
			$("#cR-4, .scenarioHide, .scenarioHide-2").removeClass("dontShow").addClass("Show");
			$("#cR-3").removeClass("Show").addClass("dontShow");
			$(".join-club-card-inactive").removeClass("Show").addClass("dontShow");
			$(".scenarioHide input").addClass("mandatory");
			
		}else{			
			$(".scenarioHide input").removeClass("mandatory");
			$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
			$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
			
		}
	}
	if($("#saResiYes").is(":checked")){
		
		$(".inputSAIDPlaceholder input").prop("disabled", false);		
		$(".dobSaOuterFirst").removeClass("Show");
	}
	if($("#saResiNo").is(":checked")){
		
		$(".inputSAIDPlaceholder input").prop("disabled", true);
		$(".dobSaOuterFirst").addClass("Show");			
	}	
	var empty_flds = 0;
	$("#secondBlock input.mandatory").each(function() {
		
		if(!$.trim($(this).val())) {
			empty_flds++;			
		}    
	});

	if (empty_flds) {		
		
		$("#secondBlock .successTick").css("display", "none");
		$("#cR-3, #cR-4").addClass("form-disabled");
		$("#cR-3 .form-disabled input, #cR-3 .form-disabled select, #cR-3 .form-disabled button, #cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
	} else {
		
		
		
			    $("#secondBlock .successTick").css("display", "block");	
				$("#cR-3").removeClass("form-disabled");
				$("#cR-3 input, #cR-3 select, #cR-3 button").removeAttr("disabled");
		}		
	
	
	$("#cR-3 input.mandatory").each(function() {
		
		if(!$.trim($(this).val())) {
			empty_flds++;
			
		}    
	});

	if (empty_flds) {	
		
		$("#cR-3 .successTick, #cR-4 .successTick").css("display", "none");
		$("#cR-4").addClass("form-disabled");
		$("#cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
	} else {
		
		$("#cR-3 .successTick, #cR-4 .successTick").css("display", "block");
		$("#cR-4").removeClass("form-disabled");
		$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
	}
	
	$(".haveClubcard input[type=radio]").change(function(){
		
		if($("#haveYes").is(":checked")){
			$("#block2emailpwd .emailPwd").remove().clone().appendTo('#block1emailpwd');
			$("#block1emailpwd input").removeAttr("disabled");
			$("#secondBlock, #cR-3, #cR-4").removeClass("Show").addClass("dontShow");
			$("#cR-1 .successTick").css("display", "none");
				if($("#JoinCC").is(":checked")){
					$("#JoinCC").removeAttr("checked");
					$("#secondBlock, #cR-3, #cR-4").removeClass("Show").addClass("dontShow");
				}
			$("#haveValue").prop("disabled", false);
			$("#termsOuter").removeClass("Show");
			$("#cR-1 .form-actions").removeClass("dontShow").addClass("Show");			
			$(".scenarioHide input").addClass("mandatory");
			$(".join-club-card-inactive").removeClass("Show").addClass("dontShow");
			if(((haveValueLength.val() != "") && (haveSaidLength.val() != "")) || ((haveValueLength.val() != "") && ((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")))) {
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
				
				
			}else {}
			
			
			if($("#saResiYes").is(":checked")){
				if((haveValueLength.val() != "") && (haveSaidLength.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
					
					
				}else {}
				
				
			}
			$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
				if((haveValueLength.val() != "") && (haveSaidLength.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
					
					
				}else {}
				
				
			});
			$(".dobSaOuterFirst select").change(function(){
				if(((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) && (haveValueLength.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
					
				}else {}
				
				
			});
		}
		if($("#haveNo").is(":checked")){
			$("#block1emailpwd .emailPwd").remove().clone().appendTo('#block2emailpwd');
			$("#JoinCC").prop("checked", true).css("visibility", "hidden");
			$("#cR-1 input[name='eMailAddress'], #cR-1 input[type='password']").val("").removeClass("error").next("label.error").css("display", "none");
			$("#secondBlock, #cR-3, #cR-4").removeClass("dontShow").addClass("Show");			
			$("#haveValue").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
			$("#termsOuter").addClass("dontShow");
			$("#cR-1 .form-actions").removeClass("Show").addClass("dontShow");
			$(".scenarioHide input").removeClass("mandatory");
			$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
			$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
			$(".acc-col-55 .cashback-msg").addClass("form-content-dis");
			$(".comuni-reg3").addClass("form-content-hide").removeClass("form-content-dis");
		    $(".comuni-reg4").addClass("form-content-dis").removeClass("form-content-hide"); 
		    $(".wo").removeClass("dontShow").addClass("Show");
		    $("#cR-1 .bg-white, #secondBlock .bg-white").removeClass("wo-h");
			if($("#saResiYes").is(":checked")){
				if((haveSaidLength.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}
			if($("#JoinCC").is(":checked")){
				$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("dontShow").addClass("Show");
				$(".join-club-card-inactive").removeClass("Show").addClass("dontShow");
			}else{
				$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
				$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
			}
			$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
				if((haveSaidLength.val() != "")) {					
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {
						
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {					
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			});	
			$(".dobSaOuterFirst select").change(function(){
				if((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) {					
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){						
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {						
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			});
			if((haveSaidLength.val() != "")) {
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){				
					$("#secondBlock").removeClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
					$("#cR-1 .successTick").css("display", "block");
				}else {
					
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}else {
			
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
			}
			if($("#saResiNo").is(":checked")){
				
				if((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) {
					
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {
						
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
					
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}
			var email=document.getElementById('customer.enroll.EMailAddress').value;
			var number=document.getElementById('customer.enroll.cell.Number').value;
			if(null!=email && email!=undefined && email!="" && null!=number && number!=undefined && number!="")
			{
				$("#cR-3").removeClass("form-disabled");
				$("#cR-3 input, #cR-3 select, #cR-3 button").removeAttr("disabled");
				$("#cR-3 .successTick").css("display", "block");
				$("#cR-3 .successTick").css("display", "block");
				var postCode=document.getElementById('customer.enroll.PostalCode').value;
				var address=document.getElementById('customer.enroll.AddressLine1').value;
				if(null!=address && address!=undefined && address!="" && null!=postCode && postCode!="" && postCode!=undefined)
				{
					$("#cR-4").removeClass("form-disabled");
					$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
					$("#cR-3 .successTick").css("display", "block");
					$("#cR-4 .successTick").css("display", "block");
				}
			}
		}
		if($("#withoutcc").is(":checked")){			
			$("#block1emailpwd .emailPwd").remove().clone().appendTo('#block2emailpwd');
			$("#JoinCC").prop("checked", true).css("visibility", "hidden");
			$("#cR-1 input[name='eMailAddress'], #cR-1 input[type='password']").val("").removeClass("error").next("label.error").css("display", "none");
			$("#secondBlock,#cR-4").removeClass("dontShow").addClass("Show");
			$("#cR-3").removeClass("Show").addClass("dontShow");
            $(".comuni-reg3").addClass("form-content-dis"); 
            $(".comuni-reg4, .cashback-msg").addClass("form-content-hide").removeClass("form-content-dis"); 
            $(".acc-col-55 .cashback-msg").addClass("form-content-hide");
			$("#haveValue").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
			$("#termsOuter").addClass("dontShow");
			$("#cR-1 .form-actions").removeClass("Show").addClass("dontShow");
			$(".scenarioHide input").removeClass("mandatory");
			$("#cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
			$("#cR-3").removeClass("Show").addClass("dontShow");
			$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
			$(".wo").removeClass("Show").addClass("dontShow");
			$("#cR-1 .bg-white, #secondBlock .bg-white").addClass("wo-h");
			if($("#saResiYes").is(":checked")){
				if((haveSaidLength.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {
						
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
										
					$("#secondBlock, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}
			if($("#JoinCC").is(":checked")){
					
				$("#cR-4, .scenarioHide, .scenarioHide-2").removeClass("dontShow").addClass("Show");
				$("#cR-3").removeClass("Show").addClass("dontShow");
				$(".join-club-card-inactive").removeClass("Show").addClass("dontShow");
			}else{
				$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");
				$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");
				
			}
			$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
				if((haveSaidLength.val() != "")) {
					
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
						
					}else {
						
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
					
					$("#secondBlock, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			});	
			$(".dobSaOuterFirst select").change(function(){
				if((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			});
			if((haveSaidLength.val() != "")) {
				if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
					$("#secondBlock").removeClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
					$("#cR-1 .successTick").css("display", "block");
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}else {
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
			}
			if($("#saResiNo").is(":checked")){
				if((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) {
					if(!$("#cR-1 input, #cR-1 select").hasClass("error")){
						$("#secondBlock").removeClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
						$("#cR-1 .successTick").css("display", "block");
					}else {
						$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
						$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
						$("#cR-1 .successTick").css("display", "none");
					}
				}else {
					$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
					$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
					$("#cR-1 .successTick").css("display", "none");
				}
			}
			var email=document.getElementById('customer.enroll.EMailAddress').value;
			var number=document.getElementById('customer.enroll.cell.Number').value;
			if(null!=email && email!=undefined && email!="" && null!=number && number!=undefined && number!="")
			{
				$("#secondBlock").removeClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button").removeAttr("disabled");
				//$("#cR-3 .successTick").css("display", "block");
				$("#secondBlock .successTick").css("display", "block");
				
				$("#cR-4").removeClass("form-disabled");
				$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
				$("#cR-3 .successTick").css("display", "block");
				$("#cR-4 .successTick").css("display", "block");
			}
		}
	});
	
	$(".saResident input[type=radio]").change(function(){
		if($("#saResiYes").is(":checked")){
			$(".inputSAIDPlaceholder input").prop("disabled", false);
			dateValueCheck.val("");
			monthValueCheck.val("");
			yearValueCheck.val("");
			$(".dobSaOuterFirst select").removeClass("error").next("label.error").css("display", "none");
			$(".dobSaOuterFirst").removeClass("Show");
			$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
			$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
			$("#cR-1 .successTick").css("display", "none");
			if($("#haveYes").is(":checked")){
				$("#cR-1 .successTick").css("display", "none");
			}
			$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
				$("#cR-1 .successTick").css("display", "none");
			});
			$(".dobSaOuterFirst select").change(function(){
				$("#cR-1 .successTick").css("display", "none");
			});
		}
		if($("#saResiNo").is(":checked")){
			$(".inputSAIDPlaceholder input").val("").prop("disabled", true).removeClass("error").next("label.error").css("display", "none");
			$(".dobSaOuterFirst").addClass("Show");	
			$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
			$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
			$("#cR-1 .successTick").css("display", "none");
		}
	});/*$("#JoinCC").change(function(){if($("#JoinCC").is(":checked")){$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("dontShow").addClass("Show");$(".join-club-card-inactive").removeClass("Show").addClass("dontShow");$(".scenarioHide input").addClass("mandatory");}else{$(".scenarioHide input").removeClass("mandatory");$("#cR-3, #cR-4, .scenarioHide, .scenarioHide-2").removeClass("Show").addClass("dontShow");$(".join-club-card-inactive").removeClass("dontShow").addClass("Show");}});*/
	
	
	$("#haveValue").bind("change keyup blur",function(){
		if(((haveValueLength.val() != "") && (haveSaidLength.val() != "")) || ((haveValueLength.val() != "") && ((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")))) {
			if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
			
		}else {}
		
		
	});
	
	$(".inputSAIDPlaceholder input").bind("change keyup blur",function(){
		if((haveValueLength.val() != "") && (haveSaidLength.val() != "")) {
			if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {
				
				
				$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
				$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
				$("#cR-1 .successTick").css("display", "none");
			}
		}else {}
		
		
	});
	$(".dobSaOuterFirst select").change(function(){
		if(((dateValueCheck.val() != "") && (monthValueCheck.val() != "") && (yearValueCheck.val() != "")) && (haveValueLength.val() != "")) {
			if(!$("#cR-1 input, #cR-1 select").hasClass("error")){}else {}
			
			
		}else {
			$("#secondBlock, #cR-3, #cR-4").addClass("form-disabled");
			$("#secondBlock input, #secondBlock select, #secondBlock button, #cR-3 input, #cR-3 select, #cR-3 button, #cR-4 input, #cR-4 select, #cR-4 button").attr("disabled", "disabled");
			$("#cR-1 .successTick").css("display", "none");
		}
	});
	
	$('#secondBlock input.mandatory').bind("change keyup blur",function() {
		
		var empty_flds = 0;
		$("#secondBlock input.mandatory").each(function() {
			
			if(!$.trim($(this).val())) {
				empty_flds++;
			}    
		});

		if (empty_flds) {
		
			$("#cR-3, #cR-4").addClass("form-disabled");
			$("#cR-3 .form-disabled input, #cR-3 .form-disabled select, #cR-3 .form-disabled button, #cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
			$("#secondBlock .successTick").css("display", "none");			
		} else {
			if(!$("#secondBlock input.mandatory").hasClass("error")){
				/*No, I would like to create an online account without a ClubCard*/

			//	var email=document.getElementById('customer.enroll.EMailAddress').value;
				if($("#withoutcc").is(":checked")){	
					$("#secondBlock .successTick").css("display", "block");
					$("#cR-4").removeClass("form-disabled");
					$("#cR-4 input, #cR-3 select, #cR-3 button").removeAttr("disabled");			
					$("#cR-4 input.mandatory").each(function() {
					if(!$.trim($(this).val())) {
						empty_flds++;
					}    
					});

					if (empty_flds) {						
					$("#cR-3").addClass("form-disabled");
					$("#cR-3 .form-disabled input, #cR-3 .form-disabled select, #cR-3 .form-disabled button").attr("disabled","disabled");
					$("#cR-4 .successTick").css("display", "none");
					$("#cR-3 .successTick").css("display", "none");
					} else {
						
						$("#cR-4").removeClass("form-disabled");
						$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
						$("#cR-3 .successTick").css("display", "block");
						$("#cR-4 .successTick").css("display", "block");
						
						
				/*	if(!$("#cR-3 input.mandatory").hasClass("error")){
						$("#cR-4").removeClass("form-disabled");
						$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
						$("#cR-3 .successTick").css("display", "block");
						$("#cR-4 .successTick").css("display", "block");
						console.log("withoutcc-655-3");	
					}else {
						$("#cR-4").addClass("form-disabled");
						$("#cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
						$("#cR-3 .successTick").css("display", "none");
						$("#cR-4 .successTick").css("display", "none");
						console.log("withoutcc-655-2");	
					}*/
					}
					
				}else{
					
					/*No, I would like to join ClubCard*/
					
					$("#secondBlock .successTick").css("display", "block");
					$("#cR-3").removeClass("form-disabled");
					$("#cR-3 input, #cR-3 select, #cR-3 button").removeAttr("disabled");			
					$("#cR-3 input.mandatory").each(function() {
					if(!$.trim($(this).val())) {
						empty_flds++;
					}    
					});

					if (empty_flds) { 
					$("#cR-4").addClass("form-disabled");
					$("#cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
					$("#cR-3 .successTick").css("display", "none");
					$("#cR-4 .successTick").css("display", "none");
					} else {
					if(!$("#cR-3 input.mandatory").hasClass("error")){
						$("#cR-4").removeClass("form-disabled");
						$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
						$("#cR-3 .successTick").css("display", "block");
						$("#cR-4 .successTick").css("display", "block");
					}else {
						$("#cR-4").addClass("form-disabled");
						$("#cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
						$("#cR-3 .successTick").css("display", "none");
						$("#cR-4 .successTick").css("display", "none");
					}
					}
					
				}	
			}else {
				$("#cR-3, #cR-4").addClass("form-disabled");
				$("#cR-3 .form-disabled input, #cR-3 .form-disabled select, #cR-3 .form-disabled button, #cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
				$("#secondBlock .successTick").css("display", "none");
			}
			
		}
	});
	
	$('#cR-3 input.mandatory').bind("change keyup blur",function() {
		
		var empty_flds = 0;
		$("#cR-3 input.mandatory").each(function() {
			if(!$.trim($(this).val())) {
				empty_flds++;
			}    
		});

		if (empty_flds) { 
			
			$("#cR-4").addClass("form-disabled");
			$("#cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
			$("#cR-3 .successTick").css("display", "none");
			$("#cR-4 .successTick").css("display", "none");
		} else {
			if(!$("#cR-3 input.mandatory").hasClass("error")){
				
				$("#cR-4").removeClass("form-disabled");
				$("#cR-4 input, #cR-4 select, #cR-4 button").removeAttr("disabled");
				$("#cR-3 .successTick").css("display", "block");
				$("#cR-4 .successTick").css("display", "block");
			}else {
				
				$("#cR-4").addClass("form-disabled");
				$("#cR-4 .form-disabled input, #cR-4 .form-disabled select, #cR-4 .form-disabled button").attr("disabled","disabled");
				$("#cR-3 .successTick").css("display", "none");
				$("#cR-4 .successTick").css("display", "none");
			}			
		}
	});
		
});