ACC.product = {
    initQuickviewLightbox: function() {
        this.enableAddToCartButton();
        this.bindToAddToCartForm();
    },
    enableAddToCartButton: function() {
        $('.addToCartButtonSubmit').removeAttr("disabled");
    },
    bindToAddToCartForm: function() {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
            success: ACC.product.displayAddToCartPopup
                //,
                //complete : ACC.product.displayAddToCartPopup,
        });
    },
    bindToAddToCartStorePickUpForm: function() {
        var addToCartStorePickUpForm = $('#pickup_store_results .add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({
            success: ACC.product.displayAddToCartPopup
        });
    },
    loginFromPopup: function() {
        if ($(window).width() > 768) {
            var mobParam = ".login ";
        } else {
            var mobParam = ".login-blk ";
        }
        $(mobParam + "#login_form")
            .validate({
                rules: {
                    j_username: {
                        required: true
                    },
                    j_password: {
                        required: true
                    },
                },
                onfocusout: function(element) {
                    $(element).valid();
                },
                messages: {
                    j_username: "Please enter the email address you used to register for this site",
                    j_password: "Please enter your password",
                },
                submitHandler: function(form) {
                    $("#login_form button span.loader").remove();
                    $("#login_form button").append("<span class='loader'></span>");
                    var path = $(mobParam + '#login_form').attr('action');
                    var datastring = $(mobParam + "#login_form").serialize();
                    $.ajax({
                            type: "POST",
                            url: path,
                            data: datastring,
                            success: function(res) {
                                if (res.indexOf("username") > -1) {
                                    $(mobParam + " .error_msg").show().html("Either username or password is wrong");
                                } else {
                                    window.location.reload();
                                    $("#login_form button span.loader").remove();
                                }
                                $("#login_form button span.loader").remove();
                            },
                            error: function() {
                             window.location.reload();
                            	}
                        });
                }
            });
    },
    displayLoginPopupForm: function(data) {
        alert("login popup" + data);
    },
    enableStorePickupButton: function() {
        $('.pickupInStoreButton').removeAttr("disabled");
    },
 
    displayAddToCartPopup: function(cartResult, statusText, xhr, formElement) {
        //var reload=0;
        //alert(count);
        $('#addToCartLayer').remove();
        if (typeof ACC.minicart.refreshMiniCartCount == 'function') {
            ACC.minicart.refreshMiniCartCount();
        }
        $.colorbox({
            html: cartResult.addToCartLayer,
            maxWidth: "100%",
            opacity: 0.7,
            width: 500,
            scrolling: false,
            closeButton: true,
            onComplete: function() {
                    $(this).colorbox.resize();
                    ACC.clicks.basketQuantity();
            }
        });

        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();
        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }
        ACC.track.trackAddToCart(productCode, quantity, cartResult.cartData);
    },
    bindPromotionFacetToggles: function() {
        $(".promotionFacet").click(function(e) {
            e.preventDefault();
            $(this).closest("li").toggleClass("facetActive");
            var facetKey = '';
            var facet = '';
            $(".promofacet").each(function(index) {
                facet = '';
                $(this).find('.facetActive a').each(function() {
                    if ('' == facet) {
                        facet = $(this).attr('id');
                    } else {
                        facet = facet + ',' + $(this).attr('id');
                    }
                });
                if (facet != '') {
                    if ('' == facetKey) {
                        facetKey = $(this).attr("id");
                    } else {
                        facetKey = facetKey + "&" + $(this).attr("id");
                    }
                    facetKey = facetKey + '=' + facet;
                }
            });
            var uri = $(this).attr('href');
            var clean_uri = uri.substring(0, uri.indexOf("?"));
            if (facetKey != '') {
                window.location = (clean_uri + '?' + facetKey);
            } else {
                window.location = clean_uri;
            }
        });
        $(".promosort").change(function(e) {
            window.location = updateURLParameter(window.location.href, "sort", $(this).val());
        });
        $(".promoloadcount").change(function(e) {
            var url = updateURLParameter(window.location.href, "size", $(this).val());
            window.location = updateURLParameter(url, "showMode", $(this).val());
        });
        $(".btn-load-promotion").click(function(e) {
            e.preventDefault();
            //pageCount = parseInt($(".promoloadcount").val())+parseInt($("#currentPageSize").val());
            pageCount = parseInt($(".promoloadcount").val());
            pageNumber = parseInt("1") + parseInt($("#pageNumber").val());
            if (pageCount == 0 || pageCount == '') {
                pageCount = 12;
            }
            var promUrl = window.location.href;
            promUrl = updateURLParameter(promUrl, "size", pageCount);
            promUrl = updateURLParameter(promUrl, "page", pageNumber);
            promUrl = updateURLParameter(promUrl, "showMode", $(".promoloadcount").val());
            $.ajax({
                type: "GET",
                url: promUrl,
                success: function(response) {
                    $(".promoPage").append(response);
                },
                error: function() {
                    //
                }
            });
        });
    }
};
$(document).ready(function() {
    with(ACC.product) {
        bindToAddToCartForm();
        bindToAddToCartStorePickUpForm();
        enableStorePickupButton();
        enableAddToCartButton();
        bindPromotionFacetToggles();
    }
});

function updateURLParameter(url, param, paramVal) {
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] != param) {
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}