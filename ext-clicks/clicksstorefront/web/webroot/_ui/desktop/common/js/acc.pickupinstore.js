ACC.pickupinstore = {
	bindAll : function() {
		with (ACC.pickupinstore) {
			bindPickupInStoreClick();
			bindPickupInStoreSearch();
			bindPickupHereInStoreButtonClick();
			bindPaginateStoreResultsButtons();
			bindPickupRadioButtonSelection();
			bindFindPickupStoresNearMeSearch();
			bindLoadingSpinnerOnClick('.add_to_cart_storepickup_form');
			bindLoadingSpinnerOnClick('.select_store_form');
		}
	},
	bindPickupInStoreClick : function() {
		$(document).on('click','.customerSubscribeBtn,.subscribeEmailBtn',function(event) {
			 var contextPath = $("#contextPath").val();
			 var productCode =  document.getElementById('product-code').value;
			 var url =contextPath+'/store-pickup/'+productCode+'/notifyCustomer';
			 if(null!=document.getElementById('storelocator-q'))
			 {
			 var code =  document.getElementById('storelocator-q').value;
			 }
			 var email =  document.getElementById('customer-email').value;
			 var storeName = $(this).attr('id')
			 var option = '';
			 $.ajax({
				 	url: url,
		            async: false,
		            type: 'POST',
		            dataType: 'json',
		            data: {
		            	locationQuery: code,
		            	productCode:productCode,
		            	email:email,
		            	storeName:storeName
		            },
		            success: function(data) {
		            	if (data != null ) {
		            		if(data.email=== undefined || data.email == null || typeof data.email == 'undefined' || data.email=='' || !data.flag)
	                		{
		            			option='<div class="notifyMessage"><div class="alert negative">'+data.message+'</div></div>';
	                    		$('#custSubscribeDiv'+data.storeName+'').append(option);
	                    		//$('.outstock-email').append(option);
	            			}
		            		else{
		            			option='<div class="notifyMessage"><div class="alert positive">'+data.message+'</div></div>';
	                    		//alert('#custSubscribeDiv'+data.storeName+'');
		            			$('#custSubscribeDiv'+data.storeName+'').append(option);
		            			//$('.outstock-email').append(option);
		            		}
		            	}
		            },
           })
		 });
		$(document).on('click','.stockSubscribeBtn',function(event) {
			 var contextPath = $("#contextPath").val();
			 var productCode =  document.getElementById('product-code').value;
			 var url =contextPath+'/store-pickup/'+productCode+'/notifyCustomer';
			 if(null!=document.getElementById('storelocator-q'))
			 {
			 var code =  document.getElementById('storelocator-q').value;
			 }
			 var email =  document.getElementById('customer-email').value;
			 var storeName = $(this).attr('id')
			 var option = '';
			 $.ajax({
				 	url: url,
		            async: false,
		            type: 'POST',
		            dataType: 'json',
		            data: {
		            	locationQuery: code,
		            	productCode:productCode,
		            	email:email,
		            	storeName:storeName
		            },
		            success: function(data) {
		            	if (data != null ) {
		            		//alert(data.email);
		            		if(data.email=== undefined || data.email == null || typeof data.email == 'undefined' || data.email=='' || !data.flag)
	                		{
		            			option='<div class="notifyMessage"><div class="alert negative">'+data.message+'</div></div>';
	                    		//$('#custSubscribeDiv'+data.storeName+'').append(option);
	                    		$('.outstock-email').append(option);
	            			}
		            		else{
		            			option='<div class="notifyMessage"><div class="alert positive">'+data.message+'</div></div>';
	                    		//alert('#custSubscribeDiv'+data.storeName+'');
		            			//$('#custSubscribeDiv'+data.storeName+'').append(option);
		            			$('.outstock-email').append(option);
		            		}
		            	}
		            },
          })
		 });
		$(document).on('click','.pickupInStoreButton',function(event) {
							event.preventDefault();
							var ele = $(this);
							var productId = "popup_store_pickup_form_"	+ $(this).attr('id');
							var cartItemProductPostfix = '';
							var productIdNUM = $(this).attr('id');
							productIdNUM = productIdNUM.split("_");
							productIdNUM = productIdNUM[1];
							if (productId !== null) {
								cartItemProductPostfix = '_' + productId;
							}
							var boxContent = $('#popup_store_pickup_form').clone();
							$.colorbox({
										html : boxContent,
										//height : 600,
										maxWidth:"100%",
										width:800,
										onComplete : function() {
											boxContent.show();
											$("#colorbox #popup_store_pickup_form *").each(
															function() {
																$(this).attr("id",$(this).attr(	"data-id"));
																$(this).removeAttr("data-id")
													});
											$("#colorbox #popup_store_pickup_form label[data-for=pickupQty]").attr("for",ele.attr("data-for"))
											$("#colorbox #popup_store_pickup_form label[data-for=pickupQty]").removeAttr("data-for")
											$("#colorbox  input#locationForSearch").focus()
											$("#colorbox #popup_store_pickup_form").attr("id", productId);
											$("#colorbox #" + productId+ ".thumb").html(
													ele.data("img"));
											$(
													"#colorbox #" + productId
															+ " .price").html(
													ele.data("productcart"));
											$(
													"#colorbox #" + productId
															+ " .details")
													.html(
															ele
																	.data("productname"))
											$(
													"#colorbox #"
															+ productId
															+ " form.searchPOSForm")
													.attr(
															"action",
															ele
																	.data("actionurl"))
											$(
													"#colorbox #"
															+ productId
															+ " form.searchPOSForm")
													.attr(
															"id",
															"pickup_in_store_search_form_product_"
																	+ productIdNUM)
											$(
													"#colorbox #" + productId
															+ " #pickupQty")
													.attr(
															"value",
															($('#qty').val() !== undefined ? $(
																	'#qty')
																	.val()
																	: ele
																			.data("value")));
											$(
													"#colorbox #"
															+ productId
															+ " input.entryNumber")
													.attr(
															"value",
															ele
																	.data("entrynumber"))
											$(
													"#colorbox #"
															+ productId
															+ " input#atCartPage")
													.attr(
															"value",
															ele
																	.data("cartpage"))
											ACC.pickupinstore
													.rememberlocationSearchSubmit(
															$('#atCartPage')
																	.val(),
															ele
																	.data("entrynumber"),
															ele
																	.data("actionurl"));
											
											
											

											
										}
									});
						});		
	},
	
	
	bindPickupInStoreSearch : function() {
		$(document).on(
				'click',
				'#pickupstore_search_button',
				function(e) {
					
					ACC.pickupinstore.locationSearchSubmit($(
							'#locationForSearch').val(),
							$('#atCartPage').val(), $('input.entryNumber')
									.val(), $(this).parents('form').attr(
									'action'));
					return false;
				});
		$(document).on(
				'keypress',
				'#locationForSearch',
				function(e) {
					if (e.keyCode === 13) {
						ACC.pickupinstore.locationSearchSubmit($(
								'#locationForSearch').val(), $('#atCartPage')
								.val(), $('input.entryNumber').val(), $(this)
								.parents('form').attr('action'));
						return false;
					}
				});
	},
	bindFindPickupStoresNearMeSearch : function() {
		$(document)
				.on(
						'click',
						'#find_pickupStoresNearMe_button',
						function(e) {
							e.preventDefault();
							var cartPageVal = $('#atCartPage').val();
							var entryNumber = $('input.entryNumber').val();
							var formAction = $(this).parents('form').attr(
									'action');
							navigator.geolocation
									.getCurrentPosition(
											function(position) {
												ACC.pickupinstore
														.locationSearchSubmit(
																'',
																cartPageVal,
																entryNumber,
																formAction,
																position.coords.latitude,
																position.coords.longitude);
											},
											function(error) {
												console
														.log("An error occurred... The error code and message are: "
																+ error.code
																+ "/"
																+ error.message);
											});
						});
	},
	bindPickupHereInStoreButtonClick : function() {
		$(document)
				.on(
						'click',
						'.pickup_add_to_bag_instore_button',
						function(e) {
							$(this).prev('.hiddenPickupQty').val(
									$('#pickupQty').val());
						});
		$(document).on('click', '.pickup_here_instore_button', function(e) {
			$(this).prev('.hiddenPickupQty').val($('#pickupQty').val());
			$.colorbox.close();
		});
	},
	bindLoadingSpinnerOnClick : function(form) {
		$(document).on('click', form, function(e) {
			$.colorbox.toggleLoadingOverlay();
		});
	},
	locationSearchSubmit : function(location, cartPage, entryNumber,
			productCode, latitude, longitude) {
		$.colorbox.toggleLoadingOverlay();
		$.ajax({
			url : productCode,
			data : {
				locationQuery : location,
				cartPage : cartPage,
				entryNumber : entryNumber,
				latitude : latitude,
				longitude : longitude
			},
			type : 'POST',
			success : function(response) {
				//alert(response.locationQuery);
				//if(location==null||location='')
        		//{
        			//document.getElementById('isValidLocationDiv').innerHTML="<div class='alert negative'><b>We're sorry currently we don't have any store near to the area you entered.</b></br></br>To proceed with your order, please try with a different location.</div>";
        		//}
				//alert(location);
				ACC.pickupinstore.refreshPickupInStoreColumn(response);
				$.colorbox.toggleLoadingOverlay();
				$('.pickup_store_results-item button').first().focus();	
			}
		});
	},
	rememberlocationSearchSubmit : function(cartPage, entryNumber, formUrl) {
		$.ajax({
			url : formUrl,
			data : {
				cartPage : cartPage,
				entryNumber : entryNumber
			},
			type : 'GET',
			success : function(response) {
				ACC.pickupinstore.refreshPickupInStoreColumn(response);
				$('#locationForSearch').val(searchLocation);
			}
		});
	},
	bindPaginateStoreResultsButtons : function() {
		$(document).on('click', '.searchPOSPaging button', function(e) {
			e.preventDefault();
			var data = {
				location : $('#locationForSearch').val(),
				cartPage : $('#atCartPage').val(),
				entryNumber : $('.entryNumber').val(),
				page : $(this).parent('form').find('input[name=page]').val()
			};
			var url = $(this).parent('form').attr('action');
			ACC.pickupinstore.paginateResultsSubmit(url, data);
		});
	},
	paginateResultsSubmit : function(url, data) {
		$.colorbox.toggleLoadingOverlay();
		$.ajax({
			url : url,
			data : data,
			type : 'GET',
			success : function(response) {
				ACC.pickupinstore.refreshPickupInStoreColumn(response);
				$.colorbox.toggleLoadingOverlay();
			}
		});
	},
	refreshPickupInStoreColumn : function(data) {
		$('#pickup_store_results').html(data);
		ACC.product.bindToAddToCartStorePickUpForm();
		window.scrolledArea = 0;		
		$('.searchPOSResultsList').scroll(function() {
		    var scrollTop = $('.searchPOSResultsList').offset().top,
		        divOffset = $('.searchPOSResult').eq(0).offset().top-21;
		        window.scrolledArea = (scrollTop-divOffset);		       
		});​
		$(".emailleft .emailNotifyLink").on("click", function() {
			var that = $(this);
			$(this).parent(".emailleft").find(".subscribe-pop").show();
			$(this).parent(".emailleft").find(".close-link").show();
			$(this).parent(".emailleft").addClass("emailbox-color");				
				var prevoffset = that.offset().top;
				var scrolled = $('.searchPOSResultsList').offset().top;
				window.scrolledArea = prevoffset - scrolled + window.scrolledArea;				
				$('.searchPOSResultsList').animate({scrollTop: window.scrolledArea}, 800);
					     
		});		
		
		$(".emailleft .close-link").on("click", function() {					
			$(this).parent(".emailleft").find(".subscribe-pop").hide();
			$(this).parent(".emailleft").find(".close-link").hide();						
			$(this).parent(".emailleft").removeClass("emailbox-color");						
			$(".notifyMessage").hide();					
		});
		$("#emailNotifyForm").validate({
            rules: {               
                email: {
                    required: !0,
                    email: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {               
                email: "Please enter a valid email address"                  
            }
        })
		$('.customerSubscribeBtn').click(function(){
            $("#emailNotifyForm").submit();
        });
	},
	bindPickupRadioButtonSelection : function() {
		$("form.cartEntryShippingModeForm")
				.each(
						function() {
							var formELE = $(this);
							formELE.find("input[checked]").click();
							formELE
									.find("input[name=shipMode]")
									.change(
											function() {
												if ($(this).val() == "pickUp") {
													ACC.pickupinstore
															.bindChangeToPickupinStoreTypeSelection(formELE);
												} else {
													ACC.pickupinstore
															.bindChangeToShippingTypeSelection(formELE);
												}
											})
						})
	},
	bindChangeToShippingTypeSelection : function(formELE) {
		formELE.find('.changeStoreLink').hide();
		formELE.removeClass("shipError");
		$('div#noStoreSelected').hide();
		formELE.find('input[type="radio"]').removeAttr("checked");
		formELE.find('input[type="radio"][value="ship"]').attr("checked",
				"checked");
		if (!ACC.pickupinstore.checkIfPointOfServiceIsEmpty(formELE)) {
			formELE.submit();
		}
	},
	bindChangeToPickupinStoreTypeSelection : function(formELE) {
		formELE.find('.changeStoreLink').show();
		formELE.find('input[type="radio"]').removeAttr("checked");
		formELE.find('input[type="radio"][value="pickUp"]').attr("checked",
				"checked");
	},
	validatePickupinStoreCartEntires : function() {
		var validationErrors = false;
		$("form.cartEntryShippingModeForm").each(
				function() {
					var formid = "#" + $(this).attr('id');
					if ($(formid + ' input[value=pickUp][checked]').length
							&& ACC.pickupinstore
									.checkIfPointOfServiceIsEmpty($(this))) {
						$(this).addClass("shipError");
						validationErrors = true;
					}
				});
		if (validationErrors) {
			$('div#noStoreSelected').show().focus();
			$(window).scrollTop(0);
		}
		return validationErrors;
	},
	checkIfPointOfServiceIsEmpty : function(cartEntryDeliveryModeForm) {
		return (!cartEntryDeliveryModeForm.find('.pointOfServiceName').text()
				.trim().length);
	}
};
$(document).ready(function() {
	ACC.pickupinstore.bindAll();
});