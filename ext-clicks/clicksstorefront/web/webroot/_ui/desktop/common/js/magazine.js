$(document).ready(function() {
    var appendLoadArticles = $(".appendLoadArticles").length;
    var totalNoOfArticles = $("#totalNoOfArticles").val();
    if (appendLoadArticles == totalNoOfArticles) {
        $(".load-more-articles-ajax").remove();
    }
});
$(document).on("click", ".load-more-articles-ajax", function(e) {
    e.preventDefault();
    var activeTab = $("#activeTab").val();
    var url = ACC.config.contextPath + "/clubcard-magazine/getMoreArticles?id=" + activeTab;
    var appendLoadArticles = $(".appendLoadArticles").length;
    var pageType =$("#pageTypeVal").val();
    $.ajax({
        url: url,
        type: "get",
        data: {
            initialValue: appendLoadArticles,
            pageType: pageType
        },
        success: function(e) {
            $(".active-tab-outer").append(e);
            var articlesInPage = $(".appendLoadArticles").length;
            var totalNoOfArticles = $("#totalNoOfArticles").val();
            if (articlesInPage == totalNoOfArticles) {
                $(".load-more-articles-ajax").remove();
            }
        },
        error: function(e) {
            alert("Sorry..could not Load more Articles");
        }
    })
});
$(document).ready(function() {
    var appendTravelDeals = $(".appendTravelDeals").length;
    var totalNoOfTravelDeals = $("#totalNoOfTravelDeals").val();
    if (appendTravelDeals == totalNoOfTravelDeals) {
        $(".load-more-travel-deals").remove();
    }
});
$(document).on("click", ".load-more-travel-deals", function(e) {
    e.preventDefault();
    var url = ACC.config.contextPath + "/clubcardtravel/traveldeals/loadMore";
    var appendTravelDeals = $(".appendTravelDeals").length;
    $.ajax({
        url: url,
        type: "get",
        data: {
            initialValue: appendTravelDeals
        },
        success: function(data) {
            $(".active-loadmore-tab-outer").append(data);
            var traveldealsInPage = $(".appendTravelDeals").length;
            var totalNoOfTravelDeals = $("#totalNoOfTravelDeals").val();
            if (traveldealsInPage == totalNoOfTravelDeals) {
                $(".load-more-travel-deals").remove();
            }
        },
        error: function(e) {
            alert("Sorry..could not Load more travel deals");
        }
    })
});
$('#basic').selectric().on('change', function() {
    var selectedVal = $('#basic').val();
    $('#productSortCount').val(selectedVal);
    $('.sortForm').submit();
});
$(document).on("click", ".searchProductsLoadMore", function(e) {
    e.preventDefault();
    var productSortCount = $('#productSortCount').val();
    var currentQuery = $('#currentQuery').val();
    var productsInPage = $('.plpsep').length;
    var newProductLoadCount = parseInt(productSortCount) + parseInt(productsInPage);
    var url = ACC.config.contextPath + "/search/productsLoadMore?q=" + currentQuery + "&count=" + newProductLoadCount;
    $.ajax({
        type: "get",
        url: url,
        success: function(data) {
            $("#searchProducts").html(data);
        },
        error: function(e) {
            alert(e+"Sorry..could not load more products");
        }
    });
});
$(document).on("click", ".searchLoadMoreInspirationArticles", function(e) {
    e.preventDefault();
    var textSearched = $('#textSearched').val();
    var contextPath = $('#contextPath').val();
    var size = $(".insparticlescount").length;
    var articlesSize = $('#articlesSize').val();
    var totalSize = parseInt(size) + parseInt(articlesSize);
    var url = ACC.config.contextPath + "/search/loadMore?text=" + textSearched + "&articlesSize=" + totalSize + "&isHealthArticle=" + false;
    $.ajax({
        url: url,
        type: "get",
        success: function(data) {
            $("#searchLoadMoreInspirationArticles").html(data)
        },
        error: function(data) {
            alert("sorry..could not load more Articles");
        }
    });
});
$(document).on("click", ".searchLoadMoreHealthArticles", function(e) {
    e.preventDefault();
    var textSearched = $('#textSearched').val();
    var contextPath = $('#contextPath').val();
    var size = $(".healtharticlescount").length;
    var articlesSize = $('#articlesSize').val();
    var totalSize = parseInt(size) + parseInt(articlesSize);
    var url = ACC.config.contextPath + "/search/loadMore?text=" + textSearched + "&articlesSize=" + totalSize + "&isHealthArticle=" + true;
    $.ajax({
        url: url,
        type: "get",
        success: function(data) {
            $("#searchLoadMoreHealthArticles").html(data)
        },
        error: function(data) {
            alert("sorry..could not load more Articles");
        }
    });
});
$(document).on("click", ".load-more-competitions", function(e) {
    e.preventDefault();
    var contextPath = $('#contextPath').val();
    var incSize = $('#maxNoOfCompetitions').val();
    var count = $('.competitionsCount').length;
    var newCount = parseInt(count) + parseInt(incSize);
    var url = ACC.config.contextPath + "/competitions/view/loadMore?pageSize=" + newCount;
    $.ajax({
        url: url,
        type: "get",
        success: function(data) {
            $("#competitions").html(data);
        },
        error: function(e) {
            alert("Sorry..could not load more competitions");
        }
    })
});
if ($(window).width() > 768) {
    var mobParam = ".login ";
} else {
    var mobParam = ".login-blk ";
}
$(document).on("submit", "#forgottenPwdForm", function(e) {
    e.preventDefault();
    var path = $(mobParam + '#forgottenPwdForm').attr('action');
    var datastring = $(mobParam + "#forgottenPwdForm").serialize();
    $.ajax({
        type: "POST",
        url: path,
        data: datastring,
        success: function(res) {
            var data = $(res).find('.forgottenPwd');
            $(mobParam + ".forgotPassword").html(res);
            $('.go-back').on('click', function(e) {
                e.preventDefault();
                $(".fpassword-block").hide();
                $(".userLogin").show();
            });
        },
        error: function() {}
    });
});
$(document).on("click", ".seeAllButton", function(e) {
    e.preventDefault();
    var val = $('.seeAllButton').attr('data');
    $("#searchForm :input").val(val);
    $('#searchForm').submit();
});
$(document).on("change", "#clinicProvince", function(e) {
    $('#preferredClinic').find('option').remove().end().append('<option value="" disabled="disabled" selected="selected">Please select</option>');
    $('#alternativeClinic').find('option').remove().end().append('<option value="" disabled="disabled" selected="selected">Please select</option>');
    var val = $("#clinicProvince option:selected").text();
    var url = ACC.config.contextPath + "/clinicBooking/preferredClinic?provinceName=" + val;
    $.ajax({
        url: url,
        type: "GET",
        success: function(data) {
            data.forEach(function(obj) {
                $('#preferredClinic').append($('<option>').text(obj.name).attr('value', obj.name));
                $('#alternativeClinic').append($('<option>').text(obj.name).attr('value', obj.name));
            });
        },
        error: function(data) {}
    });
    e.preventDefault();
});
$(document).on("change", "#preferredClinic", function(e) {
    $('#alternativeClinic').find('option').remove().end().append('<option value="" disabled="disabled" selected="selected">Please select</option>');
    var val = $("#clinicProvince option:selected").text();
    var url = ACC.config.contextPath + "/clinicBooking/preferredClinic?provinceName=" + val;
    $.ajax({
        url: url,
        type: "GET",
        success: function(data) {
            data.forEach(function(obj) {
                var preferredClinicValue = $("#preferredClinic option:selected").text();
                if (obj.name != preferredClinicValue) {
                    $('#alternativeClinic').append($('<option>').text(obj.name).attr('value', obj.name));
                }
            });
        },
        error: function(data) {}
    });
    e.preventDefault();
});
$(document).on("click", "#cmsClinicsButton", function(e) {
    e.preventDefault();
    var link = $(this).attr("href");
    if (link.indexOf("store-finder") > -1) {
        if (link.indexOf("store-finder/position") > -1) {
            $("#findStoresNearMe").click();
        } else {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
        }
    } else {
        window.location.href = link;
    }

    function showPosition(position) {
        link = link + "&latitude=" + position.coords.latitude + "&longitude=" + position.coords.longitude;
        window.location.href = link;
    }
});