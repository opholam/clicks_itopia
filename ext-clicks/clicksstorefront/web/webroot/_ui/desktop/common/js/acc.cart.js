ACC.cart = {

	bindAll: function ()
	{
		this.bindCartUpdateProduct();
	},
	bindCartUpdateProduct: function ()
	{
		$('.updateQuantityProduct').on("click", function (event)
		{
			event.stopImmediatePropagation();
			$(this).prop('disabled', true);
			var prodid = $(this).attr('id').split("_")
			var form = $('#updateCartForm' + prodid[1]);
			var productCode = form.find('input[name=productCode]').val(); 
			var initialCartQuantity = form.find('input[name=initialQuantity]');
			var newCartQuantity = form.find('input[name=quantity]');
			if(initialCartQuantity != newCartQuantity)
			{
				ACC.track.trackUpdateCart(productCode, initialCartQuantity.val(), newCartQuantity.val((parseInt(initialCartQuantity.val()) + 1)));
				form.submit();
			}

		});
		$('.removeProductQuantity').on("click", function (event)
				{				
					event.stopImmediatePropagation();
					$(this).prop('disabled', true);
					var prodid = $(this).attr('id').split("_")
					var form = $('#updateCartForm' + prodid[1]);
					var productCode = form.find('input[name=productCode]').val(); 
					var initialCartQuantity = form.find('input[name=initialQuantity]');
					var newCartQuantity = form.find('input[name=quantity]');
					if(initialCartQuantity != newCartQuantity)
					{
						ACC.track.trackUpdateCart(productCode, initialCartQuantity.val(), newCartQuantity.val((parseInt(initialCartQuantity.val()) - 1)));
						form.submit();
					}

			});
		var timer = 0;
		$('.quantity').each(function(){			
			$(this).on("keyup", function(){
				var qty = $(this);
				if(qty.val() != '' && parseInt(qty.val()) > 0) {
					if(timer !=0 ) 
						clearTimeout(timer);
					timer  = setTimeout(function(){
					//$(this).attr("value", ($(this).val()));
					var updatedQty = qty.val();
					var prodid =qty.siblings('input[name=entryNumber]').val();
					var form = $('#updateCartForm' + prodid);
					var productCode = form.find('input[name=productCode]').val(); 
					var initialCartQuantity = form.find('input[name=initialQuantity]');
					var newCartQuantity = form.find('input[name=quantity]');
					if(updatedQty != '' && parseInt(updatedQty) > 0) {
					if(initialCartQuantity.val() != updatedQty)
						{
							ACC.track.trackUpdateCart(productCode, initialCartQuantity.val(), newCartQuantity.val(parseInt(updatedQty)));
							form.submit();
						}
					}
					}, 1000);
				}
			});
		});
			
	}
}

$(document).ready(function ()
{
	ACC.cart.bindAll();
});
