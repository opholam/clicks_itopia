$(document).ready(function() {	
	$('#storeDeliveryAddressSubmit').hide();
	$('#isStoreNameSelected').hide();
	 $('#storeLocatorSearchButton, #changeStoreButton,#nearbyStoreSearchButton,#storelocator-q').click(function(event) {
		 /*
		  * fetch stores for location query
		  */
		 var gps = navigator.geolocation;
		 var contextPath = $("#contextPath").val();
		 if(null!=document.getElementById('selected.store'))
		 {
		 var deliveryMode=document.getElementById('selected.store').value;
		 }
		 var url =contextPath+'/checkout/multi/delivery-address/getStores';
		 var buttonId = $(this).attr('id');
		 if(null!=document.getElementById('storelocator-q') && (buttonId=='storeLocatorSearchButton' || (buttonId=='storelocator-q' && document.getElementById('storelocator-q').value!="") || buttonId=='changeStoreButton'))
		 {
			 var code =  document.getElementById('storelocator-q').value;
			 showPosition(code);
		 }
		 else
		 {
			 var code = '';
		 }
		if(gps && buttonId=='nearbyStoreSearchButton'){
			gps.getCurrentPosition(showPosition);
		}
		
		/* if(null!=document.getElementById('storelocator-latitude') && buttonId=='nearbyStoreSearchButton')
		 {
			 var latitude =  document.getElementById('storelocator-latitude').value;
		 }
		 else
		 {
			 var latitude='';
		 }
		 if(null!=document.getElementById('storelocator-longitude') && buttonId=='nearbyStoreSearchButton')
		 {
			 var longitude =  document.getElementById('storelocator-longitude').value;
		 }
		 else
		 {
			 var longitude='';
		 } */
		 function showPosition(position) {
			 var latitude='';
			 var longitude='';
			 if(position.coords!=undefined || null!=position.coords){
		       latitude=position.coords.latitude;
		       longitude=position.coords.longitude;
			 }
		        var option = '';
				 $.ajax({
					 	url: url,
			            async: false,
			            type: 'GET',
			            dataType: 'json',
			            data: {
			            	q: code,
			            	latitude:latitude,
			            	longitude:longitude,
			            	deliveryMode:deliveryMode
			            },
			            success: function(data) {
			            	if (data != null ) {
			            		if(!data.isDeliverable)
		                		{
		            				$(".store_areaForm").slideUp();
		                			document.getElementById('isStoreDiv').innerHTML="<div class='alert negative'><b>We're sorry currently we don't have any store near to the area you entered.</b></br></br>To proceed with your order, please try with a different location.</div>";
		                			$("#storeListDiv").hide();
		                			$('#storeDeliveryAddressSubmit').hide();
		                		}
			            		else
		                		{
			            			if(data.pointOfServiceData == null ||data.pointOfServiceData.length== 0){

			            				$(".store_areaForm").slideUp();
			                			document.getElementById('isStoreDiv').innerHTML="<div class='alert negative'><b>We're sorry currently we don't have any store near to the area you entered.</b></br></br>To proceed with your order, please try with a different location.</div>";
			                			$("#storeListDiv").hide();
			                			$('#storeDeliveryAddressSubmit').hide();
			            				
			            			}
			            			else{
			            		$("#storeListDiv").show();
		                		$(".store_areaForm").slideDown();
		                		document.getElementById('isStoreDiv').innerHTML="<div class='alert positive'>We have below stores near this area.</div>";
		                		if (data.pointOfServiceData != null && data.pointOfServiceData.length != 0) {
		                			$('#storeDeliveryAddressSubmit').show();
		                			$('#storeDeliveryAddress').hide();
		                    		option='<div id="storeListOuterDiv" ><div class="pickupStores">';
		                    		$('#storeListDiv').empty().append(option);
		                    		var count = 0;
		                    		$('.showAllStores').hide();
		                    		$.each(data.pointOfServiceData, function(i, item) {
		                    			if (count <5)
		                    			{    
		                    			count++;
		                    		option='<div class="store-search-content"><div class="store-search-innercontent1"><div class="fetch_stores" id="showStores'+item.name+'"><label for="'+item+'">'+item.displayName+' <br/> <span>'+item.address.line1+','+item.address.line2+','+item.address.town+','+item.address.postalCode+'</span> </label></div><div class="price pull-right" id="price'+item.name+'">'+item.formattedDistance+'</div></div><div class="store-search-innercontent1"><div class="storeLinks" id="storeLinks'+item.name+'"><div class="selectedStore" id="selectedStoreData'+item.name+'"><a href="#checkoutContentPanel" id="'+item.name+'" class="selectStore">Collect from this store</a><div class="showAllStores" id="'+item.name+'"><a id="link_'+item.name+'">change</a></div></div></div><div class="storeInfo" id="storeInfo'+item.name+'"><a href="javascript:void(0);" id="'+data.q+'_'+item.name+'" class="selectedStoreInfo" data-toggle="modal" data-target="#storeInfo">Show more details</a></div></div></div></div>'
		                    		$('.showAllStores').hide();
		                    		$('#storeListDiv').append(option);
		                    		//option='<div class="storeInfo" id="storeInfo'+item.name+'"><a href="javascript:void(0);" id="'+data.q+'_'+item.name+'" class="selectedStoreInfo" data-toggle="modal" data-target="#storeInfo">Show more details</a></div></div></div></div>';
		                    		//$('#storeListDiv').append(option);
		                    		}
		                    	});
		                    		if(data.pointOfServiceData.length> count)
		                    		{
		                    			option='<div class="loadDelStores" id="'+data.q+'_'+count+'"><a class="btn" id="'+data.q+'_'+count+'">LOAD MORE</a></div>';
		                    			$('#storeListDiv').append(option);
		                			}
		                    	}
		                		}
		                		}
			            		
			            	}
			            	 event.preventDefault();
		                        },
		                    })
		    }
		
	 });
	 
	 /*
	  * Pick up store address submit on click of continue button
	  */
	 $(document).on("click", ".delAddressSubmit", function(event) 
				{
				 var contextPath = $("#contextPath").val();
				 var formAction = contextPath + '/checkout/multi/delivery-address/getStores';	
				 var storeId= document.getElementById('posName').value;
				 if(storeId== undefined || storeId == null || typeof storeId == 'undefined' || storeId=='')
				 {
					 event.preventDefault();
					$('#isStoreNameSelected').show();
				 }
				 else{
					 //$('#isStoreNameSelected').hide();
				  $('#pickUpStoreForm').attr('action', formAction);
				 $('#pickUpStoreForm').submit();
				 }
					
				 }); 
	 

	 /*
	  * Pop up with store details
	  */
	  $(document).on("click", ".selectedStoreInfo, .storePopUp", function(event)
				{
				 var contextPath = $("#contextPath").val();
				 var url =contextPath+'/checkout/multi/delivery-address/getStores';
				 var id = $(this).attr('id').split("_");
				 var storeid=id[1];
				 var locationText=id[0];
				 var option = '';
				 $.ajax({
					 	url: url,
			            async: false,
			            type: 'GET',
			            dataType: 'json',
			            data: {
			            	q : locationText,
			            	posId: storeid
			            },
			            success: function(data) {
			            	if (data != null ) {
			            		if (data.pointOfServiceData != null && data.pointOfServiceData.length != 0) {
			            			$.each(data.pointOfServiceData, function(i, item) {
			            				if(item.name==data.posName){
			            					//$('#storeDetails').html('');
			            						$('.modal-dialog').remove();
			            					 $('#storeInfo').append($('#storeDetails').render(data.pointOfServiceData[i]))
			            				}
			            			});
			            			
			            		}
			            	}
			            	
			            },
				 })
							
				});

		 /*
		  * show all store details on click of change
		  */
	  $(document).on("click", ".showAllStores", function(event)
		{
		  var id=$(this).attr('id');
		  //document.getElementById('posName').value='';
		  $('#isStoreNameSelected').hide();
		  $('.fetch_stores').show();
		  $('.storeLinks').show();
		  $('.storeInfo').show();
		  $('.price').show();
		  $('.selectStore').show();
		  $('.showAllStores').hide();
		  $('.showStore').hide();
		  $('.storeCheckBox').hide();
		  $('#selectedStoreData'+$(this).attr('id')+'').show();
		  $('.loadDelStores').show();	
		});
	  
	  $(document).on("click", ".showStore", function(event)
				{
				  var id=$(this).attr('id');
				  $('.fetch_stores').show();
				  $('.storeLinks').show();
				  $('.storeInfo').show();
				  $('.price').show();
				  $('.selectStore').show();
				  $('.showAllStores').hide();
				  $('.showStore').hide();
				  $('.storeCheckBox').hide();
				  $('#selectedStoreData'+$(this).attr('id')+'').show();
				  $('.loadDelStores').hide();
				});
		 /*
		  * Show selected store and hide other stores
		  */
	 $(document).on("click", ".selectStore", function(event)
		{
		 document.getElementById('posName').value=$(this).attr('id');
		// var posName= document.getElementById('posName').value;
		 $('.selectStore').hide();	
		 $('#isStoreNameSelected').hide();
		// $(".price").css("visibility", "visible");   
		// $('.price').show();
		$('#selectedStoreData'+$(this).attr('id')+'').append('<label class="storeCheckBox"><input type="checkbox" checked="checked" name="myCheckbox" class="myCheckbox" disabled="disabled"/>Collect from this store</label>');
		//alert($('.showAllStores').children().length);
		//alert($('.showAllStores').contents().length);
		//alert($('.showAllStores').is(':empty'));
		if($('.showStore').is(':empty')) {
			
			$('.showStore').append('<a id="link_'+$(this).attr('id')+'">change</a>')
		}
		 /* var contextPath = $("#contextPath").val();
		  var url =contextPath+'/checkout/multi/delivery-address/getStores';
		  $.ajax({
			 	url: url,
	            async: false,
	            type: 'GET',
	            dataType: 'json',
	            data: {
	            	posId: posName
	            },
	            success: function(data) {
	            },
		 })*/
		
		$('.showAllStores').show();
		$('.showStore').show();
		$('.fetch_stores').hide();
		$('#showStores'+$(this).attr('id')+'').show();
		$('.storeLinks').hide();
		$('#storeLinks'+$(this).attr('id')+'').show();
		$('.storeInfo').hide();
		$('#storeInfo'+$(this).attr('id')+'').show();
		$('.price').hide();
		$('#price'+$(this).attr('id')+'').show();
		$('.loadDelStores').hide();
		});
	 

	 /*
	  * Load more stores
	  */
	 $(document).on("click", ".loadDelStores", function(event)
	{
		 var contextPath = $("#contextPath").val();
		 var url =contextPath+'/checkout/multi/delivery-address/loadMore';
		 var id=$(this).attr('id').split("_");
		 var count=id[1];
		 var q=id[0];
		 var option = '';
		 $.ajax({
			 	url: url,
	            async: false,
	            type: 'POST',
	            dataType: 'json',
	            data: {
	            	count:count,
	            	q:q,
	            },
             success: function(data) {
            	//alert(data.count);
            	//alert(data.pointOfServiceData.length);
            	 if(data!=null){
            	 if (data.pointOfServiceData != null && data.pointOfServiceData.length != 0) {
            		 $('.loadDelStores').hide();
            		 var count=data.count;
            		 if(data.pointOfServiceData.length>count && count>0)
            		 {
            			 $.each(data.pointOfServiceData, function(i, item) {
	            				if(i<5){
	            					//count++;
	            					$('#storeListDiv').append($('#loadMoreStoresTemplate').render(data.pointOfServiceData[i]));
	            				}
	            			}); 
            		 }
            		else
             		{
            			$.each(data.pointOfServiceData, function(i, item) {
            				 if(i<5){
        					//count++;
        					$('#storeListDiv').append($('#loadMoreStoresTemplate').render(data.pointOfServiceData[i]));
        						}
            				});
             		}
            		 //alert(data.pointOfServiceData.length);
					if(data.pointOfServiceData.length>count){
						 count=data.pointOfServiceData.length +count-1;
            			 //alert(count);
						$('.loadDelStores').hide();
						option='<div class="loadDelStores" id="'+data.q+'_'+count+'"><a class="btn" id="'+data.q+'_'+count+'">LOAD MORE</a></div>';
              			$('#storeListDiv').append(option);
						}
            		 
         			}
            
            	 }
            		 
             } ,
			 
		 })
	 });
	
    $('#deliveryAreaSearchButton').click(function() {
    	
        var contextPath = $("#contextPath").val();
        var url = contextPath + '/checkout/multi/delivery-address/getDeliveryArea';
        var code =  document.getElementById('address.postcode').value;
        var selectedDeliveryMode=document.getElementById('selected.addr').value;
       // alert(selectedDeliveryMode);
/*        if(code && code.length != 4){
        	return;
        }*/
        var option = '';
        $.ajax({
            url: url,
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {
            	postcode: code,
            	selectedDeliveryMode:selectedDeliveryMode
            },
            success: function(data) {
            	
               
                if (data != null ) {
                	if(!data.isDeliverable)
                		{
            				$(".deliver_areaForm").slideUp();
                			document.getElementById('isDeliverableDiv').innerHTML="<div class='alert negative'><b>We're sorry currently we don't deliver to this area, or we didn't recognise the postcode you entered.</b></br></br>To proceed with your order, please submit a different postcode.</div>";
                		}
                	else
                		{
                		$(".deliver_areaForm").slideDown();
                		document.getElementById('isDeliverableDiv').innerHTML="<div class='alert positive'>We deliver to your area.</div>";
                		document.getElementById('address.postcode').value=code;
                		document.getElementById('address.postcode.show').value=data.postcode;
                    	document.getElementById('address.Suburb').value=data.suburb;
                    	document.getElementById('address.country').value=data.countryName;
                    	
                    	$("#saveAddressInMyAddressBookDiv").show();
                    	if(data.countryIso != null){
                    	document.getElementById('countryIso').value=data.countryIso;
                    	}
                    	//alert(data.deliveryModes);
                    	if (data.deliveryModes != null && data.deliveryModes.length != 0) {
                    		option='<div id="deliveryMethodsOuterDiv" ><div class="delMethodRate"><h2>Delivery Method <strong class="pull-right">Cost</strong></h2>';
                    		$('#deliveryMethodsDiv').empty().append(option);
                    		
                    	$.each(data.deliveryModes, function(i, item) {
                    		//alert(data.selectedDeliveryMode);
                    		if((data.selectedDeliveryMode == null && i== 0 ) || (data.selectedDeliveryMode != null && item.code == data.selectedDeliveryMode ) ){
                        		checked = "checked" ;
                    		}
                    		else{
                        		checked = "" ;

                    		}
                    		//option='<div class="radioOuter clearfix"><form:radiobutton path="selectedDeliveryMode" value="'+item.code+'" name="delMethod" id="delMethodstand" cssClass="radio-hide" /><label for="delMethodstand">'+item.name+'<span>('+item.description+')</span></label><div class="price pull-right">'+item.deliveryCost.formattedValue+'</div></div>';

                    		option='<div class="choose_delivery_method"><div class="delivery_method_item radioOuter pull-left"><input type="radio" class="radio-hide" name="selectedDeliveryMode" id="'+item.code+'" value="'+item.code+'" '+checked+'/><label for="'+item.code+'">'+item.name+' - <span>'+item.description+'</span></label></div><div class="price pull-right">'+item.deliveryCost.formattedValue+'</div></div>'
                    		$('#deliveryMethodsDiv').append(option);
                    		$('#deliveryMethodsDiv').css('display','block');
                    	});
                    	option='<div class="moreaboutdelivery"><a href="javascript:void(0);" data-toggle="modal" data-target="#moreAboutDelivery">Find out more about delivery fees</a></div></div></div>';
                		$('#deliveryMethodsDiv').append(option);
                    	}
                    	
                		}
                }
            },
        })
        
        
        $(".delivery_method_item input:radio").click(function() {

            document.getElementById('selectedDeliveryMode').value=this.value;

           });
        
        
    });
   
});






