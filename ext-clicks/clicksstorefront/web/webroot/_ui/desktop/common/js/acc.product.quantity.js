ACC.product.quantity = {

	bindAll: function ()
	{
		this.bindCartUpdateProduct();
	},
	bindCartUpdateProduct: function ()
	{		
		$("#addToCartForm").each(function(){
			var initialCartQuantity =1;
			$('.updateProductQuantity').click(function () {
				var initialCartQuantity = parseInt($(this).parents("#addToCartForm").find(".quantity").val());
				initialCartQuantity=initialCartQuantity + 1;
				$(this).parents("#addToCartForm").find(".quantity").attr("value",initialCartQuantity);
				
			});
			$('.removeQuantityProduct').click(function () {
				var initialCartQuantity = parseInt($(this).parents("#addToCartForm").find(".quantity").val());
				if(initialCartQuantity > 1){
					initialCartQuantity=initialCartQuantity - 1;
					$(this).parents("#addToCartForm").find(".quantity").attr("value",initialCartQuantity);
				}
	
			});
		});
	}
}

$(document).ready(function ()
{
	ACC.product.quantity.bindAll();
});
