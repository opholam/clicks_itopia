ACC.cart.popup = {

	bindAll: function ()
	{
		this.bindCartUpdate();
		this.bindToUpdateCartForm();
	},
	bindCartUpdate: function ()
	{
		$(document).on("click", ".update_quantity_form .incrementProductQuantity", function(event)
		{
			event.preventDefault();
			event.stopImmediatePropagation();
			var prodid = $(this).attr('id').split("_");
			var form = $('#updateCartForm' + prodid[1]);
			var productCodePost = form.find('input[name=productCodePost]').val(); 
			var initialCartQuantity = form.find('input[name=initialQuantity]');
			var newCartQuantity = form.find('input[name=quantity]');
			if(initialCartQuantity != newCartQuantity)
			{
				ACC.track.trackUpdateCart(productCodePost, initialCartQuantity.val(), newCartQuantity.val((parseInt(initialCartQuantity.val()) + 1)));
				ACC.cart.popup.bindToUpdateCartForm();
			}

		});
		$(document).on("click", ".update_quantity_form .submitRemoveProduct", function(event)
				{
					event.preventDefault();
					event.stopImmediatePropagation();
					var prodid = $(this).attr('id').split("_")
					var form = $('#updateCartForm' + prodid[1]);
					var productCodePost = form.find('input[name=productCodePost]').val(); 
					var initialCartQuantity = form.find('input[name=initialQuantity]');
					var cartQuantity = form.find('input[name=quantity]');

					ACC.track.trackRemoveFromCart(productCodePost, initialCartQuantity.val());
					cartQuantity.val(0);
					initialCartQuantity.val(0);
					ACC.cart.popup.bindToUpdateCartForm();
				});
		$(document).on("click", ".update_quantity_form .decrementProductQuantity", function(event)
				{				
					event.preventDefault();
					event.stopImmediatePropagation();
					var prodid = $(this).attr('id').split("_")
					var form = $('#updateCartForm' + prodid[1]);
					var productCodePost = form.find('input[name=productCodePost]').val();
					var initialCartQuantity = form.find('input[name=initialQuantity]');
					var newCartQuantity = form.find('input[name=quantity]');
					if(initialCartQuantity != newCartQuantity)
					{
						ACC.track.trackUpdateCart(productCodePost, initialCartQuantity.val(), newCartQuantity.val((parseInt(initialCartQuantity.val()) - 1)));						
						ACC.cart.popup.bindToUpdateCartForm();
					}

			});
		var timer = 0;	
		$(document).on("keyup", ".update_quantity_form .quantity", function(event)
				{
					var qty = $(this);
					if(qty.val() != '' && parseInt(qty.val()) > 0) {
						if(timer !=0 ) 
							clearTimeout(timer);
						timer  = setTimeout(function(){
							var updatedQty = qty.val();
							var prodid =qty.siblings('input[name=entryNumber]').val();
							var form = $('#updateCartForm' + prodid);
							var productCodePost = form.find('input[name=productCodePost]').val();
							var initialCartQuantity = form.find('input[name=initialQuantity]');
							var newCartQuantity = form.find('input[name=quantity]');
							if(updatedQty != '' && parseInt(updatedQty) > 0) {
							if(initialCartQuantity.val() != updatedQty)
								{
									ACC.track.trackUpdateCart(productCodePost, initialCartQuantity.val(), newCartQuantity.val(parseInt(updatedQty)));
									ACC.cart.popup.bindToUpdateCartForm();
								}
							}
						}, 1000);
					}
		});
			
	},
 	bindToUpdateCartForm : function() {
		var updateQuantityForm = $('.update_quantity_form');
		
		updateQuantityForm.ajaxSubmit({
			success : ACC.cart.popup.displayAddToCartPopup
		});
	},
    displayAddToCartPopup : function(cartResult, statusText, xhr, formElement) {
    	if (typeof ACC.minicart.refreshMiniCartCount == 'function') {
    		ACC.minicart.refreshMiniCartCount();
    	}
    	$.colorbox({
    		html: cartResult.addToCartLayer,
    		maxWidth:"100%",
    		opacity:0.7,
    		width:500,
    		scrolling: false,
    		closeButton: true,
    		onComplete: function(){
    			$(this).colorbox.resize();
    			ACC.clicks.basketQuantity();
    		}
    	});
    	
    	var productCode = $('[name=productCodePost]', formElement).val();
    	var quantityField = $('[name=quantity]', formElement).val();
    	var quantity = 1;
    	if (quantityField != undefined) {
    		quantity = quantityField;
    	}
    	ACC.track.trackAddToCart(productCode, quantity,cartResult.cartData);
    }
}

$(document).ready(function ()
{
	ACC.cart.popup.bindAll();
});
