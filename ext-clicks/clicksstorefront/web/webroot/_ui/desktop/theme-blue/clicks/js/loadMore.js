function loadStores(count) {
    $("#storeList").html($("#storeListTemplate").render({
        count: parseInt(count + 10)
    }));
	window.scrolledArea = 0;		
		$('.searchPOSResultsList').scroll(function() {
		    var scrollTop = $('.searchPOSResultsList').offset().top,
		        divOffset = $('.searchPOSResult').eq(0).offset().top-21;
		        window.scrolledArea = (scrollTop-divOffset);		       
		});
		$(".emailleft .emailNotifyLink").on("click", function() {
			var that = $(this);
			$(this).parent(".emailleft").find(".subscribe-pop").show();
			$(this).parent(".emailleft").find(".close-link").show();
			$(this).parent(".emailleft").addClass("emailbox-color");				
				var prevoffset = that.offset().top;
				var scrolled = $('.searchPOSResultsList').offset().top;
				window.scrolledArea = prevoffset - scrolled + window.scrolledArea;				
				$('.searchPOSResultsList').animate({scrollTop: window.scrolledArea}, 800);
					     
		});	
		$(".emailleft .close-link").on("click", function() {					
			$(this).parent(".emailleft").find(".subscribe-pop").hide();
			$(this).parent(".emailleft").find(".close-link").hide();						
			$(this).parent(".emailleft").removeClass("emailbox-color");						
			$(".notifyMessage").hide();					
		});
	$("#emailNotifyForm").validate({
            rules: {               
                email: {
                    required: !0,
                    email: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {               
                email: "Please enter a valid email address"                  
            }
        })
		$('.customerSubscribeBtn').click(function(){
            $("#emailNotifyForm").submit();			
        });
	
    stores = $.parseJSON($('#map_canvas').data('allstores'));
    i = 0;
    cnt = count;
    $.each(stores, function(k, v) {
        if (i > cnt && i < cnt + 10) {
            ACC.storefinder.addStore(v.id, v.latitude, v.longitude, v.name);
        }
        i = i + 1;

    });

    $('html, body').animate({
        'scrollTop': $("#" + parseInt(count + 5)).position().top
    });
}

function loadfeature(feature) {
    var a = document.getElementById("hiddenField").value = feature;

}

function loadProducts(count, categoryCode) {

    $.ajax({
        type: "GET",
        url: categoryCode + "/results?count=" + count + '&q=' + $.query.get('q'),
        dataType: "json",
        contentType: "application/json; charset=utf-8",

        cache: false,
        success: function(response1) {
            //var cnt=1;

            response = response1.results;

            for (i = (count - (count / 2)) + 1; i < response.length + 1; i++) {
                response[i - 1].i = i;

                if (null != response[i - 1].price && null != response[i - 1].price.formattedValue) {
                    response[i - 1].priceValue = response[i - 1].price.formattedValue.split('.')[0];
                    response[i - 1].decimalValue = response[i - 1].price.formattedValue.split('.')[1];
                }

                var row_count = Math.ceil(i / 3);
                if (i % 3 == 1) {
                    $(".filterContent").append($("#productListStart").render({
                        i: row_count
                    }));
                }
                //  console.log(response[i - 1]);
                if (null != response[i - 1]) {
                    $(".horiz-" + row_count).append($("#productList").render(response[i - 1]));
                }

                // Price Section
                var priceDiv = '<div class="price">';
                if (null != response[i - 1].price && null != response[i - 1].price.formattedValue) {
                    var price = response[i - 1].price.formattedValue.substring(1);
                }
                if (null != response[i - 1].price && null != response[i - 1].potentialPromotions && null != response[i - 1].price.grossPriceWithPromotionApplied && response[i - 1].price.value != response[i - 1].price.grossPriceWithPromotionApplied) {
                    priceDiv = '<div class="price red">';
                    price = parseFloat(response[i - 1].price.grossPriceWithPromotionApplied).toFixed(2);
                } else {
                    priceDiv = '<div class="price blue">';
                }

                if (price != undefined) {
                    var promoPrice = price.split(".");
                    var promoPriceMessage = priceDiv + 'R' + promoPrice[0] + '<sup>' + promoPrice[1] + '</sup></div>';

                    $('#priceMessage_' + response[i - 1].code).html(promoPriceMessage);
                }

                // Ratings Section
                var ratings = '<span class="stars large"><span class=" ${addClass}" style="width:' + response[i - 1].averageRating * 15.5 + 'px; margin-right:' + (5 - response[i - 1].averageRating) * 15.5 + 'px;">' + response[i - 1].averageRating + '</span></span>';
                $('#star_' + response[i - 1].code).html(ratings);
            }

            // Prev and Next link rel tag for Load more products on SEO catch

            var prevPage = parseInt($('#seoPageNext').val()) - 1;
            $('#seoPagePrev').val(prevPage);
            $('#plpLoadMoreSEOPrev').attr('href', location.protocol + "//" + location.host + location.pathname + "?page=" + prevPage);

            var nextPage = parseInt($('#seoPageNext').val()) + 1;
            if (response1.pagination.totalNumberOfResults == response.length) {
                nextPage = "";
            }

            $('#seoPageNext').val(nextPage);
            $('#plpLoadMoreSEONext').attr('href', location.protocol + "//" + location.host + location.pathname + "?page=" + nextPage);

            // SEO links section ends

            if (response1.pagination.totalNumberOfResults == response.length) {
                $("#loadMoreProd").html($("#hideLoadMore").render(''));
            } else {
                $("#loadMoreProd").html($("#showLoadMore").render({
                    count: count * 2
                }));
            }

            if ($(window).width() <= 991) {
                $('.productRow').contents().unwrap();
                var $div = $('.filterContent .plpsep'),
                    length = $div.length;
                for (var i = 0; i < length; i = i + 2) {
                    $div.slice(i, i + 2).wrapAll('<div class="productRow clearfix" />')
                }
            }
            if ($(window).width() >= 992) {
                $('.productRow').contents().unwrap();
                var $div = $('.filterContent .plpsep'),
                    length = $div.length;
                for (var i = 0; i < length; i = i + 3) {
                    $div.slice(i, i + 3).wrapAll('<div class="productRow clearfix" />')
                }
            }
        },
        error: function(xht, textStatus, ex) {
            //alert("Error details [" + xht + ", " + textStatus + ", " + ex + "]");
        }
    });
}

function loadOrders()
{
		var count = $('#orderHisCount').val();
	  $.ajax({
             type: "GET",
             url: $('#contextPath').val()+"/my-account/loadOrders?count="+count,
             dataType: "json",
             contentType: "application/json; charset=utf-8",

             cache: false,
             success: function(response) {
           	  //var cnt=1;
           	  var datecreated;
           	  var end=0;
           	$('#orderHisCount').val(parseInt(count)+parseInt(response.length));
/* 		           	if(count <= response.length)
                {
                	end = count;
                }
               else
                {
	                end = response.length
                } */
           	  for (i = 0; i < response.length; i++) {
           			 datecreated = new Date(response[i].placed);
           			 datecreated = datecreated.toString().split(" ");
                	 response[i].formatDate = datecreated[2] + " " +datecreated[1]+" "+datecreated[3];
                	 $("#orderId").append($("#orderListTemplate").render(response[i]));
                 }
		           	if(parseInt($('#orderHisCount').val()) >= parseInt($('#OrderHisListTotalCount').val()))
	                {
	                	$(".load-order").html($("#hideLoadMore").render(''));
	                }
	               else
	                {
	                	$(".load-order").html($("#showLoadMore").render(''));
	                }
                 
   },
   error: function(e) {
       alert("Sorry..could not load more orders");
   }
         });

}