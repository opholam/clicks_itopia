$(document).ready(function() {
	/* google map - store locator */
	function initialize() {
		var myCenter=new google.maps.LatLng(55.680313, 12.548468);	
		var mapProp = {
			center: myCenter,
			zoom:7,
			disableDefaultUI:true,    
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
		var map1 = new google.maps.Map(document.getElementById("googleMap1"),mapProp);
		var map2 = new google.maps.Map(document.getElementById("googleMap2"),mapProp);  
	}
	google.maps.event.addDomListener(window, 'load', initialize);

});	
