clicksjs1 = {
	'grunt' : 
	{
		'js_files' :
		[
			'../../common/js/jquery-1.11.2.js',
			'bootstrap/js/bootstrap.min.js',
			'js/modernizr.js',
			'../../common/js/jquery-1.7.2.min.js',
			'../../common/js/jsrender.js'
		]
	}
}
clicksjs2 = {
	'grunt' : 
	{
		'js_files' :
		[
			'../../common/js/jquery.query-2.1.7.js',
			'../../common/js/jquery-ui-1.8.18.min.js',
			'../../common/js/jquery.jcarousel-0.2.8.min.js',
			'../../common/js/jquery.tmpl-1.0.0pre.min.js',
			'../../common/js/jquery.blockUI-2.39.js',
			'../../common/js/jquery.colorbox.custom-1.3.16.js',
			'../../common/js/jquery.slideviewer.custom.1.2.js',
			'../../common/js/jquery.easing.1.3.js',
			'../../common/js/jquery.waitforimages.min.js',
			'../../common/js/jquery.scrollTo-1.4.2-min.js',
			'../../common/js/jquery.ui.stars-3.0.1.min.js',
			'../../common/js/jquery.form-3.09.js',
			'../../common/js/jquery.bgiframe-2.1.2.min.js'
		]
	}
}
clicksjs3 = {
	'grunt' : 
	{
		'js_files' :
		[
			'../../common/js/jquery.bt-0.9.5-rc1.min.js',
			'../../common/js/jquery.pstrength.custom-1.2.0.js',
			'../../common/js/acc.common.js',
			'js/jquery.bxslider.rahisified.min.js',
			'js/slider.js',
			'../../common/js/acc.userlocation.js',
			'../../common/js/acc.track.js',
			'../../common/js/acc.cms.js',
			'../../common/js/acc.product.js',
			'../../common/js/acc.refinements.js',
			'../../common/js/acc.storefinder.js',
			'../../common/js/acc.carousel.js',
			'../../common/js/acc.autocomplete.js',
			'../../common/js/acc.pstrength.js',
			'../../common/js/acc.password.js',
			'../../common/js/acc.minicart.js',
			'../../common/js/acc.pickupinstore.js',
			'../../common/js/acc.langcurrencyselector.js',
			'../../common/js/acc.paginationsort.js',
			'../../common/js/acc.checkout.js',
			'../../common/js/acc.cartremoveitem.js',
			'../../common/js/acc.cart.js',
			'../../common/js/acc.cart.popup.js',
			'../../common/js/acc.product.quantity.js',
			'../../common/js/acc.address.js',
			'../../common/js/acc.refresh.js',
			'../../common/js/acc.stars.js',
			'../../common/js/acc.forgotpassword.js',
			'../../common/js/acc.register.js',
			'../../common/js/acc.joincc.js',
			'../../common/js/jquery.accessible-tabs-1.9.7.min.js',
			'../../common/js/acc.productDetail.js',
			'../../common/js/acc.producttabs.js'
		]
	}
}
clicksjs4 = {
	'grunt' : 
	{
		'js_files' :
		[
			'js/bootstrap-datepicker.js',
			'../../common/js/acc.skiplinks.js',
			'js/picturefill.min.js',
			'js/jquery.validate.js',
			'js/jquery.hoverIntent.js',
			'js/initialize.js',
			'js/validation.js',
			'js/saidvalidation.js',
		]
	}
}
clicksjs5 = {
	'grunt' : 
	{
		'js_files' :
		[
			'js/loadMore.js',
			'js/jquery.selectric.js',
			'js/demo.js',
			'../../common/js/magazine.js',
			'../../common/js/acc.facebook.plugin.js',
			'../../common/js/productDetailsPage.js',
			'../../common/js/add.to.any.page.js',
			'../../common/js/getDeliveryArea.js'
		]
	}
}

module.exports = function(grunt) {

	grunt.initConfig({

		// Remove built directory
		clean: {
		  build: ['build/']
		},

		// Build the site using grunt-includes
		includes: {
		  build: {
			cwd: 'html',
			src: [ '*.html' ],
			dest: 'build/',
			options: {
			  flatten: true,
			  includePath: 'include',
			}
		  }
		},

		less: {
		  development: {
			options: {
			  compress: true,
			  yuicompress: true,
			  optimization: 2
			},        
			 files: {
			 "css/main.css": ["main-less/main.less"],
			}
		  }
		},
		
		cssmin: {
			options: {
			  compress: true,
			  yuicompress: true,
			  optimization: 2
			},
			lib: {
				files: [{
					src: [
						'include-less/css/bootstrap-datepicker.css',
						'include-less/css/jquery.ui.stars-3.0.1.custom.css',
						'include-less/css/jquery.ui.autocomplete-1.8.18.css',
						'include-less/css/forms.css',
						'include-less/css/siteSearch.css',
						'include-less/css/facetNav.css',
						'include-less/css/colorBox.css',
						'include-less/css/jquery-ui.css'
					],
					dest: 'css/hybrismain.css'
				}]
			}
		},
		
		sprite:{
			/*all: {
				src: ['image/sprite/*.png', 'image/sprite/*.jpg'], 
				dest: 'image/out/sprite.png',
				destCss: 'image/out/sprite.css',
				algorithm: 'top-down',
				padding: 20
			},*/
			/*all: {
				src: ['image/mobile-sprite/*.png', 'image/mobile-sprite/*.jpg'], 
				dest: 'image/mb-out/mb-sprite.png',
				destCss: 'image/mb-out/mb-sprite.css',
				algorithm: 'top-down',
				padding: 20
			}*/
			cssHandlebarsTemplate: {
				src: ['image/sprite-new/*.png', 'image/sprite-new/*.jpg'],
				dest: 'image/build-sprite/sprite.png',
				destCss: 'css/sprite.css',
				// Use top-down to make maintenance easier
				algorithm: 'top-down',
				padding: 20,
				cssTemplate: 'image/handlebarsStr.css.handlebars'
      },
	  
		},

		svg_sprite : {
			basic : {
				// Target basics 
				expand              : true,
				cwd                 : '',
				src                 : 'image/svg-images/*.svg',
				dest                : 'svg-sprite/',
				
				// Target options 
				options             : {
					mode            : {
						css         : {     // Activate the «css» mode 
							render  : {
								css : true  // Activate CSS output (with default options) 
							}
						}
					}
				}
			}
		},
		uglify: {
			build: {
				files: [{
					src: clicksjs1.grunt.js_files,
					dest: 'js/combined1.js'
				}]
			},
			comjs2: {
				files: [{
					src: clicksjs2.grunt.js_files,
					dest: 'js/combined2.js'
				}]
			},
			comjs3: {
				files: [{
					src: clicksjs3.grunt.js_files,
					dest: 'js/combined3.js'
				}]
			},
			comjs4: {
				files: [{
					src: clicksjs4.grunt.js_files,
					dest: 'js/combined4.js'
				}]
			},
			comjs5: {
				files: [{
					src: clicksjs5.grunt.js_files,
					dest: 'js/combined5.js'
				}]
			}
		},
		
	// Build the site using grunt-includes	
	watch: {
			html: {
				files: ['include/*.html'],
				tasks: ['includes'],
				options: {livereload: true},
				event: ['added', 'deleted']
			},
			styles: {
				files: ['include-less/**/*.less'], // which files to watch
				tasks: ['less'],
				options: {
				  nospawn: true
				}
			},
			cssstyles: {
				files: ['include-less/**/*.css'], // which files to watch
				tasks: ['cssmin:lib'],
				options: {
				  nospawn: true
				}
			},
			svgsprites: {
				files: ['image/svg-images/*.svg'], // which files to watch
				tasks: ['svg_sprite'],
				options: {
				  nospawn: true
				}
			},
			spriteimages: {
				files: ['image/sprite/**/*.png', 'image/sprite/**/*.jpg'], // which files to watch
				tasks: ['sprite'],
				options: {
				  nospawn: true,
				}
			},
			comjs: {
				files: ['js/*.js', '../../common/js/*.js'], // which files to watch
				tasks: ['uglify'],
				options: {
				  nospawn: true
				}
			},
		}
	});

  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-spritesmith');
//  grunt.loadNpmTasks('grunt-svg-sprite');

// Task definitions
  // grunt.registerTask('build', ['clean','watch']);
  // grunt.registerTask('default', ['build']);
  // grunt.registerTask('default', ['less', 'watch']);
  // grunt.registerTask('default', ['build', 'svg_sprite']);
  
  
  grunt.registerTask('build', ['watch']);
  grunt.registerTask('default', ['sprite','less','cssmin:lib','uglify','includes','build']);
 };