var contextPathURL = $("#contextPath").val();
var currActionURL = $('#paymentDetailsForm').attr('action');
var currButtonVal = 'Continue';
var showButton=$('#showButtonOnBilling').val();

function onProductClick(e, s, i, a, t, o, v) {
    ga("ec:addProduct", {
        id: e,
        name: (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + (s + "").toLowerCase().replace(/[^a-zA-Z0-9 ]/g, ""),
        brand: (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
        list: (a + "").toLowerCase().replace(/[^a-zA-Z0-9_]/g, ""),
        position: t
    }), ga("ec:setAction", "click", {
        list: (a + "").toLowerCase().replace(/[^a-zA-Z0-9_]/g, "")
    }), ga("send", "event", "internal placement", (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, ""), (a + "").toLowerCase().replace(/[^a-zA-Z0-9_ ]/g, "") + "_" + v, {
        hitCallback: function() {
            document.location = o
        }
    })
}

function onPromoClick(e, s, i, a, t, o, n, l) {
    ga("ec:addPromo", {
        id: (e + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + s.replace(/[^a-zA-Z0-9]/g, "") + "_" + (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + (a + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + t.replace(/[^a-zA-Z0-9-]/g, "") + "_" + (o + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
        name: (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
        position: n
    }), ga("ec:setAction", "promo_click"), ga("send", "event", "internal placement", i, e + "_" + s, {
        hitCallback: function() {
            document.location = l
        }
    })
}

function gaTracking(e, s, i, a) {
    ga('ec:addProduct', {         
        'id': e,
                   'name': (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + (s + "").toLowerCase().replace(/[^a-zA-Z0-9 ]/g, ""),
         'brand': (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
                
    });
    ga('ec:setAction', 'add');   
    ga('send', 'event', 'basket', a);
}

function gaTrackingClp(e, s, i, a, c, p) {
    ga('ec:addProduct', {         
        'id': e,
                   'name': (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + (s + "").toLowerCase().replace(/[^a-zA-Z0-9 ]/g, ""),
         'brand': (i + "").toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
          
        'list': (c + "").toLowerCase().replace(/[^a-zA-Z0-9_]/g, ""),
          'position': p 
    });
    ga('ec:setAction', 'add', {
        'list': (c + "").toLowerCase().replace(/[^a-zA-Z0-9_]/g, "")
    });   
    ga('send', 'event', 'basket', a);
}

ACC.clicks = {
    mobileValue: 0,
    openSubMenu: function() {
        $(this).find("section").css({
            "visibility": "visible",
            "display": "block"
        })
    },
    closeSubMenu: function() {
        $(this).find("section").css({
            "visibility": "hidden",
            "display": "none"
        })
    },
    headerMenu: function() {
        if ($(window).width() > 768) this.mobileValue = 0,
            $(".footer-links ul li h5").addClass("active"), $(".inner-menu img").each(function(e, s) {
                var i = $(s),
                    a = i.width();
                i.addClass(a > 50 ? "bigImg" : "smallImg")
            });
        else {
            this.mobileValue = 1, $(".nav-list > li").off("mouseover", this.openSubMenu), $(".nav-list > li").off("mouseout", this.closeSubMenu);
            var e = $("header").height(),
                s = e + $(".mobile-header .nav-collapse").height() - 1;
            $(".mobile-header .nav-collapse").css({
                top: e
            }), $(".main-nav").css({
                top: s
            }), $(".footer-links ul li h5").removeClass("active")
        }
        this.resizesMethod(this)
    },
    resizesMethod: function(e) {
        $(window).resize(function() {
            if ($(window).width() > 768) 1 == e.mobileValue && (e.mobileValue = 0,
                $(".nav-list > li").hoverIntent(function() {
                        $(this).addClass("active").siblings().removeClass("active");
                        setTimeout(function() {
                            $(".nav-list > li.active").find("section").css({
                                    "visibility": "visible",
                                    "display": "block"
                                }).parents("li").siblings().find("section").css({
                                    "visibility": "hidden",
                                    "display": "none"
                                }),
                                $("#menu-overlay, #menu-overlay-footer").show(),
                                $("footer").css("border-top", "0px")
                        }, 600);
                    },
                    function() {
                        setTimeout(function() {
                            $(".nav-list > li").find("section").css({
                                    "visibility": "hidden",
                                    "display": "none"
                                }),
                                $("#menu-overlay, #menu-overlay-footer").hide(),
                                $("footer").css("border-top", "0px")
                        }, 0);
                    }),
                $(".main-nav .nav-list > li > a").removeClass("active").next("section").css({
                    display: "block",
                    visibility: "hidden"
                }), $("#menu-overlay, #menu-overlay-footer").css({
                    display: "none"
                }), $(".main-nav, .menu-list-links").css({
                    display: "block",
                    overflow: ""
                }), $(".footer-links ul li > .collapse").slideDown(), $(".footer-links ul li h5").addClass("active")), $(".mobile-header .nav-collapse").css({
                top: ""
            }), $(".main-nav").css({
                top: ""
            });
            else {
                0 == e.mobileValue && (e.mobileValue = 1, $(".nav-list > li").off("mouseover", e.openSubMenu), $(".nav-list > li").off("mouseout", e.closeSubMenu), $(".main-nav .nav-list section").css({
                    visibility: "",
                    display: ""
                }), $(".icon-menu").hasClass("active") || $(".main-nav").css({
                    display: ""
                }), $(".menu-list-links").css({
                    display: ""
                }), $(".footer-links ul li > .collapse").slideUp(), $(".footer-links ul li h5").removeClass("active"));
                var s = $("header").height(),
                    i = s + $(".mobile-header .nav-collapse").height() - 1;
                $(".mobile-header .nav-collapse").css({
                    top: s
                }), $(".main-nav").css({
                    top: i
                })
            }
        })
    },
    showHideContent: function() {
        $(".financeinfo").hide(), $(".moreInfo").click(function(e) {
            var s = e.target;
            return $(this).next(".financeinfo").slideToggle("slow", function() {
                $(s).html($(this).is(":visible") ? "Hide details" : "Show details")
            }), !1
        })
    },
    header: function() {
        $(".mob-container .nav-list > li > a").on("click", function(e) {
                $(window).innerWidth() > 1024 || e.preventDefault(), $(window).innerWidth() > 992 || (e.preventDefault(), $(this).toggleClass("active").next("section").slideToggle().parent().siblings().find("a").removeClass("active").next("section").slideUp().parents("ul").siblings("ul").children("li").removeClass("active").find("a").removeClass("active").next("section").slideUp()), $(this).off(e)
            }), $(".mob-container").find(".accordion-toggle").click(function() {
                $(this).toggleClass("active").siblings("ul").slideToggle().parent("li").siblings().children("h3").removeClass("active").siblings("ul").slideUp();
            }), $(".mobile-nav-bar nav ul li a").on("click", function(e) {
                e.preventDefault();
                var s = $(this).attr("data-tab");
                $(this).hasClass("active") ? (".main-nav" == s ? ($(".mobile-tabs:not(.nav-collapse)").slideUp(), setTimeout(function() {
                    $(".mobile-header .nav-collapse").hide()
                }, 400)) : $(".mobile-tabs:not(.nav-collapse)").slideUp(), $(".mobile-nav-bar nav ul li a").removeClass("active").removeClass("opac-show"), $("#menu-overlay, #menu-overlay-footer").hide(), $("footer").css("border-top", "1px")) : ($(".mobile-tabs").hide(), $(".mobile-nav-bar nav ul li a").removeClass("active").addClass("opac-show"), $(this).addClass("active").removeClass("opac-show"), ".main-nav" == s && $(".mobile-header .nav-collapse").show(), $(s).slideDown(), $("#menu-overlay, #menu-overlay-footer").show(), $("footer").css("border-top", "0px"))
            }),
            $("#menu-overlay, #menu-overlay-footer").on("click", function() {
                $(".mobile-nav-bar nav ul li a").removeClass("active").removeClass("opac-show"), $("#menu-overlay, #menu-overlay-footer").hide()
            });
        var e = $(".find-store").clone(),
            s = $(".header .logo-block .global-search").clone(),
            a = ($(".basket-list").clone());
        $(".store-blk").append(e), $(".search-blk").append(s), $(".basket-blk").append(a), $(".mobile-tabs>div").removeClass("dropdown-list"), this.signInValidation(), $(".footer-links ul li h5").on("click", function(e) {
            $(window).width() < 769 && (e.preventDefault(), $(this).hasClass("active") ? ($(this).removeClass("active"), $(this).next("div.collapse").slideUp()) : ($(".footer-links ul li h5").removeClass("active"), $(this).addClass("active"), $(".footer-links ul li > .collapse").slideUp(), $(this).next("div.collapse").slideDown()))
        })
    },
    autoComplete: function() {
        $(".global-search input.text").focus(function() {}), $(".global-search input.text").blur(function() {})
    },
    signInValidation: function(e) {
        var s = $(e + " #loginForm");
        s.on("submit", function() {
            var i = $(e + " #j_username").val(),
                a = $(e + " #j_password").val();
            return "" == i ? (s + $(" #j_username").focus(), s + $(" #j_username").addClass("error"), s + $(" #j_username").attr("placeholder", "Please enter a value"), !1) : "" == a ? (s + $(" #j_password").focus(), s + $(" #j_password").addClass("error"), s + $(" #j_password").attr("placeholder", "Please enter a value"), !1) : $("" != i && "" != a) ? !0 : void 0
        })
    },
    checkNumbers: function(e) {
        var s = "";
        return /^[0-9]{8}$/.test(+e) && 8 == e.length ? s : "Please Enter only Number"
    },
    checkEmail: function(e) {
        var s = "",
            i = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
            a = /[\(\)\<\>\,\;\:\\\"\[\]]/;
        return 0 == e.length ? s = "Enter the valid email address\n" : i.test(e) ? e.match(a) && (s = "Email address contains illegal characters.\n") : s = "Enter a valid email address.\n", s
    },
    checkPassword: function(e) {
        var s = "",
            i = /^.{6,}$/;
        return 0 == e.length ? s = "Enter the valid Password\n" : i.test(e) || (s = "Enter a valid Password.\n"), s
    },
    checkPasswordMatch: function(e) {
        var s = "",
            i = $("#createAccount input[name='password']").val(),
            a = e;
        return s = i != a ? "Password doesn't match" : ""
    },
    checkName: function(e) {
        var s = "",
            i = /^[a-z ,.'-]+$/i,
            a = /[\(\)\<\>\,\;\:\\\"\[\]]/;
        return 0 == e.length ? s = "Please Enter Name\n" : i.test(e) ? e.match(a) && (s = "Enter a LastName.\n") : s = "Enter a Name.\n", s
    },
    ClubCardNumberValidation: function() {},
    promoErrImage: function() {},
    forgotPopup: function() {
        $(".forgot-block").on("click", function() {
            $(".sign-in").next(".dropdown-list").is(":visible") ? ($(".sign-in").next(".dropdown-list").find("div.userLogin").hide(), $(".sign-in").next(".dropdown-list").find(".fpassword-block").show()) : ($(".sign-in").closest(".dropdown").find(".dropdown-list").css("z-index", "9999").slideToggle(400, "swing", function() {
                $("html, body").animate({
                    scrollTop: $(this).offset().top
                }, "slow")
            }), $("#div-overlay").css({"display": "block", "position": "fixed"}), $(".sign-in").next(".dropdown-list").find("div.userLogin").hide(), $(".sign-in").next(".dropdown-list").find(".fpassword-block").show(), $(".sign-in").parent(".dropdown").addClass("open"))
        })
    },
    newletterValidation: function() {
        $("#item-form").submit(function() {
            var e = $.trim($("#contact_email").val()),
                s = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return "" === e ? ($("#contact_email").focus().addClass("error"), $(".newError").show(), !1) : 0 == s.test($("#contact_email").val()) ? ($(this).focus(), $(".newError").show(), !1) : !0
        })
    },
    inputErrorValidation: function() {
        $(".form-wrap input").on("focus", function() {
            $(".redCommon").length > 0 && $(this).closest(".control-group").hasClass("error") && ($(this).closest(".control-group").removeClass("error"), $(this).closest(".control-group").find("input").val(""))
        })
    },
    addBaby: function() {
        var e = $(".clonedInput:first-child").clone();
        $(".basic").selectric(), $(".allCatsContainer").on("click", "#addChild", function() {
            var s = $(".clonedInput").length,
                i = new Number(s - 1 + 1),
                a = e.clone().fadeIn("slow");
            return 3 > s ? (a.find(".heading-reference").attr("id", "ID" + i + "_reference").attr("name", "ID" + i + "_reference").html("Child #" + (i + 1)), a.find(".fnameLabel").attr("for", "ID" + i + "_title"), a.find(".fName").attr("id", "children[" + i + "].firstName").attr("name", "children[" + i + "].firstName").val("").attr("value", ""), a.find(".surLabel").attr("for", "ID" + i + "_sName"), a.find(".sName").attr("id", "children[" + i + "].lastName").attr("name", "children[" + i + "].lastName").val("").attr("value", ""), a.find(".genderLabel").attr("for", "ID" + i + "_gender"), a.find(".genderSelect").attr("id", "children[" + i + "].gender").attr("name", "children[" + i + "].gender").val(""), a.find(".dobLab").attr("for", "ID" + i + "_dobLab"), a.find(".childrenDob").attr("name", "children[" + i + "].date").val("").attr("id", "children" + i + "").val(""), a.find(".btnDate").attr("for", "children" + i + ""), a.find("input[type='hidden']").attr("class", "children" + i + ""), $(a).appendTo(".clone_wrap").find(".basic").selectric(), $("#children" + i + "").datepicker({
                startDate: "-3y",
                endDate: "current date",
                autoclose: true
            }).on('changeDate', function() {
                $(".children" + i + "").val($(this).val())
            }), void 0) : (alert("Sorry, You can add upto 3 childrens only"), !1)

        }), $(".allCatsContainer").on("click", ".btnDel", function() {
            var e = $(".clonedInput").length;
            1 != e && confirm("Are you sure you wish to remove this section? This cannot be undone.") && $(this).closest(".clonedInput").slideUp("slow", function() {
                $(this).closest(".clonedInput").remove(), $(".clonedInput").each(function(e) {
                    var s = e + 1;
                    var k = s - 1;
                    $(this).find(".heading-reference").attr("id", "ID" + k + "_reference").attr("name", "ID" + k + "_reference").html("Child #" + s),
                        $(this).find(".fnameLabel").attr("for", "ID" + k + "_title"),
                        $(this).find(".fName").attr("id", "children[" + k + "].firstName").attr("name", "children[" + k + "].firstName"),
                        $(this).find(".surLabel").attr("for", "ID" + k + "_sName"),
                        $(this).find(".sName").attr("id", "children[" + k + "].lastName").attr("name", "children[" + k + "].lastName").val("").attr("value", ""),
                        $(this).find(".genderLabel").attr("for", "ID" + k + "_gender"),
                        $(this).find(".genderSelect").attr("id", "children[" + k + "].gender").attr("name", "children[" + k + "].gender").val(""),
                        $(this).find(".dobLab").attr("for", "ID" + k + "_dobLab"),
                        $(this).find(".childrenDob").attr("name", "children[" + k + "].date").val("").attr("id", "children" + k + "").val(""),
                        $(this).find(".btnDate").attr("for", "children" + k + ""),
                        $(this).find("input[type='hiden']").attr("class", "children" + k + "")
                })
            })
            if (1 === e) {
                $(this).closest(".clonedInput").find("input").val("").attr("value", "");
                $(this).closest(".clonedInput").find("select").val("").children("option:first-child").attr("selected", "selected").siblings().removeAttr("selected");
            }
        })
    },
    readMorePpl: function() {
        $(".readMoreWrap .readMoreChild").each(function() {
            $(this).find("ul").children("li").length > 10 && ($(this).find(".allFacetValues").addClass("readMoreFacet"), $(this).find("ul").children("li:gt(9)").css("display", "none"), $(this).find(".read-more-facet").css("display", "block"), $(this).find(".read-less-facet").css("display", "none"))
        }), $(".read-more-facet").click(function(e) {
            e.preventDefault(), e.stopPropagation(), $(this).prev("ul.facet_block").children("li:gt(9)").css("display", "block"), $(this).css("display", "none"), $(this).siblings(".read-less-facet").css("display", "block")
        }), $(".read-less-facet").click(function(e) {
            e.preventDefault(), e.stopPropagation(), $(this).prev().prev("ul.facet_block").children("li:gt(9)").css("display", "none"), $(this).css("display", "none"), $(this).siblings(".read-more-facet").css("display", "block")
        })
    },
    basketQuantity: function() {
        $(".quantity").each(function() {
                var e = $(this).val();
                1 == e ? $(this).closest(".no_items").find(".minus").attr("disabled", "disabled") : $(this).closest(".no_items").find(".minus").removeAttr("disabled")
            }),
            $(".quantity").keypress(function(e) {
                var s = String.fromCharCode(e.which),
                    i = /^\d+$/,
                    t = $(this).val() + s;
                i.test(t) || e.preventDefault()
            })
    },
    checkoutBillingShow: function() {
        $("#billingAddress").click(function() {
            var isCheckedformAction = contextPathURL + '/checkout/multi/payment-method/useDeliveryAsBillingAddress';
            $(this).is(":checked") ? ($(".billinAvailable").slideDown(), $(".nobillinAvailable").slideUp(), $('#saveBillingAdddress').html('Continue'), $('#paymentDetailsForm').attr('action', isCheckedformAction), $('#availableBillingAddress').hide()) : ($(".nobillinAvailable").slideDown(), $(".billinAvailable").slideUp(), $('#saveBillingAdddress').html(currButtonVal), $('#paymentDetailsForm').attr('action', currActionURL), $('#availableBillingAddress').show())
        })
    },
    showDeliverAddress: function() {
        $(".addNew").click(function() {
            document.getElementById("address.line1").value = '';
            document.getElementById("address.line2").value = '';
            document.getElementById("address.Suburb").value = '';
            document.getElementById("address.townCity").value = '';
            document.getElementById("address.postcode").value = '';
            document.getElementById("address.deliveryInstructions").value = '';
            document.getElementById("address.country").value = '';
            $(".deliver_areaForm").css("display", "none");
            $(".deliver_areaSearch").slideDown();
            $(".store_areaSearch").slideDown();
            $('#addressForm').attr('action', contextPathURL + '/checkout/multi/delivery-address/add');
        })
    },
    initialize: function() {
        this.headerMenu(), this.header(), this.autoComplete(), this.showHideContent(), this.forgotPopup(), this.ClubCardNumberValidation(), this.newletterValidation(), this.addBaby(), this.readMorePpl(), this.basketQuantity(), this.checkoutBillingShow(), this.showDeliverAddress(), this.dobValidation(), this.signInBlockSetup()
    },
    getCategoryName: function(e) {
        var s = $.query.get("q");
        if (-1 != s.indexOf(e)) var i = ACC.clicks.parseStringBetween(e, ":", s);
        if ($("#facetName_" + i).text() != '') {
            $(".product-left").html("<h2>" + $("#facetName_" + i).text() + "</h2>")
        }
    },
    parseStringBetween: function(e, s, i) {
        var a = i.indexOf(e);
        if (-1 === a) return null;
        var t = e.length,
            o = a + t,
            n = i.indexOf(s, o);
        return -1 === n && (n = i.length), i.substring(o, n)
    },
    dobValidation: function() {
        $(".dob select").change(function() {
            var day = $(".dateCheck").val();
            var month = $(".monthCheck").val();
            var year = $(".yearCheck").val();
            var age = 18;
            var mydate = new Date();
            mydate.setFullYear(year, month - 1, day);

            var currdate = new Date();
            var setDate = new Date();
            setDate.setFullYear(mydate.getFullYear() + age, month - 1, day);

            if (!((currdate - setDate) > 0)) {
                $(this).parents(".dobSaOuterFirst").next("label.error").remove();
                $(this).parents(".dobSaOuterFirst").after("<label class='error'>You have to be 18 years or older to join ClubCard.</label>");
            } else {
                $(this).parents(".dobSaOuterFirst").next("label.error").remove();
            }
        });
    },
    signInBlockSetup: function() {
        var f = ACC.config.contextPath + "/login";
        $.ajax({
            type: "GET",
            url: f,
            success: function(e) {
                var s = $(e).find(".userLogin");
                signInBlockClass = $(window).width() > 768 ? ".login" : ".login-blk ",
                    $(signInBlockClass).html(s), mobParam = $(window).width() > 768 ? "" : ".login-blk ", ACC.clicks.signInValidation(mobParam), $(mobParam + " .forgot-block").on("click", function() {
                        var e = $(this).attr("data-url");
                        0 == $(signInBlockClass + " .fpassword-block").length ? ($.ajax({
                            type: "GET",
                            url: e,
                            success: function(e) {
                                $(e).find(".forgottenPwd");
                                $("<div/>", {
                                    "class": "fpassword-block"
                                }).appendTo(signInBlockClass), $(".fpassword-block").html(e), $(".go-back").on("click", function(e) {
                                    e.preventDefault(), $(".fpassword-block").hide(), $(".userLogin").show()
                                })
                            },
                            error: function() {}
                        }), $(this).closest(mobParam + ".userLogin").hide(), $(".fpassword-block").show()) : ($(this).closest(mobParam + ".userLogin").hide(), $(".fpassword-block").show())
                    }), ACC.product.loginFromPopup();
                $(".signin-block .acc-links-block .acc-drop-down").clone().appendTo(".login-blk");
            },
            error: function() {}
        });
    }

}, $(document).ready(function() {
	if(showButton == 'false')
	{
		$('#saveBillingAdddress').hide();
	}
    var val = $("#gaTrackingCA").val();
    if (val == "true") {
        ga('send', 'event', 'online account created', 'outside of checkout', 'successful');

    }

    var val = $("#gaTrackingChechout").val();
    if (val == "true") {
        ga('send', 'event', 'online account created', 'within checkout', 'successful');

    }

    $(".signin-block a.sign-in").click(function(e) {
        if (!$(this).hasClass("signedInUser")) {
            if ($(".login.uppermenubox").children("div").hasClass("userLogin")) {
                $(".login.uppermenubox").slideToggle().parents("li").toggleClass("open");
                if (!$(this).parent("li").hasClass("open")) {
                    $("#div-overlay").css({
                        "display": "none",
                        "position": "absolute"
                    });
                } else {
                    $("#div-overlay").css({
                        "display": "block",
                        "position": "fixed"
                    });
                }
                e.stopImmediatePropagation();
            } else {
                ACC.clicks.signInBlockSetup();
            }
        }
    });

    $(document).on("bind click", ".addToCartButtonSubmit", function() {
        ACC.product.bindToAddToCartForm();
        var e = $(this).parents(".add_to_cart_form").find("input[name=productCode]").val();
        var s = $(this).parents(".add_to_cart_form").find("input[name=gatitle]").val();
        var i = $(this).parents(".add_to_cart_form").find("input[name=gabrand]").val();
        var a = $(this).parents(".add_to_cart_form").find("input[name=pageStatus]").val();
        var c = $(this).parents(".add_to_cart_form").find("input[name=gaList]").val();
        var p = $(this).parents(".add_to_cart_form").find("input[name=gaPosition]").val();

        if (typeof c != "undefined") {
            gaTrackingClp(e, s, i, a, c, p);
        } else {
            gaTracking(e, s, i, a);

        }
    });

    var everythingLoaded = setInterval(function() {
        if (/loaded|complete/.test(document.readyState)) {
            clearInterval(everythingLoaded);
            var windowWidth = $(window).width();
            if (windowWidth > 768) {
                $(".healthTwitter iframe#twitter-widget-0").contents().find(".timeline-Widget").css("max-width", "100%");
                $(".healthTwitter iframe#twitter-widget-0").contents().find(".timeline-TweetList-tweet").css({
                    "width": "33%",
                    "float": "left",
                    "min-height": "450px"
                });
                $(".healthTwitter iframe#twitter-widget-0").contents().find(".timeline-Tweet-text").css({
                    "font-size": "inherit",
                    "line-height": "inherit"
                });
            } else {
                $(".healthTwitter iframe#twitter-widget-0").contents().find(".timeline-Widget").css("max-width", "100%");
                $(".healthTwitter iframe#twitter-widget-0").contents().find(".timeline-Tweet-text").css({
                    "font-size": "inherit",
                    "line-height": "inherit"
                });
            }
        }
    }, 10);

    $(".icon-sign-in").click(function() {
        if (!$(this).hasClass("signedInUser")) {
            if ($(".login-blk.mobile-tabs").children("div").hasClass("userLogin")) {
                $(".login-blk.mobile-tabs").slideToggle();
            } else {
                ACC.clicks.signInBlockSetup();
            }
        } else {
            $(".login-blk").empty();
            $(".signin-block .acc-links-block .acc-drop-down").clone().appendTo(".login-blk");
        }
    });

    $(".nav-list > li").hoverIntent(function() {
            $(this).addClass("active").siblings().removeClass("active");
            setTimeout(function() {
                $(".nav-list > li.active").find("section").css({
                        "visibility": "visible",
                        "display": "block"
                    }).parents("li").siblings().find("section").css({
                        "visibility": "hidden",
                        "display": "none"
                    }),
                    $("footer").css("border-top", "0px")
            }, 600);
            $("#menu-overlay, #menu-overlay-footer").show()
        },
        function() {
            setTimeout(function() {
                $(".nav-list > li").find("section").css({
                        "visibility": "hidden",
                        "display": "none"
                    }),
                    $("#menu-overlay, #menu-overlay-footer").hide(),
                    $("footer").css("border-top", "0px")
            }, 0);
        });

    $(".checkout-content .edit").bind("click", function(){
    	$("#paymentDetailsBillingForm").css("display", "none");
    });
    $("#user-login-page .forgot-block").click(function(e) {
        ACC.clicks.forgotPopup();
        if ($(window).width() <= 768) {
            $(".icon-sign-in").addClass("active"), $(".userLogin").hide(), $(".fpassword-block").show(), $(".login-blk").css("z-index", "9999").slideToggle(400, "swing", function() {
                $("html, body").animate({
                    scrollTop: $(this).offset().top
                }, "slow")
            })
        }
    });
    if (($(window).width()) <= 768) {
        $("#div-overlay").on("click", function() {
            $(".login-blk").slideUp(),
                $(".icon-sign-in").removeClass("active"),
                $(".userLogin").show(),
                $(".fpassword-block").hide()
        })
    }
    $("#user-login-page input[name='_spring_security_remember_me']").attr("id", "_spring_security_remember_me2").siblings("label").attr("for", "_spring_security_remember_me2");

    $(".signedInUser").before("<span class='signedInUsericon'></span>");
    $(".basket").before("<span class='mini-basket-icon'></span>");

    $(".signedInUsericon").hover(function() {
        $(".acc-links-block").slideDown(function() {
            $("#div-overlay").css({
                "display": "block",
                "position": "fixed"
            });
            $(this).parents("li").addClass("open").siblings("li").removeClass("open");
            $(this).hover(function() {
                $(this).css("display", "block");
                $("#div-overlay").css({
                    "display": "block",
                    "position": "fixed"
                });
            }, function() {
                $(".acc-links-block").slideUp();
                $("#div-overlay").css({
                    "display": "none",
                    "position": "absolute"
                });
                $(this).parents("li").removeClass("open");
            });
        });
    });

    $(document).on("click", ".addTobasketpopup .addQuantity button", function() {
        var incPlus = $(this).attr("class");
        if (incPlus == "updateProductQuantity btn-default") {
            var initialQty = parseInt($(".addTobasketpopup .addQuantity .quantity").val());
            var initialPdpQty = parseInt($("#addToCartForm .quantity").val());
            initialPdpQty += 1;
            initialQty += 1;
            $("#addToCartForm .quantity").attr("value", initialPdpQty);
            $(".addTobasketpopup .addQuantity .quantity").attr("value", initialQty);
        } else {
            var initialQty = parseInt($(".addTobasketpopup .addQuantity .quantity").val());
            var initialPdpQty = parseInt($("#addToCartForm .quantity").val());
            if (initialQty > 1) {
                initialQty -= 1;
                initialPdpQty -= 1;
                $("#addToCartForm .quantity").attr("value", initialPdpQty);
                $(".addTobasketpopup .addQuantity .quantity").attr("value", initialQty);
            }
        }
    });

    var cpb = $("#checkoutProgress li").length;
    if (cpb == 4) {
        $("#checkoutProgress li").css("width", "23%");
    } else {
        $("#checkoutProgress li").css("width", "31%");
    }

    $('#children0').datepicker({
        startDate: "-3y",
        endDate: "current date",
        autoclose: true
    });
    $('#children0').datepicker().on('changeDate', function() {
        $(".children0").val($(this).val());
    });
    $('#children1').datepicker({
        startDate: "-3y",
        endDate: "current date",
        autoclose: true
    });
    $('#children1').datepicker().on('changeDate', function() {
        $(".children1").val($(this).val());
    });
    $('#children2').datepicker({
        startDate: "-3y",
        endDate: "current date",
        autoclose: true
    });
    $('#children2').datepicker().on('changeDate', function() {
        $(".children2").val($(this).val());
    });

    $(".details-block").each(function() {
        if ($(this).find('h4').is(':empty')) {
            $(this).find('h4').css("display", "none").siblings("p").css({
                "display": "table-cell",
                "height": "70px",
                "vertical-align": "middle"
            });
        }
        if ($(this).find('p').is(':empty')) {
            $(this).find('p').css("display", "none").siblings("h4").css({
                "display": "table-cell",
                "height": "70px",
                "vertical-align": "middle"
            });
        }
    });

    $('.collapseMap').click(function() {
        if ($(this).siblings().hasClass("innerSiteMap")) {
            $(this).siblings(".innerSiteMap").slideToggle().parent("li").toggleClass("active").siblings("li").removeClass("active").children("ul.innerSiteMap").slideUp();
        }
    });
    $(".siteMap li").each(function() {
        if (!$(this).children("ul").length) {
            $(this).find(".collapseMap").addClass("noCollapse");
        }
    });
    $(".editEmail").prop("disabled", true);
    $(".editPwdCheckbox").click(function() {
        if ($(this).is(":checked")) {
            $(".editEmail").prop("disabled", false);
            $(".editPwdInput").css("display", "block");
            $(".edit-email-checkbox-validation").css("display", "block");
        } else {
            $(".editEmail").prop("disabled", true);
            $(".editPwdInput").css("display", "none");
            $(".edit-email-checkbox-validation").css("display", "none");
        }
    });
    $("[id=contact_email]").val($('#orderConfirmationEmail').text());
    $(".mini-basket-count").clone().appendTo(".icon-cart");
    $(".storeSearchPostCode").attr("placeholder", "Postcode");

    var addNewAddUrl = $(".addnewurl").attr("value");
    $(".addNewAdd").click(function() {
        if ($(".savedAddnew").hasClass("active")) {
            $(".savedAddnew").find('#addressForm').attr("action", addNewAddUrl).find("input[type=text], select").val("");
        } else {
            $(".savedAddnew").slideToggle().toggleClass("active");
            $(".savedAddnew").find('#addressForm').find("input[type=text], select").val("");
        }
    });
    if (($("#addressForm input[name='line1']").val()) != "") {
        $(".savedAddnew").slideDown().addClass("active");
    }
    if ($('#editDeliveryForm').val() === 'true') {
        $(".deliver_areaSearch").css("display", "block");
        $(".store_areaSearch").css("display", "block");
        $(".deliver_areaForm").css("display", "block");
        document.getElementById("address.postcode.show").value = document.getElementById("address.postcode").value;
    }
    if ($('#hasErrors').val() === 'true') {
        $('#paymentDetailsForm').attr('action', currActionURL);
        $('#saveBillingAdddress').removeAttr('disabled');
    } else {
        if ($('#editForm').val() === 'false' || $('#editForm').val() === '') {
            $(".billinAvailable").slideDown();
            $("#billingAddress").prop('checked', true);
            $('#paymentDetailsForm').attr('action', contextPathURL + '/checkout/multi/payment-method/useDeliveryAsBillingAddress');
        } else if ($('#editForm').val() === 'true') {
            $('#paymentDetailsForm').attr('action', contextPathURL + '/checkout/multi/payment-method/edit');
            $('#saveBillingAdddress').removeAttr('disabled');
        }
    }
    $("#displayPaymentBillingAddForm").click(function(e) {
    	$('#saveBillingAdddress').show();
        e.preventDefault();
        $("#billingAddress").prop('checked', false);
        $(".billinAvailable").slideUp();
        $("#paymentDetailsBillingForm").css("display", "block");
        $('#paymentDetailsForm').attr('action', contextPathURL + '/checkout/multi/payment-method/add');
        currButtonVal = 'Continue';
        $('#saveBillingAdddress').html('Continue');
        $('#saveBillingAdddress').removeAttr('disabled');
        document.getElementById("address.firstName").value = '';
        document.getElementById("address.surname").value = '';
        document.getElementById("address.email").value = '';
        document.getElementById("address.line1").value = '';
        document.getElementById("address.line2").value = '';
        document.getElementById("address.suburb").value = '';
        document.getElementById("address.townCity").value = '';
        document.getElementById("address.postcode").value = '';
        var textToFind = '';
        var dd = document.getElementById('address.billing.province');
        for (var i = 0; i < dd.options.length; i++) {
            if (dd.options[i].value === textToFind) {
                dd.selectedIndex = i;
                break;
            }
        }
        document.getElementById("billingAddress.addressId").value = '';
    });

    function e() {
        $("#loader").hide(), $.ajax({
            url: "store-results.html",
            method: "post",
            beforeSend: function() {
                $("#loader").show().fadeOut("slow", 100)
            },
            success: function(e) {
                "" != e && setTimeout(function() {
                    $(".store-info li").after(e), $("#loader").hide()
                }, 100)
            }
        })
    }

    $('#saveBillingAdddress').click(function(e) {
        e.preventDefault();
        if ($('#saveBillingAdddress').html().trim() == 'Update') {
            var formAction = contextPathURL + '/checkout/multi/payment-method/edit';
            //set form action
            $('#paymentDetailsForm').attr('action', formAction);
        } else if ($('#billingAddress').prop("checked") == false) {
            var formAction = contextPathURL + '/checkout/multi/payment-method/add';
            //set form action
            $('#paymentDetailsForm').attr('action', formAction);
        }
        //submit form
        $('#paymentDetailsForm').submit();
    });

    $("a[id$='_useBillingAddressDetails']").click(function(e) {
        e.preventDefault();
        var pk = this.id.split("_")[0];
        var formAction = contextPathURL + '/checkout/multi/payment-method/useExistingBillingAddress';
        //set form action
        $('#paymentDetailsForm').attr('action', formAction);
        document.getElementById("billingAddress.addressId").value = pk;
        //submit form
        $('#paymentDetailsForm').submit();
    });

    $("a[id$='_editBillingAddressDetails']").click(function(e) {
        e.preventDefault();
        currButtonVal = 'Update';
        $('#saveBillingAdddress').html('Update');
        $('#saveBillingAdddress').removeAttr('disabled');
        $("#billingAddress").prop('checked', false);
        $(".billinAvailable").slideUp();
        document.getElementById("address.firstName").value = '';
        document.getElementById("address.surname").value = '';
        document.getElementById("address.email").value = '';
        document.getElementById("address.line1").value = '';
        document.getElementById("address.line2").value = '';
        document.getElementById("address.suburb").value = '';
        document.getElementById("address.townCity").value = '';
        document.getElementById("address.postcode").value = '';
        var textToFind = '';
        var dd = document.getElementById('address.billing.province');
        for (var i = 0; i < dd.options.length; i++) {
            if (dd.options[i].value === textToFind) {
                dd.selectedIndex = i;
                break;
            }
        }
        document.getElementById("billingAddress.addressId").value = '';
        $("#paymentDetailsBillingForm").css("display", "block");
        var pk = this.id.split("_")[0];
        if (undefined != document.getElementById(pk + "_firstname")) {
            document.getElementById("address.firstName").value = document.getElementById(pk + "_firstname").value;
        }
        if (undefined != document.getElementById(pk + "_lastname")) {
            document.getElementById("address.surname").value = document.getElementById(pk + "_lastname").value;
        }
        if (undefined != document.getElementById(pk + "_email")) {
            document.getElementById("address.email").value = document.getElementById(pk + "_email").value;
        }
        if (undefined != document.getElementById(pk + "_streetname")) {
            document.getElementById("address.line1").value = document.getElementById(pk + "_streetname").value;
        }
        if (undefined != document.getElementById(pk + "_streetnumber")) {
            document.getElementById("address.line2").value = document.getElementById(pk + "_streetnumber").value;
        }
        if (undefined != document.getElementById(pk + "_suburb")) {
            document.getElementById("address.suburb").value = document.getElementById(pk + "_suburb").value;
        }
        if (undefined != document.getElementById(pk + "_town")) {
            document.getElementById("address.townCity").value = document.getElementById(pk + "_town").value;
        }
        if (undefined != document.getElementById(pk + "_postalcode")) {
            document.getElementById("address.postcode").value = document.getElementById(pk + "_postalcode").value;
        }
        if (undefined != document.getElementById(pk + "_province")) {
            var textToFind = document.getElementById(pk + "_province").value;

            var dd = document.getElementById('address.billing.province');
            for (var i = 0; i < dd.options.length; i++) {
                if (dd.options[i].value === textToFind) {
                    dd.selectedIndex = i;
                    break;
                }
            }
        }
        document.getElementById("billingAddress.addressId").value = pk;

    });

    function s(e) {
        var s = 1;
        $(".filterContent .plpsep ").each(function() {
            $(this).addClass("plpsep-" + s), s++
        });
        var i = $(".filterContent .plpsep").length,
            a = Math.ceil(i / e);
        for (p = 1; a >= p; p++) {
            var t = "<div class='productRow clearfix horiz-" + p,
                o = "'></div>";
            $(".filterContent").append(t + o);
            var n = p * e;
            for (j = n - (e - 1); n >= j; j++) $(".plpsep-" + j).appendTo($(".horiz-" + p))
        }
    }

    function i(e, s) {
        return $(".formCADis").css("opacity") < 1 ? !1 : ($("." + s).css("display", "none"), $("." + s).val(""), $("." + e).css("display", "block"), void $(".dob select").selectric("refresh"))
    }

    function a(e, s) {}

    function o() {
        $(".accordion .accordion-section-title").removeClass("active"), $(".accordion .accordion-section-content").slideUp(300).removeClass("open")
    }
    $("#div-overlay").appendTo($("body")).css({
        display: "none"
    }), $("#menu-overlay").appendTo($(".main-container")), $("#menu-overlay-footer").appendTo($("footer")), ACC.clicks.initialize(), $(".promotion-body").find(".border-block:odd").addClass("even"), $(".quick-links .dropdown-toggle").click(function(e) {
        $(this).closest(".dropdown").addClass("open").find(".dropdown-list").slideToggle(400, "swing", function() {
            $("#div-overlay").css("position", "fixed");
            ACC.cartremoveitem.bindAll();
        }), $(this).closest(".dropdown").siblings().removeClass("open"), $(this).next(".dropdown-list").is(":visible") ? ($(this).parent(".dropdown").addClass("open"), $(this).next(".dropdown-list").find(">div").hide(), $(this).next(".dropdown-list").find(".userLogin").show()) : $(this).closest(".dropdown").removeClass("open"), e.preventDefault()
    }), $(".header .dropdown > a").on("click", function(e) {
        var e = document.getElementById("div-overlay");
        e.style.display = "none" == e.style.display ? "block" : "none"
    }), $(".find-store, .basket-list, .login").mouseleave(function() {}), $(".nav-list.offers > li:nth-child(1)").addClass("nav-news"), $(".nav-list.offers > li:nth-child(2)").addClass("nav-promo"), $(".nav-list.offers > li:nth-child(3)").addClass("nav-clubcard"), $(".menu-list-links > li:last-child a:contains('All ')").addClass("link"), $(".menu-list-links > li:last-child a:contains('Read more')").addClass("link");
    var n = $(".category_carousel").length > 0 ? !1 : !0,
        l = $(".carouselCount").length > 0 ? 4 : 3;
    $(".btn-load-more").click(function() {
        e()
    });
    var r = $(window).width(),
        c = $(window).height();
    992 > r ? 480 > r || r > 991 ? r > 479 || s(2) : s(2) : s(3), $(".filter .panel-default > a").click(function(e) {
        e.preventDefault()
    }), $(".facet_block li").click(function(e) {
        e.stopImmediatePropagation();
    }), $(".filter .panel-group ul li a").click(function() {
        $(this).toggleClass("activeLink")
    }), $(".filter .panel-group > .panel > a").click(function() {
        if (980 >= r && c >= 320) {
            var e = $(this).closest(".panel-default").find(".panel-collapse");
            $(".panel-default .panel-collapse").not(e).slideUp(function() {
                "none" == $(".panel-group .panel-default .panel-collapse").css("display") && $(this).parent().removeClass("closed")
            });
            var s = $(this).closest(".filter").find(".no-toggle");
            s.find("ul").addClass("panel-collapse")
        }
        $(this).parent().find(".panel-collapse").slideToggle(), $(this).parent().toggleClass("closed")
    });
    $(".filter .panel-group > li > a").click(function() {
        if ($(this).siblings("ul.collapse").hasClass("in")) {
            $(this).siblings("ul.collapse").removeClass("in").parents("li").removeClass("closed");
        } else {
            $(this).siblings("ul.collapse").addClass("in").parents("li").addClass("closed");
        }
    }); {
        var d = $("footer").outerHeight();
        $(".push").height(d)
    }
    $("#page").css({
        marginBottom: "-" + d + "px"
    }), window.location.href.indexOf("all") > -1 ? ($(".store-locator .store-map ul li").removeClass("active"), $(".store-locator .store-map ul li:first-child").addClass("active")) : window.location.href.indexOf("pharmacy") > -1 ? ($(".store-locator .store-map ul li").removeClass("active"), $(".store-locator .store-map ul li:nth-child(2)").addClass("active")) : window.location.href.indexOf("clinic") > -1 && ($(".store-locator .store-map ul li").removeClass("active"), $(".store-locator .store-map ul li:last-child").addClass("active")), $(".imageOuterThumb a").click(function() {
        var e = $(this).attr("href");
        return $(".imageOuter img").stop().animate({
            opacity: "0"
        }, function() {
            $(this).attr("src", e)
        }).load(function() {
            $(this).stop().animate({
                opacity: "1"
            })
        }), !1
    }), $(".acc-row").each(function() {
        var e = 0;
        $(this).children(".alpha").each(function() {
            var s = $(this).height();
            s > e && (e = s)
        }), $(".acc-col").height(e)
    }), $(".acc-row-2").each(function() {
        var e = 0;
        $(this).children(".alpha").each(function() {
            var s = $(this).height();
            s > e && (e = s)
        }), $(".acc-col-2").height(e)
    }), $(".acc-row-3").each(function() {
        var e = 0;
        $(this).children(".alpha3").each(function() {
            var s = $(this).height();
            s > e && (e = s)
        }), $(this).find(".acc-col-3").height(e)
    }), $(".allCatsContainer").css("display", "none"), $(".topCats label.radio-box").click(function() {
        $(this).addClass("current"), $(".allCats").find(".radio-box").removeClass("current"), $(".topCatsContainer").slideDown(function() {
            $(".topCatsContainer").css("overflow", "")
        }), $(".allCatsContainer").slideUp()
    }), $(".allCats label.radio-box").click(function() {
        $(this).addClass("current"), $(".topCats").find(".radio-box").removeClass("current"), $(".allCatsContainer").slideDown(function() {
            $(".allCatsContainer").css("overflow", "")
        }), $(".topCatsContainer").slideUp()
    }), $(".dobSaOuter").css("display", "none"), $(".clubcardhide").click(function() {
        var e = $(this).data("toshow"),
            s = $(this).data("tohide");
        i(e, s)
    }), $("#aboutFamily .dob select").removeAttr("disabled"), $(".acc-row-6").each(function() {
        var e = 0;
        $(this).children(".alpha6").each(function() {
            var s = $(this).height();
            s > e && (e = s)
        }), $(this).find(".acc-col-6").height(e)
    }), $(".acc-row-7").each(function() {
        var e = 0;
        $(this).children(".alpha7").each(function() {
            var s = $(this).height();
            s > e && (e = s + 20)
        }), $(this).find(".acc-col-7").height(e)
    });
    var h = 200;
    $(".show-read-more p").each(function() {
        var e = $(this).text();
        if ($.trim(e).length > h) {
            var s = e.substring(0, h),
                i = e.substring(h, $.trim(e).length);
            $(this).empty().html(s), $(this).append('<span class="more-text">' + i + "</span>")
        }
    }), $(".read-more").click(function() {
        $(".more-text").show(), $(".read-more").hide()
    });
    var p = 0;
    $(".share").click(function() {
        0 == p ? ($(".shareProduct").show(), p++) : ($(".shareProduct").hide(), p = 0)
    }), $('button[data-dismiss="modal"]').click(function() {
        var e = $(this).parent(".modal-footer").siblings("div.modal-body").find("iframe"),
            s = e.attr("src");
        e.attr("src", ""), e.attr("src", s)
    }), $(".trascTable").hide(), $(".transcTab").click(function() {
        var e = $(this);
        $(".trascTable").slideToggle("slow", function() {
            e.text($(this).is(":visible") ? "hide transaction history" : "see transaction history")
        })
    });
    var u = "Unknown OS"; - 1 != navigator.appVersion.indexOf("Win") && (u = "Windows"), -1 != navigator.appVersion.indexOf("Mac") && (u = "MacOS"), -1 != navigator.appVersion.indexOf("X11") && (u = "UNIX"), -1 != navigator.appVersion.indexOf("Linux") && (u = "Linux"), "MacOS" == u && $("body").addClass("mac-font"), $(document).on("click", function() {}), $(document).click(function(e) {
        e.stopPropagation();
        var s = $(".dropdown");
        if (0 === s.has(e.target).length && "div-overlay" == e.target.id) {
            $(".dropdown-list").slideUp();
            $(".dropdown").removeClass("open");
            var e = document.getElementById("div-overlay");
            e.style.display = "none"
        }
    });
    var r = $(window).width(),
        c = $(window).height();
    if (r > 768 || $(".jumper").on("click", function(e) {
            var s = $($(this).attr("href"));
            s.length && (e.preventDefault(), $("html, body").animate({
                scrollTop: $(".healthTabWrapper").offset().top
            }, 1e3))
        }), $(window).width() < 769) {
        $(".home-promotions").find(".pad-zero").css("width", "100%");
        var g = $(".product-slider").attr("data-classname"),
            m = $(".product-slider").attr("data-minslide");
        a(g, m)
    }
    var g = $(".slider-component").attr("data-classname"),
        v = $(".slider-component").attr("data-maxslide");
    $(".accordion-section-title").click(function(e) {
        var s = $(this).attr("href");
        $(e.target).is(".active") ? o() : (o(), $(this).addClass("active"), $(".accordion " + s).slideDown(300).addClass("open")), e.preventDefault()
    });
    var b = location.href.toLowerCase();
    if ($(".commonTab li a").each(function() {
            b.indexOf(this.href.toLowerCase()) > -1 && ($(".commonTab li").removeClass("active"), $(this).parent().addClass("active"))
        }), $("#dobSaOuter1").css("display", "none"), $(".registerOuter #saResiReg").on("click", function() {
            $(".registerOuter #dobSaOuter1").css("display", "block").parent().find("#ClubcardValue1").css("display", "none")
        }), $(".registerOuter #saResi12").on("click", function() {
            $(".registerOuter #dobSaOuter1").css("display", "none").parent().find("#ClubcardValue1").css("display", "block")
        }), $("#shop").length > 0);
    else {
        var C = ACC.config.contextPath + "/health";
        $(".healthbreadcrumb").text("Health Hub Home"), $(".healthbreadcrumb").attr("href", "" + C), $("ul.nav-tabs > li").removeClass("active"), $("ul.nav-tabs li:last-child").addClass("active")
    }
    $(".inputDsabled input").attr("readOnly", "true"), $(".inputClubcardPlaceholder input").attr("placeholder", "Please enter your ClubCard number"), $(".inputSAIDPlaceholder input").attr("placeholder", "Please enter your SA ID number"), $("#dialog").dialog({
        resizable: !0,
        autoOpen: !1,
        width: 550,
        height: 300,
        show: {
            effect: "fadeIn",
            duration: 1e3,
            delay: 250
        },
        hide: {
            effect: "explode",
            duration: 1e3
        }
    }), $("#opener").click(function(e) {
        $("#dialog").dialog("open"), $("#headerPicUpload").show(), e.preventDefault()
    }), $(".cusPicUploadbtn").click(function(e) {
        if (0 == $("#file").val().length) e.preventDefault(), $("#picExtErrorMsg").hide(), $("#picErrorMsg").show();
        else {
            var s = $("#file").val().split(".").pop().toLowerCase(); - 1 == $.inArray(s, ["gif", "png", "jpg", "jpeg"]) && (e.preventDefault(), $("#picErrorMsg").hide(), $("#picExtErrorMsg").show())
        }
    }), $("#deletePhoto").click(function(e) {
        $("#cusProfilePic").length > 0 && !confirm("Are you sure you want to remove your profile picture?") && e.preventDefault()
    }), $("#picUploadMessage").fadeIn().delay(4e3).fadeOut(), $(".marketing-update").click(function() {
        $(this).is(":checked") ? $(".markettingMessage").hide() : $(".email-mar.marketing-update").is(":checked") || $(".sms-mar.marketing-update").is(":checked") || ($(".markettingMessage").show(), $(".marketing-update").attr("checked", "checked"), $(this).removeAttr("checked"))
    }), $(window).width() < 768 || $(".no-toggle a").off("click"), $(".nav-list > li").hover(function() {
        $(this).addClass("active")
    }, function() {
        $(this).removeClass("active")
    }), $(".accLandOuter .redCommon").length > 0 && $("#isSAResident2").is(":checked") && ($("#bb").css("display", "none"), $(".dobSaOuterFirst").css("display", "block")), $(".color li").each(function() {
        var e = $(".colorControls").html();
        $(".colorControls").html(e), $(this).hover(function() {
            var e = $(this).find(".swatch_colour_a").attr("title");
            $(".colorControls").html(e)
        }, function() {
            $(".colorControls").html(e)
        })
    });
    var k = $(".color").height();
    k > 135 && ($(".color").css("height", "135"), $(".seeMore").css("display", "block"), $(".seeMore").click(function(e) {
        e.preventDefault(), $(".color").css("height", "auto"), $(".seeMore").css("display", "none"), $(".seeLess").css("display", "block")
    }), $(".seeLess").click(function(e) {
        e.preventDefault(), $(".color").css("height", "135"), $(".seeMore").css("display", "block"), $(".seeLess").css("display", "none")
    })), $(".tabContentWrapper .tabContent").each(function() {
        var e = $(this).height(),
            s = $(".tabContent").css("line-height"),
            i = Math.round(e / parseInt(s));
        i > 20 && ($(this).css({
            height: "402px"
        }), $(this).find(".tabSeeMore").css("display", "block"), $(this).find(".tabSeeMore").click(function(e) {
            e.preventDefault(), $(this).parents().find(".tabContent.active").css("height", "auto"), $(this).css("display", "none"), $(this).siblings(".tabSeeLess").css("display", "block")
        }), $(this).find(".tabSeeLess").click(function(e) {
            e.preventDefault(), $(this).parents().find(".tabContent.active").css("height", "402px"), $(this).css("display", "none"), $(this).siblings(".tabSeeMore").css("display", "block")
        }))
    }), -1 != $(location).attr("href").indexOf("OH1") && -1 != $(location).attr("href").indexOf("allPromotions") ? ACC.clicks.getCategoryName("allPromotions:") : -1 != $(location).attr("href").indexOf("collections") && ACC.clicks.getCategoryName("collections:"), $(".conditionMVSHeader").text($("#condnHeader").val());
    var w = $(".bxslider-ss-outer #pagerSS #bx-pager a").width(),
        y = $(".bxslider-ss-outer #pagerSS #bx-pager a").length,
        S = w * y;
    $(".bxslider-ss-outer #pagerSS #bx-pager").css("width", S), $("#bx-pager").find("a:nth-child(" + $("#pagerSS").width() / w + ")").addClass("csright"), $("#pagerSS a").click(function() {
        var e = this;
        if (!$(this).is(":first-child") && !$(this).is(":last-child")) {
            var s, i, a = $(this).closest("#bx-pager").css("margin-left"),
                t = parseInt($("#pagerSS").width() + Math.abs(parseInt(a))),
                o = parseInt($("#pagerSS").width() / w),
                n = parseInt($(this).attr("data-slide-index"));
            if ($(this).hasClass("csleft")) s = n, rightA = s + o - 1, i = -w * (t / w - o - 1);
            else {
                if (!$(this).hasClass("csright")) return;
                s = n - o + 3, rightA = s + o - 1, i = -w * (t / w - o + 1)
            }
            $(".bxslider-ss-outer #pagerSS #bx-pager").css("margin-left", i), setTimeout(function() {
                $(e).closest("#bx-pager").find("a").removeClass("csleft csright"), $(e).closest("#bx-pager").find("a:nth-child(" + s + ")").addClass("csleft"), $(e).closest("#bx-pager").find("a:nth-child(" + rightA + ")").addClass("csright")
            }, 500)
        }
    }), $("#editNonClubCardForm .saResident input[type=radio]").change(function() {
        $("#isSAResident1").is(":checked") && ($("#editNonClubCardForm #bb").css("display", "block"), $("#editNonClubCardForm #cc").css("display", "none")), $("#isSAResident2").is(":checked") && ($("#editNonClubCardForm #bb").css("display", "none"), $("#editNonClubCardForm #cc").css("display", "block"))
    }), $("input").bind("keyup change blur", function() {
        $(this).hasClass("error") ? $(this).next("label.error").css("display", "block") : $(this).next("label.error").css("display", "none")
    }), $("select.reportLostCard option").removeAttr("selected"), $('select.reportLostCard option:contains("Lost")').attr("selected", "selected");
    if ($("#saResi1").is(":checked")) {
        $(".dobSaOuter").css("display", "none");
        $("#saValueCR").css("display", "block");
    }
    if ($("#saResi").is(":checked")) {
        $("#saValueCR").css("display", "none");
        $(".dobSaOuter").css("display", "block");
    }
    $("#clubCardRegister .saResident input[type='radio']").change(function() {
        if ($("#saResi1").is(":checked")) {
            $(".dobSaOuter").css("display", "none");
            $("#saValueCR").css("display", "block");
        }
        if ($("#saResi").is(":checked")) {
            $("#saValueCR").removeClass("error").css("display", "none");
            $("#saValueCR").next(".error").css("display", "none");
            $(".dobSaOuter").css("display", "block");
        }
    });



    if ($("#currentPregnant").is(":checked")) {
        $(this).next(".radio-box").addClass("current");
        $("#alreadyParent").next(".radio-box").removeClass("current");
        $(".topCatsContainer").slideDown(function() {
            $(".topCatsContainer").css("overflow", "")
        });
        $(".allCatsContainer").slideUp();
    }
    if ($("#alreadyParent").is(":checked")) {
        $(this).next(".radio-box").addClass("current");
        $("#currentPregnant").next(".radio-box").removeClass("current");
        $(".allCatsContainer").slideDown(function() {
            $(".allCatsContainer").css("overflow", "")
        });
        $(".topCatsContainer").slideUp();
    }

    $('.categoryLanding .home-promo-text h4').each(function() {
        var el = $(this);
        var textLength = el.html().length;
        if (textLength > 22) {
            el.css({
                'font-size': '2em',
                'line-height': '1.5em'
            });
        }
    });
    $("#editClubCardForm .saResident input[type=radio]").change(function() {
    	$('#bb').val('');
        if ($("#SAResident2").is(":checked")) {
            $("#bb").removeClass("error");
            $("#bb-error").css("display", "none");
        }
    });

}), "true" == $("#isSizeVariantValEmpty").val() && $("#variantSizesHideID").hide(), window.setTimeout(function() {
    $(".twitter-timeline").contents().find(".timeline").css("max-width", "1024px")
}, 1e3);
$(window).resize(function() {
    if ($(window).width() <= 991) {
        $('.filterContent .productRow').contents().unwrap();
        var $div = $('.filterContent .plpsep'),
            length = $div.length;
        for (var i = 0; i < length; i = i + 2) {
            $div.slice(i, i + 2).wrapAll('<div class="productRow clearfix" />')
        }
    }
    if ($(window).width() >= 992) {
        $('.filterContent .productRow').contents().unwrap();
        var $div = $('.filterContent .plpsep'),
            length = $div.length;
        for (var i = 0; i < length; i = i + 3) {
            $div.slice(i, i + 3).wrapAll('<div class="productRow clearfix" />')
        }
    }
    if ($(window).width() <= 768) {
        $(".login.uppermenubox").find(".userLogin").remove();
    }
    if ($(window).width() > 768) {
        $(".login-blk").find(".userLogin").remove();
    }
    if (($(window).width()) <= 768) {
        $(".icon-cart .mini-basket-count").remove();
        $(".mini-basket-count").clone().appendTo(".icon-cart");
        if ($(".basket-blk .basket-list-inner").children().hasClass("bx-wrapper")) {
            setTimeout(function() {
                $(".basket-blk .mini-basket-product-list li.popupCartItem").each(function() {
                    var screenWidth = $(window).width();
                    var sliderWidth = parseInt(screenWidth - 30)
                    $(".mini-basket-product-list li.popupCartItem").each(function() {
                        $(this).css("width", sliderWidth);
                    });
                });
            }, 100);
        } else {
            $(".basket-blk .mini-basket-product-list").bxSlider({
                controls: false,
                infiniteLoop: false,
                onSliderLoad: function() {
                    var screenWidth = $(window).width();
                    var sliderWidth = parseInt(screenWidth - 30)
                    $(".mini-basket-product-list li.popupCartItem").each(function() {
                        $(this).css("width", sliderWidth);
                    });
                }
            });
        }
    }
    if (($(window).width()) > 768) {
        $(".mini-basket-product-list li.popupCartItem").each(function() {
            $(this).css("width", "100%");
        });
    }
});
$(document).ready(function(){
	$(".pickup-store").hide();
	 $(".standard-courier").hide();
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="pickup-store"){
            $(".box").not(".pickup-store").hide();
            $(".pickup-store").show();
        }
        if($(this).attr("value")=="standard-courier"){
            $(".box").not(".standard-courier").hide();
            $(".standard-courier").show();
        }
    });
});
$(document).ready(function(){
	
	$('.voucher-info').hover(function() { 
		$('.voucher-info-box').show(); 
		}, function() { 
		$('.voucher-info-box').hide(); 
		});	
});	
$(document).ready(function(){
    		
	$(".store_areaSearch").css("display", "none");
    $('#pickUpStoreForm').attr('action', contextPathURL + '/checkout/multi/delivery-address/add');
	$(".store_areaSearch").slideDown();

})	;
$(document).ready(function(){
    		
		$(".outstock-elink").on("click", function() {
			var that = $(this);
			$(this).parent(".outstock-email").find(".subscribe-pop").show();
			$(this).parent(".outstock-email").find(".close-link").show();
			$(this).parent(".outstock-email").addClass("emailbox-color");				
				var prevoffset = that.offset().top;
				var scrolled = $('.searchPOSResultsList').offset().top;
				window.scrolledArea = prevoffset - scrolled + window.scrolledArea;				
				$('.searchPOSResultsList').animate({scrollTop: window.scrolledArea}, 800);
					     
		});		
		
		$(".outstock-email .close-link").on("click", function() {					
			$(this).parent(".outstock-email").find(".subscribe-pop").hide();
			$(this).parent(".outstock-email").find(".close-link").hide();						
			$(this).parent(".outstock-email").removeClass("emailbox-color");						
			$(".notifyMessage").hide();					
		});
		$("#outstock-emailNotifyForm").validate({
            rules: {               
                email: {
                    required: !0,
                    email: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {               
                email: "Please enter a valid email address"                  
            }
        })
		$('.customerSubscribeBtn').click(function(){
            $("#outstock-emailNotifyForm").submit();
        });

});

/*$(document).on("click", "#editDeliveryModeSelect", function(event)
{
	 var contextPath = $("#contextPath").val();
	 var deliveryMode=document.getElementById('deliveryModeName').value;
	 var url =contextPath+'/checkout/multi/delivery-address/add';
	 $.ajax({
		 url: url,
         async: false,
         type: 'GET',
         dataType: 'json',
         data: {
        	 deliveryMode: code,
         	latitude:latitude,
         	longitude:longitude,
         	deliveryMode:deliveryMode
         },
         success: function(data) {
        	 
         },
	 })
} */
/*$('html').click(function() {
    $('#subscribe-pop').hide();
 })

 $('#emailleft').click(function(e){
     e.stopPropagation();
 });

 $('#link').click(function(e) {
     $('#subscribe-pop').toggle();
     $("#subscribe-pop").slideDown();
 });  
/*$(document).ready(function(){
	   $("#nav a").click(function(){
		      var id =  $(this).attr('id');
		      id = id.split('_');
		      $("#menu_container div").hide(); 
		      $("#menu_container #menu_"+id[1]).show();
});
		}); */