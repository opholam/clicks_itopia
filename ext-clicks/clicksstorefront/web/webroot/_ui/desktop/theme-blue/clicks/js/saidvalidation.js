/**
 *  South African ID Number Algorithm
 *  Copyright 2011, Stefan Bar | http://stefanbar.com
 *  
 *  Released under the MIT license:
 *  http://www.opensource.org/licenses/mit-license.php
 */
function getSAID(id) {
    if (id) {
        var exp = /^\d{10}[0-1]\d{2}$/;
        var id = id.toString();
        if (exp.test(id) && id != "0000000000000") {
            var d = id.split("");
            var s = 0;
            var sr = 0;
            for (var i = 0, il = (d.length - 1); i < il; i++) {
                if (!(i % 2 == 0)) {
                    d[i] = parseInt(d[i]) * 2;
                    if (d[i].toString().length > 1) {
                        var c = d[i].toString().split("");
                        d[i] = (parseInt(c[0]) + parseInt(c[1]));
                    }
                }
                s += parseInt(d[i]);
            }
            sr = s;
            if (sr.toString().length > 1) {
                if (sr.toString().substr(1, 1) != "0") {
                    sr = parseInt(s.toString().substr(0, 1)) + 1;
                    sr = parseInt(sr.toString() + "0");
                }
            }
            if ((sr - s) == parseInt(id.substr(12, 1))) {
                return {
                    "valid": true,
                    "dob": id.substr(4, 2) + "/" + id.substr(2, 2) + "/" + id.substr(0, 2),
                    "date": id.substr(4, 2),
                    "month": id.substr(2, 2),
                    "year": id.substr(0, 2),
                    "gender": ((id.substr(6, 1) > 4) ? "male" : "female")
                }
            }
        }
    }
    return false;
}

$('#SAIDNumber').blur(function(){
	var givenSAIDNumber = $(this).val();	
	var regsaid = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;
	
	
	if((givenSAIDNumber != "") && (regsaid.test(givenSAIDNumber) == true)) {
		var identity = getSAID(givenSAIDNumber);
		if (identity) {
			$("#SAIDNumber").removeClass("error").after("");
			// returns a object if valid
			var id_date = identity.date.replace(/^0+/, '');
		    var id_month = identity.month.replace(/^0+/, '');
		    var id_yeargiven = parseInt("20"+ identity.year);
		    var cd = new Date();
		    var cy = cd.getFullYear();
		    
		    if(id_yeargiven >= cy) {
		    	var id_year = parseInt(id_yeargiven - 100);
		    }else{
		    	var id_year = id_yeargiven;
		    }
		    
		    var day = id_date;
	        var month = id_month;
	        var year = id_year;
	        var age = 18;
	        var mydate = new Date();
	        mydate.setFullYear(year, month-1, day);

	        var currdate = new Date();
	       var setDate = new Date();         
	       setDate.setFullYear(mydate.getFullYear() + age, month-1, day);

	        if (!((currdate - setDate) > 0)){
	        	$("#SAIDNumber-error").remove();
	        	$("#SAIDNumber").removeClass("error").after("");
	        	$("#SAIDNumber").addClass("error").after("<label id='SAIDNumber-error' class='error' for='SAIDNumber' style='display: block;'>You have to be 18 years or older to join ClubCard.</label>");
	        }
		    
		    $('#createAccount .dateCheck option').each(function() {
		        if (id_date == $(this).val()){
		            $(this).attr("selected", "selected");
		        }
		    });
		    
		    $('#createAccount .monthCheck option').each(function() {
		        if ((id_month) == $(this).val()){
		            $(this).attr("selected", "selected");
		        }
		    });
		    
		    $('#createAccount .yearCheck option').each(function() {
		        if (id_year == $(this).val()){
		            $(this).attr("selected", "selected");
		        }
		    });	
		} else {
			// otherwise returns false
			$("#SAIDNumber-error").remove();
			$('#createAccount .dateCheck option:first-child, #createAccount .monthCheck option:first-child, #createAccount .yearCheck option:first-child').attr("selected", "selected").siblings().removeAttr("selected");			
			$("#SAIDNumber").addClass("error").after("<label id='SAIDNumber-error' class='error' for='SAIDNumber' style='display: block;'>Please enter a valid ID number</label>");			
		}
	}
	else {
		 $('#createAccount .dateCheck option:first-child, #createAccount .monthCheck option:first-child, #createAccount .yearCheck option:first-child').attr("selected", "selected").siblings().removeAttr("selected");
	}
});
$('#bb').blur(function(){
	var givenSAIDNumber = $(this).val();	
	var regsaid = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;
	
	
	if((givenSAIDNumber != "") && (regsaid.test(givenSAIDNumber) == true)) {
		var identity = getSAID(givenSAIDNumber);
		if (identity) {
			$("#bb").removeClass("error").after("");
			// returns a object if valid
			var id_date = identity.date.replace(/^0+/, '');
		    var id_month = identity.month.replace(/^0+/, '');
		    var id_yeargiven = parseInt("20"+ identity.year);
		    var cd = new Date();
		    var cy = cd.getFullYear();
		    
		    if(id_yeargiven >= cy) {
		    	var id_year = parseInt(id_yeargiven - 100);
		    }else{
		    	var id_year = id_yeargiven;
		    }
		    
		    var day = id_date;
	        var month = id_month;
	        var year = id_year;
	        var age = 18;
	        var mydate = new Date();
	        mydate.setFullYear(year, month-1, day);

	        var currdate = new Date();
	       var setDate = new Date();         
	       setDate.setFullYear(mydate.getFullYear() + age, month-1, day);

	        if (!((currdate - setDate) > 0)){
	        	$("#bb-error").remove();
	        	$("#bb").removeClass("error").after("");
	        	$("#bb").addClass("error").after("<label id='bb-error' class='error' for='bb' style='display: block;'>You have to be 18 years or older to join ClubCard.</label>");
	        }
		    
		    $('.dateCheck option').each(function() {
		        if (id_date == $(this).val()){
		            $(this).attr("selected", "selected");
		        }
		    });
		    
		    $('.monthCheck option').each(function() {
		        if ((id_month) == $(this).val()){
		            $(this).attr("selected", "selected");
		        }
		    });
		    
		    $('.yearCheck option').each(function() {
		        if (id_year == $(this).val()){
		            $(this).attr("selected", "selected");
		        }
		    });	
		} else {
			// otherwise returns false
			$("#bb-error").remove();
			$('.dateCheck option:first-child, .monthCheck option:first-child, .yearCheck option:first-child').attr("selected", "selected").siblings().removeAttr("selected");			
			$("#bb").addClass("error").after("<label id='bb-error' class='error' for='bb' style='display: block;'>Please enter a valid ID number</label>");			
		}
	}
	else {
		 $('.dateCheck option:first-child, .monthCheck option:first-child, .yearCheck option:first-child').attr("selected", "selected").siblings().removeAttr("selected");
	}
});