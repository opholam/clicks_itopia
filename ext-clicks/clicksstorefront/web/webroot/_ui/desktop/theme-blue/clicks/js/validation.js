ACC.formValidations = {
    editPerCCValidation: function() {
        $("#editClubCardForm, #editNonClubCardForm").validate({
            rules: {
                firstName: {
                    required: !0,
                    firstName: !0,
                    emptyCheck: !0
                },
                lastName: {
                    required: !0,
                    lastName: !0,
                    emptyCheck: !0
                },
                IDNumber: {
                	required: "#SAResident1:checked",
                    SANumber: !0
                },
                email: {
                    required: !0,
                    email: !0
                },
                contactNumber: {
                    required: !0,
                    phoneNo: !0
                },
                alternateNumber: {
                    phoneNo: !0
                },
                addressLine1: {
                    required: !0,
                    emptyCheck: !0
                },
                postalCode: {
                    required: !0,
                    maxlength: 4,
                    minlength: 4,
                    postCodeVal: !0
                },
                suburb: {
                    required: !0,
                    emptyCheck: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                firstName: "Please enter your first name",
                lastName: "Please enter your surname",
                IDNumber: "Please enter a valid ID number",
                email: "Please enter a valid email address",
                contactNo: "Please enter a valid cellphone number",
                contactNumber: "Please enter a valid cellphone number",
                addressLine1: "Address 1st line is mandatory",
                postalCode: "Please enter a valid postcode",
                suburb: "Please enter your suburb - this is mandatory"
            }
        })
    },
    linkCCvalidation: function() {
        $("#linkCC").validate({
            rules: {
                clubcardNumber: {
                    required: !0,
                    CCNumber: !0
                },
                eMailAddress: {
                    required: !0,
                    email: !0
                }
            },
            submitHandler: function(e) {
                path = $("#linkCC").find("a").attr("href"), window.location = path
            },
            messages: {
                clubcardNumber: "Please enter your ClubCard number",
                eMailAddress: "Please enter the email address you used to register for this site"
            }
        }), $("#registerCustomerFormlin").click(function(e) {
            e.preventDefault(), $("#linkCC").submit()
        })
    },
      addNewAddressValidation: function() {
        $("#addressForm").validate({
            rules: {
                firstName: {
                    required: !0,
                    firstName: !0,
                    maxlength: 30,
                    emptyCheck: !0
                },
                lastName: {
                    required: !0,
                    lastName: !0,
                    maxlength: 30,
                    emptyCheck: !0
                },
                email: {
                    required: !0,
                    maxlength: 50,
                    email: !0
                },
                phone: {
                    required: !0,
                    phoneNo: !0,
                    emptyCheck: !0
                },
                alternatePhone: {
                    phoneNo: !0
                },
                line1: {
                    required: !0,
                    maxlength: 50,
                    emptyCheck: !0
                },
                line2: {
                    maxlength: 50
                },
                countryName: {
                    required: !0
                },
                postcode: {
                    required: !0,
                    maxlength: 10,
                    minlength: 4,
                    postCodeVal: !0
                },
                deliveryInstructions: {
                	maxlength: 128
                },
                suburb: {
                    required: !0,
                    maxlength: 30,
                    emptyCheck: !0,
                    suburbCheck: !0
                },
                townCity: {
                    maxlength: 30
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                firstName: {
                	required: "Please enter your first name",
                	maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
                },
                lastName: {
                	required: "Please enter your surname",
                	maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
                },
                email: {
                	required: "Please enter a valid email address",
                	maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 51"),
                },
                contactNo: "Please enter a valid cellphone number",
                phone: "Please enter a valid cellphone number",
                line1: {
                	required: "Address 1st line is mandatory",
                	maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 51"),
                },
                line2: {
                	maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 51"),
                },
                countryName: "Country is mandatory",
                postcode: "Please enter a valid postcode",
                deliveryInstructions: {
                	maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 129"),
                },
            	suburb: {
            		required: "Please enter your suburb - this is mandatory",
            		suburbCheck: jQuery.validator.format("Please enter a valid suburb"),
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
            	},
            	townCity: {
                    maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
                }
            }
        })
    },
    linkJoinCCValidation: function() {
        $("#registerClubCardForm").validate({
            rules: {
                memberID: {
                    required: !0,
                    CCNumber: !0
                },
                eMailAddress: {
                    required: !0,
                    email: !0
                },
                firstName: {
                    required: !0,
                    firstName: !0,
                    emptyCheck: !0
                },
                lastName: {
                    required: !0,
                    emptyCheck: !0
                },
                contactNumber: {
                    required: !0,
                    phoneNo: !0
                },
                alternateNumber: {
                    phoneNo: !0
                },
                addressLine1: {
                    required: !0,
                    emptyCheck: !0
                },
                postalCode: {
                    required: !0,
                    maxlength: 4,
                    minlength: 4,
                    postCodeVal: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                memberID: "Please enter your ClubCard number",
                firstName: "Please enter your first name",
                lastName: "Please enter your surname",
                eMailAddress: "Please enter a valid email address",
                contactNumber: "Please enter a valid cellphone number",
                addressLine1: "Address 1st line is mandatory",
                postalCode: "Please enter a valid postcode"
            }
        })
    },
    changePass: function() {
        $("#updatePasswordForm").validate({
            rules: {
                currentPassword: {
                    required: !0
                },
                newPassword: {
                    required: !0
                },
                checkNewPassword: {
                    required: !0,
                    equalTo: "#profile-newPassword"
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                currentPassword: "Please enter your current password",
                newPassword: "Please enter a new password",
                checkNewPassword: "Your password and password confirmation do not match"
            }
        })
    },
    reviewForm: function() {
        $("#reviewForm").validate({
            rules: {
                headline: {
                    required: !0
                },
                comment: {
                    required: !0
                },
                rating: {
                    required: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                headline: "Please enter a headline for your review",
                comment: "Please enter your product review here ",
                rating: "Please enter rating"
            }
        })
    },
    forgottenForm: function() {
        $("#clubCardRegister").validate({
            ignore: [],
            rules: {
                clubCardNumber: {
                    required: !0,
                    number: true,
                    minlength : 13
                },
                residentNumber: {
                    required: function() {
                        return $("#saResi1").is(":checked") ? !0 : !1
                    },
                    number: true,
                    minlength : 13,
                    maxlength:13
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
               // clubCardNumber: "Please enter your ClubCard number",
                clubcardNumber: "Please enter your ClubCard number",
                //residentNumber: "Please enter your SA ID numer",
                residentNumber: {
                    required: "Please enter your SA ID number",
                    maxlength : "SA ID number has to be exactly 13 digits",
                    minlength : "SA ID number has to be exactly 13 digits",
                    number : "SA ID number cannot be less than 13 digits"
                },
                clubCardNumber: {
                    required: "Please enter your ClubCard number",
                    minlength: "Club card number cannot be less than 13 digits",
                    number : "Club card number cannot be less than 13 digits"
                },
            }
        })
    },
    babyLogin: function() {
        $(".login_form").validate({
            rules: {
                j_username: {
                    required: !0
                },
                j_password: {
                    required: !0
                }
            },
            submitHandler: function(e) {
                path = $("#linkCC").find("a").attr("href"), window.location = path
            },
            onfocusout: function(e) {
                $(e).valid()
            }
        }), $(".babyLogin").click(function(e) {
            e.preventDefault()
        })
    },
    joinBaby: function() {
	        $("#aboutFamily").validate({
                rules: {
                	estimatedDeliveryDate: {
                        required: !0
                    }
                },
                messages: {
                	estimatedDeliveryDate: "Please select the date"
                }
	        }),
	        $("#datepicker").datepicker({
	        	startDate: "current date",
	    		autoclose: !0,
	    		date: !1,
	    		onSelect: function() {
	    			$(".date").hide()
	    		}
	    	}),
	        $("#saveBaby").on("click", function(e) {	        	
	            if ($("#alreadyParent").is(":checked")) {
	            	$(".fName").each(function(){
	            		if(($.trim($(this).val())) == ""){
	            			e.preventDefault();
	            			$(this).next("label.error").remove();
		                	$(this).after("<label class='error'>Please enter your first name</label>");
	            		}
	            		$(this).keypress(function(){
	            			if(!($.trim($(this).val())) == ""){
		            			$(this).next("label.error").remove();
		            		}
	            		});
	            	});
	            	$(".sName").each(function(){
	            		if(($.trim($(this).val())) == ""){
	            			e.preventDefault();
	            			$(this).next("label.error").remove();
		                	$(this).after("<label class='error'>Please enter your surname</label>");
	            		}
	            		$(this).keypress(function(){
	            			if(!($.trim($(this).val())) == ""){
		            			$(this).next("label.error").remove();
		            		}
	            		});
	            	});
	            	$(".childrenDob").each(function(){
	            		if(($(this).val()) == ""){
	            			e.preventDefault();
	            			$(this).parents("#datePickerContainer").next("label.error").remove();
		                	$(this).parents("#datePickerContainer").after("<label class='error'>You can only add a child who is 3 years old or younger</label>");
	            		}
	            	});
	            }
	            if ($("#currentPregnant").is(":checked")) {
	    			var r = $.trim($("#datepicker").val());
	    			return "" === r ? ($(".date").show(), !1) : !0
	    		}
	        })
        },
    contactValidation: function() {
        $("#contactUsForm, #clinicBookingForm").validate({
            rules: {
                firstName: {
                    required: !0,
                    firstName: !0,
                    emptyCheck: !0
                },
                lastName: {
                    required: !0,
                    lastName: !0,
                    emptyCheck: !0
                },
                email: {
                    required: !0,
                    email: !0
                },
                contactNo: {
                    required: !0,
                    phoneNo: !0
                },
                contactNumber: {
                    required: !0,
                    phoneNo: !0
                },
                preferredClinic: {
                    required: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                firstName: "Please enter your first name",
                lastName: "Please enter your surname",
                email: "Please enter a valid email address",
                contactNo: "Please enter a valid cellphone number",
                contactNumber: "Please enter a valid cellphone number",
                preferredClinic: "Please select your preferred clinic"
            }
        })
    },
    userRegisterValidation: function() {
        $("#createAccount").validate({
            rules: {
                memberID: {
                    required: !0,
                    CCNumber: !0
                },
                IDNumber: {
                    required: !0,
                    SANumber: !0
                },
                SA_DOB_day: {
                    required: !0
                },
                SA_DOB_month: {
                    required: !0
                },
                SA_DOB_year: {
                    required: !0
                },
                password: {
                    required: {
                        depends: function(e) {
                            return "" !== $('input[name="eMailAddress"]').val() || $("#haveNo").is(":checked") ? !0 : !1 || $("#withoutcc").is(":checked") ? !0 : !1 
                        }
                    },
                    minlength: 6
                },
                confirmPassword: {
                    required: {
                        depends: function(e) {
                            return "" !== $('input[name="eMailAddress"]').val() ? !0 : !1                            		
                        }
                    },
                    equalTo: "input[name='password']"
                },
                eMailAddress: {
                    required: !0,
                    email: !0
                },
                firstName: {
                	required: {
                        depends: function(e) {
                        	return "" !== $("#withoutcc:checked").is(":checked") || $("#haveNo").is(":checked")
                        }
                    },                	
                    firstName: !0,
                    emptyCheck: !0
                },
                lastName: {
                	required: {
                        depends: function(e) {
                        	return "" !== $("#withoutcc:checked").is(":checked") || $("#haveNo").is(":checked")
                        }
                    },
                    emptyCheck: !0
                },
                contactNumber: {
                    required: "#JoinCC:checked",
                    phoneNo: !0
                },
                alternateNumber: {
                    phoneNo: !0
                },
                addressLine1: {
                    required: "#JoinCC:checked",
                    emptyCheck: !0
                },
                suburb: {
                    required: "#JoinCC:checked",
                    emptyCheck: !0
                },
                postalCode: {
                    required: "#JoinCC:checked",
                    maxlength: 4,
                    minlength: 4,
                    postCodeVal: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()                
            },
            messages: {
                memberID: "Please enter your ClubCard number",
                IDNumber: "Please enter a valid ID number",
                SA_DOB_day: "Please enter the day of your birthdate",
                SA_DOB_month: "Please enter the month of your birthdate",
                SA_DOB_year: "Please enter the year of your birthdate",
                password: {
                    required: "Please enter your password",
                    minlength: "Your password must contain at least 6 characters "
                },
                confirmPassword: "Your password and password confirmation do not match",
                eMailAddress: "Please enter a valid email address",
                firstName: "Please enter your first name",
                lastName: "Please enter your surname",
                addressLine1: "Address 1st line is mandatory",
                suburb: "Please enter your suburb - this is mandatory",
                postalCode: "Please enter a valid postcode",
                contactNumber: "Please enter a valid cellphone number "
            }
        })
    },
    joinCCValidation: function() {
        $("#createAccountJoinCC").validate({
            rules: {
                memberID: {
                    required: !0,
                    CCNumber: !0
                },
                IDNumber: {
                    required: !0,
                    SANumber: !0
                },
                SA_DOB_day: {
                    required: "#saResiNo:checked"
                },
                SA_DOB_month: {
                    required: "#saResiNo:checked"
                },
                SA_DOB_year: {
                    required: "#saResiNo:checked"
                },
                eMailAddress: {
                	required: {
                        depends: function(e) {
                        	return "" !== $("#withoutcc:checked").is(":checked") || $("#haveNo").is(":checked")
                        }
                    },
                    email: !0
                },
                firstName: {
                    required: !0,
                    firstName: !0,
                    emptyCheck: !0
                },
                lastName: {
                    required: !0,
                    emptyCheck: !0
                },
                contactNumber: {
                    required: !0,
                    phoneNo: !0
                },
                alternateNumber: {
                    phoneNo: !0
                },
                addressLine1: {
                    required: !0,
                    emptyCheck: !0
                },
                suburb: {
                    required: !0,
                    emptyCheck: !0
                },
                postalCode: {
                    required: !0,
                    maxlength: 4,
                    minlength: 4,
                    postCodeVal: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
                memberID: "Please enter your ClubCard number",
                IDNumber: "Please enter a valid ID number",
                SA_DOB_day: "Please enter the day of your birthdate",
                SA_DOB_month: "Please enter the month of your birthdate",
                SA_DOB_year: "Please enter the year of your birthdate",
                password: {
                    required: "Please enter your password",
                    minlength: "Your password must contain at least 6 characters "
                },
                confirmPassword: "Your password and password confirmation do not match",
                eMailAddress: "Please enter a valid email address",
                firstName: "Please enter your first name",
                lastName: "Please enter your surname",
                addressLine1: "Address 1st line is mandatory",
                suburb: "Please enter your suburb - this is mandatory",
                contactNumber: "Please enter a valid cellphone number ",
                postalCode: "Please enter a valid postcode"
            }
        })
    },
    paymentMethodForm: function() {
        $("#paymentDetailsForm").validate({
            rules: {
            	"billingAddress.firstName": {
                    required: !0,
                    firstName: !0,
                    maxlength: 30,
                    emptyCheck: !0
                },
                "billingAddress.lastName": {
                    required: !0,
                    lastName: !0,
                    maxlength: 30,
                    emptyCheck: !0
                },
                "billingAddress.email": {
                    required: !0,
                    maxlength: 50,
                    email: !0
                },
                "billingAddress.line1": {
                    required: !0,
                    maxlength: 50,
                    emptyCheck: !0
                },
                "billingAddress.line2": {
                    maxlength: 50
                },
                "billingAddress.postcode": {
                    required: !0,
                    maxlength: 10,
                    minlength: 4,
                    postCodeVal: !0
                },
                "billingAddress.suburb": {
                    required: !0,
                    maxlength: 30,
                    emptyCheck: !0,
                    suburbCheck: !0
                },
                "billingAddress.townCity": {
                    maxlength: 30
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {
            	"billingAddress.firstName": {
            		required: "Please enter your first name",
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
            	},
            	"billingAddress.lastName": {
            		required: "Please enter your surname",
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
            	},
            	"billingAddress.email": {
            		required: "Please enter a valid email address",
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 51"),
            	},
            	"billingAddress.line1": {
            		required: "Address 1st line is mandatory",
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 51"),
            	},
            	"billingAddress.line2": {
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 51"),
            	},
            	"billingAddress.postcode": "Please enter a valid postcode",
            	"billingAddress.suburb": {
            		required: "Please enter your suburb - this is mandatory",
            		suburbCheck: jQuery.validator.format("Please enter a valid suburb"),
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
            	},
            	"billingAddress.townCity": {
            		maxlength: jQuery.validator.format("Please check number of characters entered. It should be less than 31"),
            	}
            }
        })
    },
	deliverycontinue: function() {
		$("#pickUpStoreForm").validate({
            rules: {               
                email: {
                    required: !0,
                    email: !0
                },                
                phone: {
                    required: !0,
                    phoneNo: !0
                }
            },
            onfocusout: function(e) {
                $(e).valid()
            },
            messages: {               
                email: "Please enter a valid email address",                
                phone: "Please enter a valid cellphone number "
                
            }
        })
		$('#delAddressSubmit').click(function(){
            $("#pickUpStoreForm").submit();				
        });
		$("input[name='alternatePhone']").keypress(function(){
			if ( isNaN( String.fromCharCode(event.keyCode) )) 
			return false;
		});
		
	},
    methodInit: function() {
        this.editPerCCValidation(),this.addNewAddressValidation(),  this.contactValidation(), this.linkCCvalidation(), this.forgottenForm(), this.changePass(), this.reviewForm(), this.joinBaby(), this.userRegisterValidation(), this.joinCCValidation(), this.linkJoinCCValidation(), this.paymentMethodForm(), this.deliverycontinue()
                        
    }
}, $(document).ready(function() {
    ACC.formValidations.methodInit(), 
    jQuery.validator.addMethod("firstName", function(e, r) {
        return this.optional(r) || /^[ a-zA-Z-']+(?:(?:. |[' ])[a-zA-Z]+)*$/.test(e)
    }, "Please enter your first name"), 
    jQuery.validator.addMethod("lastName", function(e, r) {
        return this.optional(r) || /^[ a-zA-Z-']+(?:(?:. |[' ])[a-zA-Z]+)*$/.test(e)
    }, "Please enter your surname"), 
    jQuery.validator.addMethod("phoneNo", function(e, r) {
        return this.optional(r) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(e)
    }, "Please enter a valid cellphone number"), 
    jQuery.validator.addMethod("emailSpace", function(e, r) {
        return this.optional(r) || jQuery.validator.methods.email.call(this, jQuery.trim(e), r)
    }, "Please enter a valid email address"), 
    jQuery.validator.addMethod("CCNumber", function(e, r) {
        return this.optional(r) || /^\+?[0-9]{10,13}$/.test(e)
    }, "Please enter your ClubCard number"),
    jQuery.validator.addMethod("SANumber", function(e, r) {
        return this.optional(r) || /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/.test(e)
    }, "Please enter your SA ID number"),
    jQuery.validator.addMethod("emptyCheck", function(value, element) {
		return this.optional(element) || /\S+/.test(value);
	}),
	jQuery.validator.addMethod("postCodeVal", function(value, element) {
		return this.optional(element) || /^[0-9]*$/.test(value);
	}),
	jQuery.validator.addMethod("suburbCheck", function(value, element) {
		return this.optional(element) || /^([^0-9]*)$/.test(value);
	}, "Please enter a valid suburb")
});