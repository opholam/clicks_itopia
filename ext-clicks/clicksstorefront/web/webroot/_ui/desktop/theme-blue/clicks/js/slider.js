$(document).ready(function() {
	 $(".home_bxslider").show().bxSlider({
	        slideWidth: $(window).innerWidth(),
	        adaptiveHeight: !0,
	        auto: !0,
	        pause: 6000,
	        autoControls: !0,
	        "default": !0,
	        controls: !1,
	        pager: !0,
	        onSliderLoad: function() {
	            $(".home-slider-wrap").css({
	                visibility: "visible",
	                height: "auto"
	            }),
	            $(".home_bxslider li").not(".bx-clone").css({
	                display: "block"
	            })
	        },
	        onSlideAfter: function(l, g, a) {
	            if ($("#gaallow_" + a).val() != "") {
	                void 0 != $("#gaallow_" + a).val() && (ga("ec:addPromo", {
	                    id: $("#gapage_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gaplacementcode_" + a).val().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gabrand_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gacosttype_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gaduration_" + a).val().replace(/[^a-zA-Z0-9-]/g, "") + "_" + $("#gacreative_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
	                    name: $("#gabrand_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
	                    position: $("#gaskuid_" + a).val()
	                }), ga("send", "event", "internal placement", "slider", "rotate", {nonInteraction: true}))
	            }
	        }
	    }),
    $(".nav-tabs a").one("click", function() {
        var a = this;
        setTimeout(function() {
            "#inspiration" == $(a).attr("href") && $(".article-slider").bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideMargin: 0,
                pager: !1,
                controls: !0,
				infiniteLoop: false,
                autoReload: !0,
                breaks: [{
                    screen: 0,
                    slides: 1,
                    pager: !1,
                    controls: !0
                }, {
                    screen: 320,
                    slides: 1,
                    pager: !0,
                    controls: !1
                }, {
                    screen: 440,
                    slides: 2,
                    controls: !0
                }, {
                    screen: 600,
                    slides: 2,
                    controls: !1,
                    pager: !0
                }, {
                    screen: 768,
                    slides: 2,
                    pager: !0,
                    controls: !1
                }, {
                    screen: 978,
                    slides: 1,
                    controls: !0
                }]
            })
        }, 400)
    });
    var j = $('.pdpSlider > div[class^="col-"]').length;
    j > 3 && $(".pdpSlider").bxSlider({
        minSlides: 3,
        maxSlides: 4,
        slideMargin: 0,
        pager: !1,
        controls: !1,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0
        }, {
            screen: 480,
            slides: 2,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0
        }, {
            screen: 1024,
            slides: 4,
            pager: !1,
            controls: !0
        }]
    });

    function d(g, a) {
        $("." + g).bxSlider({
            minSlides: a,
            maxSlides: 4,
            slideMargin: 0,
            pager: !1,
            slideWidth: 0,
            controls: !1,
            autoReload: !0,
            breaks: [{
                screen: 0,
                slides: 1,
                pager: !1,
                controls: !0
            }, {
                screen: 320,
                slides: 1,
                pager: !0
            }, {
                screen: 480,
                slides: 2,
                pager: !0
            }, {
                screen: 768,
                slides: 2,
                pager: !0
            }, {
                screen: 1024,
                slides: a,
                pager: !1,
                controls: !0
            }]
        })
    }

    function h(g, a) {
        $("." + g).bxSlider({
            minSlides: 3,
            maxSlides: a,
            slideWidth: 70,
            autoReload: !0,
            controls: !0,
            pager: !1,
            slideMargin: 0,
            moveSlides: 1,
            onSliderLoad: function() {
                $(".partner-spacing").css("visibility", "visible"), $(".partner-spacing").css("height", "auto")
            },
            breaks: [{
                screen: 0,
                slides: 1,
                pager: !1
            }, {
                screen: 320,
                slides: 2
            }, {
                screen: 568,
                slides: 3
            }, {
                screen: 978,
                slides: a,
                controls: !0
            }]
        })
    }
    $(".partner-slider").bxSlider({
        minSlides: 3,
        maxSlides: 5,
        slideWidth: 110,
        autoReload: !0,
        controls: !0,
        pager: !1,
        slideMargin: 0,
        moveSlides: 1,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1
        }, {
            screen: 460,
            slides: 2
        }, {
            screen: 768,
            slides: 3
        }, {
            screen: 978,
            slides: 5,
            controls: !0
        }]
    });
    $(".promo-slider").bxSlider({
        slideMargin: 0,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 440,
            slides: 2,
            controls: !0
        }, {
            screen: 768,
            slides: 3,
            controls: !0
        }, {
            screen: 978,
            slides: 4,
            controls: !0
        }]
    }), $('a[href="#inspiration"]').one("shown.bs.tab", function() {
        $(".article-slider").bxSlider({
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 0,
            pager: !1,
            controls: !0,
            autoReload: !0,
			infiniteLoop: false
        })
    }),
    $(".bxslider-pdp-mainImage").bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        pager: !0,
        controls: !1,
        autoReload: !0
    }), $(".sliderOuter").bxSlider({
        minSlides: 2,
        maxSlides: 4,
        slideMargin: 0,
        pager: !1,
        slideWidth: 0,
        controls: !1,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0
        }, {
            screen: 480,
            slides: 2,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0
        }, {
            screen: 1024,
            slides: 4,
            controls: !0
        }]
    });
    var k = $(".category_carousel").length > 0 ? !1 : !0,
        c = $(".carouselCount").length > 0 ? 4 : 3;
    $(".brandSlide .article-slider, .category_carousel .article-slider").bxSlider({
        minSlides: 2,
        maxSlides: 3,
        slideMargin: 0,
        pager: !1,
        slideWidth: 0,
        controls: !1,
		infiniteLoop: false,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0
        }, {
            screen: 480,
            slides: 2,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0
        }, {
            screen: 1024,
            slides: c,
            pager: !1,
            controls: k
        }]
    }), $(".sliderFinal .sliderOuter1").bxSlider({
        minSlides: 2,
        maxSlides: 3,
        slideMargin: 0,
        pager: !1,
        slideWidth: 0,
        controls: !1,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0
        }, {
            screen: 480,
            slides: 2,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0
        }, {
            screen: 1024,
            slides: 4,
            pager: !1,
            controls: !0
        }]
    }), $(".pdp-second-content .article-slider").bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        pager: !1,
        controls: !0,
        autoReload: !0,
		infiniteLoop: false,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0,
            controls: !1
        }, {
            screen: 440,
            slides: 2,
            controls: !0
        }, {
            screen: 600,
            slides: 2,
            controls: !1,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0,
            controls: !1
        }, {
            screen: 978,
            slides: 1,
            controls: !0
        }]
    }), $(".babyMiddleOuter .article-slider").bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        pager: !1,
        controls: !0,
        autoReload: !0,
		infiniteLoop: false,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 440,
            slides: 2,
            controls: !0
        }, {
            screen: 600,
            slides: 2,
            controls: !0,
            pager: !1
        }, {
            screen: 768,
            slides: 2,
            pager: !1,
            controls: !0
        }, {
            screen: 978,
            slides: 1,
            controls: !0
        }]
    }), $(".health-promo-slider").bxSlider({
        minSlides: 3,
        maxSlides: 4,
        slideMargin: 0,
        pager: !1,
        slideWidth: 0,
        auto: !0,
        controls: !1,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0
        }, {
            screen: 480,
            slides: 1,
            pager: !0
        }, {
            screen: 768,
            slides: 1,
            pager: !0
        }, {
            screen: 1024,
            slides: 1,
            pager: !0,
            controls: !1
        }],
	    onSlideAfter: function(l, g, a) { 
	        if ($("#gaallow_" + a).val() != "") {
	            void 0 != $("#gaallow_" + a).val() && (ga("ec:addPromo", {
	                id: $("#gapage_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gaplacementcode_" + a).val().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gabrand_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gacosttype_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, "") + "_" + $("#gaduration_" + a).val().replace(/[^a-zA-Z0-9-]/g, "") + "_" + $("#gacreative_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
	                name: $("#gabrand_" + a).val().toLowerCase().replace(/[^a-zA-Z0-9]/g, ""),
	                position: $("#gaskuid_" + a).val()
	            }), ga("send", "event", "internal placement", "slider", "rotate", {nonInteraction: true}))
	        }
	    }
    });
    if ($(window).width() < 769) {
        $(".home-promotions").find(".pad-zero").css("width", "100%");
        var i = $(".product-slider").attr("data-classname"),
            b = $(".product-slider").attr("data-minslide");
        d(i, b), $(".new-arrivals").bxSlider({
            minSlides: 1,
            maxSlides: 2,
            slideMargin: 0,
            pager: !1,
            slideWidth: 0,
            controls: !1,
            autoReload: !0,
            breaks: [{
                screen: 0,
                slides: 1,
                pager: !1,
                controls: !0
            }, {
                screen: 320,
                slides: 1,
                pager: !0
            }, {
                screen: 480,
                slides: 2,
                pager: !1
            }, {
                screen: 768,
                slides: 2,
                pager: !1
            }, {
                screen: 1024,
                slides: 3,
                pager: !1,
                controls: !0
            }]
        })
    }
    var i = $(".slider-component").attr("data-classname"),
        f = $(".slider-component").attr("data-maxslide");
    h(i, f);
    $(".promotion-slider .article-slider").bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        pager: !1,
        controls: !0,
        autoReload: !0,
		infiniteLoop: false,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0,
            controls: !1
        }, {
            screen: 440,
            slides: 2,
            controls: !0
        }, {
            screen: 600,
            slides: 2,
            controls: !1,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0,
            controls: !1
        }, {
            screen: 978,
            slides: 1,
            controls: !0
        }]
    }), $(".health-center-slider").bxSlider({
        minSlides: 3,
        maxSlides: 5,
        slideWidth: 110,
        autoReload: !0,
        controls: !0,
        pager: !1,
        slideMargin: 0,
        moveSlides: 1,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1
        }, {
            screen: 460,
            slides: 2
        }, {
            screen: 768,
            slides: 3
        }, {
            screen: 978,
            slides: 3,
            controls: !0
        }]
    }), $(".health-top-slider").bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 110,
        autoReload: !0,
        controls: !0,
        pager: !1,
        slideMargin: 0,
        moveSlides: 1,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1
        }, {
            screen: 460,
            slides: 2
        }, {
            screen: 768,
            slides: 3
        }, {
            screen: 978,
            slides: 5,
            controls: !0
        }]
    }), $(".conditionsSlider").bxSlider({
        minSlides: 1,
        maxSlides: 4,
        slideMargin: 0,
        pager: !1,
        slideWidth: 0,
        controls: !1,
        autoReload: !0,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: !1,
            controls: !0
        }, {
            screen: 320,
            slides: 1,
            pager: !0
        }, {
            screen: 480,
            slides: 2,
            pager: !0
        }, {
            screen: 768,
            slides: 2,
            pager: !0
        }, {
            screen: 1024,
            slides: 1,
            pager: !0,
            controls: !1
        }]
    });
    $(".bxslider-ss").bxSlider({
        pagerCustom: "#bx-pager"
    });
    $(".bxslider-superSection").bxSlider({
    	controls: false,
    	auto: !0,
        pagerCustom: "#bx-pager-superSection"
    })
});