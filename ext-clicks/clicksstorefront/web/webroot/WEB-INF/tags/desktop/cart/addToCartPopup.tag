<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>

{"cartData": {
"total": "${cartData.totalPrice.value}",
"products": [
<c:forEach items="${cartData.entries}" var="cartEntry" varStatus="status">
	{
		"sku":		"${cartEntry.product.code}",
		"name": 	"<c:out value='${cartEntry.product.name}' />",
		"qty": 		"${cartEntry.quantity}",
		"price": 	"${cartEntry.basePrice.value}",
		"categories": [
		<c:forEach items="${cartEntry.product.categories}" var="category" varStatus="categoryStatus">
			"<c:out value='${category.name}' />"<c:if test="${not categoryStatus.last}">,</c:if>
		</c:forEach>
		]
	}<c:if test="${not status.last}">,</c:if>
</c:forEach>
]
},
"cartAnalyticsData":{"cartCode" : "${cartCode}","productPostPrice":"${entry.basePrice.value}","productName":"${product.name}"}
,
"addToCartLayer":"<spring:escapeBody javaScriptEscape="true">
<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/add" var="addCart"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>
<c:url value="${product.url}" var="productUrl"/>
<div class="inidiviual-order-wrap text-center addTobasketpopup" id="addTobasket">
<div class="modal-header "><h4><spring:theme code="basket.added.to.basket" /></h4></div>
<c:if test="${not empty entry.entryNumber}">
<div class="shopping-list">
	<div class="itemThumb">
	<product:productPrimaryImage product="${product}" format="cartIcon"/>
	</div>
	<div class="border shop-wrap clearfix" id="otheritems">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 text-left">
			<div class="prod-info text-left">
			<h4><a href="${productUrl}"><c:out value="${product.brand}"/></a></h4>
				<span><c:out value="${product.name}" /></span>
					<div class="price-wrap">
					<div class="price red">
								<c:choose>
									<c:when test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
									<fmt:formatNumber minFractionDigits="2" var="productPrice" pattern="####.##" value="${entry.product.price.grossPriceWithPromotionApplied}" />
									R<dateFormat:dateFormat conversionType="rand" input="${productPrice}"/>
									</c:when>
									<c:otherwise>
									<fmt:formatNumber minFractionDigits="2" var="productPrice" pattern="####.##" value="${entry.product.price.value}" />
									R<dateFormat:dateFormat conversionType="rand" input="${productPrice}"/>
									</c:otherwise>
									</c:choose>
					</div>
						</div>
					<div class="price-offer">	
								<c:if
								test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
	 							Was <format:price priceData="${entry.basePrice}"/>
										<span class="price red">Save
										<fmt:formatNumber minFractionDigits="2" var="promoPrice" pattern="####.##" value="${entry.product.price.value - entry.product.price.grossPriceWithPromotionApplied}" />
										<c:set var="gross" value="${promoPrice}" />
										<c:set var="dec" value="${fn:split(gross, '.')}" />
										<c:set var="dec1" value="${dec[0]}" />
										<c:set var="dec2" value="${dec[1]}" />
										${dec1}<sup>${dec2}</sup>
								<dateFormat:dateFormat conversionType="rand" input="${save}"/>	
							</span>
						</c:if>
						</div>  
						<c:forEach items="${entry.potentialPromoDescription}" var="promotion">
							<ul class="potentialPromotions">
							<li>
												<div class="prod_notify tick">
												<a href="${productUrl}">
													<ycommerce:testId code="cart_potentialPromotion_label">
													Missed Savings! <br />
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion,'productName',entry.product.name)}"></c:set> 
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion}
												</c:otherwise>
												</c:choose>
												</ycommerce:testId>
												</a>
												</div>
											</li>
											</ul>
							</c:forEach>
					<!-- <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
							<ul class="potentialPromotions">
								<c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
									<c:choose>
									<c:when test="${fn:containsIgnoreCase(promotion.promotionData.promotionType, 'Multi')}">
									<c:set var="displayed" value="false" />
									<c:forEach items="${promotion.consumedEntries}"
										var="consumedEntry">
										<c:if
											test="${not displayed and consumedEntry.orderEntryNumber == entry.entryNumber and not empty promotion.description}">
											<c:set var="displayed" value="true" />
											<li>
												<div class="prod_notify tick">
												<c:choose>
												<c:when test="${not empty url}">
												<a href="${url}"> <ycommerce:testId
															code="cart_potentialPromotion_label">
													Missed Savings! <br />
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion.description, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion.description,'productName',entry.product.name)}"></c:set>
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion.description}
												</c:otherwise>
												</c:choose>
												</ycommerce:testId>
													</a>
												</c:when>
												<c:otherwise>
												<a href="${productUrl}"> <ycommerce:testId
															code="cart_potentialPromotion_label">
													Missed Savings! <br />
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion.description, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion.description,'productName',entry.product.name)}"></c:set>
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion.description}
												</c:otherwise>
												</c:choose>
												</ycommerce:testId>
													</a>
												</c:otherwise>
												</c:choose>
												</div>
											</li>
										</c:if>
									</c:forEach>
									</c:when>
									<c:otherwise>
									<c:set var="displayed" value="false" />
									<c:forEach items="${promotion.consumedEntries}"
										var="consumedEntry">
										<c:if
											test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
											<c:set var="displayed" value="true" />
											<li>
												<div class="prod_notify tick">
												<a href="${productUrl}"> <ycommerce:testId
															code="cart_potentialPromotion_label">
													Missed Savings! <br />
														<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion.description, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion.description,'productName',entry.product.name)}"></c:set>
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion.description}
												</c:otherwise>
												</c:choose>
												</ycommerce:testId>
													</a>
												</div>
											</li>
										</c:if>
									</c:forEach>
									</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
						</c:if>  -->	
						<c:forEach items="${entry.appliedPromoDescription}" var="promotion">
						<ul class="appliedPromotions">
						<li>
												<div class="prod_notify tick">
													<ycommerce:testId code="cart_appliedPromotion_label">
												
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion,'productName',entry.product.name)}"></c:set> 
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion}
												</c:otherwise>
												</c:choose>
												
												</ycommerce:testId>
												</div>
											</li>
											</ul>
						</c:forEach>
			</div>
		</div>
		 	<c:url value="/cart/update" var="cartUpdateFormAction" />
		 	<input type="hidden" class="onKeyUpdate" value="${cartUpdateFormAction}" />
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 paddingNone shop-list text-center no_items">
						<form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post" commandName="updateQuantityForm${entry.entryNumber}" class="update_quantity_form">
							<c:if test="${entry.updateable}" >
							<ycommerce:testId code="cart_product_removeProduct">
								<spring:theme code="text.iconCartRemove" var="iconCartRemove"/>
								<button id="QuantityProduct_${entry.entryNumber}" class="decrementProductQuantity minus btn-default">-</button>
							</ycommerce:testId>
							</c:if>
							<input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
							<input type="hidden" name="productCodePost" value="${entry.product.code}"/>
							<input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
							<ycommerce:testId code="cart_product_quantity">
								<form:label cssClass="skip" path="quantity" for="quantity${entry.entryNumber}"><spring:theme code="basket.page.quantity"/></form:label>
								<form:input disabled="${not entry.updateable}" type="text" size="1" id="quantity${entry.entryNumber}" class="form-control quantity" path="quantity" />
							</ycommerce:testId>
							
							<c:if test="${entry.updateable}" >
								<ycommerce:testId code="cart_product_updateQuantity">
									<button id="QuantityProduct_${entry.entryNumber}" class="incrementProductQuantity plus btn-default">+</button>
 								</ycommerce:testId>
 								<c:if test="${entry.updateable}" >
							<ycommerce:testId code="cart_product_removeProduct">
								<spring:theme code="text.iconCartRemove" var="iconCartRemove"/>
								<a href="#" id="RemoveProduct_${entry.entryNumber}" class="submitRemoveProduct">${iconCartRemove}</a>
							</ycommerce:testId>
						</c:if>
								</c:if>
						</form:form>
			</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 shop-list">
		<div class="price-wrap price-right">
			<c:choose>
				<c:when test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
					<c:choose>
						<c:when test="${not empty entry.totalPrice and entry.totalPrice.formattedValue ne 'R0.00'}">
							<format:discountedPrice priceData="${entry.totalPrice}" displayFreeForZero="true"/>
						</c:when>
						<c:otherwise>
							<format:discountedPrice priceData="${entry.basePrice}" displayFreeForZero="true"/>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty entry.totalPrice and entry.totalPrice.formattedValue ne 'R0.00'}">
							<format:discountedPrice priceData="${entry.totalPrice}" displayFreeForZero="true"/>
						</c:when>
						<c:otherwise>
							<format:discountedPrice priceData="${entry.basePrice}" displayFreeForZero="true"/>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 shopBasket">
		<div class="indiviSubTotal">
			<div class="subTotal">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 paddingNone price-right">
					<h4>Subtotal</h4>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 paddingNone price-right">
				<c:choose>
				 <c:when test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
                       <format:discountedPrice priceData="${cartData.subTotal}"/>
				</c:when>
				<c:otherwise>
						 <format:price priceData="${cartData.subTotal}"/>
				</c:otherwise>
		</c:choose>
				</div>
			</div>
			 <c:if test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
			<div class="totalsaving red">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 paddingNone price-right">(You have saved</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 paddingNone price-right">
				 <format:discountedPrice priceData="${cartData.totalDiscounts}"/>)
				</div>
			</div>
			</c:if>
				</div>
	</div>
</div>
</c:if>
<c:if test="${not empty errorMsg}">

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<div class="alert negative">
	<div class="item_container_holder promo">
<spring:theme code="${errorMsg}" />
</div></div></div>
</c:if>
<c:if test="${not empty accErrorMsgs}">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<div class="alert negative">
	<div class="item_container_holder promo">
<spring:theme code="${accErrorMsgs}" />
</div></div></div>
</c:if>
<c:forEach items="${cartData.potentialOrderPromotions}" var="promotion">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<div class="alert positive">
	<div class="item_container_holder promo">
		<div class="title_holder">
			<h2><spring:theme code="basket.potential.promotions" /></h2>
		</div>
		<div class="item_container">
			<ycommerce:testId code="cart_recievedPromotions_labels">
				<ul>
					<li class="cart-promotions-applied">${promotion.description}</li>
				</ul>
			</ycommerce:testId>
		</div>
	</div>
	</div>
	</div>
</c:forEach>
<c:if test="${not empty cartData.appliedOrderPromotions}">
<div class="alert positive">
	<div class="item_container_holder promo">
		<div class="title_holder">
			<h2><spring:theme code="basket.received.promotions" /></h2>
		</div>
		<div class="item_container">
			<ycommerce:testId code="cart_recievedPromotions_labels">
				<ul>
					<c:forEach items="${cartData.appliedOrderPromotions}" var="promotion">
						<li class="cart-promotions-applied">${promotion.description}</li>
					</c:forEach>
				</ul>
			</ycommerce:testId>
		</div>
	</div>
	</div>
</c:if>

<div class="popup-btn clearfix">
<c:choose>
<c:when test="${not empty shoppingUrl}">
<a href="${shoppingUrl}" class="btn primary_btn contShop">Continue shopping</a>
</c:when>
<c:otherwise>
<a href="${contextPath}" class="btn primary_btn contShop">Continue shopping</a>
</c:otherwise>
</c:choose>
<p>OR</p>
	<a href="${cartUrl}" class="btn primary_btn"><spring:theme code="checkout.checkout" /></a>
</div>
<c:if test="${entry.entryNumber == 0 && entry.quantity == 1}">
<div style="clear: both;height: 25px;"></div>
</c:if>
<%-- <div class="links"><a href="${cartUrl}" class="button positive"><spring:theme code="checkout.checkout" /></a></div> --%>
</div>
</spring:escapeBody>"
}



