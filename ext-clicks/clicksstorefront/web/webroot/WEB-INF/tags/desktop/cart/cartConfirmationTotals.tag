<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTaxEstimate" required="false"
	type="java.lang.Boolean"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>

<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
	<div class="summary-content">
		<spring:theme code="basket.page.totals.subtotal" />
		<span> (excluding delivery)</span>
	</div>
</div>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
	<%-- <ycommerce:testId code="Order_Totals_Subtotal"> --%>
	<div class="ord-summary-content">
						<format:orderCartPrice priceData="${cartData.subTotal}"/> 
		</div>
	<%-- </ycommerce:testId> --%>
</div>
<c:if test="${cartData.orderDiscounts.value > 0}">
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
		<div class="summary-content red">
			<%-- <spring:theme code="basket.page.totals.savings" /> --%>
			Discounts & Vouchers
			<%-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<div class="savingsValue">
							<format:cartPrice priceData="${cartData.totalDiscounts}"/>
						</div></div>) --%>
		</div>
	</div>

	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
		<div class="savingsValue">
		<c:choose>
	<c:when test="${cartData.orderDiscounts.value > 0}">
	<div class="ord-summary-content">
	<format:orderDiscountedPrice priceData="${cartData.orderDiscounts}"/> 
	</div>
	</c:when>
	<c:otherwise>
<div></div>
	</c:otherwise>
</c:choose>
		</div>
	</div>
</c:if>
<c:choose>
	<c:when test="${not empty deliveryFeeMessage}">
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<div class="summary-content">
				<spring:theme code="basket.page.totals.delivery" />
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right delivery_text">
			<div class="red">
				${deliveryFeeMessage}
			</div>
		</div>
	</c:when>
	<c:when test="${not empty cartData.deliveryCost and cartData.deliveryCost.value > 0}">
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<div class="summary-content">
				<spring:theme code="basket.page.totals.delivery" />
			</div>
		</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
						<c:choose>
								<c:when test="${cartData.deliveryCost.value > 0}">
								<div class="ord-summary-content">
								<format:orderCartPrice priceData="${cartData.deliveryCost}" />
								</div>
								</c:when>
								<c:otherwise>
								<!--<c:choose>
								<c:when test="${not empty deliveryFeeMessage}">
								<div>${deliveryFeeMessage}</div>
								</c:when>
								<c:otherwise> -->
								<div>FREE</div>
							<!-- 	</c:otherwise>
								</c:choose>  -->
							</c:otherwise>
						</c:choose>
		</div>
		<%-- <div class="price blue">
					<c:set var="fee" value="R${DeliveryFee}" />
					<c:set var="dec" value="${fn:split(fee, '.')}" />
					<c:set var="dec1" value="${dec[0]}" />
					<c:set var="dec2" value="${dec[1]}" />
					${dec1}<sup>${dec2}</sup>
				</div> --%>

	</c:when>
	<c:when test="${not empty cartData.deliveryCost}">
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<div class="summary-content">
				<spring:theme code="basket.page.totals.delivery" />
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
		<div class="ord-summary-content">
		<small class="red">FREE</small>
		</div>
		</div>
	</c:when>
</c:choose>
<%-- <cart:taxExtimate cartData="${cartData}"
			showTaxEstimate="${showTaxEstimate}" /> --%>


<c:choose>
	<c:when test="${showTax}">
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<div class="summary-content totalvat">
				<spring:theme code="checkoutstep.pages.total" />
				<div class="specialCheckout">
					Total includes
					<format:orderCartPrice priceData="${cartData.totalTax}" />
					VAT
				</div>				
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
					<div class="grandTotal main-total">
					<div class="ord-summary-content">
						<format:orderCartPrice priceData="${cartData.totalPriceWithTax}" />
						</div>
					</div>
				</div>
	</c:when>
	<c:otherwise>
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<div class="summary-content">
<spring:theme code="checkoutstep.pages.total" />
				
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
					<div class="grandTotal">
					<div class="ord-summary-content">
						<format:orderCartPrice priceData="${cartData.totalPrice}" />
						</div>
					</div>
				</div>
	</c:otherwise>
	
</c:choose>
<c:if test="${cartData.totalDiscounts.value > 0}">
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<div class="summary-content">
<spring:theme code="checkoutstep.pages.total.savings" text="Total saved"/>
				
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
					<div class="grandTotal">
					<div class="ord-summary-content">
						<format:orderCartPrice priceData="${cartData.totalDiscounts}" displayFreeForZero="false"/>
						</div>
					</div> 
				</div>
				<br/>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<span class="offer-code-1">*Total Savings includes all promotional discounts and voucher redemptions.<br/>Delivery fee is calculated separately.<br/>
Certain product savings have been applied to your subtotal amount.   
</span>
</div>
				</c:if>



