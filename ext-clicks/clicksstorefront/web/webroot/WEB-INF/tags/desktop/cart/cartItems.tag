<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="rue" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="storepickup"
	tagdir="/WEB-INF/tags/desktop/storepickup"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>

<!-- Google Analytics tracking Data for basket page starts -->
	<script>
	<c:forEach items="${cartData.entries}" var="entry">
	<c:set value="${entry.product}" var="product" />
		ga('ec:addProduct', {                 
		  'id': '${product.code}',                    
		  'name': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" +"${product.sizeVariantTitle}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''), 
		  'brand': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
		  'price': "${product.price.value}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
		  'quantity': "${entry.quantity}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'')
		 });
		</c:forEach>
		ga('ec:setAction', 'checkout', {'step': 1});  
		ga('send', 'pageview');
	</script>
	<!-- Google Analytics tracking Data for basket page ends -->
	
<div id="cartItems" class="clear">
	<!-- <div class="headline">
		<spring:theme code="basket.page.title.yourItems"/>
		<span class="cartId">
			<spring:theme code="basket.page.cartId"/>&nbsp;<span class="cartIdNr">${cartData.code}</span>
		</span>
	</div>  -->

	<div class="inidiviual-order-wrap shopping-list shop-prod-list ">
		<!-- <div class="cart"> -->
		<div class="order-product-head clearfix hidden-xs">
			<!-- Title-->
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-6 shop-list"
				id="header2">
				<h4>
					<spring:theme code="basket.page.title" />
				</h4>
			</div>
			<div
				class="col-lg-3 col-md-3 col-sm-3 col-xs-4 shop-list text-center"
				id="header4">
				<h4>
					<spring:theme code="basket.page.quantity" />
				</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 shop-list"
				id="header3">
				<h4>
					<spring:theme code="basket.page.itemPrice" />
				</h4>
			</div>
			<!-- Title-->
		</div>
		<!-- Shopping list row -->
		<c:forEach items="${cartData.entries}" var="entry">
			<c:url value="${entry.product.url}" var="productUrl" />
			<div class="border shop-wrap clearfix" id="otheritems">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 shop-list cart_img">
					<a href="${productUrl}"> <product:productPrimaryImage
							product="${entry.product}" format="cartIcon" />
					</a>
				</div>

				<div
					class="col-lg-5 col-md-5 col-sm-5 col-xs-4 shop-list paddingNone">
					<div class="prod-info">
						<h4>
							<a href="${productUrl}">${entry.product.brand}</a>
						</h4>
						<ycommerce:testId code="cart_product_name">
							<p>
								<a href="${productUrl}">${entry.product.name}</a>
							</p>
						</ycommerce:testId>
						<div class="price-wrap">
							<c:choose>
								<c:when
									test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
									<div class="price red">
									<c:choose>
									<c:when test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
									<fmt:formatNumber minFractionDigits="2" var="productPrice" pattern="####.##" value="${entry.product.price.grossPriceWithPromotionApplied}" />
									R<dateFormat:dateFormat conversionType="rand" input="${productPrice}"/>
									</c:when>
									<c:otherwise>
									<fmt:formatNumber minFractionDigits="2" var="productPrice" pattern="####.##" value="${entry.product.price.value}" />
									R<dateFormat:dateFormat conversionType="rand" input="${productPrice}"/>
									</c:otherwise>
									</c:choose>
									</div>
								</c:when>
								<c:otherwise>
									<div class="price blue">
									<format:price priceData="${entry.basePrice}"/>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
						<c:set var="entryStock"
							value="${entry.product.stock.stockLevelStatus.code}" />
							<div class="price-offer">
							<c:if
								test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
	 								Was <format:price priceData="${entry.basePrice}"/>					
	 							<span class="price red">Save <fmt:formatNumber minFractionDigits="2" var="promoPrice" pattern="####.##" value="${entry.product.price.value - entry.product.price.grossPriceWithPromotionApplied}" />
										<c:set var="gross" value="${promoPrice}" />
										<c:set var="dec" value="${fn:split(gross, '.')}" />
										<c:set var="dec1" value="${dec[0]}" />
										<c:set var="dec2" value="${dec[1]}" />
										${dec1}<sup>${dec2}</sup>
									<dateFormat:dateFormat conversionType="rand" input="${save}" />
								</span>
							</c:if>
						</div>  
						<c:forEach items="${entry.potentialPromoDescription}" var="promotion">
							<ul class="potentialPromotions">
							<li>
												<div class="prod_notify tick">
												<a href="${productUrl}">
													<ycommerce:testId code="cart_potentialPromotion_label">
													Missed Savings! <br />
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion,'productName',entry.product.name)}"></c:set> 
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion}
												</c:otherwise>
												</c:choose>
												</ycommerce:testId>
												</a>
												</div>
											</li>
											</ul>
							</c:forEach>
						<c:forEach items="${entry.appliedPromoDescription}" var="promotion">
						<ul class="appliedPromotions">
						<li>
												<div class="prod_notify tick">
													<ycommerce:testId code="cart_appliedPromotion_label">
												
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion,'productName',entry.product.name)}"></c:set> 
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion}
												</c:otherwise>
												</c:choose>
												
												</ycommerce:testId>
												</div>
											</li>
											</ul>
						</c:forEach>
					<!-- 	<c:if
							test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
							<ul class="appliedPromotions">
								<c:forEach items="${cartData.appliedProductPromotions}"
									var="promotion">
									<c:set var="displayed" value="false" />
									<c:forEach items="${promotion.consumedEntries}"
										var="consumedEntry">
										<c:if
											test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
											<c:set var="displayed" value="true" />
											<li>
												<div class="prod_notify tick">
													<ycommerce:testId code="cart_appliedPromotion_label">
												
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion.description, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion.description,'productName',entry.product.name)}"></c:set>
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion.description}
												</c:otherwise>
												</c:choose>
												
												</ycommerce:testId>
												</div>
											</li>
										</c:if>
									</c:forEach>
								</c:forEach>
							</ul>
							<!-- 	<c:forEach items="${productPromo}" var="promotion">  
							<ycommerce:testId code="cart_appliedPromotion_label">
													${promotion.description}
							</ycommerce:testId>
							</c:forEach> 
						</c:if>  -->
					</div>
				</div>
				<c:url value="/cart/update" var="cartUpdateFormAction" />
				<div
					class="col-lg-3 col-md-3 col-sm-3 col-xs-5 paddingNone shop-list text-center no_items">
					<form:form id="updateCartForm${entry.entryNumber}"
						action="${cartUpdateFormAction}" method="post"
						commandName="updateQuantityForm${entry.entryNumber}">
						<c:if test="${entry.updateable}">
							<ycommerce:testId code="cart_product_removeProduct">
								<spring:theme code="text.iconCartRemove" var="iconCartRemove" />
								<button id="QuantityProduct_${entry.entryNumber}"
									class="removeProductQuantity minus btn-default">-</button>
							</ycommerce:testId>
						</c:if>
						<input type="hidden" name="entryNumber"
							value="${entry.entryNumber}" />
						<input type="hidden" name="productCode"
							value="${entry.product.code}" />
						<input type="hidden" name="initialQuantity"
							value="${entry.quantity}" />
						<ycommerce:testId code="cart_product_quantity">
							<form:label cssClass="skip" path="quantity"
								for="quantity${entry.entryNumber}">
								<spring:theme code="basket.page.quantity" />
							</form:label>
							<form:input disabled="${not entry.updateable}" type="text"
								size="1" id="quantity${entry.entryNumber}"
								class="form-control quantity" path="quantity" />
						</ycommerce:testId>

						<c:if test="${entry.updateable}">
							<ycommerce:testId code="cart_product_updateQuantity">
								<button id="QuantityProduct_${entry.entryNumber}"
									class="updateQuantityProduct plus btn-default">+</button>
							</ycommerce:testId>
						</c:if>
					</form:form>
					<c:if test="${entry.updateable}">
						<ycommerce:testId code="cart_product_removeProduct">
							<spring:theme code="text.iconCartRemove" var="iconCartRemove" />
							<a href="#" id="RemoveProduct_${entry.entryNumber}"
								class="submitRemoveProduct">${iconCartRemove}</a>
						</ycommerce:testId>
					</c:if>
				</div>
				<div
					class="col-lg-2 col-md-2 col-sm-2 col-xs-3 shop-list price-right">
					<c:choose>
						<c:when
							test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
							<c:choose>
								<c:when test="${not empty entry.totalPrice and entry.totalPrice.formattedValue ne 'R0.00'}">
									<format:discountedPrice priceData="${entry.totalPrice}"
										displayFreeForZero="true" />
								</c:when>
								<c:otherwise>
									<format:discountedPrice priceData="${entry.basePrice}"
										displayFreeForZero="true" />
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${not empty entry.totalPrice and entry.totalPrice.formattedValue ne 'R0.00'}">
									<format:discountedPrice priceData="${entry.totalPrice}"
										displayFreeForZero="true" />
								</c:when>
								<c:otherwise>
									<format:discountedPrice priceData="${entry.basePrice}"
										displayFreeForZero="true" />
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</c:forEach>
		<!-- Shopping list row -->
		<div class="totalcost">
			<div class="shop-wrap clearfix">
				<div class="subTotalexcluding">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 price-right ">
						<h4>
							<spring:theme code="basket.page.totals.subtotal" />
							<br/>
							<div class="specialCheckout"><span> (includes VAT and excluding delivery cost)</span></div>
						</h4>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 price-right">
						<c:choose>
							<c:when
								test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
								<format:discountedPrice priceData="${cartData.subTotal}" />
							</c:when>
							<c:otherwise>
								<format:price priceData="${cartData.subTotal}" />
							</c:otherwise>
						</c:choose>
					</div>
				</div>
<%-- 				<c:if --%>
<%-- 					test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}"> --%>
<!-- 					<div class="totalsaving"> -->
<!-- 						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 price-right">(You -->
<!-- 							have saved</div> -->
<!-- 						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 price-right"> -->
<%-- 							<ycommerce:testId code="Order_Totals_Savings"> --%>
<%-- 								<format:discountedPrice priceData="${cartData.totalDiscounts}" />) --%>
<%-- 									</ycommerce:testId> --%>
<!-- 						</div> -->
<!-- 					</div> -->
<%-- 				</c:if> --%>
			</div>
		</div>
	</div>

</div>

