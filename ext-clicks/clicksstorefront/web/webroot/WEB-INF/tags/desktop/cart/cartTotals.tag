<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTaxEstimate" required="false"
	type="java.lang.Boolean"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>

<table id="orderTotals">
	<tbody>
		<%-- <dl>
			<dt>
				<h4>
					<spring:theme code="basket.page.totals.subtotal" />
				</h4>
			</dt>
			<dd>
				<ycommerce:testId code="Order_Totals_Subtotal">
					<format:cartPrice priceData="${cartData.subTotal}" />
				</ycommerce:testId>
			</dd>
		</dl>--%>
		<c:if test="${not empty cartData.orderDiscounts and cartData.orderDiscounts.value > 0}">
		    <div class="savings">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 paddingNone price-right">
					<div class="savings-tit">
					  <h4>
						<spring:theme code="basket.page.totals.orderlevel.savings" text="Discounts & Vouchers"/>
				      </h4>
				 </div>
			    </div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 paddingNone price-right">
					<div class="price-wrap dis-text">
				<div class="price red">			
					<ycommerce:testId code="Order_Totals_Savings">
								<format:discountedPrice priceData="${cartData.orderDiscounts}"/>
								</ycommerce:testId>
				</div>		
		   	</div>				
				</div>
			</div>						
		</c:if> 
		<%-- <c:choose>		 --%>
		<c:if test="${not empty cartData.deliveryCost and cartData.deliveryCost.value > 0}">
		<div class="deliveryfee">
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 paddingNone price-right">
						<div class="savings-tit blue">
						  <h4 class="blue savings-tit-1 ">
							<spring:theme code="basket.page.totals.delivery" />
					      </h4>
					 </div>
				    </div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 paddingNone price-right">
					<div class="price-wrap dis-text">
				<div class="price blue">			
					<format:cartPrice priceData="${cartData.deliveryCost}"/>
				</div>		
		   	</div>					 
					</div>
			</div>				
		</c:if>
	<%--	<c:otherwise>
		<div class="deliveryfee">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 paddingNone price-right">
					<h4>
						<spring:theme code="basket.page.totals.delivery" />
					</h4>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 paddingNone price-right">
			<h4><mark><spring:theme code="basket.page.totals.delivery.cost" /> </mark></h4>
			</div>
			</div>
		</c:otherwise> 
		</c:choose> --%>
		<cart:taxExtimate cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" />
		<div class="totalcost">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 paddingNone price-right">
				<h4>
					<spring:theme code="basket.page.totals.total" />
				</h4>
				<div class="specialCheckout">
				<span> Total includes
										<c:set var="totalTax" value="${cartData.totalTax.formattedValue}"/>
										<c:set var="dec" value="${fn:split(totalTax, '.')}" />
										<c:set var="dec1" value="${dec[0]}" />
										<c:set var="dec2" value="${dec[1]}" />
										${dec1}<sup>${dec2}</sup> VAT </span>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 paddingNone price-right">
				<ycommerce:testId code="cart_totalPrice_label">
					<c:choose>
						<c:when test="${not empty cartData.totalTax.formattedValue and cartData.totalTax.formattedValue ne 'R0.00'}">
							<format:cartPrice priceData="${cartData.totalPriceWithTax}" />
							<%-- <span> Your order includes 
										<c:set var="totalTax" value="${cartData.totalTax.formattedValue}"/>
										<c:set var="dec" value="${fn:split(totalTax, '.')}" />
										<c:set var="dec1" value="${dec[0]}" />
										<c:set var="dec2" value="${dec[1]}" />
										${dec1}<sup>${dec2}</sup> VAT </span> --%>
						</c:when>
						<c:otherwise>
						   <c:choose>
                                 <c:when test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
                                 	 <format:discountedPrice priceData="${cartData.totalPrice}"/>
                                 </c:when>
                                 <c:otherwise>
                                 <c:choose>
                                 <c:when test="${not empty cartData.totalPrice and cartData.totalPrice.formattedValue ne 'R0.00' }">
                                 <format:price priceData="${cartData.totalPrice}"/>
                                 </c:when>
                                 <c:otherwise>
                                 <format:price priceData="${cartData.subTotal}" />
                                 </c:otherwise>
                                 </c:choose>
                                 </c:otherwise>
                                 </c:choose>
						</c:otherwise>
					</c:choose>

				</ycommerce:testId>
			</div>
		</div>
				<c:if test="${not empty cartData.totalDiscounts and cartData.totalDiscounts.value > 0}">
		<div class="savings">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 paddingNone price-right">
					<div class="savings-tit red"><h4>
						<spring:theme code="basket.page.totals.totalsavings" text="Total Saved"/>
					</h4></div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 paddingNone price-right">
			<div class="price-wrap">
			<div class="price red">
					<format:discountedPrice priceData="${cartData.totalDiscounts}"/>
				</div>
				</div>
				</div>
				</div>
				
		</c:if>
	</tbody>
</table>



