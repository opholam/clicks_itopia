<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<form:form action="/cart" method="get" commandName="cartPageCalForm" >
<c:choose>
<c:when test="${user.uid eq 'anonymous' and not empty clubcardPoints}">
<h4>You could <mark> earn ${clubcardPoints}</mark> ClubCard points.</h4>
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${empty user.memberID }">
<c:if test="${not empty clubcardPoints}">
<h4>You could earn <mark> ${clubcardPoints} ClubCard points.</mark>  </h4>
</c:if>
</c:when>
<c:otherwise>
<c:if test="${not empty clubcardPoints}">
<h4> <mark> ${clubcardPoints} points </mark></h4>
</c:if>
</c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</form:form>

