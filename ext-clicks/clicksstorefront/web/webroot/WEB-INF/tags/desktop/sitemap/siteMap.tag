<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="sitemap" tagdir="/WEB-INF/tags/desktop/sitemap"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="category" required="true"
	type="de.hybris.platform.category.model.CategoryModel"%>
<ul class="innerSiteMap">
	<c:forEach items="${category.categories}" var="subcategory">
		<li><span class="collapseMap"></span>
		<a href="./c/${subcategory.code}">${subcategory.name}</a>
			<c:if test="${not empty subcategory.categories && subcategory.categories.size() > 0}">
				<sitemap:siteMap category="${subcategory}" />
			</c:if>
		</li>
	</c:forEach>
</ul>