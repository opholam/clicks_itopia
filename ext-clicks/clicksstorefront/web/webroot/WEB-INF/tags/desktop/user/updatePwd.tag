<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<%-- <cms:pageSlot position="SideContent" var="feature" element="div" class="span-4 side-content-slot cms_disp-img_slot"> --%>
<%-- 	<cms:component component="${feature}"/> --%>
<%-- </cms:pageSlot> --%>

	
	<section class="rewards account">
		<div class="container">			
			<div class="acc-heading">
				<h1><spring:theme code="updatePwd.title"/></h1>
			</div>
			<div id="globalMessages">
				<common:globalMessages/>
			</div>
		</div>
	</section>
	<div class="contentWrapper bg-gray">
				<div class="container">
						<div class="accLandOuter reset_form   clearfix">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2><spring:theme code="updatePwd.title"/></h2>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 clearfix">
									<div class="row">
										<div class="pad-spacing form-wrap">
											<p>
												<spring:theme code="updatePwd.description" />
											</p>
											<p class="required">
												<spring:theme code="form.required" />
											</p>
											<form:form method="post" commandName="updatePwdForm">
												<div class="form-group clearfix">
													<formElement:formPasswordBox idKey="updatePwd-pwd"
														labelKey="updatePwd.pwd" path="pwd"
														inputCSS="text password strength" mandatory="true" />
												</div>
												<div class="form-group clearfix">
													<formElement:formPasswordBox idKey="updatePwd.checkPwd"
														labelKey="updatePwd.checkPwd" path="checkPwd"
														inputCSS="text password" mandatory="true"
														errorPath="updatePwdForm" />
												</div>
												<div class="form-group clearfix">
													<ycommerce:testId code="update_update_button">
														<button class="btn btn-save-change">
															<spring:theme code="updatePwd.submit" />
														</button>
													</ycommerce:testId>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
				</div>
			</div>
	
	<%-- <div class="title_holder">
		<h2><spring:theme code="updatePwd.title"/></h2>
	</div> --%>

	<%-- <div class="item_container">
		<p><spring:theme code="updatePwd.description"/></p>
		<p class="required"><spring:theme code="form.required"/></p>
		<form:form method="post" commandName="updatePwdForm">
			<div class="form_field-elements">
				<div class="form_field-input">
					<formElement:formPasswordBox idKey="updatePwd-pwd" labelKey="updatePwd.pwd" path="pwd" inputCSS="text password strength" mandatory="true"/>
					<formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd" path="checkPwd" inputCSS="text password" mandatory="true" errorPath="updatePwdForm"/>
				</div>
			</div>
			<div class="form-field-button">
				<ycommerce:testId code="update_update_button">
					<button class="form"><spring:theme code="updatePwd.submit"/></button>
				</ycommerce:testId>
			</div>
		</form:form>
	</div> --%>
