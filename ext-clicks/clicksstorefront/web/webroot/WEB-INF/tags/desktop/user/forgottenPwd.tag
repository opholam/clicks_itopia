<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="form-wrap clearfix forgotPassword">
		<!--  <div class="headline"><spring:theme code="forgottenPwd.title"/></div>
		<div class="required right"><spring:theme code="form.required"/></div>
		<div class="description"><spring:theme code="forgottenPwd.description"/></div>-->
		
		
		<%-- <c:url var="actionVal" value="/login/pw/request"/> --%>
		<div class="clearfix ">
				<div class="pull-left">
					<h4>Forgotten password?</h4>
				</div>
				<div class="pull-right">
				<a href="#" class="go-back">Go Back</a>
				</div>
		</div>
		<c:if test="${not empty emailDoenotExist }">
			<div class="error_msg"><spring:theme code="forgottenPwd.email.doesnotexist"/></div>
		</c:if>										
		<form:form method="post" commandName="forgottenPwdForm" action="${request.contextPath}/login/pw/request">
			<formElement:formInputBox  idKey="forgottenPwd.email" labelKey="forgottenPwd.email" path="email" inputCSS="form-control" mandatory="true"/>
			<button class="btn" id="forgotPwdButton" type="submit"><spring:theme code="forgottenPwd.submit"/></button>
			<div class="text-center">
			<a href="<c:url value="/forgotten-details"/>">I've forgotten all details</a>
			</div>
		</form:form>
</div>
