<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

	<form:form action="${action}" method="post" commandName="guestForm">
		<h4>Shop online as a guest</h4>
		<input type="hidden" id="guest.email"  name="email" value="GUEST@CLICKS.COM"/>
		<ycommerce:testId code="guest_Checkout_button">
					<div class="text-center"><button type="submit" class="btn btn-save-change" onclick="">Checkout as Guest</button></div>
		</ycommerce:testId>
	</form:form>
	
