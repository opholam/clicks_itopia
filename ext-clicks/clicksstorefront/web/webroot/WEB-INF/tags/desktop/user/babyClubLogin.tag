<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%-- <c:url value="/login/register" var="registerActionUrl" />  --%>
<c:url value="/register/registerCustomer" var="registerActionUrl" />

<div class="bg-white acc-col-4">
<form:form action="${action}" method="post" commandName="loginForm" class="login_form" id="">	
<div class="bg-white clearfix bc-login">
	<div class="pad-spacing form-wrap">
		<div class="form-group clearfix">
			<formElement:formInputBox idKey="j_username"  labelKey="login.email" path="j_username" inputCSS="form-control" mandatory="true"/>
		</div>
		<div class="form-group clearfix">
			<formElement:formPasswordBox idKey="j_password" labelKey="login.password" path="j_password" inputCSS="form-control password" mandatory="true"/>
		</div>
 		<div class="form-group clearfix">
			<input type="checkbox" name="sms" class="check-hide" id="remMe">
			<label for="remMe" class="check-box">
				<span><strong>Remember me -</strong> optional</span>
			</label>
		</div>
 		<ycommerce:testId code="login_Login_button">
 		<button type="submit" class="btn btn-save-change" onclick="window.location.href='${action}'">Sign in &amp; Join Babyclub</button>
		</ycommerce:testId>
		
	</div>
</div>
</form:form>
<div class="pad-spacing form-wrap">
<div class="separator"></div>
<h4>I don't have an online account</h4>
		<a  class="btn btn-save-change" onclick="window.location.href='${registerActionUrl}'">Create Account &amp; Join Babyclub</a>
</div>
</div>

		
