<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!DOCTYPE html>
<html lang="${currentLanguage.isocode}">
<head>
	<title>
		${not empty pageTitle ? pageTitle : not empty cmsPage.title ? cmsPage.title : 'Accelerator Title'}
	</title>
	
	<c:set var="showNextLink" value="true" />
	<c:set var="showPrevLink" value="true" />
	<c:set var="showRootPageLink" value="false" />
	
	<c:if test="${cmsPage.title eq 'Product List'}">
		<c:set var="nextPage" value="1" />
		<c:set var="prevPage" value="" />
		<c:choose>
			<c:when test="${not empty  param.page}">
				<c:set var="nextPage" value="${param.page + 1}" />
				<c:set var="prevPage" value="${param.page - 1}" />
				<c:if test="${param.page eq 1}">
					<c:set var="prevPage" value="" />
					<c:set var="showPrevLink" value="false" />
					<c:set var="showRootPageLink" value="true" />
				</c:if>
				<c:if test="${not empty searchPageData.pagination and param.page eq (searchPageData.pagination.numberOfPages -1)}">
					<c:set var="nextPage" value="" />
					<c:set var="showNextLink" value="false" />
				</c:if>
			</c:when>
			<c:otherwise>
				<c:set var="showPrevLink" value="false" />
			</c:otherwise>
		</c:choose>
		
		<c:if test="${showNextLink}">
			<link rel="next" href="${request.scheme}://${request.serverName}${request.contextPath}${requestScope['javax.servlet.forward.servlet_path']}?page=${nextPage}" id="plpLoadMoreSEONext"/>
		</c:if>
		
		<c:if test="${showPrevLink}">
			<link rel="prev" href="${request.scheme}://${request.serverName}${request.contextPath}${requestScope['javax.servlet.forward.servlet_path']}?page=${prevPage}" id="plpLoadMoreSEOPrev"/>
		</c:if>
		
		<c:if test="${showRootPageLink}">
			<link rel="prev" href="${request.scheme}://${request.serverName}${request.contextPath}${requestScope['javax.servlet.forward.servlet_path']}" id="plpLoadMoreSEOPrev"/>
		</c:if>
		
		<input type="hidden" value="${nextPage}" id="seoPageNext" />
		<input type="hidden" value="" id="seoPagePrev" />
	</c:if>
	
	<%-- Meta Content --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<%-- Additional meta tags --%>
	<htmlmeta:meta items="${metatags}"/>

<%-- 	<c:if test="${not empty metaRobots}"> --%>
<%-- 		<meta name="robots" content="${metaRobots}"/> --%>
<%-- 	</c:if> --%>


	<%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
	<template:styleSheets/>

	<%-- Inject any additional CSS required by the page --%>
	<jsp:invoke fragment="pageCss"/>
	 <analytics:analytics/> 

	<%-- Favourite Icon --%>
	<spring:theme code="img.favIcon" text="/" var="favIconPath"/>
    <link rel="shortcut icon" type="image/x-icon" media="all" href="${originalContextPath eq '/' ? '' : originalContextPath}${favIconPath}" />

<c:if test="${!empty googleApiVersion}">
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=${googleApiVersion}&amp;key=${googleApiKey}&amp;sensor=false"></script>
</c:if>


<script type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>

<script type="text/javascript">
	(function() {
		var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true;
		em.src = ('https:' == document.location.protocol ? 'https://za-ssl' : 'http://za-cdn') + '.effectivemeasure.net/em.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s);
	})();
</script>
<noscript>
	<img src="https://za.effectivemeasure.net/em_image" alt="" style="position:absolute; left:-5px;" />
</noscript>

<c:set var = "flag" value="true" />
<c:if test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], '/specials')}">
		<c:set var="allCategories" value="${param.allCategories}"/>
		<c:set var="allVal" value="${fn:length(fn:split(allCategories, ','))}"/>
		 <c:if test="${not empty allCategories and (allVal eq 1)}">
		 	 <c:set var="flag" value="false" />
	 	</c:if>
	 	
	 	<c:set var="promotionTypeID" value="${param.promotionTypeID}"/>
		 <c:set var="promoVal" value="${fn:length(fn:split(promotionTypeID, ','))}"/>
		 <c:if test="${not empty promotionTypeID and (promoVal eq 1)}">
		 	 <c:set var="flag" value="false" />
		 </c:if>
</c:if>

<c:if test="${not fn:contains(requestScope['javax.servlet.forward.servlet_path'], '/conditionList') and flag}">
	<link rel="canonical" href="${request.scheme}://${request.serverName}${request.contextPath} ${requestScope['javax.servlet.forward.servlet_path']}" />
</c:if>

</head>

<body class="${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${currentLanguage.isocode}">

	<%-- Inject the page body here --%>
	<jsp:doBody/>

	<form name="accessiblityForm">
		<input type="hidden" id="accesibility_refreshScreenReaderBufferField" name="accesibility_refreshScreenReaderBufferField" value=""/>
	</form>
	<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>

	<%-- Load JavaScript required by the site --%>
	<template:javaScript/>
	
	<%-- Inject any additional JavaScript required by the page --%>
	<jsp:invoke fragment="pageScripts"/>	


</body>

<debug:debugFooter/>

</html>
