<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>

<script type="text/javascript" src="${themeResourcePath}/clicks/js/combined1.js"></script>

<template:javaScriptVariables/>

<script type="text/javascript" src="${themeResourcePath}/clicks/js/combined2.js"></script>

<!--[if IE]><script type="text/javascript" src="${commonResourcePath}/js/excanvas-r3.compiled.js"></script>-->

<script type="text/javascript" src="${themeResourcePath}/clicks/js/combined3.js"></script>

<%-- Cms Action JavaScript files --%>
<c:forEach items="${cmsActionsJsFiles}" var="actionJsFile">
    <script type="text/javascript" src="${commonResourcePath}/js/cms/${actionJsFile}"></script>
</c:forEach>

<%-- AddOn JavaScript files --%>
<c:forEach items="${addOnJavaScriptPaths}" var="addOnJavaScript">
    <script type="text/javascript" src="${addOnJavaScript}"></script>
</c:forEach>

<script type="text/javascript" src="${themeResourcePath}/clicks/js/combined4.js"></script>

<!--[if lt IE 9]>
      <script src="${themeResourcePath}/clicks/js/html5shiv.js"></script>
      <script src="${themeResourcePath}/clicks/js/respond.min.js"></script>
    <![endif]-->
	
<script type="text/javascript" src="${themeResourcePath}/clicks/js/combined5.js"></script>