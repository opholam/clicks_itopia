<%@ attribute name="supportedCountries" required="true" type="java.util.List"%>
<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/desktop/address"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/clickscheckoutaddon/desktop/checkout/multi" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>



<%-- <c:if test="${not empty deliveryAddresses}">
	<button type="button" class="positive clear view-address-book" id="viewAddressBook" >
		<spring:theme code="checkout.checkout.multi.deliveryAddress.viewAddressBook" text="View Address Book"/>
	</button>
</c:if> --%>

<input type="hidden" id="contextPath" value="${contextPath}">

	<%-- <input type="hidden" id="addressFormPostcode" value="${addressForm.postcode}"> --%>
	<%-- <div id="countrySelector" data-address-code="${addressData.id}" data-country-iso-code="${addressData.country.isocode}" class="clearfix">
		<formElement:formSelectBox idKey="address.country"
		                           labelKey="address.country"
		                           path="countryIso"
		                           mandatory="true"
		                           skipBlank="false"
		                           skipBlankMessageKey="address.selectCountry"
		                           items="${supportedCountries}"
		                           itemValue="isocode"
		                           selectedValue="${addressForm.countryIso}"/>
	</div> --%>
							<div class="deliver_areaSearch" >
								<form id="delAreaForm" onsubmit="return false;" >								
								<div class="form-group" style=${editMode eq true?'display:none':'display:block'}>
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 paddingNone">
								<input id="address.postcode" type="text" name="postcode" placeholder="Postcode" value="${addressForm.postcode}" class="text storeSearchPostCode" />
								</div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 paddingNone">
								<button class="btn btn-save-change" id="deliveryAreaSearchButton" >Search</button>
								</div>
								</div>
								</form>
								<div  id="isDeliverableDiv" >
								
								</div>
							</div>
							<div class="deliver_areaForm">
							<h3>Delivery Address</h3>					
<form:form method="post" commandName="addressForm"  >
	<div style="${editMode eq true?'display:none':'display:block'}">
	<form:hidden path="addressId" class="add_edit_delivery_address_id" status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" id="deliveryAddressForm"/>
	<input type="hidden" name="bill_state" id="address.billstate"/>
<%-- 	<form:hidden path="selectedDeliveryMode" id="selectedDeliveryMode"/> --%>
	<form:hidden path="countryIso" />
	
<%-- 	<formElement:formSelectBox idKey="address.country"
		                           labelKey="address.country"
		                           path="countryIso"
		                           mandatory="true"
		                           skipBlank="false"
		                           skipBlankMessageKey="address.selectCountry"
		                           items="${supportedCountries}"
		                           itemValue="isocode"
		                           selectedValue="${addressForm.countryIso}"/> --%>
		<div class="form-group clearfix">	<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="text" mandatory="true"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="text" mandatory="false"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.Suburb" labelKey="address.Suburb" path="suburb" inputCSS="readonlyField" mandatory="true"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="text" /></div>
		<div class="form-group clearfix"><formElement:formSelectBox idKey="address.province" labelKey="address.province" skipBlankMessageKey="form.select.empty" path="province"  items="${province}" /></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.country" labelKey="address.country" path="countryName" inputCSS="readonlyField" mandatory="true" /></div>
	<div class="form-group clearfix"><div class=" control-group"><label class="control-label">Post Code</label> <input id="address.postcode.show" name="postcode" value="${addressForm.postcode}" type="text" readonly  /></div></div>
<%-- 		<div class="form-group clearfix"><formElement:formInputBox idKey="address.postcode.show" labelKey="address.postcode" path="postcode" inputCSS="text" mandatory="true"/></div> --%>
	<div class="form-group clearfix"><formElement:formTextArea idKey="address.deliveryInstructions" labelKey="address.deliveryInstructions" path="deliveryInstructions"  /></div>
	<%-- <div class="form-group clearfix"><formElement:formSelectBox idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}" selectedValue="${addressForm.titleCode}"/></div> --%>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix"><formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" mandatory="true" /></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.alternatePhone" labelKey="address.alternatePhone" path="alternatePhone" inputCSS="text" /></div>
	
	<div class="form-group clearfix contact_method" style="display: none;"><label class="control-label">Preferred contact method</label>
	<div class="radioOuter">
	<form:radiobutton path="preferredContactMethod" id="address.preferredContactMethod1" cssClass="radio-hide" label="Email"  value="0" />
	</div>
	<div class="radioOuter">
	<form:radiobutton path="preferredContactMethod" id="address.preferredContactMethod2"  cssClass="radio-hide" label="SMS"  value="1" />
	</div>
	</div>
	</div>
	<div id="deliveryMethodsDiv" style="${edit eq true?'display:none':'display:block'}">
	<c:if test="${not empty deliveryModes}">
	<div id="deliveryMethodsOuterDiv" ><div class="delMethodRate"><h2>Delivery Method <strong class="pull-right">Cost</strong></h2>
	<c:forEach items="${deliveryModes}" var="deliveryMode">
	<div class="choose_delivery_method">
		<div class="delivery_method_item radioOuter pull-left">
			<form:radiobutton path="selectedDeliveryMode" cssClass="radio-hide" value="${deliveryMode.code}"/>
		<label for="${deliveryMode.code}">${deliveryMode.name}<span> ${deliveryMode.description} </span></label>		
		</div>
		<div class="price pull-right">${deliveryMode.deliveryCost.formattedValue}</div>
		<div class="moreaboutdelivery"><a href="javascript:void(0);" data-toggle="modal" data-target="#moreAboutDelivery">Find out more about delivery fees</a></div>		
		</div>
	</c:forEach>
		</div>
		</div>
	</c:if>
</div>
	<%-- <div id="i18nAddressForm" class="i18nAddressForm">
		<c:if test="${not empty country}">
			<address:addressFormElements regions="${regions}"
			                             country="${country}"/>
		</c:if>
		
	</div> --%>
	<div class="form-additionals clearfix">
		<c:if test="${edit != true  and editMode != true }">
	
						<formElement:formCheckbox idKey="billingAddress" labelKey="checkout.summary.deliveryAddress.asbillingaddress" path="billingAddress" inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
		
<!-- 								<input type="checkbox" name="billingAddress"  id="billingAddress" class="add-address-left-input" />  -->
<!-- 								<label for="billingAddress"  class="add-address-left-label"> -->
<!-- 									Use this delivery address as your billing address (the address that appears on your bank statement)  -->
<!-- <span>(If not, you can add a billing address in the next step.)</span> -->
<!-- 								</label> -->
	</c:if>
				<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
		<c:choose>
		
			<c:when test="${showSaveToAddressBook  and editMode != true}">
				<div class="form-group clearfix">
			
							<formElement:formCheckbox idKey="saveAddressInMyAddressBook" labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook" path="saveInAddressBook" inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
					</div>
			</c:when>
			<c:otherwise>
					<div id="saveAddressInMyAddressBookDiv" class="form-group clearfix" style="display: none;">
			
							<formElement:formCheckbox idKey="saveAddressInMyAddressBook" labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook" path="saveInAddressBook" inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
					</div>
			
			</c:otherwise>
			
		</c:choose>
					<a class="backtoaddress" href="${contextPath}/my-account/saved-addresses" target="_blank">Back to Addressbook</a>
		
			</sec:authorize>
		<sec:authorize ifAllGranted="ROLE_ANONYMOUS">
							
		<c:choose>
			<c:when test="${showSaveToAddressBook and editMode != true}">
		<div class="form-group clearfix" style="display:none;">
								<input type="checkbox" name="saveAddressInMyAddressBook" id="saveAddressInMyAddressBook" class="add-address-left-input"> 
								<label for="saveAddressInMyAddressBook" class="add-address-left-label" >
									Save these address details <br />
  
<span>We will create an account so you can checkout faster next time you shop with us.</span>
								</label>
					</div>			</c:when>
			
		</c:choose>
			</sec:authorize>
	</div>

	<div id="addressform_button_panel" class="form-actions">
			
<%-- 				<c:if test="${not noAddress}"> --%>
<%-- 					<ycommerce:testId code="multicheckout_cancel_button"> --%>
<%-- 						<c:url value="${cancelUrl}" var="cancel"/> --%>
<%-- 							<a class="button" href="${cancel}"><spring:theme code="checkout.multi.cancel" text="Cancel"/></a> --%>
<%-- 					</ycommerce:testId> --%>
<%-- 				</c:if> --%>
				<c:choose>
					<c:when test="${edit eq true}">
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button " type="submit">
								<spring:theme code="checkout.multi.saveAddress" text="Save address"/>
							</button>
						</ycommerce:testId>
					</c:when>
					<c:when test="${editMode eq true}">
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button " type="submit">
								<spring:theme code="checkout.multi.saveDeliveyMode" text="Save Delivery Mode"/>
							</button>
						</ycommerce:testId>
					</c:when>
					<c:otherwise>
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button " type="submit">
								<spring:theme code="checkout.multi.deliveryAddress.clicks.continue" text="Continue"/>
							</button>
						</ycommerce:testId>
					</c:otherwise>
				</c:choose> 
			</div>
</form:form>
</div>
<!-- Modal -->
<div id="moreAboutDelivery" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><spring:theme code="delivery.fees.terms.title"/></h4>
      </div>
      <div class="modal-body">
        <p><spring:theme code="delivery.fees.terms.content"/><br/><a href="${contextPath}/onlineshoppinghelp#delivery"><spring:theme code="delivery.fees.terms.link"/></a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
ga('ec:setAction','checkout', {'step': 2});
</script>