<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${clubCardPartners ne null}">
	<c:forEach items="${clubCardPartners}" var="clubCardPartnerModel">
		
		<!-- BLOCK START -->
	
		<div class="policy_outer">
			<div class="services_wrap clearfix">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<div class="partner_detailsWrap">

						<div class="cmsimage">
							<c:if test="${clubCardPartnerModel.partnerLogo ne null}">
								<img title="partner_2"
									src="${clubCardPartnerModel.partnerLogo.url}" alt="partner_2">
							</c:if>
						</div>

						<div class="content">
							<c:if test="${not empty clubCardPartnerModel.title}">
								${clubCardPartnerModel.title}
							</c:if>
						</div>

						<div class="content">
							<c:if test="${not empty clubCardPartnerModel.summary}">
								${clubCardPartnerModel.summary}
							</c:if>
							<c:if test="${not empty clubCardPartnerModel.description}">
								<a href="#" class="moreInfo" target="financePolicy">Show details</a>
								<div class="financeinfo" id="financePolicy" style="display: none;">
									${clubCardPartnerModel.description}
								</div>
							</c:if>		
						</div>
						
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="content">
						<div class="partner_offer">
							<c:if test="${not empty clubCardPartnerModel.promotionalText}">
								${clubCardPartnerModel.promotionalText}
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- BLOCK END -->
		
	</c:forEach>
</c:if>
















