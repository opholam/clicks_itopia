<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>



<div>
	<!--<div class="headline">Enroll for clubcard </div>-->
	<!--<div class="required right"><spring:theme code="form.required"/></div>
	<div class="description"><spring:theme code="register.description"/></div> -->

	<div class="createOuterWrapper bg-gray" id="joinCcForm">
		<div class="container ">

			<!--<br/> <b><div class="required right"><spring:theme code="form.required"/></div></b> -->

			<div class="createWrapper clearfix ">

				<form:form method="post" commandName="clicksRegisterForm" action="${action}" id="createAccountJoinCC">
				<input type="hidden" name="checkoutForm" id="isCheckoutForm" value="${not empty clicksRegisterForm.checkoutForm ? clicksRegisterForm.checkoutForm : 'false'}"/>
					
					<div class="form_field-elements">

						<div class="row acc-row-5">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5" id="section1">
								<div class=" bg-white  clearfix">
									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>1. Personal details <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong> </h2>
										</div>
									</div>
									<div class="clearfix acc-col-5">
										<div class="pad-spacing form-wrap">

											<formElement:formInputBox idKey="customer.enroll.FirstName" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text mandatory" mandatory="true"/>
											<formElement:formInputBox idKey="customer.enroll.PreferedName" labelKey="customer.enroll.PreferedName" path="preferedName" inputCSS="text" />
											<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text mandatory" mandatory="true"/>
											
											<%-- <formElement:formInputBox idKey="customer.enroll.SAResident" labelKey="customer.enroll.SAResident" path="SAResident" inputCSS="text" mandatory="true"/> --%>
											<%-- <formElement:formInputBox idKey="customer.enroll.nonRSA_DOB" labelKey="customer.enroll.nonRSA_DOB" path="nonRSA_DOB" inputCSS="text" mandatory="true"/> --%>			
											
											<div class="form-group saResident clearfix" > <!-- <div class="form-group saResident" id="dobHide"> -->
													<label><spring:theme code="customer.create.account.isSAResident"/> </label>
													
													 <div class="form-group clearfix">
													 	<div class="clearfix">
													 		 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone radioOuter saResidentEnrollYOuter">
														 		<form:radiobutton path="isSAResident" value="1" label="Yes"  cssClass="radio-hide custom_hide"  /><!--  id="saResidentEnrollY " name="saResi12" -->
															 </div>
															<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
																<formElement:isClubcardFormInputBox idKey="bb" labelCSS="" labelKey="bb" path="IDNumber" inputCSS="ClubcardValue1" mandatory="false"  />
			           										</div>
			           									</div>	
			           									
			           									<div class="clearfix radioOuter">
			           										 
			           											<form:radiobutton path="isSAResident" value="0" label="No"  cssClass="radio-hide customno"  /> <!-- id="saResidentEnrollN" name="saResi12" -->
			           									 
			           									</div>
		           									 </div>
		           									 
		           									<div class="clearfix dobSaOuterFirst " id="cc" > <!--  id="cc" -->
		           									 <label><spring:theme code="customer.create.account.SA_DOB"/> </label>
		           									 <div class="dob">
		           									 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
		           									  <div class="dobWrapper">
															<formElement:DOBformSelectBox selectCSSClass="dobSa12 dateCheck" labelCSS="dobSa12" idKey="customer.create.account.SA_DOB_day" labelKey="customer.create.account.SA_DOB_day" path="SA_DOB_day" mandatory="true" items="${days}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
													  <div class="monthWrapper">
															<formElement:DOBformSelectBox selectCSSClass="monthCheck" idKey="customer.create.account.SA_DOB_month" labelKey="customer.create.account.SA_DOB_month" path="SA_DOB_month" mandatory="true" items="${months}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
													  <div class="yearWrapper">
															<formElement:DOBformSelectBox selectCSSClass="yearCheck" idKey="customer.create.account.SA_DOB_year" labelKey="customer.create.account.SA_DOB_year" path="SA_DOB_year" mandatory="true" items="${years}" />
													  </div>
													  </div>
													</div>
													</div>
											</div>
											
											<%-- <formElement:formInputBox idKey="customer.enroll.Gender" labelKey="customer.enroll.Gender" path="gender" inputCSS="text" mandatory="true"/> --%>
									 		<div class="form-inline" >
												
	           									<div class="gender" ><label><spring:theme code="customer.enroll.Gender"/> </label></div>
	           									<div class="form-group margin-spacing clearfix radioOuter">
												 <form:radiobutton path="gender" value="1" label="Male"  name="male" cssClass="radio-hide" />
												 </div> 
												 <div class="form-group clearfix radioOuter">
												 <form:radiobutton path="gender" value="2" label="Female"  name="female" cssClass="radio-hide" />
												 </div>
	           									</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5" id="section2">
								<div class=" bg-white  clearfix">
									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>2. Contact details <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong> </h2>
										</div>
									</div>
									<div class="clearfix acc-col-5">
										<div class="pad-spacing form-wrap">
										<c:if test="${user.uid eq 'anonymous'}">
											<formElement:formInputBox idKey="customer.enroll.EMailAddress" labelKey="customer.enroll.EMailAddress" path="eMailAddress" inputCSS="text mandatory" mandatory="true"/>
										</c:if>
										<c:set var="contactKey" value="customer.enroll.cell.Number"></c:set>
											<c:if test="${user.uid eq 'anonymous'}">
												<c:set var="contactKey" value="customer.enroll.cell.Number"></c:set>
											</c:if>
											<formElement:formInputBox idKey="${contactKey}" labelKey="${contactKey}" path="contactNumber" inputCSS="text mandatory" mandatory="true"/>
											
											<formElement:formInputBox idKey="customer.enroll.alternate.Number" labelKey="customer.enroll.alternate.Number" path="alternateNumber" inputCSS="text" mandatory="false"/>
											<%-- <formElement:formInputBox idKey="customer.enroll.hybris.RetailerId" labelKey="customer.enroll.hybris.RetailerId" path="hybrisRetailerId" inputCSS="text" mandatory="true"/>
											<formElement:formInputBox idKey="customer.enroll.hybris.CustomerId" labelKey="customer.enroll.hybris.CustomerId" path="customerID" inputCSS="text" mandatory="true"/>
											<formElement:formInputBox idKey="customer.enroll.IDNumber" labelKey="customer.enroll.IDNumber" path="IDNumber" inputCSS="text" mandatory="true"/>
											 --%>
											 <form:hidden path="hybrisRetailerId" />
											 <form:hidden path="memberID" />
											 <c:if test="${user.uid eq 'anonymous'}">
											 	<div class="errorMandatory">
											 <formElement:formPasswordBox idKey="customer.enroll.Password" labelKey="customer.enroll.Password" path="password"  inputCSS="text mandatory" mandatory="true"/>
											 </div>
											  <div class="errorMandatory">
											 <formElement:formPasswordBox idKey="customer.enroll.ConfirmPassword" labelKey="customer.enroll.ConfirmPassword" path="confirmPassword" inputCSS="text mandatory" mandatory="true"/>
											</div>
											 </c:if>
											 
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row acc-row-55">

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55" id="section3">
								<div class="bg-white clearfix">

									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>3. Address details <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong></h2>
										</div>
									</div>
									<div class="clearfix">
										<div class="pad-spacing form-wrap acc-col-55">
										
											<%-- <formElement:formInputBox idKey="customer.enroll.AddressTypeId" labelKey="customer.enroll.AddressTypeId" path="addressTypeId" inputCSS="text" mandatory="true"/> --%>
											<%-- <formElement:formInputBox idKey="customer.enroll.Country" labelKey="customer.enroll.Country" path="country" inputCSS="text" mandatory="true"/> --%>
											<formElement:formSelectBox idKey="customer.enroll.Country" labelKey="customer.enroll.Country" path="country"  mandatory="false" items="${country}" />
											<formElement:formInputBox idKey="customer.enroll.AddressLine1" labelKey="customer.enroll.AddressLine1" path="addressLine1" inputCSS="text mandatory" mandatory="true"/>
											<formElement:formInputBox idKey="customer.enroll.AddressLine2" labelKey="customer.enroll.AddressLine2" path="addressLine2" inputCSS="text" />
											<formElement:formInputBox idKey="customer.enroll.Suburb" labelKey="customer.enroll.Suburb" path="suburb" inputCSS="text mandatory" mandatory="true"/>
											<formElement:formInputBox idKey="customer.enroll.City" labelKey="customer.enroll.City" path="city" inputCSS="text" />
											<%-- <formElement:formInputBox idKey="customer.enroll.Province" labelKey="customer.enroll.Province" path="province" inputCSS="text" mandatory="true"/> --%>
											<div class="errorMandatory">
											<formElement:formSelectBox idKey="customer.enroll.Province" labelKey="customer.enroll.Province" path="province"  items="${province}" />
											</div>
											<formElement:formInputBox idKey="customer.enroll.PostalCode" labelKey="customer.enroll.PostalCode" path="postalCode" inputCSS="text mandatory" mandatory="true"/>
										 
										</div>
									</div>
								</div>
								<div class="blockWhite">
										<div class="disclaimerWrapper"><p><spring:theme code="customer.mandatory"/></p></div>
										</div>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55 edit-form-wrap" id="section4">
								<div class="bg-white clearfix">
									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>
												4. Communication from Clicks <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong> <span>
											</h2>
										</div>
									</div>
									<div class="markettingMessage" style="display: none;">We require at least one way in which you would like to receive your cashback and benefit communications.</div>
									<div class="clearfix">

										<div class="pad-spacing form-wrap acc-col-55">
										
										<%-- 
										<label><spring:theme code="customer.enroll.ACCINFO"/> </label>
									<div class="form-group clearfix checkFix">
										<input type="checkbox" name="marketingEmail" class="check-hide" id="check3"  >
												<label for="check3" class="check-box">
													<spring:theme code="customer.enroll.marketing.Email"/>
												</label>
											</div>	
											<div class="form-group clearfix checkFix">
										<input type="checkbox" name="marketingSMS" class="check-hide" id="check4"  >
												<label for="check4" class="check-box">
													<spring:theme code="customer.enroll.marketing.SMS"/>
												</label>
												
												
											</div>	
											<br/>
										<label><spring:theme code="customer.enroll.MARKETING"/> </label>
										<div class="form-group clearfix checkFix">
										<input type="checkbox" name="marketingEmail" class="check-hide" id="check"  >
												<label for="check" class="check-box">
													<spring:theme code="customer.enroll.marketing.Email"/>
												</label>
											</div>	
											<div class="form-group clearfix checkFix">
										<input type="checkbox" name="marketingSMS" class="check-hide" id="check1"  >
												<label for="check1" class="check-box">
													<spring:theme code="customer.enroll.marketing.SMS"/>
												</label>
											</div>	 --%>
									
											<label><spring:theme code="customer.enroll.ACCINFO" /> </label>
											<formElement:formCheckbox idKey="customer.enroll.marketing.Email3" labelKey="customer.enroll.marketing.Email" path="accinfoEmail" inputCSS="text email-mar marketing-update" />
											<formElement:formCheckbox idKey="customer.enroll.marketing.SMS3" labelKey="customer.enroll.marketing.SMS" path="accinfoSMS" inputCSS="text sms-mar marketing-update" />
										<br/>
										<label><spring:theme code="customer.enroll.MARKETING"/> </label>
											<formElement:formCheckbox idKey="customer.enroll.marketing.Email4" labelKey="customer.enroll.marketing.Email" path="marketingEmail" inputCSS="text" />
											<formElement:formCheckbox idKey="customer.enroll.marketing.SMS4" labelKey="customer.enroll.marketing.SMS" path="marketingSMS" inputCSS="text" />
										
										
											<br/>
											<br/>
											<div class="form-actions clearfix">
												<ycommerce:testId code="enroll_button">
													<button type="submit" class=" btn btn-save-change">
														<spring:theme code='${actionNameKey}' />
													</button>
												</ycommerce:testId>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

