<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="registerNonCCKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="registerNonCCAction" required="true"
	type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer"%>

<div>
	<%-- <div class="headline">Register Non-ClubCard customer</div>
	<div class="required right"><spring:theme code="form.required"/></div>
	<div class="description"><spring:theme code="register.description"/></div> --%>

	<div class="createOuterWrapper bg-gray">
		<div class="container ">
			<!--<br/> <b><div class="required right"><spring:theme code="form.required"/></div></b> -->
			<div class="createWrapper clearfix ">

				<form:form method="post" commandName="registerCustomerForm"
					action="${action}" id="createAccount">
				<form:hidden path="checkoutForm" id="isCheckoutForm"/>
					<div class="row acc-row-4">
						<div
							class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha4 createBoxWrapper">
							<div class=" bg-white heightFormFix">
								<div class="clearfix bg-light-blue">
									<div
										class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2>1. Clubcard</h2>
									</div>
								</div>
								<div class="bg-white clearfix">
									<div class="pad-spacing form-wrap">
										<label><spring:theme
												code="customer.create.account.isClubCard" /> </label>
										<div class="form-group clearfix haveClubcard">
											<div class="  clearfix">
												<div
													class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone radioOuter">
													<form:radiobutton path="isClubCard" value="1" label="Yes"
														name="yes" cssClass="ClubcardYes current radio-hide"
														for="ClubcardHave" id="haveYes" />
												</div>
												<div
													class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone inputClubcardPlaceholder">
													<formElement:isClubcardFormInputBox idKey="haveValue"
														labelKey="" path="clubcardNumber"
														inputCSS="form-control ClubcardValue" mandatory="true" />
												</div>
											</div>
											<div class=" clearfix radioOuter">
												<form:radiobutton path="isClubCard" value="0" label="No"
													cssClass="ClubcardNo radio-hide" for="ClubcardHave"
													id="haveNo" />
												<!-- id="haveNo"  -->
											</div>
										</div>


										<div class="form-group saResident">
											<!-- id="dobHide" -->
											<label><spring:theme
													code="customer.create.account.isSAResident" /> </label>

											<div class="form-group clearfix">
												<div class="clearfix">
													<div
														class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone">
														<div class="custom_hide radioOuter">
															<form:radiobutton path="isSAResident" value="1"
																label="Yes" name="ccSA" id="saResiYes"
																cssClass="radio-hide" />
															<!-- name="ccSA" id="saResiYes" -->
														</div>
													</div>
													<div
														class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone inputSAIDPlaceholder">
														<formElement:isClubcardFormInputBox idKey="bb" labelCSS=""
															labelKey="" path="IDNumber" inputCSS="" mandatory="true" />
													</div>
												</div>

												<div class="clearfix">
													<div class="customno radioOuter">
														<form:radiobutton path="isSAResident" value="0" label="No"
															id="saResiNo" name="ccSA" cssClass="radio-hide" />
														<!--  id="saResiNo" name="ccSA"  -->
													</div>
												</div>
											</div>

											<div class=" clearfix dobSaOuterFirst" id="cc">
												<!--  id="cc" -->
												<label><spring:theme
														code="customer.create.account.SA_DOB" /> </label>
												<div class="dob">
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
														<div class="dobWrapper">
															<formElement:DOBformSelectBox 
															selectCSSClass="dobSa12 dateCheck"
																labelCSS="dobSa12"
																idKey="customer.create.account.SA_DOB_day"
																labelKey="customer.create.account.SA_DOB_day"
																path="SA_DOB_day" mandatory="true" items="${days}" />
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
														<div class="monthWrapper">
															<formElement:DOBformSelectBox
															 selectCSSClass="dobSa12 monthCheck"
																idKey="customer.create.account.SA_DOB_month"
																labelKey="customer.create.account.SA_DOB_month"
																path="SA_DOB_month" mandatory="true" items="${months}" />
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
														<div class="yearWrapper">
															<formElement:DOBformSelectBox
															selectCSSClass="yearCheck"
																idKey="customer.create.account.SA_DOB_year"
																labelKey="customer.create.account.SA_DOB_year"
																path="SA_DOB_year" mandatory="true" items="${years}" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group createTerms" id="termsOuter">
											<formElement:formCheckbox
												idKey="customer.create.account.joinClubCard"
												labelKey="customer.create.account.joinClubCard"
												path="joinClubCard" inputCSS="text " mandatory="true" />
										</div>
										<br />
										<br />
										<div class="form-actions clearfix">
											<ycommerce:testId code="registerNonCC_button">
												<button type="submit" class="btn" id="hideBtn">
													<spring:theme code='${actionNameKey}' />
												</button>
												<button type="submit" class="btn" id="hideBtn2">
													<spring:theme code="customer.enroll.submit.button" />
												</button>
											</ycommerce:testId>
										</div>
									</div>
								</div>
							</div>
						</div>



						<%-- </form:form>
 --%>


						<!--<br/> <b><div class="required right"><spring:theme code="form.required"/></div></b> -->

						<%-- 	<form:form method="post" commandName="registerNonClubCardForm"
					action="${registerNonCCAction}" id="createAccount">
					<div class="form_field-elements">
						 --%>

						<div
							class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5 formCADis">
							<div class=" bg-white  clearfix">
								<div class="clearfix bg-light-blue">
									<div
										class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2>2. Account details</h2>
									</div>
								</div>
								<div class="clearfix acc-col-5">
									<div class="pad-spacing form-wrap">

										<formElement:formInputBox idKey="customer.enroll.EMailAddress"
											labelKey="customer.enroll.EMailAddress" path="eMailAddress"
											inputCSS="text" mandatory="true" />
										<div class="errorMandatory">
											<formElement:formPasswordBox idKey="customer.enroll.Password"
												labelKey="customer.enroll.Password" path="password"
												mandatory="true" />
										</div>
										<div class="errorMandatory">
											<formElement:formPasswordBox
												idKey="customer.enroll.ConfirmPassword"
												labelKey="customer.enroll.ConfirmPassword"
												path="confirmPassword" mandatory="true" />
										</div>
										<br />

									</div>
								</div>
							</div>
						</div>

					</div>

					<div class="row acc-row-5">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5 formCADis registerOuter">
							<div class=" bg-white  clearfix">
								<div class="clearfix bg-light-blue">
									<div
										class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2>3. Personal details</h2>
									</div>
								</div>
								<div class="clearfix">
									<div class="pad-spacing form-wrap">

										<formElement:formInputBox idKey="customer.enroll.FirstName"
											labelKey="customer.enroll.FirstName" path="firstName"
											inputCSS="text" mandatory="true" />
										<formElement:formInputBox idKey="customer.enroll.PreferedName"
											labelKey="customer.enroll.PreferedName" path="preferedName"
											inputCSS="text"  />
										<formElement:formInputBox idKey="customer.enroll.LastName"
											labelKey="customer.enroll.LastName" path="lastName"
											inputCSS="text" mandatory="true" />
										<%--  <formElement:formSelectBox idKey="customer.enroll.Gender" labelKey="customer.enroll.Gender" path="gender" mandatory="true" items="${gender}" /> --%>

										<%--  <div class="form-group saResident">
													<label><spring:theme code="customer.create.account.isSAResident"/> </label>
													
													 <div class="form-group clearfix">
													 	<div class="clearfix">
													 		 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone radioOuter" data-toshow="ClubcardValue1" data-tohide="dobSaOuter1">
																<form:radiobutton path="isSAResident" value="1" label="yes" id="saResi12" cssClass="radio-hide "  name="saResi12"  />
															 </div>
															<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone ClubcardValue1">
																<formElement:isClubcardFormInputBox idKey="ClubcardValue1" labelCSS="" labelKey="" path="IDNumber" inputCSS="" mandatory="true"/>
			           										</div>
			           									</div>	
			           									
			           									<div class="clearfix radioOuter">
			           										<form:radiobutton path="isSAResident" value="0" label="No" id="saResiReg" name="saResi12" cssClass="radio-hide"  data-tohide="ClubcardValue1"  data-toshow="dobSaOuter1" />
			           									</div>
		           									 </div>
		           									 
		           									<div class=" clearfix dobSaOuter" id="dobSaOuter1"> 
		           									 <label><spring:theme code="customer.create.account.SA_DOB"/> </label>
		           									 <div class="dob">
		           									 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
		           									  <div class="dobWrapper">
															<formElement:DOBformSelectBox selectCSSClass="dobSa12" labelCSS="dobSa12" idKey="customer.create.account.SA_DOB_day" labelKey="customer.create.account.SA_DOB_day" path="SA_DOB_day" mandatory="true" items="${days}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
													  <div class="monthWrapper">
															<formElement:DOBformSelectBox idKey="customer.create.account.SA_DOB_month" labelKey="customer.create.account.SA_DOB_month" path="SA_DOB_month" mandatory="true" items="${months}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
													  <div class="yearWrapper">
															<formElement:DOBformSelectBox idKey="customer.create.account.SA_DOB_year" labelKey="customer.create.account.SA_DOB_year" path="SA_DOB_year" mandatory="true" items="${years}" />
													  </div>
													  </div>
													</div>
													</div>
													
											</div>  --%>


										
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55 edit-form-wrap  formCADis" >
								<div class="bg-white clearfix">
									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>
												4. Communication from Clicks <span>
											</h2>
										</div>
									</div>
									<div class="clearfix">
										 <div class="pad-spacing form-wrap acc-col-55">
										<%--
											<label><spring:theme code="customer.enroll.ACCINFO" /> </label>
											<formElement:formCheckbox idKey="customer.enroll.marketing.Email3" labelKey="customer.enroll.marketing.Email" path="accinfoEmail" inputCSS="text" />
											<formElement:formCheckbox idKey="customer.enroll.marketing.SMS3" labelKey="customer.enroll.marketing.SMS" path="accinfoSMS" inputCSS="text" />
										<br/> --%>
										<label><spring:theme code="customer.enroll.MARKETING"/> </label>
											<formElement:formCheckbox idKey="customer.enroll.marketing.Email4" labelKey="customer.enroll.marketing.Email" path="marketingEmail" inputCSS="text" />
											<%-- <formElement:formCheckbox idKey="customer.enroll.marketing.SMS4" labelKey="customer.enroll.marketing.SMS" path="marketingSMS" inputCSS="text" /> --%>
										<div class="form-actions clearfix createBtn">
											<ycommerce:testId code="registerNonCC_button">
												<button type="submit" class="btn btn-save-change">
													<spring:theme code='${registerNonCCKey}' />
												</button>
											</ycommerce:testId>
										</div>
											</div>
									</div>
								</div>
							</div>

					</div>
			</div>
		</div>
	</div>
	</form:form>
</div>

