<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="contact-det bg-white clearfix">
	<div id="globalMessages">
		<common:globalMessages />
	</div>
	<div class="cmsimage">
		<cms:component uid="emailImage"/>
		<!-- <img src="/medias/?context=bWFzdGVyfHJvb3R8NDU0fGltYWdlL2pwZWd8aDcyL2gyYi84ODE0NjEwMDIyNDMwLmpwZ3w0Njk3ZjY0Zjc1MjE3MDMxYzZhY2E1NmI5ZDliZGFmYzZlYmFiNmMxM2ZjYzBlZjVjMmY4NTc5MmYxZmYzZDNj"/>-->
	</div>
	<h3>Send us an email</h3> 
	<div class="row">
		<c:url
			value="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/contactus/contactUsProcessor"
			var="contactusAction" />
		<form:form method="post" commandName="contactUsForm"
			action="${contactusAction}">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="form-wrap">
					<formElement:formInputBox idKey="customer.enroll.FirstName"
						labelKey="customer.enroll.FirstName" path="firstName"
						inputCSS="text" mandatory="true" />
					<formElement:formInputBox idKey="customer.enroll.LastName"
						labelKey="customer.enroll.LastName" path="lastName"
						inputCSS="text" mandatory="true" />
					<formElement:formInputBox idKey="customer.enroll.EMailAddress"
						labelKey="customer.enroll.EMailAddress" path="email"
						inputCSS="text" mandatory="true" />
					<formElement:formInputBox idKey="customer.enroll.contact.Number"
						labelKey="customer.enroll.contact.Number" path="contactNo"
						inputCSS="text" mandatory="true" />
					<formElement:formInputBox idKey="customer.club.card.Number"
						labelKey="customer.club.card.Number" path="clubcardNo"
						inputCSS="text" />
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="form-wrap">
					<div class="errorMandatory">
						<formElement:formSelectBox idKey="address.title"
							labelKey="address.title" path="Topic" mandatory="true"
							items="${Topic}" selectedValue=""
							skipBlankMessageKey="helping.hand.form.select.empty" />
					</div>
					<formElement:formTextArea idKey="customer.club.card.message"
						labelKey="customer.club.card.message" path="message" mandatory="true"
						areaCSS="textarea" />
					<div class="form-actions clearfix">
						<ycommerce:testId code="clincBooking">
							<button type="submit" class="btn primary_btn btn-block positive">
								<spring:theme code='contact.us.enquiry' />
							</button>
						</ycommerce:testId>
					</div>
				</div>
			</div>
		</form:form>
	</div>
</div>

