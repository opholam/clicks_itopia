<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="registerNonCCKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="registerNonCCAction" required="true"
	type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>




	<!--<div class="headline">Enroll for clubcard </div>-->
	<!--<div class="required right"><spring:theme code="form.required"/></div>
	<div class="description"><spring:theme code="register.description"/></div> -->
<c:url value="/login/pw/request" var="passowrdResetUrl" />
<div class="createOuterWrapper bg-gray" id="joinCcForm">
	<div class="container ">
		<!--<br/> <b><div class="required right"><spring:theme code="form.required"/></div></b> -->
		<div class="createWrapper clearfix ">
			<form:form method="post" commandName="clicksRegisterForm" action="${action}" id="createAccount">
<input id="isCheckoutForm" name="checkoutForm" type="hidden" value="${not empty checkoutForm ? checkoutForm : 'false'}">				
				<div class="form_field-elements blockWhite">
					<div class="row acc-row-5">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5" id="cR-1">
							<div class=" bg-white  clearfix">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2>1. Account details <strong class="successTick"> <img  src="${themeResourcePath}/clicks/image/tick.png" /></strong> </h2>
									</div>
								</div>
								<div class="clearfix acc-col-5">
									<div class="pad-spacing form-wrap">
										<div class="form-group clearfix haveClubcard">
										<label><spring:theme code="customer.create.account.isClubCard" /> </label>
											<div class="  clearfix">
												<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone radioOuter">
													<form:radiobutton path="isClubCard" value="1" label="Yes" name="CCNumber" cssClass="ClubcardYes current radio-hide" id="haveYes" />
												</div>
												<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone inputClubcardPlaceholder acc-clubCard">
													<formElement:isClubcardFormInputBox idKey="haveValue" labelKey="" path="memberID" inputCSS="form-control ClubcardValue" mandatory="true" />
												
												</div>
											</div>
											<div class=" clearfix radioOuter">
												<form:radiobutton path="isClubCard" value="0" label="No, I would like to join ClubCard" name="CCNumber" cssClass="ClubcardNo radio-hide"   id="haveNo" />
												<!-- id="haveNo"  -->
											</div>
											<div class=" clearfix radioOuter">
												<form:radiobutton path="isClubCard" value="2" label="No, I would like to create an online account without a ClubCard" name="CCNumber" cssClass="withoutcc radio-hide"   id="withoutcc" />
												<!-- id="haveNo"  -->
											</div>
										</div>
										
										<div class="form-group saResident">
											<!-- id="dobHide" -->
											<label><spring:theme code="customer.create.account.isSAResident" /> </label>
											<div class="form-group clearfix">
												<div class="clearfix">
													<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone">
														<div class="custom_hide radioOuter">
															<form:radiobutton path="isSAResident" value="1" label="Yes" name="ccSA" id="saResiYes" cssClass="radio-hide" />
															<!-- name="ccSA" id="saResiYes" -->
														</div>
													</div>
													<div
														class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone inputSAIDPlaceholder">
														<formElement:isClubcardFormInputBox idKey="SAIDNumber" labelCSS="" labelKey="" path="IDNumber" inputCSS="" mandatory="true" />
													</div>
												</div>

												<div class="clearfix">
													<div class="customno radioOuter">
														<form:radiobutton path="isSAResident" value="0" label="No"
															id="saResiNo" name="ccSA" cssClass="radio-hide" />
														<!--  id="saResiNo" name="ccSA"  -->
													</div>
												</div>
											</div>

											<div class=" clearfix dobSaOuterFirst" >
												<!--  id="cc" -->
												<label><spring:theme
														code="customer.create.account.SA_DOB" /> </label>
												<div class="dob">
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
														<div class="dobWrapper">
															<formElement:DOBformSelectBox selectCSSClass="dobSa12 dateCheck"
																labelCSS="dobSa12"
																idKey="customer.create.account.SA_DOB_day"
																labelKey="customer.create.account.SA_DOB_day"
																path="SA_DOB_day" mandatory="true" items="${days}" />
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
														<div class="monthWrapper">
															<formElement:DOBformSelectBox
															selectCSSClass="monthCheck"
																idKey="customer.create.account.SA_DOB_month"
																labelKey="customer.create.account.SA_DOB_month"
																path="SA_DOB_month" mandatory="true" items="${months}" />
														</div>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
														<div class="yearWrapper">
															<formElement:DOBformSelectBox
															selectCSSClass="yearCheck"
																idKey="customer.create.account.SA_DOB_year"
																labelKey="customer.create.account.SA_DOB_year"
																path="SA_DOB_year" mandatory="true" items="${years}" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group createTerms" id="termsOuter">
											<formElement:formCheckbox idKey="JoinCC" labelKey="customer.create.account.joinClubCard"
												path="joinClubCard" inputCSS="text "  />
										</div>
										<div class="form-actions clearfix">
										<div id="block1emailpwd">
										<div class="emailPwd">
										<c:if test="${user.uid eq 'anonymous'}">
												<formElement:formInputBox idKey="customer.enroll.EMailAddress" labelKey="customer.enroll.EMailAddress" path="eMailAddress" inputCSS="text mandatory" mandatory="true"/>
												<c:if test ="${duplicateCheck eq 'true'}">
												<!-- <button type="submit" class="btn" onclick="window.location='${updatePasswordUrl}'"> Reset Password</button> -->
												<a href="javascript:void(0)" data-url="${passowrdResetUrl}" id="forgotBlock" class="forgot-block btn primary_btn">
														Reset Password
												</a>
												</c:if>
											</c:if>
											<c:if test="${user.uid eq 'anonymous'}">
												<div class="errorMandatory">
												 <formElement:formPasswordBox idKey="customer.enroll.Password" labelKey="customer.enroll.Password" path="password"  inputCSS="text mandatory" mandatory="true"/>
												</div>
												<div class="errorMandatory">
												 <formElement:formPasswordBox idKey="customer.enroll.ConfirmPassword" labelKey="customer.enroll.ConfirmPassword" path="confirmPassword" inputCSS="text mandatory" mandatory="true"/>
												</div>
											</c:if>
											</div>
											</div>
											<ycommerce:testId code="registerNonCC_button">
												<button type="submit" class="btn" id="retrieveBtn">
													<spring:theme code='customer.enroll.submit.button' />
												</button>
												<%-- <button type="submit" class="btn" id="hideBtn2">
													<spring:theme code="customer.enroll.submit.button" />
												</button> --%>
											</ycommerce:testId>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5 form-disabled" id="secondBlock">
							<div class=" bg-white  clearfix">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2>2. Personal details <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong> </h2>
									</div>
								</div>
								<div class="clearfix acc-col-5">
									<div class="pad-spacing form-wrap">
									<div id="block2emailpwd">
									</div>
											<%-- <c:if test="${user.uid eq 'anonymous'}">
												<formElement:formInputBox idKey="customer.enroll.EMailAddress" labelKey="customer.enroll.EMailAddress" path="eMailAddress" inputCSS="text mandatory" mandatory="true"/>
											</c:if>
											<c:if test="${user.uid eq 'anonymous'}">
												<div class="errorMandatory">
												 <formElement:formPasswordBox idKey="customer.enroll.Password" labelKey="customer.enroll.Password" path="password"  inputCSS="text mandatory" mandatory="true"/>
												</div>
												<div class="errorMandatory">
												 <formElement:formPasswordBox idKey="customer.enroll.ConfirmPassword" labelKey="customer.enroll.ConfirmPassword" path="confirmPassword" inputCSS="text mandatory" mandatory="true"/>
												</div>
											</c:if> --%>
											
											<%-- <formElement:formSelectBox idKey="customer.enroll.Title" labelKey="customer.enroll.Title" path="title"  items="${titles}" skipBlankMessageKey="form.select.empty"/> --%>
											
											<div class="control-group wo">
											<label class="control-label " for="customer.enroll.Title">Title</label>
												<div class="controls">
												<form:select path="title"
													 id="customer.enroll.Title" >
														<form:option value="mr">Mr.</form:option>
														<form:option value="mrs">Mrs.</form:option>
														<form:option value="miss">Ms.</form:option>
														<form:option value="dr">Dr.</form:option>
														<form:option value="rev">Rev.</form:option>
												</form:select>
												</div>
											</div>
											
											<formElement:formInputBox idKey="customer.enroll.FirstName" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text mandatory" mandatory="true"/>
											<div class="wo"><formElement:formInputBox idKey="customer.enroll.PreferedName" labelKey="customer.enroll.PreferedName" path="preferedName" inputCSS="text" /></div>
											<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text mandatory" mandatory="true"/>
											
											<c:set var="contactKey" value="customer.enroll.cell.Number"></c:set>
												<c:if test="${user.uid eq 'anonymous'}">
													<c:set var="contactKey" value="customer.enroll.cell.Number"></c:set>
												</c:if>
											<div class="scenarioHide">
											<formElement:formInputBox idKey="${contactKey}" labelKey="${contactKey}" path="contactNumber" inputCSS="text mandatory" mandatory="true"/>
											</div>
											<div class="wo">
											<div class="scenarioHide-2">
												<formElement:formInputBox idKey="customer.enroll.alternate.Number" labelKey="customer.enroll.alternate.Number" path="alternateNumber" inputCSS="text" mandatory="false"/>
											</div>
											</div>
											<div class="form-inline wo" >
												<div class="gender"><label><spring:theme code="customer.enroll.Gender"/> </label></div>
	           									<div class="gender_selection">
	           									<div class="form-group margin-spacing clearfix radioOuter">
												 	<form:radiobutton path="gender" value="1" label="Male"  name="male" cssClass="radio-hide" />
												</div> 
												<div class="form-group clearfix radioOuter">
												 	<form:radiobutton path="gender" value="2" label="Female"  name="female" cssClass="radio-hide" />
												</div>
												</div>
											</div>
											<form:hidden path="hybrisRetailerId" />
											<form:hidden path="customerId" />
											<form:hidden path="memberIDPlasticCard" />
											<div class="join-club-card-inactive">
												<label><spring:theme code="customer.enroll.MARKETING"/> </label>
												<formElement:formCheckbox idKey="customer.enroll.marketing.Email5" labelKey="customer.enroll.marketing.Email" path="marketingEmailNonCC" inputCSS="text" />
												<div class="form-actions clearfix">
													<ycommerce:testId code="enroll_button">
														<button type="submit" class=" btn btn-save-change">
															<spring:theme code='customer.registerNonCC.button' />
														</button>
													</ycommerce:testId>
												</div>								
										</div>
									</div>
								</div>								
							</div>
							
						</div>
					</div>
					<div class="row acc-row-55">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55 form-disabled" id="cR-3">
							<div class="bg-white clearfix">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2>3. Postal address details <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong></h2>
									</div>
								</div>
								<div class="clearfix">
									<div class="pad-spacing form-wrap acc-col-55">
										<%-- <formElement:formInputBox idKey="customer.enroll.AddressTypeId" labelKey="customer.enroll.AddressTypeId" path="addressTypeId" inputCSS="text" mandatory="true"/> --%>
										<%-- <formElement:formInputBox idKey="customer.enroll.Country" labelKey="customer.enroll.Country" path="country" inputCSS="text" mandatory="true"/> --%>
										<formElement:formSelectBox idKey="customer.enroll.Country" labelKey="customer.enroll.Country" path="country"  mandatory="false" items="${country}" />
										<formElement:formInputBox idKey="customer.enroll.AddressLine1" labelKey="customer.enroll.AddressLine1" path="addressLine1" inputCSS="text mandatory" mandatory="true"/>
										<formElement:formInputBox idKey="customer.enroll.AddressLine2" labelKey="customer.enroll.AddressLine2" path="addressLine2" inputCSS="text" />
										<formElement:formInputBox idKey="customer.enroll.Suburb" labelKey="customer.enroll.Suburb" path="suburb" inputCSS="text mandatory" mandatory="true"/>
										<formElement:formInputBox idKey="customer.enroll.City" labelKey="customer.enroll.City" path="city" inputCSS="text" />
										<%-- <formElement:formInputBox idKey="customer.enroll.Province" labelKey="customer.enroll.Province" path="province" inputCSS="text" mandatory="true"/> --%>
										<formElement:formSelectBox idKey="customer.enroll.Province" labelKey="customer.enroll.Province" path="province"  items="${province}" />
										<formElement:formInputBox idKey="customer.enroll.PostalCode" labelKey="customer.enroll.PostalCode" path="postalCode" inputCSS="text mandatory" mandatory="true"/>
										<div class="disclaimerWrapper"><p><spring:theme code="customer.mandatory"/></p></div>
									</div>
								</div>
							</div>							
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55 edit-form-wrap form-disabled" id="cR-4">
							<div class="bg-white clearfix">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<h2><span class="comuni-reg3">3.</span><span class="comuni-reg4">4.</span> Communication from Clicks <strong class="successTick"> <img src="${themeResourcePath}/clicks/image/tick.png" /></strong></h2>
									</div>
								</div>
								<div class="markettingMessage" style="display: none;">We require at least one way in which you would like to receive your cashback and benefit communications.</div>
								<div class="clearfix">
									<div class="pad-spacing form-wrap acc-col-55">
									<%-- 
									<label><spring:theme code="customer.enroll.ACCINFO"/> </label>
								<div class="form-group clearfix checkFix">
									<input type="checkbox" name="marketingEmail" class="check-hide" id="check3"  >
											<label for="check3" class="check-box">
												<spring:theme code="customer.enroll.marketing.Email"/>
											</label>
										</div>	
										<div class="form-group clearfix checkFix">
									<input type="checkbox" name="marketingSMS" class="check-hide" id="check4"  >
											<label for="check4" class="check-box">
												<spring:theme code="customer.enroll.marketing.SMS"/>
											</label>
										</div>	
										<br/>
									<label><spring:theme code="customer.enroll.MARKETING"/> </label>
									<div class="form-group clearfix checkFix">
									<input type="checkbox" name="marketingEmail" class="check-hide" id="check"  >
											<label for="check" class="check-box">
												<spring:theme code="customer.enroll.marketing.Email"/>
											</label>
										</div>	
										<div class="form-group clearfix checkFix">
									<input type="checkbox" name="marketingSMS" class="check-hide" id="check1"  >
											<label for="check1" class="check-box">
												<spring:theme code="customer.enroll.marketing.SMS"/>
											</label>
										</div>	 --%>
								
										<div class="cashback-msg"><label><spring:theme code="customer.enroll.ACCINFO" /> </label>
										<formElement:formCheckbox idKey="customer.enroll.marketing.Email3" labelKey="customer.enroll.marketing.Email" path="accinfoEmail" inputCSS="text email-mar marketing-update" />
										<formElement:formCheckbox idKey="customer.enroll.marketing.SMS3" labelKey="customer.enroll.marketing.SMS" path="accinfoSMS" inputCSS="text sms-mar marketing-update" /></div>
										<label><spring:theme code="customer.enroll.MARKETING"/> </label>
										<formElement:formCheckbox idKey="customer.enroll.marketing.Email4" labelKey="customer.enroll.marketing.Email" path="marketingEmail" inputCSS="text" />
										<formElement:formCheckbox idKey="customer.enroll.marketing.SMS4" labelKey="customer.enroll.marketing.SMS" path="marketingSMS" inputCSS="text" />
										<div class="form-actions clearfix">
											<ycommerce:testId code="enroll_button">
												<button type="submit" class=" btn btn-save-change">
													<spring:theme code='${actionNameKey}' />
												</button>
											</ycommerce:testId>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>

