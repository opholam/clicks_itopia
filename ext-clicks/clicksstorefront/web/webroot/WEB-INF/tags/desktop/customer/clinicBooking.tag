<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<c:if test="${fn:containsIgnoreCase(formMessage, 'success')}">
	<div class="alert positive">
		<spring:theme code='clinic.booking.enquiry.success' text="success"/>
		</div>
</c:if>
<c:if test="${fn:containsIgnoreCase(formMessage, 'failed')}">
	<div class="alert negative">
		<spring:theme code='clinic.booking.enquiry.failed' text="failed"/>
	</div>
</c:if>
	
<div class="book_app_section bg-white">
	<div class="greenbgOuter">
		<div class="green-bg">Book a clinic appointment</div>
	</div>
	<c:url value="/clinicBooking/clinicBookingProcessor" var="clinicBookingAction" />
	<form:form method="post" commandName="clinicBookingForm" action="${clinicBookingAction}">
		<div class="pad-spacing form-wrap">
		
		
	<formElement:formInputBox idKey="customer.enroll.FirstName" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text" mandatory="true" />
	<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text" mandatory="true"/>
	<formElement:formInputBox idKey="customer.enroll.contact.Number" labelKey="customer.enroll.contact.Number" path="contactNumber" inputCSS="text mandatory" mandatory="true"/>
	<div class="errorMandatory">
		<formElement:formSelectBox idKey="clinicProvince" labelKey="clinic.booking.region" path="region" mandatory="false" items="${province}" skipBlankMessageKey="form.select.empty" />
	</div>
	<div class="errorMandatory">
		<formElement:formSelectBox idKey="preferredClinic" labelKey="clinic.booking.preferred.clinic" path="preferredClinic" mandatory="false" items="${preferredClinic}" skipBlankMessageKey="form.select.empty"/>
	</div>							
	<div class="errorMandatory form-group">
		<formElement:formSelectBox idKey="alternativeClinic" labelKey="clinic.booking.alternative.clinic" path="alternativeClinic" mandatory="false" items="${alternativeClinic}" skipBlankMessageKey="form.select.empty"/>
	</div>
			
		
		<div class="clearfix">
			<ycommerce:testId code="clincBooking">
				<button type="submit" class="btn primary_btn btn-block positive"><spring:theme code='clinic.booking.enquiry'/></button>
			</ycommerce:testId>
		</div>
	</div>	
	</form:form>
</div>

