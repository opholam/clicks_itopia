<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<div class="contact-det bg-white clearfix">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
								<h3>Send us a message</h3>
					<div class="row">
	<form:form method="post" commandName="contactUsForm" action="${contextPath}/my-account/report-card/contactUsProcessor">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="form-wrap">
				<formElement:formInputBox idKey="customer.enroll.FirstName" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text"  mandatory="true"/>
				<formElement:formInputBox idKey="customer.enroll.EMailAddress" labelKey="customer.enroll.EMailAddress" path="email" inputCSS="text" mandatory="true"/>
				<formElement:formInputBox idKey="customer.enroll.contact.Number" labelKey="customer.enroll.contact.Number" path="contactNo" inputCSS="text" mandatory="true"/>
				<formElement:formInputBox idKey="customer.club.card.Number" labelKey="customer.club.card.Number" path="clubcardNo" inputCSS="text"/>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-wrap">
		<div class="errorMandatory">
		<formElement:formSelectBox idKey="report.address.title" selectCSSClass="reportLostCard" labelKey="report.address.title" path="topic" mandatory="false" items="${Topic}" skipBlankMessageKey="contact.us.form.select.default.option"/>
		<formElement:formTextArea idKey="customer.club.card.message" labelKey="customer.club.card.message" path="message" areaCSS="textarea"/>
			<div class="form-actions clearfix">
			<ycommerce:testId code="clincBooking">
				<button type="submit" class="btn primary_btn btn-block positive"><spring:theme code='contact.us.enquiry'/></button>
			</ycommerce:testId>
		</div>
		</div>
			</div>
			</div>
					<%-- <formElement:formSelectBox idKey="address.title" labelKey="address.title" path="topic" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${topic}" selectedValue="${contactUsForm.topic}"/> --%>
					
	
	</form:form>
	</div>
</div>

