<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<form action="/joinbabyclub" method="POST" commandName="CustomerData" cssClass="clearfix">

<!-- <form action="#" id="aboutFamily" class="clearfix"> -->
	<div class="form-group clearfix topCats">
		<input type="radio" id="currentPregnant" class="radio-hide" name="isAlreadyPregnant">
		<label class="radio-box current" for="parent"><span>Currently pregnant</span></label>
	</div>
	<div class="topCatsContainer"> 
		<div class="form-group clearfix">
			<label>Estimated date of delivery</label>							
			<input type="text" value=" " class="text" id="datepicker">
			<label for="datepicker" class="btnDate"></label>
		</div>
		<div class="form-group clearfix">
			<label>Is it a boy or a girl?</label>							
			<div class="gender">
				<select id="basic3">
					<option value="Don’t know yet">Don’t know yet</option>
					<option value="Boy">Boy</option>
					<option value="Girl">Girl</option>
				</select>
			</div>
		</div>
	</div>
	<div class="form-group clearfix allCats">
		<input type="radio" id="alreadyParent" class="radio-hide" name="isAlreadyParent">
		<label class="radio-box" for="parent"><span>Already a parent</span></label>
	</div>
	<div class="allCatsContainer">
	<div class="form-group clearfix">
		<label>First name</label>							
		<input type="text" value="" class="text" id="FirstName">
	</div>
	<div class="form-group clearfix">
		<label>Surname</label>							
		<input type="text" value="" class="text" id="LastName">
	</div>
	<div class="form-group clearfix">
		<label>Gender</label>							
		<div class="gender">
			<select id="gender" >
				<option value="Boy">Boy</option>
				<option value="Girl">Girl</option>
			</select>
		</div>
	</div>
	<div class="form-group clearfix">
		<label>Date of Birth</label>							
		<div class="dob">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
				<div class="dobWrapper">
					<select id="day">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
				<div class="monthWrapper">
				<select id="month">
					<option value="Jan">Jan</option>
					<option value="Feb">Feb</option>
				</select>
				</div>
			</div>	
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
				<div class="yearWrapper">
					<select id="year">
						<option value="2010">2010</option>
						<option value="2010">2011</option>
						<option value="2012">2012</option>
						<option value="2013">2013</option>
						<option value="2014">2014</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	</div>
	<button class="btn">Join Babyclub</button>
</form>