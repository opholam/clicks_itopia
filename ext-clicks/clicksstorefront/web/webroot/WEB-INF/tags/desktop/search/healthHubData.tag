<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set value="${fn:length(healthHubData)}" var="articlesInPage"/>
<c:set value="${totalNoOfHealthArticles}" var="totalNoOfArticles"/>
<div id="hub" class="search-out ">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="searchLoadMoreHealthArticles">
				<div class="search-container">
					<c:if test="${totalNoOfHealthArticles <= 0}">
						<spring:theme code="article.no.articles.found" text="No articles found."/>
					</c:if>
					<c:forEach items="${healthHubData}" var="articleData">
						<div class="hub-container healtharticlescount">
							<%-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							<img class="img-responsive" src="${articleData.articleImage}" alt="article">
						</div> --%>
							<h4>
								<c:if test="${articleData.articleType=='MAGAZINES'}">
									<a href="${contextPath}/clubcard-magazine/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</c:if>
								<c:if test="${articleData.articleType=='CONDITIONS'}">
									<a href="${contextPath}/health/conditions/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</c:if>
								<c:if test="${articleData.articleType=='MEDICINES'}">
									<a href="${contextPath}/health/medicines/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</c:if>
								<c:if test="${articleData.articleType=='VITAMINS'}">
									<a href="${contextPath}/health/supplements/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</c:if>
								<c:if test="${articleData.articleType=='HEALTH'}">
									<a	href="${contextPath}/health/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</c:if>
							</h4>
							<div class="articleTagWrapper">
							<c:forEach items="${articleData.articleTags}" var="articleTag">
								<span class="articleTag">${articleTag}</span>
							</c:forEach>
							</div>
							<div class="content-height">
								<p>${articleData.titleContent}</p>
							</div>
						</div>

					</c:forEach>
				</div>
				<c:if test="${totalNoOfArticles > articlesInPage}" >
					<div class="text-center searchLoadMoreHealthArticles">
						<a class="btn btn-load-articles">Load more articles</a>
					</div>
				</c:if>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 healthhub_buttons">
				<cms:pageSlot position="Section4" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
</div>
