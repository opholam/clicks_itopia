<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="top" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowAll" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowPaged" required="true" type="java.lang.Boolean" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="numberPagesShown" required="false" type="java.lang.Integer" %>

<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/desktop/nav/pagination" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<!-- <div class="col-md-4 col-sm-6 col-xs-12 titleWrapper">
		<div class="floatRight">
			<label class="floatLeft" for="Sort by">Sort by</label>
			<div class="sortList floatLeft">
				<div class="selectric-wrapper selectric-basic2">
					<div class="selectric-hide-select">
						<select class="basic2" tabindex="0">
							<option value="Price : low to high">Price : low to high</option>
							<option value="Price : hight to low">Price : hight to
								low</option>
						</select>
					</div>
					<div class="selectric">
						<p class="label">Price : low to high</p>
						<b class="button"></b>
					</div>
					<div class="selectric-items" tabindex="-1" style="width: 165px;">
						<div class="selectric-scroll">
							<ul>
								<li data-index="0" class="selected">Price : low to high</li>
								<li data-index="1" class="last">Price : hight to low</li>
							</ul>
						</div>
					</div>
					<input class="selectric-input" tabindex="0">
				</div>
			</div>
		</div>
	</div>
	 -->

	<c:if test="${not empty searchPageData.sorts}">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 titleWrapper">
		<div class="sortList">  
		<form id="sort_form${top ? '1' : '2'}" name="sort_form${top ? '1' : '2'}" method="get" action="#" class="sortForm">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 title-text">SortBy</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 paddingNone">
			<select id="sortOptions${top ? '1' : '2'}" name="sort" class="sortOptions basic">
				<c:forEach items="${searchPageData.sorts}" var="sort">
					<option value="${sort.code}" ${sort.selected ? 'selected="selected"' : ''}>
						<c:choose>
							<c:when test="${not empty sort.name}">
								${sort.name}
							</c:when>
							<c:otherwise>
								<spring:theme code="${themeMsgKey}.sort.${sort.code}"/>
							</c:otherwise>
						</c:choose>
					</option>
				</c:forEach>
			</select>
			</div>
			<c:catch var="errorException">
				<spring:eval expression="searchPageData.currentQuery.query" var="dummyVar"/><!-- This will throw an exception is it is not supported -->
				<input type="hidden" name="q" id="currentQuery" value="${searchPageData.currentQuery.query.value}"/>
				<input type="hidden" id="productSortCount" name="count" value="${count}"/>
				<input type="hidden"  name="text" value="${searchPageData.freeTextSearch}"/>
			</c:catch>

			<c:if test="${supportShowAll}">
				<ycommerce:testId code="searchResults_showAll_link">
					<input type="hidden" name="show" value="Page"/>
				</ycommerce:testId>
			</c:if>
			<c:if test="${supportShowPaged}">
				<ycommerce:testId code="searchResults_showPage_link">
					<input type="hidden" name="show" value="All"/>
				</ycommerce:testId>
			</c:if>
		</form>
		
		</div>
		</div>
	</c:if>