<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/desktop/nav/pagination" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set value="${fn:length(inspirationData)}" var="articlesInPage"/>
<c:set value="${totalNoOfInspirationArticles}" var="totalNoOfArticles"/>

<div id="inspiration" class="col-md-12 search-out">
		<div class="search-container clearfix">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 bord-right" id="searchLoadMoreInspirationArticles">
				<c:if test="${totalNoOfInspirationArticles <= 0}" >
					No articles found
				</c:if>
				<c:forEach items="${inspirationData}" var="articleData">
					<div class="article-container row insparticlescount">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							<img class="img-responsive" src="${articleData.articleImage}" alt="article">
						</div>
						<div class="col-lg-8 col-md-7 col-sm-8 col-xs-8">
							<c:if test="${articleData.articleType=='MAGAZINES'}">
								<h4>
									<a href="${contextPath}/clubcard-magazine/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</h4>
							</c:if>
							<c:if test="${articleData.articleType=='HEALTH'}">
								<h4>
									<a	href="${contextPath}/health/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</h4>
							</c:if>
							<c:if test="${articleData.articleType=='CONDITIONS'}">
								<h4>
									<a href="${contextPath}/heaht/conditions/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</h4>
							</c:if>
							<c:if test="${articleData.articleType=='MEDICINES'}">
								<h4>
									<a href="${contextPath}/health/medicines/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
								</h4>
							</c:if>	
							<c:if test="${articleData.articleType=='VITAMINS'}">
							<h4>
									<a href="${contextPath}/health/supplements/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
									</h4>
								</c:if>										
							<div class="articleTagWrapper">
							 <c:forEach items="${articleData.articleTags}" var="articleTag">
								<span class="articleTag">${articleTag}</span>
							</c:forEach> 
							</div>
							<div class="content-height">
								<p>${articleData.titleContent}</p>
							</div>
						</div>										
					</div>
				</c:forEach>
				<c:if test="${totalNoOfArticles > articlesInPage}" >
					<div class="text-center searchLoadMoreInspirationArticles">
						<a class="btn btn-load-articles">Load more articles</a>
					</div>
				</c:if>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<h3 class="text-center">
					<a href="#">Save on summer skin care</a>
				</h3>
			
				<cms:pageSlot position="Section2" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				
			</div>
		</div>
</div>