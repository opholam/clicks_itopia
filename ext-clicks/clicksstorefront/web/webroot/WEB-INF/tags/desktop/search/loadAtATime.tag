<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="top" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowAll" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowPaged" required="true" type="java.lang.Boolean" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="numberPagesShown" required="false" type="java.lang.Integer" %>

<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/desktop/nav/pagination" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set value="${not empty count ? count:12 }" var="count"></c:set>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 titleWrapper">
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 paddingNone title-text">Load</div>
	<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 paddingNone">
	<form id="load_form${top ? '1' : '2'}" name="load_form${top ? '1' : '2'}" method="get" action="#" class="load_form">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 paddingNone">
	<div class="loadCount">
		<select id="basic" tabindex="0" name="count">
				<option value="12" selected="selected">12</option>
				<option value="24"${(count eq '24') ? 'selected="selected"' : ''}>24</option>
				<option value="48"${(count eq '48') ? 'selected="selected"' : ''}>48</option>
		</select>
	</div>
	</div>
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 title-text">
	products at a time
	</div>
	</form>
	</div>
</div>

<%-- <c:set value="${not empty count?count:12 }" var="count"></c:set>
<div class="col-md-8 col-sm-6 col-xs-12 titleWrapper">
	<label class="floatLeft" for="load">Load</label>
	<form id="load_form${top ? '1' : '2'}" name="load_form${top ? '1' : '2'}" method="get" action="#" class="load_form">
	<div class="loadCount floatLeft">
		<select id="basic" tabindex="0" name="count" >
		<option value="12">Select</option>
			<option value="12" selected="${count eq '12'}">12</option>
			<option value="5" selected="${count eq 5}">5</option>
			<option value="10" selected="${count eq 10}">10</option>
			<option value="15" selected="${count eq 15}">15</option>
			<option value="20" selected="${count eq 20}">20</option>
		<!-- 	<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option> -->
		</select> count>>${count}
	</div>
	</form>
</div> --%>
