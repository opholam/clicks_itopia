<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- Information (confirmation) messages --%>
<c:if test="${not empty accConfMsgs}">
		<c:forEach items="${accConfMsgs}" var="msg">
			<div class="alert positive">
				<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
			</div>
		</c:forEach>
</c:if>

<!-- logout message to all the pages--> 
<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
<c:if test="${param.logout && empty accConfMsgs}">
		<div class="alert positive">
			<spring:theme code="account.confirmation.signout.title" text="You have signed out of your account." />
		</div>
</c:if>
</sec:authorize>

<%-- Warning messages --%>
<c:if test="${not empty accInfoMsgs}">
		<c:forEach items="${accInfoMsgs}" var="msg">
			<%-- <div class="alert neutral">
				<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
			</div> --%>
		</c:forEach>
</c:if>

<%-- Error messages (includes spring validation messages)--%>
<c:if test="${not empty accErrorMsgs}">
		<c:forEach items="${accErrorMsgs}" var="msg">
			<%-- <div class="alert negative">
				<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
			</div> --%>
			<div class="redCommon">
				<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
			</div>
		</c:forEach>
</c:if>

