<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>



<c:url value="/store-finder" var="storeFinderFormAction" />
<c:url value="/store-finder/position" var="nearMeStorefinderFormAction"/>
<c:url value="/health" var="healthPageUrl"/>
<c:url value="/" var="siteRootUrl"/>



<header>
<%-- Test if the UiExperience is currently overriden and we should show the UiExperience prompt --%>
<c:if test="${uiExperienceOverride and not sessionScope.hideUiExperienceLevelOverridePrompt}">
	<c:url value="/_s/ui-experience?level=" var="clearUiExperienceLevelOverrideUrl"/>
	<c:url value="/_s/ui-experience-level-prompt?hide=true" var="stayOnDesktopStoreUrl"/>
	<div class="backToMobileStore">
		<a href="${clearUiExperienceLevelOverrideUrl}"><span class="greyDot">&lt;</span><spring:theme code="text.swithToMobileStore" /></a>
		<span class="greyDot closeDot"><a href="${stayOnDesktopStoreUrl}">x</a></span>
	</div>
</c:if>
<c:if test="${TLSUpgradeRequired}">
	<div class="promotions_home">
		<a href="#"><spring:theme code="text.upgrade.browser.tls" text="Your browser will no longer be able to display this website due to outdated encryption of ${TLSVersion}. Please upgrade your browser as soon as possible."/></a>
	</div>
</c:if>
<c:if test="${cmsPage.uid eq 'homepage'}">
	<cms:pageSlot position="HeaderContent" var="feature">
		<div class="promotions_home">
			<cms:component component="${feature}" />
		</div>
	</cms:pageSlot>
</c:if>
	<div class="header hidden-xs">
		<div class="top-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<ul class="nav nav-tabs">
							<li class="active"><a href="${siteRootUrl}"><spring:theme code="site.shop.name" text="Clicks Shop"/></a></li>
							<li><a href="${healthPageUrl}"><spring:theme code="site.health.name" text="Health Hub"/></a></li>
						</ul>
					</div>
					<div class="col-md-7 col-sm-7">
							<ul class="quick-links pull-right">
								
								
				<!--	<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
					<c:set var="maxNumberChars" value="25"/>
					<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
						<c:set target="${user}" property="firstName" value="${fn:substring(user.firstName, 0, maxNumberChars)}..."/>
					</c:if>
					<li class="logged_in"><ycommerce:testId code="header_LoggedUser"><spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" htmlEscape="true"/></ycommerce:testId></li>
				</sec:authorize>
				-->
				<li class="dropdown signin-block">
					<c:if test="${empty loginPage or empty LoginLink }">
					<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
						<ycommerce:testId code="header_Login_link">
						<a href="javascript:void(0)" class="dropdown-toggle sign-in" data-href="/login">Sign in / Register</a></ycommerce:testId>
						<div class="dropdown-list login uppermenubox"> </div>
						
						<script>
							ga('set', 'dimension1', 'loggedout');
						</script>
					</sec:authorize>
					</c:if>
					<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
						<a href="#" class="dropdown-toggle sign-in signedInUser">
						<c:set var="firstName" value="${fn:toUpperCase(fn:substring(user.firstName, 0, 1))}${fn:toLowerCase(fn:substring(user.firstName, 1, -1))} " />
						<c:set var="lastName" value="${fn:toUpperCase(fn:substring(user.lastName, 0, 1))}${fn:toLowerCase(fn:substring(user.lastName, 1, -1))} " />
						<ycommerce:testId code="header_LoggedUser"><spring:theme code="header.welcome" arguments="${firstName},${lastName}" htmlEscape="true"/></ycommerce:testId>
						</a>
						<div class="dropdown-list acc-links-block uppermenubox">
								<ul class="acc-drop-down">
									<%-- As per client updates. commenting for pharse one--%>
									<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account"/>"><spring:theme code="header.link.account.dropdown"/></a></ycommerce:testId></li>
									<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account/orders"/>"><spring:theme code="header.link.account.order.history"/></a></ycommerce:testId></li>
									<c:if test="${not empty user.memberID}">
										<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account/my-rewards-activity"/>"><spring:theme code="header.link.clubcard.reward.activity"/></a></ycommerce:testId><br/></li>
									</c:if> 
									 
									<li class="dark-sep separator"></li>	
<%-- 									<c:if test="${empty hideHeaderLinks}"> --%>
										<li><ycommerce:testId code="header_signOut"><a href="<c:url value='/logout'/>"><spring:theme code="header.link.logout"/></a></ycommerce:testId></li>
<%-- 									</c:if> --%>
								</ul>
						</div>
						
						<%-- <c:set var="gaUid" value="${fn:split(user.uid,'@')}"/> --%>
						<script>
							ga('set', 'dimension1', 'loggedin' );
							<c:choose>
								<c:when test="${not empty user.memberID}">
									ga('set', 'dimension2', '${user.memberID}');
									ga('set', '&uid', '${user.memberID}');
								</c:when>
								<c:otherwise>
									ga('set', 'dimension2', 'H${user.customerID}');
									ga('set', '&uid', 'H${user.customerID}');
								</c:otherwise>
							</c:choose>	
						</script>
						
					</sec:authorize>
				</li>
						
				
				
				<%-- <sec:authorize ifNotGranted="ROLE_ANONYMOUS">
				<li class="dropdown">
				<a href="#" class="dropdown-toggle sign-in">
					<ycommerce:testId code="header_LoggedUser"><spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" htmlEscape="true"/></ycommerce:testId>
				</a>
				<div class="dropdown-list acc-links-block uppermenubox">
							<ul class="acc-drop-down">
								<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account"/>"><spring:theme code="header.link.account"/></a></ycommerce:testId></li>
								<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account/orders"/>"><spring:theme code="header.link.account.order.history"/></a></ycommerce:testId></li>
								<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account/my-rewards-activity"/>"><spring:theme code="header.link.clubcard.reward.activity"/></a></ycommerce:testId><br/></li>
								<li class="dark-sep separator"></li>	
								<c:if test="${empty hideHeaderLinks}">
									<li><ycommerce:testId code="header_signOut"><a href="<c:url value='/logout'/>"><spring:theme code="header.link.logout"/></a></ycommerce:testId></li>
								</c:if>
							</ul>
				</div>
				</li>	
				</sec:authorize> --%>
				
				
					<li class="dropdown">
								<a href="javascript:void(0)" class="dropdown-toggle store">Find a store</a>
								<div class="dropdown-list find-store uppermenubox">
									<h3>Find a local store, pharmacy or clinic</h3>
									<form:form id="nearMeStorefinderForm" name="near_me_storefinder_form" method="POST" action="${nearMeStorefinderFormAction}" commandName="storePositionForm">
										<input type="hidden" id="latitude" name="latitude"/>
										<input type="hidden" id="longitude" name="longitude"/>
										<button  id="findStoresNearMe" class="btn icon_target" type="submit">Find my nearest Clicks</button>
									</form:form>

									<form:form action="${storeFinderFormAction}" method="post" commandName="storeFinderForm">	
									<ycommerce:testId code="storeFinder_search_box">						
										<input type="text" class="text" placeholder="Store name, suburb or postcode" name="q" id="storelocator-query"  mandatory="true">
										<input type="submit" class="btnSearch" value="q">
									</ycommerce:testId>
									</form:form>
								</div>
							</li>
							<li class="dropdown">
									<cms:pageSlot position="MiniCart" var="cart" limit="1">
											<cms:component component="${cart}"  />
										</cms:pageSlot>
							</li>
							<c:if test="">
							 <li class="dropdown">
								<a href="#" class="dropdown-toggle order-presc">Order prescription</a>
							</li>							
							<%-- <li class="dropdown">
								<a href="#" id="item2" class="dropdown-toggle basket">Basket</a>
								<div class="dropdown-list basket-list uppermenubox">
											<section class="basket-checkout">
										<cms:pageSlot position="MiniCart" var="cart" limit="1">
											<cms:component component="${cart}" element="li" class="miniCart" />
										</cms:pageSlot>
									</section>
									
								</div>
							</li>  --%>
							</c:if>
						</ul>
					</div> 
				</div>
			</div>
		</div>
		<div class="logo-block">
			<div class="container">
				<div class="row">
					<div class="col-md-6 site-logo col-sm-6">
						<a href="${siteRootUrl}">
							<%-- <img src="${themeResourcePath}/clicks/image/logo.png" alt="logo"/> --%>
							<cms:pageSlot position="SiteLogo" var="feature">
									<cms:component   component="${feature}" />
							</cms:pageSlot>							
						</a>
					</div>
					<div class="col-md-6 col-sm-6">					
						<cms:pageSlot position="SearchBox" var="component" element="div" class="global-search pull-right">
							<cms:component component="${component}"/>
						</cms:pageSlot>
					</div>								
				</div>
			</div>
		</div>	
	</div>
<div class="mobile-header visible-xs visible-sm">	
	<div class="mb-logo">
		<%-- <a href="${contextPath}"><img src="${themeResourcePath}/clicks/image/mb_logo.png" alt="logo" data-elem="pinchzoomer"/></a> --%>
		<a href="${siteRootUrl}">
			<cms:pageSlot position="SiteLogo" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>	
		</a>
	</div>
	<div class="mobile-nav-bar paddingNone">
		<nav>
				<ul>
					<li>
						<a href="#" class="icon-menu" data-tab=".main-nav"><span>Menu icon</span></a>
					</li>
					<li class="search">
						<a href="#" class="icon-search" data-tab=".search-blk"><span>Search</span></a>
					</li>
					<li class="dropdown">
					<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
						<a href="#" class="icon-sign-in" data-tab=".login-blk"><span>Sign in</span></a>
						</sec:authorize>
						<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
						<a href="#" class="icon-sign-in signedInUser" data-tab=".login-blk"><span>Sign in</span></a>
						</sec:authorize>						
					</li>
					<li class="dropdown">
						<a href="#" class="icon-locator" data-tab=".store-blk"><span>Store locator</span></a>						
					</li>
					<!-- <li class="dropdown">
						<a href="#" class="icon-desc" data-tab=".prescription-blk"><span>Order prescription</span></a>
						<ul class="dropdown-menu"></ul>
					</li> -->
					<li class="dropdown">					
						<a href="javascript:void(0);" class="icon-cart basket" data-tab=".basket-blk"><span>Basket</span></a>						
					</li>
				</ul>
			</nav>
	   </div>
		<div class="nav-collapse mobile-tabs">
			<ul class="nav nav-tabs">
				<li class="active"><a href="${siteRootUrl}"><spring:theme code="site.shop.name" text="Clicks Shop"/></a></li>
				<li><a href="${healthPageUrl}"><spring:theme code="site.health.name" text="Health Hub"/></a></li>
			</ul>
		</div>
		<div class="search-blk mobile-tabs"></div>
		<div class="login-blk mobile-tabs"></div>
		<div class="store-blk mobile-tabs"></div>
		<div class="prescription-blk mobile-tabs"></div>
		<div class="basket-blk mobile-tabs dropdown-list"><div class="basket-list"><div id="mobminiCartLayer"></div></div></div>
	</div>
</header>


<%-- <div id="header" class="clearfix">
	<cms:pageSlot position="TopHeaderSlot" var="component">
		<cms:component component="${component}"/>
	</cms:pageSlot>
	
	<div class="headerContent">
		<ul class="nav clearfix">
			<c:if test="${empty hideHeaderLinks}">
				<c:if test="${uiExperienceOverride}">
					<li class="backToMobileLink">
						<c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl"/>
						<a href="${backToMobileStoreUrl}"><spring:theme code="text.backToMobileStore"/></a>
					</li>
				</c:if>

				<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
					<c:set var="maxNumberChars" value="25"/>
					<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
						<c:set target="${user}" property="firstName" value="${fn:substring(user.firstName, 0, maxNumberChars)}..."/>
					</c:if>
					<li class="logged_in"><ycommerce:testId code="header_LoggedUser"><spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" htmlEscape="true"/></ycommerce:testId></li>
				</sec:authorize>
				<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
					<li><ycommerce:testId code="header_Login_link"><a href="<c:url value="/login"/>"><spring:theme code="header.link.login"/></a></ycommerce:testId></li>
				</sec:authorize>
				<li><ycommerce:testId code="header_myAccount"><a href="<c:url value="/my-account"/>"><spring:theme code="header.link.account"/></a></ycommerce:testId></li>
			</c:if>

			<cms:pageSlot position="HeaderLinks" var="link">
				<cms:component component="${link}" element="li"/>
			</cms:pageSlot>
		
			<c:if test="${empty hideHeaderLinks}">
				<li><a href="<c:url value="/store-finder"/>"><spring:theme code="general.find.a.store" /></a></li>
				<sec:authorize ifNotGranted="ROLE_ANONYMOUS"><li><ycommerce:testId code="header_signOut"><a href="<c:url value='/logout'/>"><spring:theme code="header.link.logout"/></a></ycommerce:testId></li></sec:authorize>
			</c:if>

			<cms:pageSlot position="MiniCart" var="cart" limit="1">
				<cms:component component="${cart}" element="li" class="miniCart" />
			</cms:pageSlot>
		</ul>
	</div>

	<cms:pageSlot position="SearchBox" var="component" element="div" class="headerContent secondRow">
		<cms:component component="${component}" element="div" />
	</cms:pageSlot>


	<cms:pageSlot position="SiteLogo" var="logo" limit="1">
		<cms:component component="${logo}" class="siteLogo"  element="div"/>
	</cms:pageSlot>
</div> --%>
