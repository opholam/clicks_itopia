 <%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<footer role="footer">
	<div class="container">
		<div class="subscribe-block">
			<figure class="shareBlock">
				<cms:pageSlot position="FooterA" var="feature" class="footer">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</figure>
		</div>
	</div>
	<section class="footer-links">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<cms:pageSlot position="FooterB" var="feature" element="div" class="footer">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12 paddingNone">
				<cms:pageSlot position="Footer" var="feature" element="div" class="">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
	<div class="secure_payment_by">
		<cms:pageSlot position="FooterC" var="feature" class="footer">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	
	</div>
	</div>
	<div class="copyright">
		<div class="container">
			<div class="pull-left copyrightText hidden-xs">
			<p><spring:theme code="clicks.copyright" /></p>
				
			</div>
			<div class="pull-right foot-otherlinks">
				<cms:pageSlot position="FooterD" var="feature" class="footer">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				<span class="visible-xs">
					<p><spring:theme code="clicks.copyright" /></p>
				</span>
			</div>
		</div>
	</div>
	<div id="menu-overlay-footer"></div>
</footer>

<script>
	ga('send', 'pageview');
</script>