<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="order" required="true"
	type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ attribute name="orderGroup" required="true"
	type="de.hybris.platform.commercefacades.order.data.OrderEntryGroupData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="showAddToCart" type="java.lang.Boolean" %>

<c:forEach items="${order.entries}" var="entry">
<li>
		<c:url value="${entry.product.url}" var="productUrl" />
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 paddingNone">
		<c:if test="${empty showAddToCart || !showAddToCart}">
			<div class="ord-summary-img">
				<a href="${productUrl}"> <product:productPrimaryImage
						product="${entry.product}" format="cartIcon" />
				</a>
			</div>
		</c:if>
			<div class="ord-summary-content">
				<h4>${entry.product.brand}</h4>
				<c:if test="${not empty entry.product.name}">
					<p><a  href="${contextPath}${entry.product.url}">${entry.product.name}</a></p>
				</c:if>
				
				 <c:set var="price" value="${entry.product.price.formattedValue}" />
 				<c:if test="${not empty entry.product.price.grossPriceWithPromotionApplied and entry.product.price.grossPriceWithPromotionApplied gt 0}">
 						<fmt:formatNumber minFractionDigits="2" var="promoPrice" pattern="####.##" value="${entry.product.price.grossPriceWithPromotionApplied}" />
 					<c:set var="price" value="R${promoPrice}" />
 		</c:if>
		<c:set var="dec" value="${fn:split(price, '.')}" />
		<c:set var="dec1" value="${dec[0]}" />
		<c:set var="dec2" value="${dec[1]}" />

			<c:if test="${empty entry.product.potentialPromotions[0] || entry.product.price.value - entry.product.price.grossPriceWithPromotionApplied <= 0}">
				<c:set var="blue" value="blue" />
			</c:if>


			<small class="price blue">${entry.quantity} x </small>
			<c:if test="${not empty entry.product.price.grossPriceWithPromotionApplied && entry.product.price.value - entry.product.price.grossPriceWithPromotionApplied > 0 && entry.product.price.grossPriceWithPromotionApplied ne '0.00'}">
			<small class="price ${blue}">

			 ${dec1}<sup>${dec2}</sup>
			 <c:set var="wasValue" value="Was&nbsp;" />
			</small>
			</c:if>
			<small class="price blue">
			<c:if test="${not empty entry.product.price.grossPriceWithPromotionApplied &&  entry.product.price.value - entry.product.price.grossPriceWithPromotionApplied > 0 && entry.product.price.grossPriceWithPromotionApplied ne '0.00'}">
	 		${wasValue}
			</c:if>
			<dateFormat:dateFormat conversionType="rand" input="${entry.product.price.formattedValue}"/></small>
			</div>
			<c:if test="${not empty showAddToCart && showAddToCart}">
			<div class="product-qty"><product:productAddToCartPanel product="${entry.product}" allowAddToCart="${empty showAddToCart ? true : showAddToCart}" isMain="true" /></div>
			</c:if>
		</div>
		
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right">
					<c:set var="totaldec" value="${fn:split(entry.totalPrice.formattedValue, '.')}" />
					<c:set var="totaldec1" value="${totaldec[0]}" />
					<c:set var="totaldec2" value="${totaldec[1]}" />
					<small class="price ${blue}">${totaldec1}<sup>${totaldec2}</sup></small>
			</div>
			</li>
			<%-- 'price': "${entry.product.price.formattedValue}", --%>
		<c:if test="${gaOderConfirmation}">		
		<script>
			ga('ec:addProduct', {
				'id': "${entry.product.code} ddd",
				'name': "${entry.product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${entry.product.name}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''),
				'brand': "${entry.product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
				'price': "${fn:replace(entry.product.price.formattedValue,',','')}",
				'quantity': "${entry.quantity}",
				});
		</script>
		</c:if>
	</c:forEach>


<%-- <div class="orderList">
	<div class="headline">
		<spring:theme code="basket.page.title.yourDeliveryItems"
			text="Your Delivery Items" />
	</div>



	<table class="orderListTable">
		<thead>
			<tr>
				<th id="header2" colspan="2"><spring:theme
						code="text.productDetails" text="Product Details" /></th>
				<th id="header4"><spring:theme code="text.quantity"
						text="Quantity" /></th>
				<th id="header5"><spring:theme code="text.itemPrice"
						text="Item Price" /></th>
				<th id="header6"><spring:theme code="text.total" text="Total" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${orderGroup.entries}" var="entry">
				<c:url value="${entry.product.url}" var="productUrl" />
				<tr class="item">
					<td headers="header2" class="thumb"><a href="${productUrl}">
							<product:productPrimaryImage product="${entry.product}"
								format="thumbnail" />
					</a></td>
					<td headers="header2" class="details"><ycommerce:testId
							code="orderDetails_productName_link">
							<div class="itemName">
								<a href="${entry.product.purchasable ? productUrl : ''}">${entry.product.name}</a>
							</div>
						</ycommerce:testId> <c:forEach items="${entry.product.baseOptions}" var="option">
							<c:if
								test="${not empty option.selected and option.selected.url eq entry.product.url}">
								<c:forEach items="${option.selected.variantOptionQualifiers}"
									var="selectedOption">
									<dl>
										<dt>${selectedOption.name}:</dt>
										<dd>${selectedOption.value}</dd>
									</dl>
								</c:forEach>
							</c:if>
						</c:forEach> <c:if test="${not empty order.appliedProductPromotions}">
							<ul>
								<c:forEach items="${order.appliedProductPromotions}"
									var="promotion">
									<c:set var="displayed" value="false" />
									<c:forEach items="${promotion.consumedEntries}"
										var="consumedEntry">
										<c:if
											test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
											<c:set var="displayed" value="true" />
											<li><span>${promotion.description}</span></li>
										</c:if>
									</c:forEach>
								</c:forEach>
							</ul>
						</c:if></td>
					<td headers="header4" class="quantity"><ycommerce:testId
							code="orderDetails_productQuantity_label">${entry.quantity}</ycommerce:testId>
					</td>
					<td headers="header5"><ycommerce:testId
							code="orderDetails_productItemPrice_label">
							<format:price priceData="${entry.basePrice}"
								displayFreeForZero="true" />
						</ycommerce:testId></td>
					<td headers="header6" class="total"><ycommerce:testId
							code="orderDetails_productTotalPrice_label">
							<format:price priceData="${entry.totalPrice}"
								displayFreeForZero="true" />
						</ycommerce:testId></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</div>
 --%>