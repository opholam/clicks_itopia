<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="hasShippedItems" value="${order.deliveryItemsQuantity > 0}" />
	<c:if test="${not hasShippedItems}">
		<spring:theme code="checkout.pickup.no.delivery.required"/>
	</c:if>
	<c:if test="${hasShippedItems}">
								<div class="del-method">
								<h2><spring:theme code="text.deliveryAddress" text="Delivery Address"/></h2>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.firstName)}&nbsp;${fn:escapeXml(order.deliveryAddress.lastName)}</div>
								<div class="confirm-content">Clicks - ${fn:escapeXml(deliveryAddress.companyName)}</div>
								<div class="confirm-content">
								<c:if test="${not empty order.deliveryAddress.department}">
								${fn:escapeXml(order.deliveryAddress.department)}&nbsp;
								</c:if>
								<c:if test="${not empty order.deliveryAddress.apartment}">
								 ${fn:escapeXml(order.deliveryAddress.apartment)}
								</c:if>
								</div>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.line1)}</div>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.line2)}</div>
								<c:choose>
								<c:when test="${order.deliveryAddress.town eq order.deliveryAddress.suburb}">
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.town)}</div>
								</c:when>
								<c:otherwise>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.town)}</div>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.suburb)}</div>
								</c:otherwise>
								</c:choose>
								<div class="confirm-content">
								<c:forEach items="${province}" var="provinceVal">
								<c:if test="${provinceVal.code eq order.deliveryAddress.province}">
								${provinceVal.name}
								</c:if>
								</c:forEach>
								</div>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.postalCode)}</div>
								<c:if test="${deliveryAddress.isStoreAddress ne true}">
								<div class="confirm-content">${order.deliveryAddress.email}</div>
								</c:if>
								<div class="confirm-content">${fn:escapeXml(order.deliveryAddress.phone)}</div>
							</div>	
<%-- 		<ul>
			<li>${fn:escapeXml(order.deliveryAddress.title)}&nbsp;${fn:escapeXml(order.deliveryAddress.firstName)}&nbsp;${fn:escapeXml(order.deliveryAddress.lastName)}</li>
			<li>${fn:escapeXml(order.deliveryAddress.line1)}</li>
			<li>${fn:escapeXml(order.deliveryAddress.line2)}</li>
			<li>${fn:escapeXml(order.deliveryAddress.town)}</li>
			<li>${fn:escapeXml(order.deliveryAddress.region.name)}</li>
			<li>${fn:escapeXml(order.deliveryAddress.postalCode)}</li>
			<li>${fn:escapeXml(order.deliveryAddress.country.name)}</li>
		</ul> --%>
	</c:if>
