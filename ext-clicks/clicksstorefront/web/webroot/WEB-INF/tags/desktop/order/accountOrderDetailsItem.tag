<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="consignment" required="true" type="de.hybris.platform.commercefacades.order.data.ConsignmentData" %>
<%@ attribute name="inProgress" required="false" type="java.lang.Boolean" %>
<%@ attribute name="count" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:set var="subTotalForParcel" value="0.00" />
							<div class="inidiviual-order-wrap shopping-list">
								<div class="medicine-coll-head clearfix bg-white">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<h4>
											<span class="consignment_status">Parcel ${count} - <spring:theme code="text.account.order.consignment.status.${consignment.statusDisplay}"/></span> <span class="consignment_trackingID">#${consignment.trackingID}</span>
										</h4>
									</div>
								</div>
								<!-- Title-->
								<div class="order-product-head clearfix">
									<div
										class="col-lg-4 col-lg-offset-6 col-sm-offset-7 col-md-3 col-sm-3 hidden-xs text-center">
										<h4>Re-order</h4>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 hidden-xs paddingNone price-right">
										<h4>Price</h4>
									</div>
								</div>
								<!-- Title-->
								<!-- Shopping list row -->
								<c:forEach items="${consignment.entries}" var="entry">
							<c:url value="${entry.orderEntry.product.url}" var="productUrl"/>
								<div class="border  shop-wrap clearfix">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9 shop-list paddingNone">
										<div class="prod-info">
											<h4>${entry.orderEntry.product.brand}</h4>
												<c:if test="${not empty entry.orderEntry.product.name}">
											  <p class="prd-desc"><a  href="${contextPath}${entry.orderEntry.product.url}">${entry.orderEntry.product.name}</a></p>
												</c:if>			
				 <c:set var="price" value="${entry.orderEntry.product.price.formattedValue}" />
 				<c:if test="${not empty entry.orderEntry.product.price.grossPriceWithPromotionApplied and entry.orderEntry.product.price.grossPriceWithPromotionApplied gt 0}">
 						<fmt:formatNumber minFractionDigits="2" var="promoPrice" pattern="####.##" value="${entry.orderEntry.product.price.grossPriceWithPromotionApplied}" />
 					<c:set var="price" value="R${promoPrice}" />
 		</c:if>
		<c:set var="dec" value="${fn:split(price, '.')}" />
		<c:set var="dec1" value="${dec[0]}" />
		<c:set var="dec2" value="${dec[1]}" />

			<c:if test="${empty entry.orderEntry.product.potentialPromotions[0]}">
				<c:set var="blue" value="blue" />
			</c:if>


			<small class="price blue">${entry.quantity} x </small>
			<c:if test="${not empty entry.orderEntry.product.price.grossPriceWithPromotionApplied and entry.orderEntry.product.price.grossPriceWithPromotionApplied gt 0}">
			<small class="price ${blue}">
			 ${dec1}<sup>${dec2}</sup>
			 <c:set var="wasValue" value="Was&nbsp;" />
			</small>
			</c:if>
			<small class="price blue">
			<c:if test="${not empty entry.orderEntry.product.price.grossPriceWithPromotionApplied and entry.orderEntry.product.price.grossPriceWithPromotionApplied gt 0}">
	 		${wasValue}
			</c:if>
			<dateFormat:dateFormat conversionType="rand" input="${entry.orderEntry.product.price.formattedValue}"/>
			</small>
							</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 text-center hidden-xs">
					<c:if test="${not empty consignment.status && (fn:containsIgnoreCase(consignment.status, 'SHIPPED') or fn:containsIgnoreCase(consignment.status, 'DELIVERED'))}">				
					<product:productAddToCartPanel product="${entry.orderEntry.product}" allowAddToCart="${empty showAddToCart ? true : showAddToCart}" isMain="true" />					
									</c:if>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 shop-list paddingNone price-right">
					<c:set var="totaldec" value="${fn:split(entry.priceForQuantity.formattedValue, '.')}" />
					<c:set var="totaldec1" value="${totaldec[0]}" />
					<c:set var="totaldec2" value="${totaldec[1]}" />
					<div class="price-wrap"><div class="price ${blue}">${totaldec1}<sup>${totaldec2}</sup></div></div>
									</div>
									<div class="col-xs-12 text-center hidden-lg hidden-md hidden-sm paddingNone">
										<button class="btn btn-small" data-toggle="modal" data-target="#addTobasket">Add to basket</button>
									</div>
								</div>
					<c:set var="subTotalForParcel" value="${subTotalForParcel + entry.priceForQuantity.value}" />
								</c:forEach>
								<!-- Shopping list row  end -->

								<!-- Shopping list row  end -->
								<!-- Shopping list row -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
										<div class="subTotal">
											<div class="shop-wrap indiviSubTotal">
											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 price-right paddingNone">
												<h4>Subtotal</h4>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 price-right paddingNone">
												<div class="price blue">
											<c:set var="subTotalForParcel" value="${fn:split(subTotalForParcel, '.')}"  />	
											R${subTotalForParcel[0]}<sup>${subTotalForParcel[1]}</sup>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>