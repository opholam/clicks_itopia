<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderData" required="false" type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ attribute name="count" required="false" type="java.lang.String" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>

<script id="orderListTemplate" type='text/x-jsrender'>
<ul class="store-info">
<c:if test="${searchPageData ne null and !empty searchPageData.results}">
		<c:forEach items="${searchPageData.results}" var="pos" varStatus="loopStatus" >
		{{if count >= ${loopStatus.count} }}


			<c:url value="${pos.url}" var="posUrl"/>
			
			<li>
			<section class="pull-left">		
				<c:url value="${pos.url}" var="storeUrl" scope="request"/>
				<h3>
							<ycommerce:testId code="storeFinder_result_link">
								<a href="${posUrl}"> ${loopStatus.count}.&nbsp;  ${pos.displayName} &nbsp;&nbsp;
								<span>${pos.formattedDistance} away </span>
								</a>
							</ycommerce:testId>
							
				</h3>	
				
				<P>
					<ycommerce:testId code="storeFinder_result_address_label">
						<c:if test="${not empty pos.address}">
								${pos.address.line1}, &nbsp;
								${pos.address.line2},&nbsp;
								${pos.address.town},&nbsp;
								${pos.address.postalCode}.
						</c:if>
					</ycommerce:testId>
				</P>
			</section>
			
			<div class="pull-right">

						<c:if test="${not empty pos.features}">
							
									<c:forEach items="${pos.features}" var="feature">
									<c:set var="value" value="${feature.value}"/>
									<c:if test="${fn:containsIgnoreCase(value, 'ph')}">
										<p class="blue">${feature.value}</p>
									</c:if>
									</c:forEach>
									<c:forEach items="${pos.features}" var="feature">
									<c:set var="value" value="${feature.value}"/>
									<c:if test="${fn:containsIgnoreCase(value, 'cl')}">
										<p class="green">${feature.value}</p>
									</c:if>
									</c:forEach>
									
								
						</c:if>
				</div>
				</li>
				{{/if}}
				</c:forEach>
		

</c:if>
</ul>

{{if count <= ${fn:length(searchPageData.results)} }}
<a class="btn btn-load-more" href="#" onclick="loadStores({{:count}})">Load more</a>
{{/if}}
</script>