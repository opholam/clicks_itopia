<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="orderData" required="false"
	type="de.hybris.platform.commercefacades.order.data.OrderHistoryData"%>

<script id="hideLoadMore" type='text/x-jsrender'></script>

<script id="showLoadMore" type='text/x-jsrender'>
 <a class="btn btn-load-articles" onclick="loadOrders()">Load more</a>
</script>


<script id="orderListTemplate" type="text/x-jsrender">
<div id="orderList">
<div class="recent-head recent-custom">
<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
{{if clicksOrderCode}}
#{{:clicksOrderCode}}
{{else}}
#{{:code}}
{{/if}}
</div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">{{:formatDate}}</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
				{{:deliveryAddress.line1}},
				{{:deliveryAddress.postalCode}},
				{{:deliveryAddress.town}}
		</div>

	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">${orderData.statusDisplay}</div>

	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
{{if clicksOrderCode}}
<a href="${contextPath}/my-account/order/{{:clicksOrderCode}}">View order details</a>
{{else}}
<a href="${contextPath}/my-account/order/{{:code}}">View order details</a>
{{/if}}
	</div>

</div>
</div>

</script>
<div id="orderList">
<c:choose>
<c:when test="${not empty orderData.clicksOrderCode}">
<c:set var="orderCode" scope="page" value="${orderData.clicksOrderCode}"/>
</c:when>
<c:otherwise>
<c:set var="orderCode" scope="page" value="${orderData.code}"/>
</c:otherwise>
</c:choose>
	<div class="recent-head recent-custom">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">#${orderCode}</div>
		
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
			<fmt:formatDate pattern="dd MMM yyyy" value="${orderData.placed}" />
		</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
				<c:if test="${not empty orderData.deliveryAddress.line1}">${orderData.deliveryAddress.line1}, </c:if>
				<c:if test="${not empty orderData.deliveryAddress.postalCode}">${orderData.deliveryAddress.postalCode}, </c:if>
				<c:if test="${not empty orderData.deliveryAddress.town}">${orderData.deliveryAddress.town}</c:if>
		</div>

		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">${orderData.statusDisplay}</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
			<a href="${contextPath}/my-account/order/${orderCode}">View order details</a>
		</div>

<!-- 		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
			<a href="#">Buy again all</a>
		</div> -->
	</div>
</div>
