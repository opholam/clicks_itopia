<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="storepickup"
	tagdir="/WEB-INF/tags/desktop/storepickup"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>

<spring:theme code="text.addToCart" var="addToCartText" />
<c:url value="${product.url}" var="productUrl" />

<spring:url value="/cart/add" var="addtoCartUrl" />

<div class="col-md-4 col-sm-6 col-xs-12 plpsep">
	<div class="productBlock">
		<ycommerce:testId code="test_searchPage_wholeProduct">
	
			<a href="${productUrl}" title="${product.name}"> <product:productPrimaryImage
					product="${product}" format="productListing" />
			</a>

			<div class="badges">
				<c:if test="${not empty product.potentialPromotions and not empty product.potentialPromotions[0].stickerMediaUrls}">
				<c:set var="promotionStickerUrls" value="${product.potentialPromotions[0].stickerMediaUrls}" scope="request"/>
							<c:forEach items="${promotionStickerUrls}"  var="stickerURL">
								<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
							</c:forEach>
				</c:if>
				<c:if test="${not empty product.nonPromoStickerUrls}">
					<c:forEach items="${product.nonPromoStickerUrls}" var="stickerURL">
								<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
							</c:forEach>
				</c:if>
			</div>			

			<div class="detailContent clearfix">
				<a href="${productUrl}" title="${product.name}"> <ycommerce:testId
						code="searchPage_productName_link_${product.code}">
						<h5 title='${product.brand}'>${product.brand}</h5>
						<div class="product-name">
							<c:set var="title" value="${product.sizeVariantTitle ne null ? product.sizeVariantTitle : product.styleVariantTitle ne null ? product.styleVariantTitle: product.baseProductTitle ne null? product.baseProductTitle: product.name }" />
							<p title='${title}'>${title}</p>
						</div>
					</ycommerce:testId>
				</a>

				<price:fromPrice product="${product}" />

				<%-- <div class="offer-blk">
					
					<c:if test="${(not empty product.price.grossPriceWithPromotionApplied)}">
						<fmt:formatNumber var="save" pattern="####.##" value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
						<c:if test="${not empty save and (product.price.value - product.price.grossPriceWithPromotionApplied) gt 0}">
						Was&nbsp;<dateFormat:dateFormat conversionType="rand" input="${product.price.formattedValue}"/> 								
						<span class="red">Save</span>
						<span class="price red"> 
							<fmt:formatNumber var="save" pattern="####.##"
									value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
							R<dateFormat:dateFormat conversionType="rand" input="${save}"/>
						</span>
						</c:if>
					</c:if>
				</div> --%>

				<div class="starWrapper">
					<product:productStars rating="${product.averageRating}" />
						<c:set var="product" value="${product}" scope="request" />
				<c:set var="addToCartText" value="${addToCartText}" scope="request" />
				<c:set var="addToCartUrl" value="${addToCartUrl}" scope="request" />
				<div id="actions-container-for-${component.uid}"
					class="listAddPickupContainer clearfix">
					<action:actions element="div" parentComponent="${component}" />
				</div>
				</div>
				
				<c:if test="${not empty product.potentialPromotions}">
					<product:productPromotionSection product="${product}" />
				</c:if>

			

			</div>

		</ycommerce:testId>
	</div>
</div>
