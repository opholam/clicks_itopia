<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ attribute name="productPromotionList" required="true"
	type="java.util.List"%>


<c:forEach items="${productPromotionList}" var="product">

	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 plpsep">

		<div class="productBlock">



			<c:if test="${not empty product.potentialPromotions[0].productBanner}">
				<img src="${product.potentialPromotions[0].productBanner.url}" class="mix-match-badge"/>
			</c:if>


			<div class="thumb">
				<product:productPrimaryImage product="${product}" format="product"/>
			</div>


			<div class="detailContent clearfix">
				<h5>${product.name}</h5>
				<div class="content-height">
					<p>${product.summary}</p>
				</div>
				<price:fromPrice product="${product}" />
				<product:productStars rating="${product.averageRating}" />
				
				<button class="btn">Buy</button>
						<product:productPromotionSection product="${product}" />

			</div>
		</div>
	</div>
</c:forEach>

