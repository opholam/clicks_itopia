<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="galleryImages" required="true" type="java.util.List"%>
<%@ attribute name="brandName" required="true" type="java.lang.String"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:theme code="text.addToCart" var="addToCartText" />

<!-- Google Analytics tracking Data for PDP page -->
	<script>
		ga('ec:addProduct', {                 
		  'id': '${product.code}',                    
		  'name': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" +"${product.sizeVariantTitle}".toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''), 
		  'brand': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'')               
		 });
		
		ga('ec:setAction', 'detail');  
	</script>
	
<%-- <div class="productDetailsPanel"> --%>
<div class="bg-white clearfix pdp-main-content">
	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
		<product:productImagePanel product="${product}"	galleryImages="${galleryImages}" />
			<div class="badges">
				<c:if test="${not empty product.potentialPromotions}">
						<c:forEach items="${product.potentialPromotions}" var="promotions">
							<c:if test="${not empty promotions.stickerMediaUrls}">
								<c:forEach items="${promotions.stickerMediaUrls}" var="stickerURL">
									<img src="${stickerURL}" class="" alt="Clicks Promo"/>
								</c:forEach>
							</c:if>
						</c:forEach>
				</c:if>
				
				<c:if test="${not empty product.nonPromoStickerUrls}">
					<c:forEach items="${product.nonPromoStickerUrls}" var="stickerURL">
						<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
					</c:forEach>
				</c:if>
			</div>
	</div>
	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
		<div class="pdp-content clearfix">
			<div class="pdp-topBar clearfix">
				<div class="floatLeft">
					<product:productReviewSummary product="${product}" />
				</div>
				<div class="floatRight product-code">${product.code}</div>
			</div>
			
			<cms:pageSlot  position="FbLikeSection" var="feature" element="div" class="pdp-fb">
				<cms:component component="${feature}" element="div" class="pdp-fb"/>
			</cms:pageSlot>
			
			<c:if test="${not empty product.brand}">
				<h1 class="product-category">${product.brand}</h1>
			</c:if>	
		
			<ycommerce:testId
				code="productDetails_productNamePrice_label_${product.code}">

			</ycommerce:testId>

			<h1 class="product-name">${product.baseProductTitle ne null ? product.baseProductTitle : product.styleVariantTitle ne null ? product.styleVariantTitle: product.sizeVariantTitle ne null? product.sizeVariantTitle: product.name }
			</h1>

			<c:if test="${not empty product.copyWriteDescription}">
				<div class="product-desc">
					<p>${product.copyWriteDescription}</p>
				</div>
			</c:if>

			<c:if test="${not empty product.price.formattedValue}">
				<div class="price-details-wrapper clearfix">
					<div class="price-left floatLeft">

						<ycommerce:testId
							code="productDetails_productNamePrice_label_${product.code}">
							<price:fromPrice product="${product}" />
						<%-- <div class="price-offer">	
						<c:if test="${not empty product.price.grossPriceWithPromotionApplied and product.price.grossPriceWithPromotionApplied ne product.price.value}">
	 						<fmt:formatNumber var="save" pattern="####.##"
									value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
									<c:if test="${not empty save and (product.price.value - product.price.grossPriceWithPromotionApplied) gt 0}">
	 						Was&nbsp;<dateFormat:dateFormat conversionType="rand" input="${product.price.formattedValue}"/>
							<span class="red">Save</span>
							<span class="price red"> 
								
								R<dateFormat:dateFormat conversionType="rand" input="${save}"/>	
							</span>
							</c:if>
						</c:if>
						</div> --%>
						</ycommerce:testId>

					</div>
					<div class="price-right floatLeft price-content red">
					<product:productPromotionSection product="${product}" />
					</div>
				</div>
			</c:if>
			<product:productAddToCartPanel product="${product}" allowAddToCart="${empty showAddToCart ? true : showAddToCart}" isMain="true" />
			
			<%--<c:url var="notifyUrl" value="/store-pickup/${product.code}/notifyCustomer" />
			<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}">
			<form:form id="outstock-emailNotifyForm"  action="${notifyUrl}" method="post" commandName="storeFinderForm" onsubmit="return false;" >
			<div id="outstock-emailleft" class="outstock-email">
			<div  id="notifyMessage">
								
								</div>
			<a id="link" class="emailNotifyLink outstock-elink">Email me when in stock</a>
			<span class="close-link outstock-clink">Close</span>
			 <div id="subscribe-pop" class="subscribe-pop">
			 <input type="hidden" name="productCode" id="product-code" value="${productCode}"/> 
			 <input type="hidden" id="contextPath" value="${contextPath}">
			 <form:input path="email"  name="email" id="customer-email"/>
			<div class="paddingNone">
			<button type="submit" id="9000" class="btn stockSubscribeBtn"> NOTIFY ME </button>
			</div>
			</div>
			</div>
				</form:form>
			</c:if>	<br/>  
			
	<c:set var="actionUrl" value="${fn:replace(url,'{productCode}', product.code)}" scope="request"/>
	<storepickup:clickPickupInStore product="${product}" cartPage="false"/>
	<storepickup:pickupStorePopup/> --%>
	<c:set var="actionUrl" value="${fn:replace(url,'{productCode}', product.code)}" scope="request"/>	
	<c:remove var="actionUrl"/>
	<!--<c:if test="${not empty disclaimerMessage}">
		<span class="disclaimer">${disclaimerMessage}</span>
	</c:if> -->
			<c:if test="${not empty product.prodRewardPoints}">
				<div class="club-offer">
					Earn <span class="red">${product.prodRewardPoints} points</span> if
					you buy with Clicks ClubCard
				</div>
			</c:if>

			<%-- <product:productVariantSelector product="${product}" /> --%>
			<cms:pageSlot position="VariantSelector" var="component" element="div">
					<cms:component component="${component}" />
			</cms:pageSlot>

			<!-- 			<div class="pdp-btn"> This is not a part of phase 1 development
							<button type="submit" class="btn">Add to basket</button>
							<button type="submit" class="btn grey">Check store availability</button> 
						</div> -->		
			<div class="moreLinks clearfix">
				<!-- 		<a class="wish-list" href="#">Add to Wishlist</a>
							<a class="fav" href="#">Add to Favourties</a> -->
     			<c:set var="req" value="${pageContext.request}" />
				<c:set var="baseURL" value="${fn:replace(req.requestURL, fn:substring(req.requestURI, 1, fn:length(req.requestURI)), req.contextPath)}" />
     			<a class="email" href="javascript:void(0)" onclick="javascript:window.location='mailto:?subject=${product.name}&body=' + window.location;">Email</a>
     			<input id="urlVal" value="${requestScope['javax.servlet.forward.request_uri']}" type="hidden">
     			<input id="baseURL" value="${baseURL}" type="hidden">
				<a class="share" href="javascript:void(0)">Share</a>
				<!-- Share via social networking sites -->
				<div class="shareProduct a2a_kit social-share">
					<span class="popup-left-arrow"></span>
					<ul>
							<!-- <li><a title="Facebook" target="_blank" class="facebook_icon" href="https://www.facebook.com/"><img /></a></li> -->
							<li><a title="facebook" target="_blank" class="a2a_button_facebook icon-sprite icon-video-fb-share facebook_icon" href="https://www.facebook.com/"><img />Facebook</a></li>
							<!-- <li><a title="Twitter" target="_blank" class="twitter_icon" href="https://twitter.com/"><img /></a></li> -->
							<li><a title="twitter" target="_blank" class="a2a_button_twitter icon-sprite icon-video-tweet-share twitter_icon" href="https://twitter.com/"><img />Twitter</a></li>
							<li><a title="Google +" target="_blank" class="a2a_button_google_plus icon-sprite icon-video-gp-share google_icon" href="https://plus.google.com/"><img />Google +</a></li>   
							<!-- <li><a title="Google +" target="_blank" class="google_icon" href="https://plus.google.com/"><img /></a></li> -->  
							<!-- <li><a title="Email" class="email_icon"></a></li> -->      
					</ul>
				</div>
				<!-- Share via social networking sites -->
			</div>
		</div>

		<cms:pageSlot position="Section2" var="feature" element="div"
			class="span-8 section2 cms_disp-img_slot last">
			<cms:component component="${feature}" />
		</cms:pageSlot>


	</div>
</div>
