<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/desktop/product"%>

<%@ attribute name="featuredProductComponents" required="true"
	type="java.util.List"%>


<c:forEach items="${featuredProductComponents}"
	var="featuredProductComponent" varStatus="count">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-block">
		<div class="doubleBlockWrapper">
		<c:if test="${count.index ne 3}">
		<cms:component component="${featuredProductComponent}" />
		</c:if>
		<c:if test="${count.index eq 3 }">
		<!-- Add seerv block -->
		</c:if>
		</div>

	</div>
	
</c:forEach>

