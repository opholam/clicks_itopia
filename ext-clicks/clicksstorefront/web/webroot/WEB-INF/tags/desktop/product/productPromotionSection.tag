<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<div class="offerDesc red">
	<ycommerce:testId code="productDetails_promotion_label">
		<c:if test="${not empty product.potentialPromotions}">
			<c:choose>
				<c:when test="${not empty product.potentialPromotions[0].couldFireMessages}">
					<span>${product.potentialPromotions[0].couldFireMessages[0]}</span>
				</c:when>
				<c:otherwise>	
				<%-- ${product.potentialPromotions[0].description} --%>
				<c:forEach items="${product.potentialPromotions}" var="promotionDesc">
				<a class="offerDesc red" href="${contextPath}/c/OH1?q=%3Arelevance%3AallPromotions%3A${promotionDesc.code}">
				  ${promotionDesc.description}<br/>
				  </a>
				</c:forEach>
				</c:otherwise>
			</c:choose>
		</c:if>
	</ycommerce:testId>
</div>