<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>



<!-- <div id="productTabs"> -->
	<div class="tabOuterWrapper">
			<ul class="tabs">
				<li class="active"><a href="#information" data-toggle="tab">Detailed information</a></li>
				<li><a href="#use" data-toggle="tab">How to use</a></li>
				<li><a href="#ingredients" data-toggle="tab">Ingredients</a></li>
			</ul>
			<div class="tabContentWrapper">
				<div class="active tabContent" id="information">
				<c:choose>
				<%-- <p><product:productDetailsTab product="${product}"/></p> --%>
				<c:when test="${product.isProductDescDataAvailable}">
				<c:if test="${product.description ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.description" /></b><br/> ${product.description}</p></c:if>
				<c:if test="${product.otherDescription ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.detailedDescription"/></b><br/> ${product.otherDescription}</p></c:if>
				<c:if test="${product.flavour ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.flavour" /></b><br/> ${product.flavour}</p></c:if>
				<c:if test="${product.packSize ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.packSize" /></b><br/> ${product.packSize}</p></c:if>
				<c:if test="${product.servingSize ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.servingSize" /></b><br/> ${product.servingSize}</p></c:if>
				<c:if test="${product.learnMore ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.learnMore" /></b><br/> ${product.learnMore}</p></c:if>
				<c:if test="${product.features ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.features" /></b><br/> ${product.features}</p></c:if>
				<c:if test="${product.size ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.size" /></b><br/> ${product.size}</p></c:if>
				<c:if test="${product.qtyinPack ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.qtyinPack" /></b><br/> ${product.qtyinPack}</p></c:if>
				<c:if test="${product.marketingDescription ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.marketingDescription" /></b><br/> ${product.marketingDescription}</p></c:if>
				<c:if test="${product.brand ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.brand" /></b><br/> ${product.brand}</p></c:if>
				<c:if test="${product.techSpecifications ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.techSpecifications" /></b><br/> ${product.techSpecifications}</p></c:if>
				<c:if test="${product.endorsements ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.endorsements" /></b><br/> ${product.endorsements}</p></c:if>
				<%-- <c:if test="${product.packageType ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.packageType" /></b> ${product.packageType}</p></c:if> --%>
				<c:if test="${product.packageType ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.prodDesc.packageType" /></b><br/>
				<c:forEach items="${product.packageType}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				</c:when>
				<c:when test="${product.isLifestyeDataAvailable}">
				<c:if test="${product.kosherCode ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.lifestyle.kosherCode" /></b><br/><c:forEach items="${product.kosherCode}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				<c:if test="${product.hilalCode ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.lifestyle.hilalCode" /></b><br/><c:forEach items="${product.hilalCode}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				<c:if test="${product.recycleCode ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.lifestyle.recycleCode" /></b><br/><c:forEach items="${product.recycleCode}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				<c:if test="${product.lifestyle ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.lifestyle.lifestyle" /></b><br/><c:forEach items="${product.lifestyle}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				<c:if test="${product.freeFrom ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.lifestyle.freeFrom" /></b><br/><c:forEach items="${product.freeFrom}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				<c:if test="${product.healthClaims ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.lifestyle.healthClaims" /></b><br/><c:forEach items="${product.healthClaims}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if>
				</c:when>
				<c:when test="${product.isManufacturerDataAvailable}">
				<c:if test="${product.manufacturer ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.manufacturer" /></b><br/> ${product.manufacturer}</p></c:if>
				<c:if test="${product.manufacturerAddress ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.manufacturerAddress" /></b><br/> ${product.manufacturerAddress}</p></c:if>
				<c:if test="${product.manufacturerPhone ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.manufacturerPhone" /></b><br/> ${product.manufacturerPhone}</p></c:if>
				<c:if test="${product.manufacturerURL ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.manufacturerURL" /></b><br/> ${product.manufacturerURL}</p></c:if>
				<c:if test="${product.manufacturerEmail ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.manufacturerEmail" /></b><br/> ${product.manufacturerEmail}</p></c:if>
				<c:if test="${product.distributor ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.distributor" /></b><br/> ${product.distributor}</p></c:if>
				<c:if test="${product.distributorAddress ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.distributorAddress" /></b><br/> ${product.distributorAddress}</p></c:if>
				<c:if test="${product.distributorPhone ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.distributorPhone" /></b><br/> ${product.distributorPhone}</p></c:if>
				<c:if test="${product.distributorEmail ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.distributorEmail" /></b><br/> ${product.distributorEmail}</p></c:if>
				<c:if test="${product.distributorURL ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.distributorURL" /></b><br/> ${product.distributorURL}</p></c:if>
				<c:if test="${product.countryOfOrigin ne null}"><p><b><spring:theme code="pdp.detailedInfoTab.manufacturer.countryOfOrigin" /></b><br/> ${product.countryOfOrigin}</p></c:if>	
				</c:when>
				<c:otherwise>
				<p><spring:theme code="pdp.infoTabs.infoNotAvailable" /></p>
				</c:otherwise>
</c:choose>
				<a href="#" class="tabSeeMore">See more</a>
				<a href="#" class="tabSeeLess">See less</a>
				</div>
				<div class="tabContent" id="use">
				<c:choose>
				<c:when test="${product.isUsageDataAvailable}">
				<c:if test="${product.usageInstructions ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.usageInstructions" /></b><br/> ${product.usageInstructions}</p></c:if>
				<c:if test="${product.howToUse ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.howToUse" /></b><br/> ${product.howToUse}</p></c:if>
				<c:if test="${product.usage ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.usage" /></b><br/> ${product.usage}</p></c:if>
				<c:if test="${product.serving ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.serving" /></b><br/> ${product.serving}</p></c:if>
				<c:if test="${product.howtowear ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.howtowear" /></b><br/> ${product.howtowear}</p></c:if>
				<c:if test="${product.application ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.application" /></b><br/> ${product.application}</p></c:if>
				<c:if test="${product.cookingState ne null}"><p><b><spring:theme code="pdp.howToUseTab.usage.cookingState" /></b><br/> ${product.cookingState}</p></c:if>
				</c:when>
				<c:when test="${product.isStorageDataAvailable}">
				<c:if test="${product.storageInstructions ne null}"><p><b><spring:theme code="pdp.howToUseTab.storage.storageInstructions" /></b><br/> ${product.storageInstructions}</p></c:if>
				<c:if test="${product.storage ne null}"><p><b><spring:theme code="pdp.howToUseTab.storage.storage" /></b><br/> ${product.storage}</p></c:if>
				<c:if test="${product.howtoStore ne null}"><p><b><spring:theme code="pdp.howToUseTab.storage.howtoStore" /></b><br/> ${product.howtoStore}</p></c:if>
				</c:when>
				<c:when test="${product.isHazardsDataAvailable}">
				<c:if test="${product.warnings ne null}"><p><b><spring:theme code="pdp.howToUseTab.hazardsCautions.warnings" /></b><br/> ${product.warnings}</p></c:if>
				<c:if test="${product.hazardsandCautions ne null}"><p><b><spring:theme code="pdp.howToUseTab.hazardsCautions.hazardsandCautions" /></b><br/> ${product.hazardsandCautions}</p></c:if>
				<c:if test="${product.cautions ne null}"><p><b><spring:theme code="pdp.howToUseTab.hazardsCautions.cautions" /></b><br/> ${product.cautions}</p></c:if>
				</c:when>
				<c:otherwise>
				<p><spring:theme code="pdp.infoTabs.infoNotAvailable" /></p>
				</c:otherwise>
				</c:choose>	
				<a href="#" class="tabSeeMore">See more</a>
				<a href="#" class="tabSeeLess">See less</a>
				</div>
				
				<div  class="tabContent" id="ingredients">
				<c:choose>
				<c:when test="${product.isIngredientsDataAvailable}">
				<c:if test="${product.ingredients ne null}"><p><b><spring:theme code="pdp.ingredients.ingredients.ingredients" /></b><br/> ${product.ingredients}</p></c:if>
				<c:if test="${product.contains ne null}"><p><b><spring:theme code="pdp.ingredients.ingredients.contains" /></b><br/> ${product.contains}</p></c:if>
				<c:if test="${product.includes ne null}"><p><b><spring:theme code="pdp.ingredients.ingredients.includes" /></b><br/> ${product.includes}</p></c:if>
				<c:if test="${product.allergies ne null}"><p><b><spring:theme code="pdp.ingredients.ingredients.allergies" /></b><br/> ${product.allergies}</p></c:if>
				<c:if test="${product.vitaminsAndMinerals ne null}"><p><b><spring:theme code="pdp.ingredients.ingredients.vitaminsAndMinerals" /></b><br/> ${product.vitaminsAndMinerals}</p></c:if>
				</c:when>
				<c:when test="${product.isNutritionalDataAvailable}">
				<c:if test="${product.measure ne null}"><p><b><spring:theme code="pdp.ingredients.nutritionalInformation.measure" text="Measure:"/></b><br/> ${product.measure}</p></c:if>
				<c:if test="${product.servingSizeFull ne null}"><p><b><spring:theme code="pdp.ingredients.nutritionalInformation.servingSizeFull" text="Serving Size:"/></b><br/> ${product.servingSizeFull}</p></c:if>
				<c:if test="${product.numberOfServings ne null}"><p><b><spring:theme code="pdp.ingredients.nutritionalInformation.numberOfServings" text="Number of servings:"/></b><br/> ${product.numberOfServings}</p></c:if>
<%-- 				<c:if test="${product.tableHeadings ne null}"><p><b><spring:theme code="pdp.ingredients.nutritionalInformation.tableHeadings" text="Table Headings:"/></b><br/><c:forEach items="${product.tableHeadings}" var="name" varStatus="loop">
    				${name}${!loop.last ? ',' : ''}
				</c:forEach></p></c:if> --%>
				<c:if test="${product.nutrientItemsList ne null}">
				<c:set var="newLine" value="\n"/>
				<table class="table table-bordered table-hover prod_table">
         			 <thead>
            		 <tr>
             			<th><spring:theme code="pdp.ingredients.nutritionalInformation.nutrientItems.name" text="Name"/></th>
              			<th><spring:theme code="pdp.ingredients.nutritionalInformation.nutrientItems.uom" text="Unit of measure"/></th>
              			<c:forEach items="${product.tableHeadings}" var="tableHeading" varStatus="loop">
             			<th>${tableHeading}</th>
             			</c:forEach>
            		</tr>
          			</thead>
          			<tbody>
          			<c:forEach items="${product.nutrientItemsList}" var="nutrients" varStatus="loop">
            		<tr>
              		   <td>${nutrients.nutrientItemName}</td>
              		   <td>${nutrients.nutrientItemUOM}</td>
              		   <c:forEach items="${nutrients.itemValueList}" var="name" varStatus="loop1">
              		   <c:if test="${not empty name && name ne newLine}">
              		   <td>${name}</td>
              		   </c:if>
              		   </c:forEach>
            		</tr>
            		</c:forEach>
          			</tbody>
         		</table>	
				</c:if>
				</c:when>
				<c:otherwise>
				<p><spring:theme code="pdp.infoTabs.infoNotAvailable" /></p>
				</c:otherwise>
				</c:choose>	
				<a href="#" class="tabSeeMore">See more</a>
				<a href="#" class="tabSeeLess">See less</a>
				</div>				
			</div>	
	</div>
<!-- </div> -->

