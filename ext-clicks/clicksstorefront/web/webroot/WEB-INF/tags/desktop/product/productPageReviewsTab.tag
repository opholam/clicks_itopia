<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>


<c:url value="${product.url}/reviewhtml/3" var="getPageOfReviewsUrl" />
<c:url value="${product.url}/reviewhtml/all" var="getAllReviewsUrl" />


<div id="reviews" class="reviews" data-reviews="${getPageOfReviewsUrl}"
	data-allreviews="${getAllReviewsUrl}"></div>
<div id="write_reviews" class="reviews" style="display: none">
	<input id="productCode" type="hidden" value="${product.code}">
	<input id="contextPath" type="hidden" value="${contextPath}">
	<input id="showReviewForm" type="hidden" value="${showReviewForm}">
	<div class="write_review clearfix">
		<div class="headline">
			<spring:theme code="review.write.title" />
		</div>
		<div class="required right">
			<spring:theme code="review.required" />
		</div>
		<div class="description">
			<spring:theme code="review.write.description" />
		</div>

		<c:url value="${product.url}/review" var="productReviewActionUrl" />
		<form:form method="post" action="${productReviewActionUrl}"
			commandName="reviewForm">

			<div class="write_review_container form-wrap">
				<div class="form-group">
					<formElement:formInputBox idKey="review.headline"
						labelKey="review.headline" path="headline" inputCSS="form-control"
						mandatory="true" />
				</div>
				<div class="form-group">
					<formElement:formTextArea idKey="review.comment"
						labelKey="review.comment" path="comment" areaCSS="textarea"
						mandatory="true" />
				</div>

				<spring:bind path="rating">
					<div class="form-group <c:if test="${not empty status.errorMessages}"> error</c:if>">
						<label class="control-label"><spring:theme
								code="review.rating" /></label>
						<div id="stars-wrapper" class="controls clearfix">
							<c:forEach begin="1" end="5" varStatus="status">
								<label><img class="no_star"
									src="${commonResourcePath}/images/jquery.ui.stars.custom.gif"
									alt="<spring:theme code="review.rating.alt"/>" /> <form:radiobutton
										path="rating" value="${status.index}" />${status.index}/${status.end}</label>
							</c:forEach>
						</div>
						<div class="help-inline">
							<form:errors path="rating" />
						</div>
					</div>
				</spring:bind>
				<div class="form-group">
					<formElement:formInputBox idKey="alias" labelKey="review.alias"
						path="alias" inputCSS="form-control" mandatory="false" />
				</div>
			</div>
			<div class="form-group">
				<button class="btn primary_btn" type="submit"
					value="<spring:theme code="review.submit"/>">
					<spring:theme code="review.submit" />
				</button>
				<%-- <a href="${contextPath}/p/${product.code}#reviewsOuter" id="read_reviews_action" class="btn primary_btn"><spring:theme code="review.back" /></a> --%>
			</div>

		</form:form>
	</div>
	<!-- <div class="actionBar bottom clearfix"></div> -->
</div>

<div id='reviewsOuter' class="reviewsOuter">
	<h3>Reviews</h3>
	<div class="reviewCountWrapper">
		<span class="floatLeft"> 
		<product:productStars
				rating="${product.averageRating}" /> 
				<span class="reviewCount">from
				${product.numberOfReviews} reviews</span>
		</span> 
		<a id="writeReview" class="reviewWrite floatRight"
			href="#reviews">Write a review</a>
	</div>
	<div id="appendMoreReview">
		<c:forEach items="${product.reviews}" var="review" begin="0" end="1">
			<div class="reviewContentOuter">
				<div class="row">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
						<span class="reviewTitle">${review.headline}</span>
						<product:productStars rating="${review.rating}" />
						<c:choose>
							<c:when
								test="${fn:contains(review.principal.name, 'Anonymous')}">
								<div class="reviewerOuter">${review.principal.name}
									reviewer ,
									<fmt:formatDate pattern="dd MMM yyyy" value="${review.date}" />
								</div>
							</c:when>
							<c:otherwise>
								<div class="reviewerOuter">${review.principal.name}
									,
									<fmt:formatDate pattern="dd MMM yyyy" value="${review.date}" />
								</div>
							</c:otherwise>
						</c:choose>
						<c:if test="${review.principal.isExpertReview}">
							<a class="reviewspecial" href="#">Clicks expert reviewer</a>
						</c:if>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
				<div class="img-circle img-responsive">
				<c:choose>
				<c:when test="${review.principal.profilePicUrl ne null && not empty review.principal.profilePicUrl}">
				<img class="img-circle img-responsive" src="${review.principal.profilePicUrl}"/>
				</c:when>
				<c:otherwise>
				<cms:component uid="section1A-CMSImage-component1-for-profileImagePage"></cms:component>
				</c:otherwise>
				</c:choose>
				</div>
					</div>
				</div>
				<p>${review.comment}</p>
			</div>
		</c:forEach>
	</div>
</div>
<c:if
	test="${product.numberOfReviews ne null && product.numberOfReviews > 2}">
	<button id="loadMoreForPDP" class="btn btn-load-more">Load
		more reviews</button>
</c:if>

<script id="loadCusImageOnPdp" type='text/x-jsrender'>	
								{{if profilePicUrl}}		
									<img src="{{:profilePicUrl}}"/>
								{{else}}
							<cms:component uid="section1A-CMSImage-component1-for-profileImagePage"></cms:component>
									{{/if}}
</script>

<script id="loadExpertReview" type='text/x-jsrender'>	
								{{if isExpertReview}}		
									<a class="reviewspecial" href="#">Clicks expert reviewer</a>
									{{/if}}
</script>