<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%-- <%@ attribute name="imageData" required="false"
	type="de.hybris.platform.commercefacades.product.data.ImageData"%> --%>
<%@ attribute name="productData" required="false"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>


<c:set var="count" value="0" scope="page" />
<c:set var="isProdListingAvailable" value="false" scope="page"/>
<c:choose>
<c:when test="${not empty categoryName and not empty gaplacement}">
<a href="${contextPath}${productData.url}" class="relatedPro_img" onclick="onProductClick('${productData.code}','${gatitle}','${gabrand}', '${gaCategoryName}', '${gaplacement}', '${gaurl}', '${gaplacement}'); return !ga.loaded;">
</c:when>
<c:otherwise>
<a href="${contextPath}${productData.url}" class="relatedPro_img">
</c:otherwise>
</c:choose>

<c:forEach items="${productData.images}" var="imageData">
<c:if test="${count < 1 && imageData.format eq 'productListing'}">
	<c:set var="count" value="${count + 1}" scope="page"/>
	<c:set var="isProdListingAvailable" value="true" scope="page"/>
	<img src="${imageData.url}" alt="${fn:escapeXml(imageData.altText)}" title="${fn:escapeXml(imageData.altText)}"/>
</c:if>
</c:forEach>
<c:if test="${!isProdListingAvailable}">
<theme:image code="img.missingProductImage.product" alt="${fn:escapeXml(product.name)}" title="${fn:escapeXml(product.name)}" />
</c:if>
</a>