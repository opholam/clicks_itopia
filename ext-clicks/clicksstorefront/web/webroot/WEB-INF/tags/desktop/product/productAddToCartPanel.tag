<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="allowAddToCart" required="true" type="java.lang.Boolean" %>
<%@ attribute name="isMain" required="true" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>

<c:url value="/cart/add" var="cartUrl"/>
<form:form action="${cartUrl}" method="post" commandName="addToCartForm" class="add_to_cart_form">
<input type="hidden" name="productCodePost" value="${product.code}"/>
<div class="quantityWrapper no_items clearfix">
	<c:if test="${product.stock.stockLevelStatus.code ne 'outOfStock' && product.purchasable}">
		<span>
			<spring:theme code="basket.page.quantity"/>
		</span>
		<div class="addQuantity">
			<button type="button" class="removeQuantityProduct btn-default">-</button>
			<ycommerce:testId code="qtyInput">
			 <form:input cssClass="qty form-control quantity" path="qty" idKey="qtyInput" value="1"/> 
			</ycommerce:testId>
			<button type="button" class="updateProductQuantity btn-default">+</button>
		 </div>
	</c:if>

	<c:if test="${product.stock.stockLevel gt 0}">
		<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
			<spring:theme code="product.variants.in.stock"/>
		</c:set>
	</c:if>
	<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
		<c:set var="productStockLevel">
			<spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
		</c:set>
	</c:if>
	<c:if test="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}">
		<c:set var="productStockLevel">
			<spring:theme code="product.variants.available"/>
		</c:set>
	</c:if>

	<!--<ycommerce:testId code="productDetails_productInStock_label">
		<p class="stock_message">${productStockLevel}</p>
	</ycommerce:testId> -->
</div>
<div class="pdp-btn">
<%-- <form id="addToCartForm" class="add_to_cart_form" action="<c:url value="/cart/add"/>" method="post"> --%>
<%--  <input type="hidden" name="productCodePost" value="${product.code}"/> --%>
<%--  <input type="hidden" name="qty" class="quantity" id="qtyInput" value="${addToCartForm.qty}"> --%>

  <c:set var="buttonType">button</c:set>

  <c:if test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' and product.stock.stockLevel gt 0}">
   <c:set var="buttonType">submit</c:set>
  </c:if>

  <c:choose>
   <c:when test="${fn:contains(buttonType, 'button')}">
    <button type="${buttonType}" class="addToCartButton outOfStock btn disabled" disabled="disabled">
     <spring:theme code="product.pdp.out.of.stock"/>
    </button>
   </c:when>

   <c:otherwise>
  					<c:set var="brand" value="${fn:replace(product.brand,'\\'','')}"></c:set>
					<c:set var="title" value="${fn:replace(product.sizeVariantTitle,'\\'','')}"></c:set>
					<%-- <c:set var="url" value="${contextPath}${fn:replace(product.url,'\\'','')}"></c:set> --%>
					<%-- <c:set var="pageStatus" value="add from product list page"/> --%>
					<input type="hidden" name="productCode" value="${product.code}"/>
					<input type="hidden" name="gatitle" value="${title}" />
					<!-- <input type="hidden" name="gaurl" value="" /> -->
					<input type="hidden" name="pageStatus" value="add from pdp" />
					<input type="hidden" name="gabrand" value="${brand}" />
						
    <button  type="${buttonType}" class="btn addToCartButtonSubmit" name="button" disabled="disabled">
     <spring:theme code="basket.add.to.basket"/>
    </button>
   </c:otherwise>
  </c:choose>
 <c:if test="${not empty product.maxOrderQuantity}">
<div class="offerDesc red">
<p>${productCappingMessage}</p>
</div>
</c:if>

 </div>
 </form:form>
<div id="actions-container-for-${component.uid}" class="pdp-btn">
	
		<action:actions element="li" styleClass="productAddToCartPanelItem span-5" parentComponent="${component}"/>
	
</div>

