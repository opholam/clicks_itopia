<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${financialServiceModels ne null}">
<c:forEach items="${financialServiceModels}" var="financialServiceModel">
<c:set var="activelink" value="${financialServiceModel.activationLink}"/>
	<c:choose>
		<c:when test="${fn:containsIgnoreCase(activelink, 'activate')}">
			<c:url value="/clubcard/financial/activelink" var="activateUrl" />
		</c:when>
	<c:when test="${fn:containsIgnoreCase(activelink, 'quote')}">
			<c:url value="http://www.regent.co.za/clicks/quickquote.aspx" var="activateUrl" />
	</c:when>
	<c:otherwise>
		<c:url value="https://hybrisq.clicks.co.za/store/clubcard/financial" var="activateUrl" />
	</c:otherwise>
</c:choose>

	<div class="policy_outer">
		<div class="services_wrap clearfix">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<div class="policy_offer">
					<div class="offer_details">
						<!-- Up to R7,500
					<span> cover at no cost to you </span> -->
						${financialServiceModel.name}
					</div>
					<a href="${activateUrl}" target="_blank"> <!-- Activate now -->
						${financialServiceModel.activationLink}
					</a>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<!-- <h4>Funeral cover <span>at no cost to you</span></h4> -->
				${financialServiceModel.title}
				<!-- 
			<a class="btn-small">Exclusive ClubCard benefit</a>
			<p>
				If you are a ClubCard member aged 18-65, you are eligible for funeral cover at no cost to you. 
				In the event of your death, your family will have cash towards your funeral. Your cover is 10 times the value of 
				your average monthly purchases at Clicks - up to a maximum of R7500.
			</p> -->
				<p>${financialServiceModel.summary} </p>
				
			
				<c:if test="${not empty financialServiceModel.description}">
				<a class="moreInfo" href = " " target="myFinancePolicy">Show details</a>
				<div id="financePolicy" class="financeinfo" style="display: none;">
				${financialServiceModel.description}
				
				<div class="downloads">
					<c:set var="flag" value="false"/>
						<c:forEach items="${financialServiceModel.documents}" var="document">
							<c:if test="${not flag}">
										<c:set var="flag" value="true" />				
										<h5>Downloads</h5>
									</c:if>
							<a href="${document.downloadURL}" class="pdf-ico" download="${document.altText}">${document.altText}</a><br/>
						</c:forEach>
					</div>
					</div>
					</c:if>
			</div>
		</div>
	</div>
</c:forEach>
</c:if>
<!-- End First Block -->

