<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="facetData" required="true"
 type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>
 <%@ attribute name="index" type="java.lang.String"%>
 <%@ attribute name="breadcrumb" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${not empty facetData.values and facetData.name ne 'allCategories' and facetData.name ne 'allPromotions'}">
 
 <c:set var="hasSelectedFacetVal" value="false"></c:set>
 <c:forEach items="${facetData.values}" var="facetVal">
 <c:if test="${facetVal.selected}">
 	 <c:set var="hasSelectedFacetVal" value="true"></c:set>
 </c:if>
 </c:forEach>
   <div class="panel panel-default bg-white readMoreChild ${facetData.name eq 'Type' or hasSelectedFacetVal eq 'true' ? 'closed' : ''}">
    <spring:theme code="text.hideFacet" var="hideFacetText" />
    <spring:theme code="text.showFacet" var="showFacetText" />
    
    <a class="refinementToggle tittle"  href="javascript:void(0)"
     data-hide-facet-text="${hideFacetText}"
     data-show-facet-text="${showFacetText}"> <spring:theme
      code="search.nav.facetTitle" arguments="${facetData.name}" />
    </a>
 <!--  <c:if test="${facetData.name eq 'Type' || facetData.name eq 'Category'}">
    	<h4><a href="${request.contextPath}/c/${categoryCode}">All ${categoryName}</a></h4>
    </c:if>
   -->    
    <ycommerce:testId code="facetNav_facet${facetData.name}_links">
     <div class="facetValues">
      <c:if test="${not empty facetData.topValues}">
       <div class="topFacetValues">
        <ul class="panel-collapse collapse in">
        
         <c:forEach items="${facetData.topValues}" var="facetValue">
         <!--Top Values : ${facetValue.name} -->
          	<c:set var="escapeFacet" value="false" />
        	<c:forEach items="${rootCategories}" var="rootCat">
        		<c:if test="${rootCat eq fn:toLowerCase(facetValue.name)}">
        			<c:set var="escapeFacet" value="true" />		
        		</c:if>
        	</c:forEach>
        	
         	<c:if test="${not escapeFacet}">
	          <li><a href="javascript:void(o)"> 
	          <c:if test="${facetData.multiSelect}">
	             <form action="#" method="get">
	              <input type="hidden" name="q"
	               value="${facetValue.query.query.value}" /> <input
	               type="hidden" name="text"
	               value="${searchPageData.freeTextSearch}" /> <label
	               class="facet_block-label"> <input type="checkbox"
	               ${facetValue.selected ? 'checked="checked"' : ''}
	               onchange="$(this).closest('form').submit()" /> <span
	               ${facetValue.selected ? 'class="activeLink"' : ''}>
	               <span id="facetName_${facetValue.code}">${facetValue.name}&nbsp;</span> <span
	                class="facetValueCount"><spring:theme
	                  code="search.nav.facetValueCount"
	                  arguments="${facetValue.count}" /></span>
	              </span>
	              </label>
	             </form>
	            </c:if> <c:if test="${not facetData.multiSelect}">
	             <c:url value="${facetValue.query.url}"
	              var="facetValueQueryUrl" />
	             <a href="${facetValueQueryUrl}&amp;text=${searchPageData.freeTextSearch}">${facetValue.name}
	          <span class="facetValueCount"><spring:theme
	               code="search.nav.facetValueCount"
	               arguments="${facetValue.count}" /></span>
	               </a>
	            </c:if>
	          </a></li>
          </c:if>
         </c:forEach>
		  <c:if test="${not empty facetData.topValues and not empty facetData.values}">
	      <span class="more">
	       <a href="#" class="moreFacetValues" ><spring:theme code="search.nav.facetShowMore_${facetData.code}" text="See more"/></a>
	      </span>
			</c:if>
        </ul>
       </div>
      </c:if>
     
    
     <div class="allFacetValues panel-collapse collapse ${facetData.name eq 'Type' or hasSelectedFacetVal eq 'true' ? 'in' : ''}" style="${not empty facetData.topValues ? 'display:none' : ''}" style="${not empty facetData.topValues ? 'display:none' : ''}">
      
      <ul class="facet_block ${facetData.multiSelect ? '' : 'indent'}">
      
<c:choose>
<c:when test="${facetData.name eq 'Type' and not empty catFacets}">
<c:set var="plpFacetValues" value="${catFacets}" />
</c:when><c:otherwise>
<c:set var="plpFacetValues" value="${facetData.values}" />
</c:otherwise>
</c:choose>
       <c:forEach items="${plpFacetValues}" var="facetValue" varStatus="countStats">
         <c:set var="escapeFacet" value="false" />
        <c:forEach items="${rootCategories}" var="rootCat">
        	<c:if test="${rootCat eq fn:toLowerCase(facetValue.name)}">
        		<c:set var="escapeFacet" value="true" />		
        	</c:if>
        </c:forEach>
        <c:if test="${fn:contains(facetValue.code, 'OH2') or fn:contains(facetValue.code, 'OH1') or fn:contains(facetValue.code, 'brands')  or fn:contains(facetValue.code, 'collections')}">
        	<c:set var="escapeFacet" value="true" />		
        </c:if>
        
        <c:if test="${not escapeFacet}">
        <li class="${facetValue.selected ? 'active' : ''}">
        <!--multiselect : ${facetValue.name} -->
      
	        <c:if test="${facetData.multiSelect}">
	          <form action="#" method="get">
	          <a href=""  class="hiddenClass"> <input type="hidden" name="q"
	            value="${facetValue.query.query.value}" /> <input
	            type="hidden" name="text"
	            value="${searchPageData.freeTextSearch}" />
	            </a> 
	            <input id="filterId${index}${countStats.index}" type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''} 
	            	onchange="$(this).closest('form').submit()" />
	            	      <label class="facet_block-label" for="filterId${index}${countStats.index}"> 
	            	<span id="facetName_${facetValue.code}">${facetValue.name}&nbsp;</span> 
	            	<span class="facetValueCount">
	            		<spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}" />
	            	</span>
	           </label>
	            	<c:forEach items="${breadcrumb}" var="removeUrls">
	            		<c:if test="${removeUrls.facetValueName eq facetValue.name}">
	            			<c:url value="${removeUrls.removeQuery.url}" var="removeQueryUrl"/>
	            			<a class="removeAppliedFilter" href="${removeQueryUrl}">Remove</a>
	            		</c:if>
	            	</c:forEach>
	          </form>
	         </c:if>
         
         <c:if test="${not facetData.multiSelect}">
          <c:url value="${facetValue.query.url}"
           var="facetValueQueryUrl" />
          <a href="${facetValueQueryUrl}">${facetValue.name}&nbsp;
          <spring:theme
            code="search.nav.facetValueCount"
            arguments="${facetValue.count}" /> </a>
         </c:if>
         </li>
         </c:if>
       </c:forEach>
      </ul>
      <button class='read-more-facet'>See more</button>
      <button class='read-less-facet'>See less</button>
      <c:if test="${not empty facetData.topValues}">
       <span class="more"> <a href="#" class="lessFacetValues"><spring:theme
          code="search.nav.facetShowLess_${facetData.code}" text="See less"/></a>
       </span>
      </c:if>
     </div>
     </div>
    </ycommerce:testId>
   </div>
   
   

</c:if>