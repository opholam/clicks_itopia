<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>


<c:if test="${not empty pageData.breadcrumbs}">
	<div class="filterResult">
	<div class="facet">
	<div class="facetValues filterSelectedSection">
	<c:url value="?text=${pageData.freeTextSearch}&q=" var="clearAll" />
	<h2 class="bg-white clearfix"><span>Filtered by</span> <a href="${clearAll}" class="clearallBtn">Clear All</a></h2>
		<ul class="facet_block filterSelection">
			<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
				<li class="remove_item_left">
					<c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl"/>
					<span class="remove_item_left_name">
					<c:choose>
					<c:when test="${breadcrumb.facetName eq 'New' or breadcrumb.facetCode eq 'newProduct'}"> ${breadcrumb.facetName}</c:when>
					<c:otherwise>
					${breadcrumb.facetValueName} 
					</c:otherwise>
					</c:choose></span>
					
					<span class="remove"><a href="${removeQueryUrl}" ></a></span>
					<div class="clear"></div>
				</li>
			</c:forEach>
		</ul>
	</div> 
	</div>
	</div>

</c:if>
