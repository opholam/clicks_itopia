<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="entryNumber" required="false" type="java.lang.Long" %>
<%@ attribute name="cartPage" required="false" type="java.lang.Boolean" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>


<c:url var="addToCartToPickupInStoreUrl" value="/store-pickup/cart/add"/>
<c:url var="updateSelectStoreUrl" value="/store-pickup/cart/update"/>
<div id="globalMessages">
		<common:globalMessages />
</div>
<script type="text/javascript">
	var addToCartToPickupInStoreUrl = '${addToCartToPickupInStoreUrl}';
	var searchLocation = '${locationQuery}';
</script>
<script id="storeListTemplate" type='text/x-jsrender'>
<c:if test="${searchPageData ne null and !empty searchPageData.results}">
<ul class="searchPOSResultsList clear_fix">
	<c:forEach items="${searchPageData.results}" var="pickupStore" varStatus="loopStatus">
	{{if count >= ${loopStatus.count}}}
	<c:url value="${pickupStore.url}" var="pickupStoreUrl" />
		<li class="column searchPOSResult">		
		<h3>

           <div class="searchresult-content">
				<c:choose>
				<c:when test="${pickupStore.stockData.stockLevelStatus.code eq 'outOfStock'}">
					<div class="resultStock negative out-stock">
						<spring:theme code="pickup.out.of.stock"/>
					</div>
				</c:when>
				<c:when test="${pickupStore.stockData.stockLevelStatus.code ne 'outOfStock' and empty pickupStore.stockData.stockLevel}">
					<div class="resultStock">
						<spring:theme code="pickup.force.in.stock"/>
					</div>
				</c:when>
				<c:otherwise>
					<div class="resultStock">
						<!--<spring:theme code="pickup.in.stock" arguments="${pickupStore.stockData.stockLevel}"/> -->
						<spring:theme code="pickup.in.stock"/>
					</div>
				</c:otherwise>
			</c:choose>
			at <div class="resultName">${pickupStore.displayName}</div>
            </div>
			<div class="resultDistance"><span>${pickupStore.formattedDistance}</span></div>
			</h3>
			<P>
			<div class="clear_fix address">
				<div class="resultLine1">${pickupStore.address.line1}</div>
				<div class="resultLine2">${pickupStore.address.line2}</div>
				<div class="resultTown">${pickupStore.address.town}</div>
				<div class="resultUip">${pickupStore.address.postalCode}</div>
			</div>
				</P>
				<c:if
								test="${pickupStore.stockData.stockLevelStatus.code eq 'outOfStock'}">
								<!--  <div id="popup_store_pickup_form_product_${searchPageData.product.code}" class="searchPOS clearfix" style=""> -->
								<div id="custSubscribeDiv${pickupStore.name}">
								<div  id="notifyMessage">
								
								</div>
								<form id="emailNotifyForm" onsubmit="return false;" >
								<div id="emailleft" class="emailleft">
								<a id="link" class="emailNotifyLink">Email me when in stock at ${pickupStore.displayName}</a>
								<span class="close-link">Close</span>
								 <div id="subscribe-pop" class="subscribe-pop">
								 <input type="hidden" name="productCode" id="product-code" value="${searchPageData.product.code}"/> 
								 <input type="hidden" id="contextPath" value="${contextPath}">
								<!--  <form id="emailNotifyForm" onsubmit="return false;" >	  -->							
								<input type="hidden" class="text" name="locationQuery" id="storelocator-q" value="${locationQuery}" mandatory="true">
								<input type="text" name="email" id="customer-email" placeholder="<spring:theme code="pickup.email.message" />" value="${email}">
								<input type="hidden" name="cartPage" data-id="atCartPage" value="${cartPage}" />
								<input type="hidden" name="storeName" id="store-name"  value="${pickupStore.name}"/>
								<input type="hidden" name="entryNumber" value="${entryNumber}" class="entryNumber" />
								<div class="paddingNone">
								<button type="submit" id="${pickupStore.name}" class="btn subscribeEmailBtn"> NOTIFY ME </button>
								</div>
								</div>
								</div>
								</form>
								</div>
							</c:if>	
		<!-- 	<c:if test="${pickupStore.stockData.stockLevel gt 0 or empty pickupStore.stockData.stockLevel}">
				<c:choose>
					<c:when test="${cartPage}">
						<form:form id="selectStoreForm" class="select_store_form" action="${updateSelectStoreUrl}" method="post">
							<input type="hidden" name="storeNamePost" value="${pickupStore.name}"/>
							<input type="hidden" name="entryNumber" value="${entryNumber}"/>
							<input type="hidden" name="hiddenPickupQty" value="1" class="hiddenPickupQty"/>
							<button type="submit" class="positive  pickup_here_instore_button">
								<spring:theme code="pickup.here.button"/>
							</button>
						</form:form>
					</c:when>
					<c:otherwise>
						<form:form  class="add_to_cart_storepickup_form" action="${addToCartToPickupInStoreUrl}" method="post">
							<input type="hidden" name="storeNamePost" value="${pickupStore.name}"/>
							<input type="hidden" name="productCodePost" value="${searchPageData.product.code}"/>
							<input type="hidden" name="hiddenPickupQty" value="1" class="hiddenPickupQty" />
							<button type="submit" class="positive pickup_add_to_bag_instore_button">
								<spring:theme code="text.addToCart"/>
							</button>
						</form:form>
					</c:otherwise>
				</c:choose>
			</c:if>  -->
			
		</li>
	{{/if}}
	</c:forEach>
</ul>
<div class="more-btn">
{{if count < ${fn:length(searchPageData.results)} }}
<a class="btn btn-load-more" href="#" onclick="loadStores({{:count}})">SEE MORE STORES</a>
{{/if}}
<h5> <c:if test="${not empty disclaimerMessage}">
		<span class="disclaimer">${disclaimerMessage}</span>
	</c:if><br/>
<c:if test="${not empty modifiedTime}" >
Stock levels calculated at ${modifiedTime} and may be out of date.
</c:if></h5>  
</div>
</c:if>
</script>
<div id="storeList">
<ul class="searchPOSResultsList clear_fix">
	<c:forEach items="${searchPageData.results}" var="pickupStore" varStatus="loopStatus" begin="0" end="9">
	<c:url value="${pickupStore.url}" var="pickupStoreUrl" />
		<li class="column searchPOSResult">			
		<h3>
		     <div class="searchresult-content">
				<c:choose>
				<c:when test="${pickupStore.stockData.stockLevelStatus.code eq 'outOfStock'}">
					<div class="resultStock negative out-stock">
						<spring:theme code="pickup.out.of.stock"/>
					</div>
				</c:when>
				<c:when test="${pickupStore.stockData.stockLevelStatus.code ne 'outOfStock' and empty pickupStore.stockData.stockLevel}">
					<div class="resultStock">
						<spring:theme code="pickup.force.in.stock"/>
					</div>
				</c:when>
				<c:otherwise>
					<div class="resultStock">
						<!--<spring:theme code="pickup.in.stock" arguments="${pickupStore.stockData.stockLevel}"/> -->
						<spring:theme code="pickup.in.stock"/>
					</div>
				</c:otherwise>
			</c:choose>
			<div class="result-content">at</div> <div class="resultName">${pickupStore.displayName}</div>
			</div>
			<div class="resultDistance"><span>${pickupStore.formattedDistance}</span></div>
			</h3>
			<P>
			<div class="clear_fix address">
				<div class="resultLine1">${pickupStore.address.line1}</div>
				<div class="resultLine2">${pickupStore.address.line2}</div>
				<div class="resultTown">${pickupStore.address.town}</div>
				<div class="resultUip">${pickupStore.address.postalCode}</div>
			</div>
				</P>
						<c:if
								test="${pickupStore.stockData.stockLevelStatus.code eq 'outOfStock'}">
								<!--  <div id="popup_store_pickup_form_product_${searchPageData.product.code}" class="searchPOS clearfix" style=""> -->
								<div id="custSubscribeDiv${pickupStore.name}">
								<div  id="notifyMessage">
								
								</div>
								<form id="emailNotifyForm" onsubmit="return false;" >
								<div id="emailleft" class="emailleft">
								<a id="link" class="emailNotifyLink">Email me when in stock at ${pickupStore.displayName}</a>
								<span class="close-link">Close</span>
								 <div id="subscribe-pop" class="subscribe-pop">
								 <input type="hidden" name="productCode" id="product-code" value="${searchPageData.product.code}"/> 
								 <input type="hidden" id="contextPath" value="${contextPath}">
								<!--    -->							
								<input type="hidden" class="text" name="locationQuery" id="storelocator-q" value="${locationQuery}" mandatory="true">
								<input type="text" name="email" id="customer-email" placeholder="<spring:theme code="pickup.email.message" />" value="${email}">
								<input type="hidden" name="cartPage" data-id="atCartPage" value="${cartPage}" />
								<input type="hidden" name="storeName" id="store-name"  value="${pickupStore.name}"/>
								<input type="hidden" name="entryNumber" value="${entryNumber}" class="entryNumber" />
								<div class="paddingNone">
								<button type="submit" id="${pickupStore.name}" class="btn customerSubscribeBtn"> NOTIFY ME </button>
								</div>
								</div>
								</div>
								</form>
								</div>
							</c:if>			
		</li>
	</c:forEach>
</ul>
<c:if test="${fn:length(searchPageData.results)>10}">
		<div class="more-btn">
		<a class="btn btn-load-more" href="#" onclick="loadStores(10)">SEE
			MORE STORES</a>
			<h5> <c:if test="${not empty disclaimerMessage}">
		<span class="disclaimer">${disclaimerMessage}</span>
	</c:if> <br/><c:if test="${not empty modifiedTime}" >
Stock levels calculated at ${modifiedTime} and may be out of date.
</c:if></h5>
		</div>
	</c:if>
	<c:if test="${fn:length(searchPageData.results) > 0 and fn:length(searchPageData.results)<10}">
	<h5> <c:if test="${not empty disclaimerMessage}">
		<span class="disclaimer">${disclaimerMessage}</span>
	</c:if><br/><c:if test="${not empty modifiedTime}" >
Stock levels calculated at ${modifiedTime} and may be out of date.
</c:if></h5> 
	</c:if>
	</div>
	