<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<div id="globalMessages">
	 <common:globalMessages />  
</div>
<div id="popup_store_pickup_form" class="searchPOS clearfix" style="display:none">
	<div class="head clearfix">	
			<h4 class="store-head-left">CLICKS</h4>
		    <div class="back store-head-right">
		    	<a href="${referrer}"type="submit" class="btn"><span class="back-arrow"></span>Back</a>
		    </div>		
	</div>
	
	<div class="prod_grid column">
		<span class="thumb"></span>
		<div class="headline"><h4><spring:theme code="pickup.product.availability" /></h4>
		<span>Choose a Clicks store:</span></span></div>
		<!-- <div class="details"></div>  -->
		<!-- <div class="price"></div> -->
	<!-- 	<div class="quantity pickup_store_search-quantity">
			<label data-for="pickupQty"><spring:theme code="basket.page.quantity" /></label>
			<input type="text" size="1" maxlength="3"  data-id="pickupQty" name="qty" class="qty" />
		</div>  -->
	</div>
	
	<div class="column last searchPOSContent">
			<form:form name="pickupInStoreForm" action="#" method="post" class="searchPOSForm clearfix">
				 <label for="locationForSearch" class="skip">Choose a Clicks store: </label>
				 <ul>
					<li class="loc-sym">
						<button type="submit" class="btn icon_target" data-id="find_pickupStoresNearMe_button"></button>
					</li>
						
				    <li class="loc-txt">
					    <input type="text" name="locationQuery" data-id="locationForSearch" class="left location" placeholder="<spring:theme code="pickup.search.message" />" />
					</li>
				    
					<li>
						<input type="hidden" name="cartPage" data-id="atCartPage" value="${cartPage}" />
					</li>
					
					<li>
						<input type="hidden" name="entryNumber" value="${entryNumber}" class="entryNumber" />
					</li>
					
					<li class="loc-search">
						<button type="submit" class="loc-submit btn" data-id="pickupstore_search_button"><spring:theme code="pickup.search.button" /></button>
					</li>
				</ul>	
					
				
			</form:form>

			<div data-id="pickup_store_results" > </div>
		</div>
		
</div>
