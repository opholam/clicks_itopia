<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="store" required="true"
	type="de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="container">
	<div class="title-bar title-icons-wrap">
		<h1 class="main-title">${store.displayName}</h1>
		<p>
			<c:if test="${not empty store.features}">
				<c:forEach items="${store.features}" var="feature"
					varStatus="status">
					<c:set var="value" value="${value}${feature.value}" />
				</c:forEach>
				<c:if test="${value ne 'Store' }">
					<strong>Facilities</strong>
				</c:if>
				<c:forEach items="${store.featureAttrList}" var="feature">
					<c:set var="value" value="${feature.feature}" />
					<c:if test="${fn:containsIgnoreCase(value, 'ph')}">
						<c:if test="${not empty feature.icon}">
							<img src="${feature.icon.url}" />
						</c:if>
						<span class="blue">${value}</span>
					</c:if>
				</c:forEach>
				<c:forEach items="${store.featureAttrList}" var="feature">
					<c:set var="value" value="${feature.feature}" />
					<c:if test="${fn:containsIgnoreCase(value, 'cl') and !fn:containsIgnoreCase(value, 'click')}">
						<c:if test="${not empty feature.icon}">
							<img src="${feature.icon.url}" />
						</c:if>
						<span class="green">${value}</span>
					</c:if>
				</c:forEach>
			</c:if>
		</p>
		<p>
			<c:if
				test="${not empty store.address.line1 || not empty store.address.line2 ||
				  not empty store.address.town || not empty store.address.country.name ||
				  not empty store.address.postalCode}">
				<strong>Address</strong>
				<span> <c:if test="${not empty store.address.shopNumber}">${store.address.shopNumber},&nbsp;</c:if>
					<c:if test="${not empty store.address.centerName}">${store.address.centerName},&nbsp;</c:if>
					<c:if test="${not empty store.address.line1}">${store.address.line1},&nbsp;</c:if>
					<c:if test="${not empty store.address.line2}">${store.address.line2},&nbsp;</c:if>
					<c:if test="${not empty store.address.town}">${store.address.town},&nbsp;</c:if>
					<c:if test="${not empty store.address.postalCode}">${store.address.postalCode}</c:if>.
				</span>
			</c:if>
		</p>
	</div>
</div>


<div class="store-finder">
	<div class="container">
		<div class="row">
			<div class="col-md-6 store-map">
				<div class="map-container">
					<store:storeMap store="${store}" />
				</div>
			</div>

			<div class="col-md-6">
			<%-- 	<c:if test="${not empty store.openDtae}">
					<div class="blue-bar">
						Store opens on
						<fmt:formatDate type="date" dateStyle="full"
							value="${store.openDtae}" />
					</div>
				</c:if> --%>
				<div class="store-hours">
					<c:forEach items="${store.features}" var="feature"
						varStatus="status">
						<c:set var="value" value="${value}${feature.value}" />
					</c:forEach>
					<c:choose>
						<c:when test="${fn:contains(value,'Pharmacy')}">
							<h2 class="blue">Store & pharmacy</h2>
						</c:when>
						<c:otherwise>
							<h2 class="blue">Store</h2>
						</c:otherwise>
					</c:choose>
					<div class="store-info">
						<div class="store-individual">
							<h3>Opening hours</h3>
							<div class="row openingHoursWrap">
								<c:if test="${not empty store.openingHoursList}">
									<c:forEach items="${store.openingHoursList}" var="open"
										varStatus="loopStatus">
										<c:set var="name" value="${open.name}" />
										<c:if test="${fn:containsIgnoreCase(name, 'str')}">
											<store:openingSchedule openingSchedule="${open}" />
										</c:if>
									</c:forEach>
								</c:if>
							</div>
							<c:if test="${not empty store.openingHoursList}">
								<c:forEach items="${store.openingHoursList}" var="spl"
									varStatus="loopStatus">
									<c:if test="${not empty spl.specialDayOpeningList}">
										<c:set var="name" value="${spl.name}" />
										<c:if test="${fn:containsIgnoreCase(name, 'str')}">
											<store:openingSpecialDays openingSchedule="${spl}" />
										</c:if>
									</c:if>
								</c:forEach>
							</c:if>
							<em>Call to confirm</em>
						</div>
						<!-- added for pharmasist -->
						<div class="store-individual">
							<h3>Contact Details</h3>
							<div class="contactStore">
								<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12 store_pharm1'>
									<c:if test="${not empty store.phoneNumberLists}">
									<dl>
										<c:forEach items="${store.phoneNumberLists}" var="phone"
											varStatus="loopStatus">
											<c:set var="name" value="${phone.name}" />
											<c:if test="${not fn:containsIgnoreCase(name, 'cl') and (fn:containsIgnoreCase(name, 'tel'))}">
											
													<c:forEach items="${phone.number}" var="numbers"
														varStatus="loopStatus">
														<dt>
															<c:if test="${fn:containsIgnoreCase(name, 'str')}">Shop telephone ${loopStatus.count}</c:if>
															<c:if test="${(fn:containsIgnoreCase(name, 'ph')) && (fn:containsIgnoreCase(name, 'tel'))}">Pharmacy tel</c:if>
															<%-- <c:if test="${(fn:containsIgnoreCase(name, 'tel')) and (!fn:containsIgnoreCase(name, 'str'))}">
															<span class="text-right shop_tel">tel</span> 
														</c:if> --%>
														
															<%-- <c:if test="${(fn:containsIgnoreCase(name, 'str')) && (fn:containsIgnoreCase(name, 'fax'))}">Shop fax</c:if>
															<c:if test="${(fn:containsIgnoreCase(name, 'ph')) && (fn:containsIgnoreCase(name, 'fax'))}">Pharmacy fax</c:if> --%>
														</dt>
														<dd>
															<c:if test="${fn:containsIgnoreCase(name, 'tel')}"><span class='phone_num'> ${numbers} </span></c:if>
														</dd>
													</c:forEach>
											
											</c:if>
										</c:forEach>
										</dl>
									</c:if>
								</div>
								<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12 store_pharm1'>
								<c:if test="${not empty store.phoneNumberLists}">
								<dl>
										<c:forEach items="${store.phoneNumberLists}" var="phone" varStatus="loopStatus">
											<c:set var="name" value="${phone.name}" />
											<c:if test="${(not fn:containsIgnoreCase(name, 'cl')) && (fn:containsIgnoreCase(name, 'fax'))}">
											
													<c:forEach items="${phone.number}" var="numbers" varStatus="loopStatus">
														<dt>
															<c:if test="${(fn:containsIgnoreCase(name, 'str')) && (fn:containsIgnoreCase(name, 'fax'))}">Shop fax</c:if>
															<c:if test="${(fn:containsIgnoreCase(name, 'ph')) && (fn:containsIgnoreCase(name, 'fax'))}">Pharmacy fax</c:if>
														</dt>
														<dd>
															<c:if test="${fn:containsIgnoreCase(name, 'fax')}"> <span class='phone_num'> ${numbers} </span></c:if>
														</dd>
													</c:forEach>
											
											</c:if>
										</c:forEach>
										</dl>
									</c:if>
								</div>
							</div>
						<%-- As per client requrement commenting this --%>
							<%-- <c:if test="${not empty store.featureAttrList}">
								<c:forEach items="${store.featureAttrList}" var="featureattr"
									varStatus="loopStatus">
									<c:if test="${not empty featureattr.id}">
										<span>Responsible Pharmacist: ${featureattr.id} </span>
										<span>Pharmacy registration number:
											${featureattr.value}</span>
									</c:if>
								</c:forEach>
							</c:if> --%>
						</div>
					</div>
				</div>



				<!-- added for pharmasist -->

				<c:if test="${not empty store.features}">
					<c:forEach items="${store.features}" var="feature">
						<c:set var="value" value="${feature.value}" />
						<c:if test="${fn:containsIgnoreCase(value, 'cl') and !fn:containsIgnoreCase(value, 'click')}">
							<div class="store-hours clinic-info">
								<h2 class="green">Clinic</h2>
								<div class="store-info ">
									<div class="store-individual">
										<h3>Opening hours</h3>
										<div class="row openingHoursWrap">
											<c:if test="${not empty store.openingHoursList}">
												<c:forEach items="${store.openingHoursList}" var="open"
													varStatus="loopStatus">
													<c:set var="name" value="${open.name}" />
													<c:if test="${fn:containsIgnoreCase(name, 'cl')}">
														<store:openingSchedule openingSchedule="${open}" />
													</c:if>
												</c:forEach>
											</c:if>
										</div>
										<c:if test="${not empty store.openingHoursList}">
											<c:forEach items="${store.openingHoursList}" var="spl"
												varStatus="loopStatus">
												<c:if test="${not empty spl.specialDayOpeningList}">
													<c:set var="name" value="${spl.name}" />
													<c:if test="${fn:containsIgnoreCase(name, 'cl')}">
														<store:openingSpecialDays openingSchedule="${spl}" />
													</c:if>
												</c:if>
											</c:forEach>
										</c:if>
									</div>
									<div class="store-individual">
										<c:set var="isClinic" value="No" />
										<c:forEach items="${store.phoneNumberLists}" var="phone"
											varStatus="loopStatus">
											<c:set var="name" value="${phone.name}" />
											<c:if test="${fn:containsIgnoreCase(name, 'cl')}">
												<c:set var="isClinic" value="yes" />
											</c:if>
										</c:forEach>
										<c:if test="${fn:containsIgnoreCase(isClinic, 'yes')}">
											<h3>Contact Details</h3>
										</c:if>

										<div class="row contactStore">
											<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
												<c:if test="${not empty store.phoneNumberLists}">
													<c:forEach items="${store.phoneNumberLists}" var="phone"
														varStatus="loopStatus">
														<c:set var="name" value="${phone.name}" />

														<c:if test="${fn:containsIgnoreCase(name, 'cl') and !fn:containsIgnoreCase(name, 'click')}">
															<dl>
																<dt>
																	<%-- <c:if test="${fn:containsIgnoreCase(name, 'cl')}">Clinic&nbsp;&nbsp;</c:if> --%>
																	<c:if test="${fn:containsIgnoreCase(name, 'tel')}">
																		<span> Telephone </span>
																	</c:if>
																	<%-- <c:if test="${fn:containsIgnoreCase(name, 'fax')}">Fax</c:if> --%>
																</dt>
																<dd>
																	<c:forEach items="${phone.number}" var="numbers"
																		varStatus="loopStatus">
																	${numbers} 
																	</c:forEach>
																</dd>
															</dl>
														</c:if>
													</c:forEach>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</c:if>
			</div>
		</div>
	</div>
</div>


<%-- 

<c:if test="${not empty store.formattedDistance}">
	<div class="detailSection">
		<div class="detailSectionHeadline"><spring:theme code="storeDetails.table.distance" /></div>
		<c:choose>
			<c:when test="${not empty locationQuery}">
				<spring:theme code="storeDetails.table.distanceFromSource" argumentSeparator="^" arguments="${store.formattedDistance}^${fn:escapeXml(locationQuery)}"/>
			</c:when>
			<c:otherwise>
				<spring:theme code="storeDetails.table.distanceFromCurrentLocation" argumentSeparator="^" arguments="${store.formattedDistance}"/>
			</c:otherwise>
		</c:choose>
	</div>
</c:if>


<c:if test="${not empty store.address.phone}">
	<div class="detailSection">
		<div class="detailSectionHeadline"><spring:theme code="storeDetails.table.telephone" /></div>
		${store.address.phone}
	</div>
</c:if>

<c:if test="${not empty store.address.email}">
	<div class="detailSection">
		<div class="detailSectionHeadline"><spring:theme code="storeDetails.table.email" /></div>
		<a href="mailto:${store.address.email}">${store.address.email}</a>
	</div>
</c:if>



<c:if test="${not empty store.openingHoursList}">
<c:forEach items="${store.openingHoursList}" var="open" varStatus="loopStatus">
<div class="detailSection">
<div class="detailSectionHeadline">
		<c:set var="name" value="${open.name}"/>
								<c:if test="${fn:containsIgnoreCase(name, 'str')}">
  								Shop
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'ph')}">
  								Pharmacy
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'cl')}">
  								Clinic
								</c:if>
		<spring:theme code="storeDetails.table.opening" /></div>
		<store:openingSchedule openingSchedule="${open}" />
		<c:if test="${not empty spl.specialDayOpeningList}">
		<store:openingSpecialDays openingSchedule="${open}" /></c:if>
	</div>
</c:forEach>
</c:if>


<c:if test="${not empty store.openingHoursList}">
<c:forEach items="${store.openingHoursList}" var="spl" varStatus="loopStatus"> 
<c:if test="${not empty spl.specialDayOpeningList}"> 
	<div class="detailSection">
		<div class="detailSectionHeadline">
		<c:set var="name" value="${spl.name}"/>
								<c:if test="${fn:containsIgnoreCase(name, 'str')}">
  								Shop
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'ph')}">
  								Pharmacy
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'cl')}">
  								Clinic
								</c:if>
		
		<spring:theme code="storeDetails.table.openingSpecialDays" /></div>
		<store:openingSpecialDays openingSchedule="${spl}" />
	</div>
</c:if>
</c:forEach>
</c:if>




<c:if test="${not empty store.features}">
	<div class="detailSection">
		<div class="detailSectionHeadline"><spring:theme code="storeDetails.table.features" /></div>
		<ul>
			<c:forEach items="${store.features}" var="feature">
				<li>${feature.value}</li>
			</c:forEach>
		</ul>
	</div>
</c:if>


<c:if test="${not empty store.phoneNumberLists}">
<div class="detailSection">
		<div class="detailSectionHeadline"><spring:theme code="Contact Numbers" /></div>
			<c:forEach items="${store.phoneNumberLists}" var="phone" varStatus="loopStatus">
						<ul>
								<c:set var="name" value="${phone.name}"/>
								<c:if test="${fn:containsIgnoreCase(name, 'str')}">
  								Shop&nbsp;&nbsp;
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'ph')}">
  								Pharmacy&nbsp;&nbsp;
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'cl')}">
  								Clinic&nbsp;&nbsp;
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'tel')}">
  								Tel
								</c:if>
								<c:if test="${fn:containsIgnoreCase(name, 'fax')}">
  								Fax
								</c:if>
								<li> &nbsp;&nbsp;&nbsp;${phone.number}</li>
						</ul>
			</c:forEach>
</div>
</c:if>
--%>