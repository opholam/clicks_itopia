<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="errorNoResults" required="true" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url value="/store-finder" var="storeFinderFormAction" />
<c:url value="/store-finder/position" var="nearMeStorefinderFormAction"/>


		<div class="container">
			<div class="row title-bar">
				<h1 class="main-title col-md-6 col-xs-12"><span>Find your nearest</span> Clicks</h1>
				
				<form:form action="${storeFinderFormAction}" method="post" commandName="storeFinderForm" class="store-search col-md-4 pull-right col-xs-12">	
									<ycommerce:testId code="storeFinder_search_box"><span class="storesearchicon"></span>
										<input type="text" class="text" placeholder="Search the store" name="q" id="storelocator-query"  mandatory="true" value="${locationQuery}">
										<input type="submit" class="btnSearch" value="q">
									</ycommerce:testId>
				</form:form>
				
			</div>
		</div>


<!--     ----Default OOTB hybris store search code below: ------
									

<div class="searchPane">
	<div class="headline"><spring:theme code="storeFinder.find.a.store" /></div>
	<div class="description"><spring:theme code="storeFinder.use.this.form" /></div>
	
	<form:form action="${storeFinderFormAction}" method="get" commandName="storeFinderForm">
		<ycommerce:testId code="storeFinder_search_box">
			<formElement:formInputBox idKey="storelocator-query" labelKey="storelocator.query" path="q" inputCSS="text" mandatory="true" />
			<button class="positive" type="submit"><spring:theme code="storeFinder.search" /></button>
		</ycommerce:testId>
	</form:form>
	
	<hr>
	
	<form:form id="nearMeStorefinderForm" name="near_me_storefinder_form" method="POST" action="${nearMeStorefinderFormAction}">
		<input type="hidden" id="latitude" name="latitude"/>
		<input type="hidden" id="longitude" name="longitude"/>
		<button href="#" id="findStoresNearMe" class="positive input-block-level findStoresNearMe" type="submit"><spring:theme code="storeFinder.findStoresNearMe"/></button>
	</form:form>
	
</div>

 -->