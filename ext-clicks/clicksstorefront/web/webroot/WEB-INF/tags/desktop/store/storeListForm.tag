<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="locationQuery" required="false" type="java.lang.String" %>
<%@ attribute name="count" required="false" type="java.lang.String" %>
<%@ attribute name="geoPoint" required="false" type="de.hybris.platform.commerceservices.store.data.GeoPoint" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>

<script id="storeListTemplate" type='text/x-jsrender'>

<ul class="store-info">
<c:if test="${searchPageData ne null and !empty searchPageData.results}">
		<c:forEach items="${searchPageData.results}" var="pos" varStatus="loopStatus" >

		{{if count >= ${loopStatus.count}}}

			<c:url value="${pos.url}" var="posUrl"/>
			<li>
			<section class="pull-left">		
				<c:url value="${pos.url}" var="storeUrl" scope="request"/>
				<h3>
							<ycommerce:testId code="storeFinder_result_link">
								<a id= "${loopStatus.count}" ></a> ${loopStatus.count} <a href="${posUrl}"> ${pos.displayName}</a>
								<span>${pos.formattedDistance} away </span>
								
							</ycommerce:testId>
							
				</h3>	
				<P>
					<ycommerce:testId code="storeFinder_result_address_label">
						<c:if test="${not empty pos.address}">
								<c:if test="${not empty pos.address.shopNumber}">${pos.address.shopNumber},</c:if>
								<c:if test="${not empty pos.address.centerName}">${pos.address.centerName},</c:if>
								<c:if test="${not empty pos.address.line1}">${pos.address.line1},</c:if>
								<c:if test="${not empty pos.address.line2}">${pos.address.line2},</c:if>
								<c:if test="${not empty pos.address.town}">${pos.address.town},</c:if>
								<c:if test="${not empty pos.address.postalCode}">${pos.address.postalCode}</c:if>.
						</c:if>
					</ycommerce:testId>
				</P>
			</section>
			<div class="pull-right">

						<c:if test="${not empty pos.features}">

									<c:forEach items="${pos.featureAttrList}" var="feature" >
									
									<c:set var="value" value="${feature.feature}"/>
									<c:if test="${fn:containsIgnoreCase(value, 'cl') and !fn:containsIgnoreCase(value, 'click')}">
									<div class="pharmaClinicWrap">
										<c:if test="${not empty feature.icon}">
											<img src="${feature.icon.url}" />
										</c:if>
										<p class="green">${feature.feature}</p>
									</div>	
									</c:if>
									
									</c:forEach>
							
									<c:forEach items="${pos.featureAttrList}" var="feature">
									
									<c:set var="value" value="${feature.feature}"/>
									<c:if test="${fn:containsIgnoreCase(value, 'ph')}">
									<div class="pharmaClinicWrap">
										<c:if test="${not empty feature.icon}">
											<img src="${feature.icon.url}" />
										</c:if>
										<p class="blue">${feature.feature}</p>
									</div>	
									</c:if>
									
									</c:forEach>									
									
						</c:if>
				</div>
				</li>
				{{/if}}
				</c:forEach>
</c:if>
</ul>
{{if count < ${fn:length(searchPageData.results)} }}
<a class="btn btn-load-more" href="#" onclick="loadStores({{:count}})">Load more</a>
{{/if}}
</script>


<%-- <c:set value="/store-finder?q=${param.q}" var="searchUrl" />
<c:if test="${not empty geoPoint}">
	<c:set  value="/store-finder?latitude=${geoPoint.latitude}&longitude=${geoPoint.longitude}&q=${param.q}" var="searchUrl" />
</c:if>
 --%>

<div id="storeList">
<ul class="store-info"> 

<c:if test="${searchPageData ne null and !empty searchPageData.results}">
		<c:forEach items="${searchPageData.results}" var="pos" varStatus="loopStatus" begin="0" end="9">
			<c:url value="${pos.url}" var="posUrl"/>
			<li>
			<section class="pull-left">		
				<c:url value="${pos.url}" var="storeUrl" scope="request"/>
				<h3>
							<ycommerce:testId code="storeFinder_result_link">
								<a id= "${loopStatus.count}" ></a> ${loopStatus.count} <a href="${posUrl}"> ${pos.displayName}</a>
								<span>${pos.formattedDistance} away </span>
								
							</ycommerce:testId>	
				</h3>	
				<P>
					<ycommerce:testId code="storeFinder_result_address_label">
						<c:if test="${not empty pos.address}">
								<c:if test="${not empty pos.address.shopNumber}">${pos.address.shopNumber},</c:if>
								<c:if test="${not empty pos.address.centerName}">${pos.address.centerName},</c:if>
								<c:if test="${not empty pos.address.line1}">${pos.address.line1},</c:if>
								<c:if test="${not empty pos.address.line2}">${pos.address.line2},</c:if>
								<c:if test="${not empty pos.address.town}">${pos.address.town},</c:if>
								<c:if test="${not empty pos.address.postalCode}">${pos.address.postalCode}</c:if>
						</c:if>
					</ycommerce:testId>
				</P>
			</section>
			
			<div class="pull-right">
						<c:if test="${not empty pos.featureAttrList}">								
								<c:forEach items="${pos.featureAttrList}" var="feature" >
									<c:set var="value" value="${feature.feature}"/>
									<c:if test="${fn:containsIgnoreCase(value, 'cl') and !fn:containsIgnoreCase(value, 'click')}">
									<div class="pharmaClinicWrap">
										<c:if test="${not empty feature.icon}">
											<img src="${feature.icon.url}" />
										</c:if>
										<p class="green">${feature.feature}</p>
									</div>	
									</c:if>
								</c:forEach>
								<c:forEach items="${pos.featureAttrList}" var="feature">
									<c:set var="value" value="${feature.feature}"/>
									<c:if test="${fn:containsIgnoreCase(value, 'ph')}">
									<div class="pharmaClinicWrap">
										<c:if test="${not empty feature.icon}">
											<img src="${feature.icon.url}" />
										</c:if>
										<p class="blue">${feature.feature}</p>
									</div>	
									</c:if>
								</c:forEach>
						</c:if>
				</div>
				</li>
				
				</c:forEach>
<%-- 	<nav:pagination top="false"
					supportShowAll="false"
					supportShowPaged="false"
					searchPageData="${searchPageData}"
					searchUrl="${searchUrl}"
					msgKey="text.storefinder.desktop.page"
					numberPagesShown="${numberPagesShown}"/> --%>
					

</c:if>
</ul>

<%--  <div ><h3>Total Number of stores : ${fn:length(searchPageData.results)}</h3></div>  --%>
<c:if test = "${fn:length(searchPageData.results)>10}">
<a class="btn btn-load-more" href="#" onclick="loadStores(10)">Load more</a>
</c:if>
</div>

