<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>

<%@ attribute name="openingSchedule" required="true" type="de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- 
<c:if test="${not empty openingSchedule}">
	<ycommerce:testId code="storeDetails_table_openingSchedule_label">
		<table class="store-openings weekday_openings">
			<tbody>
				<c:forEach items="${openingSchedule.weekDayOpeningList}" var="weekDay">
					<tr class="${weekDay.closed ? 'weekday_openings_closed' : 'weekday_openings'}">
						<td class="weekday_openings_day">${weekDay.weekDay}</td>
						<td class="weekday_openings_times">
							<c:choose>
								<c:when test="${weekDay.closed}" >
										<spring:theme code="storeDetails.table.opening.closed" />
								</c:when>
								<c:otherwise>
									${weekDay.openingTime.formattedHour} - ${weekDay.closingTime.formattedHour}
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</ycommerce:testId>
</c:if>
 --%>
 
 
<c:if test="${not empty openingSchedule}">
	<ycommerce:testId code="storeDetails_table_openingSchedule_label">
				<c:forEach items="${openingSchedule.weekDayOpeningList}" var="weekDay" varStatus="i">
				<c:set var="value" value="${weekDay.weekDay}" />
					<c:if test="${not fn:containsIgnoreCase(value, 'sun')}">
					 <c:if test="${i.count==2 || i.count==6}"><div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'></c:if>
						<dl><dt>${weekDay.weekDay}</dt>
						<dd>
							<c:choose>
								<c:when test="${weekDay.closed}" >
										<spring:theme code="storeDetails.table.opening.closed" text="Closed"/>  
								</c:when>
								<c:when test="${not empty weekDay.openingTime.formattedHour and not empty weekDay.closingTime.formattedHour}">
									${weekDay.openingTime.formattedHour} - ${weekDay.closingTime.formattedHour}
									</c:when>
								<c:otherwise>
									<spring:theme code="storeDetails.table.opening.closed" text="Closed"/>
								</c:otherwise>
							</c:choose>
						</dd></dl>
						<c:if test="${i.count==5}"> </div> </c:if>
					</c:if>	
				</c:forEach>
				
				<c:forEach items="${openingSchedule.weekDayOpeningList}" var="weekDay" varStatus="i">
				<c:set var="value" value="${weekDay.weekDay}" />
					<c:if test="${fn:containsIgnoreCase(value, 'sun')}">
				
						<dl><dt>${weekDay.weekDay}</dt>
						<dd>
							<c:choose>
								<c:when test="${weekDay.closed}" >
										<spring:theme code="storeDetails.table.opening.closed" text="Closed"/>
								</c:when>
								<c:when test="${not empty weekDay.openingTime.formattedHour and not empty weekDay.closingTime.formattedHour}">
									${weekDay.openingTime.formattedHour} - ${weekDay.closingTime.formattedHour}
									</c:when>
								<c:otherwise>
									<spring:theme code="storeDetails.table.opening.closed" text="Closed"/>
								</c:otherwise>
							</c:choose>
						</dd></dl>
					</div>
					</c:if>
				</c:forEach>
	</ycommerce:testId>
</c:if>

