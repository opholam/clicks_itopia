<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="storeSearchPageData" required="false"
	type="de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="map-container">
 

	<c:if
		test="${storeSearchPageData ne null and !empty storeSearchPageData.results}">
		<div id="map_canvas" class="map active"
			data-latitude='${searchPageData.sourceLatitude}'
			data-longitude='${searchPageData.sourceLongitude}'
			data-south-Latitude='${searchPageData.boundSouthLatitude}'
			data-west-Longitude='${searchPageData.boundWestLongitude}'
			data-North-Latitude='${searchPageData.boundNorthLatitude}'
			data-east-Longitude='${searchPageData.boundEastLongitude}'
			data-stores='{
		<c:forEach items="${storeSearchPageData.results}" var="singlePos" varStatus="status"  begin="0"  end="9">
			<c:if test="${(status.index != 0)}">,</c:if>"store${status.index}":{"id":"${status.index}","latitude":"${singlePos.geoPoint.latitude}","longitude":"${singlePos.geoPoint.longitude}","name":"<a href=${request.contextPath}${singlePos.url}><div class=strong>${fn:escapeXml(singlePos.displayName)}</div><div>${fn:escapeXml(singlePos.address.line1)}</div><div>${fn:escapeXml(singlePos.address.line2)}</div><div>${fn:escapeXml(singlePos.address.town)}</div><div>${fn:escapeXml(singlePos.address.postalCode)}</div><div>${fn:escapeXml(singlePos.address.country.name)}</div></a>"}
		</c:forEach>
			}'
		data-allstores='{
		<c:forEach items="${storeSearchPageData.results}" var="singlePos" varStatus="status"  >
			<c:if test="${(status.index != 0)}">,</c:if>"store${status.index}":{"id":"${status.index}","latitude":"${singlePos.geoPoint.latitude}","longitude":"${singlePos.geoPoint.longitude}","name":"<a href=${request.contextPath}${singlePos.url}><div class=strong>${fn:escapeXml(singlePos.displayName)}</div><div>${fn:escapeXml(singlePos.address.line1)}</div><div>${fn:escapeXml(singlePos.address.line2)}</div><div>${fn:escapeXml(singlePos.address.town)}</div><div>${fn:escapeXml(singlePos.address.postalCode)}</div><div>${fn:escapeXml(singlePos.address.country.name)}</div></a>"}
		</c:forEach>
			}'
			>

		</div>
	</c:if>


	<h2>
		<cms:pageSlot position="SideContent" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</h2>
</div>



