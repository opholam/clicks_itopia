<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<!-- Google Advance E-commerce plugin Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	<c:choose>
	<c:when test="${previewForGoogleAnalytics}">
		ga('create', '${googleAnalyticsTrackingId}','auto');
	</c:when>
	<c:otherwise>
		//Creating tracker for the page
		 ga('create', '${googleAnalyticsTrackingId}',{cookieDomain: 'clicks.co.za'});
		</c:otherwise>	
	</c:choose>
  //loading ecommerce plugin for google analytics
  ga('require', 'ec');
</script>