<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>

<script type="text/javascript" src="${sharedResourcePath}/js/analyticsmediator.js"></script>
<%-- <analytics:googleAnalytics/> --%>

<%-- Custom google analytics tag for using GA advance ecommerce plugin --%>
<analytics:ecommerceAnalytics/>
<analytics:hotJarAnalytics/>

<analytics:jirafe/>