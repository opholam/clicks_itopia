<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="priceData" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="displayFreeForZero" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%--
 Tag to render a currency formatted price.
 Includes the currency symbol for the specific currency.
--%>
<c:set var="price" value="${priceData.formattedValue}" />
<c:choose>
	<c:when test="${priceData.value > 0}">
		<c:set var="dec" value="${fn:split(price, '.')}" />
		<c:set var="dec1" value="${dec[0]}" />
		<c:set var="dec2" value="${dec[1]}" />
		<div class="price-wrap">
			<c:if test="${empty product.potentialPromotions[0]}">
				<c:set var="red" value="red" />
			</c:if>
			<div class="price ${red}">
				${dec1}<sup>${dec2}</sup>
			</div>
		</div>
		
	</c:when>
	<c:otherwise>
		<c:if test="${displayFreeForZero}">
			<spring:theme code="text.free" text="FREE"/>
		</c:if>
		<c:if test="${not displayFreeForZero}">
		<c:set var="dec" value="${fn:split(price, '.')}" />
		<c:set var="dec1" value="${dec[0]}" />
		<c:set var="dec2" value="${dec[1]}" />
		<div class="price-wrap">
			<c:if test="${empty product.potentialPromotions[0]}">
				<c:set var="red" value="red" />
			</c:if>
			<div class="price ${red}">
				${dec1}<sup>${dec2}</sup>
			</div>
		</div>
		
		</c:if>
	</c:otherwise>
</c:choose>


<%-- <c:choose>
	<c:when test="${priceData.value > 0}">
		${priceData.formattedValue}
	</c:when>
	<c:otherwise>
		<c:if test="${displayFreeForZero}">
			<spring:theme code="text.free" text="FREE"/>
		</c:if>
		<c:if test="${not displayFreeForZero}">
			${priceData.formattedValue}
		</c:if>
	</c:otherwise>
</c:choose> --%>
