<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%--
 Tag to render a currency formatted price.
 Includes the currency symbol for the specific currency.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- <c:choose>
	<c:when test="${product.price.priceType == 'FROM'}">
		
		We pass the formatted currency amount into the message so that it can be used in the from message.
		Note: As the formatted currency may contain characters that <spring:theme> interprets as argument
		separators (e.g. comma) we change the separator to some random string sequence that will not appear
		in the formatted currency value.
		
		<spring:theme code="product.price.from"
			arguments="${product.price.formattedValue}" argumentSeparator="#~/@!�$%^" /> s
	</c:when>
	<c:otherwise>
 --%>
 		<c:set var="price" value="${product.price.formattedValue}" />
 		<c:if test="${not empty product.price.grossPriceWithPromotionApplied and product.price.grossPriceWithPromotionApplied gt 0}">
 			<fmt:formatNumber minFractionDigits="2" var="promoPrice" pattern="####.##" value="${product.price.grossPriceWithPromotionApplied}" />
 			<c:set var="price" value="R${promoPrice}" />
 		</c:if>
		<c:set var="dec" value="${fn:split(price, '.')}" />
		<c:set var="dec1" value="${dec[0]}" />
		<c:set var="dec2" value="${dec[1]}" />

		<div class="price-wrap">
			<c:if test="${empty product.potentialPromotions[0]}">
				<c:set var="blue" value="blue" />
			</c:if>


			<div class="price ${blue}">

				${dec1}<sup>${dec2}</sup>
			</div>
			
		</div>
<%-- 
	</c:otherwise>
</c:choose> --%>
