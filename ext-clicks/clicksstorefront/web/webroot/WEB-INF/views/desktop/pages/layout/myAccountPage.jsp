<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>


<template:page pageTitle="${pageTitle}">


		<!-- Points Account start-->
<c:set var="botswanaaccount" value="" scope="request" />
<c:set var="africaaccount" value="" scope="request" />

<c:forEach var="account" items="${user.pointStatement.accounts}">
	<c:if test="${account.accountID eq 5000003}">
		<c:set var="botswanaaccount" value="${account}" scope="request" />
	</c:if>
	<c:if test="${account.accountID eq 5000002}">
		<c:set var="africaaccount" value="${account}" scope="request"/>
	</c:if>
	
</c:forEach>
<!-- Points Account Ends-->


<c:set var="botswanaCBRAccount" value="" scope="request" />
<c:set var="africaCBRAccount" value="" scope="request" />

<c:forEach var="cbrStatement" items="${user.CBRstatement}">
	<c:if test="${cbrStatement.accountID eq 5000000}">
		<c:set var="africaCBRAccount" value="${cbrStatement}" scope="request" />
	</c:if>
	
	<c:if test="${cbrStatement.accountID eq 5000001}">
		<c:set var="botswanaCBRAccount" value="${cbrStatement}" scope="request"/>
	</c:if>
</c:forEach>

<c:set var="qualify" value="false"></c:set>
	<c:if test="${not empty botswanaaccount and botswanaaccount.pointsToQualify gt 0}">
		<c:set var="qualify" value="true"></c:set>
	</c:if>


<div class="clearfix bg-white accounttop">
		<div class="container">
			<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 paddingNone">
				<!-- Profile Image  -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<c:choose>
				<c:when test="${not empty user.profilePicture.url}">
				<div class="img-circle img-responsive">
				<img class="img-circle img-responsive" src="${user.profilePicture.url}" />
				</div>
				</c:when>
				<c:otherwise>
				<div class="img-circle img-responsive">
				<cms:component uid="section1A-CMSImage-component1-for-profileImagePage" />
				</div>
				</c:otherwise>
				</c:choose>
						<%-- <cms:component uid="account_profile_1"/> --%>
				</div>
				<!-- End Profile Image  -->

				<!-- Heading - My clicks  -->
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					<h2>My Clicks</h2>
					<c:set var="clubCardVal" scope="page"><dateFormat:profile segments="${user.loyalty_segments}"/></c:set>					
					<div class="col-lg-4 col-md-4 col-sm-2 col-xs-4 row">
						<c:if test="${not empty user.memberID}">
							<cms:component uid="section1A-CMSImage-component-for-account_summary_${fn:split(clubCardVal,' ')[0]}_ClubCard"/>
						</c:if>
					</div>
						<!-- <img class="img-responsive pull-left" src="../image/account-land/clubcard.jpg"> -->
						<div class="col-lg-8 col-md-8 col-sm-10 col-xs-8 Clubcard-details">
							<h4 class="dark-blue">Welcome, ${fn:toUpperCase(fn:substring(user.firstName, 0, 1))}${fn:toLowerCase(fn:substring(user.firstName, 1, -1))}!</h4>
							<h4 class="light-blue">${clubCardVal}</h4>
							<c:if test="${not empty user.memberID}">
							<h4 class="grey-font">Card: ${user.memberID}</h4>
							</c:if>
						</div>						
				</div>
				<!-- End Heading - My clicks  -->
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 paddingNone">
				<!-- Cash back - Block1 -->
				<c:if test="${not empty user.memberID}">
					<c:if test="${not empty botswanaCBRAccount or not empty botswanaaccount}">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right cashbackcontainer">
							<div class="cashback-block">
								<h4>Cashback</h4>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<div class="price blue">
											<c:choose>
												<c:when test="${not empty botswanaCBRAccount}">
													R<dateFormat:dateFormat conversionType="rand" input="${botswanaCBRAccount.availableBalance}"/>
												</c:when>
												<c:otherwise>
													R<dateFormat:dateFormat conversionType="rand" input="0.00"/>
												</c:otherwise>
											</c:choose>
										</div>
										<span>available to spend in-store</span>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<c:if test="${not empty botswanaaccount and not empty botswanaaccount.pointsBalance}">
											<div class="price red">
												${botswanaaccount.pointsBalance}
												
											</div>
											<span>points earned </span>
										</c:if> 
										<c:if test="${qualify}">
											<span class="red">
												<c:if test="${not empty botswanaaccount}">
													+${botswanaaccount.pointsToQualify} needed
												</c:if>
											</span>
										</c:if>
										
									</div>
								</div>
								<c:choose>
									<c:when test="${cmsPage.uid eq 'promotionsOffersPage'}">
										<a href="botswana-rewards-activity">See activity breakdown</a>
									</c:when>
									<c:otherwise>
										<a href="my-account/botswana-rewards-activity">See activity breakdown</a>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</c:if>
				
					<!-- End Cash back - Block1 -->
	
					<!-- Cash back - Block2 -->
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right cashbackcontainer">
						<div class="cashback-block flag">
							<h4>Cashback</h4>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="price blue">
										<c:choose>
											<c:when test="${not empty africaCBRAccount}">
												R<dateFormat:dateFormat conversionType="rand" input="${africaCBRAccount.availableBalance}"/>
											</c:when>
											<c:otherwise>
												R<dateFormat:dateFormat conversionType="rand" input="0.00"/>
											</c:otherwise>
										</c:choose>
									</div>
								
										<span>available to spend in-store</span>
								
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<c:if test="${not empty africaaccount and not empty africaaccount.pointsBalance}">
										<div class="price red">
											${africaaccount.pointsBalance}
											
										</div>
										<span>points earned </span>
									</c:if> 
									<c:if test="${qualify}">
										<span class="red">
											<c:if test="${not empty africaaccount}">
												+${africaaccount.pointsToQualify} needed
											</c:if>
										</span>
									</c:if>
								</div>
							</div>
							<c:choose>
								<c:when test="${cmsPage.uid eq 'promotionsOffersPage'}">
									<a href="my-rewards-activity">See activity breakdown</a>
								</c:when>
								<c:otherwise>
									<a href="my-account/my-rewards-activity">See activity breakdown</a>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:if>
					<!-- End Cash back - Block2 -->
			</div>
			</div>
		</div>
	</div>
	
	
	
	<section class="main-container">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="accLandOuter clearfix accountAside">

						<!-- Left side -->
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="filter">

								<!-- Left side first link -->
								<div class="my-account-block bg-white">
									<h2>
										<a href="${request.contextPath}/my-account">
										<spring:theme code="myaccount.summary.title"/>
										</a>
									</h2>
								</div>
								<!-- End Left side first link -->

								<ul class="panel-group" id="accordion">
									<!-- Left Drop down 1 - Accordion -->
										<cms:pageSlot position="SideNavigation" var="feature">
											<cms:component component="${feature}"/>
										</cms:pageSlot>

								</ul>
							</div>
						</div>
						<!-- End Left side -->


						<!-- Right Side Content  -->
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<!-- Promo Block  -->
								<cms:pageSlot position="Section2" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
							<!-- End Promo Block  -->
							<div class="account-body">
								<div class="acc-table-wrap">
									<!-- Recent Orders table  -->
										<cms:pageSlot position="Section3" var="feature">
											<cms:component component="${feature}"/>
										</cms:pageSlot>
								</div> 
								<!-- End Recent Orders table  -->

								<!-- My medicines table  -->
								<%-- Swapnil : Commented out below section as it is part of Phase 2 --%>
<%-- 								<div class="acc-table-wrap">
									<cms:pageSlot position="Section4" var="feature">
											<cms:component component="${feature}"/>
										</cms:pageSlot>
								</div> --%>
								<!-- End My medicine table  -->
								
								<c:set var="flag" value="false" />
								
								<!-- Latest Promotions : Phase 2 development-->
								<c:choose>
									<c:when test="${not empty user.memberID and flag}">
									<div class="contentWrapper clearfix bg-white bottom-marg">
										<div class="clearfix bgred">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<div class="pull-left">
												<h2>Your promotions & offers</h2>
											</div>
											<div class="pull-right text-right">
												<a class="arrow-right" href="javascript:void(0)">See all
													savings</a>
											</div>
										</div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-block">
										<div class="doubleBlockWrapper account-promo">
											<div class="doubleBlock">
												<div class="categoryType">
													Exclusive <span> savings </span>
												</div>
												<h3>Free standard shipping on any order</h3>
												<p>
													Enter <strong>FREEDEL-12345678</strong> at checkout (use
													this once)
												</p>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-block">
										<div class="doubleBlockWrapper odd account-promo">
											<div class="doubleBlock">
												<div class="categoryType">
													Exclusive <span> savings </span>
												</div>
												<h3 class="red">Additional 10% off your entire order</h3>
												<p>
													Enter <strong> FREEDEL-12345678 </strong> at checkout (use
													this once)
												</p>
											</div>
										</div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-block">
										<div class="doubleBlockWrapper">
											<div class="badges blue">
												<span>New</span>
											</div>
											<div class="doubleBlock">
												<div class="categoryType">
													New in <span> from </span> <a href="javascript:void(0)">John
														Freida</a>
												</div>
												<h3>Dream curls daily styling spray</h3>
												<p class="content-height">Donec ullamcorper nulla non
													metus auctor fringilla, etiam porta sem.</p>
												<button class="btn">Shop now</button>
											</div>
											<div class="promoImg">
												<img src="../image/category-1.jpg" alt="category" />
											</div>
										</div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-block">
										<div class="doubleBlockWrapper odd">
											<div class="doubleBlock">
												<div class="categoryType">
													Skincare <span> special offer </span>
												</div>
												<h3 class="red">20% off skincare</h3>
												<p class="content-height">Donec ullamcorper nulla non
													metus auctor fringilla, etiam porta sem.</p>
												<button class="btn">Shop now</button>
											</div>
											<div class="promoImg">
												<img src="../image/category-2.jpg" alt="category" />
											</div>
										</div>
									</div>
									</div>
									</c:when>
									<c:otherwise>
									<div id="globalMessages">
												<common:globalMessages />
											</div>
											
									<c:if test="${empty user.memberID }">
										<div class="myyacc-noclub">
											
												<div class="promo-links clearfix">
													<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 promo-options">
														<h2>Don't miss exclusive savings with clicks ClubCard</h2>
													</div>
													<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 promo-options">
														<div class="promolink-wrap">
																 <!-- <a href="/whatssnew"> Sign in</a> to see all savings or  <a href="promotion">join</a> ClubCard -->
																 <a href="${request.contextPath}/register/joincc">Join ClubCard</a>
														</div>
													</div>
												</div>
											
										</div>
									</c:if>
									</c:otherwise>
								</c:choose>
								
								<!-- Latest Promotions -->
								<!-- Suggestion by clicks : Not in Phase 1 -->
								<c:if test="${not empty user.memberID and flag}">
									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<div class="pull-left">
												<h2>Suggested by Clicks</h2>
											</div>
											<div class="pull-right text-right">
												<a class="arrow-right" href="javascript:void(0)">See all
													suggestions</a>
											</div>
										</div>
									</div>
									<div class="bg-white specialBlock clearfix productRange tb-res bottom-marg">
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="productBlock">
												<div class="thumb">
													<img src="../image/frieda-range-1.jpg" alt="product1" />
												</div>
												<div class="detailContent clearfix">
													<h5>
														<a href="javascript:void(0)">John Frieda</a>
													</h5>
													<div class="content-height">
														<p>Frizz-Ease Miraculous Recovery Cr�me Waves Serum
															50ml</p>
													</div>
													<div class="price-wrap">
														<div class="price">
															28<sup>60</sup>
														</div>
													</div>
													<div class="starWrapper">
														<img src="../image/star.jpg" alt="ratings" /> <a href="#"
															class="btn">Buy</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="productBlock">
												<div class="thumb">
													<img src="../image/frieda-range-2.jpg" alt="product1" />
												</div>
												<div class="detailContent clearfix">
													<h5>
														<a href="javascript:void(0)">John Frieda</a>
													</h5>
													<div class="content-height">
														<p>Luxurious Volume Thickening Hairspray 250ml</p>
													</div>
													<div class="price-wrap">
														<div class="price">
															28<sup>60</sup>
														</div>
														<div class="offer-blk">
															Was <span class="price">R44<sup>60</sup></span> <span
																class="red"> Save </span> <span class="price red">R44<sup>60</sup></span>
														</div>
													</div>
													<div class="starWrapper">
														<img src="../image/star.jpg" alt="ratings" /> <a href="#"
															class="btn">Buy</a>
													</div>
												</div>
											</div>
										</div>
	
	
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="productBlock">
												<p class="badges red">
													<span>Save 20%</span>
												</p>
												<div class="thumb">
													<img src="../image/frieda-range-3.jpg" alt="product1" />
												</div>
												<div class="detailContent clearfix">
													<h5>
														<a href="javascript:void(0)">John Frieda</a>
													</h5>
													<div class="content-height">
														<p>Sheer Blonde Hydration Leave-In Conditioner 200ml</p>
													</div>
													<div class="price-wrap">
														<div class="price">
															28<sup>60</sup>
														</div>
													</div>
													<div class="starWrapper">
														<img src="../image/star.jpg" alt="ratings" /> <a href="#"
															class="btn">Buy</a>
													</div>
													<div class="offerDesc red">
														Save 20% on <span>selected John Frieda</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:if>
								
								<!-- Suggestion by clicks  -->

								<!-- Financial Services start  -->
								
								<c:if test="${not empty user.memberID}">
								
								<cms:pageSlot position="Section7" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
								
								<!-- <div class="clearfix bg-light-blue">
									<div
										class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
										<div class="pull-left">
											<h2>Financial Services</h2>
										</div>
										<div class="pull-right text-right">
											<span> </span>
										</div>
									</div>
								</div>
								<div class="bottom-marg clearfix bg-white fsOuter">
									<div class="row border-bottom">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="fsWrapper fsWrapperOne">
												<h2 class="darkBlue">Personal funeral insurance cover</h2>
												<h3 class="tick">
													You benefit from <span class="red">R892</span> - at no cost!
												</h3>
												<p>Total is updated weekly, last updated 00/00/0000</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="fsWrapper fsWrapperTwo">
												<p>Up to R7,500 Funeral cover - at no cost to you!</p>
												<p>
													Your cover is calculated at 10 times the value of your
													average monthly purchases in Clicks for the 26 weeks prior
													to death. The more you spend the higher your cover - up to
													R7,500. <a href="javascript:void(0)">Find out more</a>
												</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="fsWrapper">
												<div class="fsCover">
													<h2 class="lightGreen">Family funeral insurance cover</h2>
													<p class="tick">
														You're covered. <a href="javascript:void(0)">Find out
															more</a>
													</p>
												</div>
												<div class="fsCover">
													<h2 class="darkPink">Life cover</h2>
													<p>
														Not covered. <a href="javascript:void(0)">Find out more</a>
													</p>
												</div>
												<div class="fsCover">
													<h2 class="orange">Hospital event cover</h2>
													<p>
														Not covered. <a href="javascript:void(0)">Find out more</a>
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="fsWrapper">
												<div class="fsCover">
													<h2 class="lavander">Car and home cover</h2>
													<p>
														Not covered. <a href="javascript:void(0)">Find out more</a>
													</p>
												</div>
												<div class="fsCover">
													<h2 class="magenta">Women-only Cancer and accident
														cover</h2>
													<p>
														Not covered. <a href="javascript:void(0)">Find out more</a>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
 -->								<!-- End Financial services -->

								<!-- Two Club cards  -->
								
								<c:set var="showPartnerDiv" value="false" />
								<cms:pageSlot position="Section8" var="feature" >
									<c:if test="${showPartnerDiv eq false}">
										<c:set var="showPartnerDiv" value="true" />
										<div class="bottom-marg clearfix bg-white">
											<div class="recent-head recent-custom">	
									</c:if>
									<cms:component component="${feature}"/>
								</cms:pageSlot>
								
								<c:if test="${showPartnerDiv eq true}">
									 </div>
								</div> 
								</c:if>
								
								
								<!-- <div class="bottom-marg clearfix bg-white">
									<div class="recent-head recent-custom">
										Cash back block
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											<div class=" clearfix club-card-block">
												<div class="row1">
													<div class="club-img">
														<img class="img-responsive"
															src="../image/account-land/club-1-acc.jpg" alt="club-card">
														<a href="">Find out more</a>
													</div>
													<div class="club-text">
														<h5>Are you a Discovery Health or Vitality member?</h5>
														<span>earn up to 25% cashback on 2,500+ HealthyCare
															products</span>
													</div>
												</div>
											</div>
										</div>
										End Cash back block

										Cash back block
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											<div class="clearfix club-card-block">
												<div class="row1">
													<div class="club-img">
														<img class="img-responsive"
															src="../image/account-land/club-2-acc.jpg" alt="club-card">
														<a href="">Find out more</a>
													</div>
													<div class="club-text">
														<ul>
															<li>Double points on some baby products & clinic
																costs</li>
															<li>Expert advice and vaccination SMS contact</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										End Cash back block
									</div>
								</div>
 -->								<!-- End two club cards -->
 
 													

								<!-- partner slider  -->
								<div class="acc-table-wrap">
									<!-- Partner slider heading  -->
									<%-- <div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-custom">
											<div class="pull-left">
												<h2>Earn ClubCard points and exclusive savings with our
													partners</h2>
											</div>
											<div class="pull-right text-right">
												<a class="arrow-right" href="/clubcardpartnerspage">See all
													partners</a>
											</div>
										</div>
									</div>

															
										<div class="partner-wrap clearfix bg-white">
										<div class="partner-spacing col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<ul class="only-partner-slider slider-component" data-classname="only-partner-slider" data-maxslide="5">
														<cms:pageSlot position="Section9" var="feature">
															<li>
																<div class="col-md-12 col-sm-12 col-xs-12 ">
																	<cms:component component="${feature}" />
																</div>
															</li>
														</cms:pageSlot>
													</ul>
										</div>
									</div>
 --%>
										<!-- Partner slider -->
									<!-- <div class="partner-wrap clearfix bg-white">
										<div
											class="partner-spacing col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<ul class="partner-slider">
												<li>
													
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-2.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-3.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-4.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-5.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-6.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-7.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-8.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-9.jpg" /></a>
													</div>
												</li>
												<li>
													<div class="col-md-12 col-sm-12 col-xs-12 ">
														<a href="#"><img
															src="../image/account-land/acc-partner-10.jpg" /></a>
													</div>
												</li>
											</ul>
										</div>
									</div> -->
								</div>
							</c:if>
								<!-- partner slider  -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</template:page>

	