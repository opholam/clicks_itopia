<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section1A" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section1B" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>

			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix clinic-wrap">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer pharmacy_clinic_outer">
						<cms:pageSlot position="Section2A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
											
				</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="pharma_buttons">
						<cms:pageSlot position="Section2B1" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					<div class="vitSupBlock customer_phone clearfix">
						<cms:pageSlot position="Section2B2" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					<div class="letclicks_help pharma_letclicks bg-white clearfix">
						<cms:pageSlot position="Section2B3" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						<ul>
							<cms:pageSlot position="Section2B4" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</ul>
					</div>
					
					
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pharClinicWrap">
					<div class="bg-white contentContainer clearfix">
					
					<cms:pageSlot position="Section3" var="feature">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<cms:component component="${feature}" />
						</div>
					</cms:pageSlot>
					</div>
				</div>
			</div>
			<%-- <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 space-bottom">
					<div class="clearfix bg-light-blue">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
						<cms:pageSlot position="Section4-Header" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						</div>
					</div>
					<div class="partner-wrap clearfix bg-white">
						<div
							class="partner-spacing col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<ul class="only-partner-slider slider-component"
								data-classname="only-partner-slider" data-maxslide="7">
								<cms:pageSlot position="Section4" var="feature">
									<li><div class="col-md-12 col-sm-12 col-xs-12 ">
											<cms:component component="${feature}" />
										</div></li>
								</cms:pageSlot>
							</ul>
						</div>
					</div>
				</div>
			</div> --%>

			<%-- <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer">
							<cms:pageSlot position="Section5" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					
				</div>
			</div> --%>
			
			
		</div>
	</section>
</template:page>