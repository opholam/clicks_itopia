<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>


<template:page pageTitle="${pageTitle}">

<div id="globalMessages">
	<common:globalMessages/>
</div>
<div class="leaderboardWrapper clearfix">
		<div class="container">
		<c:if test="${not empty article.bottomContent1}">
			<c:set var="name" value="${article.bottomContent1}"/>
			<c:set var="seperSectionIcon" value=""/>
				<c:choose>
					<c:when test="${fn:containsIgnoreCase(name, 'heart')}">
						<c:set var="seperSectionIcon" value="heart"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'flu')}">
						<c:set var="seperSectionIcon" value="flu"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'cancer')}">
						<c:set var="seperSectionIcon" value="cancer"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'hiv')}">
						<c:set var="seperSectionIcon" value="hiv"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'diabetes')}">
						<c:set var="seperSectionIcon" value="diabetes"/>
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				<div class="articlePage"> <a class="${seperSectionIcon} article-view-top arrow-right" href="${article.bottomContent1Url}">${article.bottomContent1}</a> </div>
			</c:if>			
	</div>
</div>
<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<h1 class="main-title">${article.titleName}</h1>
					<p>${article.titleContent}</p>					
						<div class="authorWrapper">
						 <div class="authorPic"><img src="${article.titleIcon.url}" alt="${article.titleIcon.altText}" /></div> 
							<div class="authorInfoWrapper">
								${contentDate} 
								${article.contentBy}
							</div>
						</div>	
					<c:set var ="articleName" value="${fn:replace(article.titleName, '\"', '&#34;')}" />			
					<div class="moreLinks pdp">
						<a class="share" href="javascript:void(0)">Share</a>
						<a class="email" href="javascript:void(0)" onclick="javascript:window.location='mailto:?subject=${articleName}&body=' + window.location;">Email</a>
						<div class="shareProduct a2a_kit social-share">
							<span class="popup-left-arrow"></span>
							<ul>
								<!-- <li><a title="Facebook" target="_blank" class="facebook_icon" href="https://www.facebook.com/"><img /></a></li> -->
								<li><a title="facebook" target="_blank"
									class="a2a_button_facebook icon-sprite icon-video-fb-share facebook_icon"
									href="https://www.facebook.com/"><img />Facebook</a></li>
								<!-- <li><a title="Twitter" target="_blank" class="twitter_icon" href="https://twitter.com/"><img /></a></li> -->
								<li><a title="twitter" target="_blank"
									class="a2a_button_twitter icon-sprite icon-video-tweet-share twitter_icon"
									href="https://twitter.com/"><img />Twitter</a></li>
								<li><a title="Google +" target="_blank"
									class="a2a_button_google_plus icon-sprite icon-video-gp-share google_icon"
									href="https://plus.google.com/"><img />Google +</a></li>
								<!-- <li><a title="Google +" target="_blank" class="google_icon" href="https://plus.google.com/"><img /></a></li> -->
								<!-- 	<li><a title="Email" class="email_icon"></a></li> -->
							</ul>
						</div>
					</div>								
				</div>
				<c:if test="${not empty article.sideImage1}">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="image-center">
							<img src="${article.sideImage1.url}" alt="${article.sideImage1.altText}" />
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix articlePage articleMagaz">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer">
						${article.mainContent} 
					</div>
					<c:if test="${not empty article.bottomContent1}">
						<a class="${seperSectionIcon} article-view-top arrow-right" href="${article.bottomContent1Url}">${article.bottomContent1}</a>
					</c:if>
					
					<c:if test="${not empty article.articleType && article.articleType.code=='MAGAZINES'}">				
						<div class="bg-white">
							<cms:pageSlot position="fbcomments" var="component">
								<cms:component component="${component}" element="div" class="image-center"/>
							</cms:pageSlot>
						</div>
					</c:if>
				</div>
				
				<div class="col-md-4 col-sm-12 col-xs-12">
					<aside>
						<c:if test="${not empty article.sideHeading}">
							<div class="symptomsChecker">
								 <a class="stethoscope arrow-right" href="${article.sideHeadingUrl}">${article.sideHeading}</a>
						 	</div>
						 </c:if>
					 	
						<%-- <cms:pageSlot position="Section1" var="component">
								<cms:component component="${component}" element="div" class="image-center"/>
						</cms:pageSlot> --%>
						<!-- DFP Section starts -->
				
							<cms:pageSlot position="Section2" var="component">
									<cms:component component="${component}"/>
							</cms:pageSlot>
						
						<!-- DFP Section ends -->
						
									
						<c:if test="${not empty article.relatedArticles}">
							<h2 class="bg-blue">${article.relArticleHeading}</h2>
							 <div class="bg-white">						 
								 <c:forEach var="relatedArticle" items="${article.relatedArticles}">						 
								 	<div class="articleSidebar clearfix">
										<div class="articleSideWrapper clearfix">
											<div class="image-center">
												<img src="${relatedArticle.sideImage1.url}" alt="${article.sideImage1.altText}"/>
											</div>
											<c:if test="${relatedArticle.articleType=='HEALTH'}">
												<c:url var = "relatedUrl" value ="/health/article-view/${relatedArticle.articleTitle}"/>
											</c:if>
											<c:if test="${relatedArticle.articleType=='MEDICINES'}">
												<c:url var = "relatedUrl" value ="/health/medicines/article-view/${relatedArticle.articleTitle}"/>
											</c:if>
											<c:if test="${relatedArticle.articleType=='VITAMINS'}">
												<c:url var = "relatedUrl" value ="/health/supplements/article-view/${relatedArticle.articleTitle}" />
											</c:if>
											<c:if test="${relatedArticle.articleType=='CONDITIONS'}">
												<c:url var = "relatedUrl" value ="/health/conditions/article-view/${relatedArticle.articleTitle}" />
											</c:if>
											<c:if test="${relatedArticle.articleType=='MAGAZINES'}">
												<c:url var = "relatedUrl" value ="/clubcard-magazine/article-view/${relatedArticle.articleTitle}" />
											</c:if>
											<h3><a href="${relatedUrl}">${relatedArticle.titleName}</a></h3>
											<p>${relatedArticle.titleContent}</p>
										</div>
									</div>
									<div class="separator"></div>								
								</c:forEach>						
							</div>
						</c:if>
							
					</aside>
				</div>
			</div>
		</div>
	</section>

<script>
$('.dropdown-toggle').dropdown();
</script>

</template:page>