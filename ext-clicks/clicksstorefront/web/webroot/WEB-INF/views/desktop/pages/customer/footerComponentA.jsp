
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<div class="container">
		<div class="subscribe-block">
			<form>
				<fieldset>
					<label for="email">Don't miss out on savings:</label>
					<input type="text" id="email" placeholder="Enter email address" />
					<input type="button" name="subscribe" class="btnSubscribe btn" value="Subscribe"/>
				</fieldset>
			</form>
			<figure class="shareBlock">
				<a href="#" class="icon-twitter">Twitter</a>
				<a href="#" class="icon-fb">Facebook</a>
				<a href="#" class="icon-gplus">Google plus</a>
				<a href="#" class="icon-pinterest">Pinterest</a>
			</figure>
		</div>
	</div>