<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="storepickup"	tagdir="/WEB-INF/tags/desktop/storepickup"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="search" tagdir="/WEB-INF/tags/desktop/search" %>
	
<template:page pageTitle="${pageTitle}">
	<section style="position: relative;" class="main-container">
		<div class="search-results">
			<div class="container">
				<div id="globalMessages">
					<common:globalMessages />
				</div>

				<div class="col-md-12 search-tabs col-xs-12">
					<div class="row title-bar">
						<spring:theme code="search.results" var="searchResults" />
						<h1 class="main-title col-md-12 col-xs-12">${searchResults}</h1>
						<input type="hidden" id="textSearched" value="${searchPageData.freeTextSearch}">
						<input type="hidden" id="articlesSize" value="${articlesSize}">
					</div>
					<div class="row">
						<div class="col-md-12 search-tabs col-xs-12">
							<ul class="nav nav-tabs">
								<li class="col-lg-3 col-md-3 col-sm-3 col-xs-4 active">
									<a	href="#products" data-toggle="tab" aria-expanded="true"><spring:theme code="search.tab.products" text="Products"/> (${searchPageData.pagination.totalNumberOfResults})</a></li>
								<li class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
									<a 	href="#inspiration" data-toggle="tab" aria-expanded="false"><spring:theme code="search.tab.inspiration" text="Articles"/> (${totalNoOfInspirationArticles})</a></li>
								<li class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><a href="#hub" data-toggle="tab"><spring:theme code="search.tab.healthhub" text="Health Hub"/> (${totalNoOfHealthArticles})</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="border-line"></div>
			<div class="search-results-wrap">
				<div class="container">
					<storepickup:pickupStorePopup />
					<div id="products" class="search-out active">
						<div class="filter clearfix">
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="span-6 facetNavigation">
									<%-- <h2 class="bg-white clearfix">Filter ${searchPageData.pagination.totalNumberOfResults} results</h2> --%>
									<div class="panel-group readMoreWrap" id="accordion">
										<cms:pageSlot position="ProductLeftRefinements" var="feature">										
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</div>
							</div>
														
							<div class="col-md-9 col-sm-12 col-xs-12">								
								<cms:pageSlot position="SearchResultsListSlot" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
							
						</div>
					</div>
	
					<search:InspirationData/>
					<search:healthHubData/>
					
				</div>
			
			</div>
		</div>
	</section>
</template:page>
