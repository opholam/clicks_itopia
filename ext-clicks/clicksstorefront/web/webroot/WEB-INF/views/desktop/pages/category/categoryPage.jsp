<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>
	
	<div class="promotion_offer_top clearfix bg-white">
		<div class="container">
			<div class="row featured-wrap">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="product-left">
						<h1>${categoryName}</h1>
						<p>${categoryDescription}</p>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 paddingNone">
				<div class="feature-prod">
					<cms:pageSlot position="Position1" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				</div>
			</div>
		</div>
	</div>

	<section class="main-container cat">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="categoryOuter clearfix">
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="mainCatSidebar readMoreWrap">
								<div class="bg-white categorySidebar readMoreChild">
									<h2>In ${categoryName}</h2>
									<c:choose>
									<c:when test="${subCategories.size() > 0}">
									<div class="allFacetValues">
									<ul class="facet_block">
										<c:forEach items="${subCategories}" var="category">
										<c:if test="${(fn:startsWith(categoryCode, 'OH1') and fn:startsWith(category.code, 'OH2')) or (fn:startsWith(categoryCode, 'OH2') and fn:startsWith(category.code, 'OH3')) or fn:startsWith(categoryCode, 'OH3')}">
											
											<c:choose>
											<c:when test="${fn:startsWith(categoryCode, 'OH2')}">
											<c:url value="${contextPath}/c/${categoryCode}?q=%3Arelevance%3Acategory%3A${category.code}"
													var="categoryUrl" /> 
											</c:when>
											<c:otherwise>
											<c:url value="${contextPath}/c/${category.code}"
													var="categoryUrl" />
											</c:otherwise>
											</c:choose>
											<li><a href="${categoryUrl}">${category.name}</a></li>
										</c:if>
										</c:forEach>
									</ul>
									<button class="read-more-facet" >See more</button>
     								<button class="read-less-facet" >See less</button>
									</div>
									</c:when>
									<c:otherwise><span class="norecords " >Related categories are not available.</span></c:otherwise>
									</c:choose>
								</div>
								<div class="bg-white categorySidebar readMoreChild">
									<h2>Featured brands</h2>
									<ul>
										<cms:pageSlot position="Position2" var="feature">
										<li>
										<cms:component component="${feature}" />
										</li>
										</cms:pageSlot>
									</ul>
									<div class="allFacetValues">
									<ul class="facet_block">
										<%-- <cms:component component="${feature}" /> --%>
										<c:forEach items="${facetValDataList}" var="facetData">
										<c:if test="${not fn:containsIgnoreCase(facetData.name, 'brands')}">
										<li><a href="${contextPath}/c/${categoryCode}?q=%3Arelevance%3Abrand%3A${facetData.code}">${facetData.name}</a></li>
										</c:if>
										</c:forEach>
									</ul>
									<button class="read-more-facet" >See more</button>
     								<button class="read-less-facet" >See less</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 categoryPage">
							<c:if test="${fn:containsIgnoreCase(cmsPage.uid, 'health')}">
								<div class="clinic-links clearfix">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border-right">
										<cms:pageSlot position="SectionCategoryPage4" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<cms:pageSlot position="SectionCategoryPage5" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</div>
							</c:if>
							<div class="contentWrapper clearfix bgred">
								<div
									class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titleWrapper">
									<cms:pageSlot position="Position4" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
							
							<!-- Latest promotion carousel -->
							<div class="category_carousel">								
								<cms:pageSlot position="Position5" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
							<!-- Latest promotion carousel -->
							
							<div class="home-promotions categoryLanding clearfix bg-white bottom-marg">
								<cms:pageSlot position="Position6" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
							<c:if test="${fn:containsIgnoreCase(cmsPage.uid, 'health')}">
								<div class="pharmacy-links">
									<ul class="clinic-links clearfix">
										<cms:pageSlot position="SectionCategoryPage6" var="feature">
											<div
												class="col-lg-3 col-md-3 col-sm-6 col-xs-12 border-right">
												<cms:component component="${feature}" />
											</div>
										</cms:pageSlot>
<%-- 										<cms:pageSlot position="SectionCategoryPage7" var="feature">
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
												<cms:component component="${feature}" />
											</div>
										</cms:pageSlot> --%>
									</ul>
								</div>
							</c:if>
							<div class="moreCategory home-clubcard clearfix">
								<div class="clearfix">
									<cms:pageSlot position="Position7" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<div class="home-club-wrap clearfix">
									<cms:pageSlot position="Position8" var="feature">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
											<cms:component component="${feature}" />
										</div>
									</cms:pageSlot>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- <script>
		ga('send', 'pageview');
	</script> -->
</template:page>
