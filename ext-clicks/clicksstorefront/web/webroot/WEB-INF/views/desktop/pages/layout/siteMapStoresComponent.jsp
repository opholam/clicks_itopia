<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<c:if test="${not empty pos}">
<c:forEach items="${pos}" var="store" varStatus="i">
	<c:url var="url" value = "${store.url}"/>
	<li><a href="${url}">${store.displayName}</a></li>
</c:forEach>
</c:if>