<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/desktop/nav/breadcrumb"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<c:set var="add-address" value="/my-account/add-address"/>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<div class="saved-addr-wrap">
		<div class="bg-white clearfix top-margin">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<!-- <h2>Change Password</h2> -->
				<h2>
					<spring:theme code="text.savedaddresses.title"
						text="Saved addresses" />
				</h2>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="pull-right">
					<ycommerce:testId code="addressBook_addAddress_button">				
					<input type="hidden" class="addnewurl" value="${request.contextPath}/my-account/saved-addresses"/>
					<a class="button addNewAdd" href="javascript:void(0);"> <spring:theme code="text.addNewAddress.title" text="Add new address" /> </a>
					</ycommerce:testId> 
				</div>
			</div>
		</div>

		<div class="bg-white clearfix top-margin">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 address-wrap">
				<div class="address-details">
					<div class="row">
						<c:choose>
							<c:when test="${not empty addressData}">
								<c:forEach items="${addressData}" var="address">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone address-details-sections">
									<ycommerce:testId code="addressBook_address_label">

										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
											<h4>${fn:escapeXml(address.country.name)}</h4>
										</div>

										<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
											<span> ${fn:escapeXml(address.line1)},
												${fn:escapeXml(address.line2)},${fn:escapeXml(address.town)},${fn:escapeXml(address.postalCode)}
											</span>	 
											<!--<ycommerce:testId code="addressBook_editAddress_button">
												<a class="button" href="edit-address/${address.id}"> <spring:theme
														code="text.edit.addresses" text="Edit these details" />
												</a>
											</ycommerce:testId>  -->											
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
											<input type="hidden" name="addressCode" value="${address.id}"/>
                    									 <a href="${request.contextPath}/my-account/saved-addresses/edit-address/${address.id}" class="edit negative remove-payment-item left" tabindex="${status.count + 22}"> <spring:theme
														code="text.edit.addresses" text="Edit these details" />
														</a>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-1 col-xs-4">
											<ycommerce:testId code="addressBook_removeAddress_button">
											<a class="button"
														data-address-id="${address.id}"
														href="remove-address/${address.id}"> <spring:theme code="text.delete" text="Delete" />
													</a>
											</ycommerce:testId>
											</div>
										</ycommerce:testId>
								

									<div style="display: none">
										<div id="popup_confirm_address_removal_${address.id}">
											<div class="addressItem">
												<ul>
													<li>${fn:escapeXml(address.title)}&nbsp;${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}</li>
													<li>${fn:escapeXml(address.line1)}</li>
													<li>${fn:escapeXml(address.line2)}</li>
													<li>${fn:escapeXml(address.town)}</li>
													<li>${fn:escapeXml(address.region.name)}</li>
													<li>${fn:escapeXml(address.postalCode)}</li>
													<li>${fn:escapeXml(address.country.name)}</li>
												</ul>

												<spring:theme code="text.adress.remove.confirmation"
													text="Are you sure you would like to delete this address?" />
												</a>

												<div class="buttons">
													<a class="button removeAddressButton"
														data-address-id="${address.id}"
														href="remove-address/${address.id}"> <spring:theme
															code="text.yes" text="Yes" />
													</a> <a class="button closeColorBox"
														data-address-id="${address.id}"> <spring:theme
															code="text.no" text="No" /></a>
												</div>
											</div>
										</div>
									</div>
									</div>
								</c:forEach>

							</c:when>
							<c:otherwise>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<p class="emptyMessage">
									<spring:theme code="text.account.addressBook.noSavedAddresses" />
								</p>
							</div>
							</c:otherwise>
						</c:choose>

					</div>
				</div>

			</div>
		</div>	
	</div>
	<div class="bg-white clearfix savedAddnew">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class=" form-wrap">
			<!--  <div class="form-group" >
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 paddingNone">
								<input id="address.postcode" type="text" name="postcode" placeholder="Suburb or Postcode" value="${addressForm.postcode}" class="text storeSearchPostCode" />
								</div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 paddingNone">
								<div class="btn btn-save-change" id="deliveryAreaSearchButton" >Search</div>
								</div>
								</div>
								<div  id="isDeliverableDiv" >
								
			</div> -->
			<form:form method="post" commandName="addressForm">
	<form:hidden path="addressId" class="add_edit_delivery_address_id" status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" id="deliveryAddressForm"/>
	<input type="hidden" name="bill_state" id="address.billstate"/>
	<form:hidden path="selectedDeliveryMode" id="selectedDeliveryMode"/>
	<form:hidden path="countryIso" />
	
<%-- 	<formElement:formSelectBox idKey="address.country"
		                           labelKey="address.country"
		                           path="countryIso"
		                           mandatory="true"
		                           skipBlank="false"
		                           skipBlankMessageKey="address.selectCountry"
		                           items="${supportedCountries}"
		                           itemValue="isocode"
		                           selectedValue="${addressForm.countryIso}"/> --%>
		<div class="form-group clearfix">	<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="text" mandatory="true"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="text" mandatory="false"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.Suburb" labelKey="address.Suburb" path="suburb" inputCSS="readonlyField" mandatory="true"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="text" /></div>
		<div class="form-group clearfix"><formElement:formSelectBox idKey="address.province" labelKey="address.province" path="province"  items="${province}" /></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.country" labelKey="address.country" path="countryName" inputCSS="readonlyField" mandatory="true" /></div>
	<div class="form-group clearfix"><formElement:formInputBox idKey="address.postcode.show" labelKey="address.postcode" path="postcode" inputCSS="text" mandatory="true"/></div>
<%-- 		<div class="form-group clearfix"><formElement:formInputBox idKey="address.postcode.show" labelKey="address.postcode" path="postcode" inputCSS="text" mandatory="true"/></div> --%>
	
	<div class="form-group clearfix"><formElement:formInputBox idKey="address.deliveryInstructions" labelKey="address.deliveryInstructions" path="deliveryInstructions" inputCSS="text"  /></div>
	<%-- <div class="form-group clearfix"><formElement:formSelectBox idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}" selectedValue="${addressForm.titleCode}"/></div> --%>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix"><formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" mandatory="true" /></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.alternatePhone" labelKey="address.alternatePhone" path="alternatePhone" inputCSS="text" /></div>
	
	<%--<div class="form-group clearfix"><label class="control-label">Preferred contact method</label>
	<form:radiobutton path="preferredContactMethod" id="address.preferredContactMethod" label="Email"  value="0" />
	<form:radiobutton path="preferredContactMethod" id="address.preferredContactMethod" label="SMS"  value="1" />
	</div>  --%>
	<div id="deliveryMethodsDiv" >
	<c:if test="${not empty deliveryModes}">
	<div id="deliveryMethodsOuterDiv" ><div class="delMethodRate"><h2>Delivery Method <strong class="pull-right">Cost</strong></h2>
	<c:forEach items="${deliveryModes}" var="deliveryMode">
		<div class="delivery_method_item"><input type="radio" name="delivery_method" id="${deliveryMode.code}" value="${deliveryMode.code}"/><label for="${deliveryMode.code}">${deliveryMode.name}&nbsp;&nbsp;<span>${deliveryMode.description}</span></label>&nbsp;&nbsp;<div class="price pull-right">${deliveryMode.deliveryCost.formattedValue}</div></div>
	</c:forEach>
		</div>
		</div>
	</c:if>
</div>
	<%-- <div id="i18nAddressForm" class="i18nAddressForm">
		<c:if test="${not empty country}">
			<address:addressFormElements regions="${regions}"
			                             country="${country}"/>
		</c:if>
		
	</div> 
	
	<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
	<div class="form-additionals">
		<c:choose>
			<c:when test="${showSaveToAddressBook}">
				<formElement:formCheckbox idKey="saveAddressInMyAddressBook" labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook" path="saveInAddressBook" inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
			</c:when>
			<c:when test="${not addressBookEmpty && not isDefaultAddress}">
				<formElement:formCheckbox idKey="defaultAddress" labelKey="address.default" path="defaultAddress"
				                          inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
			</c:when>
		</c:choose>
		
	</div>
	</sec:authorize> --%>

	<div id="addressform_button_panel" class="form-actions">
			
				
				<c:choose>
					<c:when test="${edit eq true}">
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button show_processing_message" type="submit">
								<spring:theme code="checkout.multi.saveAddress" text="Save address"/>
							</button>
						</ycommerce:testId>
					</c:when>
					<c:otherwise>
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button show_processing_message" type="submit">
								<spring:theme code="checkout.multi.deliveryAddress.clicks.continue" text="Continue"/>
							</button>
						</ycommerce:testId>
					</c:otherwise>
				</c:choose> 
				<c:if test="${not noAddress}">
					<ycommerce:testId code="multicheckout_cancel_button">
						<c:url value="${cancelUrl}" var="cancel"/>
							<a class="btn btn-cancel" href="${request.contextPath}/my-account/saved-addresses"><spring:theme code="checkout.multi.cancel" text="Cancel"/></a>
					</ycommerce:testId>
				</c:if>
			</div>
			</form:form>
			</div>
			</div>
			</div>
</div>