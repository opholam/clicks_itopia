<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<cart:cartExpressCheckoutEnabled />
<a href="${continueShoppingUrl}" class="btn primary_btn contShop">
	<spring:theme text="Continue Shopping" code="cart.page.continue"/>
</a>
<cite><spring:theme code="basket.page.button.or"/></cite>
<button id="checkoutButtonBottom" class="doCheckoutBut positive btn primary_btn" type="button" data-checkout-url="${checkoutUrl}">
	<spring:theme code="checkout.checkout" />
</button>