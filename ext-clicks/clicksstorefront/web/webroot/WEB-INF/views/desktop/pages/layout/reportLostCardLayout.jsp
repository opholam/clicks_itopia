<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
	
		<div class="clearfix bg-white createAccountOuter">
		<div class="container">
		
		
				<div class="contact-wrap-top">

				<cms:pageSlot position="Section1" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
				
				</div>
				<!-- End Heading - My clicks  -->
		</div>
	</div>
	<section class="main-container">
		<div class="createOuterWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="contactOuter clearfix ">

						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
							
														
							<div class="contact-det bg-white clearfix">
								
								<!-- SECTION 2A -->
								<cms:pageSlot position="Section2A" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>

							</div>
							<!-- Customer service block -->

						
							
							<!-- Send message block -->
								<!-- SECTION 2B -->
								
								<cms:pageSlot position="Section2B" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
							
							<!-- Send message block -->
						</div>
						
						
						<!-- Right Side Content start -->
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							
							<!-- Browse help section starts -->
							
							<!-- SECTION 3A -->
							
							<!--<div class="contact-det bg-white clearfix ">
								<h3>Browse our help section</h3>
								<p>Find answers to your questions quickly online in our comprehensive Help Section.</p>
								<button class="btn btn-save-change">Browse help</button>
							</div>-->
							<div class="contact-det bg-white clearfix ">
								<cms:pageSlot position="Section3A" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
 							</div>
 							
							<!-- SECTION 3B -->
							
								<cms:pageSlot position="Section3B" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>		
						
						</div>
						<!-- Right Side Content end -->
					</div>
				</div>
			</div>
		</div>
	</section>	
</template:page>