
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- <%@ taglib prefix="clubCard" tagdir="/WEB-INF/tags/customer" %> --%>


<c:url
	value="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/baby/my-account/join-babyclub"
	var="registerActionUrl" />


<form:form method="post" commandName="joinBabyClubForm"
	action="${registerActionUrl}" id="aboutFamily" cssClass="clearfix">
	<%-- <form action="#" id="aboutFamily" class="clearfix"> --%>
<form:hidden path="alreadyBabyClubcardMember" />
	<div class="form-group clearfix topCats">
		<form:radiobutton path="pregnancyStatus" id="currentPregnant"
			value="yes" cssClass="radio-hide" />
		<form:label path="" cssClass="radio-box current" for="currentPregnant">
			<span>Currently pregnant</span>
		</form:label>
		<!-- <input type="radio" id="currentPregnant" class="radio-hide"	name="currentPregnant">
		 				<label class="radio-box current" for="parent"><span>Currently pregnant</span></label> -->
	</div>
	<div class="topCatsContainer">
		<label class="date error">Please select the date</label>
		<div class="form-group clearfix">
			<form:label path="" for="">Estimated date of delivery</form:label>
			<spring:bind path="estimatedDeliveryDate">
				<c:if test="${not empty estimatedDeliveryDate}"> ${estimatedDeliveryDate} </c:if>
				<form:input path="estimatedDeliveryDate" id="datepicker"
					cssClass="text" />

				<form:label path="estimatedDeliveryDate" id="datepicker"
					for="datepicker" cssClass="btnDate">Estimated date of delivery</form:label>
			</spring:bind>
		</div>
		<div class="form-group clearfix">
			<!-- <label>Is it a boy or a girl?</label> -->
			<form:label path="">Is it a boy or a girl?</form:label>
			<spring:bind path="expectedGender">
				<div class="gender">
					<form:select path="expectedGender" id="basic3">
						<form:option value="Unknown">Don't know yet</form:option>
						<form:option value="Boy">Boy</form:option>
						<form:option value="Girl">Girl</form:option>
					</form:select>
					<!--<select id="basic3">
					<option value="Unknown">Don't know yet</option>
					<option value="Boy">Boy</option>
					<option value="Girl">Girl</option>
				</select> -->
				</div>
			</spring:bind>
		</div>
	</div>
	<div class="form-group clearfix allCats">
		<form:radiobutton path="pregnancyStatus" id="alreadyParent" value="no"
			cssClass="radio-hide" />
		<form:label path="" cssClass="radio-box" for="alreadyParent">
			<span>Already a parent</span>
		</form:label>
		<!-- <input type="radio" id="alreadyParent" class="radio-hide" name="alreadyParent"/> 
				<label class="radio-box" for="parent"><span>Already a parent</span></label> -->
	</div>
	<div class="allCatsContainer">
		<%-- <div class="form-group clearfix">
			<formElement:formInputBox idKey="joinBabyClubForm.firstName" labelCSS="fnameLabel" labelKey="label.joinBabyClubForm.firstName" path="firstName" inputCSS="form-control fName" mandatory="true" />
		</div>
		
		<div class="form-group clearfix">
			<formElement:formInputBox idKey="joinBabyClubForm.lastName" labelCSS="surLabel" labelKey="label.joinBabyClubForm.lastName" path="lastName" inputCSS="form-control sName" mandatory="true" />
		</div>
		<div class="form-group clearfix">
			<form:label path="" class="genderLabel">Gender</form:label>
			<div class="gender">
				<form:select path="gender" class="genderSelect basic" id="">
					<form:option value="Boy">Boy</form:option>
					<form:option value="Girl">Girl</form:option>
				</form:select>
			</div>
		</div> --%>
		<div class="clone_wrap"> 
		<c:choose>
		<c:when test="${empty joinBabyClubForm.children}">
		
		<div class="clonedInput">
		<div class="form-group clearfix">
						<h2 id="reference" name="reference" class="heading-reference">Child
							#1</h2>
						<a href="javascript:void(0)" class="btnDel">close</a>
					</div>
					<%-- ${status.count} --%>
					<div class="form-group clearfix">
						<label><spring:theme
								code="customer.enroll.child.FirstName" text="First name" /></label> <input
							name="children[0].firstName" type="text"
							value="${child.firstName}" class="fName" />
					</div>
					<div class="form-group clearfix">
						<label><spring:theme code="customer.enroll.child.Surname"
								text="Last name" /></label> <input
							name="children[0].lastName" type="text"
							value="${child.lastName}" class="sName" />
					</div>
					<%--  <input name="children[${status.index}].gender" value="${child.gender}"/>
	             <input name="children[${status.index}].day" value="${child.day}"/>
	              <input name="children[${status.index}].month" value="${child.month}"/>
	               <input name="children[${status.index}].year" value="${child.year}"/>
	               --%>
					<div class="form-group clearfix">
						<form:label path="" class="genderLabel">Gender</form:label>
						<div class="gender">
							<form:select path="children[0].gender"
								class="genderSelect basic" id="">
								<form:option value="Boy">Boy</form:option>
								<form:option value="Girl">Girl</form:option>
							</form:select>
						</div>
					</div>
					<div class="form-group clearfix">
						<form:label path="" class="dobLab">Date of Birth</form:label>
						<div id="datePickerContainer">
						<input type="text" class="form-control childrenDob" id="children0" name="children[0].date" value="${child.date}">
						<label for="children${status.index}" class="btnDate"></label>
						<!-- <input type="hidden" name="date" class="children${status.index}" value="">  -->
						</div>
					</div>
					</div>
		
		</c:when>
		<c:otherwise>
		
		<c:forEach items="${joinBabyClubForm.children}" var="child" varStatus="status">			
			
				<div class="clonedInput">
					<div class="form-group clearfix">
						<h2 id="reference" name="reference" class="heading-reference">Child
							#${status.count}</h2>
						<a href="javascript:void(0)" class="btnDel">close</a>
					</div>
					<%-- ${status.count} --%>
					<div class="form-group clearfix">
						<label><spring:theme
								code="customer.enroll.child.FirstName" text="First name" /></label> <input
							name="children[${status.index}].firstName" type="text"
							value="${child.firstName}" class="fName" />
					</div>
					<div class="form-group clearfix">
						<label><spring:theme code="customer.enroll.child.Surname"
								text="Last name" /></label> <input
							name="children[${status.index}].lastName" type="text"
							value="${child.lastName}" class="sName" />
					</div>
					<%--  <input name="children[${status.index}].gender" value="${child.gender}"/>
	             <input name="children[${status.index}].day" value="${child.day}"/>
	              <input name="children[${status.index}].month" value="${child.month}"/>
	               <input name="children[${status.index}].year" value="${child.year}"/>
	               --%>
					<div class="form-group clearfix">
						<form:label path="" class="genderLabel">Gender</form:label>
						<div class="gender">
							<form:select path="children[${status.index}].gender"
								class="genderSelect basic" id="">
								<form:option value="Boy">Boy</form:option>
								<form:option value="Girl">Girl</form:option>
							</form:select>
						</div>
					</div>
					<div class="form-group clearfix">
						<form:label path="" class="dobLab">Date of Birth</form:label>
						<%-- <div class="dob child-dob">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
								<div class="dobWrapper">
									<formElement:DOBformSelectBox selectCSSClass="dob_day"
										idKey="customer.create.account.SA_DOB_day"
										labelKey="customer.create.account.SA_DOB_day"
										path="children[${status.index}].day"
										selectedValue="${child.day}" mandatory="true" items="${days}" />
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
								<div class="monthWrapper">
									<formElement:DOBformSelectBox selectCSSClass="dob_month"
										idKey="customer.create.account.SA_DOB_month"
										labelKey="customer.create.account.SA_DOB_month"
										path="children[${status.index}].month" mandatory="true"
										selectedValue="${child.month}" items="${months}" />
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
								<div class="yearWrapper">
									<formElement:DOBformSelectBox selectCSSClass="dob_year"
										idKey="customer.create.account.SA_DOB_year"
										labelKey="customer.create.account.SA_DOB_year"
										path="children[${status.index}].year" mandatory="true"
										items="${years}" selectedValue="${child.year}" />
								</div>
							</div>
						</div> --%>
						<div id="datePickerContainer">
						<input type="text" class="form-control childrenDob" id="children${status.index}" name="children[${status.index}].date" value="${child.date}">
						<label for="children${status.index}" class="btnDate"></label>
						<!-- <input type="hidden" name="date" class="children${status.index}" value="">  -->
						</div>
					</div>
				</div>  
		</c:forEach>
		</c:otherwise>
		</c:choose>
			</div>
		<a href="javascript:void(0)" id="addChild"> Add another child</a>
	</div>
	
	<c:set var="value" value="${isUpdate}" />
	<c:if test="${fn:containsIgnoreCase(value, 'yes')}">
	<button class="btn" id="saveBaby">UPDATE MY DETAILS</button>
	</c:if>
	<c:if test="${fn:containsIgnoreCase(value, 'no')}">
	<button class="btn" id="saveBaby">JOIN BABYCLUB</button>
	</c:if>
</form:form>




