<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
 	
 
<c:url var="profileUrl" value="/my-account/change-password" />
<!-- Right Side Content  -->
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<div class="changePassword-wrap">
			<form:form action="${profileUrl}" method="post" commandName="updatePasswordForm" autocomplete="off">
			<div class="row ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class=" bg-white  clearfix">
						<div class="clearfix bg-light-blue">
							<div
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
								<!-- <h2>Change Password</h2> -->
								<h2><spring:theme code="change.password.title"/></h2>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 clearfix">
	 						<div class="row">
								<div class="pad-spacing form-wrap">
									<formElement:formPasswordBox idKey="profile.currentPassword" 	labelKey="profile.currentPassword" 		path="currentPassword" 	inputCSS="form-control"  mandatory="true" />
									<formElement:formPasswordBox idKey="profile-newPassword" 		labelKey="profile.newPassword" 			path="newPassword" 		inputCSS="form-control"  mandatory="true"/>
									<formElement:formPasswordBox idKey="profile.checkNewPassword" 	labelKey="profile.checkNewPassword" 	path="checkNewPassword" inputCSS="form-control"  mandatory="true"/>
									<button class="btn btn-save-change"><spring:theme code="profile.button.title"/></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form:form>
	</div>
</div>
<!-- End Right side Conent -->
