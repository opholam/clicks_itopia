<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	
		<div class="healthTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 paddingLeftNone">
					<cms:pageSlot position="Section1A" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
	 			</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="fb-like-page">
					<cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix hLandingPage">
		<div class="container">
			<!-- <div class="row">
				<div class="bg-white clearfix travels-heading-outer">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<cms:pageSlot position="Section2A" var="feature">
							<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
				</div>
			</div> -->
			<div class="row">
				<div class="bg-white clearfix travels-heading-outer">
					<div class="col-lg-12">
						<cms:pageSlot position="Section2B" var="feature">
							<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
				</div>
			</div>
		<!-- 	<div class="row">
				<div class="commonTab healthTab">
					<div class="tabContentWrapper healthTabWrapper tb-res">
						<div class="travelContent">
						<div class="active row" id="tab1">
							<div class="row healthTabOuter">						
								<div class="col-lg-12">
									<div class="active-loadmore-tab-outer">
									<input id="totalNoOfTravelDeals" type="hidden" value="${travelDeals.size()}">								
									<input id="maxNoOfTraveldealsShown" type="hidden" value="${maxNoOfTraveldealsShown}">
									<input id="contextPath" type="hidden" value="${contextPath}">
									<c:forEach items="${travelDeals}" var="travelDeal" begin="0" end="${maxNoOfTraveldealsShown}">
										<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 appendTravelDeals">
											<div class="travelOuterWrapper">
												<div class="tabThumbImage">
													<img src="${travelDeal.picture.downloadURL}" alt="${travelDeal.picture.altText}" />
												</div>
												<div class="travelRateTitle clearfix"> <h4><a href="${request.contextPath}/clubcardtravel/traveldeals/${travelDeal.referenceNo}">${travelDeal.travelLocation}</a></h4>
													<div class="travelRateOuter clearfix"><span>From</span><div class="travelRate">${travelDeal.fromPrice}</div></div>
												</div>
													<h5>${travelDeal.name}</h5>
													<div class="greyBox">Travel between <strong>
													<fmt:formatDate value="${travelDeal.startDate}" /> and <fmt:formatDate value="${travelDeal.endDate}" /></strong></div>
												<p>${travelDeal.description}</p>
												<div class="referenceTravel">Reference No: ${travelDeal.referenceNo}</div>
											</div>
										</div>
										</c:forEach>
									</div>
								</div>
							</div>
							<div class="text-center load-more-travel-deals">
								<button class="btn btnBig">LOAD MORE DEALS</button>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</section>
</template:page>