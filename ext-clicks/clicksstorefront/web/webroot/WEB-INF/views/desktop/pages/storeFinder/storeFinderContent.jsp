<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>


<%-- <c:url value='/store-finder/position?feature=all' var="searchUrlAll" /> --%>
<c:url value="/store-finder/position?feature=all'" var="searchUrlAll" />
<c:url value='/store-finder/position?feature=clinic' var="searchUrlCl" />
<c:url value='/store-finder/position?feature=pharmacy' var="searchUrlPh" />

<%-- <c:url value="/store-finder" var="storeFinderFormAction" />
<c:url value="/store-finder/position" var="nearMeStorefinderFormAction"/> --%>

<c:if test="${not empty param.q}">
	<c:url value='/store-finder?q=${param.q}&feature=all'
		var="searchUrlAll" />
	<c:url value='/store-finder?q=${param.q}&feature=clinic'
		var="searchUrlCl" />
	<c:url value='/store-finder?q=${param.q}&feature=pharmacy'
		var="searchUrlPh" />

</c:if>
<c:if test="${not empty geoPoint.latitude}">
	<c:url value='/store-finder?q=${param.q}&feature=all'
		var="searchUrlAll" />
	<c:url value='/store-finder?q=${param.q}&feature=clinic&latitude=${geoPoint.latitude}&longitude=${geoPoint.longitude}'
		var="searchUrlCl" />
	<c:url value='/store-finder?q=${param.q}&feature=pharmacy&latitude=${geoPoint.latitude}&longitude=${geoPoint.longitude}'
		var="searchUrlPh" />

</c:if>

<div class="store-locator">
	<store:storeSearch errorNoResults="${errorNoResults}" />



	<c:choose>
		<c:when
			test="${searchPageData ne null and !empty searchPageData.results}">



			<div class="store-finder">
				<div class="container">
					<div class="row">

						<div class="col-md-6 store-map">
							<ul class="nav nav-tabs">
								<c:choose>
									<c:when test="${not empty param.q}">
										<li class="active"><a href="${searchUrlAll}">All</a></li>
									</c:when>
									<c:otherwise>
										<li class="active"><form:form id="nearMeStorefinderForm"
												name="near_me_storefinder_form" method="POST"
												action="${searchUrlAll}">
												<input type="hidden" id="latitude" name="latitude" />
												<input type="hidden" id="longitude" name="longitude" />
												<a id="findStoresNearMe" class="btn" type="submit">All</a>
											</form:form></li>
									</c:otherwise>
								</c:choose>
								<li><a href="${searchUrlPh}">With pharmacy</a></li>
								<li><a href="${searchUrlCl}">With clinic</a></li>
							</ul>

							<store:storesMap storeSearchPageData="${searchPageData}" />

						</div>



						<div class="col-md-6">


							<store:storeListForm searchPageData="${searchPageData}"
								locationQuery="${locationQuery}"
								numberPagesShown="${numberPagesShown}" geoPoint="${geoPoint}" />

						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="store-finder bg-white">
				<div class="container">
						<div id="globalMessages">
							<common:globalMessages />
						</div>
				</div>
			</div>

		</c:otherwise>
	</c:choose>


</div>