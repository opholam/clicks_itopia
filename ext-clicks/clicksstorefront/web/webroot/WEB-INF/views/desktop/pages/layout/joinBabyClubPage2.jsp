<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form action="${contextPath}/my-account/join-babyclub" method="POST" commandName="joinBabyClubForm" cssClass="clearfix">
	<spring:bind path="pregnancyStatus">
		<div class="form-group clearfix topCats">
			<label class="radio-box current" for="currentPregnant">
			<form:radiobutton id="currentPregnant" path="pregnancyStatus"  value="yes" />
			<span>Currently pregnant</span></label>
		</div>
	</spring:bind>
	<div class="topCatsContainer"> 
	<spring:bind path="estimatedDeliveryDate">
		<div class="form-group clearfix">
			<label>Estimated date of delivery</label>		
			<form:input path="estimatedDeliveryDate" id="datepicker" cssClass="text"/>					
			<label for="datepicker" class="btnDate"></label>
		</div>
		</spring:bind>
		<div class="form-group clearfix">
			<label>Is it a boy or a girl?</label>							
			<spring:bind path="expectedGender">
			<div class="gender">
				<form:select id="basic3" path="expectedGender">
					<form:option value="Unkown">Do not know yet</form:option>
					<form:option value="Boy">Boy</form:option>
					<form:option value="Girl">Girl</form:option>
				</form:select>
			</div>
			</spring:bind>
		</div>
	</div>
	<div class="form-group clearfix allCats">
	<spring:bind path="pregnancyStatus">
			<label class="radio-box" for="alreadyPregnant">
			<form:radiobutton id="alreadyPregnant" path="pregnancyStatus" value="no" />
			<span>Already a parent</span></label></spring:bind>
	</div>
	<div class="allCatsContainer">
	<spring:bind path="firstName">
	<div class="form-group clearfix">
		<label>First name</label>							
		<form:input type="text" value="" cssClass="text" id="firstName" path="firstName"/>
	</div>
	</spring:bind>
	<spring:bind path="lastName">
	<div class="form-group clearfix">
		<label>Surname</label>							
		<form:input type="text" value="" cssClass="text" id="lastName" path="lastName"/>
	</div>
	</spring:bind>
	<spring:bind path="gender">
	<div class="form-group clearfix">
		<label>Gender</label>							
		<div class="gender">
		<form:select path="gender" cssClass="basic2">
			<form:option value="boy">Boy</form:option>
			<form:option value="girl">Girl</form:option>
		</form:select>
		</div>
	</div>
	</spring:bind>
	<div class="form-group clearfix">
		<label>Date of Birth</label>							
		<div class="dob">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
			<spring:bind path="day">
				<div class="dobWrapper">
				<form:select path="day" cssClass="basic2">
					<form:option value="1">1</form:option>
					<form:option value="2">2</form:option>
				</form:select>
				</div>
			</spring:bind>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
				<spring:bind path="month">
					<div class="monthWrapper">
					<form:select path="month">
						<form:option value="Jan">Jan</form:option>
						<form:option value="Feb">Feb</form:option>
						<form:option value="Mar">Mar</form:option>
						<form:option value="April">Apr</form:option>
						
					</form:select>
					</div>
				</spring:bind>
			</div>	

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
				<spring:bind path="year">
				<div class="yearWrapper">
				<form:select path="year">
					<form:option value="2010">2010</form:option>
					<form:option value="2011">2011</form:option>
					<form:option value="2012">2012</form:option>
					<form:option value="2013">2013</form:option>
				</form:select>
				</div>
				</spring:bind>
			</div>
		</div>
	</div>
	</div>
	<button class="btn">Join Babyclub</button>
</form:form>