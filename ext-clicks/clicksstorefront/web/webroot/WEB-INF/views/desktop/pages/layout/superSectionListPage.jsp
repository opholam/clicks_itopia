<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<template:page pageTitle="${pageTitle}">
					<%-- <cms:pageSlot position="SectionConditionList5" var="feature" element="div">
						<cms:component component="${feature}" />
					</cms:pageSlot> --%>
					<div class="healthTop">
						<div class="container">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
											<h1 class="main-title">
											<cms:pageSlot position="Section1" var="feature">
												<cms:component component="${feature}" />
											</cms:pageSlot>
											</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
	<section class="main-container bg-gray clearfix  conditionList">
		 <div class="container">
			<div class="row">
			
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="bg-white">
							<div class="tabOuterWrapper ">
								<div class="vitSupBlock bg-white clearfix">
								<cms:pageSlot position="Section2" var="feature" element="ul">
										<cms:component component="${feature}" element="li"/>
								</cms:pageSlot>
							</div>
						</div>
					</div>
				</div> 
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section3" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>

					<cms:pageSlot position="Section4" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<div class="home-promotions clearfix">
						<div class="promo-wrap clearfix">
							<ul class="home-promo-slider product-slider clearfix"
								data-classname=" " data-minslide="1">
								<cms:pageSlot position="Section5" var="feature">
									<li> <cms:component component="${feature}" /></li>
								</cms:pageSlot>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</template:page>