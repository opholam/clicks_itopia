<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>


						<div class="account-body">
							<div class="acc-table-wrap cashbackTable">
									
									<div class="clearfix bg-light-blue">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>Active Cashback rewards</h2>
										</div>
									</div>
									<!-- End Recent Orders heading  -->

									<!-- Recent Orders table  -->
									<div class="acc-body clearfix bg-white">
										<div class="recent-head">
											<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 tableContent">Amount issued</div>
											<div class="col-lg-7 col-md-7 col-sm-5 col-xs-5 tableContent">On date</div>
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 tableContent">Expires</div>
										</div>
										<div class="repeat-table-wrap">
										<c:choose>
											<c:when test="${not empty cbraccount}">
												<c:forEach var="cbrtransaction" items="${cbraccount.CBRtransactions}">
													<div class="recent-head recent-custom">
														<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 tableContent">
															<div class="rewardPoint">
																<c:choose>
																	<c:when test="${not empty selectedaccount and selectedaccount.accountID eq 5000002 }">
																		R<dateFormat:dateFormat conversionType="rand" input="${cbrtransaction.amountIssued}"/>
																	</c:when>
																	<c:otherwise>
																		P<dateFormat:dateFormat conversionType="rand" input="${cbrtransaction.amountIssued}"/>
																	</c:otherwise>
																</c:choose>
																
																
																<%-- P${cbrtransaction.amountIssued} --%>
																<!-- P23<sup>80</sup> -->
															</div>
														</div>
														<c:set var="issueDate"><dateFormat:dateFormat conversionType="date" input="${cbrtransaction.issueDate}" format="dd MMMM yyyy"/></c:set>
														<div class="col-lg-7 col-md-7 col-sm-5 col-xs-5 tableContent">${issueDate}</div>
														<c:set var="expiry"><dateFormat:dateFormat conversionType="dateDifference" input="${cbrtransaction.expiryDate}"/></c:set>
														<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 tableContent"><strong class="red"><c:choose><c:when test="${expiry lt 0}">Expired</c:when><c:when test="${expiry eq 0}">Today</c:when><c:otherwise>${expiry} days</c:otherwise></c:choose></strong></div>
													</div>
												</c:forEach>
											</c:when>
											<c:otherwise> 
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<spring:theme code="text.myAccount.activeCashBack" text="You have no active rewards yet"/>
											</div>
											</c:otherwise>
										</c:choose>
										</div>
									</div>
								</div>
							</div>