<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<!-- success message  -->

	<%-- <cms:component uid="my-reward-point-component" /> --%>	
	<c:if test="${GATrakingCA}">
		<input id="gaTrackingCA" type="hidden" name="GATrakingCA" value="${GATrakingCA}">
	</c:if>
	<!-- Points Account start-->
<c:set var="selectedaccount" value="" scope="request" />

<c:forEach var="account" items="${user.pointStatement.accounts}">
	<c:if test="${account.accountID eq 5000002}">
		<c:set var="selectedaccount" value="${account}" scope="request" />
	</c:if>
</c:forEach>
<!-- Points Account Ends-->

<!-- CBR Account starts-->
<c:set var="cbraccount" value="" scope="request" />

<c:forEach var="cbrStatement" items="${user.CBRstatement}">
	<c:if test="${cbrStatement.accountID eq 5000000}">
		<c:set var="cbraccount" value="${cbrStatement}" scope="request" />
	</c:if>
</c:forEach>
<!-- CBR Account Ends-->
	<div class="clearfix success-message">
	<c:choose>
	<c:when test="${not empty selectedaccount}">
	<c:choose>
	<c:when test="${selectedaccount.pointsToQualify eq 0 and selectedaccount.pointsBalance gt 0}">
		<cms:pageSlot position="Section3B" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
		</c:when>
		<c:otherwise>
		<cms:pageSlot position="Section3A" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
		</c:otherwise>
		</c:choose>
		<c:if test="${selectedaccount.pointsToQualify gt 0}">
			<c:set var="points" value="${selectedaccount.pointsToQualify}"></c:set>
			<c:set var="qualify" value="${points} points required"></c:set>
		</c:if>
	</c:when>
	<c:otherwise>
	<cms:pageSlot position="Section3A" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	</c:otherwise>
	</c:choose>
</div>
	<cms:pageSlot position="Section2" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	
	<cms:pageSlot position="Section2a" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<!-- pointsDetails End-->

	
	<div class="account-body rewardsSection rewardsWrapper">

		<!-- Points Activity Section Starts  -->
		
		<cms:pageSlot position="Section3" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	
		<!-- Points Activity Section Ends  -->
		
		<!-- Financial Services start  -->

		<cms:pageSlot position="Section4" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>

		<!-- Financial Services end  -->
		
		<!-- ClubCard digital magazines start  -->
		<div class="clearfix bg-light-blue">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
				<div class="pull-left">
					<h2>ClubCard digital magazines</h2>
				</div>
			</div>
		</div>
		<div class="clearfix bg-white magazineWrapper">
			<div class="row">
				<cms:pageSlot position="Section5" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				
				<!-- Swapnil : Needs to check with Vandhana as clickscmsmedialinkparagraphcomponent has already some code in it and following new code needs to get merged over there -->
	
				<!-- New code start -->
				<%-- <div class="col-md-6 col-sm-6 col-xs-12">
					<div class="magazineImg">
						<img src="${component.media.url}" />
					</div>
					<div class="magazineContent">
						${component.title} <a href="${component.mediaLink.linkName}">Read
							online now</a>
					</div>
				</div> --%>
				<!-- New code ends -->

				<%-- <div class="col-md-6 col-sm-6 col-xs-12">
					<div class="magazineImg">
						<!-- <img src="../image/rewards/clicks-magazine-1.jpg" /> -->
						<img src="${ component.media.url}" />
					</div>
					<div class="magazineContent">
						<h3>ClubCard Magazine Issue Q1/2015</h3>
						<p>Take up the #NewYearNewYou challenge, and read Maps
							Maponyane's fitness tips</p>
						<a href="${component.mediaLink.linkName}">Read online now</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="magazineImg">
						<img src="../image/rewards/clicks-magazine-2.jpg" />
					</div>
					<div class="magazineContent">
						<h3>ClubCard Magazine Issue Q1/2014</h3>
						<p>Take up the #NewYearNewYou challenge, and read Maps
							Maponyane's fitness tips</p>
						<a href="javascript:void(0)">Read online now</a>
					</div>
				</div> --%>
			</div>
		</div>
		<!-- ClubCard digital magazines end  -->
	</div>
</div>