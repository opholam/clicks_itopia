<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="myaccount" tagdir="/WEB-INF/tags/desktop/myaccount"%>

<template:page pageTitle="${pageTitle}">
	<div id="wrapper">
			<div class="clearfix bg-white editprofiletop order-top-space">
		<div class="container">
			<div class="row">
				<!-- Profile Image  -->
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-5">
					<img class="img-circle img-responsive"
						src="../image/edit-form/default-profile-pic.jpg" alt="Profile image"
						title="Profile image">
				</div>
				<!-- End Profile Image  -->

				<!-- Heading - My clicks  -->
				<div class="col-lg-10 col-md-10 col-sm-9 col-xs-7">
					<h1>
						Update your <span>personal details</span>
					</h1>
					<a href="">Change photo</a> <a href="">Delete photo</a>
				</div>
				<!-- End Heading - My clicks  -->
			</div>
	</div>
		</div>
		<section class="main-container">
			<div class="contentWrapper bg-gray">
				<div class="container">
					<div class="row">
						<div class="accLandOuter clearfix accountAside">

							<!-- Left side -->
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="filter">

									<!-- Left side first link -->
									<div class="my-account-block bg-white">
										<h2>
											<a href="${request.contextPath}/my-account" class="active"><spring:theme
													code="myaccount.summary.title" /></a>
										</h2>
									</div>
									<!-- End Left side first link -->

									<ul class="panel-group" id="accordion">
										<!-- Left Drop down 1 - Accordion -->
										<cms:pageSlot position="SideNavigation" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
										<!-- End Left Drop down 1 - Accordion-->
									</ul>
									<!-- End Last Drop down  -->
								</div>
							</div>
							<!-- End Left side -->

							<!-- Right Side Content  -->
							<cms:pageSlot position="Section1" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<cms:pageSlot position="BodyContent" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<!-- Right Side Content  -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</template:page>