<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">

	

	<div id="globalMessages">
		<%--  <common:globalMessages />  --%>
			<common:registerGlobalMessages />
	</div>

	<cms:pageSlot position="Section1" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<div class="home-page-wrapper bg-gray">
		<div class="container">
			<div class="home-notification">
				<cms:pageSlot position="Section2" var="feature" element="div" class="text-center">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>

			<div class="clinic-links clearfix ">
				<cms:pageSlot position="Section3" var="feature">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<cms:component component="${feature}" />
					</div>
				</cms:pageSlot>
			</div>

			<div class="home-promotions">
				<div class="clearfix bgred">
					<div
						class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
						<cms:pageSlot position="Section4" var="feature"
							class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>



				<div class="promo-wrap clearfix">
					<ul class="home-promo-slider product-slider clearfix"
						data-classname="home-promo-slider" data-minslide="3">
						<cms:pageSlot position="Section5" var="feature">
							<li> <cms:component component="${feature}" element="div" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pad-zero"/></li>
						</cms:pageSlot>
					</ul>
				</div>
			</div>
			
			<div class="promo-links clearfix">

				<cms:pageSlot position="Section6" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>


			</div>

			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 body-shop">
					<div class="body-shop-promo">
						<div class="clearfix bgskyblue">
							<div
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
								<cms:pageSlot position="Section7" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
						</div>
					</div>
					<div class="body-shop-wrap">
						<ul class="new-arrivals clearfix">
							<cms:pageSlot position="Section8" var="feature">
									<li> <cms:component component="${feature}" element="div" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 body-content"/></li>
								</cms:pageSlot>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 body-shop">
					<cms:pageSlot position="GoogleAd" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>


			</div>




			<div class="only-at-clicks">
				<div class="clearfix bg-light-blue">
					<div
						class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
						<cms:pageSlot position="Section9" var="feature">
							<h2>
								<cms:component component="${feature}" />
							</h2>
						</cms:pageSlot>
					</div>
				</div>
				<div class="partner-wrap clearfix bg-white">
					<div class="partner-spacing col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<ul class="only-partner-slider slider-component" data-classname="only-partner-slider" data-maxslide="7">
									<cms:pageSlot position="Section10" var="feature">
										<li>
											<div class="col-md-12 col-sm-12 col-xs-12 ">
												<cms:component component="${feature}" />
											</div>
										</li>
									</cms:pageSlot>
								</ul>
					</div>
				</div>
			</div>
			<div class="moreCategory home-clubcard clearfix">
				<div class="clearfix bg-light-blue">
					<div
						class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
						<cms:pageSlot position="Section11" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<div class="home-club-wrap clearfix">
					<cms:pageSlot position="Section12" var="feature">
						<cms:component component="${feature}" element="div" class="col-lg-4 col-md-4 col-sm-4 col-xs-12"/>
					</cms:pageSlot>
				</div>
			</div>

		</div>
	</div>
	<!-- <script>
		ga('send', 'pageview');
	</script> -->
</template:page>