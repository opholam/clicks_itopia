<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<template:page pageTitle="${pageTitle}">

<!-- building the search query for load more promotions START -->
    <c:set var="qryString" value="${request.queryString}"></c:set>
	<c:set value="${fn:substring(qryString,0 ,qryString.lastIndexOf('&')) }" var="searchQuery"/>
<!-- building the search query for load more promotions END -->
	
	<c:if test="${not empty message}">
		<spring:theme code="${message}"/>
	</c:if>

	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<div class="promotion_offer_top clearfix bg-white">
		<div class="container">
			<div class="row featured-wrap">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 feature-prod">
					<div class="product-left"><h1 class="main-title">${promotionsPageTitle}</h1>
						<cms:pageSlot position="Section1A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				
				<div class="feature-prod">
					
					<cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="accLandOuter clearfix ">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="promo-links clearfix integrated_promo">
						
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<cms:pageSlot position="Section2" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						</div>
					
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right">
						 <sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
								<a href="javascript:void(0);" onclick="$('.sign-in').click();">Sign in </a> or <a
									href="${request.contextPath}/register/getEnrollForm">Join ClubCard</a>
							</sec:authorize>
							<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
								<c:choose>
									<c:when test="${not empty user.memberID}">

									</c:when>
									<c:otherwise>
										<a href="${request.contextPath}/register/joincc">Join ClubCard</a>
									</c:otherwise>
								</c:choose>
							</sec:authorize>
						</div>
						</div>
						   
						</div>
						<!-- Left side -->
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="filter promo-filter">

							
								<div class="my-account-block promo-navwrap">
									<h2>
										<a href="javascript:void(0)">Promotions and Savings</a>
									</h2>
								</div>
							
								<div class="my-account-block show_promotions">
									<a href="${request.contextPath}/specials">All promotions</a>
								</div>
							
								<ul class="panel-group" id="accordion">
								<c:forEach items="${searchResult.facetMap}" var="facetEntry">								
									<li class="panel panel-default bg-white closed">		
										<a class="tittle" href="javascript:void(0)"><spring:theme code="${facetEntry.value.name}" text="${facetEntry.value.name}" /></a>								
									<ul class="panel-collapse collapse in promofacet" id="${facetEntry.value.name}">
										<c:forEach items="${facetEntry.value.facetValues}" var="facet">
											<c:if test="${facet.name ne 'OH1' and (facetEntry.value.name eq 'promotionTypeID' or fn:contains(facet.name, 'OH1'))}">
											<li <c:if test="${facet.selected}">class="facetActive active" </c:if>><a id="${facet.name}" class="promotionFacet" href="${request.contextPath}/specials?${facetEntry.value.name}=${facet.name}">${facet.displayName}</a></li>
											</c:if>
										</c:forEach>
									</ul>
									</li>
								</c:forEach>
							</ul>
								<!-- End Last Drop down  -->
							</div>
						</div>
						<!-- End Left side -->
						
						<!-- Right Side Content  -->
						 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						 <div class="row load-sort">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 titleWrapper">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 paddingNone title-text">Load</div>
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 paddingNone">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 paddingNone">
									<div class="loadCount">
									<select class="basic promoloadcount" >
										<option value="12" ${param.showMode eq '12' or empty param.showMode  ? 'selected': ''}>12</option>
										<option value="24" ${param.showMode eq '24' ? 'selected': ''}>24</option>
										<option value="48" ${param.showMode eq '48' ? 'selected': ''}>48</option>
									</select> </div>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 title-text">
									promotions at a time</div>
								</div>
								</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 titleWrapper">
								<div class="sortList">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 title-text">SortBy</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 paddingNone">
										<select class="basic promosort">
											<option value="startDate-desc" ${param.sort eq 'startDate-desc'or empty param.sort  ? 'selected': ''}>Just announced</option>
											<option value="endDate" ${param.sort eq 'endDate' ? 'selected': ''}>Expiring soon</option>
											<option value="title" ${param.sort eq 'title' ? 'selected': ''}>A - Z</option>
											<option value="title-desc" ${param.sort eq 'title-desc' ? 'selected': ''}>Z - A</option>
										</select>
										</div>
								</div>
							</div>
						</div>
							<div class="promotion-body">
								<!-- Latest Promotions -->
								<div class="categoryLanding clearfix bg-white bottom-marg promoPage">
									<c:forEach items="${searchResultData}" var="promotion" end="${pageSize}">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-zero">
											<div class="categoryType red">
													${promotion.offer} 
											</div>
											<div class="home-promo-img">
												<img src="${promotion.productBanner.url}" alt="category" class="promo_backup_picture" onError="this.onerror=null;this.src='${themeResourcePath}/clicks/image/missing-promotions.jpg';" />
											</div>
											<div class="home-promo-text">
												<div class="banner_caption">
													<div class="caption-inner">
														<div class="caption-wrap">
															<div class="offer_sticker">
																<c:forEach items="${promotion.stickerMediaUrls}"
																	var="stickerUrl">
																	<img src="${stickerUrl}" alt="Clicks Promo">
																</c:forEach>
															</div>
															<h4>${promotion.title}</h4>
															<p>
															<c:choose>
															<c:when test="${not empty promotion.disclaimer}">
															<c:if test="${not empty promotion.description}">${promotion.description}.&nbsp;</c:if>${promotion.disclaimer}.
															</c:when>
															<c:otherwise>
															${promotion.description}
															</c:otherwise>
															</c:choose>
															</p>
															<button class="btn" onclick="window.location='${request.contextPath}/c/OH1?q=%3Arelevance%3AallPromotions%3A${promotion.code}'">Shop now</button>
														</div>
													</div>
												</div>
											</div>
											<div class="promoOffer">
												<strong>Valid until <fmt:formatDate value="${promotion.endDate}" pattern="dd MMM yyyy" /></strong>
											</div>				
										</div>
									</c:forEach>
								</div>
							</div> 							
							<!-- Latest Promotions -->
							<c:set value="${searchResult.totalNumberOfResults}" var="size"/>
							<input type="hidden" id="currentPageSize" value="${pageSize}"/>	
							<input type="hidden" id="pageNumber" value="${page}"/>						
							<c:if test="${size > pageSize}">
								<div class="text-center"> <a class="btn btn-load-promotion">Load more promotions</a></div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</template:page>
