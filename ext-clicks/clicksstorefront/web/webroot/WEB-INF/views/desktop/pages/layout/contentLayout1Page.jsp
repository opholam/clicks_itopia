<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<template:page pageTitle="${pageTitle}">
	<section class="main-container bg-gray brand">
		<div class="bannerOuter">
			<div class="banner-brand">
				<cms:pageSlot position="Section1" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>

<div class="container">
    <div class="row">
		<div class="brand-landing clearfix">
		<div class="col-md-3 col-sm-12 col-xs-12">
			<aside class="brandSidebar bg-white">
				<h2>${cmsPage.name}</h2>
					<ul>
						<cms:pageSlot position="Section2" var="feature">
							<cms:component component="${feature}" element="li"/>
						</cms:pageSlot>
					</ul>
			</aside>
	</div>
		
	<!--
	<div class="span-24 section2">
		<cms:pageSlot position="Section2A" var="feature" element="div" class="span-4 zone_a cms_disp-img_slot">
			<cms:component component="${feature}"/>
		</cms:pageSlot>

		<cms:pageSlot position="Section2B" var="feature" element="div" class="span-20 zone_b last">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>

	-->
	<div class="col-md-9 col-sm-12 col-xs-12">
		<c:set var="hideDiv" value="false" />
			<cms:pageSlot position="Section3" var="feature">		
				<c:if test="${hideDiv eq 'false'}">	
				<div class="brand-products clearfix tb-res">
					<c:set var="hideDiv" value="true" />	
				</c:if>
				<cms:component component="${feature}" element="div" class="col-md-6 col-sm-6 col-xs-12"/>		
			</cms:pageSlot>
			<c:if test="${hideDiv eq 'true'}"> 
				</div>
			</c:if>
		
			<c:set var="hideDiv" value="false" />
			<cms:pageSlot position="Section4" var="feature">
				<c:if test="${hideDiv eq 'false'}">
					<div class="brand-videos bg-white clearfix tb-res">
					<c:set var="hideDiv" value="true" />	
				</c:if>
				<cms:component component="${feature}" />
			</cms:pageSlot>
			<c:if test="${hideDiv eq 'true'}"> 
				</div>
			</c:if>
			<c:set var="hideDiv" value="false" />
					<cms:pageSlot position="Section5" var="feature">
					<c:if test="${hideDiv eq 'false'}">
					<div class="brand-content bg-white clearfix">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="brand-content-inner">
								<c:set var="hideDiv" value="true" />
							</c:if>	
						<cms:component component="${feature}"/>
					</cms:pageSlot>		
					<c:if test="${hideDiv eq 'true'}"> 
				</div>
			</div>
		</div>
		</c:if>
		<div class="bg-gray specialBlock clearfix productRange tb-res brandSlide">					
			<div class="contentWrapper clearfix">	
				<cms:pageSlot position="Section6-head" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>		
			<cms:pageSlot position="Section6" var="feature" element="div">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
	
	
		<div class="bg-gray specialBlock clearfix productRange tb-res brandSlide">					
			<div class="contentWrapper clearfix">
				<cms:pageSlot position="Section7-head" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>		
			</div>	
			<cms:pageSlot position="Section7" var="feature" element="div" class="span-24 section3 cms_disp-img_slot">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>	
	</div>
	</div>
	</div>
</div>
</section>		
</template:page>
