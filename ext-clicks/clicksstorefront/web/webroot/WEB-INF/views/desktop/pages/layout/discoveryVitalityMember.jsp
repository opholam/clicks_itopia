<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	
	<!-- script for tweeter plug-in  -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<!-- script for tweeter plug-in  -->

<section class="rewards account">
	<div class="container">
		<div class="acc-heading">
			<h1>${cmsPage.title}</h1>
		</div>
	</div>	
</section>
<section class="main-container">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="clearfix discover_vital">
						<div class="col-lg-12 col-md-9 col-sm-12 col-xs-12">

							<div class="activate-vitality-cash bg-white clearfix">
								<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
									<cms:pageSlot position="Section1A" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>

								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 cash-back-img">
									<div class="cash-back-outer">
										<cms:pageSlot position="Section1B" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</div>
							</div>
							
							<div class="discover-vitality-wrap bg-white clearfix">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="pull-left vital-img-text">
										<cms:pageSlot position="Section2A" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</div>
							</div>
							
							<div class="vital-logo-wrap bg-white clearfix">
								<cms:pageSlot position="Section3A" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
								<div class="row">
									<cms:pageSlot position="Section3B" var="feature">
										<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 vital-logo">
											<div class="border-full text-center">
												<cms:component component="${feature}" />
											</div>
										</div>
									</cms:pageSlot>
								</div>
								<div class="terms_bottom">
									<cms:pageSlot position="Section2B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</template:page>