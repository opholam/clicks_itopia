<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:page pageTitle="${pageTitle}">
<%-- 	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<div>
		<c:if test="${not empty message}">
			<c:out value="${message}"/>
		</c:if>
	</div>

	<cms:pageSlot position="MiddleContent" var="comp" element="div" class="span-20">
		<cms:component component="${comp}"/>
	</cms:pageSlot>

	<cms:pageSlot position="BottomContent" var="comp" element="div" class="span-20 cms_disp-img_slot last">
		<cms:component component="${comp}"/>
	</cms:pageSlot>

	<cms:pageSlot position="SideContent" var="feature" element="div" class="span-4 narrow-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot> --%>
	
	
	<div class="clearfix bg-white page_notfound ">
		<div class="container">
				<div class="contact-wrap-top text-center">
					<h1>
						<common:globalMessages/>
					</h1>
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<cms:pageSlot position="Section1" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<!-- End Heading - My clicks  -->
		</div>
	</div>
	<div class="bg-gray clearfix productRange tb-res category_carousel carouselCount pagenfound">
		<div class="container">	
		<div class="pagedata_wrapper">
			<div class="clearfix bg-light-blue">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
				<cms:pageSlot position="Section2" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				</div>
			</div>
			<div class="bg-white">
			<cms:pageSlot position="Section3" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
			</div>
		</div>
		</div>
	</div>
</template:page>
