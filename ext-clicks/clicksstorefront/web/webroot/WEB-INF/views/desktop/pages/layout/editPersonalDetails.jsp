<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<script>
function start() {
	if(${fn:length(editClubCardForm.IDNumber)} == 13){
		//do nothing
		//alert($('#bb').val());
	}
	else{
		$('#bb').val('');
	}
	}
	window.onload = start;
</script>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
<div id="globalMessages">
	 <common:globalMessages />  
</div>
<c:url var="profileUrl" value="/my-account/edit-personal-details" />
<!-- Right Side Content  -->

			<form:form action="${contextPath}/my-account/edit-personal-details" method="post" commandName="editClubCardForm">
			<!-- Right Side Content  -->
			<div class="editpersonal_page">
					<div class="row acc-row-5">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5">
							<div class=" bg-white  clearfix">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
									<h2><spring:theme code="edit.profile.title"/></h2>
									</div>
								</div>
								<div class="clearfix acc-col-5">
									<div class="pad-spacing form-wrap">
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="fname" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text mandatory form-control" mandatory="true" />
										</div>
										<div class="form-group clearfix">
										<formElement:formInputBox idKey="lname" labelKey="customer.edit.profile.PreferedName" path="preferedName" inputCSS="text mandatory" mandatory="false"/>
										</div>
									<div class="form-group clearfix">
										<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text mandatory" mandatory="true"/>
										</div>
										<div class="form-group saResident">
													<label><spring:theme code="customer.create.account.isSAResident"/> </label>
													 <div class="form-group clearfix">
													 	<div class="clearfix">
													 		 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone">
													 		 <div class="custom_hide radioOuter">
														 		<form:radiobutton path="isSAResident" value="1" label="yes"  cssClass="radio-hide" />
															 </div>
															 </div>
															<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
																<formElement:isClubcardFormInputBox idKey="bb" labelCSS="" labelKey="" path="IDNumber" inputCSS="" mandatory="false" autocomplete="off" />
			           										</div>
			           									</div>	
			           									
			           									<div class="clearfix">
			           										<div class="customno radioOuter">
			           											<form:radiobutton path="isSAResident" value="0" label="No"  cssClass="radio-hide" />
			           										</div>
			           									</div>
		           									 </div>
		           									 
		           									<div class=" clearfix dobSaOuterFirst" id="cc">
		           									 <label><spring:theme code="customer.create.account.SA_DOB"/> </label>
		           									 <div class="dob">
		           									 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
		           									  <div class="dobWrapper">
															<formElement:DOBformSelectBox selectCSSClass="dobSa12 dateCheck" labelCSS="dobSa12" idKey="customer.create.account.SA_DOB_day" labelKey="customer.create.account.SA_DOB_day" path="SA_DOB_day" mandatory="true" items="${days}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
													  <div class="monthWrapper">
															<formElement:DOBformSelectBox selectCSSClass="monthCheck" idKey="customer.create.account.SA_DOB_month" labelKey="customer.create.account.SA_DOB_month" path="SA_DOB_month" mandatory="true" items="${months}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
													  <div class="yearWrapper">
															<formElement:DOBformSelectBox selectCSSClass="yearCheck" idKey="customer.create.account.SA_DOB_year" labelKey="customer.create.account.SA_DOB_year" path="SA_DOB_year" mandatory="true" items="${years}" />
													  </div>
													  </div>
													</div>
													</div>
											</div>
										<div class="form-inline" >
												<div class="gender" ><label><spring:theme code="edit.profile.gender.title"/></label> </div> 
												<div class="form-group margin-spacing clearfix radioOuter">
													<form:radiobutton path="gender" value="1" label="Male"  name="male" cssClass="radio-hide" />
												</div>
												<div class="form-group clearfix radioOuter">
													<form:radiobutton path="gender" value="2" label="Female"  name="female" cssClass="radio-hide" />
												</div>
											</div>
									</div>
								</div>
							</div>
						</div>

						<!-- Contact details Form Start -->
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5" >
							<div class="bg-white  clearfix">
								<!-- Conact Details heading  -->
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
									<h2><spring:theme code="edit.profile.contact.title"/></h2>
									</div>
								</div>
								<!-- End Conact Details heading -->

								<div class="bg-white clearfix acc-col-5">
									<div class="form-wrap pad-spacing">
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.edit.cc.profile.EMailAddress" labelKey="customer.edit.cc.profile.EMailAddress" path="eMailAddress" inputCSS="text mandatory editEmail"  mandatory="true"/>
										</div>
										<div class="form-group clearfix">
											<formElement:formCheckbox idKey="emailIDEditable" labelKey="edit.email.id.checkbox.account.exists" path="emailIDEditable" inputCSS="text editPwdCheckbox"/>
										</div>
										<label class="edit-email-checkbox-validation red"><spring:theme code="edit.email.id.checkbox.account.exists.password.validation" ></spring:theme> </label>
										<div class="form-group clearfix editPwdInput">
											 <formElement:formPasswordBox idKey="customer.enroll.Password" labelKey="customer.enroll.Password" path="password" inputCSS="text"/>
										</div>
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.enroll.contact.Number" labelKey="customer.edit.cc.contact.Number" path="contactNumber" inputCSS="text mandatory" mandatory="true"/>
										</div>
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.enroll.alternate.Number" labelKey="customer.enroll.alternate.Number" path="alternateNumber" inputCSS="text"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Contact details Form End -->
					</div>
					 <div class="row acc-row-55">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55  ">
							<div class="bg-white ">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
									<h2><spring:theme code="edit.profile.address.title"/></h2>
									</div>
								</div>
								<div class="bg-white clearfix acc-col-55">
									<div class="pad-spacing form-wrap">
									<div class="form-group clearfix">
									<formElement:formSelectBox idKey="customer.enroll.Country" labelKey="customer.enroll.Country" path="country" mandatory="false" items="${country}" />
									</div>
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.enroll.AddressLine1" labelKey="customer.enroll.AddressLine1" path="addressLine1" inputCSS="text mandatory" mandatory="true"/>
											</div>
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.enroll.AddressLine2" labelKey="customer.enroll.AddressLine2" path="addressLine2" inputCSS="text mandatory" />
											</div>

											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.enroll.Suburb" labelKey="customer.enroll.Suburb" path="suburb" inputCSS="text mandatory" mandatory="true"/>
											</div>

											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.enroll.City" labelKey="customer.edit.cc.City" path="city" inputCSS="text"/>
											</div>

											<div class="form-group clearfix">
												 <div class="errorMandatory">
													<formElement:formSelectBox idKey="customer.enroll.Province" labelKey="customer.edit.cc.province" path="province" mandatory="false" items="${province}" />
												</div>
											</div>
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.enroll.PostalCode" labelKey="customer.enroll.PostalCode" path="postalCode" inputCSS="text mandatory" mandatory="true"/>
											</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55 edit-form-wrap">
							<div class="bg-white">
								<div class="clearfix bg-light-blue">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
									<h2><spring:theme code="edit.profile.communication.title"/></h2>
									</div>
								</div>
								<div class="bg-white clearfix acc-col-55">
								<div class="pad-spacing form-wrap ">
												<label><spring:theme code="customer.enroll.ACCINFO"/> </label>
											
											<div class="form-group clearfix">
												<formElement:formCheckbox idKey="customer.enroll.marketing.Email1" labelKey="customer.enroll.marketing.Email" path="accinfoEmail" inputCSS="text email-mar marketing-update"/>
											</div>

											<div class="form-group clearfix">
												<formElement:formCheckbox idKey="customer.enroll.marketing.SMS1" labelKey="customer.enroll.marketing.SMS" path="accinfoSMS" inputCSS="text sms-mar marketing-update"/>
											</div>
											<br/>
											 <div class="form-group clearfix">
											<label><spring:theme code="customer.enroll.MARKETING"/> </label>
												<formElement:formCheckbox idKey="customer.enroll.marketing.Email" labelKey="customer.enroll.marketing.Email" path="marketingEmail" inputCSS="text check-hide"/>
													<formElement:formCheckbox idKey="customer.enroll.marketing.SMS" labelKey="customer.enroll.marketing.SMS" path="marketingSMS" inputCSS="text check-hide"/>
											</div>
													<div class="form-actions">
														<ycommerce:testId code="editCC_SaveUpdatesButton">
																<button class="btn btn-save-change positive" type="submit"><spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/></button>
														</ycommerce:testId>
													</div>
										</div>
									</div>	
							</div>
						</div>
					</div>
			</div>
	</form:form>
</div>
<!-- End Right side Conent -->
