<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="/login/pw/request" var="passowrdResetUrl" />
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	<div class="bg-white  clearfix ">
		<div class="clearfix bg-light-blue">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
				<!-- <h2>If you don't have a Clicks ClubCard</h2> -->
				<h2><spring:theme code="forgottenDetails.no.clubcard"/></h2>
			</div>
		</div>
		<div class="bg-white clearfix acc-col-4">
			<div class="pad-spacing">
			<div class="form-wrap">
				<!-- <div class="form-group clearfix"> -->
					<p>Try entering any other email addresses you may have used to registed your ClubCard on the <a href="javascript:void(0)" data-url="${passowrdResetUrl}" class="forgot-block">forgotten password</a> screen</p>
				<!-- </div> -->
			</div>
				<!-- <button class="btn btn-save-change">Forgotten password</button> -->
				<!-- <a href="#" data-url="/store/login/pw/request" class="btn btn-save-change password-forgotten forgot-block"> -->
				<a href="javascript:void(0)" data-url="${passowrdResetUrl}" id="forgotBlock" class="forgot-block btn primary_btn">
					<spring:theme code="forgottenDetails.password"/>
				</a>				
				<div class="alternate-info">
					<cms:pageSlot position="RightContentDetailSlot" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
</div>