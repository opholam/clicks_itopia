<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<c:choose>
<c:when test="${not empty request.contextPath}">
<c:set value="${request.contextPath}/j_spring_security_check" var="loginActionUrl" />
</c:when>
<c:otherwise>
<c:url value="/j_spring_security_check" var="loginActionUrl" />
</c:otherwise>
</c:choose>
<div id	="user-login-page"><div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><user:login actionNameKey="login.login" action="${loginActionUrl}"/></div></div></div>
