<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="storepickup"
	tagdir="/WEB-INF/tags/desktop/storepickup"%>
<%@ taglib prefix="pagination"
	tagdir="/WEB-INF/tags/desktop/nav/pagination"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>
	<c:forEach items="${searchPageData.sorts}" var="sortData">
	<c:if test="${sortData.selected}">
	<c:set var="sortCode" scope="request" value="${sortData.code}"/>
	</c:if>
	</c:forEach>
		<div class="clearfix backgroundWhite specialBlockTop category-top">
		<div class="container">
			<div class="${not empty featuredProdListm?'col-lg-4 col-md-4 col-sm-12 col-xs-12':'col-lg-12 col-md-12 col-sm-12 col-xs-12' }">
				<div class="product-left">
				<c:choose>
				<c:when test="${not empty promoOffer and not empty promoTitle}">
				<h1 class="main-title red">${promoOffer}</h1>
				<h1 class="main-title red">${promoTitle}</h1>
				</c:when>
				<c:otherwise>
					<h1 class="main-title">
					<c:choose>
					<c:when test="${categoryCode eq 'OH1' and fn:contains(param.q, 'newProduct')}">
					New at Clicks
					</c:when>
					<c:when test="${categoryCode ne 'OH1' and fn:contains(param.q, 'newProduct')}">
					New in ${categoryName}
					</c:when>
					<c:when test="${categoryCode eq 'OH1'}">
					
					</c:when>
					<c:otherwise>
					 ${categoryName}
					</c:otherwise>
					</c:choose>
					</h1>
					<p>${categoryDescription}</p>
				</c:otherwise>
				</c:choose>
				</div>
			</div>

			<c:forEach items="${featuredProdListm}" var="product">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
					<a href="${request.contextPath}/p/${product.code}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-4 col-xs-6 productBlock">
								<product:productPrimaryImage product="${product}" format="product"/>  
							</div>
	
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 sp-content">
								<h4 class="blue">
									<%-- <spring:theme code="featured.product.title" /> --%>
									<cms:component uid="featuredProductComponent" />
								</h4>
								<span class="subTitle">${product.brand}</span>
								<p>${product.sizeVariantTitle ne null ? product.sizeVariantTitle : product.styleVariantTitle ne null ? product.styleVariantTitle: product.baseProductTitle ne null? product.baseProductTitle: product.name }</p>
							</div>
						</div>
					</a>
				</div>
			</c:forEach>
		</div>
	</div>

	<!-- section class="main-container"> -->
	<div class="contentWrapper bg-gray">
		<div class="container">
			<div class="row">
				<div class="filter clearfix">
					<div class="col-sm-3 col-md-3">
					 <div class="span-6 facetNavigation">
						<%-- <h2 class="bg-white clearfix">Filter ${searchPageData.pagination.totalNumberOfResults} results</h2> --%>
						<div class="panel-group readMoreWrap" id="accordion">
							<cms:pageSlot position="ProductLeftRefinements" var="feature">								
								<%-- <ycommerce:testId code="searchResults_productsFound_label">
								<div class="totalResults"><spring:theme code="${themeMsgKey}.totalResults" arguments="${searchPageData.pagination.totalNumberOfResults}"/></div>
							</ycommerce:testId> --%>
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						</div>
					</div>

					<div class="col-md-9 col-sm-9 col-xs-12">
						<div class="row load-sort">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 titleWrapper">
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 paddingNone title-text">Load</div>
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 paddingNone">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 paddingNone">
								<div class="loadCount">
									<select id="basic" onchange="window.location.href=this.value">
										<option value="${categoryCode}?count=12&sort=${param.sort}&q=${param.q}" selected="selected">12</option>
										<option value="${categoryCode}?count=24&sort=${param.sort}&q=${param.q}"${(param.count eq '24') ? 'selected="selected"' : ''}>24</option>
										<option value="${categoryCode}?count=48&sort=${param.sort}&q=${param.q}"${(param.count eq '48') ? 'selected="selected"' : ''}>48</option>

									</select>
								</div>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 title-text">products at a time</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 titleWrapper">
							<div class="sortList">  
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 title-text">SortBy</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 paddingNone">
								<form id="sort_form1" name="sort_form1" method="get" action="#"
									class="sortForm">									
										<select id="basic2" name="sort" class="sortOptions basic">
											<option value="price-asc"${(sortCode eq 'price-asc') ? 'selected="selected"' : ''}>Price: low to high</option>
											<option value="relevance"${(sortCode eq 'relevance') ? 'selected="selected"' : ''}>Newest</option>
											<option value="price-desc"${(sortCode eq 'price-desc') ? 'selected="selected"' : ''}>Price: high to low</option>
											<option value="topRated"${(sortCode eq 'topRated') ? 'selected="selected"' : ''}>Top Rated</option>
											<option value="name-asc"${(sortCode eq 'name-asc') ? 'selected="selected"' : ''}>Product A-Z</option>
											<option value="name-desc"${(sortCode eq 'name-desc') ? 'selected="selected"' : ''}>Product Z-A</option>
										</select>
									<input type="hidden" name="q" value="${param.q}">
									<input type="hidden" name="count" value="${param.count}">
								</form>
									</div>
							</div>
							</div>
						</div>
						<div class="filterContent">
							<cms:pageSlot position="ProductListSlot" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>

						<c:if test="${searchPageData.pagination.totalNumberOfResults gt fn:length(searchPageData.results)}">
							<div class="text-center" id="loadMoreProd">
								<a class="btn"
									onclick="loadProducts(${count*2},'${categoryCode}')">Load
									more products</a>
							</div>
						</c:if>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 	</section> -->



	<%-- <div class="contentWrapper bg-gray">
		<div class="container">
			<div class="row">
				<div class="clearfix product-display">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-display-inner">
						<cms:pageSlot position="GoogleAd" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div> --%>

	<c:if test="${not empty bestSellerProdList}">

		<div
			class="bg-gray specialBlock clearfix productRange tb-res rel-product best-wrapper">
			<div class="container">
				<h2>
					<spring:theme code="bestsellers.title" />
					&nbsp;<span>${categoryName}</span>
				</h2>
				<ul class="sliderOuter clearfix">


					<c:forEach items="${bestSellerProdList}" var="product"
						varStatus="status">
						<li>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 productBlock">
								<div class="badges">
									<c:if test="${not empty product.potentialPromotions}">
										<c:forEach items="${product.potentialPromotions}" var="promotions">
											<c:if test="${not empty promotions.stickerMediaUrls}">
												<c:forEach items="${promotions.stickerMediaUrls}" var="stickerURL">
													<img src="${stickerURL}" class="mix-match-badge" alt="Clicks Promo"/>
												</c:forEach>
											</c:if>
										</c:forEach>
									</c:if>
									
									<c:if test="${not empty product.nonPromoStickerUrls}">
										<c:forEach items="${product.nonPromoStickerUrls}" var="stickerURL">
											<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
										</c:forEach>
									</c:if>
								</div>

								<a href="${request.contextPath}${product.url}"><product:productPrimaryImage product="${product}" format="product" /></a>
								
								<div class="detailContent clearfix">
								<a href="${request.contextPath}${product.url}">
									<h5 title='${product.brand}'>${product.brand}</h5>
									<div class="product-name">
									<c:set var="title" value="${product.sizeVariantTitle ne null ? product.sizeVariantTitle : product.styleVariantTitle ne null ? product.styleVariantTitle: product.baseProductTitle ne null? product.baseProductTitle: product.name }" />
										<p title='${title}'>${title}</p>
									</div>		
								</a>
									<price:fromPrice product="${product}" />
									
								<%-- 	<div class="offer-blk">
										<c:if test="${not empty product.price.grossPriceWithPromotionApplied}">
					 						
						 						Was&nbsp;<dateFormat:dateFormat conversionType="rand" input="${product.price.formattedValue}"/>
												<span class="red">Save</span>
												<span class="price red"> 
													<fmt:formatNumber var="save" pattern="####.##"
														value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
													R<dateFormat:dateFormat conversionType="rand" input="${save}"/>	
												</span>
										</c:if>

									</div> --%>
									
									<div class="starWrapper">
										<product:productReviewSummary product="${product}" />
									</div>
										<product:productListAddToCartPanel product="${product}" allowAddToCart="${empty showAddToCart ? true : showAddToCart}" isMain="true" />
									
									<c:if test="${not empty product.potentialPromotions}">
										<product:productPromotionSection product="${product}" />
									</c:if>
								</div>
							</div>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</c:if>
	<%-- <storepickup:pickupStorePopup /> --%>

</template:page>
