<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>


<div class="clearfix bg-light-blue">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
		<div class="pull-left">
			<h2>Financial Services</h2>
		</div>
	</div>
</div>

<c:set var="accumulateLifeCoverStatus" value="" />
<c:set var="accumulateLifeCoverLastUpdated" value="" />
<c:set var="accumulateLifeCoverAmount" value="" />
<c:set var="familyFuneralInsuranceStatus" value="" />
<c:set var="carAndHomeCoverStatus" value="" />
<c:set var="lifeCoverStatus" value="" />
<c:set var="womenAccidentStatus" value="" />
<c:set var="hospitalCoverStatus" value="" />

<!-- TODO: Swapnil : Uncomment the following section when model is ready. Need to change the messages of covered/not covered depending on the status of respective cover -->

 <c:forEach var="benefits" items="${user.financialServices}">
	<c:if test="${benefits.benefitId eq '1'}">
		<c:set var="accumulateLifeCoverStatus" value="${benefits.benefitStatus}" />
		<c:set var="accumulateLifeCoverLastUpdated" value="${benefits.benefitDate}" />
		<c:set var="accumulateLifeCoverAmount"><dateFormat:dateFormat conversionType="rand" input="${benefits.benefitAmount}"/></c:set>		
	</c:if>
	
	<c:if test="${benefits.benefitId eq '2'}">
		<c:set var="familyFuneralInsuranceStatus" value="${benefits.benefitStatus}" />	
	</c:if>
	
	<c:if test="${benefits.benefitId eq '3'}">
		<c:set var="carAndHomeCoverStatus" value="${benefits.benefitStatus}" />	
	</c:if>
	
	<c:if test="${benefits.benefitId eq '4'}">
		<c:set var="lifeCoverStatus" value="${benefits.benefitStatus}" />	
	</c:if>
	
	<c:if test="${benefits.benefitId eq '5'}">
		<c:set var="womenAccidentStatus" value="${benefits.benefitStatus}" />	
	</c:if>
	
	<c:if test="${benefits.benefitId eq '6'}">
		<c:set var="hospitalCoverStatus" value="${benefits.benefitStatus}" />	
	</c:if>
</c:forEach>

<div class="clearfix bg-white fsOuter">
	<div class="row border-bottom">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="fsWrapper fsWrapperOne">
				<h2 class="darkBlue">Personal funeral insurance cover</h2>
				<c:set var="active" value="false" />
				<c:if test="${accumulateLifeCoverStatus eq 1}">
					<c:set var="active" value="green_tick" />
				</c:if>
				<h3 class="${active}">
					You benefit from <span class="red">R${accumulateLifeCoverAmount}</span> - at no cost!
				</h3>
				<p>Total is updated weekly, last updated <dateFormat:dateFormat conversionType="date" input="${accumulateLifeCoverLastUpdated}" format="dd MMMM yyyy"/></p>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="fsWrapper fsWrapperTwo">
				<p>Up to R7,500 Funeral cover - at no cost to you!</p>
				<p>
					Your cover is calculated at 10 times the value of your average
					monthly purchases in Clicks for the 26 weeks prior to death. The
					more you spend the higher your cover - up to R7,500. <a
						href="${request.contextPath }/clubcard/financial">Find out more</a>
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="fsWrapper">
				<div class="fsCover">
					<h2 class="lightGreen">Family funeral insurance cover</h2>
					<c:choose>
						<c:when test="${familyFuneralInsuranceStatus eq 1}">
							<p class="tick">
								You're covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:when>
						<c:otherwise>
							<p>
								Not covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:otherwise>
					</c:choose>
					
					
				</div>
				<div class="fsCover">
					<h2 class="darkPink">Life cover</h2>
					<c:choose>
						<c:when test="${lifeCoverStatus eq 1}">
							<p class="tick">
								You're covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:when>
						<c:otherwise>
							<p>
								Not covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="fsCover">
					<h2 class="orange">Hospital event cover</h2>
					<c:choose>
						<c:when test="${hospitalCoverStatus eq 1}">
							<p class="tick">
								You're covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:when>
						<c:otherwise>
							<p>
								Not covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="fsWrapper">
				<div class="fsCover">
					<h2 class="lavander">Car and home cover</h2>
					<c:choose>
						<c:when test="${carAndHomeCoverStatus eq 1}">
							<p class="tick">
								You're covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:when>
						<c:otherwise>
							<p>
								Not covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="fsCover">
					<h2 class="magenta">Women-only Cancer and accident cover</h2>
					<c:choose>
						<c:when test="${womenAccidentStatus eq 1}">
							<p class="tick">
								You're covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:when>
						<c:otherwise>
							<p>
								Not covered. <a href="${request.contextPath }/clubcard/financial">Find out more</a>
							</p>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>