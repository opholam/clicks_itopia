<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="woobox" value="${competition.dataoffer}" />
<div id="woobox-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//woobox.com/js/plugins/woo.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'woobox-sdk'));</script> 
<div class="woobox-offer" data-offer="${competition.dataoffer}"></div>   
<!--  <div class="woobox-offer" data-offer="npsc6n"></div> -->