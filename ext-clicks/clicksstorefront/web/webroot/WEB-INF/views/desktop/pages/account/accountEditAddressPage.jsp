<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/desktop/address"%>

<div class="span-24">
	<div class="span-20 last">
		<div class="accountContentPane clearfix">
			<div class="headline"><spring:theme code="text.account.addressBook.addressDetails" text="Address Details"/></div>
			<div class="required right"><spring:theme code="form.required" text="Fields marked * are required"/></div>
			<div class="description"><spring:theme code="text.account.addressBook.addEditform" text="Please use this form to add/edit an address."/></div>
			
			<form:form method="post" commandName="addressForm">
	<form:hidden path="addressId" class="add_edit_delivery_address_id" status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" id="deliveryAddressForm"/>
	<input type="hidden" name="bill_state" id="address.billstate"/>
	<form:hidden path="selectedDeliveryMode" id="selectedDeliveryMode"/>
	<form:hidden path="countryIso" />
	
<%-- 	<formElement:formSelectBox idKey="address.country"
		                           labelKey="address.country"
		                           path="countryIso"
		                           mandatory="true"
		                           skipBlank="false"
		                           skipBlankMessageKey="address.selectCountry"
		                           items="${supportedCountries}"
		                           itemValue="isocode"
		                           selectedValue="${addressForm.countryIso}"/> --%>
		<div class="form-group clearfix">	<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="text" mandatory="true"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="text" mandatory="false"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.Suburb" labelKey="address.Suburb" path="suburb" inputCSS="readonlyField" mandatory="true"/></div>
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="text" /></div>
		<formElement:formSelectBox idKey="address.province" labelKey="address.province" path="province"  items="${province}" />
		<div class="form-group clearfix"><formElement:formInputBox idKey="address.country" labelKey="address.country" path="countryName" inputCSS="readonlyField" mandatory="true" /></div>
	<div class="form-group clearfix"><div class="control-group"><label class="control-label">Post Code</label> <formElement:formInputBox idKey="address.postcode.show" labelKey="address.postcode" path="postcode" inputCSS="text" mandatory="true"/></div></div>
<%-- 		<div class="form-group clearfix"><formElement:formInputBox idKey="address.postcode.show" labelKey="address.postcode" path="postcode" inputCSS="text" mandatory="true"/></div> --%>
	
	<div class="form-group clearfix"><formElement:formInputBox idKey="address.deliveryInstructions" labelKey="address.deliveryInstructions" path="deliveryInstructions" inputCSS="text"  /></div>
	<%-- <div class="form-group clearfix"><formElement:formSelectBox idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}" selectedValue="${addressForm.titleCode}"/></div> --%>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix"><formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" mandatory="true" /></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="text" mandatory="true"/></div>
	<div class="form-group clearfix">	<formElement:formInputBox idKey="address.alternatePhone" labelKey="address.alternatePhone" path="alternatePhone" inputCSS="text" /></div>
	
	<div class="form-group clearfix"><label class="control-label">Preferred contact method</label>
	<form:radiobutton path="preferredContactMethod" id="address.preferredContactMethod" label="Email"  value="0" />
	<form:radiobutton path="preferredContactMethod" id="address.preferredContactMethod" label="SMS"  value="1" />
	</div>
	<div id="deliveryMethodsDiv" >
	<c:if test="${not empty deliveryModes}">
	<div id="deliveryMethodsOuterDiv" ><div class="delMethodRate"><h2>Delivery Method <strong class="pull-right">Cost</strong></h2>
	<c:forEach items="${deliveryModes}" var="deliveryMode">
		<div class="delivery_method_item"><input type="radio" name="delivery_method" id="${deliveryMode.code}" value="${deliveryMode.code}"/><label for="${deliveryMode.code}">${deliveryMode.name}&nbsp;&nbsp;<span>${deliveryMode.description}</span></label>&nbsp;&nbsp;<div class="price pull-right">${deliveryMode.deliveryCost.formattedValue}</div></div>
	</c:forEach>
		</div>
		</div>
	</c:if>
</div>
	<%-- <div id="i18nAddressForm" class="i18nAddressForm">
		<c:if test="${not empty country}">
			<address:addressFormElements regions="${regions}"
			                             country="${country}"/>
		</c:if>
		
	</div> --%>
	
	<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
	<div class="form-additionals">
		<c:choose>
			<c:when test="${showSaveToAddressBook}">
				<formElement:formCheckbox idKey="saveAddressInMyAddressBook" labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook" path="saveInAddressBook" inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
			</c:when>
			<c:when test="${not addressBookEmpty && not isDefaultAddress}">
				<formElement:formCheckbox idKey="defaultAddress" labelKey="address.default" path="defaultAddress"
				                          inputCSS="add-address-left-input" labelCSS="add-address-left-label" mandatory="false"/>
			</c:when>
		</c:choose>
		
	</div>
	</sec:authorize>

	<div id="addressform_button_panel" class="form-actions">
			
				<c:if test="${not noAddress}">
					<ycommerce:testId code="multicheckout_cancel_button">
						<c:url value="${cancelUrl}" var="cancel"/>
							<a class="button" href="${cancel}"><spring:theme code="checkout.multi.cancel" text="Cancel"/></a>
					</ycommerce:testId>
				</c:if>
				<c:choose>
					<c:when test="${edit eq true}">
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button show_processing_message" type="submit">
								<spring:theme code="checkout.multi.saveAddress" text="Save address"/>
							</button>
						</ycommerce:testId>
					</c:when>
					<c:otherwise>
						<ycommerce:testId code="multicheckout_saveAddress_button">
							<button class="btn btn-save-change positive right change_address_button show_processing_message" type="submit">
								<spring:theme code="checkout.multi.deliveryAddress.clicks.continue" text="Continue"/>
							</button>
						</ycommerce:testId>
					</c:otherwise>
				</c:choose> 
			</div>
			</form:form>
	<!-- 		<address:addressFormSelector supportedCountries="${countries}" regions="${regions}"
										 cancelUrl="/my-account/address-book"/> -->
			
			<address:suggestedAddresses selectedAddressUrl="/my-account/select-suggested-address"/>  
		
		
		</div>
	</div>
</div>

