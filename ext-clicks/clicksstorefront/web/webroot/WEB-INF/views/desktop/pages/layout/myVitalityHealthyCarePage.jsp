<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/desktop/nav/breadcrumb"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<div class="activate-vitality-cash bg-white clearfix">
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
			<cms:pageSlot position="VitalitySection1A" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>

		</div>
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 cash-back-img">
			<cms:pageSlot position="VitalitySection1B" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>

	</div>
	<div class="discover-vitality-wrap my-vitality-health clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="pull-left vital-img-text fsOuter">

				<cms:pageSlot position="VitalitySection2B" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>

				<cms:pageSlot position="VitalitySection2A" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
			<div class="clearfix vitality_txt_wrap">
				<cms:pageSlot position="VitalitySection3" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>

	</div>

</div>


