<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>

<c:set var="baseUrl" value="${contextPath}" />
<c:choose>
<c:when test="${user.uid eq 'anonymous'}">
<span> <a href="${baseUrl}/login">Sign
		in</a> or <a
	href="${baseUrl}/register/registerCustomer">register</a>
	to collect points.
</span>
</c:when>
<c:otherwise>
<c:if test="${empty user.memberID }">
<span> <a href="${baseUrl}/clicks/en/register/joincc"> Join ClubCard </a> or <a
	href="${baseUrl}/my-account/link-cc">link your ClubCard</a>
	to collect points.
</span>
</c:if>
</c:otherwise>
</c:choose>