<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>
	<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section1A" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="image-center">
						<cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section
		class="main-container bg-gray clearfix articlePage articleMagaz medicinePage">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer">
						<cms:pageSlot position="Section2A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<aside>
						<div class="contentContainer bg-white">
							<cms:pageSlot position="SideContent" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</section>
</template:page>