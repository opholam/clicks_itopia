<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>
	<section class="main-container bg-gray clearfix v-n-s-Page cI">
		<div class="bg-gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="promo-wrap clearfix health-slider-outer">
							<ul class="health-promo-slider product-slider clearfix"
								data-classname="home-promo-slider" data-pager="false">
								<cms:pageSlot position="Section1A" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</ul>
							<c:if test="${not empty hpGATracking}">
										<script>
												var gaId = "${hpGATracking.page}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${hpGATracking.placementCode}".replace(/[^a-zA-Z0-9]/g,'') + "_" + "${hpGATracking.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${hpGATracking.costType}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${hpGATracking.duration}".replace(/[^a-zA-Z0-9-]/g,'') + "_" + "${hpGATracking.creative}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'');
												
												ga('ec:addPromo', {               
												  'id': gaId,     
												  'name': '${hpGATracking.brand}'.toLowerCase().replace(/[^a-zA-Z0-9]/g,''),                  
												  'position': '${hpGATracking.skuId}'.replace(/[^a-zA-Z0-9]/g,'')
												});
										</script>
								<c:remove var="hpGATracking" scope="session" />   
								<c:remove var="hpCount" scope="session" />   
							</c:if>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="healthSyptoms">
							<cms:pageSlot position="Section1B" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="clearfix healthSlilder">
							<!-- Partner slider heading  -->
							<div class="clearfix bg-light-green">
								<div
									class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-custom">
									<div class="pull-left">
										<h2>
											<cms:pageSlot position="Section2" var="feature">
												<cms:component component="${feature}" />
											</cms:pageSlot>
										</h2>
									</div>
								</div>
							</div>
							<!-- Partner slider  -->
							<div class="partner-wrap clearfix bg-white hCWrapper">
								<div
									class="health-slider-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<ul class="health-top-slider">
										<cms:pageSlot position="Section3" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clinic-links clearfix">
					<cms:pageSlot position="Section4" var="feature">
						<div class="col-md-4 col-sm-12 col-xs-12">
							<cms:component component="${feature}" />
						</div>
					</cms:pageSlot>
				</div>
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="clearfix bg-light-green">
							<div
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-custom">
								<div class="pull-left">
									<h2>
										<cms:pageSlot position="Section5" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</h2>
								</div>
							</div>
						</div>
						<cms:pageSlot position="Section6" var="feature" element="div"
							class="articleBlock bg-white clearfix tb-res">
							<cms:component component="${feature}" element="div"
								class="col-md-6 col-sm-6 col-xs-12" />
						</cms:pageSlot>

					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 body-shop">
						<cms:pageSlot position="GoogleAd" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<div class="home-promotions">
					<div class="clearfix bgred">
						<div
							class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
							<cms:pageSlot position="Section7" var="feature"
								class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
					<div class="promo-wrap clearfix">
						<ul class="home-promo-slider product-slider clearfix"
							data-classname="home-promo-slider" data-minslide="3">
							<cms:pageSlot position="Section8" var="feature">
								<li> <cms:component component="${feature}" element="div" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pad-zero"/> </li>
							</cms:pageSlot>
						</ul>
					</div>
				</div>
				<div class="clearfix bg-light-blue">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
						<cms:pageSlot position="Section9" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
					</div>
				</div>
				<div class="bg-white clearfix healthTwitter">
					<cms:pageSlot position="Section10" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
				</div>
			</div>
		</div>
	</section>
</template:page>