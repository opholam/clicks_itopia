<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>

	<section class="rewards account">
		<div class="container">
				<div class="acc-heading">
					<h1>Join<span> babyclub</span></h1>
				</div>
		</div>
	</section>	
	<section class="main-container">
		<div class="createOuterWrapper bg-gray">
			<div class="container">
				<div class="createWrapper clearfix accLandOuter">
					<!-- <form action="#" id="createAccount" class="clearfix accLandOuter"> -->
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 createBoxWrapper">
									<div class="bg-white acc-col-4">
											<cms:pageSlot position="Section1" var="feature" element="div" class="clearfix bg-light-blue">
												<cms:component component="${feature}" element="div" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar"/>
											</cms:pageSlot>
									
											<cms:component uid="section2-JspIncludeComponent-for-joinBabyClubLoginPage"/>
								</div>
							</div>
						</div>
					<!-- </form> -->
				</div>
			</div>
		</div>
	</section>
</template:page>