<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>


	<div class="articleTop medicalMem-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<cms:pageSlot position="Section1A" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 medical-image">
					<cms:pageSlot position="Section1B" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>


			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix articlePage">
		<div class="container">
			<div class="row">
				
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="contentContainer helpinghand_wrap">
						<cms:pageSlot position="Section2A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>

					<div class="contentContainer helpinghand_wrap">
						<cms:pageSlot position="Section3" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>

				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hand_right">
					<aside>
						<div class="bg-white">
							<div class="articleSidebar clearfix">
								<div class="articleSideWrapper clearfix">
									<cms:pageSlot position="Section2B1" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
						</div>
						<div class="bg-white helping-hand">
							<cms:pageSlot position="Section2B2" var="feature">
									<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						
						<div class="bg-white">
							<div class="articleSidebar clearfix">
								<div class="articleSideWrapper clearfix">

									<cms:pageSlot position="Section2B3" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
						</div>
						<div class="bg-white helping-links">
							<div class="articleSidebar clearfix">
								<div class="articleSideWrapper clearfix">
									<cms:pageSlot position="Section2B4" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
						</div>
						
						
					</aside>
				</div>
			</div>
		</div>
	</section>

</template:page>