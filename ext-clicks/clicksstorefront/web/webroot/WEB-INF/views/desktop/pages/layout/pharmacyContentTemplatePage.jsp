<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<cms:pageSlot position="Section1A" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<cms:pageSlot position="Section1B" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix clinic-wrap">
		<div class="container">
			<div class="row">

				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer clearfix">
						<cms:pageSlot position="Section2A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						
						<div class="pharmacyclini_wrap">
							<div class="row">
								<cms:pageSlot position="Section3A" var="feature">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<cms:component component="${feature}" />
									</div>
								</cms:pageSlot>
							</div>
						</div>
						
						<cms:pageSlot position="Section4" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					
					<div class="vitSupBlock pharSideCol clearfix">
					 <cms:pageSlot position="Section2B" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
					
					
						 <cms:pageSlot position="Section3B" var="feature">
							<div class="pdp-second-content pharHead">
							<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
				 	
				</div>
			</div>
		</div>
	</section>

</template:page>