<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="myaccount" tagdir="/WEB-INF/tags/desktop/myaccount"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<template:page pageTitle="${pageTitle}">
<input id="contextPath" type="hidden" value="${contextPath}">
	<div id="wrapper">
			<div class="clearfix bg-white editprofiletop order-top-space">
		<div class="container">
			<div class="row">
				<!-- Profile Image  -->
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-5">
				<c:choose>
				<c:when test="${null != profilePicURL}">
				<div id="cusProfilePic" class="img-circle img-responsive">
				<img class="img-circle img-responsive" src="${profilePicURL}"/>
				</div>
				</c:when>
				<c:otherwise>
						<cms:pageSlot position="Section1A" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
				</c:otherwise>
				</c:choose>
				</div>
				<!-- End Profile Image  -->

				<!-- Heading - My clicks  -->
				<div class="col-lg-10 col-md-10 col-sm-9 col-xs-7">
					<h1>
						Update your <span>personal details</span>
					</h1>
<%-- 					<a href="javascript:void(0)" id="opener">Change photo</a> <a id="deletePhoto" href="${contextPath}/my-account/removeProfilePicture">Delete photo</a> --%>
				</div>
				<div id='picUploadMessage'><font color="red" >${profilePicMessage}</font></div>
				<!-- End Heading - My clicks  -->
			</div>
	</div>
		</div>
		<section class="main-container">
			<div class="contentWrapper bg-gray">
				<div class="container">
					<div class="row">
						<div class="accLandOuter clearfix accountAside">

							<!-- Left side -->
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="filter">

									<!-- Left side first link -->
									<div class="my-account-block bg-white">
										<h2>
											<a href="javascript:void(0)" onclick="window.location='${contextPath}/my-account'"><spring:theme
													code="myaccount.summary.title" /></a>
										</h2>
									</div>
									<!-- End Left side first link -->

									<ul class="panel-group" id="accordion">
										<!-- Left Drop down 1 - Accordion -->
										<cms:pageSlot position="SideNavigation" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
										<!-- End Left Drop down 1 - Accordion-->
									</ul>
									<!-- End Last Drop down  -->
								</div>
							</div>
							<!-- End Left side -->

							<!-- Right Side Content  -->
							<cms:pageSlot position="Section1B" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<cms:pageSlot position="BodyContent" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							<!-- Right Side Content  -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div id="dialog" title="Upload your Latest Photo"> 
    <form:form method="POST" action="${contextPath}/my-account/uploadPicture"   enctype="multipart/form-data" commandName="fileUploadForm"> 
    <h1 id="headerPicUpload"  style="display:none">Select file to upload:</h1>
        <spring:bind path="file" ><form:input path="file" type="file" name="file" size="60" /></spring:bind><br />
        Supported file format : 
		<strong>png, jpg, jpeg</strong>
		<p>Uploading a new photo will replace the existing photograph.</p>
		<p id="picErrorMsg" style="display:none">
		<font  color="red">Please select picture!</font>
		</p>
		<p id="picExtErrorMsg" style="display:none"><font color="red" >Invalid extension!</font></p>
        <br /> <form:input path="" type="submit" value="Upload" class="cusPicUploadbtn btn btn-small"/>
   </form:form>
</div>
</template:page>
