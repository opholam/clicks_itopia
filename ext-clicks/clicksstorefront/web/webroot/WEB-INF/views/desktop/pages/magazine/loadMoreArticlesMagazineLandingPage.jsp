<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

	<c:forEach var="article" items="${articles}" varStatus="status" begin="${initialValue}" end="${endValue-1}">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 appendLoadArticles" >
			<div class="tabThumbWrapper">
				<div class="tabThumbImage">
					<c:choose>
						<c:when test="${not empty article.sideImage1.url}">
								<img src="${article.sideImage1.url}" alt="${article.sideImage1.altText}">
						</c:when>
						<c:otherwise>
								<img src="${themeResourcePath}/clicks/image/missing-plp.jpg"/>
						</c:otherwise>
					</c:choose>				
					<div class="TagWrapper">
					<c:forEach items="${article.articleTags}" var="articleTag">
						 <span class="TagOuter">${articleTag.tagName}</span>
						 </c:forEach>
					</div>
				</div>			
				<c:if test="${article.articleType.code=='MAGAZINES'}">
					<h4><a href="${contextPath}/clubcard-magazine/article-view/${article.articleTitle}">${article.titleName}</a></h4>
				</c:if>
				<c:if test="${article.articleType.code=='HEALTH'}">
					<h4><a href="${contextPath}/health/article-view/${article.articleTitle}">${article.titleName}</a></h4>
				</c:if>
				<c:if test="${article.articleType.code=='CONDITIONS'}">
					<h4><a href="${contextPath}/health/conditions/article-view/${article.articleTitle}">${article.titleName}</a></h4>
				</c:if>
				<c:if test="${article.articleType.code=='MEDICINES'}">
					<h4><a href="${contextPath}/health/medicines/article-view/${article.articleTitle}">${article.titleName}</a></h4>
				</c:if>	
				<c:if test="${article.articleType.code=='VITAMINS'}">
					<h4><a href="${contextPath}/health/supplements/article-view/${article.articleTitle}">${article.titleName}</a></h4>
				</c:if>				
				<p>${article.titleContent}</p>
			</div>
		</div>
	</c:forEach>										
										
		
	

