<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>


<c:url value="/baby/my-account/join-babyclub" var="bayClub"/>

<c:if test="${isBabyClubCard}">
	<c:set value="disabled" var="toBeDisabled"/>
</c:if>
<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>

<!-- script for tweeter plug-in  -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<!-- script for tweeter plug-in  -->

	<div class="bc-landingTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<cms:pageSlot position="Section1A" var="feature">
								<cms:component component="${feature}"/>
							</cms:pageSlot>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
						<cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
					<c:choose>
						<c:when test="${isBabyClubCard}">
						<a href="${bayClub}" class="btn btnBig" ${toBeDisabled}><spring:theme code="text.babyClubCard.afterLogin" text="Welcome to Baby Club"/></a>
						</c:when>
					<c:otherwise>
						<a href="${bayClub}" class="btn btnBig"><spring:theme code="text.babyClubCard.beforeLogin" text="Opt in to BabyClub now"/></a>
					</c:otherwise>
					</c:choose>
					
					
					</div>
					</div>
					
					</div>
					
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="image-center">
						<cms:pageSlot position="Section1C" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="main-container">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="accLandOuter clearfix babyClubWrapper">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<!-- baby Club Section Content  -->
						<div class=" bg-white clearfix">
							 <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 boxOuter tb-res">
							 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 borderBlock">
							 <div class="image-center bcBox">
							 	<cms:pageSlot position="Section6A" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
								<cms:pageSlot position="Section6B" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
								 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 borderBlock">
							 <div class="image-center bcBox">
							 	<cms:pageSlot position="Section7A" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
								<cms:pageSlot position="Section7B" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>	
								 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 borderBlock">
							 <div class="image-center bcBox">
							 	<cms:pageSlot position="Section8A" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
								<cms:pageSlot position="Section8B" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>	
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 borderBlock">
							 <div class="image-center bcBox">
							 	<cms:pageSlot position="Section9A" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
								<cms:pageSlot position="Section9B" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 borderBlock">
							 <div class="image-center bcBox">
							 	<cms:pageSlot position="Section10A" var="feature">
										<cms:component component="${feature}"/>
								</cms:pageSlot>
								</div>
								</div>
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 borderBlock">
							 <div class="image-center bcBox">
									<cms:pageSlot position="GoogleAd" var="feature">
										<cms:component component="${feature}" />
								</cms:pageSlot>
								</div>
								</div>
							 </div>
							 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fb-feed">
							
							<!--Facebook Page plugin code  -->
							<div class="fb-page" data-href="https://www.facebook.com/BabyClubbyClicks/?fref=ts" data-small-header="false" 
								data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"
							 	data-show-posts="false"><div class="fb-xfbml-parse-ignore">
							 	<blockquote cite="https://www.facebook.com/BabyClubbyClicks/?fref=ts">
							 		<a href="https://www.facebook.com/BabyClubbyClicks/?fref=ts">Clicks</a>
							 	</blockquote>
							 	</div>
							</div>
							<!-- script for tweeter plug-in  -->
														 
							<a class="twitter-timeline"
							  data-widget-id="640884305854599169"
							  href="https://twitter.com/TwitterDev"
							  width="300"
							  height="550">
							Tweets by @TwitterDev
							</a>
							<!-- <a class="twitter-timeline" data-widget-id="600720083413962752"
							href="https://twitter.com/TwitterDev"
							data-chrome="nofooter noborders"> Tweets by @TwitterDev </a> -->
						<!-- script for tweeter plug-in  -->
	
							 </div>
						</div>
						<!-- baby Club Section Content Ends -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<div class="bg-gray specialBlock clearfix productRange tb-res brandSlide carouselCount">
		<div class="container">	
				<cms:pageSlot position="Section3A" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
		</div>
	</div>
	
	
	
		<!-- Online baby articles Section  -->
	<div class="bg-gray babyArticleLand">
		<div class="container">
			<div class="clearfix moreCategory babyArticle bg-white">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 babyArticleTitle">
						<div class="pull-left">
						<cms:pageSlot position="Section4A" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>						
						</div>
					</div>
					
				<div class="babyArticlesOuter clearfix">
						<cms:pageSlot position="Section4C" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
				</div>
				
			
		</div>
		<cms:pageSlot position="Section4B" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>						
		</div>
			
	</div>
	
	<!-- Clicks wellness center -->
	<%-- <cms:pageSlot position="Section5A" var="feature">
		<cms:component component="${feature}"/>
	</cms:pageSlot> --%>
	<!-- Clicks wellness center -->
	<!-- Online baby articles Section Ends -->
	
</template:page>