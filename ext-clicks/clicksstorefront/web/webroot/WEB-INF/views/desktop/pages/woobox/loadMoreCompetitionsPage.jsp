<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:set value="${fn:length(competitions)}" var="TotalCompetitions"/>
<c:forEach var="competition" items="${competitions}"  varStatus="status" begin="0" end="${pageSize}">
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 competitionsCount">
		<div class="tabThumbWrapper">
			<a href="${contextPath}/competitions/view/${competition.code}"><div class="tabThumbImage">
				<img	src="${competition.media.url}" alt="${competition.media.altText}">
					<div class="TagWrapper">
						<c:forEach items="${competition.tags}" var="competitionTag">
							<span class="TagOuter">${competitionTag}</span>
						</c:forEach>
					</div>
			</div>
			<p>${competition.title}</p>
			</a>
		</div>
	</div>
</c:forEach>
<c:if test="${TotalCompetitions > pageSize }">
	<div class="text-center  load-more-competitions">
		<button class="btn btnBig">Load more competitions</button>
	</div>
</c:if>
