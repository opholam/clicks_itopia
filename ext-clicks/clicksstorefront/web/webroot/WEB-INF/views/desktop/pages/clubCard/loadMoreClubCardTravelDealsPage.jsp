<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
				
	<c:forEach items="${travelDeals}" var="travelDeal" begin="${initialValue}" end="${endValue}">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 appendTravelDeals">
			<div class="travelOuterWrapper">
				<div class="tabThumbImage">
					<img src="${travelDeal.picture.downloadURL}" alt="${travelDeal.picture.altText}" />
				</div>
				<div class="travelRateTitle clearfix"> <h4><a href="${request.contextPath}/clubcardtravel/traveldeals/${travelDeal.referenceNo}">${travelDeal.travelLocation}</a></h4>
					<div class="travelRateOuter clearfix"><span>From</span><div class="travelRate">${travelDeal.fromPrice}</div></div>
				</div>
					<h5>${travelDeal.name}</h5>
					<div class="greyBox">Travel between <strong>
					<fmt:formatDate value="${travelDeal.startDate}" /> and <fmt:formatDate value="${travelDeal.endDate}" /></strong></div>
				<p>${travelDeal.description}</p>
				<div class="referenceTravel">Reference No: ${travelDeal.referenceNo}</div>
			</div>
		</div>
	</c:forEach>
							
					