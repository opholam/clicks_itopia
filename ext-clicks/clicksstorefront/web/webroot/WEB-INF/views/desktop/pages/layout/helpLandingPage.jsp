<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<div class="clearfix bg-white helpTop order-top-space">
		<div class="container">

			<cms:pageSlot position="Section1" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>

		</div>
	</div>
	<section class="main-container helpOuterWrapper">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="clearfix helpInnerWrapper">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="bg-white">
								<div class="accordion">
									<cms:pageSlot position="Section2A" var="feature">
										<cms:component component="${feature}" element="div"
											class="helpSection" />
									</cms:pageSlot>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="bg-white">

								<cms:pageSlot position="Section2B" var="feature">
									<cms:component component="${feature}" element="div"
										class="helpSection" />
								</cms:pageSlot>

							</div>
						</div>
					</div>
					<!-- End wrapper div -->
					<%-- <c:if test="${cmsPage.title eq 'Help'}"> --%>

					<cms:pageSlot position="Section3" var="feature">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="contactCommonSection clearfix bg-white">
								<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
									<cms:component component="${feature}" />
								</div>
							</div>
						</div>
					</cms:pageSlot>

					<%-- </c:if> --%>

				</div>
			</div>
	</section>

</template:page>