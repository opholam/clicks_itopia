<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	
	<section class="rewards account">
		<div class="container">
			<cms:pageSlot position="Section1" var="feature">
				<cms:component component="${feature}" element="div" class="section1"/>
			</cms:pageSlot>			
		</div>
	</section>
	<section class="main-container bg-gray clearfix articlePage" style="position: relative;">
		<div class="container">
			<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section2A" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<div class="CC_rewards_wrap clearfix">
						<div class="col-lg-7 col-md-7">
						<h4><spring:theme code="clubcard.text1"/></h4>
							<ol>
								<cms:pageSlot position="Section2B" var="feature">
									<li><cms:component component="${feature}" /></li>
								</cms:pageSlot>
							</ol>
									<c:choose>
									<c:when test="${not empty user.memberID}">
									<cms:pageSlot position="Section3A" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
									</c:when> 
									<c:otherwise>
									<cms:pageSlot position="Section2C" var="feature">
										<cms:component component="${feature}"  />
									</cms:pageSlot>
									</c:otherwise>
									</c:choose>
						</div>
						<div class="col-lg-5 col-md-5 text-center">
							<cms:pageSlot position="Section2D" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
					
					<div class="clubcard_icons_wrap clearfix">
						<%-- <cms:pageSlot position="Section3A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot> --%>

						<%-- <cms:pageSlot position="Section3B" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot> --%>

						<div class="clubcard_icons clearfix">
							<cms:pageSlot position="Section3C" var="feature">
								<div class="col-md-4 col-sm-12 col-xs-12 clubMagindex">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
						<div class="cashback_dates clearfix">
						<cms:pageSlot position="Section3D" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						
<%-- 						<cms:pageSlot position="Section3E" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
 --%>						</div>
					</div>
					<%-- <div class="accLandOuter club_partner">
					<cms:pageSlot position="Section4A" var="feature" >
						<cms:component component="${feature}" />
					</cms:pageSlot>
					
					<div class="partner-wrap clearfix bg-white">
						<div class="partner-spacing col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<ul class="only-partner-slider slider-component" data-classname="only-partner-slider" data-maxslide="7">
							<cms:pageSlot position="Section4B" var="feature">
							<li><div class="col-md-12 col-sm-12 col-xs-12 ">	<cms:component component="${feature}" /></div></li>
							</cms:pageSlot>
							</ul>
						</div>
					</div>
					</div> --%>
					<div class="clearfix bgdarkblue">
						<cms:pageSlot position="Section5A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>

					<div class="financial_data">

						<cms:pageSlot position="Section5B" var="feature">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 border-block">
								<div class="doubleBlockWrapper promo_blocks">
									<div class="doubleBlock">
										<cms:component component="${feature}" />
									</div>
								</div>
							</div>
						</cms:pageSlot>

						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 border-block">
									<cms:pageSlot position="GoogleAds" var="feature">
										<cms:component component="${feature}"/>
									</cms:pageSlot>
						</div>
						
					</div>

					<div class="home-promotions clubpromo">
						<div class="clearfix bgred">
							<cms:pageSlot position="Section6A" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						<div class="promo-wrap clearfix">
							<ul class="home-promo-slider categoryLanding product-slider clearfix"
								data-classname="home-promo-slider" data-minslide="3">
								<cms:pageSlot position="Section6B" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</ul>
						</div>
					</div>
					
					<div class="moreCategory moreclubindex clearfix">
						<div class="clearfix bgdarkblue">
							<cms:pageSlot position="Section7A" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						<div class="home-club-wrap clearfix">
							<cms:pageSlot position="Section7B" var="feature">
								<cms:component component="${feature}" element="div" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 clubMagindex"/>
							</cms:pageSlot>
						</div>
					</div>
				
				</div>
		</div>
	</div>
</section>	
</template:page>