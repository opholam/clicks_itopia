
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/addons/clickscheckoutaddon/desktop/checkout/multi" %>

<div data-role="content">
<div class="container">
<div id="globalMessages">
			<%--  <common:globalMessages />  --%>
			<common:registerGlobalMessages />
</div>
</div>

	<c:url value="${!checkoutForm? action: '/login/checkout'.concat(action) }" var="submitAction" />

<c:if test="${fn:containsIgnoreCase(action, 'enroll')}">
<section class="checkoutTop">
		<div class="container">
			<c:if test="${!checkoutForm }">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="acc-heading">
					<h1><spring:theme code='customer.enroll.create.account'/></h1>
					
				</div>
				
			</div>
			</c:if>
			<c:if test="${checkoutForm }">				
			<div class="row">
			<%-- <div class="pull-right backToLink">
							<c:choose>
							<c:when test="${not empty returnToShopping}">
							<a href="${returnToShopping}">Back to shopping</a>
							</c:when>
							<c:otherwise>
							<a href="${contextPath}">Back to shopping</a>
							</c:otherwise>
							</c:choose>
							</div> --%>
				<multi-checkout:checkoutProgressBar stepName="Register & Checkout" steps="${checkoutSteps}" progressBarId="register"/>
			</div>
			</c:if>
		</div>
		</section>
		<customer:clicksRegister actionNameKey="customer.enroll.button" registerNonCCKey="" registerNonCCAction="" action="${submitAction}" />	
		
</c:if>

<c:if test="${fn:containsIgnoreCase(action, 'joincc')}">

		<div class="container">
							<c:if test="${!checkoutForm }">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="acc-heading">
					<h1><spring:theme code='customer.enroll.joinClubcard'/></h1>
				</div>
			</div>
			</c:if>
			<c:if test="${checkoutForm }">
			<div class="row">
				<%-- <div class="pull-right backToLink">
							<c:choose>
							<c:when test="${not empty returnToShopping}">
							<a href="${returnToShopping}">Back to shopping</a>
							</c:when>
							<c:otherwise>
							<a href="${contextPath}">Back to shopping</a>
							</c:otherwise>
							</c:choose>
							</div> --%>
				<multi-checkout:checkoutProgressBar stepName="Register & Checkout" steps="${checkoutSteps}" progressBarId="register"/>
			</div>
			</c:if>
		</div>

		<customer:enroll actionNameKey="customer.joincc.button"  action="${submitAction}"/>
		
</c:if>


<c:if test="${fn:containsIgnoreCase(action, 'registerCC')}">

		<div class="container">
							<c:if test="${!checkoutForm }">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="acc-heading">
					<h1>Register Clubcard User</h1>
				</div>
			</div>
			</c:if>
			<c:if test="${checkoutForm }">
			<div class="row">
				<%-- <div class="pull-right backToLink">
							<c:choose>
							<c:when test="${not empty returnToShopping}">
							<a href="${returnToShopping}">Back to shopping</a>
							</c:when>
							<c:otherwise>
							<a href="${contextPath}">Back to shopping</a>
							</c:otherwise>
							</c:choose>
							</div> --%>
				<multi-checkout:checkoutProgressBar stepName="Register & Checkout" steps="${checkoutSteps}" progressBarId="register"/>
			</div>
			</c:if>
		</div>
		
	<customer:clicksRegister actionNameKey="customer.registerCC.button" registerNonCCKey="" registerNonCCAction="" action="${submitAction}"/>
</c:if>


<c:if test="${fn:containsIgnoreCase(action, 'createAccount') or fn:containsIgnoreCase(action, 'registerNonCC')}">
	
		<div class="container">
							<c:if test="${!checkoutForm }">
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="acc-heading">
					<h1><spring:theme code='customer.enroll.create.account'/></h1>
				</div>
			</div>
			</c:if>
			<c:if test="${checkoutForm }">
			<div class="row">
				<%-- <div class="pull-right backToLink">
							<c:choose>
							<c:when test="${not empty returnToShopping}">
							<a href="${returnToShopping}">Back to shopping</a>
							</c:when>
							<c:otherwise>
							<a href="${contextPath}">Back to shopping</a>
							</c:otherwise>
							</c:choose>
							</div> --%>
				<multi-checkout:checkoutProgressBar stepName="Register & Checkout" steps="${checkoutSteps}" progressBarId="register"/>
			</div>
			</c:if>
		</div>
		
	<c:url value="${!checkoutForm? RegisterAction: '/login/checkout'.concat(RegisterAction) }" var="regNonCCsubmitAction" />
	<customer:clicksRegister actionNameKey="customer.create.account.Retrieve"
		action="${submitAction}" registerNonCCKey="customer.registerNonCC.button" registerNonCCAction="${regNonCCsubmitAction}" />
		
</c:if>

</div>

