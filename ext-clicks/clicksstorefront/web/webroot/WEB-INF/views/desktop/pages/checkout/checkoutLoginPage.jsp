<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>

<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<div class="span-24">
		<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
			<c:set var="spanStyling" value="span-8" />
		</sec:authorize>
		<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
			<c:set var="spanStyling" value="span-12" />
			<c:set var="spanStylingLast" value=" last" />
		</sec:authorize>

		<%-- 		<cms:pageSlot position="LeftContentSlot" var="feature" element="div" class="${spanStyling}"> --%>
		<%-- 			<cms:component component="${feature}"/> --%>
		<%-- 		</cms:pageSlot> --%>

		<%-- 		<cms:pageSlot position="CenterContentSlot" var="feature" element="div" class="${spanStyling}${spanStylingLast}"> --%>
		<%-- 			<cms:component component="${feature}"/> --%>
		<%-- 		</cms:pageSlot> --%>

		<%-- 		<cms:pageSlot position="RightContentSlot" var="feature" element="div" class="${spanStyling} last"> --%>
		<%-- 			<cms:component component="${feature}"/> --%>
		<%-- 		</cms:pageSlot> --%>

		<section class="rewards account checkout-loginhead">
			<div class="container">
				<div class="acc-heading">
					<h1>Checkout</h1>
				</div>
			</div>
		</section>
		<section class="main-container">
			<div class="createOuterWrapper bg-gray">
				<div class="container">
					<div class="createWrapper clearfix ">
						<c:url value="/checkout/j_spring_security_check"
							var="loginAndCheckoutActionUrl" />


						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-push-3">
								<div class="bg-white checkoutLoginWrapper">
									<div class="clearfix bg-light-blue">
										<div
											class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>Sign in</h2>
										</div>
									</div>
									<c:if test="${not empty message}">
										<span class="errors"> <spring:theme code="${message}" />
										</span>
									</c:if>

									<div class="bg-white clearfix bc-login">
										<div class="pad-spacing form-wrap">
											<form:form action="${loginAndCheckoutActionUrl}"
												id="checkoutLogin" method="post" commandName="loginForm">

												<div class="form-group clearfix">
													<formElement:formInputBox idKey="j_username"
														labelKey="login.email" path="j_username"
														inputCSS="form-control" mandatory="true" />

												</div>
												<div class="form-group clearfix">
													<formElement:formPasswordBox idKey="j_password"
														labelKey="login.password" path="j_password"
														inputCSS="form-control password" mandatory="true" />
												</div>

												<div class="cl-quick-links clearfix">
													<div class="pull-left">
														<a href="javascript:void(0)"
															data-url="<c:url value='/login/pw/request'/>"
															class="password-forgotten forgot-block"><spring:theme
																code="login.link.forgottenPwd" /></a>
													</div>
													<div class="pull-right">
														<a href="<c:url value="/forgotten-details"/>"><spring:theme
																code="login.link.forgottenDetails" /></a>
													</div>
												</div>
												<div class="form-group clearfix">
													<formElement:formCheckbox
														idKey="_spring_security_remember_me_checkout"
														labelKey="login.remember"
														path="_spring_security_remember_me" />

												</div>
												<div class="text-center">
												<button type="submit" class="btn btn-save-change">Sign
													in &amp; Checkout</button>
													</div>
												</form:form>
												<c:url value="/login/checkout/register" var="CreateAccountAction" />
													
												<div class="separator"></div>
												<h4>New to Clicks website?</h4>
												<p>
													<strong>Create an account to shop online, earn
														ClubCard points and spend rewards</strong>
												</p>
												<div class="text-center"><button type="submit" onclick="window.location.href='${CreateAccountAction}'" class="btn btn-save-change">Register
													&amp; Checkout</button></div>
											<cms:pageSlot position="RightContentSlot" var="feature"
												element="div" class="${spanStyling} last">
												<cms:component component="${feature}" />
											</cms:pageSlot>

										</div>
									</div>
								</div>
							</div>
						</div>
						

					</div>

				</div>
			</div>
		</section>
</template:page>
