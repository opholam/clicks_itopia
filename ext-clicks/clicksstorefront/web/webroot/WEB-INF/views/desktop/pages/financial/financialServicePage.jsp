<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="financial" tagdir="/WEB-INF/tags/desktop/financial" %>

<%-- <c:url value="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/my-account/join-babyclub" var="joinClubCardToday" /> --%>
  
<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<div class="financial_top">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<cms:pageSlot position="Section1A" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="image-center">
					<cms:pageSlot position="Section1B" var="feature" element="div">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>

<section class="main-container bg-gray clearfix articlePage">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="policy_container">
					
					<financial:service/>
					
					<!-- Regent Block -->
					<div class="regent_block">
						<div class="services_wrap clearfix">
							
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 tab-content">
								<cms:pageSlot position="Section3A" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<cms:pageSlot position="Section3B" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
								
							</div>
						</div>
					</div>
					<!-- Regent Block -->
					</div>
				</div>
			</div>
		</div>
	</section>

</template:page>