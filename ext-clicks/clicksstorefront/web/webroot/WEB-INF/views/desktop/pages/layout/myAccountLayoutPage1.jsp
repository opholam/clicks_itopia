<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="myaccount" tagdir="/WEB-INF/tags/desktop/myaccount"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<template:page pageTitle="${pageTitle}">

	<div id="wrapper">
		<div class="clearfix bg-white editprofiletop order-top-space">
			<div class="container">
			<div id="globalMessages">
				<common:globalMessages />
			</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<c:choose>
						<c:when test="${not empty orderData.created}">
					<h1>
						Order placed on <span><fmt:formatDate pattern="dd MMM" value="${orderData.created}" /></span>
					</h1>
						</c:when>
						<c:otherwise>
						<h1>
						<c:set var="value" value="${cmsPage.title}" />
						<c:if test="${not fn:containsIgnoreCase(value, 'babyclub')}">
							${cmsPage.title}
						</c:if>
						<c:if test="${fn:containsIgnoreCase(value, 'babyclub')}">
						<c:set var="value" value="${isUpdate}" />
							<c:if test="${fn:containsIgnoreCase(value, 'yes')}">
							Update Babyclub
							</c:if>
							<c:if test="${fn:containsIgnoreCase(value, 'no')}">
							Join Babyclub
							</c:if>
						</c:if>
						
						</h1>
						</c:otherwise>
						</c:choose>
					</div>
					<!-- End Heading - My clicks  -->
					<div class="span-20 last accountContentPane">
			<cms:pageSlot position="TopContent" var="feature" element="div" class="accountTopContentSlot">
				<cms:component component="${feature}" element="div" class="clearfix" />
			</cms:pageSlot>
	
				</div>
			</div>
		</div>		
	</div>
		<section class="main-container">
			<div class="contentWrapper bg-gray">
				<div class="container">
					<div class="row">
						<div class="accLandOuter clearfix accountAside shopBasket">

							<!-- Left side -->
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="filter">

									<!-- Left side first link -->
									<div class="my-account-block bg-white">
										<h2>
											<a href="javascript:void(0)" onclick="window.location='${contextPath}/my-account'"><spring:theme
													code="myaccount.summary.title" /></a>
										</h2>
									</div>
									<!-- End Left side first link -->
									
									<ul class="panel-group" id="accordion">
										<!-- Left Drop down 1 - Accordion -->
										<cms:pageSlot position="SideNavigation" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
										<!-- End Left Drop down 1 - Accordion-->
									</ul>
									<!-- End Last Drop down  -->
								</div>
							</div>
							<!-- End Left side -->

							<!-- Right Side Content  -->
							<cms:pageSlot position="Section1" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							
								<cms:pageSlot position="BodyContent" var="feature" element="div" class="accountBodyContentSlot">
									<cms:component component="${feature}" element="div" class="clearfix" />
								</cms:pageSlot>
							
							
							<!-- Right Side Content  -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</template:page>