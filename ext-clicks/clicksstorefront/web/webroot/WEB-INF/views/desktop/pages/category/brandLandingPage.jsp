<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="storepickup"
	tagdir="/WEB-INF/tags/desktop/storepickup"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/desktop/nav/pagination"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<section class="main-container bg-gray brand">
		<div class="bannerOuter">
			<div class="banner">
				<cms:pageSlot position="BannerSection" var="feature">
					<cms:component component="${feature}" element="div" />
				</cms:pageSlot>

			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="brand-landing clearfix">
					<div class="col-md-3 col-sm-12 col-xs-12">

						<aside class="brandSidebar bg-white">
							<h2>${cmsPage.name}</h2>
							<ul>
								<c:forEach items="${cmsPage.brandCollectionList}"
									var="brandCollection">
									<li><a href="c/${brandCollection.code}">${brandCollection.name}</a></li>
								</c:forEach>
							</ul>
						</aside>
					</div>

					<div class="col-md-9 col-sm-12 col-xs-12">

						<c:if test="${not empty cmsPage.brandCollectionList}">
							<div class="brand-products clearfix tb-res">
								<c:forEach items="${cmsPage.brandCollectionList}"
									var="brandCollection">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="productOuter">
											<a href="c/${brandCollection.code}"> <img
												src="${brandCollection.picture.url}" /> <span>${brandCollection.name}</span></a>
										</div>
									</div>
								</c:forEach>
							</div>
						</c:if>


						<!-- brand-products Ends-->
						<div class="brand-videos bg-white clearfix tb-res">
							<cms:pageSlot position="Section1A" var="feature">
								<cms:component component="${feature}" element="div" />
							</cms:pageSlot>



						</div>

						<div class="brand-content bg-white clearfix">
							<div class="col-md-7 col-sm-6 col-xs-12">
								<div class="brand-content-inner">
									<cms:pageSlot position="Section2A" var="feature">
										<cms:component component="${feature}" element="div" />
									</cms:pageSlot>
								</div>
							</div>
							<div class="col-md-5 col-sm-6 col-xs-12">
								<cms:pageSlot position="Section2B" var="feature">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>



						<div
							class="bg-gray specialBlock clearfix productRange poductQuote brandSlide">

							<div class="bg-white clearfix">
								<h2>

									<span>${cmsPage.name}</span>&nbsp;
									<spring:theme code="bestsellers.title" />
								</h2>



								<ul class="sliderOuter1 clearfix">
									<c:forEach items="${bestSellersProducts}" var="product">

										<li>
											<div class="col-md-4 col-sm-12 col-xs-12">
												<div class="productBlock">
													<%-- 	<c:if
															test="${not empty product.price.grossPriceValueWithPromotionAppliedFormattedValue}">
															
												<p class="badges red"><span>${product.promoBadge}</span></p>
												</c:if> --%>
													<c:if
														test="${not empty product.potentialPromotions and not empty product.potentialPromotions[0].productBanner}">
														<div class="badges">
															<img class="promo"
																src="${product.potentialPromotions[0].productBanner.url}"
																alt="${product.potentialPromotions[0].description}"
																title="${product.potentialPromotions[0].description}" />

														</div>
													</c:if>
													<div class="thumb">
														<a href="p/${product.code}"> <product:productPrimaryImage
																product="${product}" format="product" /></a>
													</div>
													<div class="detailContent clearfix">
														<h5>
															<a href="p/${product.code}">${product.name}</a>
														</h5>
														<div class="content-height">
															<p>${product.summary}</p>
														</div>

														<price:fromPrice product="${product}" />



														<%-- <div class="offer-blk">

															<c:if
																test="${not empty product.price.grossPriceValueWithPromotionAppliedFormattedValue}">
								
 								
															 Was&nbsp;<c:set var="dec"
																	value="${fn:split(product.price.formattedValue, '.')}" />
																${dec[0]}<sup>${dec[1]}</sup>
																<span class="red">Save</span>
																<span class="price red"> <fmt:formatNumber
																		var="save" pattern="####.##"
																		value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
																	<c:set var="dec1" value="${fn:split(save, '.')}" />
																	R${dec1[0]}<sup>${dec1[1]}</sup>
																</span>

															</c:if>
														</div> --%>

														<div class="starWrapper">

															<product:productStars rating="${product.averageRating}" />


															<a href="#" class="btn">Buy</a>
														</div>
														<c:if
															test="${ not empty product.potentialPromotions[0].description}">
															<div class="offerDesc skyblue">
																<span>${product.potentialPromotions[0].description}</span>
															</div>
														</c:if>
													</div>
													<c:if test="${ not empty product.remarks}">
														<div class="quoteBlock">
															<div class="quoteBlockInner">
																<p>"${product.remarks}"</p>
															</div>
														</div>
													</c:if>
												</div>
											</div>
										</li>

									</c:forEach>
								</ul>



							</div>
						</div>

						<div
							class="bg-gray specialBlock clearfix productRange tb-res brandSlide">

							<div class="contentWrapper clearfix">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widthFull">
									<div class="pull-left">
										<h2>
											${cmsPage.name}<span>&nbsp;range</span>
										</h2>
									</div>
									<div class="pull-right text-right">
										<a class="arrow-right" href="javascript:void(0)">See all</a>
									</div>
								</div>
							</div>
							<ul class="sliderOuter1 clearfix">

								<c:forEach items="${rangeProducts}" var="rangeProduct">
									<li>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<div class="productBlock">
												<c:if
														test="${not empty rangeProduct.potentialPromotions and not empty rangeProduct.potentialPromotions[0].productBanner}">
														<div class="badges">
															<img class="promo"
																src="${rangeProduct.potentialPromotions[0].productBanner.url}"
																alt="${rangeProduct.potentialPromotions[0].description}"
																title="${rangeProduct.potentialPromotions[0].description}" />

														</div>
													</c:if>
												<div class="thumb">
													<a href="p/${product.code}"> <product:productPrimaryImage
															product="${rangeProduct}" format="product" /></a>
												</div>
												<div class="detailContent clearfix">
													<h5>
														<a href="javascript:void(0)">${rangeProduct.name}</a>
													</h5>
													<div class="content-height">
														<p>${rangeProduct.summary}</p>
													</div>
													<price:fromPrice product="${rangeProduct}" />

												<%-- 	<div class="offer-blk">

														<c:if
															test="${not empty rangeProduct.price.grossPriceValueWithPromotionAppliedFormattedValue}">
								
 								
															 Was&nbsp;<c:set var="dec"
																value="${fn:split(rangeProduct.price.formattedValue, '.')}" />
																${dec[0]}<sup>${dec[1]}</sup>
															<span class="red">Save</span>
															<span class="price red"> <fmt:formatNumber
																	var="save" pattern="####.##"
																	value="${rangeProduct.price.value - rangeProduct.price.grossPriceWithPromotionApplied}" />
																<c:set var="dec1" value="${fn:split(save, '.')}" />
																R${dec1[0]}<sup>${dec1[1]}</sup>
															</span>

														</c:if>
													</div> --%>

													<div class="starWrapper">
														<product:productReviewSummary product="${rangeProduct}" />
														<a href="#" class="btn">Buy</a>
													</div>
													<c:if
														test="${ not empty rangeProduct.potentialPromotions[0].description}">
														<div class="offerDesc red">${rangeProduct.potentialPromotions[0].description}</div>
													</c:if>
												</div>
											</div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</div>




					</div>

				</div>
			</div>
		</div>
	</section>
</template:page>