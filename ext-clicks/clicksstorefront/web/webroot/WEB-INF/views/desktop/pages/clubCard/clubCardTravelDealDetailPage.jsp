<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>

<template:page pageTitle="${pageTitle}">


	<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<h1 class="main-title">${travelDeal.name}</h1>
					<p>${travelDeal.description}</p>
					<div class="authorWrapper">
						<div class="authorInfoWrapper">
							<div class="authorInfo">by the Reference No:
								${travelDeal.referenceNo}</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="image-center">
						<img src="${travelDeal.picture.downloadURL}" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix articlePage indTravel">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer">${travelDeal.summary}
					</div>
					<div class="bg-white clearfix disclaimerWrapper">
						<p>${travelDeal.travelTerms}</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<aside>
						<h2>Enquire about this offer</h2>
						<div class="bg-white">
							<div class="pad-spacing form-wrap">
								<div id="globalMessages">
									<common:globalMessages />
								</div>
								<form:form commandName="contactUsForm" action="${request.contextPath}/clubcardtravel/traveldeals/sendenquiry/${travelDeal.referenceNo}" id="contactUsForm" class="clearfix" >
									<formElement:formInputBox idKey="travel.enquiry.firstName" labelKey="travel.enquiry.firstName" path="firstName" inputCSS="text" mandatory="true"/>
									<formElement:formInputBox idKey="travel.enquiry.lastname" labelKey="travel.enquiry.lastname" path="lastName" inputCSS="text" mandatory="true"/>
									<formElement:formInputBox idKey="travel.enquiry.contactNo" labelKey="travel.enquiry.contactNo" path="contactNo" inputCSS="text" mandatory="true"/>
									<formElement:formInputBox idKey="travel.enquiry.email" labelKey="travel.enquiry.email" path="email" inputCSS="text" mandatory="true"/>
									<formElement:formInputBox idKey="travel.enquiry.clubcardNo" labelKey="travel.enquiry.clubcardNo" path="clubcardNo" inputCSS="text" mandatory="false"/>
									<button type="submit" class="btn btnBig">Send Enquiry</button>
								</form:form>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</section>
</template:page>