<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<h2>${competition.title}</h2>
						<p>${competition.summary}</p>
						<c:if test="${competitionStatus=='Closed'}" >
							<div class="pull-left"><h4 class="btn-small competionClose"><spring:theme code="competition.closed.text"/></h4>
							 <c:forEach items="${competition.tags}" var="tag">
								<h4 class="btn-small">${tag}</h4>
							</c:forEach>
							</div>
						</c:if>
						<c:if test="${competitionStatus=='Open'}" >
							<div class="pull-left"><h4 class="btn-small competionOpen"><spring:theme code="competition.open.text"/>${competitionClosedDate}</h4>
							 <c:forEach items="${competition.tags}" var="tag">
								<h4 class="btn-small">${tag}</h4>
							</c:forEach>
							</div>
						</c:if>
						<div class="moreLinks">
						<a href="javascript:void(0)" onclick="javascript:window.location='mailto:?subject=${article.titleName}&body=' + window.location;" class="email">Email</a>
						<a href="javascript:void(0)" class="share">Share</a>
						<!-- Share via social networking sites -->
						    <div class="shareProduct a2a_kit social-share">
						     <span class="popup-left-arrow"></span>
						     <ul>
						       <li><a title="facebook" target="_blank" class="a2a_button_facebook icon-sprite icon-video-fb-share facebook_icon" href="https://www.facebook.com/"><img />Facebook</a></li>
						       <li><a title="twitter" target="_blank" class="a2a_button_twitter icon-sprite icon-video-tweet-share twitter_icon" href="https://twitter.com/"><img />Twitter</a></li>
						       <li><a title="Google +" target="_blank" class="a2a_button_google_plus icon-sprite icon-video-gp-share google_icon" href="https://plus.google.com/"><img />Google +</a></li>   
						     </ul>
						    </div>
						    <!-- Share via social networking sites -->
						    </div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<img src="${competition.media.url}" alt="${competition.media.altText}"/>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container cI articlePage" id="competitions_landing">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="promo-wrap clearfix health-slider-outer">
						<cms:pageSlot position="Section1" var="feature">
							<cms:component component="${feature}" element="div"/>
						</cms:pageSlot>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<aside>
						<div class="ad_block">
							<cms:pageSlot position="Section2" var="feature" element="div"
								class="span-6 zoneA">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
						<c:if test="${not empty competition.relatedWooBox}">
							<h2 class="bg-blue">More Competitions</h2>
							<div class="bg-white">
								<c:forEach var="relatedCompetition"
									items="${competition.relatedWooBox}">
									<div class="articleSidebar clearfix">
										<div class="articleSideWrapper clearfix">
											<div class="image-center">
												<img src="${relatedCompetition.media.url}"
													alt="${relatedCompetition.media.altText}" />
											</div>
											<h3>
												<a href="${relatedCompetition.code}">${relatedCompetition.title}</a>
											</h3>
											<p>${relatedCompetition.summary}</p>
										</div>
									</div>
									<div class="separator"></div>
								</c:forEach>
							</div>
						</c:if>
					</aside>
				</div>
				
			</div>
		</div>
	</section>
							
</template:page>