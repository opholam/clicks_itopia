<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">


	<div class="clearfix bg-white editprofiletop forgotten-top">
		<div class="container">
		<div id="globalMessages">
			<common:globalMessages />
		</div>
			<div class="row">
				<!-- Heading - My clicks  -->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<!-- <h1>Forgotten details</h1> -->
						<h1>${cmsPage.title}</h1>
				</div>
				<!-- End Heading - My clicks  -->
			</div>
		</div>
	</div>
	<section class="main-container">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="accLandOuter forgotten-wrap clearfix">
						<!-- Right Side Content  -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<form id="clubCardRegister" action="" method="post"
								novalidate="novalidate">
								<div class="row">
									<!-- Have Club card -->
									<cms:pageSlot position="LeftContentSlot" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
									<!-- Have Club card End-->

									<!-- Dont have Club card -->
									<cms:pageSlot position="RightContentSlot" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
									<!-- Dont have Club card End -->
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</template:page>