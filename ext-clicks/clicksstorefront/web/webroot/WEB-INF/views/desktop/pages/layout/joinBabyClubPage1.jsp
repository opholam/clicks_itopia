<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"> 
						<!-- baby Club Section Content  -->
							<div class="babyClubSection">
								<div class="babyContentOuter bg-white clearfix">
									<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
										<div class="babyContent">
										
										<cms:pageSlot position="Section1A" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
											<%-- <cms:component uid="section1A-paragraph-component-for-joinBabyClubPage"/> --%>

										</div>
									</div>	
									<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<div class="babyContentImg">
										<cms:pageSlot position="Section1B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
											<%-- <cms:component uid="section1B-CMSImage-component1-for-joinBabyClubPage"/> --%>
										</div>
									</div>	
								</div>	
							</div>
							<!-- baby Club Section Content Ends -->
							<div class="babyMiddleOuter bg-white clearfix">
								<div class="aboutFamily pdp-second-content">
									<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 babyArticleTitle">
										<cms:pageSlot position="Section2A" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
										<%-- <cms:component uid="section2A-paragraph-component-for-joinBabyClubPage"/> --%>
										
										<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
											<cms:pageSlot position="Section2B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
											<%-- <cms:component uid="section2B-JspIncludeComponent-for-joinBabyClubPage"/> --%>
											
										</div>
									</div>
									<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 babyOffers">
									<div class="row">
									 <div class="contentWrapper clearfix">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
												<div class="pull-left">
												<h2 class="arrow-right">
													<cms:pageSlot position="BabyOffersLink" var="feature">
														<cms:component component="${feature}" />
													</cms:pageSlot>
												</h2>
												</div>
											</div>
										</div>
									<cms:pageSlot position="Section2C" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
										<%-- <cms:component uid="section2C-product-carousel-component-for-joinBabyClubPage"/> --%>
									</div>
									</div>
								</div>
							</div>
							<!-- Online baby articles Section  -->
							<div class="clearfix moreCategory bg-white babyArticle">
								 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 babyArticleTitle">
										<div class="pull-left">
										<cms:pageSlot position="Section3A" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
											<%-- <cms:component uid="section3A-paragraph-component-for-joinBabyClubPage"/> --%>
										</div>
									</div>
								<div class="clubCardBot">
								<cms:pageSlot position="Section3B" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
										<%-- <cms:component uid="section3B-CmsMediaLink-component1-for-joinBabyClubPage"/>
										<cms:component uid="section3B-CmsMediaLink-component2-for-joinBabyClubPage"/> --%>
								</div>
							</div>
							<!-- Online baby articles Section Ends -->
						</div>
