<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
	
<c:url var="profileUrl" value="/my-account/link-email-to-cc" />
<!-- Right Side Content  -->
<form:form action="${contextPath}/my-account/link-email-to-cc/clubcardProcessor" method="post" commandName="registerClubCardForm">
					<div class="row acc-row-5">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5">
									<div class="bg-white">
									<div class="clearfix bg-light-blue">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>1.ClubCard Details</h2>
										</div>
									</div>
									<div class="clearfix  acc-col-5">
										<div class="pad-spacing form-wrap">
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="ccNumber" labelKey="customer.cc.link.number" path="memberID" inputCSS="text"/>
											</div>
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.cc.link.EMailAddress" labelKey="customer.cc.link.EMailAddress" path="eMailAddress" inputCSS="text mandatory" mandatory="true"/>
											</div>
											<!-- If the account/Hybris email matches the ClubCard email, then the 'Different email addresses' section does not show. --> 
											<div class="emailWrap">
											<h4>Different email addresses</h4>
											<p>To make things easier for you, Clicks will only remember one email address for ClubCard communications and your online account.</p>
											<p>Please choose which email address you wish to sign in with online and receive emails from Clicks:</p>
												<div class="form-inline" >
	           									<div class="form-group margin-spacing clearfix radioOuter">
												<form:radiobutton path="emailId" value="1" label="${registerClubCardForm.eMailAddress}" id="diffEmail1 " cssClass="radio-hide" for="diffEmail1"/>
												 </div> 
												 <div class="form-group clearfix radioOuter">
												 <form:radiobutton path="emailId" value="2" label="${registerClubCardForm.userId}" id="diffEmail2" cssClass="radio-hide" for="diffEmail2" />
												 </div>
	           									</div>
											</div>
											<!-- email block over -->  
										</div>
									</div>
								</div>
							</div>
							<!-- Personal details start -->
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha5">
										<div class=" bg-white  clearfix">
											<div class="clearfix bg-light-blue">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
														<h2>2. Personal details</h2>
												</div>
											</div>
											<div class="clearfix acc-col-5">
												<div class="pad-spacing form-wrap">
													<div class="form-group clearfix">
														<formElement:formInputBox idKey="fname" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text mandatory form-control" mandatory="true" />
													</div>
													<div class="form-group clearfix">
														<formElement:formInputBox idKey="lname" labelKey="customer.edit.profile.PreferedName" path="preferedName" inputCSS="text"/>
													</div>
													<div class="form-group clearfix">
														<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text" mandatory="true"/>
													</div>

													<div class="form-group saResident" id="dobHide">
											<label>Are you a South African resident?</label>
											<div class="form-group clearfix">
												
												<div class="form-group clearfix">
													<div class="clearfix">
														<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone radioOuter saResidentEnrollYOuter">
															<div class="custom_hide">
															<form:radiobutton path="SAResident" value="1" label="yes" id="saResidentEnrollY " cssClass="radio-hide" name="saResi12"/>
																 </div>	
														</div>
														<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
															<formElement:isClubcardFormInputBox idKey="bb" labelCSS="" labelKey="" path="IDNumber" inputCSS="" mandatory="true"  />
														</div>
													</div>
													<div class="clearfix radioOuter">
														<div class=" ">
			           										<form:radiobutton path="SAResident" value="0" label="No" id="saResidentEnrollN" cssClass="radio-hide" name="saResi12"  />
			           									</div>
													</div>
												</div>
												
												<div class=" clearfix" id="cc"> 
													<label>Date of Birth</label>
													<div class="dob">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
															<div class="dobWrapper">
																<formElement:DOBformSelectBox selectCSSClass="dobSa12 dateCheck" labelCSS="dobSa12" idKey="customer.create.account.SA_DOB_day" labelKey="customer.create.account.SA_DOB_day" path="SA_DOB_day" mandatory="true" items="${days}" />
															</div>
														</div>
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
															<div class="monthWrapper">
																	<formElement:DOBformSelectBox selectCSSClass="monthCheck" idKey="customer.create.account.SA_DOB_month" labelKey="customer.create.account.SA_DOB_month" path="SA_DOB_month" mandatory="true" items="${months}" />
															</div>
														</div>
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
															<div class="yearWrapper">
																<formElement:DOBformSelectBox selectCSSClass="yearCheck" idKey="customer.create.account.SA_DOB_year" labelKey="customer.create.account.SA_DOB_year" path="SA_DOB_year" mandatory="true" items="${years}" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
													<div class="form-inline" >
															<div class="gender" ><label>Gender</label> </div> 
															<div class="form-group margin-spacing clearfix radioOuter">
													<form:radiobutton path="gender" value="1" label="Male"  name="male" cssClass="radio-hide" />
												</div>
												<div class="form-group clearfix radioOuter">
													<form:radiobutton path="gender" value="2" label="Female"  name="female" cssClass="radio-hide" />
												</div>
														</div>
														<div id="error" style="display:none">add some text</div>
												</div>
											</div>
										</div>
									</div>
									<!--Personal details End -->
											<div class="row acc-row-55">
									<!-- Contact detail start -->
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55">
										<div class=" bg-white clearfix">
											<div class="clearfix bg-light-blue">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
														<h2>3.Contact details</h2>
												</div>
											</div>
											<div class="clearfix">
												<div class="pad-spacing form-wrap acc-col-55">
													<div class="form-group clearfix">
													<formElement:formInputBox idKey="customer.enroll.contact.Number" labelKey="customer.edit.cc.contact.Number" path="contactNumber" inputCSS="text" mandatory="true"/>
													</div>
													<div class="form-group clearfix">
														<formElement:formInputBox idKey="customer.enroll.alternate.Number" labelKey="customer.enroll.alternate.Number" path="alternateNumber" inputCSS="text"/>
													</div>	
												</div>
											</div>
										</div>
									</div>
									<!-- Contact detail End -->
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55">
										<div class="bg-white clearfix">
											<div class="clearfix bg-light-blue">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
														<h2>4.Address</h2>
												</div>
											</div>
											<div class="clearfix bg-white">
												<div class="pad-spacing form-wrap acc-col-55" style="height: 600px !important;">
														<div class="form-group clearfix">
															<formElement:formSelectBox idKey="customer.enroll.Country" labelKey="customer.enroll.Country" path="country" mandatory="false" items="${country}" />
														</div>
														<div class="form-group clearfix">
															<formElement:formInputBox idKey="customer.enroll.AddressLine1" labelKey="customer.enroll.AddressLine1" path="addressLine1" inputCSS="text mandatory" mandatory="true"/>
														</div>
														<div class="form-group clearfix">
															<formElement:formInputBox idKey="customer.enroll.AddressLine2" labelKey="customer.enroll.AddressLine2" path="addressLine2" inputCSS="text mandatory" />
														</div>

														<div class="form-group clearfix">
															<formElement:formInputBox idKey="customer.enroll.Suburb" labelKey="customer.enroll.Suburb" path="suburb" inputCSS="text mandatory" mandatory="true"/>
														</div>

														<div class="form-group clearfix">
															<formElement:formInputBox idKey="customer.enroll.City" labelKey="customer.edit.cc.City" path="city" inputCSS="text"/>
														</div>

														<div class="form-group clearfix">
															<formElement:formSelectBox idKey="customer.enroll.Province" labelKey="customer.edit.cc.province" path="province" mandatory="false" items="${province}" />
														</div>
														<div class="form-group clearfix">
															<formElement:formInputBox idKey="customer.enroll.PostalCode" labelKey="customer.enroll.PostalCode" path="postalCode" inputCSS="text mandatory" mandatory="true"/>
														</div>
												</div>
											</div>
										</div>
									</div>	
								</div>
									<div class="row acc-row-55">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 alpha55 edit-form-wrap">
										<div class="bg-white">
											<div class="clearfix bg-light-blue">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
													<h2>5. Communication from Clicks  <span>- optional</span> </h2>
												</div>
											</div>
											<div class="clearfix acc-col-55">
											<div class="pad-spacing form-wrap ">
												 		<label><spring:theme code="customer.enroll.ACCINFO"/> </label>
											
											<div class="form-group clearfix">
												<formElement:formCheckbox idKey="customer.enroll.marketing.Email1" labelKey="customer.enroll.marketing.Email" path="accinfoEmail" inputCSS="text" mandatory="true"/>
											</div>

											<div class="form-group clearfix">
												<formElement:formCheckbox idKey="customer.enroll.marketing.SMS1" labelKey="customer.enroll.marketing.SMS" path="accinfoSMS" inputCSS="text" mandatory="true"/>
											</div>
											<br/>
											 <div class="form-group clearfix">
											<label><spring:theme code="customer.enroll.MARKETING"/> </label>
												<formElement:formCheckbox idKey="customer.enroll.marketing.Email" labelKey="customer.enroll.marketing.Email" path="marketingEmail" inputCSS="text check-hide" mandatory="true"/>
													<formElement:formCheckbox idKey="customer.enroll.marketing.SMS" labelKey="customer.enroll.marketing.SMS" path="marketingSMS" inputCSS="text check-hide" mandatory="true"/>
											</div>
													<div class="form-actions">
														<ycommerce:testId code="linkCC_SaveUpdatesButton">
														<button class="btn btn-save-change positive" type="submit"><spring:theme code="text.account.link.cc"/></button>
														</ycommerce:testId>
													</div>
													</div>
												</div>	
										</div>
									</div>
									</div>
								</div>
					</form:form>
<!-- End Right side Conent -->
