<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:page pageTitle="${pageTitle}">
<div id="globalMessages">
	<common:globalMessages/>
</div>

<div class="leaderboardWrapper clearfix">
		<div class="container">
			<c:if test="${not empty article.bottomContent1}">
			<c:set var="name" value="${article.bottomContent1}"/>
			<c:set var="seperSectionIcon" value=""/>
				<c:choose>
					<c:when test="${fn:containsIgnoreCase(name, 'heart')}">
						<c:set var="seperSectionIcon" value="heart"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'flu')}">
						<c:set var="seperSectionIcon" value="flu"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'cancer')}">
						<c:set var="seperSectionIcon" value="cancer"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'hiv')}">
						<c:set var="seperSectionIcon" value="hiv"/>
					</c:when>
					<c:when test="${fn:containsIgnoreCase(name, 'diabetes')}">
						<c:set var="seperSectionIcon" value="diabetes"/>
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				<div class="articlePage"> <a class="${seperSectionIcon} article-view-top arrow-right" href="${article.bottomContent1Url}">${article.bottomContent1}</a> </div>
			</c:if>
			<%-- <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section1head" var="component">
							<cms:component component="${component}" element="div" class="image-center"/>
					</cms:pageSlot>
				</div>
			</div> --%>
		</div>
	</div>

	<div class="articleTop conditionTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<h1 class="main-title">${article.titleName}</h1>
						</div>
						<c:set var ="articleName" value="${fn:replace(article.titleName, '\"', '&#34;')}" />
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="moreLinks pdp">
							<a class="share" href="javascript:void(0)">Share</a>
							<a class="email" href="javascript:void(0)" onclick="javascript:window.location='mailto:?subject=${articleName}&body=' + window.location;">Email</a>
							<div class="shareProduct a2a_kit social-share">
							<span class="popup-left-arrow"></span>
							<ul>
								<!-- <li><a title="Facebook" target="_blank" class="facebook_icon" href="https://www.facebook.com/"><img /></a></li> -->
								<li><a title="facebook" target="_blank"
									class="a2a_button_facebook icon-sprite icon-video-fb-share facebook_icon"
									href="https://www.facebook.com/"><img />Facebook</a></li>
								<!-- <li><a title="Twitter" target="_blank" class="twitter_icon" href="https://twitter.com/"><img /></a></li> -->
								<li><a title="twitter" target="_blank"
									class="a2a_button_twitter icon-sprite icon-video-tweet-share twitter_icon"
									href="https://twitter.com/"><img />Twitter</a></li>
								<li><a title="Google +" target="_blank"
									class="a2a_button_google_plus icon-sprite icon-video-gp-share google_icon"
									href="https://plus.google.com/"><img />Google +</a></li>
								<!-- <li><a title="Google +" target="_blank" class="google_icon" href="https://plus.google.com/"><img /></a></li> -->
								<!-- 	<li><a title="Email" class="email_icon"></a></li> -->
							</ul>
						</div>
						</div>	
							</div>
						</div>						
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<cms:pageSlot position="Section1head" var="component">
							<cms:component component="${component}" element="div"/>
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix articlePage individualCondition">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<div class="bg-white contentContainer clearfix">					
							${article.mainContent}									
								<div class="authorWrapper">
									<div class="authorPic">
										<img src="${article.titleIcon.url}" alt="${article.titleIcon.altText}">
									</div>																			
									<div class="authorInfoWrapper">										
											${article.contentBy}										
									</div>
								</div>
							
						</div>
						<c:if test="${not empty article.bottomContent1}">
							<a class="${seperSectionIcon} article-view-top arrow-right" href="${article.bottomContent1Url}">${article.bottomContent1}</a>
						</c:if>
						<c:if test="${not empty article.bottomContent2}">
							<div class="bg-white clearfix disclaimerWrapper">
								<p>${article.bottomContent2}</p>
							</div>
						</c:if>
					</div>	
	
				<div class="col-md-4 col-sm-12 col-xs-12">		
					<aside>
					<c:if test="${not empty article.condition}">
						<c:if test="${not empty article.condition.relatedGlossary}">
							<div class="vitSupBlock bg-white clearfix glossaryWrapper">
								<h3>Glossary of related terms</h3>
								<ul>
								 <c:forEach var="relatedGlossary" items="${article.condition.relatedGlossary}">
									<li><strong>${relatedGlossary.glossaryName}</strong> - ${relatedGlossary.glossaryDescripiton}</li>					
								</c:forEach>	 
								</ul>
							</div>
						</c:if>
						
<%-- 						<c:if test="${not empty article.condition.relatedConditions}">
							<div class="vitSupBlock bg-white clearfix">
								<h3>Related conditions</h3>
								<ul>
								 <c:forEach var="relatedCondition" items="${article.condition.relatedConditions}">
									<li><a href="${relatedCondition.article.articleTitle}">${relatedCondition.conditionName}</a></li>				
								</c:forEach>
								</ul>
							</div>
						</c:if>
						
						<c:if test="${not empty article.condition.relatedMedicinesSupplements}">
							<div class="vitSupBlock bg-white clearfix">
								<h3>Related medicines &amp; supplements</h3>
								<ul>
								 <c:forEach var="relatedMedSuplement" items="${article.condition.relatedMedicinesSupplements}">
									<li><a href="${relatedMedSuplement.article.articleTitle}">${relatedMedSuplement.name}</a></li>				
								</c:forEach>
								</ul>
							</div>
						</c:if> --%>
	
					</c:if>	
						<c:if test="${not empty relatedConditions}">
							<h2>Related conditions</h2>
							 <div class="bg-white">
									<div class="articleSidebar clearfix">
										<div class="articleSideWrapper clearfix">
							 	<c:forEach var="relatedCondition" items="${relatedConditions}">
											<h3><a href="${contextPath}/health/conditions/article-view/${relatedCondition.articleTitle}">${relatedCondition.titleName}</a></h3>
								</c:forEach>
										</div>
									</div>
							 </div>
						</c:if>
						
						<c:if test="${not empty relatedMedicinesSupmnt}">
							<h2>Related medicines and supplements</h2>
							 <div class="bg-white">
									<div class="articleSidebar clearfix">
										<div class="articleSideWrapper clearfix">
							 	<c:forEach var="relatedMedicineSup" items="${relatedMedicinesSupmnt}">
										<c:if test="${relatedMedicineSup.articleType=='MEDICINES'}">
											<h3><a href="${contextPath}/health/medicines/article-view/${relatedMedicineSup.articleTitle}">${relatedMedicineSup.titleName}</a></h3>
										</c:if>
										<c:if test="${relatedMedicineSup.articleType=='VITAMINS'}">
											<h3><a href="${contextPath}/health/supplements/article-view/${relatedMedicineSup.articleTitle}">${relatedMedicineSup.titleName}</a></h3>
										</c:if>
								</c:forEach>
										</div>
									</div>
							 </div>
						</c:if> 			
					<c:if test="${not empty article.sideHeading}">
					 	<div class="symptomsChecker">
							<a class="stethoscope arrow-right" href="${article.sideHeadingUrl}">${article.sideHeading}</a>
					 	</div>
					 </c:if>

					 <!-- DFP Section starts -->
						 
						<cms:pageSlot position="GoogleAd" var="component">
							<cms:component component="${component}"/>
						</cms:pageSlot>
						
					<!-- DFP Section ends -->
						
						<%-- <c:if test="${not empty article.relatedArticles}">
							<h2>${article.relArticleHeading}</h2>
							 <div class="bg-white">
							 <c:forEach var="relatedArticle" items="${article.relatedArticles}">
								<div class="articleSidebar clearfix">
									<div class="articleSideWrapper clearfix">
										<div class="image-center">
											<img src="${relatedArticle.sideImage1.url}" alt="${article.sideImage1.altText}">
										</div>
										<h3><a href="${relatedArticle.articleTitle}">${relatedArticle.titleName}</a></h3>
										<p>${relatedArticle.titleContent}</p>
									</div>
								</div>
								</c:forEach>
							 </div>
						</c:if>  --%>
						<c:if test="${not empty relatedHealthArticles}">
							<h2 class="bg-blue">${article.relArticleHeading}</h2>
							 <div class="bg-white">						 
								 <c:forEach var="relatedArticle" items="${relatedHealthArticles}">						 
								 	<div class="articleSidebar clearfix">
										<div class="articleSideWrapper clearfix">
											<div class="image-center">
												<img src="${relatedArticle.sideImage1.url}" alt="${article.sideImage1.altText}"/>
											</div>
											<c:if test="${relatedArticle.articleType=='HEALTH'}">
											<h3><a href="${contextPath}/health/article-view/${relatedArticle.articleTitle}">${relatedArticle.titleName}</a></h3>
											</c:if>
											<p>${relatedArticle.titleContent}</p>
										</div>
									</div>
									<div class="separator"></div>								
								</c:forEach>						
							</div>
						</c:if>
<%-- 						<c:if test="${not empty relatedHealthArticles}">
							<h2>Related articles</h2>
							 <div class="bg-white">
							 	<c:forEach var="relatedHealthArticle" items="${relatedHealthArticles}">
									<div class="articleSidebar clearfix">
										<div class="articleSideWrapper clearfix">
											<h3><a href="${contextPath}/health/article-view/${relatedHealthArticle.articleTitle}">${relatedHealthArticle.titleName}</a></h3>
										</div>
									</div>
								</c:forEach>
							 </div>
						</c:if>  --%>
						<%-- <cms:pageSlot position="Section1" var="component">
								<cms:component component="${component}" element="div" class="image-center"/>
						</cms:pageSlot> --%>
					</aside>
				</div>
			</div>
		</div>
	</section>

</template:page>
