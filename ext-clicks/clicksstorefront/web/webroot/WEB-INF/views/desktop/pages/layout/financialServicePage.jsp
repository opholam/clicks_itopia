<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="financial" tagdir="/WEB-INF/tags/desktop/financial" %>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<div class="financial_top">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<cms:pageSlot position="Section1A" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="image-center">
					<cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
						<!-- <img src="../image/article/article-top.jpg" alt="" /> -->
					</div>
				</div>
			</div>
		</div>
	</div>

<section class="main-container bg-gray clearfix articlePage">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<financial:service/>

<!-- 					First Block
					<div class="policy_outer">
						<div class="services_wrap clearfix">
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
								<div class="policy_offer">
									<div class="offer_details">
										Up to R7,500
										<span> cover at no cost to you </span>
									</div>
									<a href="">Activate now</a>
								</div>
							</div>	
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<h4>Funeral cover <span>at no cost to you</span></h4>
								<a class="btn-small">Exclusive ClubCard benefit</a>
								<p>
									If you are a ClubCard member aged 18-65, you are eligible for funeral cover at no cost to you. 
									In the event of your death, your family will have cash towards your funeral. Your cover is 10 times the value of 
									your average monthly purchases at Clicks - up to a maximum of R7500.
								</p>
								<a href="">Hide details</a>
							</div>
						</div>	
					</div>
					End First Block -->
					
					
					<!-- Regent Block -->
					<div class="regent_block">
						<div class="services_wrap clearfix">
							
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 tab-content">
							<!-- 	<img src="../image/content_pages/regent.jpg" alt="regent logo"> -->
							
								<cms:pageSlot position="Section3A" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>

							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<!-- <p>
									Funeral, Family Funeral, Life, Hospital Event Cover, Women Only Cancer and Accident Insurance Cover brought 
									to you and underwritten by Regent Life Assurance Company Ltd, a licensed long-term insurer, company reg. 
									no.1994/001332/06, and an authorised Financial Services Provider, FSP licence 18146.</p>
								<p>
									Car and home insurance cover brought to you and underwritten by Regent Insurance Company Ltd, a licensed short-term insurer, 
									company reg. no. 1966/007612/06, and an authorised Financial Services Provider, FSP licence 25511
								</p> -->
								<cms:pageSlot position="Section3B" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
								
							</div>
						</div>
					</div>
					<!-- Regent Block -->
					
				</div>
			</div>
		</div>
	</section>

</template:page>