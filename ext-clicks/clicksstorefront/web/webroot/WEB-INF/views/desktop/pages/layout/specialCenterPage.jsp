<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages />
	</div>
	<div class="healthTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
						<cms:pageSlot position="Section1-head" var="feature">
							<cms:component component="${feature}" element="div" class="Section1-head" />
						</cms:pageSlot>
					
					<cms:pageSlot position="Section1" var="feature">
						<cms:component component="${feature}" element="div"
							class="section1" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix v-n-s-Page health-depth-Outer">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="promo-wrap clearfix health-slider-outer sliderNew">
						<cms:pageSlot position="Section2A" var="feature" element="div" class="health-slider special-center">
							<cms:component component="${feature}" />
						</cms:pageSlot>				
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="vitSupBlock clearfix right_links">						
						<ul>
							<cms:pageSlot position="Section2B" var="feature">
								<li><cms:component component="${feature}" /></li>
							</cms:pageSlot>							
						</ul>
					</div>
				</div>
			</div>
			
			<div class="row">				
				<cms:pageSlot position="Section3" var="feature">
					<cms:component component="${feature}"  element="div"  class="col-lg-4 col-md-4 col-sm-12 col-xs-12"/>
				</cms:pageSlot>				
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-8 col-sm-12 col-xs-12">
					<cms:pageSlot position="RichMedia" var="feature">
						<cms:component component="${feature}" element="div"
							class="bg-white bxslider-ss-outer flu_video clearfix" />
					</cms:pageSlot>					
				</div>
				<%-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="vns-ad-block">
						<div class="image-center">
							<cms:pageSlot position="Section4B" var="feature">
								<cms:component component="${feature}" element="h3"	class="text-center" />
							</cms:pageSlot>
						</div>
					</div>
				
					<div class="promotion-slider bg-white clearfix">
						<cms:pageSlot position="Section4CLink" var="feature">
							<cms:component component="${feature}" element="h3"	class="text-center" />
						</cms:pageSlot>
						<cms:pageSlot position="Section4C" var="feature" >
							 <cms:component component="${feature}"  element="div" class="promotionOuter" />
						</cms:pageSlot>
					</div>
					
					<div class="vns-ad-block">
						<div class="image-center">
							<cms:pageSlot position="Section4D" var="feature">
								<cms:component component="${feature}" element="h3"	class="text-center" />
							</cms:pageSlot>
						</div>
					</div>
										
				</div>	 --%>			
			</div>
			
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<cms:pageSlot position="Section4ASlot-head" var="feature"
						element="div" class="span-6 zoneA">
						<cms:component component="${feature}" />
					</cms:pageSlot>					
					<div class="articleBlock bg-white clearfix tb-res">					
						<cms:pageSlot position="Section4A" var="feature" >
						 	<cms:component component="${feature}" element="div" class="col-lg-6 col-md-6 col-sm-6 col-xs-12"  />
						</cms:pageSlot>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="vns-ad-block">
						<div class="image-center">
							<cms:pageSlot position="Section4B" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
				
					<div class="promotion-slider bg-white clearfix">
						<%-- <cms:pageSlot position="Section4CLink" var="feature">
							<cms:component component="${feature}" element="h3"	class="text-center" />
						</cms:pageSlot>
						<cms:pageSlot position="Section4C" var="feature" >
							 <cms:component component="${feature}"  element="div" class="promotionOuter" />
						</cms:pageSlot> --%>
					</div>
					
					<%-- <div class="vns-ad-block">
						<div class="image-center">
							<cms:pageSlot position="Section4D" var="feature">
								<cms:component component="${feature}" element="h3"	class="text-center" />
							</cms:pageSlot>
						</div>
					</div> --%>
										
				</div>
			</div>
		</div>
	</section>
	


<!-- Modal -->
<%-- <div id="flumyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">YouTube Video</h4>
      </div>
      <div class="modal-body">
        <iframe id="fluVideo" width="560" height="315" src="${videoUrl}" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	 --%>
</template:page>