<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>

<div class="acc-table-wrap">
	<!-- Points activity heading  -->
	<div class="clearfix bg-light-blue">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
			<div class="pull-left">
				<h2>Points activity</h2>
			</div>
			<div class="pull-right text-right">
				<c:set var="startDate">
					<dateFormat:dateFormat conversionType="date"
						input="${user.pointStatement.startDate}" format="dd MMMM yyyy" />
				</c:set>
				<span>From ${startDate}</span>
			</div>
		</div>
	</div>
	<!-- End Points activity heading  -->
	<!-- Amount spent start -->
	<div class="clearfix bg-white amountWrapper">
		<div class="recent-head">
			<div class="col-md-10 col-sm-10 col-xs-9">
				<div class="accHeading">
					<h5>Amount spent</h5>
					<p>The amount you've spent shopping at Clicks</p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3">
				<div class="price blue">
					<c:choose>
						<c:when test="${not empty selectedaccount and selectedaccount.accountID eq 5000002}">
							<c:choose>
								<c:when test="${not empty selectedaccount and not empty selectedaccount.totalSpent}">
										R<dateFormat:dateFormat conversionType="rand" input="${selectedaccount.totalSpent}" />
								</c:when>
							<c:otherwise>
								R<dateFormat:dateFormat conversionType="rand" input="0.00" />
							</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${not empty selectedaccount and selectedaccount.accountID eq 5000003}">
										P<dateFormat:dateFormat conversionType="rand" input="${selectedaccount.totalSpent}" />
								</c:when>
								<c:otherwise>
										P<dateFormat:dateFormat conversionType="rand" input="0.00" />
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>

					<!-- R512<sup>30</sup> -->
				</div>
			</div>
		</div>
		<!-- Amount spent end -->
	</div>
	<div class="clearfix bg-white">
		<!-- Clicks points  -->
		<div class="repeat-table-head rewards-custom acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha-3">
				<div class="accHeading">
					<h5>Clicks points</h5>
					<p>
						ClubCard Points earned when shopping at Clicks - <a
							href="javascript:void(0)" class="transcTab">see transaction
							history</a>
					</p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha-3">
				<div class="price blue acc-row-3">
					<c:choose>
						<c:when test="${not empty selectedaccount}">
							${selectedaccount.transactions.pointsBalance}
						</c:when>
						<c:otherwise>
							0
						</c:otherwise>
					</c:choose>

				</div>
			</div>

			<div class="col-md-9 col-sm-12 col-xs-12 trascTableWrap">
				<!-- transaction table starts  -->
				<div class="acc-body clearfix bg-white trascTable">
					<div class="recent-head">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 trascRow">Date</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 trascRow">Location</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 trascRow">Spent</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 trascRow">Points
							earned</div>
					</div>



					<div class="repeat-table-wrap">
						<c:if test="${not empty selectedaccount}">
							<c:forEach var="transaction"
								items="${selectedaccount.transactions.transactions}">
								<div class="recent-head recent-custom">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 trascRow">
										<dateFormat:dateFormat conversionType="date"
											input="${transaction.date}" format="dd MMM yyyy" />
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  trascRow">${transaction.location}</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 trascRow">${transaction.spent}</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 trascRow">
										<div class="text-center">${transaction.earned}</div>
									</div>
								</div>
							</c:forEach>
						</c:if>
					</div>
				</div>
				<!-- transaction table Ends -->
			</div>
		</div>
		<!-- Clicks points Ends -->
		<!-- Partner points  start-->
		<div class="recent-head acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
				<div class="accHeading">
					<h5>Partner points</h5>
					<p>ClubCard Points earned when you purchase goods or service
						from our partners</p>
				</div>

				<div class="row acc-row">
					<c:if test="${not empty selectedaccount}">
						<c:forEach var="partnersPoints"
							items="${selectedaccount.partnerPoints.partners}">
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
								<div class="gridBox gridGrey acc-col">
									<span class="pointsConent"> ${partnersPoints.name}</span> <span
										class="points"> ${partnersPoints.value}</span>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
				<div class="accHeading">
					<br></br>
					<div style="font-size: 8px;">
						<p>* Partner points are dependent on a 3rd party to supply us
							with the information and may take longer to reflect.</p>
					</div>
				</div>
				<div></div>
			</div>

			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
				<c:choose>
					<c:when test="${not empty selectedaccount}">
						<div class="price blue acc-col-3">${selectedaccount.partnerPoints.balance}</div>
					</c:when>
					<c:otherwise>
						<div class="price blue acc-col-3">0</div>
					</c:otherwise>
				</c:choose>

			</div>
		</div>
		<!-- Partner points Ends -->
		<!-- Bonus points -->

		<c:set var="bonus" value="0"></c:set>
		<c:set var="pharmacy" value="0"></c:set>
		<c:set var="babyClub" value="0"></c:set>
		<c:set var="senior" value="0"></c:set>

		<c:if test="${not empty selectedaccount}">
			<c:forEach var="pointsBucket"
				items="${selectedaccount.clicksPoints.buckets}" varStatus="loop">
				<c:choose>
					<c:when test="${!((loop.count)%2 == 0)}">
						<div class="repeat-table-head rewards-custom acc-row-3">
							<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
								<div class="accHeading">
									<h5>${pointsBucket.name}</h5>
									<p>
										<spring:theme code="${fn:replace(pointsBucket.name,' ', '.')}"
											text="${pointsBucket.name}" />
									</p>
								</div>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
								<div class="price blue acc-col-3">${pointsBucket.value}</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="recent-head acc-row-3">
							<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
								<div class="accHeading">
									<h5>${pointsBucket.name}</h5>
									<p>
										<spring:theme code="${fn:replace(pointsBucket.name,' ', '.')}"
											text="${pointsBucket.name}" />
									</p>
								</div>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
								<div class="price blue acc-col-3">${pointsBucket.value}</div>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<div class="recent-head acc-row-3">
				<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
					<div class="accHeading">
						<!-- <h5>Senior Points</h5> -->
						<div style="font-size: 8px;">
							<p>** Our points are updated every 24 hours and might not
								reflect immediately after you have shopped at Clicks.</p>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
					<div class="price blue  acc-col-3"></div>
				</div>
			</div>

		</c:if>
	</div>
</div>


<%-- <c:if test="${not empty selectedaccount}">
			<c:forEach var="pointsBucket" items="${selectedaccount.clicksPoints.buckets}" varStatus="loop">				
				
				<c:if test="${pointsBucket.name eq 'Bonus'}">
	         		<c:set var="bonus" value="${pointsBucket.value}"></c:set>
	        	</c:if>
	        	
	        	<c:if test="${pointsBucket.name eq 'Pharmacy'}">
	         		<c:set var="pharmacy" value="${pointsBucket.value}"></c:set>
	        	</c:if>
	        	
	        	<c:if test="${pointsBucket.name eq 'BabyClub'}">
	         		<c:set var="babyClub" value="${pointsBucket.value}"></c:set>
	        	</c:if>
	        	
	        	<c:if test="${pointsBucket.name eq 'Senior'}">
	         		<c:set var="senior" value="${pointsBucket.value}"></c:set>
	        	</c:if>
			</c:forEach>
		</c:if>
		
		<div class="repeat-table-head rewards-custom acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
				<div class="accHeading">
					<h5>Bonus points</h5>
					<p>Bonus Points are extra Points earned, like Double Bonus
						Points.</p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
				<div class="price blue acc-col-3">${bonus}</div>
			</div>
		</div>
		<!-- Bonus points Ends-->
		<!-- Pharmacy points -->
		<div class="recent-head acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
				<div class="accHeading">
					<h5>Pharmacy Points</h5>
					<p>Pharmacy Points are earned on dispensing fees or clinic
						services</p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
				<div class="price blue acc-col-3">${pharmacy}</div>
			</div>
		</div>
		<!-- Pharmacy points Ends-->
		<!-- BabyClub points -->
		<div class="repeat-table-head rewards-custom acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
				<div class="accHeading">
					<h5>BabyClub points</h5>
					<p>If you are a BabyClub member, you earn points on selected
						goods and services</p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
				<div class="price blue  acc-col-3">${babyClub}</div>
			</div>
		</div>
		<!-- BabyClub points Ends-->
		<!-- Senior points -->
		<div class="recent-head acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
				<div class="accHeading">
					<h5>Senior Points</h5>
					<p>Earn additional points on ClubCard Seniors Double Points
						Days
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
				<div class="price blue  acc-col-3">${senior}</div>
			</div>
		</div>
		<!-- Senior points Ends-->
		<!-- Total points -->
		<div class="repeat-table-head rewards-custom acc-row-3">
			<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
				<div class="totalText">Total</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
				<c:choose>
					<c:when test="${not empty selectedaccount}">
						<div class="totalValue acc-col-3">${selectedaccount.pointsBalance}</div>
					</c:when>
					<c:otherwise>
						<div class="totalValue acc-col-3">0</div>
					</c:otherwise>
				</c:choose>
				
			</div>
		</div> --%>



<!-- Total points Ends-->
