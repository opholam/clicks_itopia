<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sitemap" tagdir="/WEB-INF/tags/desktop/sitemap"%>

<template:page pageTitle="${pageTitle}">
<div class="contentWrapper bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="bg-white pagedata_wrapper siteMapcontainer">
		    <div class="row">
		      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		          <h2>Categories</h2>
		          <ul class="siteMap">
		            <c:forEach items="${subcategories}" var="subcategory">
		              <li><span class="collapseMap"></span><a href="./c/${subcategory.code}">${subcategory.name}</a>
		                <c:if test="${not empty subcategory.categories && subcategory.categories.size() > 0}">
		                  <sitemap:siteMap category="${subcategory}" />
		                </c:if>
		              </li>
		            </c:forEach>
		          </ul>
		          <h2>Featured Brands</h2>
		          <ul class="siteMap">
		            <cms:pageSlot position="BrandLandingPosition" var="feature">
		              <li><span class="collapseMap"></span>
		                <cms:component component="${feature}" />
		              </li>
		            </cms:pageSlot>
		          </ul>
		          <h2>Promotions</h2>
		          <ul class="siteMap">
		            <li><span class="collapseMap"></span><a href="./specials">Promotion</a></li>
		          </ul>
		          <h2>ClubCard</h2>
		          <ul class="siteMap">
		            <cms:pageSlot position="ClubcardPosition" var="feature">
		              <li><span class="collapseMap"></span>
		                <cms:component component="${feature}" />
		              </li>
		            </cms:pageSlot>
		            <li><span class="collapseMap"></span> <a href="./clubcard-magazine?id=all&pageType=magazineLandingPage">ClubCard Magazine</a> </li>
		          </ul>
	          </div>
	          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		          <h2>Pharmacy Services</h2>
		          <ul class="siteMap">
		            <cms:pageSlot position="PharmacyServicesPosition" var="feature">
		            <c:choose>
		            <c:when test="${feature.linkName eq 'Repeat prescriptions service'}">
		            <li><span class="collapseMap"></span>
		            <a href=".${feature.url}">${feature.linkName}</a>
		            </li>
		            </c:when>
		            <c:otherwise>
		            	<li><span class="collapseMap"></span>
		                <cms:component component="${feature}" />
		              </li>
		            </c:otherwise>
		            </c:choose>
		            </cms:pageSlot>
		          </ul>
		          <h2>My Account</h2>
		          <ul class="siteMap">
		            <li><span class="collapseMap"></span> <a href="${request.contextPath}/my-account">
		              <spring:theme code="myaccount.summary.title" />
		              </a> </li>
		            <cms:pageSlot position="MyAccountPosition" var="feature">
		              <li><span class="collapseMap"></span>
		                <cms:component component="${feature}" />
		              </li>
		            </cms:pageSlot>
		          </ul>
	          </div>
	          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		          <cms:pageSlot position="NavigationLinksPosition" var="feature">
		            <c:if test="${feature.visible}">
		              <h2>Health Hub</h2>
		              <ul class="siteMap">
		                <li><span class="collapseMap"></span> <a href="./health">Health Hub Home</a>
		                <ul class="innerSiteMap">
		                  <c:forEach items="${feature.components}" var="component" varStatus="i">
		                    <c:if test="${i.count<9}">
		                      <c:if test="${component.navigationNode.visible}">
		                        <%-- <cms:component component="${component}" evaluateRestriction="true" />  --%>		                        
		                          <li><span class="collapseMap"></span>
		                            <c:if test="${not empty component.navigationNode.links && component.navigationNode.links.size() > 0}"> 
		                            <a href=".${component.navigationNode.links[0].url}">${component.navigationNode.links[0].linkName}</a> </c:if>
		                            <ul class="innerSiteMap">
		                              <c:forEach items="${component.navigationNode.children}" var="children">
		                                <c:forEach items="${children.links}" var="link">
		                                  <li><span class="collapseMap"></span><a href="${link.url}">${link.linkName}</a></li>
		                                </c:forEach>
		                              </c:forEach>
		                            </ul>
		                          </li>		                        
		                      </c:if>
		                    </c:if>
		                  </c:forEach>
		                  </ul>
		                </li>
		              </ul>
		            </c:if>
		          </cms:pageSlot>
		          <h2>Store Locator</h2>
		          <ul class="siteMap">
		            <li><span class="collapseMap"></span> <a href="${contextPath}/store-finder/position">Store Locator</a> </li>
		            <li><span class="collapseMap"></span> <a href="${contextPath}/sitemap/stores">Stores Page</a> </li>
		          </ul>
		          <h2>Help & Information</h2>
		          <ul class="siteMap">
		            <cms:pageSlot position="HelpInformationPosition" var="feature">
		              <li><span class="collapseMap"></span>
		                <cms:component component="${feature}" />
		              </li>
		            </cms:pageSlot>
		          </ul>
		          <h2>About us</h2>
		          <ul class="siteMap">
		            <cms:pageSlot position="AboutUsPosition" var="feature">
		              <li><span class="collapseMap"></span>
		                <cms:component component="${feature}" />
		              </li>
		            </cms:pageSlot>
		          </ul>
	          </div>
      		</div>		          
        </div>
      </div>
    </div>
  </div>
</div>
</template:page>


<%-- <c:forEach items="${subcategories}" var="subcategory">
<div>
<SPAN class="plusSite">${subcategory.code}</SPAN>&nbsp;<SPAN><a href="${contextPath}/c/${subcategory.code}">${subcategory.name}</a></SPAN>
</div>
</c:forEach> --%>




