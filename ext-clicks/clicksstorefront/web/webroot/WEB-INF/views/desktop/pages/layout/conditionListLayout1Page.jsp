<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">
<cms:pageSlot position="SectionConditionList5" var="feature" element="div">
						<cms:component component="${feature}" />
					</cms:pageSlot>
	<section class="main-container bg-gray clearfix  conditionList">
		<div class="container">
			<div class="row">

				<aside>
					<cms:pageSlot position="SectionConditionList1" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</aside>

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

					<cms:pageSlot position="SectionConditionList2" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>

					<cms:pageSlot position="SectionConditionList3" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<div class="home-promotions clearfix">
						<div class="promo-wrap clearfix">
							<ul class="home-promo-slider product-slider clearfix latestpromo"
								data-classname=" " data-minslide="1">
								<cms:pageSlot position="SectionConditionList4" var="feature">
									<li> <cms:component component="${feature}"/></li>
								</cms:pageSlot>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</template:page>