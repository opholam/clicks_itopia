 <%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

 <c:choose>
 <c:when test="${not empty orderData}">
 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="bg-white clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearBoth">
  <h4>Status: ${orderData.statusDisplay}</h4>
  </div>
							<div class="clearfix bg-light-blue clearBoth">
								<div
									class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
									<div class="pull-left">
										<h2>Order Summary</h2>
									</div>
									<div class="pull-right text-right">
										<a href="javascript:void(0)">
										<c:choose>
											<c:when test="${not empty orderData.clicksOrderCode}">
											<c:set var="orderCode" scope="page" value="${orderData.clicksOrderCode}"/>
											</c:when>
											<c:otherwise>
											<c:set var="orderCode" scope="page" value="${orderData.code}"/>
											</c:otherwise>
										</c:choose>
										#${orderCode}
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 paddingNone borRight">
							<div class=" checkout-confirm-left">
								<div class="pad-spacing clearfix">
									<div class="del-method">
										<h2>Delivery Method</h2>
										<div class="confirm-content">${orderData.deliveryMode.name}</div>
									</div>
										<order:deliveryAddressItem order="${orderData}"/>

									<div class="del-method">
										<h2>Payment options</h2>
										<div class="confirm-content">Paid successfully using PayU</div>
									</div>
										<order:billingAddressItem order="${orderData}"/>
									<div class="del-method">
										<h2>ClubCard points</h2>
										<div class="order-content">
											<c:choose>
											<c:when test="${!orderData.guestCustomer && not empty user.memberID}">
											<c:choose>
											<c:when test="${clubCardPointVal > 0}">
											${clubCardPointVal} points earned
											</c:when>
											<c:otherwise>
											0 points earned
											</c:otherwise>
											</c:choose>
											</c:when>
											<c:otherwise>
											You could have earned ${clubCardPointVal} ClubCard points.
											</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 paddingNone borLeft">
							<c:if test="${not empty orderData.consignments && orderData.consignments.size() > 1}">
								<div class="form-wrap">
									<div class="message">We've split your parcel due to size,
										weight or stock availability. This may impact delivery time. We
										apologise for the inconvenience.</div>
								</div>
							</c:if>
		<c:choose>
		<c:when test="${not empty orderData.consignments && orderData.consignments.size() > 0}">
				<c:forEach items="${orderData.consignments}" var="consignment" varStatus="loop">
				<c:if test="${not empty consignment.status && fn:containsIgnoreCase(consignment.status, 'SHIPPED')}">
				<c:set var="isConsignmentShipped" scope="request" value="true"/>
				</c:if> 
				<order:accountOrderDetailsItem order="${orderData}" consignment="${consignment}" count="${loop.index+1}"/>
			<!-- </div> -->
				</c:forEach>
		</c:when>
		<c:otherwise>
		<div class="inidiviual-order-wrap shopping-list">
										<div class="medicine-coll-head clearfix bg-white">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<h4>
											<span>Parcel 1 - ${orderData.statusDisplay}</span>
										</h4>
										</div></div>
										</div>
		<ul class="orderdetails-process">
		<order:orderDetailsItem order="${orderData}" orderGroup="${orderGroup}" showAddToCart="false"/>
		</ul>
		</c:otherwise>
		</c:choose>					
								<div class="deliverPoints clearfix">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
											<div class="indivigrandTot indiviOrder">
												<div class="subTotalexcluding">
													<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 paddingNone price-right">
														<h4>Subtotal  (excluding delivery)</h4>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 paddingNone price-right">
												<div class="price blue">
												<c:set var="subTotaldec" value="${fn:split(orderData.subTotal.formattedValue, '.')}" />
												<c:set var="subTotaldec1" value="${subTotaldec[0]}" />
												<c:set var="subTotaldec2" value="${subTotaldec[1]}" />
										 			${subTotaldec1}<sup>${subTotaldec2}</sup>
														</div>
													</div>
												</div>
												<c:if test="${orderData.totalDiscounts.value > 0}">
												<c:set var="savingsdec" value="${fn:split(orderData.totalDiscounts.formattedValue, '.')}" />
												<c:set var="savingsdec1" value="${savingsdec[0]}" />
												<c:set var="savingsdec2" value="${savingsdec[1]}" />
												<div class="totalsaving">
													<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 paddingNone price-right"><h4 class="red">(You have saved</h4></div>
													<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 paddingNone price-right">
													<div class="price-wrap">
														<div class="price">
															${savingsdec1}<sup>${savingsdec2}</sup>)
														</div>
														</div>
													</div>
												</div>
												</c:if>
												<div class="deliveryfee">
													<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 paddingNone price-right">
														<h4>Delivery fee</h4>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 paddingNone price-right">
													<div class="price-wrap">
										<c:choose>
									<c:when test="${orderData.deliveryCost.value > 0}">
												<c:set var="deliverydec" value="${fn:split(orderData.deliveryCost.formattedValue, '.')}" />
												<c:set var="deliverydec1" value="${deliverydec[0]}" />
												<c:set var="deliverydec2" value="${deliverydec[1]}" />
										<div class="price blue">${deliverydec1}<sup>${deliverydec2}</sup></div> 
									</c:when>
									<c:otherwise>
									<div class="price">FREE</div>
									</c:otherwise>
									</c:choose>		</div>										
													</div>
												</div>
												<div class="totalcost">
													<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 paddingNone price-right">
														<h4>Total paid</h4>
														<c:set var="totalTaxdec" value="${fn:split(orderData.totalTax.formattedValue, '.')}" />
												<c:set var="totalTaxdec1" value="${totalTaxdec[0]}" />
												<c:set var="totalTaxdec2" value="${totalTaxdec[1]}" />
											
										<span class="specialCheckout">Total includes ${totalTaxdec1}<sup>${totalTaxdec2}</sup> VAT</span>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 paddingNone price-right">
													<div class="price-wrap">
												<div class="price blue">
												<c:set var="totalPricedec" value="${fn:split(orderData.totalPriceWithTax.formattedValue, '.')}" />
												<c:set var="totalPricedec1" value="${totalPricedec[0]}" />
												<c:set var="totalPricedec2" value="${totalPricedec[1]}" />
															${totalPricedec1}<sup>${totalPricedec2}</sup>
												</div>
													</div>
													</div>
												</div>
											</div>
										</div>
								</div>
								<c:if test="${isConsignmentShipped}">
								<div class="re-order-wrap bg-white">
									<div class="text-right">
										<img src="${themeResourcePath}/clicks/image/shopping/deliver34.png"><a href="${contextPath}/requestrefundhelp" class="">Do you need to return an item or request a refund?</a>
									</div>
								</div>
								</c:if>
							</div>
							</div>
							
						</div>
 </c:when>
 <c:otherwise>
			<h4>No Order found. Please try again.</h4>
</c:otherwise>
 </c:choose>