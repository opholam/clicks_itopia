<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


	<div class="search-container">
		<c:forEach items="${healthHubData}" var="articleData" begin="0" end="${articlesSize}">
			<div class="hub-container healtharticlescount">
				<h4>
					<c:if test="${articleData.articleType=='MAGAZINES'}">
						<a href="${contextPath}/clubcard-magazine/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='CONDITIONS'}">
						<a href="${contextPath}/health/conditions/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='MEDICINES'}">
						<a href="${contextPath}/health/medicines/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='MEDICINES'}">
						<a href="${contextPath}/health/medicines/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='HEALTH'}">
						<a href="${contextPath}/health/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='VITAMINS'}">
						<a href="${contextPath}/health/supplements/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
				</h4>
				<div class="articleTagWrapper">
				<c:forEach items="${articleData.articleTags}" var="articleTag">
					<span class="articleTag">${articleTag}</span>
				</c:forEach>
				</div>
				<div class="content-height">
					<p>${articleData.titleContent}</p>
				</div>
			</div>

		</c:forEach>
	</div>
	<c:if test="${totalNoOfHealthArticles > fn:length(healthHubData)}" >
     <div class="text-center searchLoadMoreHealthArticles">
      <a class="btn btn-load-articles">Load more articles</a>
     </div>
 </c:if>
