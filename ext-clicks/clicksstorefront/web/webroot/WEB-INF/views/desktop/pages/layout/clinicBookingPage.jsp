<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	
	<div class="articleTop clinic_top">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
				<cms:pageSlot position="Section1A" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<cms:pageSlot position="Section1B" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
	
	<section class="main-container bg-gray clearfix clinic-wrap">
		<div class="container">
			<div class="row">
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="promo-links clearfix">
					<cms:pageSlot position="Section2" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					</div>
				</div>	
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="clearfix vitality_block">
						<cms:pageSlot position="Section3" var="feature">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
					</div>
					
					<div class="bg-white contentContainer clearfix">
						<cms:pageSlot position="Section4" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="pharma_letclicks">	
					<cms:pageSlot position="SideSection1" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
	
					<cms:pageSlot position="SideSection2" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					
					<div class="letclicks_help bg-white clearfix">
						<ul><cms:pageSlot position="SideSection3" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						</ul>
					</div>
					
					</div>
				</div>
			</div>
	</div>
	</section>

</template:page>