<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
	
<c:set var="profileUrl" value="${contextPath}/my-account/link-email-to-cc" />
<!-- Right Side Content  -->

		<form:form action="${contextPath}/my-account/link-cc" method="post" commandName="registerCustomerForm" id="linkCC">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 createBoxWrapper">
									<div class="bg-white acc-col-4">
									<div class="clearfix bg-light-blue">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<h2>ClubCard Details</h2>
										</div>
									</div>
									<div class="bg-white clearfix">
										<div class="pad-spacing form-wrap">
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="ccNumber" labelKey="customer.cc.link.number" path="clubcardNumber" inputCSS="text"/>
											</div>
											<div class="form-group clearfix">
												<formElement:formInputBox idKey="customer.cc.link.EMailAddress" labelKey="customer.cc.link.EMailAddress" path="eMailAddress" inputCSS="text mandatory" mandatory="true"/>
											</div>
											<!--onclick="window.location='${profileUrl}'"  <a href="" onclick="javascript:retrieveAction();"><input type="button"  class="positive btn btn-save-change" value="Retrieve my details" name="submit"></a>-->
										 
										 <ycommerce:testId code="linkCC_SaveUpdatesButton">
										
										
										<!-- <a href="${profileUrl}" id="registerCustomerFormlin" class="btn primary_btn">Retrieve my details</a>  -->
										
										<button type="submit" class="positive btn btn-save-change" onclick="window.location='${profileUrl}'">Retrieve my details</button> 
										
										</ycommerce:testId>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form:form>
<!-- End Right side Conent -->

 

 