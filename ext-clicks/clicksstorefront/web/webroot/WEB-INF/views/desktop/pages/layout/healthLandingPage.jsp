<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<div class="healthTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<h1 class="main-title">
						<cms:pageSlot position="Section1-head" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</h1>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
			</div>
		</div>
	</div>
	<section class="main-container bg-gray clearfix v-n-s-Page cI">
		<div class="container">
			<div class="row">
			
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<c:if test="${cmsPage.uid ne 'medicinesPage'}">
						<div class="promo-wrap clearfix health-slider-outer sliderNew">
							<cms:pageSlot position="Section1" var="feature" element="div"
								class="health-slider">								
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</c:if>

					<cms:pageSlot position="Section3" var="feature" element="div"
						class="span-24">
						<cms:component component="${feature}" />
					</cms:pageSlot>

					<div class="row">
						<div class="vitSupBlockWrapper clearfix">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="vitSupBlock bg-white clearfix">
									<cms:pageSlot position="Section4A" var="feature" element="div"
										class="span-6 zoneA">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="vitSupBlock bg-white clearfix vitSupBlockTop">
									<cms:pageSlot position="Section4B" var="feature" element="div"
										class="span-6 zoneB ">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</div>
								<c:if test="${cmsPage.uid eq 'conditionsPage'}">
									<div class="vitSupBlock bg-white clearfix">
										<cms:pageSlot position="Section4C" var="feature" element="div"
											class="span-6 zoneB ">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</c:if>
							</div>
						</div>
					</div>

					<c:if test="${cmsPage.uid eq 'vitaminsNSupplement'}">
						<div class="articleBlock bg-white clearfix tb-res">
							<cms:pageSlot position="Section5" var="feature">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>

						</div>
					</c:if>

					<c:if test="${cmsPage.uid eq 'conditionsPage'}">

						<cms:pageSlot position="Section6-head" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>

						<div class="partner-wrap clearfix bg-white hCWrapper">
							<div
								class="health-slider-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<ul class="health-center-slider">
									<cms:pageSlot position="Section6" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</ul>
							</div>
						</div>

					</c:if>

					<c:if test="${cmsPage.uid eq 'vitaminsNSupplement' || cmsPage.uid eq 'medicinesPage'}">

						<div class="home-promotions clearfix">
							<div class="clearfix bgred">
								<div
									class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
									<cms:pageSlot position="Section6-head" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>

								</div>
							</div>

							<div class="promo-wrap clearfix">
								<ul class="home-promo-slider product-slider clearfix"
									data-classname="home-promo-slider" data-minslide="2">
									<cms:pageSlot position="Section6" var="feature">
										<li> <cms:component component="${feature}" /></li>
									</cms:pageSlot>
								</ul>
							</div>
						</div>
					</c:if>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<c:if test="${cmsPage.uid eq 'conditionsPage'}">
						<div class="vns-ad-block">
							<cms:pageSlot position="Section2A" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</c:if>
					<c:if test="${cmsPage.uid eq 'vitaminsNSupplement' || cmsPage.uid eq 'medicinesPage' }">						
						<cms:pageSlot position="Section2A" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>						
					</c:if>
					<c:if test="${cmsPage.uid eq 'vitaminsNSupplement' || cmsPage.uid eq 'medicinesPage'}">
						<div class="vns-ad-block">
							<cms:pageSlot position="Section2B" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</c:if>
					<c:if test="${cmsPage.uid eq 'conditionsPage'}">
						<cms:pageSlot position="Section2B" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</c:if>			
				</div>
			</div>
		</div>
	</section>

</template:page>