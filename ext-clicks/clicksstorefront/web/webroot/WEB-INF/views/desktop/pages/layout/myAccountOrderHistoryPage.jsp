<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="myaccount" tagdir="/WEB-INF/tags/desktop/myaccount"%>

<!-- Right Side Content  -->
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<div class="account-body">
	
		<div class="acc-table-wrap">
			<!-- Order summary table  -->
			<div class="acc-body clearfix order-history">
<%-- 				<div class="recent-head bg-white">
					<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
						<spring:theme code="order.placed.title" />
					</div>
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<spring:theme code="delivery.address.title" />
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<spring:theme code="status.title" />
					</div>
				</div> --%>
				<c:choose>
				<c:when test="${recentOrders.size() > 0}">
				<input type="hidden" id="OrderHisListTotalCount" value="${recentOrders[0].totalOrderCount}"/>
		  <div class="recent-head bg-white">
		  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"><spring:theme code="order.number" /></div>
           <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4"><spring:theme code="order.placed.title" /></div>
           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4"><spring:theme code="delivery.address.title" /></div>
           <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs"><spring:theme code="status.title" /></div>
           <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs"><spring:theme code="order.view.details" /></div>
          </div>
				<div class="repeat-table-wrap bg-white">
					<!-- First row data  -->
					<div id="orderId">
						<c:forEach items="${recentOrders}" var="order" varStatus="status">
							<myaccount:myAccountOrders orderData="${order}" />
							<c:set var="statusIndex" value="${status.index}"></c:set>
						</c:forEach>
					</div>
					<c:if test="${recentOrders[0].totalOrderCount > orderHisCount}">
					<input type="hidden" id="orderHisCount" value="${orderHisCount}"/>
					<input type="hidden" id="contextPath" value="${contextPath}"/>
						<div class="text-center load-order">
							<a class="btn btn-load-articles" href="#"
								onclick="loadOrders()">Load more</a>
						</div>
						<!-- End First row data  -->
					</c:if>
				</div>
				</c:when>
				<c:otherwise>
					<div class="myyacc-noclub">
	<div class="promo-links clearfix">
		
		<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 promo-options">
			<h2> You have no online orders </h2>
		</div>
		
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 promo-options">
		<div class="promolink-wrap">
				 See <a href="${contextPath}/c/OH1?q=%3Aprice-asc%3AnewProduct%3Atrue"> what's new</a> or <a href="${contextPath}/specials"> on promotion</a> 
		</div>
		</div>
		</div>
	</div>	
				</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<!-- End Order summary table  -->
</div>
