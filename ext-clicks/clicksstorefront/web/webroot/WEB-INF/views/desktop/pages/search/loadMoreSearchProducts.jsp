<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="search" tagdir="/WEB-INF/tags/desktop/search" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set value="${fn:length(searchPageData.results)}" var="displayLength"/>
<c:set value="${searchPageData.pagination.totalNumberOfResults}" var="totalResults"/>

<c:set var="rowCount" value="0"/>
<%-- <c:set var="numRows" value="${displayLength/3 }"/> --%>
<c:set var="addNewRow" value="true"/>

<div class="filterContent tb-res">	
	<c:forEach items="${searchPageData.results}" var="product" varStatus="status">
		 <c:if test="${addNewRow == true}"> 
			<div class="productRow clearfix horiz-${rowCount}">
		 </c:if>	
		<product:searchProductListerItem product="${product}"/>
		<c:if test="${status.count ne 0 && status.count % 3 == 0}">            
             <c:set var="rowCount" value="${rowCount + 1}"/>
             <c:set var="addNewRow" value="true"/>
         </c:if>
         <c:if test="${status.count ne 0 && status.count % 3 != 0}">  
           	 <c:set var="addNewRow" value="false"/>
         </c:if>
        <c:if test="${addNewRow == true}"> 
	      </div>
        </c:if>
	</c:forEach>
</div>

<c:if test="${totalResults > displayLength }">
	<button type="submit" class="btn btn-load-more searchProductsLoadMore">Load more products</button>
</c:if>
