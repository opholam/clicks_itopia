<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	<div class=" bg-white  clearfix ">
		<div class="clearfix bg-light-blue">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">			
				 <h2><spring:theme code="forgottenDetails.clubcard"/></h2>
			</div>
		</div>
		<div class="clearfix acc-col-4">
			<div class="pad-spacing form-wrap">			
				<cms:pageSlot position="LeftContentDetailSlot" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>

			<form:form commandName="forgottenDetailsForm">
			
				<div class="form-group clearfix">	
					<formElement:formInputBox idKey="club-num" labelKey="clubcard.number"  inputCSS="form-control" path="clubCardNumber"/>						
				</div>
				<div class="form-group saResident">				
					<label><spring:theme code="resident.confirm"/></label>
					<div class="form-group clearfix">
						<div class="form-group clearfix">
							<div class="clearfix">
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone">								
									<form:radiobutton path="resident" id="saResi1"  cssClass="radio-box radio-hide" value="true" />																
									<form:label path="resident" cssClass="radio-box clubcardhide" for="saResi1" data-toshow="ClubcardValue1" data-tohide="dobSaOuter" ><span>Yes</span></form:label>
								</div>
							
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
								<%-- 	 <formElement:formInputBox idKey="club-num" labelKey="resident.confirm"  inputCSS="form-control ClubcardValue1" path="residentNumber" mandatory="false" />  --%>
									 <form:input path="residentNumber" cssClass="form-control ClubcardValue1" id="saValueCR" placeholder="Enter your South African ID number"/>
										<div class="control-group error">
											<div class="help-inline">
												<form:errors path="residentNumber"/>
										</div>
									</div>					
								</div>
															
							</div>
							<div class="clearfix">
								<form:radiobutton path="resident" id="saResi" name="saResi" cssClass="radio-hide" value="false"/>								
								<form:label path="resident" cssClass="radio-box clubcardhide" for="saResi" data-tohide="ClubcardValue1" data-toshow="dobSaOuter"><span>No</span></form:label>							
							</div>
						</div>
						<div class="form-group dobSaOuter clearfix">							
							<label><spring:theme code="birthday.title"/></label>
							<div class="dob">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
									<div class="dobWrapper">
									
									<form:select path="day" id="date">
										<c:forEach begin="1" end="31" step="1" var="day">
											<form:option value="${day}">${day}</form:option>
										</c:forEach>
									</form:select>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
									<div class="monthWrapper">
									
										<form:select path="month" id="month">
											<form:option value="00">Jan</form:option>
											<form:option value="01">Feb</form:option>
											<form:option value="02">Mar</form:option>
											<form:option value="03">Apr</form:option>
											<form:option value="04">May</form:option>
											<form:option value="05">June</form:option>
											<form:option value="06">July</form:option>
											<form:option value="07">Aug</form:option>
											<form:option value="08">Sep</form:option>
											<form:option value="09">Oct</form:option>
											<form:option value="10">Nov</form:option>
											<form:option value="11">Dec</form:option>
										</form:select>
									
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
									<div class="yearWrapper">
									
									<form:select path="year" id="year">
										<c:forEach begin="1915" end="2020" step="1" var="year">
											<form:option value="${year}">${year}</form:option>
										</c:forEach>
									</form:select>
						
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<button class="btn btn-save-change"><spring:theme code="forgottenDetails.submit"/></button>
				
				</form:form>
			</div>
		</div>
	</div>
</div>