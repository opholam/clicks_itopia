<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript">
	$("#pageNumber").val('${page}');
	$("#currentPageSize").val('${pageSize}');
	var hideLoadMore = '${hideLoadMore}';
	if(hideLoadMore == 'true')
	{
		$('.text-center').hide();
	}	
</script>
									
									<c:forEach items="${searchResultData}" var="promotion" end="${pageSize}">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-zero">
											<div class="categoryType red">
													${promotion.offer} 
											</div>
											<div class="home-promo-img">
												<img src="${promotion.productBanner.url}" alt="category" class="promo_backup_picture" onError="this.onerror=null;this.src='${themeResourcePath}/clicks/image/missing-promotions.jpg';" />
											</div>
											<div class="home-promo-text">
												<div class="banner_caption">
													<div class="caption-inner">
														<div class="caption-wrap">
															<div class="offer_sticker">
																<c:forEach items="${promotion.stickerMediaUrls}"
																	var="stickerUrl">
																	<img src="${stickerUrl}" alt="Clicks Promo">
																</c:forEach>
															</div>
															<h4>${promotion.title}</h4>
															<p>
															<c:choose>
															<c:when test="${not empty promotion.disclaimer}">
															<c:if test="${not empty promotion.description}">${promotion.description}.&nbsp;</c:if>${promotion.disclaimer}.
															</c:when>
															<c:otherwise>
															${promotion.description}
															</c:otherwise>
															</c:choose>
															</p>
															<button class="btn" onclick="window.location='${request.contextPath}/c/OH1?q=%3Arelevance%3AallPromotions%3A${promotion.code}'">Shop now</button>
														</div>
													</div>
												</div>
											</div>
											<div class="promoOffer">
												<strong>Valid until <fmt:formatDate value="${promotion.endDate}" pattern="dd MMM yyyy" /></strong>
											</div>				
										</div>
										
									</c:forEach>
						