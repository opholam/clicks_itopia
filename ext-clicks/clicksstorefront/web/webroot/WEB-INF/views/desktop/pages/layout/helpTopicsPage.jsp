<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
<input type="hidden" value="${cmsPage.uid}" id="helpTopicPage"/>
<div class="clearfix bg-white helpTop order-top-space">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<cms:pageSlot position="Section1" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
		</div>
	</div>
</div>
	
	<section class="main-container helpTopicWrapper">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="clearfix helpInnerWrapper">
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="bg-white helpAccordion">
							
							 <div class="accordion">
								<cms:pageSlot position="Section2A" var="feature" >
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>

							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="helpQuicklinks clearfix bg-white">
							<h4>Help topics</h4>
							
							<%-- <c:if test="${fn:containsIgnoreCase(cmsPage.title,cmsPage.title)}">
							<c:set var ="activeClass" value="currentLink"/> 
							</c:if> --%>
							<ul>
								<cms:pageSlot position="Section2B" var="feature">
								<cms:component component="${feature}"/>
								</cms:pageSlot>
							</ul>
							</div>
							<div class="contactCommonSection clearfix bg-white">
	
								<cms:pageSlot position="Section2C" var="feature">
								<cms:component component="${feature}"/>
								</cms:pageSlot>
							</div>
						</div>
					</div>
					<!-- End wrapper div -->
				</div>
			</div>
		</div>
	</section>
	</template:page>