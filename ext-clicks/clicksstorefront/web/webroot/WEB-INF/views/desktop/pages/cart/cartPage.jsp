<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:theme text="Your Shopping Cart" var="title"
	code="cart.page.title" />
	

<template:page pageTitle="${pageTitle}">
			<div id="wrapper">
				<div id="globalMessages">
					<common:globalMessages />
					
					<c:if test="${not empty cartData.isPaymentProcessing and cartData.isPaymentProcessing eq true}"><div class="promotions_home">
 <a href="#"><spring:message text="Payment is in progress for this basket. Please complete payment transaction or try again later." 
	code="cart.page.paymentProgress" /></a></div>
</c:if>
				</div>
				<!-- <div class="container"> -->
				<!-- <div class="accLandOuter clearfix shopBasket"> -->
				<section class="rewards account shopHead">
					<div class="container">
						<div class="shopTab">
							<div class="row">
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
									<div class="acc-heading">
										<h1>${cmsPage.title}</h1>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
									<c:choose>
										<c:when test="${not empty shoppingUrl}">
											<a href="${shoppingUrl}">back to shopping</a>
										</c:when>
										<c:otherwise>
											<a href="${contextPath}">back to shopping</a>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
				</section>


				<spring:theme code="basket.add.to.cart" var="basketAddToCart" />
				<spring:theme code="cart.page.checkout" var="checkoutText" />
				<%-- <common:globalMessages /> --%>
				<cart:cartValidation />
				<cart:cartPickupValidation />
				<c:url value="/cart/releaseVoucher" var="releaseVoucherUrl" scope="session" />
				<c:url value="/cart/checkout" var="checkoutUrl" scope="session" />
				<c:url value="${continueUrl}" var="continueShoppingUrl"
					scope="session" />
				<section class="main-container">
					<div class="contentWrapper bg-gray">
						<div class="container">
							<c:if test="${not empty cartData.entries}">
								<div class="accLandOuter clearfix shopBasket">
									<div class="row">

										<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
											<cms:pageSlot position="TopContent" var="feature"
												element="div" class="span-24">
												<cms:component component="${feature}" />
											</cms:pageSlot>
											<!-- Voucher Redemption -->
										  <c:url value="/cart/redeem" var="redeem" />
											<form:form action="${redeem}" method="POST"
													commandName="voucherform">
											<div class="shopPoints clearfix">
											<c:if test="${not empty cartvoucherexpirederror}">
												<div class="red">
													<spring:theme
														code="cart.voucher.redeem.appliedorexpired.error" />
												</div>
											</c:if>
											<c:if test="${not empty cartvoucherredeemerror}">
												<div class="red">
													<spring:theme code="cart.voucher.redeem.error" />
												</div>
											</c:if>
											<c:if test="${not empty emptyVoucherCodeError}">
												<div class="red">
													<spring:theme code="${emptyVoucherCodeError }" />
												</div>
											</c:if>
											<c:if test="${not empty voucherCodeReleased}">
												<div class="red">
													<spring:theme code="${voucherCodeReleased}" />
												</div>
											</c:if>
											<div class="col-xs-12 col-md-6 vouchercode-title">
											 <h4>Gift code or discount voucher?</h4>
									         <cms:pageSlot position="cartMessageSectionSlot" var="feature">
											  <div class="voucher-msg">
									            <span class="voucher-info"></span>
									            <div class="voucher-info-box">
    												<%--  ${feature.content} --%>
    												<cms:component component="${feature}" />
									            </div>
									          </div>
									           </cms:pageSlot>
											</div>
											<div class="col-xs-12 col-md-6">
											    <div class="vouchercode-box">
													<input type="text" class="txt" name="voucherCode"
													placeholder="Enter code"
													<c:if test="${not empty appliedVouchers}">value="${appliedVouchers[0].voucherCode}"</c:if>></input>
													<button type="submit" class="btn btn-primary">
														<spring:theme code="text.button.apply" text="Apply" />
													</button>
												</div>
											</div>	
											<span class="totaldiscount"> </span>											
											</form:form>
											<div class="voucher-code">										
										<c:if test="${fn:length(cartData.appliedVouchers) > 0 && cartData.orderDiscounts.value>0.0 && empty accErrorMsgs}"> 
										<c:forEach items="${cartData.appliedVouchers}" var="appliedVoucher">
										
										Applied &nbsp;  ${appliedVoucher.description} 
<!-- 												<h5> -->
<%-- 												<c:set var="currencySymbol" value="${appliedVoucher.currency}"/> --%>
<%-- 												<p><c:if test="${not empty currencySymbol.symbol}">${currencySymbol.symbol}<fmt:formatNumber type="number" minFractionDigits="2"  value="${appliedVoucher.value}"/>&nbsp; Off applied.</c:if></p> --%>
<!-- 												</h5> -->
<!-- 												<h5> -->
<%-- 												<c:if test="${ empty currencySymbol.symbol}"><span class="icon-tick"></span>${currencySymbol.symbol}<fmt:formatNumber type="number" minFractionDigits="0"  value="${appliedVoucher.value}"/>%&nbsp; Off applied.</c:if> --%>
<!-- 												</h5> -->
											<c:choose>
												<c:when test="${not empty appliedVoucher.serialVoucherCode}">
													<a href="${releaseVoucherUrl}?voucherCode=${appliedVoucher.serialVoucherCode}" >Remove Voucher</a>
												</c:when>
												<c:otherwise>
													<a href="${releaseVoucherUrl}?voucherCode=${appliedVoucher.voucherCode}" >Remove Voucher</a>
												</c:otherwise>
											</c:choose>
											</c:forEach> 
											</c:if>
											</div>	
											</div>

											
											<!-- Voucher Redemption -->
											<!-- Shopping Points -->
											<div class="shopPoints clearfix">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<c:choose>
														<c:when test="${user.uid eq 'anonymous'}">
															<cms:pageSlot position="Section8B" var="feature">
																<cms:component component="${feature}" />
															</cms:pageSlot>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${empty user.memberID }">
																	<cms:pageSlot position="Section8B" var="feature"
																		element="div">
																		<cms:component component="${feature}" />
																	</cms:pageSlot>
																</c:when>
																<c:otherwise>
																	<cms:pageSlot position="Section7A" var="feature">
																		<cms:component component="${feature}" />
																	</cms:pageSlot>
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<c:choose>
														<c:when test="${user.uid eq 'anonymous'}">
													<cms:pageSlot position="Section8A" var="feature"
														element="div" class="collect-points">
														<cms:component component="${feature}" />
													</cms:pageSlot>
													</c:when>
													<c:otherwise>
														<div class="input-append collect-points">
											<c:choose>
											<c:when test="${empty user.memberID }">
											<cms:pageSlot position="Section8A" var="feature"
												element="div">
												<cms:component component="${feature}" />
											</cms:pageSlot>
											</c:when>
											<c:otherwise>
											<cms:pageSlot position="Section8B" var="feature"
												element="div">
												<cms:component component="${feature}" />
											</cms:pageSlot>
											</c:otherwise>
											</c:choose>
											</div>
													</c:otherwise>
													</c:choose>
												</div>
											</div>
											
											<!-- Shopping Points -->
												<div class="deliverPoints clearfix cart-page-deliver">
													<div class="border clearfix">
														<div
															class="col-lg-6 col-md-5 col-sm-5 col-xs-12 paddingNone">
															<cms:pageSlot position="CenterLeftContentSlot"
																var="feature">
																<cms:component component="${feature}" />
															</cms:pageSlot>
														</div>
														<div
															class="col-lg-6 col-md-7 col-sm-7 col-xs-12 paddingNone">
															<div class="grant-total">
																<cms:pageSlot position="CenterRightContentSlot"
																	var="feature">
																	<cms:component component="${feature}" />
																</cms:pageSlot>
															</div>
														</div>
													</div>

													<!-- Continue shopping button -->
													<div id="shoppingButons" class="clearfix">
														<div class="col-lg-10 col-lg-offset-2 text-right">
															<cms:pageSlot position="BottomContentSlot" var="feature">
																<cms:component component="${feature}" />
															</cms:pageSlot>
														</div>
														<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
															<div class="guarantee">
																<cms:pageSlot position="Section5A" var="feature">
																	<cms:component component="${feature}" />
																</cms:pageSlot>
															</div>
														</div>
														<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
															<div class="master_logo">
																<cms:pageSlot position="Section5B" var="feature">
																	<cms:component component="${feature}" />
																</cms:pageSlot>
															</div>
														</div>
													</div>
												</div>
										</div>
										<!-- Left side -->
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											<div class="basketDelivery">
												<div class="image-center bcBox">
													<cms:pageSlot position="Section6A" var="feature">
														<cms:component component="${feature}" />
													</cms:pageSlot>
												</div>
												<cms:pageSlot position="Section6B" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
											<div class="basketDelivery">
												<div class="image-center bcBox">
													<cms:pageSlot position="Section2A" var="feature">
														<cms:component component="${feature}" />
													</cms:pageSlot>
												</div>
												<cms:pageSlot position="Section2B" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
											<div class="basketDelivery">
												<div class="image-center bcBox">
													<cms:pageSlot position="Section3A" var="feature">
														<cms:component component="${feature}" />
													</cms:pageSlot>
												</div>
												<cms:pageSlot position="Section3B" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
											<div class="basketDelivery">
												<div class="image-center bcBox">
													<cms:pageSlot position="Section4A" var="feature">
														<cms:component component="${feature}" />
													</cms:pageSlot>
												</div>
												<cms:pageSlot position="Section4B" var="feature">
													<cms:component component="${feature}" />
												</cms:pageSlot>
											</div>
										</div>
										<!-- End Right side -->
									</div>
								</div>
							</c:if>
							<c:if test="${empty cartData.entries}">
								<div class="searchErr clearfix ">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<cms:pageSlot position="EmptyCartMiddleContent" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</div>
							</c:if>
						</div>
					</div>
				</section>
			</div>
	<script>
				ga('ec:setAction','checkout', {'step': 1});
			</script>
	<!-- </div>
	</div> -->
</template:page>
