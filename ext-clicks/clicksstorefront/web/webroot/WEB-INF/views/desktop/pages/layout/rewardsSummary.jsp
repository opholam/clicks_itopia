<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>



<!-- End success message -->
							<!-- pointsDetails start-->
							<div class="ptDetailsWrapper">	
								<div class="div-rounded pointsDetails">	
									<div class="pricePtWrapper">
										<div class="price blue">
											<c:choose>
												<c:when test="${not empty selectedaccount and not empty selectedaccount.pointsBalance}">
													${selectedaccount.pointsBalance}
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
										</div>
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">
											<c:choose>
												<c:when test="${not empty selectedaccount and not empty selectedaccount.pointsBalance}">
													Points earned so far
												</c:when>
												<c:otherwise>
													Shop at Clicks to earn points.
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
								<div class="div-rounded pointsDetails">	
									<div class="pricePtWrapper">
										<div class="price blue">
											<c:choose>
												<c:when test="${not empty selectedaccount and not empty selectedaccount.pointsBalance}">
													R<dateFormat:dateFormat conversionType="rand" input="${selectedaccount.pointsBalance / 10}"/>
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
										</div>
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">
											<c:choose>
												<c:when test="${not empty selectedaccount and not empty selectedaccount.pointsBalance}">
													Equivalent cashback
												</c:when>
												<c:otherwise>
													Cashback Rewards coming soon.
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
								<div class="div-rounded pointsDetails">	
									<div class="pricePtWrapper">
										<div class="blue">
										<c:choose>
										<c:when test="${not empty cashbackDate}">
										<dateFormat:dateFormat conversionType="date" input="${cashbackDate}" format="dd MMM yyyy"/>
										</c:when>
										<c:otherwise>
										<dateFormat:dateFormat conversionType="date" input="${user.pointStatement.endDate}" format="dd MMM yyyy"/>
										</c:otherwise>
										</c:choose>
										</div>
										<!--<span>2015</span>-->
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">You'll receive this on</div>
									</div>
								</div>
								<div class="div-rounded pointsDetails ptpromotion">	
									<div class="pricePtWrapper">
										<div class="price red">
											<c:choose>
												<c:when test="${not empty cbraccount}">
													R<dateFormat:dateFormat conversionType="rand" input="${cbraccount.availableBalance}"/>	
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
										</div>
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">Available to spend now
										<a href="${contextPath}/specials">See all promotions</a>
										</div>
									</div>
								</div>
							</div>
							<div class="ptDetailsMessage">
								<c:set var="startDate"><dateFormat:dateFormat conversionType="date" input="${user.pointStatement.startDate}" format="dd MMMM yyyy"/></c:set>
								<c:set var="endDate" ><dateFormat:dateFormat conversionType="date" input="${user.pointStatement.endDate}" format="dd MMMM yyyy"/></c:set>
								<%-- <fmt:formatDate var="endDate" dateStyle="long" value="${user.pointStatement.endDate}" /> --%>
								Current points collection period: <span>${startDate} - ${endDate}</span>
							</div>

