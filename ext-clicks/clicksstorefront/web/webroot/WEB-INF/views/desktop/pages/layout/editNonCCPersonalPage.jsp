<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	<div id="globalMessages">
		 <common:globalMessages />  
	</div>

<c:url var="profileUrl" value="${contextPath}/my-account/edit-personal-details"/>
<!-- Right Side Content  -->
			<form:form action="${contextPath}/my-account/edit-personal-details" method="post" commandName="editNonClubCardForm" >
			<!-- Right Side Content  -->
			<div class="editNonCCpersonal_page">
							<div class="ac-no-cc-wrap">
									<div class="row ">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class=" bg-white  clearfix">
												<div class="clearfix bg-light-blue">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
														<h2><spring:theme code="edit.profile.title"/></h2>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 clearfix">
													 	<div class="row bg-white clearfix">
									<div class="pad-spacing form-wrap">
								
									<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.enroll.EMailAddress" labelKey="customer.edit.profile.EMailAddress" path="eMailAddress" inputCSS="text mandatory editEmail"  mandatory="true" />
										</div>
										<div class="form-group clearfix">
											<formElement:formCheckbox idKey="emailIDEditable" labelKey="edit.email.id.checkbox.account.exists" path="emailIDEditable" inputCSS="text editPwdCheckbox"/>
										</div>
										<label class="edit-email-checkbox-validation red"><spring:theme code="edit.email.id.checkbox.account.exists.password.validation" ></spring:theme> </label>
										<div class="form-group clearfix editPwdInput">
											 <formElement:formPasswordBox idKey="customer.enroll.Password" labelKey="customer.enroll.Password" path="password" inputCSS="text"/>
										</div>
										
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.enroll.FirstName" labelKey="customer.enroll.FirstName" path="firstName" inputCSS="text mandatory" mandatory="true" />
										</div>
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.enroll.PreferedName" labelKey="customer.edit.profile.PreferedName" path="preferedName" inputCSS="text" mandatory="false"/>
										</div>
										<div class="form-group clearfix">
											<formElement:formInputBox idKey="customer.enroll.LastName" labelKey="customer.enroll.LastName" path="lastName" inputCSS="text" mandatory="true"/>
										</div>
										
											<div class="form-group saResident">
													<label><spring:theme code="customer.create.account.isSAResident"/> </label>
													 <div class="form-group clearfix">
													 	<div class="clearfix">
													 		 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 paddingNone">
													 		 <div class="custom_hide radioOuter">
														 		<form:radiobutton path="isSAResident" value="1" label="yes"  cssClass="radio-hide" />
															 </div>
															 </div>
															<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
																<formElement:isClubcardFormInputBox idKey="bb" labelCSS="" labelKey="" path="IDNumber" inputCSS="" mandatory="false"  />
			           										</div>
			           									</div>	
			           									
			           									<div class="clearfix">
			           										<div class="customno radioOuter">
			           											<form:radiobutton path="isSAResident" value="0" label="No"  cssClass="radio-hide" />
			           										</div>
			           									</div>
		           									 </div>
		           									 
		           									<div class=" clearfix dobSaOuterFirst" id="cc">
		           									 <label><spring:theme code="customer.create.account.SA_DOB"/> </label>
		           									 <div class="dob">
		           									 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 date">
		           									  <div class="dobWrapper">
															<formElement:DOBformSelectBox selectCSSClass="dobSa12 dateCheck" labelCSS="dobSa12" idKey="customer.create.account.SA_DOB_day" labelKey="customer.create.account.SA_DOB_day" path="SA_DOB_day" mandatory="true" items="${days}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 month">
													  <div class="monthWrapper">
															<formElement:DOBformSelectBox selectCSSClass="monthCheck" idKey="customer.create.account.SA_DOB_month" labelKey="customer.create.account.SA_DOB_month" path="SA_DOB_month" mandatory="true" items="${months}" />
													  </div>
													  </div>
													  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 year">
													  <div class="yearWrapper">
															<formElement:DOBformSelectBox selectCSSClass="yearCheck" idKey="customer.create.account.SA_DOB_year" labelKey="customer.create.account.SA_DOB_year" path="SA_DOB_year" mandatory="true" items="${years}" />
													  </div>
													  </div>
													</div>
													</div>
											</div>
									<div class="form-actions">
					<ycommerce:testId code="nonCC_SaveUpdatesButton">
						<button class="btn primary_btn btn-block positive" type="submit"><spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/></button>
					</ycommerce:testId>
				</div>
						</div>			
								</div>
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
						<!-- End Right side Conent -->
			</form:form>
			</div>
			
<!-- End Right side Conent -->
