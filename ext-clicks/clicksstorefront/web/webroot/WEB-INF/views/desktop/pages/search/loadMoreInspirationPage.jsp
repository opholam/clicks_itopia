<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set value="${fn:length(inspirationData)}" var="articlesInPage"/>
<c:set value="${totalNoOfInspirationArticles}" var="totalNoOfArticles"/>
	
	<c:forEach items="${inspirationData}" var="articleData" begin="0" end="${articlesSize}">
		<div class="article-container row insparticlescount">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<img class="img-responsive" src="${articleData.articleImage}" alt="article">
			</div>
			<div class="col-lg-8 col-md-7 col-sm-8 col-xs-8">
				<h4>
					<c:if test="${articleData.articleType=='MAGAZINES'}">
						<a href="${contextPath}/clubcard-magazine/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='HEALTH'}">
						<a href="${contextPath}/health/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='CONDITIONS'}">
							<a href="${contextPath}/health/conditions/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='MEDICINES'}">
						<a 	href="${contextPath}/health/medicines/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
					<c:if test="${articleData.articleType=='VITAMINS'}">
						<a 	href="${contextPath}/health/supplements/article-view/${articleData.articleTitle}">${articleData.titleName}</a>
					</c:if>
				</h4>
				<div class="articleTagWrapper">
				<c:forEach items="${articleData.articleTags}" var="articleTag">
					<span class="articleTag">${articleTag}</span>
				</c:forEach>
				</div>
				<div class="content-height">
					<p>${articleData.titleContent}</p>
				</div>
			</div>
		</div>
	</c:forEach>
		<c:if test="${totalNoOfArticles > articlesInPage}" >
					<div class="text-center searchLoadMoreInspirationArticles">
						<a class="btn btn-load-articles">Load more articles</a>
					</div>
				</c:if>

