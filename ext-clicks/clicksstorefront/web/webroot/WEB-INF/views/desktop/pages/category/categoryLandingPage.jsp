<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<c:set var="i" value="1" scope="page" />


<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>




	<div class="clearfix bg-white specialBlockTop category-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="product-left">
						<h2>${categoryName}</h2>
						<p>${categoryDescription}</p>
					</div>
				</div>
				<c:forEach var="featuredProduct" items="${featuredProdListm}">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 image-center">
							<a href="${featuredProduct.url}"> <product:productPrimaryImage
									product="${featuredProduct}" format="thumbnail" />
							</a>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 sp-content">
							<h4 class="blue">
								<!-- Featured product -->
								<spring:theme code="featured.product.title" />
							</h4>
							<h4>${featuredProduct.name}</h4>
							<p>${featuredProduct.summary}</p>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>


	<!--  -->


	<section class="main-container">
		<div class="contentWrapper bg-gray">
			<div class="container">
				<div class="row">
					<div class="categoryOuter clearfix">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="bg-white categorySidebar">
								<h2>
									<spring:theme code="in.title" />
									&nbsp;${categoryName}
								</h2>
								<ul>
									<%-- 	<c:forEach var="featuredProduct" items="${featuredProdListm}">
										<li><a href="javascript:void(0)"></a></li>
									</c:forEach> --%>
									<c:forEach items="${subCategories}" var="subCat">
										<li><a href="c/${subCat.code}">${subCat.name}</a></li>
									</c:forEach>
								</ul>
							</div>
							<div class="bg-white categorySidebar">
								<h2>
									<!-- Featured brands -->
									<spring:theme code="featured.brands.title" />
								</h2>
								<ul class="specialBrand">
									<c:forEach items="${subCategories}" var="subCategory">
										<c:forEach items="${subCategory.supercategories}"
											var="supCategory">
										
												
												
												<c:if test="${superCat.name eq 'brands'}">
													
															<li><a href="brand/${subCategory.code}"><img
																	src="${subCategory.logo.get(0).url}" /></a></li>
													
												</c:if>
										
										</c:forEach>
									</c:forEach>
								</ul>
								<ul>
									<c:forEach items="${subCategories}" var="subCategory">
										<c:forEach items="${subCategory.supercategories}"
											var="supCategory">
											<c:forEach items="${supCategory.supercategories}"
												var="superCat">
												<c:if test="${superCat.name eq 'brands'}">
													<li><a href="javascript:void(0)">${subCategory.name}</a></li>
												</c:if>
											</c:forEach>
										</c:forEach>
									</c:forEach>
								</ul>
							</div>
						</div>
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 categoryPage">
							<div class="contentWrapper clearfix bgred">

								<div
									class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titleWrapper">
									<div class="pull-left">
										<h2>
											<!-- Latest promotions -->
											<spring:theme code="latest.promotions.title" />
										</h2>
									</div>
									<div class="pull-right text-right">
										<a class="arrow-right" href="javascript:void(0)">
											<!-- See all promotions --> <spring:theme
												code="see.all.promotions.title" />
										</a>
									</div>
								</div>


							</div>

							<!-- Latest Promotions -->


							<div class="filterContent clearfix">
							

									<product:productPromotions
										productPromotionList="${productPromotionList}" />

							</div>


							<!-- Featured Products -->

							<div class="contentWrapper clearfix bg-white bottom-marg">
								<product:productNewInFrom
									featuredProductComponents="${featuredProductComponent}" />
							</div>

							<!-- More ClubCard Category -->

							<div class="clearfix moreCategory bg-white">
								<div
									class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titleWrapper bgblue">
									<div class="pull-left">
										<h2>
											<!-- More clubcard <span>category<span> -->
											<a class="arrow-right" href="#"> <spring:theme
													code="more.clubcard.title" /><span><spring:theme
														code="categoty.title" /></span>
											</a>

										</h2>
									</div>
								</div>


								<c:forEach items="${cmsMediaLinkComponent}"
									var="clubcardProduct">
									<div class="clubCardBot">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

											<cms:component uid="${clubcardProduct.uid}" />
										</div>
									</div>
								</c:forEach>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</template:page>
