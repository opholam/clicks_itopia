<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set value="${fn:length(competitions)}" var="TotalCompetitions"/>
<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages />
	</div>
	<c:if test="${!error}">
		<div class="healthTop magzineTop">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 paddingLeftNone">
						<cms:pageSlot position="Section1A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<div class="fb-like-page">
							<%-- <cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>  --%>
							<div class="fb-page"
								data-href="https://www.facebook.com/ClicksSouthAfrica"
								data-small-header="false" data-adapt-container-width="true"
								data-hide-cover="false" data-show-facepile="false"
								data-show-posts="true" width="350px" height="300px">
								<div class="fb-xfbml-parse-ignore">
									<blockquote cite="https://www.facebook.com/ClicksSouthAfrica">
										<a href="https://www.facebook.com/ClicksSouthAfrica">Clicks</a>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="main-container bg-gray clearfix hLandingPage" style="position: relative;">
			<div class="container">
				<div class="row">
					<div class="commonTab healthTab">
						<div class="tabContentWrapper healthTabWrapper tb-res clearfix">
							<input id="maxNoOfCompetitions" type="hidden" value="${maxNoOfCompetitionsShown}">
							<input id="contextPath" type="hidden" value="${contextPath}">
							
							
							<div class="active-tab-outer clearfix">
							<div id="competitions">
								<c:forEach var="competition" items="${competitions}"
									varStatus="status" begin="0" end="${maxNoOfCompetitionsShown}">
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 competitionsCount">
										<div class="tabThumbWrapper">
											<a href="${contextPath}/competitions/view/${competition.code}">
											<div class="tabThumbImage">
												<img src="${competition.media.url}"	alt="${competition.media.altText}">
													<div class="TagWrapper">
														<c:forEach items="${competition.tags}" var="competitionTag">
															<span class="TagOuter">${competitionTag}</span>
														</c:forEach>
													</div>
											</div>
											<p>${competition.title}</p>
											</a>
										</div>
									</div>
								</c:forEach>
								<c:if test="${TotalCompetitions > maxNoOfCompetitionsShown }">
									<div class="text-center  load-more-competitions">
										<button class="btn btnBig">Load more competitions</button>
									</div>
								</c:if>
							</div>
							</div>
						
							
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="clearfix bg-light-blue">
						<div
							class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
							<div class="pull-left">
								<cms:pageSlot position="Section2Head" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
						</div>
					</div>
					<div class="clearfix bg-white magazineWrapper">
						<div class="row">
							<cms:pageSlot position="Section2" var="feature">
								<div class="col-md-4 col-sm-12 col-xs-12 ">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>


			</div>
		</section>
	</c:if>
	<c:if test="${error}">
	<div class="container">
		<p>Unable to load the competitions</p>
	</div>
	</c:if>
</template:page>