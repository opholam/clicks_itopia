<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url var="orderDetailPage" value="/guest/order/${orderData.guid}"></c:url>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
		<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<h2>Thank you for your order</h2>
						<p>Your order is now being processed, and an order confirmation email has been sent to <a id="orderConfirmationEmail" href="#">${email}</a> </p>
				<div class="form-wrap">
					 <sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
					 <form action="https://comms.clicks-mail.com/public/contacts/subscription" method="post" id="item-form" name="item-form">
					 	<input type="hidden" name="op" value="subscribe" id="op">
						<div class="form-group clearfix">
						<fieldset>
							<label>Never miss a sale or offer at Clicks</label>
							<div class="confirmation-subscribe">
							<input id="contact_email" placeholder="Enter email address" type="text" name="contact_email" value="">
							<input id="submit_subscribe" type="submit" name="subscribe" class="btnSubscribe btn" value="Subscribe"/>
							</div>
						</fieldset>
						</div>
							<input type="hidden" name="form_hash" value="QcKplmD7wcdm5ADP" id="form_hash" >
							<input type="hidden" name="list_id[]" value="1244" id="list_id[]">
							<input type="hidden" name="list_id[]" value="1243" id="list_id[]">
							<div class="newError">Please enter valid email</div>
						</form>
					</sec:authorize>
						<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
						<c:url var="orderDetailPage" value="/my-account/order/${orderCode}"></c:url>
						<c:url var="myAccountUrl" value="/my-account"></c:url>
						<a href="${myAccountUrl}">
						Go to your account</a>
						 </sec:authorize>
				</div>
					
					
					</div>	
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="image-center">
							<cms:pageSlot position="GoogleAd" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							</div>
				</div>
			</div>
		</div>
	</div>
											<c:choose>
											<c:when test="${not empty orderData.clicksOrderCode}">
											<c:set var="orderCode" scope="page" value="${orderData.clicksOrderCode}"/>
											</c:when>
											<c:otherwise>
											<c:set var="orderCode" scope="page" value="${orderData.code}"/>
											</c:otherwise>
											</c:choose>	
		<section class="main-container bg-gray clearfix order-outerWrapper">
		<div class="container">
			<div class="orderConfirm-top clearfix bg-white">
					<div class="pull-left">Order Summary</div>
					<div class="pull-right"><a href="${orderDetailPage}">#${orderCode}</a></div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="bg-white clearfix checkout-confirm-left">
						<div class="pad-spacing clearfix">
								<div class="pull-right" style="display: none;"><a href="#">Track you order</a></div>
							<div class="del-method">
								<h2>Delivery Method</h2>
								<div class="confirm-content">${deliveryMode.name}</div>
							</div>
							<order:deliveryAddressItem order="${orderData}"/>
							<order:paymentMethodItem order="${orderData}"/>
							<c:if test="${clubCardPointVal > 0}">
							<div class="del-method order_con-content">
								<h2>ClubCard points</h2>
								<div class="order-content">
								<c:choose>
								<c:when test="${!orderData.guestCustomer && not empty user.memberID}">
								${clubCardPointVal} points earned
								</c:when>
								<c:otherwise>
								You could have earned ${clubCardPointVal} ClubCard points.
								</c:otherwise>
								</c:choose>
							</div>
							</div>
							</c:if>
						</div> 
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="bg-white clearfix orderSummOuter">
					
						<div class="order-summary-list">
							<ul>
<%-- 							<c:forEach items="${orderData.deliveryOrderGroups}" var="orderGroup">
									<order:orderDetailsItem order="${orderData}" orderGroup="${orderGroup}" />
							</c:forEach>	 --%>
							<order:orderDetailsItem order="${orderData}" orderGroup="${orderGroup}" showAddToCart="false"/>
							</ul>
								<section class="ord-summary-total clearfix">
								<div class="subTotalexcluding">
									<div class="col-lg-8 col-md-9 col-sm-9 col-xs-8">
										<div class="summary-content">Subtotal<span> (excluding delivery)</span></div>
									</div>
									<div class="col-lg-4 col-md-3 col-sm-3 col-xs-4 price-right">
									<div class="price-wrap">
										 <div class="price blue">
										 		<c:set var="subTotaldec" value="${fn:split(orderData.subTotal.formattedValue, '.')}" />
												<c:set var="subTotaldec1" value="${subTotaldec[0]}" />
												<c:set var="subTotaldec2" value="${subTotaldec[1]}" />
										 			${subTotaldec1}<sup>${subTotaldec2}</sup>
										 </div>
										 </div>
									</div>
<!-- 									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
										<div class="summary-content">Giftwrap</div>
									</div> 
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<div class="price blue">R28<sup>60</sup></div> 
									</div>-->
									</div>
								 <c:if test="${orderData.totalDiscounts.value > 0}"> 
								 <div class="totalsaving">
									<div class="col-lg-8 col-md-9 col-sm-9 col-xs-8">
										<div class="summary-content red">(Total savings</div>
									</div>
									<div class="col-lg-4 col-md-3 col-sm-3 col-xs-4 price-right">
										<div class="savingsValue">
												<c:set var="savingsdec" value="${fn:split(orderData.totalDiscounts.formattedValue, '.')}" />
												<c:set var="savingsdec1" value="${savingsdec[0]}" />
												<c:set var="savingsdec2" value="${savingsdec[1]}" />
										<div class="price">${savingsdec1}<sup>${savingsdec2}</sup>)</div> 
										</div>
									</div>
									</div>
									</c:if>
									<div class="deliveryfee">
									<div class="col-lg-8 col-md-9 col-sm-9 col-xs-8">
										<div class="summary-content">Delivery fee</div>
									</div>

									<div class="col-lg-4 col-md-3 col-sm-3 col-xs-4 price-right">
									<c:choose>
									<c:when test="${orderData.deliveryCost.value > 0}">
												<c:set var="deliverydec" value="${fn:split(orderData.deliveryCost.formattedValue, '.')}" />
												<c:set var="deliverydec1" value="${deliverydec[0]}" />
												<c:set var="deliverydec2" value="${deliverydec[1]}" />
									<div class="price-wrap">	<div class="price blue">${deliverydec1}<sup>${deliverydec2}</sup></div> </div>
									</c:when>
									<c:otherwise>
									<div class="price">FREE</div>
									</c:otherwise>
									</c:choose>
									</div>
									</div>
									<div class="totalcost">
									<div class="col-lg-8 col-md-9 col-sm-9 col-xs-8">
										<div class="summary-content">
											Total paid
												<c:set var="totalTaxdec" value="${fn:split(orderData.totalTax.formattedValue, '.')}" />
												<c:set var="totalTaxdec1" value="${totalTaxdec[0]}" />
												<c:set var="totalTaxdec2" value="${totalTaxdec[1]}" />
											
										<p class="specialCheckout">Total includes ${totalTaxdec1}<sup>${totalTaxdec2}</sup> VAT</p>
										</div>
									</div>
									<div class="col-lg-4 col-md-3 col-sm-3 col-xs-4 price-right">
												<c:set var="totalPricedec" value="${fn:split(orderData.totalPriceWithTax.formattedValue, '.')}" />
												<c:set var="totalPricedec1" value="${totalPricedec[0]}" />
												<c:set var="totalPricedec2" value="${totalPricedec[1]}" />
										<div class="grandTotal"><div class="price-wrap"><div class="price blue">${totalPricedec1}<sup>${totalPricedec2}</sup></div> </div></div>
									</div>
									</div>
								</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<c:if test="${gaOderConfirmation}">					
	<script>
	ga('ec:setAction', 'purchase', {
			'id': "${orderCode}",
			'revenue': "${fn:replace(orderData.totalPriceWithTax.formattedValue,',', '')}",
			'tax': "${orderData.totalTax.formattedValue}",
			'shipping': "${orderData.deliveryCost.formattedValue}"
	});
	
	</script>
</c:if>
<%-- 		<cms:pageSlot position="TopContent" var="feature" element="div" class="span-24 top-content-slot cms_disp-img_slot">
			<cms:component component="${feature}"/>
		</cms:pageSlot>


	<div class="span-24">
		<div>
			<a href="${request.contextPath}" class="button positive right"><spring:theme code="checkout.orderConfirmation.continueShopping" /></a>
		</div>
		<div class="orderHead">
			<ycommerce:testId code="orderConfirmation_yourOrderResults_text">
				<div><spring:theme code="checkout.orderConfirmation.thankYouForOrder" />
				Thank you for your order
				</div>
				<div><spring:theme code="checkout.orderConfirmation.copySentTo" arguments="${email}"/></div>
			</ycommerce:testId>
			<div><spring:theme code="text.account.order.orderNumber" text="Order number is {0}" arguments="${orderData.code}"/></div>
			<div><spring:theme code="text.account.order.orderPlaced" text="Placed on {0}" arguments="${orderData.created}"/></div>
			<c:if test="${not empty orderData.statusDisplay}">
				<spring:theme code="text.account.order.status.display.${orderData.statusDisplay}" var="orderStatus"/>
				<div><spring:theme code="text.account.order.orderStatus" text="The order is {0}" arguments="${orderStatus}"/></div>
			</c:if>
		</div>
		
		

			<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
				<div class="span-24 delivery_stages-guest last">
					<user:guestRegister actionNameKey="guest.register.submit"/>
				</div>
			</sec:authorize>
		
		
		<div class="orderBoxes clearfix">
		
		
				<order:deliveryAddressItem order="${orderData}"/>
				<order:deliveryMethodItem order="${orderData}"/>
				<order:paymentMethodItem order="${orderData}"/>
	
		</div>	

			<c:forEach items="${orderData.pickupOrderGroups}" var="orderGroup">
				<order:orderPickupDetailsItem order="${orderData}" orderGroup="${orderGroup}" />
			</c:forEach>	
			
			
						

			<div class="span-16">
				<order:receivedPromotions order="${orderData}"/>
			</div>
			<div class="span-8 right last">
				<order:orderTotalsItem order="${orderData}" containerCSS="positive"/>
			</div>
		

	</div>
	
	
	<cms:pageSlot position="SideContent" var="feature" element="div" class="span-24 side-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<div>
		<a href="${request.contextPath}" class="button positive right"><spring:theme code="checkout.orderConfirmation.continueShopping" /></a>
	</div> --%>
</template:page>
