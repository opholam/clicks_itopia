<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
		<div id="wrapper">
			<div class="clearfix bg-white editprofiletop order-top-space">
		<div class="container">
		<section class="rewards account">
		<div class="container">
			<div class="acc-heading">
				<h1>Link a ClubCard to this account</h1>
			</div>
		</div>
	</section>	
	</div>
		</div>
	<section class="main-container">
		<div class="createOuterWrapper bg-gray">
		<div class="container">
				<div class="row">
						<div class="accLandOuter clearfix accountAside">

							<!-- Left side 
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="filter">

									<!-- Left side first link 
									<div class="my-account-block bg-white">
										<h2>
											<a href="javascript:void(0)" class="active"><spring:theme
													code="myaccount.summary.title" /></a>
										</h2>
									</div>
									<!-- End Left side first link 

									<ul class="panel-group" id="accordion">
										<!-- Left Drop down 1 - Accordion 
										<cms:pageSlot position="SideNavigation" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
										<!-- End Left Drop down 1 - Accordion
									</ul>
									<!-- End Last Drop down  
								</div>
							</div>
							<!-- End Left side -->

							<!-- Right Side Content  -->
							<cms:pageSlot position="Section1" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
							
							
							
							<!-- Right Side Content  -->
						</div>
					</div>
				</div>
		</div>
	</section>	
	</div>
</template:page>