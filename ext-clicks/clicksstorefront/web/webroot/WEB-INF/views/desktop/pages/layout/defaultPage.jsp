<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	
		<c:set var="hideDiv" value="false" />
				<cms:pageSlot position="Section1A" var="feature">
				 <c:if test="${hideDiv eq 'false'}">
						<div class="articleTop clinic_top">
							<div class="container">
								<div class="row">
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
								<c:set var="hideDiv" value="true" />
				</c:if>
						<cms:component component="${feature}" />
						</div>
				</cms:pageSlot>
					<cms:pageSlot position="Section1B" var="feature" element="div" class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				<c:if test="${hideDiv eq 'true'}">
								</div>
							</div>
						</div>
			</c:if>
<div class="contentWrapper bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <c:set var="divClass" value="${cmsPage.uid eq 'maintenancePage' ? 'maintenancePage' : ' '}"/>
        <div id="static-contentPage" class="bg-white pagedata_wrapper siteMapcontainer">
			
			<section class="main-container clearfix clinic-wrap ${divClass}">
				<cms:pageSlot position="Section2" var="feature">
							<cms:component component="${feature}"/>
				</cms:pageSlot>
			
			<c:set var="styleClass" value="${cmsPage.uid eq 'siteMapStoresPage' ? 'store-siteMap' : 'servicesOffering'}"/>
					<cms:pageSlot position="Section3" var="feature" element="ul" class="${styleClass}">
						<cms:component component="${feature}" element="li"/>
					</cms:pageSlot>
				</section>
				</div>
			</div>
		</div>
	</div>
</div>

</template:page>

