<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages />
	</div>
	
	<c:if test="${cmsPage.uid eq 'magazineLandingPage'}">
		<div class="healthTop magzineTop">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 paddingLeftNone">
						<cms:pageSlot position="Section1A" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>				
						<div class="social-wrap clearfix">
							<h3><spring:theme code="magazine.article.dontmiss" text="Don't miss an article"/></h3>
							<ul>
								<cms:pageSlot position="SocialMedia" var="feature">
									<li>	<cms:component component="${feature}" /></li>
								</cms:pageSlot>
							</ul>
						</div>						
						<cms:pageSlot position="Section1B" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
			
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<div class="fb-like-page">							
							<div class="fb-page"
								data-href="https://www.facebook.com/ClicksSouthAfrica"
								data-small-header="false" data-adapt-container-width="true"
								data-hide-cover="false" data-show-facepile="false"
								data-show-posts="true" width="350px" height="300px">
								<div class="fb-xfbml-parse-ignore">
									<blockquote cite="https://www.facebook.com/ClicksSouthAfrica">
										<a href="https://www.facebook.com/ClicksSouthAfrica">Clicks</a>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>	
	</c:if>	

	<c:if test="${cmsPage.uid eq 'healthyLivingPage'}">
		<cms:pageSlot position="NavigationBar1" var="feature">
			<div class="tab-content main-nav tab-pane fade active in"
				id="healthhub">
				<div class="container">
					<cms:component component="${feature}" />
				</div>
			</div>
		</cms:pageSlot>
		<cms:pageSlot position="Para1" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</c:if>
	
	<section class="main-container bg-gray clearfix hLandingPage"
		style="position: relative;">
		<div class="container">
			<div class="row">
				<div class="commonTab healthTab">
					<ul class="tabs">
					<%-- 	<li class="firstLi"><spring:theme code="magazine.latest.artciles" text="Latest Articles" /></li> --%>
						<li class="active"><a
							href="${contextPath}/clubcard-magazine/?id=all&pageType=${cmsPage.uid}">All</a></li>
						<c:if test="${cmsPage.uid eq 'magazineLandingPage'}">	
							<c:forEach var="articleTag" items="${articleTags}">
								<c:if test="${articleTag.isPrioritze}">
									<li <c:if test="${tagId eq articleTag.tagId}">class="active"</c:if>><a	href="${contextPath}/clubcard-magazine?id=${articleTag.tagId}&pageType=${cmsPage.uid}">${articleTag.tagName}</a></li>
								</c:if>
							</c:forEach>
						</c:if>
						<c:if test="${cmsPage.uid eq 'healthyLivingPage'}">	
							<c:forEach var="articleTag" items="${articleTags}">
								<c:if test="${articleTag.isHealthyLiving}">
									<li <c:if test="${tagId eq articleTag.tagId}">class="active"</c:if>><a	href="${contextPath}/clubcard-magazine?id=${articleTag.tagId}&pageType=${cmsPage.uid}">${articleTag.tagName}</a></li>
								</c:if>
							</c:forEach>
					</c:if>
					</ul>
					<div class="border-line"></div>
					<c:choose>
						<c:when test="${articles.size() > 0}">
							<c:if test="${not empty articles}">
								<input id="totalNoOfArticles" type="hidden"
									value="${totalNoOfArticles}">
								<input id="activeTab" type="hidden" value="${tagId}">
								<input id="pageType" type="hidden" value="${cmsPage.uid}">
								<input id="maxNoOfArticlesShown" type="hidden"
									value="${maxNoOfArticlesShown}">
								<input id="contextPath" type="hidden" value="${contextPath}">
								<div class="tabContentWrapper healthTabWrapper tb-res">
									<div class="active" id="tab1">
										<div class="row healthTabOuter">
											<div class="col-lg-12">
												<div class="active-tab-outer">
												<c:choose>
												<c:when test="${cmsPage.uid=='healthyLivingPage'}">
												<c:set var="endCount" value="${maxNoOfArticlesShown-2}"/>
												</c:when>
												<c:otherwise>
												<c:set var="endCount" value="${maxNoOfArticlesShown-1}"/>
												</c:otherwise>
												</c:choose>
													<c:forEach var="article" items="${articles}" varStatus="status" begin="0" end="${endCount}">
												
														<c:if test="${status.count==6 and cmsPage.uid=='healthyLivingPage'}">
														<c:set var="hideDiv" value="false" />
														<cms:pageSlot position="GoogleAd1" var="feature">
															<c:if test="${hideDiv eq 'false'}">
																<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 appendLoadArticles healthGoogleADs">
																<c:set var="hideDiv" value="true" />
															</c:if>	
																	<cms:component component="${feature}" />
																</cms:pageSlot>
															<c:if test="${hideDiv eq 'true'}"> 
																</div>	
															</c:if>															
														</c:if>
														
														<c:if test="${status.count==11 and cmsPage.uid=='healthyLivingPage'}">
														<c:set var="hideDiv2" value="false" />
														<cms:pageSlot position="GoogleAd2" var="feature">
															<c:if test="${hideDiv2 eq 'false'}">
																<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 appendLoadArticles healthGoogleADs">
																<c:set var="hideDiv2" value="true" />
															</c:if>	
																	<cms:component component="${feature}" />
																</cms:pageSlot>
															<c:if test="${hideDiv2 eq 'true'}"> 
																</div>	
															</c:if>												
														</c:if>
														
														<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 appendLoadArticles">
															<div class="tabThumbWrapper">
																<div class="tabThumbImage">
																<c:choose>
																<c:when test="${not empty article.sideImage1.url}">
																<img src="${article.sideImage1.url}"
																		alt="${article.sideImage1.altText}">
																</c:when>
																<c:otherwise>
																<img src="${themeResourcePath}/clicks/image/missing-plp.jpg"/>
																</c:otherwise>
																</c:choose>				
																	<div class="TagWrapper">
																		<c:forEach items="${article.articleTags}"
																			var="articleTag">
																			<span class="TagOuter">${articleTag.tagName}</span>
																		</c:forEach>
																	</div>
																</div>
																<c:if test="${article.articleType.code=='MAGAZINES'}">
																	<h4>
																		<a href="${contextPath}/clubcard-magazine/article-view/${article.articleTitle}">${article.titleName}</a>
																	</h4>
																</c:if>
																<c:if test="${article.articleType.code=='HEALTH'}">
																	<h4>
																		<a href="${contextPath}/health/article-view/${article.articleTitle}">${article.titleName}</a>
																	</h4>
																</c:if>
																<c:if test="${article.articleType.code=='CONDITIONS'}">
																	<h4>
																		<a href="${contextPath}/health/conditions/article-view/${article.articleTitle}">${article.titleName}</a>
																	</h4>
																</c:if>
																<c:if test="${article.articleType.code=='MEDICINES'}">
																	<h4>
																		<a href="${contextPath}/health/medicines/article-view/${article.articleTitle}">${article.titleName}</a>
																	</h4>
																</c:if>
																	<c:if test="${article.articleType.code=='VITAMINS'}">
																	<h4>
																		<a href="${contextPath}/health/supplements/article-view/${article.articleTitle}">${article.titleName}</a>
																	</h4>
																</c:if>

																<p>${article.titleContent}</p>
															</div>
														</div>													
													</c:forEach>
												</div>
											</div>
										</div>
										<div class="text-center  load-more-articles-ajax">
										<input type="hidden" value="${param.pageType}" id="pageTypeVal"/>
											<button class="btn btnBig">Load more articles</button>
										</div>
									</div>
								</div>
							</c:if>
						</c:when>
						<c:otherwise>No Records found.</c:otherwise>
					</c:choose>
				</div>
			</div>
								
			<c:if test="${cmsPage.uid eq 'magazineLandingPage'}">
				<div class="row">
					<div class="clearfix bg-light-blue">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
							<div class="pull-left">
								<cms:pageSlot position="Section2" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
						</div>
					</div>

					<div class="clearfix bg-white magazineWrapper">
						<div class="row">						
							<cms:pageSlot position="Section3" var="feature">
								<div class="col-md-4 col-sm-12 col-xs-12 ">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
			</c:if>
		</div>
		
		<div id="menu-overlay"
			style="position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; display: none;">
		</div>
		
	</section>

</template:page>
