<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<template:page pageTitle="${pageTitle}">

	<section style="position: relative;" class="main-container">
		<div class="search-results">
			<div class="container">
				<c:if test="${not empty message}">
					<spring:theme code="${message}" />
				</c:if>
				<div id="globalMessages">
					<common:globalMessages />
				</div>
				<div class="row title-bar">
					<spring:theme code="search.results" var="searchResults" />
					<h2 class="main-title col-md-12 col-xs-12">${searchResults}</h2>
				</div>
			</div>	
				
				
				
				<div class="contentWrapper bg-gray">
					<div class="container">
					
					
					
					<div class="searchErr clearfix">
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<cms:pageSlot position="SideContent" var="feature" element="div"
							class="span-4 side-content-slot cms_disp-img_slot">
							<cms:component component="${feature}" />
						</cms:pageSlot>


						<div class="item_container_holder">
							<h2>
								<spring:theme code="search.no.results" text="No Results Found" />
							</h2>
						</div>
						<cms:pageSlot position="MiddleContent" var="comp" element="div"
								class="item_container">
								<cms:component component="${comp}" />
							</cms:pageSlot>
							<div class="item_container">
								<nav:searchSpellingSuggestion
									spellingSuggestion="${searchPageData.spellingSuggestion}" />
							</div>
						
	
						<cms:pageSlot position="BottomContent" var="comp" element="div"
							class="span-20 cms_disp-img_slot right last">
							<cms:component component="${comp}" />
						</cms:pageSlot>
						</div>
						
						</div>
						</div>			
					</div>
				</div>
	</section>
</template:page>
