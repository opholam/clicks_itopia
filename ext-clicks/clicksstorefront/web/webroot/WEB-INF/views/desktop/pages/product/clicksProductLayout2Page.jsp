<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<template:page pageTitle="${pageTitle}">

	<!-- <div id="menu-overlay"></div> -->
	<section class="main-container bg-gray pdp">
		<div class="container">

			<c:if test="${not empty message}">
				<spring:theme code="${message}" />
			</c:if>

			<div id="globalMessages">
				<common:globalMessages />
			</div>

			<div class="bg-white clearfix headLogo">
				<c:forEach items="${product.categories}" var="category">

					<c:if test="${fn:contains(category.name,'Stuff') }">
						<img src="${category.image.url}" />
						<c:set var="brandName" value="${category.name}" />
					</c:if>

				</c:forEach>


			</div>


			<cms:pageSlot position="Section1" var="comp" element="div"
				class="span-24 section1 cms_disp-img_slot">
				<cms:component component="${comp}" />
			</cms:pageSlot>

			<product:productDetailsPanel product="${product}"
				galleryImages="${galleryImages}" brandName="${brandName}" />
				
		<%-- 	<div class="colorWrapper clearfix">
				<span class="colorCount">Colour</span>
						<cms:pageSlot position="VariantSelector" var="feature">
								<cms:component component="${feature}" />
						</cms:pageSlot>
			</div> --%>
			
			<cms:pageSlot position="Section3" var="feature" element="div"
				class="span-20 section3 cms_disp-img_slot">
				<cms:component component="${feature}" />
			</cms:pageSlot>


			<div class="bg-white clearfix pdp-second-content">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">


					<product:productPageTabs />

					<div id="review-pdp" class="reviewsOuter">
						<product:productPageReviewsTab product="${product}" />
					</div>


				</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<aside>
				<c:if test="${productListForPromotions.size() > 0 && not empty promoCodeForPromotions}">
					<h3 class="text-center">
						<a href="${contextPath}/c/OH1?q=%3Arelevance%3ApromoStickerplp%3A${promoCodeForPromotions}">Recommended products on promotion</a>
					</h3>
				
					<!-- <div class="title">${component.title}</div>   -->
					<div class="pdp-artslider">
<%-- 				<cms:pageSlot position="CrossSelling" var="comp" element="div"
					class="span-24">
					<cms:component component="${comp}" />
				</cms:pageSlot> --%>
					<div class="bg-white">
					<ul class="clearfix article-slider">
						<c:forEach items="${productListForPromotions}" var="product">

							<c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
							<li>
							
							<!-- <div class="col-lg-12 col-md-4 col-sm-12 col-xs-12"> -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
								<div class="productBlock">
									<div class="badges">
										<c:if test="${not empty product.potentialPromotions}">
												<c:forEach items="${product.potentialPromotions}" var="promotions">
													<c:if test="${not empty promotions.stickerMediaUrls}">
														<c:forEach items="${promotions.stickerMediaUrls}" var="stickerURL">
																<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
														</c:forEach>
													</c:if>
												</c:forEach>
										</c:if>
										
										<c:if test="${not empty product.nonPromoStickerUrls}">
											<c:forEach items="${product.nonPromoStickerUrls}" var="stickerURL">
												<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
											</c:forEach>
									</c:if>
									</div>
									
																	
									<div class="thumb">
									<%-- <a href="${contextPath}${product.url}"> --%>
									<%-- <c:forEach items="${product.images}" var="productReferenceImage" varStatus="loop"> --%>
									<product:productGalleryProductList
										productData="${product}"/>
									<%-- </c:forEach> --%>
									<%-- <product:productPrimaryImage product="${product}" format="product"/> --%>
									<!-- </a> -->
									</div>
									<div class="detailContent clearfix">
										<c:if test="${not empty product.brand}">
										<h5>${product.brand}</h5>
										</c:if>
										<p><a href="${contextPath}${product.url}">${product.name}</a></p>	
										<c:if test="${not empty product.summary}">
										<div class="content-height">
										<p>${product.summary}</p>
										</div>
										</c:if>
										<c:if test="${not empty product.price}">
										<format:fromPrice product="${product}"/>
										</c:if> 
												<%-- <c:if test="${not empty product.price.grossPriceValueWithPromotionAppliedFormattedValue}">
												<div class="offer-blk">
													 Was&nbsp;<c:set var="dec"
														value="${fn:split(product.price.formattedValue, '.')}" />
														${dec[0]}<sup>${dec[1]}</sup>
													<span class="red">Save</span>
													<span class="price red"> <fmt:formatNumber
															var="save" pattern="####.##"
															value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
														<c:set var="dec1" value="${fn:split(save, '.')}" />
														R${dec1[0]}<sup>${dec1[1]}</sup>															</span>

												</div>
												</c:if> --%>
											
										<div class="starWrapper">
											<product:productStars rating="${product.averageRating}" />
											<div class="buy-btn-wrap"> 
												<%--	<a href="${contextPath}${product.url}" class="btn">See more</a> --%>
												<c:if test="${not empty product.price}">
												<product:productListAddToCartPanel product="${product}" allowAddToCart="${empty showAddToCart ? true : showAddToCart}" isMain="true" />
												</c:if>
													</div>
										</div>
									</div>
										<c:if test="${ not empty product.remarks}">
											<div class="quoteBlock">
												<div class="quoteBlockInner">
													<p>"${product.remarks}"</p>
												</div>
											</div>
										</c:if>
								</div>
							</div>
						</li>
						</c:forEach>
					</ul>
					</div>
					</div>
					</c:if>
				</aside>
			</div>
			</div>
		</div>
		<!-- <div id="div-overlay"></div> -->
					<cms:pageSlot position="UpSelling" var="comp" element="div"
						class="span-24">
						<cms:component component="${comp}" />
					</cms:pageSlot>
	</section>


</template:page>
