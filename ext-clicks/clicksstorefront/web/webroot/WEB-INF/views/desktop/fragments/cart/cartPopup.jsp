<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>

<spring:theme code="text.addToCart" var="addToCartText" />
<spring:theme code="text.popupCartTitle" var="popupCartTitleText" />
<c:url value="/cart" var="cartUrl" />
<c:url value="/cart/checkout" var="checkoutUrl" />
<c:if test="${numberShowing > 0 }">
	<div class="mini_basket_head">
		<h3>${component.title}</h3>
		<c:if test="${numberItemsInCart > numberShowing}">

			<a href="${cartUrl}" class="pull-right"> See all
				${numberItemsInCart} products </a>
		</c:if>
	</div>

</c:if>

<c:if test="${empty numberItemsInCart or numberItemsInCart eq 0}">
	<div class="mini_basket_head">
		<h3>
			<spring:theme code="popup.minicart.empty" text="Your cart is empty" />
		</h3>
	</div>
</c:if>
<c:if test="${numberShowing > 0 }">
	<div class="basket-list-inner">
		<ul class="mini-basket-product-list">
			<c:forEach items="${entries}" var="entry" end="${numberShowing - 1}">
				<c:url value="${entry.product.url}" var="entryProductUrl" />
				<li class="popupCartItem">
					<div class="basket-list-img">
						<a href="${entryProductUrl}"> <product:productPrimaryImage
								product="${entry.product}" format="cartIcon" />
						</a>
					</div>
					<div class="basket-list-content">
						<h4>
							<a class="itemName" href="${entryProductUrl}">${entry.product.brand}</a>
						</h4>

						<p>
							<a href="${entryProductUrl}"> ${entry.product.name} </a>
						</p>

						<%--<c:forEach items="${entry.product.baseOptions}"
									var="baseOptions">
									<c:forEach
										items="${baseOptions.selected.variantOptionQualifiers}"
										var="baseOptionQualifier">
										<c:if
											test="${baseOptionQualifier.qualifier eq 'style' and not empty baseOptionQualifier.image.url}">
											<div class="itemColor">
												<span class="label"><spring:theme
														code="product.variants.colour" /></span> <img
													src="${baseOptionQualifier.image.url}"
													alt="${baseOptionQualifier.value}"
													title="${baseOptionQualifier.value}" />
											</div>
										</c:if>
									</c:forEach> 
								</c:forEach>  --%>
						<c:if test="${not empty entry.deliveryPointOfService.name}">
							<div class="itemPickup">
								<span class="itemPickupLabel"><spring:theme
										code="popup.cart.pickup" /></span>${entry.deliveryPointOfService.name}
							</div>
						</c:if>
						<small class="basket_price"> 
						${entry.quantity} x  <price:fromPrice product="${entry.product}" />
					<%-- 	<c:choose>
								<c:when
									test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
									<c:choose>
										<c:when test="${not empty entry.totalPrice}">
												${entry.quantity} x <format:discountedPrice
												priceData="${entry.totalPrice}" displayFreeForZero="true" />
										</c:when>
										<c:otherwise>
												${entry.quantity} x <format:discountedPrice
												priceData="${entry.basePrice}" displayFreeForZero="true" />
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${not empty entry.totalPrice}">
											${entry.quantity} x <format:discountedPrice
												priceData="${entry.totalPrice}" displayFreeForZero="true" />
										</c:when>
										<c:otherwise>
											${entry.quantity} x <format:discountedPrice
												priceData="${entry.basePrice}" displayFreeForZero="true" />
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose> --%> 
							<c:forEach items="${entry.potentialPromoDescription}" var="promotion">
							<ul class="potentialPromotions">
							<li>
												<div class="prod_notify tick">
												<a href="${productUrl}">
													<ycommerce:testId code="cart_potentialPromotion_label">
													Missed Savings! <br />
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion,'productName',entry.product.name)}"></c:set> 
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion}
												</c:otherwise>
												</c:choose>
												</ycommerce:testId>
												</a>
												</div>
											</li>
											</ul>
											</c:forEach>
							<c:forEach items="${entry.appliedPromoDescription}" var="promotion">
						<ul class="appliedPromotions">
						<li>
												<div class="prod_notify tick">
													<ycommerce:testId code="cart_appliedPromotion_label">
												
													<c:choose>
												<c:when test="${fn:containsIgnoreCase(promotion, 'productName')}">
												<c:set var="name" value="${entry.product.name}"/>
												<c:set var="promotionMessage" value="${fn:replace(promotion,'productName',entry.product.name)}"></c:set> 
												${promotionMessage}
												</c:when>
												<c:otherwise>
												${promotion}
												</c:otherwise>
												</c:choose>
												
												</ycommerce:testId>
												</div>
											</li>
											</ul>
						</c:forEach>
						</small>
						<c:url value="/cart/remove" var="cartUpdateFormAction" />
						<form:form id="updateCartForm${entry.entryNumber}"
							action="${cartUpdateFormAction}" method="post"
							commandName="updateQuantityForm${entry.entryNumber}">
							<input type="hidden" name="entryNumber"
								value="${entry.entryNumber}" />
							<input type="hidden" name="productCode"
								value="${entry.product.code}" />
							<input type="hidden" name="initialQuantity"
								value="${entry.quantity}" />
							<form:input disabled="${not entry.updateable}" type="hidden"
								size="1" id="quantity${entry.entryNumber}"
								class="form-control quantity" path="quantity" />
							<c:if test="${entry.updateable}">
								<ycommerce:testId code="cart_product_removeProduct">
									<spring:theme code="text.iconCartRemove" var="iconCartRemove" />
									<a href="#" id="RemoveProduct_${entry.entryNumber}"
										class="submitRemoveProduct delete_prod">Remove</a>
								</ycommerce:testId>
							</c:if>
						</form:form>
					</div>
				</li>
			</c:forEach>
		</ul>
	</div>

	<section class="basket-list-total clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="basket-list-total-content">
				Subtotal (excluding delivery):
				<c:choose>
					<c:when
						test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
						<format:discountedPrice priceData="${cartData.subTotal}" />
					</c:when>
					<c:otherwise>
						<format:price priceData="${cartData.subTotal}" />
					</c:otherwise>
				</c:choose>
				<c:if
					test="${not empty cartData.totalDiscounts.formattedValue and cartData.totalDiscounts.formattedValue ne 'R0.00'}">
					<div class="totalsaving red">
						(You have saved
						<format:discountedPrice priceData="${cartData.totalDiscounts}" />
						)
					</div>
				</c:if>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<button class="positive btn primary_btn" type="button"
				onclick="window.location.href='${cartUrl}'">
				<spring:theme code="mini.checkout" text="Check out" />
			</button>
		</div>
	</section>
</c:if>

