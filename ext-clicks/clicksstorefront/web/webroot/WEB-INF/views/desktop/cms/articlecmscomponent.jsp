<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:if test="${not empty errorMsg}">
   	${errorMsg}
</c:if>
<c:if test="${empty errorMsg}">

	<c:set var="article" value="${articleData}" />

	<%-- ${article.articleCode} --%>

	<div class="articleTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<h1 class="main-title">${article.titleName}</h1>
					<p>${article.titleContent}</p>
					<div class="authorWrapper">
						<div class="authorPic">
							<img src="../image/article/article-logo.png" alt="" />
						</div>
						<div class="authorInfoWrapper">
							<fmt:formatDate type="date" value="${article.contentDate}" />
							<div class="authorInfo">
								by the <a href="#">South African Heart Foundation</a>
							</div>
						</div>
					</div>
					<div class="moreLinks">
						<a class="share" href="javascript:void(0)">Share</a> <a
							class="email" href="javascript:void(0)" onclick="javascript:window.location='mailto:?subject=${article.titleName}&body=' + window.location;">Email</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="image-center">
						<img title="Title" alt="${article.titleIcon.altText}"
							src="${article.titleIcon.url}">
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="main-container bg-gray clearfix articlePage">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="bg-white contentContainer">
						${article.mainContent}</div>
					<c:if test="${articleTypeCode == 'medicines'}">
						<a class="heart arrow-right" href="#">Read more from our Heart
							disease information centre</a>
					</c:if>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<aside>
						<c:if test="${articleTypeCode == 'medicines'}">
							<a class="stethoscope arrow-right" href="#">Check your
								symptoms</a>
						</c:if>
						<div class="image-center">${article.advertisement}</div>
						<c:if test="${articleTypeCode == 'medicines'}">
							<h1 class="main-title">In Healthy Living</h1>
						</c:if>
						<c:if test="${articleTypeCode == 'magazines'}">
							<h2 class="bg-blue">Read more</h2>
						</c:if>
						<div class="bg-white">
							<div class="articleSidebar clearfix">
								<div class="articleSideWrapper clearfix">
									<div class="image-center">
										<img title="Side Image 1" alt="${article.sideIcon1.altText}"
											src="${article.sideIcon1.url}">
									</div>
									${article.sideContent1}
								</div>
								<div class="separator"></div>
								<div class="articleSideWrapper clearfix">
									<div class="image-center">
										<img title="Side Image 1" alt="${article.sideIcon2.altText}"
											src="${article.sideIcon2.url}">
									</div>
									${article.sideContent2}
								</div>
							</div>
						</div>
					</aside>
				</div>

			</div>
		</div>
	</section>
</c:if>