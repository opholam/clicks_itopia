<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:set value="${component.styleClass} ${dropDownLayout}"
	var="bannerClasses" />

<li
	class="La ${bannerClasses} <c:if test="${not empty component.navigationNode.children}"> parent</c:if>">
	<cms:component component="${component.link}" class="nav-link"
		evaluateRestriction="true" /> <c:if
		test="${not empty component.navigationNode.children}">
		<section>
			<ul class="inner-menu">
				<c:forEach items="${component.navigationNode.children}" var="child">
					<c:if test="${child.visible}">
						<li class="col-md-3">
							<h3 class="hidden-xs hidden-sm">${child.title}</h3>
							<h3 class="accordion-toggle visible-xs visible-sm">${child.title}</h3>
							<c:forEach items="${child.links}" step="${component.wrapAfter}"
								varStatus="i">
								<c:if test="${not empty child.links}">
								<ul class="menu-list-links">
									<c:forEach items="${child.links}" var="childlink"
										begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
										<cms:component component="${childlink}"
											evaluateRestriction="true" element="li" />
									</c:forEach>
								</ul>
								</c:if>
							</c:forEach> 
							<c:if test="${not empty child.children}">
								<c:forEach items="${child.children}" var="subChild">
									<c:if test="${not empty subChild.title}">
									<ul class="menu-list-links">
										<li>										
											<h3 class="hidden-xs hidden-sm">${subChild.title}</h3>
											<h3 class="accordion-toggle visible-xs visible-sm">${subChild.title}</h3>									
											<c:if test="${not empty subChild.links}">
											<ul class="menu-list-links">
												<c:forEach items="${subChild.links}" var="subChildlink"
													begin="${i.index}"
													end="${i.index + component.wrapAfter - 1}">
													<cms:component component="${subChildlink}"
														evaluateRestriction="true" element="li" />
												</c:forEach>
											</ul>
											</c:if>
										</li>
									</ul>
									</c:if>
								</c:forEach>
							</c:if>

						</li>
					</c:if>
				</c:forEach>
			</ul>
		</section>
	</c:if>
</li>

<%-- <c:set value="${component.styleClass} ${dropDownLayout}" var="bannerClasses"/>

<li class="La ${bannerClasses} <c:if test="${not empty component.navigationNode.children}"> parent</c:if>">
<cms:component component="${component.link}" class="nav-link" evaluateRestriction="true" />
<c:if test="${not empty component.navigationNode.children}">
	<section>
		<ul class="inner-menu">
				<c:forEach items="${component.navigationNode.children}" var="child">
					<c:if test="${child.visible}">
						<li class="menu-list-links col-md-3">
							<h3 class="hidden-xs hidden-sm">${child.title}</h3>
							<c:forEach items="${child.links}" step="${component.wrapAfter}" varStatus="i">
								<ul class="Lc ${i.count < 2 ? 'left_col' : 'right_col'}"   >
								<c:forEach items="${child.links}" var="childlink" begin="${i.index}" end="${i.index + component.wrapAfter - 1}" >
									<cms:component component="${childlink}" evaluateRestriction="true" element="li" class="Lc ${i.count < 2 ? 'left_col' : 'right_col'}"  />
								</c:forEach>
								</ul>
							</c:forEach>
						</li>
					</c:if>
				</c:forEach>
		</ul>
	</section>
</c:if>

</li>
 --%>
<%-- <li class="La ${bannerClasses} <c:if test="${not empty component.navigationNode.children}"> parent</c:if>">
		
	<cms:component component="${component.link}" evaluateRestriction="true"/> 
	<c:if test="${not empty component.navigationNode.children}">
		<ul class="Lb">
			<c:forEach items="${component.navigationNode.children}" var="child">
				<c:if test="${child.visible}">
					<li class="Lb">
						<span class="nav-submenu-title">${child.title}</span>
						<c:forEach items="${child.links}" step="${component.wrapAfter}" varStatus="i">
							<ul class="Lc ${i.count < 2 ? 'left_col' : 'right_col'}">
								<c:forEach items="${child.links}" var="childlink" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
									<cms:component component="${childlink}" evaluateRestriction="true" element="li" class="Lc ${i.count < 2 ? 'left_col' : 'right_col'}"/>
								</c:forEach>
							</ul>
						</c:forEach>
						
						
						
					</li>
				</c:if>
			</c:forEach>
		</ul>
	</c:if>
</li> --%>

<%-- 
<li
	class="La ${bannerClasses} <c:if test="${not empty component.navigationNode.children}"> parent</c:if>">

	<cms:component component="${component.link}" evaluateRestriction="true" />
	<c:if test="${not empty component.navigationNode.children}">

		<section>
			<div class="inner-menu">
				<div class="row">
					<c:forEach items="${component.navigationNode.children}" var="child">
						<c:if test="${child.links.size()>0}">
						
							
							<c:if test="${not (fn:containsIgnoreCase(child.title, 'ethnicity') or fn:containsIgnoreCase(child.title, 'clubcard exclusive savings')) }"> 
							<ul class="menu-list-links col-md-3">
							 </c:if>
							
							
							<c:if test="${child.visible}">
									
									<c:if test="${fn:containsIgnoreCase(child.title, 'ethnicity') or fn:containsIgnoreCase(child.title, 'clubcard exclusive savings')}"> 
									<ul> 
									</c:if>
									
									<li><h3>${child.title}</h3></li>
									<c:forEach items="${child.links}" step="${component.wrapAfter}"
										varStatus="i">
										<c:forEach items="${child.links}" var="childlink"
											begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
										<li>	<cms:component component="${childlink}"
												evaluateRestriction="true" /></li>
												 
										</c:forEach>
									</c:forEach>
									
									<c:if test="${fn:containsIgnoreCase(child.title, 'ethnicity') }">
									 </ul> 
									 </c:if>
									
								</c:if>
								
								<c:if test="${not fn:containsIgnoreCase(child.title, 'on promotion')}"> 
								<c:if test="${not fn:containsIgnoreCase(child.title, 'shaving')}"> 
								<c:if test="${not fn:containsIgnoreCase(child.title, 'shop beauty')}"> 
								 </ul>
								 </c:if>
								</c:if>
								</c:if>
								
						</c:if>
					</c:forEach>
					
					
					<c:if test="${component.medialist.size()>0}">
					
					<c:forEach items="${component.medialist}" var="media" varStatus="i">
					  	 <c:if test="${i.count==1 || i.count==3}"> <ul class="menu-list-links col-md-3"></c:if>
									<li>
									<h3>${child.altText}</h3>
									<img title="${media.altText}" alt="${media.altText}" src="${media.url}" usemap="#map"/>
									</li>
						<c:if test="${i.count==2 || i.count==4 || i.last}"> </ul> </c:if>
					</c:forEach>
					
					</c:if>
				</div>
			</div>
		</section>
	</c:if>
</li>

--%>
