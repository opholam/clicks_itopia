<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<ul class="bxslider-superSection">
<c:forEach items="${component.sliderMedia}" var="media" varStatus="i">		
	 <li><div class="thumb-img">
	 <c:if test="${not empty sliderUrls[i.count-1]}">
	 	<c:url var="url" value="${sliderUrls[i.count-1]}"/>
	 </c:if> 
      <a href="${url}"><img title="${media.altText}" src="${media.url}" alt="${media.altText}" /></a>
    </div>
   </li> 
</c:forEach>
</ul>
<div id="pagerSS-superSection">
 <div id="bx-pager-superSection">
  <c:forEach items="${component.sliderContent}" var="component" varStatus="i">
	   <div class="details-block">
	   		<cms:component component="${component}" evaluateRestriction="true" />
	   </div>  
  </c:forEach>
 </div>
</div>

 <%--  <h4><a href="${encodedUrl}" tabindex="-1">${title }</a></h4>
    <p>${summary }</p> --%>