<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url value="${linkUrl}" var="encodedUrl" />

<c:if test="${component.sliderEnable}">
	<li>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-zero">
		<c:choose>
		<c:when test="${not empty component.googleTracking}">
			<%-- Creating session scopre for GA and removing in landingLaout1page.jsp --%>
			 <c:set var="hpGATracking" value="${component.googleTracking}" scope="session" /> 
			<c:set var="hpCount" value="${hpCount+1}" scope="session" />
										<input id="gaallow_${hpCount}" type="hidden" value="${hpGATracking}" />
										<input id="gapage_${hpCount}" type="hidden" value="${hpGATracking.page}" />
										<input id="gaplacementcode_${hpCount}" type="hidden" value="${hpGATracking.placementCode}" />
										<input id="gabrand_${hpCount}" type="hidden" value="${hpGATracking.brand}" />
										<input id="gacosttype_${hpCount}" type="hidden" value="${hpGATracking.costType}" />
										<input id="gaduration_${hpCount}" type="hidden" value="${hpGATracking.duration}" />
										<input id="gacreative_${hpCount}" type="hidden" value="${hpGATracking.creative}" />
										<input id="gaskuid_${hpCount}" type="hidden" value="${hpGATracking.skuId}" />
										
			<a href="${encodedUrl}" tabindex="-1" onclick="onPromoClick('${googleTracking.page}','${googleTracking.placementCode}','${googleTracking.brand}','${googleTracking.costType}','${googleTracking.duration}','${googleTracking.creative}','${googleTracking.skuId}','${encodedUrl}'); return !ga.loaded;"><img title="${media.altText}" src="${media.url}" alt="${media.altText}" />
			<div class="health-promo-text">
				<h4>${title }</h4>
				<div class="prod-description">${summary}</div>
			</div>
			</a>
		</c:when>
		<c:otherwise>
			<a href="${encodedUrl}" tabindex="-1"><img title="${media.altText}" src="${media.url}" alt="${media.altText}" />
			<div class="health-promo-text">
				<h4>${title }</h4>
				<div class="prod-description">${summary}</div>
			</div>
			</a>
		</c:otherwise>
		</c:choose>
		</div>
	</li>
</c:if>

<c:if test="${!component.sliderEnable}">
	<div class="thumb-img">
		<a href="${encodedUrl}" tabindex="-1"><img title="${media.altText}" src="${media.url}" alt="${media.altText}" /></a>
	</div>
	<div class="details-block">
		<h4><a href="${encodedUrl}" tabindex="-1">${title }</a></h4>
		<p>${summary }</p>
	</div>
</c:if>


