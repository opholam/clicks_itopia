<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>


 <c:choose>
	 <c:when test="${component.uid eq 'HealthNavBarComponent'}">
	 <div class="tab-content main-nav mobile-tabs" id="healthhub">
		<div class="container  mob-container">
				<nav>
					<ul class="nav-list">
						<c:if test="${component.visible}">
								<c:forEach items="${components}" var="component" varStatus="i">
								<c:if test="${i.count<9}">
									<c:if test="${component.navigationNode.visible}">
										<cms:component component="${component}" evaluateRestriction="true" />   
									</c:if>
								</c:if>
								</c:forEach>
							</c:if>
					</ul>
					</nav>
			
			</div>
		</div>
	 </c:when>	 
	 <c:otherwise> 
		<div class="tab-content main-nav mobile-tabs" id="shop">
			<div class="container  mob-container">		
					<nav>
						<ul class="nav-list">
							<c:if test="${component.visible}">
									<c:forEach items="${components}" var="component" varStatus="i">
									<c:if test="${i.count<9}">
										<c:if test="${component.navigationNode.visible}">
											<cms:component component="${component}" evaluateRestriction="true" />   
										</c:if>
									</c:if>
									</c:forEach>
								</c:if>
						</ul>
						<ul class="nav-list offers">
							<c:if test="${component.visible}">
								<c:forEach items="${components}" var="component" varStatus="i">
								<c:if test="${i.count>8}">
									<c:if test="${component.navigationNode.visible}">
									
									<cms:component component="${component}" evaluateRestriction="true"/>
									</c:if>
								</c:if>
								</c:forEach>
								
							</c:if>	
						</ul>
					</nav>	
				</div>
			</div>	
	 </c:otherwise>
</c:choose>

<%-- <c:if test="${component.visible}">
	<div class="navigationbarcollectioncomponent container">
		<ul class="clear_fix">
			<c:forEach items="${components}" var="component">
				<c:if test="${component.navigationNode.visible}">
						<cms:component component="${component}" evaluateRestriction="true"/>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</c:if>

 --%>


<%-- 

<div class="tab-content main-nav hidden-xs">
	<div class="container">
		<div id="shop" class="tab-pane fade active in">
			<nav>
				<ul class="nav-list">
					<li><c:if test="${component.visible}">
							<c:forEach items="${components}" var="component" varStatus="i">
							<c:if test="${i.count<9}">
								<c:if test="${component.navigationNode.visible}">
									<cms:component component="${component}"
										evaluateRestriction="true" />   
								</c:if>
							</c:if>
							</c:forEach>
						</c:if>
					</li>
				</ul>
				
				<ul class="nav-list offers">
							<c:if test="${component.visible}">
								<c:forEach items="${components}" var="component" varStatus="i">
								<c:if test="${i.count>8}">
									<c:if test="${component.navigationNode.visible}">
									<cms:component component="${component}"
											evaluateRestriction="true" /> 
									</c:if>
								</c:if>
								</c:forEach>
								
							</c:if>
							
				</ul>
			</nav>
		</div>
	</div>
</div>
--%>

<%-- 
<div class="tab-content main-nav">
			<div class="container">
			<div id="shop" class="tab-pane fade active in">
				<nav>
					<ul class="nav-list">
						<li>
	
			<c:forEach items="${components}" var="component">
				<c:if test="${component.navigationNode.visible}">
						<cms:component component="${component}" evaluateRestriction="true"/>
				</c:if>
			</c:forEach>
		</li></ul>
		</nav>
	</div>
	</div>
	</div> --%>
