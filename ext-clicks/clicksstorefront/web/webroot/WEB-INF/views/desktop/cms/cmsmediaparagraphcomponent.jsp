<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:url value="${mediaLink.url}" var="encodedUrl" />

<c:choose>
	<c:when test="${cmsPage.uid eq 'healthpage' and !fn:containsIgnoreCase(contentSlot.currentPosition,'Footer')}">
		<div class="magazineImg ">
			<img src="${media.url}" alt="${media.altText}">
		</div>
		<div class="magazineContent">
			<c:if test="${not empty component.title}">
				<h4>
					<a href="${mediaLink.url}">${component.title}</a>
				</h4>
			</c:if>
			<c:if test="${not empty content}"> <h4>${content}</h4> </c:if>
			
			<c:if test="${not empty mediaLink}">
				<a href="${encodedUrl}">${mediaLink.linkName}</a>
			</c:if>
		</div>
	</c:when>
	<c:when test="${cmsPage.uid eq 'helpingHandClinic' || cmsPage.uid eq 'clinicBooking'}">
		<img src="${media.url}" alt="${media.altText}">
		<div class="unite_vital"> 
			<h3><a href="${mediaLink.url}">${component.title}</a></h3>
			<p>${content}</p>
		</div>
	</c:when>
	<c:when test="${cmsPage.uid eq 'cartPage' and !fn:containsIgnoreCase(contentSlot.currentPosition,'Footer')}">
		<img src="${media.url}" alt="${media.altText}">
		<span>${component.title}</span>
	</c:when>
	<c:when test="${cmsPage.uid eq 'pharmacyClinicServicesPages'}">
		<h3><a href="${encodedUrl}">${mediaLink.linkName}</a></h3>
		<div class="unite_vital"> 
			<img src="${media.url}" alt="${media.altText}">	
			<p>${content}</p>
		</div>
	</c:when>
	<c:when test="${cmsPage.uid eq 'clubCardPage'}">
			<c:if test="${media ne null}">
			<div class="magazineImg ">
				<img src="${media.url}" alt="${media.altText}">
			</div>
			</c:if>
			<div class="magazineContent">
				<h3 align="center">${component.title}</h3>
				${content}
			</div>	
	</c:when>
	<c:otherwise>	
			<div class="magazineImg ">
				<img src="${media.url}" alt="${media.altText}">
			</div>
			<div class="magazineContent">
				<h3>${component.title}</h3>
				${content}
				<c:if test="${not empty mediaLink}">
					<a href="${mediaLink.url}">${mediaLink.linkName}</a>
				</c:if>
			</div>	
	</c:otherwise>
</c:choose>

<%-- 
<div class="col-md-4 col-sm-12 col-xs-12  clubMagindex">
	<div class="magazineImg ">
		<img src="${media.url}" alt="${media.altText}">
	</div>
	<div class="magazineContent">
		<h3>${component.title}</h3>
		${content}
		<c:if test="${not empty mediaLink}">
			<a href="${mediaLink.url}">${mediaLink.linkName}</a>
		</c:if>
	</div>
</div> --%>