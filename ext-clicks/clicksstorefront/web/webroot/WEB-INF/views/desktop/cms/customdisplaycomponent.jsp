<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url var="emcodedUrl" value="${linkUrl}"/>
	<div class="${component.styleAttributes}">
	<div class="categoryType">${promoDescription}</div>	
		<div class="home-promo-img">
			<img src="${media.url}"
				alt="${not empty headline ? headline :media.altText}"
				title="${not empty headline ? headline : media.altText}" />
		</div>
		<div class="home-promo-text">
			<div class="banner_caption">
				<div class="caption-inner">
					<div class="caption-wrap">
					<div class="offer_sticker">	
						<c:forEach items="${promoLogo}" var="promoLogo">
							<img src="${promoLogo.url}" alt="${promoLogo.altText}" />
						</c:forEach>
						</div>				
						<h4>${component.header}</h4>
						<p>${description}</p>
						<c:choose>
							<c:when test="${not empty component.googleTracking}">
							 <c:set var ="gaBrand" value='${fn:replace(googleTracking.brand, "\'", "")}' />	
								<a class="btn" href="${emcodedUrl}" onclick="onPromoClick('${googleTracking.page}','${googleTracking.placementCode}','${gaBrand}','${googleTracking.costType}','${googleTracking.duration}','${googleTracking.creative}','${googleTracking.skuId}','${emcodedUrl}'); return !ga.loaded;">${linkName}</a>
							</c:when>
							<c:otherwise>
								<a class="btn" href="${emcodedUrl}" >${linkName}</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="promoOffer">
			<c:if test="${not empty promoDate}">
								<strong>Valid until <fmt:formatDate value="${promoDate}" pattern="dd MMM yyyy" /></strong>
								</c:if>
							</div>
	<c:if test="${not empty component.googleTracking}">
	<script>
			var gaId = "${googleTracking.page}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.placementCode}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.costType}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.duration}".replace(/[^a-zA-Z0-9-]/g,'') + "_" + "${googleTracking.creative}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'');
			
			ga('ec:addPromo', {               
			  'id': gaId,     
			  'name': "${googleTracking.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),                  
			  'position': "${googleTracking.skuId}".replace(/[^a-zA-Z0-9]/g,'')
			});	
	</script>
	</c:if> 
	</div>
	
