<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="${not empty page ? page.label : urlLink}" var="encodedUrl" />

<div class="disp-img">
	<c:choose>
		<c:when test="${empty encodedUrl || encodedUrl eq '#'}">
			<div class="container">
				<div class="content">
				<c:choose>
				<c:when test="${not empty logoMedia}">
					<img class="banner-logo" src="${logoMedia.url}">
				</c:when>
				<c:otherwise>
					<c:if test="${not empty portraitMedia}">			
						<img class="banner-logo" src="${portraitMedia.url}">
					</c:if>
				</c:otherwise>
				</c:choose>
					<h2>${headline}</h2>
					<p>${content}</p>	
					
				</div>
			</div>
			<div class="thumb">
				<picture> 
					<source srcset="${mobileMedia.url}" media="(max-width: 768px)">
					<c:if test="${not empty media.url }">
						<img srcset="${media.url}"	alt="${not empty headline ? headline :media.altText}" title="${not empty headline ? headline : media.altText}" />
					</c:if> 
				</picture>
			</div>
						
			<!--<div class="action">
				<theme:image code="img.iconArrowCategoryTile" alt="${media.altText}"/>
			</div>-->
		</c:when>
		<c:otherwise>
			<a href="${encodedUrl}">
				<div class="container">
				<div class="content">
					<c:choose>
						<c:when test="${not empty logoMedia}">
							<img class="banner-logo" src="${logoMedia.url}">
						</c:when>
						<c:otherwise>
							<c:if test="${not empty portraitMedia}">
								<img class="banner-logo" src="${portraitMedia.url}">
							</c:if>
						</c:otherwise>
					</c:choose>
					
						<h2>${headline}</h2>
						<p>${content}</p>
					 
					</div>						
				</div>
					
				<div class="thumb">				
				<picture> 
					<source srcset="${mobileMedia.url}" media="(max-width: 768px)">
					<c:if test="${not empty media.url }">
						<img srcset="${media.url}"	alt="${not empty headline ? headline :media.altText}" title="${not empty headline ? headline : media.altText}" />
					</c:if> 
				</picture>
					
				</div>
				
				<%-- <span class="details">
					${content}
				</span> --%>
				<!--<span class="action">
					<theme:image code="img.iconArrowCategoryTile" alt="${media.altText}"/>
				</span>
			--></a>
		</c:otherwise>
	</c:choose>
</div>
