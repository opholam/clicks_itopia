<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="search" tagdir="/WEB-INF/tags/desktop/search" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>

<%-- <div class="results">
	<h1><spring:theme code="search.page.searchText" arguments="${searchPageData.freeTextSearch}"/></h1>
</div>
 
<nav:searchSpellingSuggestion spellingSuggestion="${searchPageData.spellingSuggestion}" />
--%>
<c:if test="${searchPageData.pagination.totalNumberOfResults > 0}">
	<div class="row load-sort">
		<search:loadAtATime  top="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/>
		<search:sortBy top="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/>	
	</div>
</c:if>

<%-- <nav:pagination top="true"  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/> --%>

<c:set value="${fn:length(searchPageData.results)}" var="displayLength"/>
<c:set value="${searchPageData.pagination.totalNumberOfResults}" var="totalResults"/>

<div id="searchProducts" >
	<div class="filterContent tb-res">
		<c:forEach items="${searchPageData.results}" var="product">
			<product:productListerItem product="${product}"/>
		</c:forEach>
		<div class="clearfix"></div>
	</div>
	<c:if test="${totalResults > displayLength }">
		<button type="submit" class="btn btn-load-more searchProductsLoadMore">Load more products</button>
	</c:if>
</div>



<%-- <nav:pagination top="false"  supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchPageData.currentQuery.url}"  numberPagesShown="${numberPagesShown}"/> --%>
