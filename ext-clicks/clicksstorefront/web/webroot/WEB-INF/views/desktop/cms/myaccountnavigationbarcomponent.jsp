<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="flag" value="true" />
<c:if
	test="${component.uid eq 'myClubCardNavBarComponent' and empty user.memberID }">
	<c:set var="flag" value="false" />
</c:if>

<c:set var="vitalityFlag" value="false" />
<c:forEach var="segment" items="${user.loyalty_segments}">
	<c:if test="${segment.segmentID eq 100008}">
		<c:set var="vitalityFlag" value="true" />
	</c:if>
</c:forEach>

<c:if test="${flag eq 'true'}">
	<c:set value="${component.styleClass} ${dropDownLayout}"
		var="bannerClasses" />
	<spring:theme code="${fn:replace(fn:toLowerCase(cmsPage.title),' ','.')}"
		var="currPage" />
	<c:if test="${fn:toLowerCase(component.link.linkName) eq fn:toLowerCase(currPage )}">
		
		<%-- Open and close tab for child link (of My clubcard and Account action tabs in My account page) is disabled now.
			<c:set var="closed" value="closed" /> --%>
			
		<c:set var="closed" value="closed" />
		<c:set var="in" value="in" />
		<c:set var="activeClass" value="active" />
	</c:if>

	<li class="panel panel-default bg-white ${closed}
		<c:if test="${component.link.linkName eq 'Account actions'}">no-toggle <c:set var="in" value="in" /></c:if>">
		<cms:component component="${component.link}"
			evaluateRestriction="true" />
			
			<c:if
			test="${not empty component.navigationNode.children}">
			<ul class="panel-collapse collapse ${in}">
				<c:forEach items="${component.navigationNode.children}" var="child">
					<c:if test="${child.links.size()>0}">

						<c:if test="${child.visible}">
							<c:forEach items="${child.links}" step="${component.wrapAfter}"
								varStatus="i">
								<c:forEach items="${child.links}" var="childlink"
									begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
									<c:set var="linkflag" value="true" />
									<c:if test="${childlink.linkName eq 'My Vitality HealthyCare' and !vitalityFlag}">
										<c:set var="linkflag" value="false" />
									</c:if>
									<c:if test="${childlink.linkName eq 'Activate Vitality HealthCare' and vitalityFlag}">
										<c:set var="linkflag" value="false" />
									</c:if>
									<c:if test="${(childlink.linkName eq 'Link existing ClubCard' or childlink.linkName eq 'Join ClubCard') and not empty user.memberID }">
										<c:set var="linkflag" value="false" />
									</c:if>
									<c:if test="${childlink.linkName eq 'Edit my medicinal details' and empty user.memberID}">
										<c:set var="linkflag" value="false" />
									</c:if>
									<%--Swapnil : Below link will be shown in phase 2. Just remove the below check to show My Promotions & offers link under my clubcard on my account side navigation  --%>
									<c:if test="${childlink.linkName eq 'My promotions & offers'}">
										<c:set var="linkflag" value="false" />
									</c:if>									
									<c:choose>
										<c:when test="${fn:toLowerCase(cmsPage.title) eq fn:toLowerCase(childlink.linkName) and linkflag}">
											<li class="${activeClass}"><cms:component
													component="${childlink}" evaluateRestriction="true" /></li>
										</c:when>
										<c:when test="${currPage eq childlink.linkName}">
											<li class="${activeClass}"><cms:component
													component="${childlink}" evaluateRestriction="true" /></li>
										</c:when>
										<c:otherwise>
											<c:if test="${linkflag }">
											<li><cms:component component="${childlink}"
													evaluateRestriction="true" /></li>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:forEach>
						</c:if>
					</c:if>
				</c:forEach>
			</ul>
		</c:if>

	</li>
</c:if>

