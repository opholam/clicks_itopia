<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:choose>
	<c:when
		test="${not empty productReferences and component.maximumNumberProducts > 0}">
		<c:set var="title" value="${component.title}" />
		<div class="bg-gray specialBlock clearfix productRange tb-res">
			<div class="container">
				<h2>
					Related popular <span>products</span>
				</h2>
				<div class="bg-white clearfix">
		<!--  Cross -  selling  -->

		<%-- <c:if test="${fn:containsIgnoreCase(title, 'acce')}">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<aside>
					<h3 class="text-center">
						<a href="#">See all products on this promotion</a>
					</h3>
					<!-- <div class="title">${component.title}</div>   -->
					<div class="article-slider">
					<c:forEach end="${component.maximumNumberProducts}"
							items="${productReferences}" var="productReference">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 plpsep ">
								<c:if
									test="${not empty productReference.target.potentialPromotions and not empty productReference.target.potentialPromotions[0].productBanner}">
									<img class="mix-match-badge"
										src="${productReference.target.potentialPromotions[0].productBanner.url}"
										alt="${productReference.target.potentialPromotions[0].description}"
										title="${productReference.target.potentialPromotions[0].description}" />
								</c:if>
								<div class="productBlock slide">

									<c:url value="${productReference.target.url}/quickView"
										var="productQuickViewUrl" />

									<a href="${productReference.target.code}" class="popup scrollerProduct">

										<product:productPrimaryImage
											product="${productReference.target}" format="product" />
										<div class="detailContent clearfix">
											<h5>
												<a href="${productReference.target.code}">${productReference.target.name}</a>
											</h5>
											<div class="content-height">
												<p>${productReference.target.summary}</p>
											</div>
											<div class="price-wrap">
												<div class="price red">
													<price:fromPrice product="${productReference.target}" />
												</div>
												<div class="offer-blk">

													<c:if
														test="${not empty productReference.target.price.grossPriceValueWithPromotionAppliedFormattedValue}">
															 Was&nbsp;<c:set var="dec"
															value="${fn:split(productReference.target.price.formattedValue, '.')}" />
																${dec[0]}<sup>${dec[1]}</sup>
														<span class="red">Save</span>
														<span class="price red"> <fmt:formatNumber
																var="save" pattern="####.##"
																value="${productReference.target.price.value - productReference.target.price.grossPriceWithPromotionApplied}" />
															<c:set var="dec1" value="${fn:split(save, '.')}" />
															R${dec1[0]}<sup>${dec1[1]}</sup>
														</span>

													</c:if>
												</div>
											</div>

											<div class="starWrapper">
												<product:productStars
													rating="${productReference.target.averageRating}" />
												<a href="${productReference.target.code}" class="btn">See More</a>
											</div>
											<c:if
												test="${ not empty productReference.target.potentialPromotions[0].description}">
												<div class="offerDesc red">
													<p> ${productReference.target.potentialPromotions[0].description} </p>
												</div>
											</c:if>
										</div>
									</a>
								</div>
							</div>
						</c:forEach>
					</div>
				</aside>
			</div>
		</c:if> --%>

		<!--  Up - selling  -->
		<c:if test="${fn:containsIgnoreCase(title, 'you')}">
		
		<div class="pdpSlider">
			<c:forEach end="${component.maximumNumberProducts}"
				items="${productReferences}" var="productReference">
				<div class="col-md-3 col-sm-6 col-xs-12 productBlock">			
			<c:if test="${not empty productReference.target.potentialPromotions}">
				<c:forEach items="${productReference.target.potentialPromotions}" var="promotions">
					<c:if test="${not empty promotions.stickerMediaUrls}">
						<c:forEach items="${promotions.stickerMediaUrls}" var="stickerURL">
							<div class="badges">
								<img src="${stickerURL}" class="promo" />
							</div>
						</c:forEach>
					</c:if>
				</c:forEach>
			</c:if>
					<c:url value="${productReference.target.url}/quickView"
						var="productQuickViewUrl" />

					<%-- <a href="${productReference.target.code}"> <product:productPrimaryImage
							product="${productReference.target}" format="product" />
					</a> --%>
						<%--  <a href="${contextPath}${productReference.target.url}" class="relatedPro_img">   --%>
 								<%-- <c:forEach items="${productReference.target.images}" var="productReferenceImage"> --%>
									<product:productGalleryProductList
										productData="${productReference.target}"/>
								<%-- </c:forEach>   --%>
<%--   						<product:productPrimaryImage
							product="${productReference.target}" format="product" />  --%>
						<!--  </a>	 --> 
						<div class="detailContent clearfix">
									<c:if test="${not empty product.brand}">
										<h5>${productReference.target.brand}</h5>
									</c:if>	
							<c:if test="${component.displayProductTitles}">
								<h5 class="title_name">
									<%-- <a href="${productReference.target.code}">${productReference.target.name}</a> --%>
									<a href="${contextPath}${productReference.target.url}">${productReference.target.name}</a>
								</h5>
							</c:if>
							<c:if test="${not empty productReference.target.summary}<">
							<div class="content-height">
								<%-- <p><a href="${productReference.target.code}">${productReference.target.summary}</a></p> --%>
								<p><a href="${contextPath}${productReference.target.url}">${productReference.target.summary}</a></p>
							</div>
							</c:if>
							<c:if test="${not empty productReference.target.price}">
									<price:fromPrice product="${productReference.target}" />
							</c:if>		
									<%-- <c:if
										test="${not empty productReference.target.price.grossPriceValueWithPromotionAppliedFormattedValue}">
										<div class="offer-blk">
															 Was&nbsp;<c:set var="dec"
											value="${fn:split(productReference.target.price.formattedValue, '.')}" />
																${dec[0]}<sup>${dec[1]}</sup>
										<span class="red">Save</span>
										<span class="price red"> <fmt:formatNumber var="save"
												pattern="####.##"
												value="${productReference.target.price.value - productReference.target.price.grossPriceWithPromotionApplied}" />
											<c:set var="dec1" value="${fn:split(save, '.')}" />
											R${dec1[0]}<sup>${dec1[1]}</sup>
										</span>										
										</div>
									</c:if> --%>
									<div class="starWrapper">
								<product:productStars
									rating="${productReference.target.averageRating}" />
								<%-- <a href="${productReference.target.code}" class="btn">See more</a> 
								<a href="${contextPath}${productReference.target.url}" class="btn">See more</a>--%>
<c:if test="${not empty productReference.target.stock and not empty productReference.target.price}">
									<c:set var="buttonType">submit</c:set>
<c:choose>
	<c:when test="${productReference.target.stock.stockLevelStatus.code eq 'outOfStock' }">
		<c:set var="buttonType">button</c:set>
		<spring:theme code="text.addToCart.outOfStock" var="addToCartText"/>
	</c:when>
	<c:when test="${productReference.target.stock.stockLevelStatus.code eq 'lowStock' }">
		<%-- <div class='lowStock'>
			<spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
		</div>   --%>
	</c:when>
</c:choose>

<div class="cart">
	<c:url value="/cart/add" var="addToCartUrl"/>
	<ycommerce:testId code="searchPage_addToCart_button_${productReference.target.code}">
		<form:form id="addToCartForm${productReference.target.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
			<input type="hidden" name="productCodePost" value="${productReference.target.code}"/>
			<c:choose>
			<c:when test="${productReference.target.stock.stockLevelStatus.code eq 'outOfStock'}">
			<div class="buy-btn-wrap">
			<button type="${buttonType}" class="btn 
			<c:if test="${productReference.target.stock.stockLevelStatus.code eq 'outOfStock' }">out-of-stock</c:if>"
			<c:if test="${productReference.target.stock.stockLevelStatus.code eq 'outOfStock' }"> disabled="disabled" aria-disabled="true"</c:if>>
			Out of stock </button>
			</div>
			</c:when>
			<c:otherwise>
			<c:set var="gabrand" value="${fn:replace(product.brand,'\\'','')}"></c:set>
					 <c:set var="brand" value="${fn:replace(product.brand,'\\'','')}"></c:set>
					<c:set var="title" value="${fn:replace(product.sizeVariantTitle,'\\'','')}"></c:set>
					<%-- <c:set var="url" value="${contextPath}${fn:replace(product.url,'\\'','')}"></c:set> --%>
					<%-- <c:set var="pageStatus" value="add from product list page"/> --%>
					<input type="hidden" name="productCode" value="${product.code}"/>
					<input type="hidden" name="gatitle" value="${title}" />
					<!-- <input type="hidden" name="gaurl" value="" /> -->
					<c:choose>
					<c:when test="${cmsPage.uid eq 'clicksproductDetails'}">
						<input type="hidden" name="pageStatus" value="add from pdp" />
					</c:when>
					 <c:otherwise>
					 	<input type="hidden" name="pageStatus" value="add from product list page" />
					 </c:otherwise>
					</c:choose>
					<input type="hidden" name="gabrand" value="${brand}" />
			
			<button type="${buttonType}" class="btn"><spring:theme code="basket.add.to.basket"/></button>
			</c:otherwise>
			</c:choose>
		</form:form>
	</ycommerce:testId>

</div>
</c:if>
<c:if test="${empty productReference.target.price}">
<button type="${buttonType}" class="btn" disabled="disabled" aria-disabled="true">out ofstock </button>
</c:if>
</div>
							<c:if test="${ not empty productReference.target.potentialPromotions[0].description}">
								<div class="offerDesc red">
									${productReference.target.potentialPromotions[0].description}
								</div>
							</c:if>
							</div>
							
						</div>
			</c:forEach>
			</div>
		
		</c:if>

		<!--  for other than accessories and you may also -->
	<%-- 	<c:if
			test="${not (fn:containsIgnoreCase(title, 'you') || fn:containsIgnoreCase(title, 'acc'))}"> other...
		<div class="scroller">
				<div class="title">${component.title}</div>
				<ul class="carousel jcarousel-skin">
					<c:forEach end="${component.maximumNumberProducts}"
						items="${productReferences}" var="productReference">
						<c:url value="${productReference.target.url}/quickView"
							var="productQuickViewUrl" />
						<li><a href="${productQuickViewUrl}"
							class="popup scrollerProduct">
								<div class="thumb">
								<product:productGalleryProductList
										productData="${productReference.target}"/>
								<c:forEach items="${productReference.images}" var="productReferenceImage">
											<c:if test="${productReferenceImage.format eq 'productListing'}">
									<product:productGalleryProductList
										imageData="${productReferenceImage}"/>
											</c:if>
								</c:forEach>
								</div> <c:if test="${component.displayProductPrices}">
								<div class="priceContainer"><format:fromPrice priceData="${productReference.target.price}"/></div>
							</c:if>  <c:if test="${component.displayProductTitles}">
									<div class="details">${productReference.target.name}---
										${productReference.target.manufacturer}</div>
								</c:if>


						</a></li>

					</c:forEach>
				</ul>
			</div>
		</c:if> --%>

				</div>
			</div>
		</div>

	</c:when>

	<c:otherwise>
		<component:emptyComponent />
	</c:otherwise>
</c:choose>
