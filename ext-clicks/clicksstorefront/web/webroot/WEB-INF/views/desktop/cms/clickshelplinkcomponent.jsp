<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="${url}" var="encodedUrl" />


<c:choose>
<c:when test="${cmsPage.uid eq 'helpLandingPage'}">
	<a class="arrow-right" href="${encodedUrl}">${component.linkName}</a>
	<p>${component.longDescription}</p>
	</c:when>
	<c:when test="${cmsPage.uid eq 'homepage'}">
		<c:choose>
			<c:when test = "${not empty component.googleTracking}">
				<a href="${encodedUrl}" title="${component.linkName}" ${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'} onclick="onPromoClick('${googleTracking.page}','${googleTracking.placementCode}','${googleTracking.brand}','${googleTracking.costType}','${googleTracking.duration}','${googleTracking.creative}','${googleTracking.skuId}','${encodedUrl}'); return !ga.loaded;">${component.linkName}</a>
			</c:when>
			<c:otherwise>
				<a href="${encodedUrl}" title="${component.linkName}" ${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'}>${component.linkName}</a>
			</c:otherwise>
		</c:choose>
	</c:when>
<c:otherwise>
	<c:if test="${fn:containsIgnoreCase(cmsPage.name, component.linkName)}">
		<c:set var="classActive" value="currentLink"/>
	</c:if>
		<li class="${classActive}"><a href="${encodedUrl}" title="${component.linkName}" ${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'}>${component.linkName}</a></li>
	</c:otherwise>
</c:choose>

<c:if test="${not empty component.googleTracking}">
<script>
			var gaId = "${googleTracking.page}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.placementCode}".replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.costType}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${googleTracking.duration}".replace(/[^a-zA-Z0-9-]/g,'') + "_" + "${googleTracking.creative}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'');
			
			ga('ec:addPromo', {               
			  'id': gaId,     
			  'name': '${googleTracking.brand}'.toLowerCase().replace(/[^a-zA-Z0-9]/g,''),                  
			  'position': '${googleTracking.skuId}'.replace(/[^a-zA-Z0-9]/g,'')
			});
	
	
</script>
</c:if> 