<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<h2>This is new created financial component</h2>


							<div class="clearfix bg-white fsOuter">
								<div class="row border-bottom">
									<div class="col-md-6 col-sm-6 col-xs-12"> 
										<div class="fsWrapper fsWrapperOne"> 
											<h2 class="darkBlue">Personal funeral insurance cover</h2> 
											<h3 class="tick">You benefit from <span class="red">R892</span> - at no cost!</h3>
											<p>Total is updated weekly, last updated 00/00/0000</p>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12"> 
										<div class="fsWrapper fsWrapperTwo"> 
											<p>Up to R7,500 Funeral cover - at no cost to you!</p>
											<p>Your cover is calculated at 10 times the value of your average monthly purchases in Clicks for the 26 weeks prior to death. The more you spend the higher your cover - up to R7,500.  <a href="javascript:void(0)">Find out more</a></p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12"> 
										<div class="fsWrapper"> 	 
											<div class="fsCover">
												<h2 class="lightGreen">Family funeral insurance cover</h2>
												<p class="tick">You're covered. <a href="javascript:void(0)">Find out more</a></p>	
											</div>
											<div class="fsCover">
												<h2 class="darkPink">Life cover</h2>
												<p>Not covered.  <a href="javascript:void(0)">Find out more</a></p>
											</div>
											<div class="fsCover">
												<h2 class="orange">Hospital event cover</h2>
												<p>Not covered.  <a href="javascript:void(0)">Find out more</a></p>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12"> 
										<div class="fsWrapper"> 
											<div class="fsCover">
												<h2 class="lavander">Car and home cover</h2>
												<p>Not covered. <a href="javascript:void(0)">Find out more</a></p>
											</div>
											<div class="fsCover">
												<h2 class="magenta">Women-only Cancer and accident cover</h2>
												<p>Not covered.  <a href="javascript:void(0)">Find out more</a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							



</body>
</html>