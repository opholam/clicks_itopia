<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="storepickup"
	tagdir="/WEB-INF/tags/desktop/storepickup"%>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<nav:pagination top="true" supportShowPaged="${isShowPageAllowed}"
	supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
	searchUrl="${searchPageData.currentQuery.url}"
	numberPagesShown="${numberPagesShown}" />

<script id="hideLoadMore" type='text/x-jsrender'></script>

<script id="showLoadMore" type='text/x-jsrender'>
	<a class="btn" onclick="loadProducts({{:count}},'${categoryCode}')">Load more products</a>
</script>

<script id="productListStart" type='text/x-jsrender'>
	<div class="productRow clearfix horiz-{{:i}}"></div>
</script>


<script id="productList" type='text/x-jsrender'>
<div class="col-md-4 col-sm-6 col-xs-12 plpsep plpsep-{{:i}}">
	<div class="productBlock">
	<ycommerce:testId code="test_searchPage_wholeProduct">
		<a href="{{:url}}" title="{{:name}}" class="productMainLink">
	
		{{if images}}
			<img src="{{:images[0].url}}" alt="{{:brand}}-{{:sizeVariantTitle}}" onError="this.onerror=null;this.src='${pageContext.servletContext.contextPath}/_ui/desktop/theme-blue/clicks/image/missing-plp.jpg';"/>
		{{else}}
			<img src="${pageContext.servletContext.contextPath}/_ui/desktop/theme-blue/clicks/image/missing-plp.jpg" alt="{{:brand}}-{{:sizeVariantTitle}}"/>
		{{/if}}
		</a>
		{{if potentialPromotions}}
			{{if potentialPromotions[0].stickerMediaUrls}}
				<div class="badges">
					{{for potentialPromotions[0].stickerMediaUrls}}
						<img src="{{>#data}}" class="promo" alt="Clicks Promo"/>
					{{/for}}
				</div>
			{{/if}}
		{{/if}}
		
		{{if nonPromoStickerUrls}}
			<div class="badges">
				{{for nonPromoStickerUrls}}
					<img src="{{>#data}}" class="promo" alt="Clicks Promo"/>
				{{/for}}
			</div>
		{{/if}}
		
	<div class="detailContent clearfix">
		<a href="{{:url}}">
			<ycommerce:testId code="searchPage_productName_link_{{:code}}">
				<h5 title='{{:brand}}'>{{:brand}}</h5>
				<div class="product-name">
				<p title='{{:name}}'>{{:name}}</p>
				</div>					
			</ycommerce:testId>
		</a>
		
		<div class="price-wrap" id="priceMessage_{{:code}}">
		</div>

		<div class="offer-blk">
			{{if price && price.grossPriceWithPromotionApplied}}
				<div id="promoMessage_{{:code}}"></div>
			{{/if}}
		</div>
			
		<div class="starWrapper">
				<div id="star_{{:code}}">
				</div>
				<div class="cart">
	<ycommerce:testId code="searchPage_addToCart_button_{{:code}}">
	<form id="addToCartForm{{:code}}" class="add_to_cart_form" action="<c:url value="/cart/add"/>">
	<input type="hidden" name="productCodePost" value="{{:code}}"/>
	{{if stock.stockLevelStatus.code == "outOfStock"}}
	<button type="{{:buttonType}}" class="btn" disabled="disabled" aria-disabled="true"> <spring:theme code="product.variants.out.of.stock"/>
	</button>
	{{/if}}
	{{if stock.stockLevelStatus.code == "inStock"}}
	<input type="hidden" name="productCode" value="{{:code}}"/>
  <input type="hidden" name="gatitle" value="{{:sizeVariantTitle}}" />
  <input type="hidden" name="pageStatus" value="add from product list page" />
  <input type="hidden" name="gabrand" value="{{:brand}}" />
	<button type="submit" class="btn addToCartButtonSubmit"><spring:theme code="basket.add.to.basket"/>
	</button>
	{{/if}}	
{{if stock.stockLevelStatus.code == "lowStock"}}
	<input type="hidden" name="productCode" value="{{:code}}"/>
  <input type="hidden" name="gatitle" value="{{:sizeVariantTitle}}" />
  <input type="hidden" name="pageStatus" value="add from product list page" />
  <input type="hidden" name="gabrand" value="{{:brand}}" />
	<button type="submit" class="btn addToCartButtonSubmit"><spring:theme code="basket.add.to.basket"/>
	</button>
{{/if}}	
	</form>
	</ycommerce:testId>
	</div>
		</div>
		<div class="offerDesc red">
			{{if potentialPromotions}}
				{{for potentialPromotions}}
				  <ycommerce:testId code="productDetails_promotion_label">
					<a class="offerDesc red" href="${contextPath}/c/OH1?q=%3Arelevance%3AallPromotions%3A{{:#data.code}}">
					{{:#data.description}}<br/>
					</a>
				   </ycommerce:testId>
				{{/for}}
			{{/if}}
		</div>	
	</div>
	</ycommerce:testId>
	</div>
</div>
</script>

<div class="productList">
	<c:forEach items="${searchPageData.results}" var="product"
		varStatus="status">

		<product:productListerItem product="${product}" />
	</c:forEach>
</div>


<nav:pagination top="false" supportShowPaged="${isShowPageAllowed}"
	supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}"
	searchUrl="${searchPageData.currentQuery.url}"
	numberPagesShown="${numberPagesShown}" />
<storepickup:pickupStorePopup />

