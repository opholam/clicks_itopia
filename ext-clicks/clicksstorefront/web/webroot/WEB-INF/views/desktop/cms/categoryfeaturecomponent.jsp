<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url value="${url}" var="componentLinkUrl"/>
<c:set value="${component.title}" var="componentTitle"/>
<c:set value="${not empty component.description ? component.description : component.category.description}" var="componentDescription"/>
<c:if test="${fn:startsWith(componentDescription, '?')}">
<c:url value="${url}${componentDescription}" var="componentLinkUrl"/>
</c:if>
<div class="productOuter">
	
	<div class="thumb">
		<a href="${componentLinkUrl}"><img title="${componentTitle}" alt="${componentTitle}" src="${not empty component.media.url ? component.media.url : component.category.thumbnail.url}"></a>
	</div>
	<c:if test="${not empty componentDescription and !fn:startsWith(componentDescription, '?')}"> 
	<div class="details">
		<a href="${componentLinkUrl}">${componentDescription}</a>
	</div>
	</c:if><!--
	<c:if test="${not empty componentTitle}">
	<div class="action">
		<theme:image code="img.iconArrowCategoryTile" alt="${componentTitle}"/>
	</div>
	</c:if>
	
	--><div class="title">
		<span><a href="${componentLinkUrl}">${componentTitle}</a></span>
	</div>
</div>
