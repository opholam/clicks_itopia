<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		
		<div class="bg-white">
		<div class="tabOuterWrapper ">
						<ul class="tabs">
						<c:if test="${firstTabMap.size() > 0}">
						<li class="active"><a href="#information" data-toggle="tab">A - M</a></li>
						</c:if>
						<c:if test="${secondTabMap.size() > 0}">
						<li><a href="#use" data-toggle="tab">N - Z</a></li>
						</c:if>			
						</ul>
							<div class="tabContentWrapper">
								<c:choose>
									<c:when test="${(empty firstTabMap) && (empty secondTabMap)}">
									No Records Found.
									</c:when>
									<c:otherwise>
									<div class="active tabContent" id="information">
									<div class="help-info">
									<c:forEach items="${firstTabMap}" var="firstTabMap">	
										<div class="contact-comm">
											<div class="contact-info">${firstTabMap.key}</div>
											<ul>
											<c:forEach items="${firstTabMap.value}" var="condData">
												<c:if test="${condData.article.articleType.code=='MAGAZINES'}">
													<li><a href="${contextPath}/clubcard-magazine/article-view/${condData.article.articleTitle}">${condData.name}</a></li>
												</c:if>
												<c:if test="${condData.article.articleType.code=='HEALTH'}">
													<li><a href="${contextPath}/health/article-view/${condData.article.articleTitle}">${condData.name}</a></li>
												</c:if>
												<c:if test="${condData.article.articleType.code=='CONDITIONS'}">
													<li><a href="${contextPath}/health/conditions/article-view/${condData.article.articleTitle}">${condData.name}</a></li>
												</c:if>
												<c:if test="${condData.article.articleType.code=='MEDICINES'}">
													<li><a href="${contextPath}/health/medicines/article-view/${condData.article.articleTitle}">${condData.name}</a></li>
												</c:if>	
												<c:if test="${condData.article.articleType.code=='VITAMINS'}">
													<li><a href="${contextPath}/health/supplements/article-view/${condData.article.articleTitle}">${condData.name}</a></li>
												</c:if>										
											</c:forEach>
											</ul>
										</div>
										<div class="separator"></div>
									</c:forEach>
								</div>
								</div>
								<div class="tabContent" id="use">
								<div class="help-info">	
										<c:forEach items="${secondTabMap}" var="secondTabMap">
										<div class="contact-comm">
											<div class="contact-info">${secondTabMap.key}</div>
											<ul>
											<c:forEach items="${secondTabMap.value}" var="condData">
												<c:if test="${condData.article.articleType.code=='MAGAZINES'}">
													<li><a href="${contextPath}/clubcard-magazine/article-view/${article.articleTitle}">${condData.name}</a></li>
												</c:if>
												<c:if test="${condData.article.articleType.code=='HEALTH'}">
													<li><a href="${contextPath}/health/article-view/${article.articleTitle}">${condData.name}</a></li>
												</c:if>
												<c:if test="${condData.article.articleType.code=='CONDITIONS'}">
													<li><a href="${contextPath}/health/conditions/article-view/${article.articleTitle}">${condData.name}</a></li>
												</c:if>
												<c:if test="${condData.article.articleType.code=='MEDICINES'}">
													<li><a href="${contextPath}/health/medicines/article-view/${article.articleTitle}">${condData.name}</a></li>
												</c:if>	
												<c:if test="${condData.article.articleType.code=='VITAMINS'}">
													<li><a href="${contextPath}/health/supplements/article-view/${condData.article.articleTitle}">${condData.name}</a></li>
												</c:if>	
											</c:forEach>
											</ul>
										</div>
										<div class="separator"></div>
										</c:forEach>
										</div>
										</div>	
									</c:otherwise>
									</c:choose>	
							</div>
			</div>
	</div>
				</div>	
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<aside>
							<div class="vitSupBlock bg-white clearfix glossaryWrapper">
							<h3>Glossary of related terms</h3>
							<ul>
							<c:forEach items="${glossaryList}" var="glossary">
							<li><strong>${glossary.glossaryName}</strong> - ${glossary.glossaryDescripiton}</li>
							</c:forEach>
							</ul>
							<c:if test="${glossaryList.size()>0}">
							<a href ="#">Show all</a>
							</c:if>
						</div>
					</aside>
					</div>
					
