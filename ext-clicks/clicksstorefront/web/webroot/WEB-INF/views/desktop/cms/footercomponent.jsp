<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>





<c:if test="${navigationNodes.size() eq '4' }">
	<c:if test="${not empty navigationNodes}">
			<ul class="footerWrapper">
				<c:forEach items="${navigationNodes}" var="node">
					<c:if test="${node.visible}">
						<li class="col-md-3 col-sm-12">
							<h5>${node.title}</h5> 
							<div class="collapse">
							<c:forEach items="${node.links}" step="${component.wrapAfter}" varStatus="i">
								<c:forEach items="${node.links}" var="childlink" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
									<cms:component component="${childlink}" evaluateRestriction="true" />
								</c:forEach>
							</c:forEach>
							</div>
						</li>
					</c:if>
				</c:forEach>
			</ul>
	</c:if>
</c:if>


<c:if test="${navigationNodes.size()>4}">
	<div class="copyright">
		<div class="container">
			<p class="copyrightText">${notice}</p>
			<p class="pull-right">
				<c:forEach items="${navigationNodes}" var="node">
					<c:if test="${node.visible}">
						<c:forEach items="${node.links}" step="${component.wrapAfter}"
							varStatus="i">
							<c:forEach items="${node.links}" var="childlink"
								begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
								<cms:component component="${childlink}"
									evaluateRestriction="true" />
							</c:forEach>

						</c:forEach>
					</c:if>
				</c:forEach>
			</p>
		</div>
	</div>
</c:if>


