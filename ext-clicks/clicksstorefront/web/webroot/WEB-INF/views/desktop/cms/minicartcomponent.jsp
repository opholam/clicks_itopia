<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/cart/miniCart/${totalDisplay}" var="refreshMiniCartUrl" />
<c:url value="/cart/rollover/${component.uid}" var="rolloverPopupUrl" />
<c:set var="checkout" value="${contextPath}/checkout" />
<c:url value="/cart" var="cartUrl" />
<a href="${cartUrl}" id="item2" class="dropdown-toggle basket"><label>Basket</label><label class="mini-basket-count">${totalItems}</label></a>
								<div class="dropdown-list mini-basket">
									<section class="bg-white clearfix basket-list">
								
<%--  <a href="${cartUrl}" class="minicart"> --%> 
	<ycommerce:testId code="miniCart_items_label">
		<!--	<c:if test="${totalDisplay == 'TOTAL'}">
				 <format:cartPrice priceData="${totalPrice}" />
			</c:if> 
			<c:if test="${totalDisplay == 'TOTAL_WITHOUT_DELIVERY'}">
				<format:price priceData="${totalNoDelivery}" />
			</c:if> -->
	</ycommerce:testId>
<!-- </a>  -->
<div id="miniCartLayer" class="miniCartPopup" data-refreshMiniCartUrl="${refreshMiniCartUrl}/?" data-rolloverPopupUrl="${rolloverPopupUrl}"></div>

          </section>
          </div>
