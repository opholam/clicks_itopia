<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<h2>This is new created points activity component</h2>
<div class="acc-table-wrap">	
									<!-- Points activity heading  -->
									<div class="clearfix bg-light-blue">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
											<div class="pull-left">
												<h2>Points activity</h2>
											</div>
											<div class="pull-right text-right">
												<span>From 16 January 2015</span>
											</div>
										</div>
									</div>
									<!-- End Points activity heading  -->
									<!-- Amount spent start -->
									<div class="clearfix bg-white amountWrapper">
										<div class="recent-head">
											<div class="col-md-10 col-sm-10 col-xs-9">
												<div class="accHeading">
													<h5>Amount spent</h5>
													<p>The amount you've spent shopping at Clicks</p>
												</div>
											</div>
											<div class="col-md-2 col-sm-2 col-xs-3">
												<div class="price blue">
												R512<sup>30</sup>
												</div>
											</div>
										</div>
									<!-- Amount spent end -->
									</div>
								<div class="clearfix bg-white">
									<!-- Clicks points  -->
									<div class="repeat-table-head rewards-custom acc-row-3"> 
										<div class="col-md-9 col-sm-10 col-xs-9 alpha-3">
											<div class="accHeading">
												<h5>Clicks points</h5>
												<p>ClubCard Points earned when shopping at Clicks</p>
											</div>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha-3">
										<div class="price blue acc-row-3">
											207
											</div>
										</div>
									</div>
									<!-- Clicks points Ends -->
									<!-- Partner points  start-->
									<div class="recent-head acc-row-3">
										<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
											<div class="accHeading">
												<h5>Partner points</h5>
												<p>ClubCard Points earned when you purchase goods or service from our partners</p>
											</div>
										
										
											<div class="row acc-row">
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														Avis</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														City Lodge Spec Savers</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														Musica</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														NetFlorist</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														NuMetro</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														Sorbet</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														SpecSavers </span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridGrey acc-col">
														<span class="pointsConent">
														Thompsons</span>
														<span class="points"> 50</span>
													</div>
												</div>
											</div>
										 </div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
											<div class="price blue acc-col-3">
												60
											</div>
										</div>
									</div>
									<!-- Partner points Ends -->
									<!-- Bonus points -->
									<div class="repeat-table-head rewards-custom acc-row-3"> 
										<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
											<div class="accHeading">
												<h5>Bonus points</h5>
												<p>Bonus Points are extra Points earned, like Double Bonus Points.</p>
											</div>
										 
											<div class="row acc-row-2">
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridWhite acc-col-2">
														<span class="pointsConent">
														Lorem</span>
														<span class="points"> 50</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridWhite acc-col-2">
														<span class="pointsConent">
														Ipsum</span>
														<span class="points"> 0</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridWhite acc-col-2">
														<span class="pointsConent">
														Dolor</span>
														<span class="points"> 10</span>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 alpha">
													<div class="gridBox gridWhite acc-col-2">
														<span class="pointsConent">
														NetFlorist</span>
														<span class="points">0</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
											<div class="price blue acc-col-3">
												60
											</div>
										</div>
									</div>
									<!-- Bonus points Ends-->
									<!-- Pharmacy points -->
									<div class="recent-head acc-row-3"> 
										<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
											<div class="accHeading">
												<h5>Pharmacy Points</h5>
												<p>Pharmacy Points are earned on dispensing fees or clinic services</p>
											</div>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
											<div class="price blue acc-col-3">
												60
											</div>
										</div>
									</div>
									<!-- Pharmacy points Ends-->
									<!-- BabyClub points -->
									<div class="repeat-table-head rewards-custom acc-row-3"> 
										<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
											<div class="accHeading">
												<h5>BabyClub points</h5>
												<p>If you are a BabyClub member, you earn points on selected goods and services</p>
											</div>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
											<div class="price blue  acc-col-3">
												0
											</div>
										</div>
									</div>
									<!-- BabyClub points Ends-->
									<!-- Senior points -->
									<div class="recent-head acc-row-3"> 
										<div class="col-md-9 col-sm-10 col-xs-9 alpha3"> 
											<div class="accHeading">
											<h5>Senior Points</h5>
											<p>Earn additional points on ClubCard Seniors Double Points Days</div>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
										<div class="price blue  acc-col-3">
											0
											</div>
										</div>
									</div>
									<!-- Senior points Ends-->
									<!-- Total points -->
									<div class="repeat-table-head rewards-custom acc-row-3"> 
										<div class="col-md-9 col-sm-10 col-xs-9 alpha3">
											<div class="totalText">Total</div>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 col-md-offset-1 alpha3">
										<div class="totalValue acc-col-3">
											347
											</div>
										</div>
									</div>
									<!-- Total points Ends-->
								</div>
							</div>
							




</body>
</html>