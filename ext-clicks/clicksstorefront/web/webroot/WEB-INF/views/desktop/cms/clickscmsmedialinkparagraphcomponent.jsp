<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<c:if test="${cmsPage.uid eq 'helpingHandClinic'}">
	<img src="${component.media.url}" alt="${component.media.altText}" />
	<a href="#" class="borderBlockTitle">${component.mediaLink.linkName}</a>
	<p>${component.title}</p>

</c:if>

<c:if test="${cmsPage.uid eq 'rewards-activity'}">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="magazineImg">
			<img src="${component.media.url}" alt="${component.media.altText}" />
		</div>
		<div class="magazineContent">
			<p>${component.title}</p>
			<a href="${component.mediaLink.url}" ${component.mediaLink.target == null || component.mediaLink.target == 'SAMEWINDOW' ? '' : 'target="_blank"'} class="borderBlockTitle">${component.mediaLink.linkName}</a>
		</div>
	</div>
</c:if>

<c:if test="${cmsPage.uid eq 'botswana-rewards-activity'}">
	<c:if test="${fn:contains(component.uid,'section5')}">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="magazineImg">
				<img src="${component.media.url}" alt="${component.media.altText}" />
			</div>
			<div class="magazineContent">
				<p>${component.title}</p>
				<a href="${component.mediaLink.url}" class="borderBlockTitle">${component.mediaLink.linkName}</a>
			</div>
		</div>
	</c:if>

	<c:if test="${fn:contains(component.uid,'section4')}">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class=" clearfix club-card-block">
				<div class="image-center bcBox">
					<div class="club-img">

						<img src="${component.media.url}" alt="${component.media.altText}" />
						<a href="${component.mediaLink.url}">${component.mediaLink.linkName}</a>
					</div>
					<div class="club-text">${component.title}</div>
				</div>
			</div>
		</div>
	</c:if>
</c:if>

<c:if test="${cmsPage.uid eq 'myAccountPage'}">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class=" clearfix club-card-block">
			<div class="image-center bcBox">
				<div class="club-img">

					<img src="${component.media.url}" alt="${component.media.altText}" />
					<a href="${component.mediaLink.url}">${component.mediaLink.linkName}</a>
				</div>
				<div class="club-text">${component.title}</div>
			</div>
		</div>
	</div>
</c:if>
<c:if test="${cmsPage.uid eq 'babyClubLandingPage'}">
	<img src="${component.media.url}" alt="${component.media.altText}" /> 
	<a href="${mediaLink.url}"> 
	 <c:if test="${not empty component.mediaLink.linkName}">
			${component.mediaLink.linkName}
		</c:if> 
	</a>
	<p> ${title}</p>
</c:if> 