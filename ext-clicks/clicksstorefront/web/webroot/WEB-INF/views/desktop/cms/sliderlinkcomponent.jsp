<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:url var="link" value="${url}"/>
<a href="${link}">
	<h4>${title}</h4>
	<p>${summary}</p>
</a>