<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="price" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:choose>
	<c:when test="${component.showLimitedDetails}">
		<c:url value="${product.url}" var="productUrl" />
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 promotion_product_wrap">
			<div class="row promo_feauture_wrap">
			
			<c:choose>
			
				<c:when test="${cmsPage.masterTemplate.uid eq 'CategoryPageTemplate'}">
									<c:choose>
									<c:when test="${fn:startsWith(categoryName, 'clp_')}">
									<c:set var="gaCategoryName" value="${categoryName}"/>
									</c:when>
									<c:otherwise>
									<c:set var="gaCategoryName" value="clp_${categoryName}"/>
									</c:otherwise>
									</c:choose>
				</c:when>
				<c:when test="${cmsPage.uid eq 'promotionsPage'}">
					<c:set var="gaCategoryName" value="specials"/>
				</c:when>
				<c:otherwise>
					<c:set var="gaCategoryName" value="other"/>
				</c:otherwise>
				</c:choose>
					<c:set var="gabrand" value="${fn:replace(product.brand,'\\'','')}"></c:set>
					<c:set var="gatitle" value="${fn:replace(product.sizeVariantTitle,'\\'','')}"></c:set>
					<c:set var="gaurl" value="${contextPath}${fn:replace(product.url,'\\'','')}"></c:set>
					<c:set var="gaplacement" value="${elementPos+1}000" />
					
				<a href="${productUrl}" onclick="onProductClick('${product.code}','${gatitle}','${gabrand}', '${gaCategoryName}', '${gaplacement}', '${gaurl}', '${gaplacement}'); return !ga.loaded;">
					<div class="col-lg-5 col-md-5 col-sm-4 col-xs-3 promo_prod_image">
					
						<div class="badges"> 
							<c:if test="${not empty product.potentialPromotions}">
					         <c:forEach items="${product.potentialPromotions}" var="promotions">
					           <c:if test="${not empty promotions.stickerMediaUrls}">
					             <c:forEach items="${promotions.stickerMediaUrls}" var="stickerURL">
					                <img src="${stickerURL}" class="promo" alt="Clicks Promo" />
					             </c:forEach>
					            </c:if>
					         </c:forEach>
				         </c:if>
				         
				         <c:if test="${not empty product.nonPromoStickerUrls}">
								<c:forEach items="${product.nonPromoStickerUrls}" var="stickerURL">
									<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
								</c:forEach>
							</c:if>
			         </div>
						<img src="${component.media.url}" />
					</div>
					<div class="col-lg-7 col-md-7 col-sm-8 col-xs-9 promo_prod_desc">
						<h4>${component.headline}</h4>
						<span>${component.title}</span>
						<p>${component.description}</p>
					</div>
				</a>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<c:url value="${product.url}" var="productUrl" />
		<c:if test="${not empty component.headline}">
			<div class="doubleBlock">
				<div class="categoryType">${component.headline}</div>
				<h3>${component.title}</h3>
				<p class="content-height">${component.description}</p>
				<!-- <button class="btn">Shop now</button> -->
			</div>
		</c:if>

		<div class="promoImg">
			<img src="${component.media.url}" />
		</div>

		<c:if
			test="${not empty product.potentialPromotions and not empty component.product.potentialPromotions[0].productBanner}">
			<div class="col-md-4 col-sm-12 col-xs-12">

				<div class="productBlock">
					<div class="badges">
						<img class="promo"
							src="${product.potentialPromotions[0].productBanner.url}"
							alt="${product.potentialPromotions[0].description}"
							title="${product.potentialPromotions[0].description}" />

					</div>

				</div>
			</div>
		</c:if>

		<div class="detailContent clearfix">
			<h5>
				<a href="p/${product.code}">${product.name}</a>
			</h5>
			<div class="content-height">
				<p>${product.summary}</p>
			</div>

			<price:fromPrice product="${product}" />



			<%-- <div class="offer-blk">

				<c:if
					test="${not empty product.price.grossPriceValueWithPromotionAppliedFormattedValue}">


	 Was&nbsp;<c:set var="dec"
						value="${fn:split(product.price.formattedValue, '.')}" />
		${dec[0]}<sup>${dec[1]}</sup>
					<span class="red">Save</span>
					<span class="price red"> <fmt:formatNumber var="save"
							pattern="####.##"
							value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
						<c:set var="dec1" value="${fn:split(save, '.')}" /> R${dec1[0]}<sup>${dec1[1]}</sup>
					</span>

				</c:if>
			</div> --%>

			<div class="starWrapper">

				<product:productStars rating="${product.averageRating}" />


				<a href="#" class="btn">Buy</a>
			</div>
			<c:if test="${ not empty product.potentialPromotions[0].description}">
				<div class="offerDesc skyblue">
					<span>${product.potentialPromotions[0].description}</span>
				</div>
			</c:if>
		</div>
		<c:if test="${ not empty product.remarks}">
			<div class="quoteBlock">
				<div class="quoteBlockInner">
					<p>"${product.remarks}"</p>
				</div>
			</div>
		</c:if>
	</c:otherwise>
</c:choose>

<script>
		    ga('ec:addImpression', {               
				  'id': "${product.code}",
				  'name': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${product.sizeVariantTitle}".toLowerCase().replace(/[^a-zA-Z0-9' ]/g,''),   
				  'brand': "${product.brand}".toLowerCase().replace(/[^a-zA-Z0-9']/g,''),
				  'list': "${gaCategoryName}".toLowerCase().replace(/[^a-zA-Z0-9_]/g,''),
				  'position': "${gaplacement}"
		  	});
</script>
