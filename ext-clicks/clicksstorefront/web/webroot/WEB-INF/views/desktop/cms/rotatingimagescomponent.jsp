<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


	<div class="home-slider-wrap">
	<%-- <div class="banner_alternative">
		<img src="${banners[0].media.url}" alt="${not empty headline ? headline :media.altText}" title="${not empty headline ? headline : media.altText}" />
	</div> --%>
		<ul class="home_bxslider" style="display: none;">
			<c:set var="defaultBanner" value=""/>
			<c:if test="${not empty banners}">
				<c:set var="defaultBanner" value="${banners[0]}"/>
			</c:if>
			
			<c:forEach items="${banners}" var="banner" varStatus="status">
				<li>
					<input id="gaallow_${status.index}" type="hidden" value="${banner.googleTracking}" />
					<input id="gapage_${status.index}" type="hidden" value="${banner.googleTracking.page}" />
					<input id="gaplacementcode_${status.index}" type="hidden" value="${banner.googleTracking.placementCode}" />
					<input id="gabrand_${status.index}" type="hidden" value="${banner.googleTracking.brand}" />
					<input id="gacosttype_${status.index}" type="hidden" value="${banner.googleTracking.costType}" />
					<input id="gaduration_${status.index}" type="hidden" value="${banner.googleTracking.duration}" />
					<input id="gacreative_${status.index}" type="hidden" value="${banner.googleTracking.creative}" />
					<input id="gaskuid_${status.index}" type="hidden" value="${banner.googleTracking.skuId}" />
				
					<c:if test="${ycommerce:evaluateRestrictions(banner)}">
						<cms:component component="${banner}" />
					</c:if>
				</li>
			</c:forEach>
		</ul>
	</div>
	
	<script>
		<c:if test="${not empty defaultBanner and not empty defaultBanner.googleTracking}">
			var gaId = "${defaultBanner.googleTracking.page}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${defaultBanner.googleTracking.placementCode}".replace(/[^a-zA-Z0-9]/g,'') + "_" + "${defaultBanner.googleTracking.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${defaultBanner.googleTracking.costType}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" + "${defaultBanner.googleTracking.duration}".replace(/[^a-zA-Z0-9-]/g,'') + "_" + "${defaultBanner.googleTracking.creative}".toLowerCase().replace(/[^a-zA-Z0-9]/g,'');
			
			ga('ec:addPromo', {               
			  'id': gaId,     
			  'name': "${defaultBanner.googleTracking.brand}".toLowerCase().replace(/[^a-zA-Z0-9]/g,''),                  
			  'position': '${defaultBanner.googleTracking.skuId}'
			});
	
		</c:if> 	
	</script>



<%--
<div class="slider_component">
	<div id="homepage_slider" class="svw">
		<ul>
			<c:forEach items="${banners}" var="banner" varStatus="status">
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
					<li><a tabindex="-1" href="${encodedUrl}"<c:if test="${banner.external}"> target="_blank"</c:if>><img src="${banner.media.url}" alt="${not empty banner.headline ? banner.headline : banner.media.altText}" title="${not empty banner.headline ? banner.headline : banner.media.altText}"/></a></li>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</div>

--%>

