<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:choose>
	<c:when test="${prescription eq true}">
		<!-- My medicine  heading  -->
		<div class="clearfix bg-light-blue">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
				<div class="pull-left">
					<h2>My medicines</h2>
				</div>
				<div class="pull-right text-right">
					<a class="arrow-right" href="javascript:void(0)">Manage asdsd
						my medicines</a>
				</div>
			</div>
		</div>
		<!-- My medicines heading  -->

		<!-- My medicines table data -->
		<div class="acc-body clearfix bg-white">
			<div class="recent-head no-border">
				<div class="col-lg-10 col-md-10 col-sm-9 col-xs-9">Medication</div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">Status</div>
			</div>
			<div class="repeat-table-wrap">
				<div class="recent-head repeat-table-head">
					<div class="col-lg-10 col-md-10  col-sm-9 col-xs-9">
						Repeat Drugname prescription or <strong> Josefa </strong> <a
							href="#"> Edit </a>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
						<span class="clock-image"></span> <a href="">Repeat</a>
					</div>
				</div>

				<div class="recent-custom repeat-table-data">
					<div class="col-lg-7 col-md-7 col-sm-5 col-xs-5">Spironolactone
						30 x 200mg</div>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">4 refills
						left</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">due 30 May</div>
				</div>
				<div class="recent-custom repeat-table-data">
					<div class="col-lg-7 col-md-7 col-sm-5 col-xs-5">Lotensin 30
						x 500mg</div>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">2 refills
						left</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">due 30 May</div>
				</div>
			</div>

			<div class="repeat-table-wrap">
				<div class="recent-head repeat-table-head">
					<div class="col-lg-10 col-md-10  col-sm-9 col-xs-9">
						Repeat Drugname prescription or <strong> Josefa </strong> <a
							href="#"> Edit </a>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
						<span class="clock-image"></span> <a href="">Repeat</a>
					</div>
				</div>

				<div class="recent-custom repeat-table-data">
					<div class="col-lg-7 col-md-7 col-sm-5 col-xs-5">Drug name 30
						x 200mg</div>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">4 refills
						left</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">due 30 May</div>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
	
	<div class="myyacc-noclub">
	<div class="promo-links clearfix">
		
		<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 promo-options">
			<h2> You have no online prescription or medicine orders </h2>
		</div>
		
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 promo-options">
		<div class="promolink-wrap">
				 How to <a href="/requestmedicine"> request medicine online</a> or <a href="promotion"> schedule regular chronic prescriptions</a> 
		</div>
		</div>
		</div>
	</div>	
</c:otherwise>
</c:choose>
