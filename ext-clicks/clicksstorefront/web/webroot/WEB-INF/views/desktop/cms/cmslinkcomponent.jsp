<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:url value="${url}" var="encodedUrl" />
<c:choose>
<c:when test="${fn:containsIgnoreCase(cmsPage.uid, 'CategoryPage')}">
<a href="${component.url}">${component.linkName}</a>
</c:when>
<c:otherwise>
<li><a href="${encodedUrl}" title="${component.linkName}" ${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'}>${component.linkName}</a></li>
</c:otherwise>
</c:choose>