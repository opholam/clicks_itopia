<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:choose>
	<c:when test="${not empty orders}">
		<!-- Recent Orders heading  -->
		<div class="clearfix bg-light-blue">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">
				<div class="pull-left">
					<h2>
						<!-- Recent Orders -->
						<spring:theme code="recent.orders.title" />
					</h2>
				</div>
<%-- 				<div class="pull-right text-right">
					<a class="arrow-right" href="javascript:void(0)"> <!-- See	order history -->
						<spring:theme code="see.order.history.title" />
					</a>
				</div> --%>
			</div>
		</div>
		<!-- End Recent Orders heading  -->
		<div class="acc-body clearfix bg-white">
			<div class="recent-head">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
					<spring:theme code="order.number" />
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
					<!-- Order placed -->
					<spring:theme code="order.placed.title" />
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
					<!-- Delivery address -->
					<spring:theme code="delivery.address.title" />
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
					<!-- Status -->
					<spring:theme code="status.title" />
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
					<!-- Status -->
					<spring:theme code="order.view.details" />
				</div>
			</div>
			<div class="repeat-table-wrap">
				<c:forEach items="${orders}" var="order" varStatus="status">
					<div class="recent-head recent-custom">
											<c:choose>
											<c:when test="${not empty order.clicksOrderCode}">
											<c:set var="orderCode" scope="page" value="${order.clicksOrderCode}"/>
											</c:when>
											<c:otherwise>
											<c:set var="orderCode" scope="page" value="${order.code}"/>
											</c:otherwise>
											</c:choose>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">#${orderCode}</div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
							<fmt:formatDate pattern="dd MMM yyyy" value="${order.placed}" />
						</div>
						<c:if test="${not empty order.deliveryAddress}">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
						<c:if test="${not empty order.deliveryAddress.line1}">${order.deliveryAddress.line1}, </c:if>
						<c:if test="${not empty order.deliveryAddress.postalCode}">${order.deliveryAddress.postalCode}, </c:if>
						<c:if test="${not empty order.deliveryAddress.town}">${order.deliveryAddress.town}</c:if>
						</div>
						</c:if>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						${order.statusDisplay}
						</div>   
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"><a href="${contextPath}/my-account/order/${orderCode}">View order details</a></div>
					</div>
				</c:forEach>
				<div class="text-center clearfix">
				<a class="btn" href="${contextPath}/my-account/orders">SEE ORDER HISTORY</a>
</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
	<div class="myyacc-noclub">
	<div class="promo-links clearfix">
		
		<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 promo-options">
			<h2> You have no current orders </h2>
		</div>
		
		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 promo-options">
		<div class="promolink-wrap">
				 See <a href="./c/OH1?q=%3Aprice-asc%3AnewProduct%3Atrue"> what's new</a> or <a href="./specials"> on promotion</a> 
		</div>
		</div>
		</div>
	</div>	
	</c:otherwise>
</c:choose>

