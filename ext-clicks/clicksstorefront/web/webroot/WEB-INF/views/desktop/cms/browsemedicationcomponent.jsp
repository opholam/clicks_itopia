<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="${url}" var="encodedUrl" />
<%-- <div class="cmsimage">
	<img title="${media.altText}" src="${media.url}" alt="${media.altText}" />
</div> --%>
<div class="symptomsChecker">
	<a class="book" href="${encodedUrl}" title="${component.linkName}"
		${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'}>${component.linkName}</a>
</div>
