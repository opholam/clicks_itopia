<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<!-- 
<c:if test="${fn:length(breadcrumbs) > 0}"> 
	<div id="breadcrumb" class="breadcrumb">
		<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	</div>
</c:if>
 -->

<c:if test="${fn:length(breadcrumbs) > 0}"> 
<div class="bg-white breadcrumb-block">	
	<div class="container">	
		<ul id="breadcrumb" class="breadcrumb">
			<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
		</ul>
		<c:if test="${cmsPage.uid eq 'productList'}">
				<div class="pull-right">
					<div class="plp_promo">
						<c:if test="${not empty plpPromotion}">
								<cms:pageSlot position="Section1" var="feature">
								<a href="${request.contextPath}/c/${categoryCode}?q=%3Arelevance%3ApromoStickerplp%3A${plpPromotion.promotionTypeID}">
									<cms:component component="${feature}" />
								</a>
								</cms:pageSlot>
						</c:if>
					</div>
				</div>
			</c:if>
			
			<c:if test="${cmsPage.masterTemplate.uid eq 'CategoryPageTemplate'}">
				<div class="pull-right">
					<div class="plp_promo">
						<c:if test="${not empty plpPromotion}">
							<cms:pageSlot position="Breadcrum-Pomo" var="feature">
							<a href="${request.contextPath}/c/${categoryCode}?q=%3Arelevance%3ApromoStickerplp%3A${plpPromotion.promotionTypeID}">
									<cms:component component="${feature}" />
							</a>
								</cms:pageSlot>
						</c:if>
					</div>
				</div>
			</c:if>
	</div>
</div>
</c:if>


