<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:choose>
	<c:when test="${cmsPage.uid eq 'helpingHandClinic'}">
		<div class="videoOuter">
			 <a
				data-toggle="modal" data-target="#${media.code}"><img
				src="${media.url}" /></a>
				<a data-toggle="modal" data-target="#${media.code}" class="popup"></a>
		</div>
		<p>${description}</p>
	</c:when>
	<c:when test="${cmsPage.uid eq 'clubCardPage'}">
			<div class="videoOuterWrapper">
				<div class="videoOuter">
					 <a data-toggle="modal" data-target="#${media.code}"><img
						src="${media.url}" /></a>
						<a data-toggle="modal" data-target="#${media.code}" class="popup"></a>
				</div>
				<a href="javascript:void(0)">${description}</a>
			</div>
	</c:when>
	<c:otherwise>
		<div class="col-md-6 col-sm-6 col-xs-12 fluVideoContainer">
			<div class="videoOuterWrapper">
				<div class="videoOuter">
					 <a data-toggle="modal" data-target="#${media.code}"><img
						src="${media.url}" /></a>
					<a data-toggle="modal" data-target="#${media.code}" class="popup"></a>
				</div>
				<a href="javascript:void(0)" data-toggle="modal" data-target="#${media.code}">${description}</a>
			</div>
		</div>
		
	</c:otherwise>
</c:choose>
<div class="modal fade" id="${media.code}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">${description}</h4>
			</div>
			<div class="modal-body">
				<iframe width="${width}" height="${height}" src="${urlLink}" allowfullscreen></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

		