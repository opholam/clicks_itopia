<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="p" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- <picture> 
	<source srcset="${mobileMedia.url}" media="(max-width: 768px)">
	<c:if test="${not empty media.url }">
		<img srcset="${media.url}"
			alt="${not empty headline ? headline :media.altText}"
			title="${not empty headline ? headline : media.altText}" />
	</c:if> </picture> --%>


	<c:choose>
		<c:when test="${not empty googleTracking}">
		 <c:set var ="gaBrand" value='${fn:replace(googleTracking.brand, "\'", "")}' />		
			<a href="${buttonUrl}" tabindex="-1" onclick="onPromoClick('${googleTracking.page}','${googleTracking.placementCode}','${gaBrand}','${googleTracking.costType}','${googleTracking.duration}','${googleTracking.creative}','${googleTracking.skuId}','${buttonUrl}'); return !ga.loaded;">
			<picture> 
				 <source srcset="${media.url}" media="(min-width: 769px)"></source>
				  <c:if test="${not empty mobileMedia.url }">
				 <img srcset="${mobileMedia.url}" alt="${not empty headline ? headline :media.altText}" title="${not empty headline ? headline : media.altText}" />
				 </c:if> 
			</picture>
		</a>
		</c:when>
		<c:otherwise>
			<a href="${buttonUrl}" tabindex="-1">
			<picture> 
				 <source srcset="${media.url}" media="(min-width: 769px)"></source>
				  <c:if test="${not empty mobileMedia.url }">
				 <img srcset="${mobileMedia.url}" alt="${not empty headline ? headline :media.altText}" title="${not empty headline ? headline : media.altText}" />
				 </c:if> 
			</picture>
		</a>
		</c:otherwise>
	</c:choose>
	<%-- <div class="container guru">
		<div class="banner-text ">
			<c:if test="${not empty bannerTopMedia}">
				<img src="${bannerTopMedia.url}" alt="${bannerTopMedia.altText}" />
			</c:if>
			<c:if test="${not empty headline and content}">
			<h1>${headline}</h1>
			<h2>${content}</h2>
			</c:if>
			<c:if test="${not empty buttonText}">
				<c:choose>
					<c:when test="${not empty googleTracking}">
						<a href="${buttonUrl}" class="btn primary_btn" tabindex="-1" onclick="onPromoClick('${googleTracking.page}','${googleTracking.placementCode}','${googleTracking.brand}','${googleTracking.costType}','${googleTracking.duration}','${googleTracking.creative}','${googleTracking.skuId}','${buttonUrl}'); return !ga.loaded;">${buttonText}</a>
					</c:when>
					<c:otherwise>
						<a href="${buttonUrl}" class="btn primary_btn" tabindex="-1">${buttonText}</a>
					</c:otherwise>
				</c:choose>
				
			</c:if>
		</div>

	</div> --%>
	
	
	

