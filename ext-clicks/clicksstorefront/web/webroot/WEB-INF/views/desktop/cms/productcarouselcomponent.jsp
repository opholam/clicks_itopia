<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:url value="${product.url}" var="productUrl" />
<c:choose>
	<c:when test="${not empty productData}">
	<c:if test="${not empty title}">
		<div class="contentWrapper clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blue-heading-bar">

				<div class="pull-left">
					<h2>${title}</h2>
				</div>
				<div class="pull-right text-right">
					<!-- <a class="arrow-right" href="javascript:void(0)">See all</a> -->
				</div>
			</div>
		</div>
		</c:if>
		<c:choose>
			<c:when test="${component.popup}">
				<div class="bg-white productRow">
					<ul class="clearfix article-slider">
						<c:forEach items="${productData}" var="product" varStatus="count">

							<c:url value="${product.url}/quickView" var="productQuickViewUrl" />
							<li>
								<!-- <div class="col-lg-12 col-md-4 col-sm-12 col-xs-12"> -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 plpsep">
									<div class="productBlock">
									
								<c:set var="gabrand" value="${fn:replace(product.brand,'\\'','')}" scope="request"></c:set>
								<c:set var="gatitle" value="${fn:replace(product.sizeVariantTitle,'\\'','')}" scope="request"></c:set>
								<c:set var="gaurl" value="${contextPath}${fn:replace(product.url,'\\'','')}" scope="request"></c:set>
								<c:set var="gaplacement" value="${count.index+1}" scope="request"/>
								
								<%-- <c:if test="${count.index+1 eq 2}">
									<c:set var="gaplacement" value="2" scope="page"/>
								</c:if>
								<c:if test="${count.index+1 eq 3}">
									<c:set var="gaplacement" value="3" scope="page"/>
								</c:if>	
										 --%>
								<div class="thumb">
									
								 <c:choose>
									<c:when test="${cmsPage.masterTemplate.uid eq 'CategoryPageTemplate'}">
									<c:choose>
									<c:when test="${fn:startsWith(categoryName, 'clp_')}">
									<c:set var="gaCategoryName" value="${categoryName}"  scope="request"/>
									</c:when>
									<c:otherwise>
									<c:set var="gaCategoryName" value="clp_${categoryName}"  scope="request"/>
									</c:otherwise>
									</c:choose>
									</c:when>
									<c:when test="${cmsPage.uid eq 'promotionsPage'}">
										<c:set var="gaCategoryName" value="specials"  scope="request"/>
									</c:when>	
									<c:otherwise>
										<c:set var="gaCategoryName" value="others" scope="request"/>
									</c:otherwise>
								</c:choose>
<%-- 										 <a href="${contextPath}${product.url}" onclick="onProductClick('${product.code}','${gatitle}','${gabrand}', '${gaCategoryName}', '${count.index+1}', '${gaurl}', '${gaplacement}'); return !ga.loaded;">  --%>
											<%-- <product:productPrimaryImage product="${product}" format="product"/> --%>
											<product:productGalleryProductList productData="${product}"/>
<!-- 											</a>  -->
											<div class="badges">
												<c:if test="${not empty product.potentialPromotions}">
													<c:forEach items="${product.potentialPromotions}" var="promotions">
														<c:if test="${not empty promotions.stickerMediaUrls}">
															<c:forEach items="${promotions.stickerMediaUrls}" var="stickerURL">
																	<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
															</c:forEach>
														</c:if>
													</c:forEach>
												</c:if>
																						
												<c:if test="${not empty product.nonPromoStickerUrls}">
													<c:forEach items="${product.nonPromoStickerUrls}" var="stickerURL">
														<img src="${stickerURL}" class="promo" alt="Clicks Promo"/>
													</c:forEach>
												</c:if>
											</div>	
										</div>
										<div class="detailContent clearfix">
											<a href="${contextPath}${product.url}" onclick="onProductClick('${product.code}','${gatitle}','${gabrand}', '${gaCategoryName}', '${count.index+1}','${gaurl}', '${gaplacement}'); return !ga.loaded;">
											<h5>${product.brand}</h5>

											<div class="product-name"><p class="product-category" title="${product.styleVariantTitle}">${product.baseProductTitle ne null ? product.baseProductTitle : product.styleVariantTitle ne null ? product.styleVariantTitle: product.sizeVariantTitle ne null? product.sizeVariantTitle: product.name}</p></div>
											
											</a>
											<c:if test="${not empty product.summary}">
												<div class="content-height"><p>${product.summary}</p></div>
											</c:if>
											
											<format:fromPrice product="${product}" />
												<%-- <c:if test="${not empty product.price.grossPriceValueWithPromotionAppliedFormattedValue}">
													<div class="offer-blk">
													 Was&nbsp;<c:set var="dec"
														value="${fn:split(product.price.formattedValue, '.')}" />
														${dec[0]}<sup>${dec[1]}</sup>
													<span class="red">Save</span>
													<span class="price red"> <fmt:formatNumber
															var="save" pattern="####.##"
															value="${product.price.value - product.price.grossPriceWithPromotionApplied}" />
														<c:set var="dec1" value="${fn:split(save, '.')}" />
														R${dec1[0]}<sup>${dec1[1]}</sup>
													</span>
													</div>
												</c:if> --%>
											
												<div class="starWrapper">
													<product:productStars rating="${product.averageRating}" />
													<div class="buy-btn-wrap">
													<product:productListAddToCartPanel product="${product}" allowAddToCart="${empty showAddToCart ? true : showAddToCart}" isMain="true" gaPositionVal="${gaplacement}"/>
													<%--	<c:choose>
															<c:when test="${not empty component.googleTracking}">
																<a href="${contextPath}/p/${product.code}" class="btn" onclick="onProductClick('${product.code}','${gatitle}','${gabrand}', '${categoryName}', '${count.index+1}', '${gaurl}', '${gaplacement}'); return !ga.loaded;">See more</a>
															</c:when>
															<c:otherwise>
																<a href="${contextPath}/p/${product.code}" class="btn">See more</a>
															</c:otherwise>
														</c:choose>	 --%>
													</div>
												</div>
											<c:if test="${not empty product.potentialPromotions}">
											<div class="offerDesc red">
												${product.potentialPromotions[0].description}
											</div>
												</c:if>
											
										</div>
										<c:if test="${ not empty product.remarks}">
											<div class="quoteBlock">
												<div class="quoteBlockInner">
													<p>"${product.remarks}"</p>
												</div>
											</div>
										</c:if>
									</div>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:when>
			<c:otherwise>
				<ul class="carousel jcarousel-skin">
					<c:forEach items="${productData}" var="product">

						<c:url value="${product.url}" var="productUrl" />
						<li><a href="${productUrl}" class="scrollerProduct">
								<div class="thumb">
									<%-- <product:productPrimaryImage product="${product}" format="product" /> --%>
									<product:productGalleryProductList productData="${product}"/>
								</div>
								<div class="priceContainer">
									<format:fromPrice product="${product}" />
								</div>
								<div class="details">${product.name}</div>

						</a></li>
					</c:forEach>
				</ul>
			</c:otherwise>
		</c:choose>
	</c:when>

	<c:otherwise>
		<component:emptyComponent />
	</c:otherwise>
</c:choose>

<!-- Google Analytics tracking Data for Product Carousal Component -->

<script>
		var productsJsonObj = JSON.parse('${productJson}');
		var count = 1;
		for (var productIndex in productsJsonObj) 
		{
		  if (productsJsonObj.hasOwnProperty(productIndex)) 
		  {	
		    var product = productsJsonObj[productIndex];
		    
		    var brandname= String(product.brand).toLowerCase()=='undefined'?"":String(product.brand).toLowerCase();
		    
		    ga('ec:addImpression', {               
				  'id': product.code,     
				  'name': brandname.toLowerCase().replace(/[^a-zA-Z0-9]/g,'') + "_" +String(product.sizeVariantTitle).toLowerCase().replace(/[^a-zA-Z0-9 ]/g,''),   
				  'brand': brandname.toLowerCase().replace(/[^a-zA-Z0-9]/g,''),
				  'list': '${gaCategoryName}'.toLowerCase().replace(/[^a-zA-Z0-9_]/g,''),
				  'position': count
		  	});
		    count= count + 1;
		  }
		}
</script>
