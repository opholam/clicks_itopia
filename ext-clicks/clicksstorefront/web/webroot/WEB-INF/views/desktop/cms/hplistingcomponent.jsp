<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="${conditon_url_prefix}" var="detailsURL"></c:url>
<h3>${component.title}</h3>
<ul>
			<c:forEach items="${HPListingData}" var="hpData" begin="0"	end="${component.displayCount-1}">
				<li><a	href="${contextPath}/conditionList?id=${hpData.conditionId}&type=${type}&name=${hpData.conditionName}">${hpData.conditionName}</a></li>
			</c:forEach>
			<c:if test="${listSize>component.displayCount}">
				<li><a href="${detailsURL}">View all</a></li>
			</c:if>
</ul>
