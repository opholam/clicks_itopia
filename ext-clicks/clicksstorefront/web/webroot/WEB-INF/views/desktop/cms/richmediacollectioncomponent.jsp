<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<h4>${richCollTitle}</h4>
<p>${richCollSummary}</p>
	
<ul class="bxslider-ss">
	<c:if test="${component.visible}">
		<c:forEach items="${richMedia}" var="component" varStatus="i">			
			<li><cms:component component="${component}" evaluateRestriction="true" /></li>	
		</c:forEach>
	</c:if>
</ul>

<div id="pagerSS">
	<div id="bx-pager">
		<c:forEach items="${richMedia}" var="component" varStatus="i">		
			<a data-slide-index="${i.count-1}" href=""><c:if test="${not empty component.thumbnail}"><img src="${component.thumbnail.url}"/></c:if></a>		
		</c:forEach>
	</div>
</div>

