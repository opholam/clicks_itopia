
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type='text/javascript'>
<c:set var="basePageVal" value="${basePage}"/>
<c:set var="gaPage" value="$basePage"/>
<c:choose>
	<c:when test="${fn:contains(param.type, 'MEDICINE')}">
		<c:set var="basePageVal" value="MEDICINE"/>
	</c:when>
	<c:when test="${fn:contains(param.type, 'VITAMINS')}">
		<c:set var="basePageVal" value="VITAMINS"/>
	</c:when>
	<c:when test="${fn:contains(param.type, 'CONDITION')}">
		<c:set var="basePageVal" value="CONDITION"/>
	</c:when>
<c:otherwise>
	<c:set var="basePageVal" value="${basePage}"/>
	<c:set var="gaPage" value="$basePage"/>
</c:otherwise>
</c:choose>
  googletag.cmd.push(function() {
	 <c:forEach var="sl" items="${slotandtraget}">
	 
	<c:if test ="${fn:contains(sl, '$ccBaby')}">
	
		<c:set var = "sl" value="${fn:replace(sl, '$ccBaby', not empty ccBaby ? ccBaby :' ')}" />
	</c:if>
	<c:if  test ="${fn:contains(sl, '$ccBeauty')}">
		<c:set var = "sl" value="${fn:replace(sl, '$ccBeauty', not empty ccBeauty? ccBeauty :' ')}" />
	</c:if>
	<c:if  test ="${fn:contains(sl, '$ccLifestyle')}">
		<c:set var = "sl" value="${fn:replace(sl, '$ccLifestyle', not empty ccLifestyle ? ccLifestyle :' ')}" />
	</c:if>
	  googletag.defineSlot(${fn:replace(sl, gaPage, basePageVal)}).addService(googletag.pubads());
	</c:forEach>
	googletag.pubads().collapseEmptyDivs();
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>







