<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script>
	!function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/
				.test(d.location) ? 'http' : 'https';
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = p + '://platform.twitter.com/widgets.js';
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, 'script', 'twitter-wjs');
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<a href="${url}" class="twitter-timeline"  data-widget-id="${twitterId}"  width="1024" height="400"
	data-show-count="false">${linkName}</a>
</div>

<!-- <a class="twitter-timeline" data-widget-id="640884305854599169"
	href="https://twitter.com/Clicks_SA" width="350" height="300">
	Tweets by @TwitterDev </a>
 -->
<!--	
<a class="twitter-timeline" data-widget-id="600720083413962752"
	href="https://twitter.com/TwitterDev" data-chrome="nofooter noborders">
	Tweets by @TwitterDev </a>
 -->
<!--  script for tweeter plug-in  -->


<%-- <a class="twitter-timeline" data-widget-id="${twitterId}"
	href="${url}" data-chrome="nofooter noborders">
	${linkName}
</a> --%>
