<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
<c:when test="${cmsPage.itemtype eq 'CategoryPage' && cmsPage.uid ne 'ProductListingPage'}">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-zero">
  <div id="${divId}" style="${style}"> 
   <script type='text/javascript'>
     googletag.cmd.push(function() {
       googletag.display('${display}');
     });
   </script> 
</div>
</div>
</c:when>
<c:otherwise>
  <div id="${divId}" class="google-ads" style="${style}"> 
   <script type='text/javascript'>
     googletag.cmd.push(function() {
       googletag.display('${display}');
     });
   </script> 
</div>
</c:otherwise>
</c:choose>