<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="value" value="${component.uid}" />
<c:url value="${url}" var="encodedUrl" />
<c:choose>
<c:when test="${fn:containsIgnoreCase(value, 'Promo')}">
<li class="menu-block-images">
	<%-- <a href="${component.pdfMedia.url}" title="${pdfMedia.altText}" download="${component.pdfMedia.url}">
			<img title="${media.altText}" src="${media.url}" alt="${media.altText}" />${component.linkName}
 	</a> --%>
	<a class="" href="${encodedUrl}" 	title="${component.linkName}" ${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'}>
			<img title="${media.altText}" src="${media.url}" alt="${media.altText}" /><span>${component.linkName}</span>
	</a>
</li>
</c:when>
<c:otherwise>
<li class="menu-block-images">
	<a class="" href="${encodedUrl}" 	title="${component.linkName}" ${component.target == null || component.target == 'SAMEWINDOW' ? '' : 'target="_blank"'}>
			<img title="${media.altText}" src="${media.url}" alt="${media.altText}" />
			<span>${component.linkName}</span>
	</a>
</li>
</c:otherwise>
</c:choose>