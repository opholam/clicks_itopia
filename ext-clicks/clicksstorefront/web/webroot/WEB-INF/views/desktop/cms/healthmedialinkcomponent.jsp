
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url value="${urlLink.url}"  var="encodedUrl"/>
<c:if test="${fn:containsIgnoreCase(urlLink.url, urlLink.name)}">
<c:set var="storeFinder" value="cmsClinicsButton"/>
</c:if>
<div class="greenbgOuter">
	<div class="green-bg">
		 <a class="arrow-right"  href="${encodedUrl}" id="${storeFinder}"> <img title="${media.altText}" src="${media.url}" alt="${media.altText}" /><span>${title}</span> </a>
	</div>
</div>
