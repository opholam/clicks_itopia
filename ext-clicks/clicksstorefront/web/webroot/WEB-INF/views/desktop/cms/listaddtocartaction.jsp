<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="buttonType">submit</c:set>
<c:choose>
	<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
		<c:set var="buttonType">button</c:set>
		<spring:theme code="text.addToCart.outOfStock" var="addToCartText"/>
	</c:when>
	<c:when test="${product.stock.stockLevelStatus.code eq 'lowStock' }">
		<%--<div class='lowStock'>
			<spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
		</div>  --%>
	</c:when>
</c:choose>

<div class="cart clearfix">
	<c:url value="/cart/add" var="addToCartUrl"/>
	<ycommerce:testId code="searchPage_addToCart_button_${product.code}">
		<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
			<input type="hidden" name="productCodePost" value="${product.code}"/>
				<c:choose>
			<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
			<button type="${buttonType}" class="btn" disabled="disabled" aria-disabled="true">
			${addToCartText}</button>
			</c:when>
			<c:when test="${product.stock.stockLevelStatus.code eq 'inStock' or product.stock.stockLevelStatus.code eq 'lowStock'}">
					<c:set var="gabrand" value="${fn:replace(product.brand,'\\'','')}"></c:set>
					 <c:set var="brand" value="${fn:replace(product.brand,'\\'','')}"></c:set>
					<c:set var="title" value="${fn:replace(product.sizeVariantTitle,'\\'','')}"></c:set>
					<%-- <c:set var="url" value="${contextPath}${fn:replace(product.url,'\\'','')}"></c:set> --%>
					
					<input type="hidden" name="productCode" value="${product.code}"/>
					<input type="hidden" name="gatitle" value="${title}" />
					<input type="hidden" name="gabrand" value="${brand}" />
					<c:if test="${cmsPage.uid eq 'search'}">
						<c:set var="pageStatus" value="add from search results"/>
					</c:if>
					<c:if test="${cmsPage.uid eq 'productList'}">
						<c:set var="pageStatus" value="add from product list page"/>
					</c:if>
					<input type="hidden" name="pageStatus" value="${pageStatus}" />
					
			<button type="${buttonType}" class="btn addToCartButtonSubmit" disabled="disabled"><spring:theme code="basket.add.to.basket"/></button>
			</c:when>
			</c:choose>
		</form:form>
	</ycommerce:testId>

</div>
