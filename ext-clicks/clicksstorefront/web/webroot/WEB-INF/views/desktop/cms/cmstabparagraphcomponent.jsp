<%@ page trimDirectiveWhitespaces="true" %>

<%-- <div class="tabHead">${component.title}</div>
<div class="tabBody">${component.content}</div> --%>

<div class="accordion-section" >
	<a class="accordion-section-title" href="#accordion-${elementPos}">${component.title}</a>
	<div id="accordion-${elementPos}" class="accordion-section-content">
		${component.content}		
	</div><!--end .accordion-section-content-->
</div><!--end .accordion-section-->
