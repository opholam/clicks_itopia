<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:url value="${component.url}" var="urlLink"/>
<c:choose>
	<c:when test="${cmsPage.uid eq 'babyClubLandingPage'}">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<a href="${urlLink}"><img src="${component.media.url}" alt="${component.media.altText}" /></a>
				<c:if test="${component.linkname ne null}">
				<h4><a href="${urlLink}">${component.linkname}</a></h4></c:if>
		</div>
	</c:when>
	<c:when test="${cmsPage.uid eq 'joinBabyClubPage'}">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<a href="${urlLink}"><img src="${component.media.url}" alt="${component.media.altText}" /></a>
				<c:if test="${component.linkname ne null}">
						<h4><a href="${urlLink}">${component.linkname}</a></h4>
				</c:if>
		</div>
	</c:when>
	<c:when test="${cmsPage.uid eq 'clubCardPage'}">
		 <div class="videoOuterWrapper">
		 	<div class="videoOuter">
			<a href="${urlLink}"><img src="${component.media.url}" alt="${component.media.altText}" /></a>
				<c:if test="${component.linkname ne null}">
						<h4><a href="${urlLink}">${component.linkname}</a></h4>
				</c:if>
		</div>
		</div>
	</c:when>
	<c:when test="${cmsPage.uid eq 'helpingHandClinic'}">
				<c:if test="${component.linkname ne null}">
						<li><a href="${component.media.url}" download="${component.linkname}">${component.linkname}</a></li>
				</c:if>
	</c:when>
	<c:when test="${cmsPage.uid eq 'medicalSchemeMembers'}">
		 <a href="${urlLink}" ${component.style}>
				<img src="${component.media.url}" alt="${component.media.altText}" />
			</a>
			<c:if test="${not empty component.linkname}">
				<a href="${urlLink}">
				<h4>${component.linkname}</h4></a>
			</c:if>
	</c:when>
	<c:when test="${cmsPage.uid eq 'magazineLandingPage'}">
	 <a class="social-share" target="_blank" href="${urlLink}"><img src="${component.media.url}" alt="${component.media.altText}" /></a>
      		 <c:if test="${component.linkname ne null}">
				<a href="${urlLink}">${component.linkname}</a>
			</c:if>
	
	</c:when>
	 <c:otherwise>
	 	<div class="mediaLinkWrapper">
	 	<c:choose>
	 	<c:when test="${cmsPage.uid eq 'discoveryVitalityMemberPage' or cmsPage.uid eq 'activateVitalityPage'}">
	 	 <a href="${urlLink}" target="_blank">
				<img src="${component.media.url}" alt="${component.media.altText}" />
		</a>
	 	</c:when>
	 	<c:otherwise>
      		 <a href="${urlLink}">
				<img src="${component.media.url}" alt="${component.media.altText}" />
			</a>
			</c:otherwise>
			</c:choose>
			<c:if test="${component.linkname ne null}">
				<h4><a href="${urlLink}">
				${component.linkname}</a></h4>
			</c:if>
				
		</div>		
		 
    </c:otherwise>
</c:choose>
