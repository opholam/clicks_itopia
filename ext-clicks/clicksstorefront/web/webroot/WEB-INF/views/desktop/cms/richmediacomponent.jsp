<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="slider-ss-image">
<c:if test="${not empty picture && not empty picture.url}">	
	<img src="${picture.url}"/>	
</c:if>

<c:if test="${empty picture}">	
	<iframe width="285px" height="220px" src="${videoUrl}"></iframe>	
</c:if>
</div>
<div class="slider-ss-content">
	<h4>${richTitle}</h4>
	<p>${component.richSummary}</p>
</div>
