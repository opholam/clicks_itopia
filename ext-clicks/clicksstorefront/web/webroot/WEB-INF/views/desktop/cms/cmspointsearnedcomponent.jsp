<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@taglib prefix="dateFormat" uri="/WEB-INF/tld/DateFormat.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<h2>This is new created points earned component</h2>

<div class="clearfix success-message">
								Congratulations, you've qualified for cashback!
							</div>
							<!-- End success message -->
							<!-- pointsDetails start-->
							<div class="ptDetailsWrapper">	
								<div class="div-rounded pointsDetails">	
									<div class="pricePtWrapper">
										<div class="price blue">
											387
										</div>
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">Points earned so far</div>
									</div>
								</div>
								<div class="div-rounded pointsDetails">	
									<div class="pricePtWrapper">
										<div class="price blue">
											R12<sup>30</sup>
										</div>
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">Equivalent cashback</div>
									</div>
								</div>
								<div class="div-rounded pointsDetails">	
									<div class="pricePtWrapper">
										<div class="blue">
										<c:choose>
										<c:when test="${not empty cashbackDate}">
										<dateFormat:dateFormat conversionType="date" input="${cashbackDate}" format="dd MMM yyyy"/>
										</c:when>
										<c:otherwise>
										16 Mar
											 <span>2015</span> 
										</c:otherwise>
										</c:choose>
										</div>
										<!--<span>2015</span>-->
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">You'll receive this on</div>
									</div>
								</div>
								<div class="div-rounded pointsDetails ptpromotion">	
									<div class="pricePtWrapper">
										<div class="price red">
											R38<sup>00</sup>
										</div>
									</div>
									<div class="pointsContentWrapper">
										<div class="pointsContent">Available to spend now
										<a href="javascript:void(0)">See all promotions</a></div>
									</div>
								</div>
							</div>
							<div class="ptDetailsMessage">
							Current points collection period: <span>16 January 2015 - 15 March 2015</span>
							</div>
							<!-- pointsDetails End-->



</body>
</html>