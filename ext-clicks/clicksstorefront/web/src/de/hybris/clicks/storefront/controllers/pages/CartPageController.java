/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;


import de.hybris.clicks.facade.cartPage.CartPageFacade;
import de.hybris.clicks.facades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.forms.VoucherForm;
import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.enums.CheckoutFlowEnum;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.CartPageCalForm;
import de.hybris.platform.clicksstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.promotion.CommercePromotionFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.voucher.VoucherService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



/**
 * Controller for cart page, to show and update cart
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/cart")
public class CartPageController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(CartPageController.class);

	public static final String SHOW_CHECKOUT_STRATEGY_OPTIONS = "storefront.show.checkout.flows";

	private static final String CART_CMS_PAGE_LABEL = "cart";
	private static final String CONTINUE_URL = "continueUrl";
	public static final String DELIVERYFEE = "DeliveryFee";
	private static final String ERROR_MSG_TYPE = "errorMsg";

	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cartPageFacade")
	private CartPageFacade cartPageFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource
	private CustomerFacade customerFacade;
	@Resource
	private CartService cartService;
	@Resource
	protected JaloSession jaloSession;
	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;
	@Resource
	private PromotionsService promotionsService;
	@Resource
	private ModelService modelService;

	@Resource
	private CommercePromotionFacade commercePromotionFacade;

	@Resource(name = "voucherService")
	private VoucherService voucherService;

	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;

	@Override
	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	@ModelAttribute("showCheckoutStrategies")
	public boolean isCheckoutStrategyVisible()
	{
		return getSiteConfigService().getBoolean(SHOW_CHECKOUT_STRATEGY_OPTIONS, false);
	}

	/**
	 * Show cart.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	/*
	 * Display the cart page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showCart(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException,
			CommerceCartModificationException
	{
		/*
		 * final String referrer = request.getHeader("referer"); if (referrer != null && (!referrer.contains("cart") ||
		 * !referrer.contains("checkout"))) { getSessionService().setAttribute("returnToShopping", referrer); }
		 */
		//final String contextPath = request.getContextPath();

		final String referer = request.getHeader("referer");
		model.addAttribute("shoppingUrl", referer);
		getSessionService().setAttribute("returnToShopping", referer);
		/*
		 * if (userFacade.isAnonymousUser()) { getSessionService().setAttribute("returnToShopping", "/"); }
		 */

		final String sessionReferer = getSessionService().getAttribute("returnToShopping");

		if (StringUtils.isNotEmpty(referer) && (referer.contains("/p/") || referer.contains("/c/") || referer.contains("/search")))
		{
			getSessionService().setAttribute("returnToShopping", referer);
		}
		else if (StringUtils.isEmpty(sessionReferer)
				|| (StringUtils.isNotEmpty(sessionReferer) && !sessionReferer.contains("/p/") && !sessionReferer.contains("/c/") || !sessionReferer
						.contains("/search")))
		{
			getSessionService().setAttribute("returnToShopping", "/");
			model.addAttribute("returnToShopping", "/");
		}
		else if (StringUtils.isNotEmpty(referer) && referer.contains("/login/"))
		{
			getSessionService().setAttribute("returnToShopping", "/");
			model.addAttribute("returnToShopping", "/");
		}

		if (cartService.hasSessionCart())
		{
			CartModel cartmodel = cartService.getSessionCart();
			/*
			 * Recalculating session cart
			 */
			cartmodel = cartPageFacade.calculateCart(cartmodel);
			CartData cartData = cartFacade.getSessionCart();
			final CartPageCalForm cartPageCalForm = new CartPageCalForm();
			String cartClubcardPoints = null;
			String totalPrice = "";
			if (null != cartData.getTotalPriceWithTax() && null != cartData.getTotalPriceWithTax().getFormattedValue())
			{
				totalPrice = cartData.getTotalPriceWithTax().getFormattedValue();
			}
			else if (null != cartData.getTotalPrice() && null != cartData.getTotalPrice().getFormattedValue())
			{
				totalPrice = cartData.getTotalPrice().getFormattedValue();
			}
			else if (null != cartData.getSubTotal().getFormattedValue())
			{
				totalPrice = cartData.getSubTotal().getFormattedValue();
				/*
				 * Calculation of club card points
				 */
			}
			try
			{
				if (StringUtils.isNotEmpty(totalPrice))
				{
					cartClubcardPoints = cartPageFacade.calculateClubCardPoints(totalPrice, cartClubcardPoints);
				}
				if (null != cartClubcardPoints)
				{
					cartData = cartPageFacade.populateData(cartData, cartClubcardPoints);
					cartPageCalForm.setClubcardPoints(cartData.getClubcardPoints());
					model.addAttribute("clubcardPoints", cartPageCalForm.getClubcardPoints());
				}
			}
			catch (final Exception e)
			{
				//
			}


			// fetching the category url
			for (final OrderEntryData entrydata : cartFacade.getSessionCart().getEntries())
			{
				for (final CategoryData category : entrydata.getProduct().getCategories())
				{
					if (category.getUrl().contains("OH"))
					{
						model.addAttribute("url", category.getUrl());
					}
				}
			}
			model.addAttribute("cartPageCalForm", cartPageCalForm);
		}
		/*
		 * cart page user navigation check
		 */
		prepareDataForPage(model);
		return ControllerConstants.Views.Pages.Cart.CartPage;
	}

	/**
	 * Handle the '/cart/checkout' request url. This method checks to see if the cart is valid before allowing the
	 * checkout to begin. Note that this method does not require the user to be authenticated and therefore allows us to
	 * validate that the cart is valid without first forcing the user to login. The cart will be checked again once the
	 * user has logged in.
	 *
	 * @return The page to redirect to
	 */
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	@RequireHardLogIn
	public String cartCheck(final Model model, final RedirectAttributes redirectModel) throws CommerceCartModificationException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
		if (Boolean.FALSE.equals(cartService.getSessionCart().getBasketEditable()))
		{
			return REDIRECT_PREFIX + "/cart";
		}
		if (!cartFacade.hasSessionCart() || cartFacade.getSessionCart().getEntries().isEmpty())
		{
			LOG.info("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}

		if (validateCart(redirectModel))
		{
			return REDIRECT_PREFIX + "/cart";
		}

		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + "/checkout";
	}

	// This controller method is used to allow the site to force the visitor through a specified checkout flow.
	// If you only have a static configured checkout flow then you can remove this method.
	@RequestMapping(value = "/checkout/select-flow", method = RequestMethod.GET)
	@RequireHardLogIn
	public String initCheck(final Model model, final RedirectAttributes redirectModel,
			@RequestParam(value = "flow", required = false) final CheckoutFlowEnum checkoutFlow,
			@RequestParam(value = "pci", required = false) final CheckoutPciOptionEnum checkoutPci)
			throws CommerceCartModificationException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		if (!cartFacade.hasSessionCart() || cartFacade.getSessionCart().getEntries().isEmpty())
		{
			LOG.info("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}

		// Override the Checkout Flow setting in the session
		if (checkoutFlow != null && StringUtils.isNotBlank(checkoutFlow.getCode()))
		{
			SessionOverrideCheckoutFlowFacade.setSessionOverrideCheckoutFlow(checkoutFlow);
		}

		// Override the Checkout PCI setting in the session
		if (checkoutPci != null && StringUtils.isNotBlank(checkoutPci.getCode()))
		{
			SessionOverrideCheckoutFlowFacade.setSessionOverrideSubscriptionPciOption(checkoutPci);
		}

		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + "/checkout";
	}


	/**
	 * Update cart quantities.
	 *
	 * @param entryNumber
	 *           the entry number
	 * @param model
	 *           the model
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param request
	 *           the request
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Update Cart Quantity
	 */
	/*
	 * Update Cart Quantity
	 */
	@SuppressWarnings("boxing")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model,
			@Valid final UpdateQuantityForm form, final BindingResult bindingResult, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final String referrer = request.getHeader("referer");
		if (Boolean.FALSE.equals(cartService.getSessionCart().getBasketEditable()))
		{
			if (referrer != null && (!referrer.contains("cart")))
			{
				return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
			}
			else
			{
				return REDIRECT_PREFIX + "/cart";
			}
		}

		//final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		/*
		 * fetching url for back to shopping link
		 */
		if (null != referrer)
		{
			model.addAttribute("shoppingUrl", referrer);
		}
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				if (error.getCode().equals("typeMismatch"))
				{
					GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
				}
				else
				{
					GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
				}
			}
		}
		else if (cartFacade.getSessionCart().getEntries() != null)
		{
			try
			{
				final CartModificationData cartModification = cartFacade.updateCartEntry(entryNumber, form.getQuantity().longValue());
				if (cartService.hasSessionCart())
				{
					if (cartModification.getQuantity() == form.getQuantity().longValue())
					{
						// Success

						if (cartModification.getQuantity() == 0)
						{
							// Success in removing entry
							GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
									"basket.page.message.remove");
						}
						else
						{
							// Success in update quantity
							model.addAttribute("quantity", form.getQuantity());
							/*
							 * adding attributes to model for cart pop up
							 */
							if (referrer != null && (!referrer.contains("cart")))
							{
								form.setQuantity(cartModification.getEntry().getQuantity());
								model.addAttribute("updateQuantityForm" + cartModification.getEntry().getEntryNumber(), form);
								model.addAttribute("entry", cartModification.getEntry());
							}
							else
							{
								GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
										"basket.page.message.update");
							}
						}
					}
					else if (cartModification.getQuantity() > 0)
					{
						if (referrer != null && (!referrer.contains("cart")))
						{
							//adding attributes to model for cart pop up
							form.setQuantity(cartModification.getEntry().getQuantity());
							model.addAttribute("updateQuantityForm" + cartModification.getEntry().getEntryNumber(), form);
							model.addAttribute("entry", cartModification.getEntry());
							model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.reducedNumberOfItemsAdded."
									+ cartModification.getStatusCode());
						}
						else
						{
							// Less than successful
							GlobalMessages.addFlashMessage(
									redirectModel,
									GlobalMessages.ERROR_MESSAGES_HOLDER,
									"basket.page.message.update.reducedNumberOfItemsAdded.lowStock",
									new Object[]
									{ cartModification.getEntry().getProduct().getName(), cartModification.getQuantity(),
											form.getQuantity(),
											request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
						}
					}
					else
					{
						if (referrer != null && (!referrer.contains("cart")))
						{
							//adding attributes to model for cart pop up
							form.setQuantity(cartModification.getEntry().getQuantity());
							model.addAttribute("updateQuantityForm" + cartModification.getEntry().getEntryNumber(), form);
							model.addAttribute("entry", cartModification.getEntry());
							// Less than successful
							model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.reducedNumberOfItemsAdded."
									+ cartModification.getStatusCode());
						}
						else
						{
							if (referrer != null && (!referrer.contains("cart")))
							{
								model.addAttribute(ERROR_MSG_TYPE, "basket.page.message.update.reducedNumberOfItemsAdded."
										+ cartModification.getStatusCode());
							}
							else
							{
								// No more stock available
								GlobalMessages.addFlashMessage(
										redirectModel,
										GlobalMessages.ERROR_MESSAGES_HOLDER,
										"basket.page.message.update.reducedNumberOfItemsAdded.noStock",
										new Object[]
										{ cartModification.getEntry().getProduct().getName(),
												request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
							}
						}
					}

					if (referrer != null && (!referrer.contains("cart")))
					{
						// Redirect to the cart page on update success so that the browser doesn't re-post again
						model.addAttribute(
								"product",
								productFacade.getProductForCodeAndOptions(cartModification.getEntry().getProduct().getCode(),
										Arrays.asList(ProductOption.BASIC)));
						CartModel cart = cartService.getSessionCart();
						cart = cartPageFacade.calculateCart(cart);
						final CartData cartData = cartFacade.getSessionCart();
						model.addAttribute("cartData", cartData);
						return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
					}
					else
					{
						// Redirect to the cart page on update success so that the browser doesn't re-post again
						return REDIRECT_PREFIX + "/cart";
					}
				}
			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
			}
		}
		if (referrer != null && (!referrer.contains("cart")))
		{
			return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
		}
		else
		{
			prepareDataForPage(model);
			return ControllerConstants.Views.Pages.Cart.CartPage;
		}
	}


	protected void createProductList(final Model model) throws CMSItemNotFoundException
	{
		final CartData cartData = cartFacade.getSessionCartWithEntryOrdering(true);
		boolean hasPickUpCartEntries = false;
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				if (!hasPickUpCartEntries && entry.getDeliveryPointOfService() != null)
				{
					hasPickUpCartEntries = true;
				}
				final UpdateQuantityForm uqf = new UpdateQuantityForm();
				uqf.setQuantity(entry.getQuantity());
				model.addAttribute("updateQuantityForm" + entry.getEntryNumber(), uqf);
			}
		}

		model.addAttribute("cartData", cartData);
		model.addAttribute("hasPickUpCartEntries", Boolean.valueOf(hasPickUpCartEntries));

		storeCmsPageInModel(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.cart"));
		model.addAttribute("pageType", PageType.CART.name());
	}

	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		final String continueUrl = (String) sessionService.getAttribute(WebConstants.CONTINUE_URL);
		model.addAttribute(CONTINUE_URL, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

		createProductList(model);

		setupCartPageRestorationData(model);
		clearSessionRestorationData();
		final VoucherForm voucherform = new VoucherForm();
		model.addAttribute("voucherform", voucherform);
		model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		model.addAttribute("supportedCountries", cartFacade.getDeliveryCountries());
		model.addAttribute("expressCheckoutAllowed", Boolean.valueOf(checkoutFacade.isExpressCheckoutAllowedForCart()));
		model.addAttribute("taxEstimationEnabled", Boolean.valueOf(checkoutFacade.isTaxEstimationEnabledForCart()));
	}

	/**
	 * Remove the session data of the cart restoration.
	 */
	protected void clearSessionRestorationData()
	{
		getSessionService().removeAttribute(WebConstants.CART_RESTORATION);
		getSessionService().removeAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS);
	}

	/**
	 * Prepare the restoration data and always display any modifications on the cart page.
	 *
	 * @param model
	 *           - the cart page
	 */
	protected void setupCartPageRestorationData(final Model model)
	{
		if (getSessionService().getAttribute(WebConstants.CART_RESTORATION) != null)
		{
			if (getSessionService().getAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS) != null)
			{
				model.addAttribute("restorationErrorMsg", getSessionService()
						.getAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS));
			}
			else
			{
				model.addAttribute("restorationData", getSessionService().getAttribute(WebConstants.CART_RESTORATION));
			}
		}
		model.addAttribute("showModifications", Boolean.TRUE);
	}

	protected boolean validateCart(final RedirectAttributes redirectModel)
	{
		//Validate the cart
		List<CartModificationData> modifications = new ArrayList<>();
		try
		{
			modifications = cartFacade.validateCartData();
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.error("Failed to validate cart", e);
		}
		if (!modifications.isEmpty())
		{
			redirectModel.addFlashAttribute("validationData", modifications);

			// Invalid cart. Bounce back to the cart page.
			return true;
		}
		return false;
	}

	@RequestMapping(value = "/redeem", method = RequestMethod.POST)
	public String voucherRedeem(@Valid @ModelAttribute("voucherForm") final VoucherForm voucherForm,
			final BindingResult bindingResult, final Model model) throws JaloPriceFactoryException, CMSItemNotFoundException,
			CalculationException
	{
		LOG.info("Voucher Redemption is Called ::::");
		final CartData cartData = cartFacade.getSessionCart();
		try
		{
			if (bindingResult.hasErrors() || StringUtils.isBlank(voucherForm.getVoucherCode())
					|| StringUtils.isEmpty(voucherForm.getVoucherCode()))
			{
				model.addAttribute("emptyVoucherCodeError", "cart.voucher.redeem.invalid");
				model.addAttribute("cartData", cartData);
				return returnCartPage(model);
			}
			else if (StringUtils.isNotBlank(voucherForm.getVoucherCode()))
			{
				voucherFacade.applyVoucher(voucherForm.getVoucherCode());
			}
		}
		catch (final VoucherOperationException e1)
		{

			model.addAttribute("emptyVoucherCodeError", StringUtils.isNotEmpty(e1.getMessage()) ? e1.getMessage()
					: "cart.voucher.redeem.invalid");
			model.addAttribute("cartData", cartData);
			return returnCartPage(model);
		}
		catch (final Exception exp)
		{
			model.addAttribute("emptyVoucherCodeError", "cart.voucher.redeem.invalid");
			model.addAttribute("cartData", cartData);
			return returnCartPage(model);
		}
		return REDIRECT_PREFIX + "/cart";
	}

	private String returnCartPage(final Model model) throws CMSItemNotFoundException
	{
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
		return ControllerConstants.Views.Pages.Cart.CartPage;
	}

	@RequestMapping(value = "/releaseVoucher", method = RequestMethod.GET)
	public String releaseVoucher(@Valid @ModelAttribute("voucherForm") final VoucherForm voucherForm,
			final BindingResult bindingResult, final Model model) throws JaloPriceFactoryException, CMSItemNotFoundException,
			CalculationException
	{
		if (StringUtils.isNotEmpty(voucherForm.getVoucherCode()))
		{
			try
			{
				voucherFacade.releaseVoucher(voucherForm.getVoucherCode());
			}
			catch (final VoucherOperationException e)
			{
				LOG.error(e.getStackTrace());
			}
		}
		model.addAttribute("returnMessage", "Voucher has been released.");
		return REDIRECT_PREFIX + "/cart";
	}

}
