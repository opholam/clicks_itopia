/**
 *
 */
package de.hybris.clicks.storefront.profile.processor.impl;

import de.hybris.clicks.storefront.edit.profile.processor.LinkCCAccountProcessor;
import de.hybris.clicks.storefront.forms.LinkCCForm;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;



/**
 * @author shruti.jhamb
 *
 */

public class LinkCCAccountProcessorImpl implements LinkCCAccountProcessor
{

	private static final Logger LOG = Logger.getLogger(LinkCCAccountProcessorImpl.class);

	@Override
	public CustomerData populateLinkedClubCardCustData(final LinkCCForm linkCCForm, final CustomerData customerData,
			final CustomerData currentCustomerData)
	{


		if (null != linkCCForm.getFirstName())
		{
			customerData.setFirstName(linkCCForm.getFirstName());
		}
		if (null != linkCCForm.getLastName())
		{
			customerData.setLastName(linkCCForm.getLastName());
		}
		if (null != linkCCForm.getEmailAddress())
		{
			customerData.setEmailID(linkCCForm.getEmailAddress());
		}
		if (null != customerData.getEmailID())
		{
			customerData.setUid(customerData.getEmailID());
		}
		if (null != linkCCForm.getPreferedName())
		{
			customerData.setPreferedName(linkCCForm.getPreferedName());
		}
		if (linkCCForm.isSAResident())
		{
			customerData.setSAResident(Boolean.TRUE);
			if (null != linkCCForm.getIDNumber())
			{
				customerData.setIDnumber(linkCCForm.getIDNumber());
			}
		}
		else
		{
			customerData.setSAResident(Boolean.FALSE);
		}
		if (null != linkCCForm.getSA_DOB_day() && null != linkCCForm.getSA_DOB_month() && null != linkCCForm.getSA_DOB_year())
		{
			final String date = linkCCForm.getSA_DOB_month() + "/" + linkCCForm.getSA_DOB_day() + "/" + linkCCForm.getSA_DOB_year();
			customerData.setNonRSA_DOB(new Date(date));
		}
		if (linkCCForm.getGender() == 1)
		{
			customerData.setGender("male");
		}
		else if (linkCCForm.getGender() == 2)
		{
			customerData.setGender("female");
		}
		final List<AddressData> addresses = new ArrayList<AddressData>();
		final AddressData address = new AddressData();

		if (null != linkCCForm.getAddressLine1())
		{
			address.setLine1(linkCCForm.getAddressLine1());
		}
		if (null != linkCCForm.getAddressLine2())
		{
			address.setLine2(linkCCForm.getAddressLine2());
		}
		if (null != linkCCForm.getAddressTypeId())
		{
			address.setTypeID(linkCCForm.getAddressTypeId());
		}
		if (null != linkCCForm.getSuburb())
		{
			address.setSuburb(linkCCForm.getSuburb());
		}
		if (null != linkCCForm.getCity())
		{
			address.setCity(linkCCForm.getCity());
		}

		if (null != linkCCForm.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setIsocode(linkCCForm.getCountry());
			address.setCountry(countryData);
		}

		if (null != linkCCForm.getPostalCode())
		{
			address.setPostalCode(linkCCForm.getPostalCode());
		}
		if (null != linkCCForm.getProvince())
		{
			address.setProvince(linkCCForm.getProvince());
		}

		addresses.add(address);
		if (addresses.size() > 0)
		{
			customerData.setAddresses(addresses);
		}
		if (linkCCForm.isMarketingEmail())
		{
			customerData.setMarketingConsent_email(new Boolean(linkCCForm.isMarketingEmail()));
		}
		if (linkCCForm.isMarketingSMS())
		{
			customerData.setMarketingConsent_SMS(new Boolean(linkCCForm.isMarketingSMS()));
		}
		if (linkCCForm.isAccinfoEmail())
		{
			customerData.setAccinfo_email(new Boolean(linkCCForm.isAccinfoEmail()));
		}
		if (linkCCForm.isAccinfoSMS())
		{
			customerData.setAccinfo_SMS(new Boolean(linkCCForm.isAccinfoSMS()));
		}

		if (StringUtils.isNotBlank(linkCCForm.getContactNumber()) || StringUtils.isNotBlank(linkCCForm.getAlternateNumber()))
		{
			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
			if (StringUtils.isNotBlank(linkCCForm.getContactNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(linkCCForm.getContactNumber());
				contact.setTypeID("2");
				contactDetailList.add(contact);
			}
			if (StringUtils.isNotBlank(linkCCForm.getAlternateNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(linkCCForm.getAlternateNumber());
				contact.setTypeID("4");
				contactDetailList.add(contact);
			}
			if (contactDetailList.size() > 0)
			{
				customerData.setContactDetails(contactDetailList);
			}
		}
		if (null != currentCustomerData.getUid())
		{
			customerData.setUid(currentCustomerData.getUid());
		}
		if (null != currentCustomerData.getDisplayUid())
		{
			customerData.setDisplayUid(currentCustomerData.getDisplayUid());
		}
		return customerData;

	}

	@Override
	public void populateLinkedClubCardCustData(final LinkCCForm linkccForm, final CustomerData customer)
	{
		if (null != customer.getEmailID())
		{
			linkccForm.setEmailAddress(customer.getEmailID());
		}
		if (null != customer.getMemberID())
		{
			linkccForm.setCcNumber(customer.getMemberID());
		}
		if (null != customer.getFirstName())
		{
			linkccForm.setFirstName(customer.getFirstName());
		}
		if (null != customer.getPreferedName())
		{
			linkccForm.setPreferedName(customer.getPreferedName());
		}
		if (null != customer.getLastName())
		{
			linkccForm.setLastName(customer.getLastName());
		}
		if (null != customer.getSAResident())
		{
			linkccForm.setSAResident(customer.getSAResident().booleanValue());
			if (null != customer.getIDnumber() && linkccForm.isSAResident())
			{
				linkccForm.setIDNumber(customer.getIDnumber());
			}
			if (null != customer.getNonRSA_DOB())
			{
				final Calendar cal = Calendar.getInstance();
				if (null != customer.getNonRSA_DOB())
				{
					linkccForm.setNonRSA_DOB(String.valueOf(customer.getNonRSA_DOB().getTime()));
					cal.setTime(customer.getNonRSA_DOB());
					final int day = cal.get(Calendar.DAY_OF_MONTH);
					linkccForm.setSA_DOB_day(String.valueOf(day));
					final int year = cal.get(Calendar.YEAR);
					linkccForm.setSA_DOB_year(String.valueOf(year));
					final int month = cal.get(Calendar.MONTH);
					linkccForm.setSA_DOB_month(String.valueOf(month));
				}
			}
		}
		if (null != customer.getGender())
		{
			if (customer.getGender().equalsIgnoreCase("MALE"))
			{
				linkccForm.setGender(1);
			}
			else if (customer.getGender().equalsIgnoreCase("FEMALE"))
			{
				linkccForm.setGender(2);
			}
		}

		try
		{

			if (null != customer.getAddresses() & customer.getAddresses().size() > 0)
			{
				for (final AddressData address : customer.getAddresses())
				{
					if (null != address.getLine1())
					{
						linkccForm.setAddressLine1(address.getLine1());
					}
					if (null != address.getLine2())
					{
						linkccForm.setAddressLine2(address.getLine2());
					}
					if (null != address.getCity())
					{
						linkccForm.setCity(address.getCity());
					}
					if (null != address.getCountry().getIsocode())
					{
						linkccForm.setCountry(address.getCountry().getIsocode());
					}
					if (null != address.getPostalCode())
					{
						linkccForm.setPostalCode(address.getPostalCode());
					}
					if (null != address.getProvince())
					{
						linkccForm.setProvince(address.getProvince());
					}
					if (null != address.getSuburb())
					{
						linkccForm.setSuburb(address.getSuburb());
					}
				}
			}

		}
		catch (final Exception e)
		{
			LOG.error("error while saving address" + e);
		}
		if (null != customer.getContactDetails())
		{
			for (final ContactDetail contact : customer.getContactDetails())
			{
				if (contact.getTypeID().equalsIgnoreCase("2"))
				{
					linkccForm.setContactNumber(contact.getNumber());
				}
				if (contact.getTypeID().equalsIgnoreCase("4"))
				{
					linkccForm.setAlternateNumber(contact.getNumber());
				}
			}
		}
		if (null != customer.getMarketingConsent_email())
		{
			linkccForm.setMarketingEmail(customer.getMarketingConsent_email().booleanValue());
		}
		if (null != customer.getMarketingConsent_SMS())
		{
			linkccForm.setMarketingSMS(customer.getMarketingConsent_SMS().booleanValue());
		}
		if (null != customer.getAccinfo_email())
		{
			linkccForm.setAccinfoEmail(customer.getAccinfo_email().booleanValue());
		}
		if (null != customer.getAccinfo_SMS())
		{
			linkccForm.setAccinfoSMS(customer.getAccinfo_SMS().booleanValue());
		}
	}


}
