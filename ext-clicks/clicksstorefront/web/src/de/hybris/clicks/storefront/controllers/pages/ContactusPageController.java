/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.myaccount.MyAccountFacade;
import de.hybris.clicks.storefront.contactUs.processor.ContactUsProcessor;
import de.hybris.clicks.storefront.controllers.validators.ContactUsValidator;
import de.hybris.clicks.storefront.forms.ContactUsForm;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TopicsData;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



/**
 * @author shruthi.jhamb
 *
 */

/*
 * Submitting a query to clicks team by customer
 */
@Controller
@Scope("tenant")
@RequestMapping("/contactus")
public class ContactusPageController extends AbstractSearchPageController
{
	private static final String CONTACT_US_PAGE = "/contactus";
	private static final String SUFFIX = "#globalMessages";

	@Resource(name = "contactUsProcessor")
	private ContactUsProcessor contactUsProcessor;

	@Resource(name = "myAccountFacade")
	private MyAccountFacade myAccountFacade;

	@Resource(name = "contactUsValidator")
	private ContactUsValidator contactUsValidator;

	@Resource(name = "customerData")
	private CustomerData customerData;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "contactUsBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder contactUsBreadcrumbBuilder;

	@Resource
	FlexibleSearchService flexibleSearchService;

	/**
	 * fetching the topic list.
	 *
	 * @return the topic list
	 */
	@ModelAttribute("Topic")
	public Collection<TopicsData> getTopicList()
	{
		Collection<TopicsData> topicList = new ArrayList<TopicsData>();
		topicList = myAccountFacade.fetchTopicData(topicList, Boolean.FALSE.booleanValue());
		return topicList;
	}

	/**
	 * fetching the contact us details in form for customer
	 *
	 * @param model
	 *           the model
	 * @return the contact us page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getContactUsPage(final Model model) throws CMSItemNotFoundException
	{
		final ContactUsForm contactUsForm = new ContactUsForm();

		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			final CustomerData customer = customerFacade.getCurrentCustomer();
			if (StringUtils.isNotBlank(customer.getFirstName()))
			{
				contactUsForm.setFirstName(getTitleCase(customer.getFirstName()));
			}
			if (StringUtils.isNotBlank(customer.getLastName()))
			{
				contactUsForm.setLastName(getTitleCase(customer.getLastName()));
			}
			contactUsForm.setEmail(customer.getUid());
			contactUsForm.setClubcardNo(customer.getMemberID());

			if (null != customer.getContactDetails())
			{
				for (final ContactDetail contact : customer.getContactDetails())
				{
					if (null != contact.getTypeID() && contact.getTypeID().equalsIgnoreCase("2"))
					{
						contactUsForm.setContactNo(contact.getNumber());
					}
				}
			}
		}

		model.addAttribute("contactUsForm", contactUsForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_US_PAGE));
		model.addAttribute("breadcrumbs", contactUsBreadcrumbBuilder.getBreadcrumbs(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_US_PAGE));
		return getViewForPage(model);
	}

	/**
	 * submitting a query to clicks team by customer.
	 *
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/contactUsProcessor", method = RequestMethod.POST)
	public String sendMessage(final ContactUsForm form, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		contactUsValidator.validate(form, bindingResult);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "contact.us.enquiry.failed");
		}
		else
		{
			final boolean result = contactUsProcessor.processsContactUsData(form);
			if (result)
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "contact.us.enquiry.success",
						new Object[]
						{ form.getEmail() });
				model.addAttribute("breadcrumbs", contactUsBreadcrumbBuilder.getBreadcrumbs(null));
				model.addAttribute(form);
				storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_US_PAGE));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_US_PAGE));

				return REDIRECT_PREFIX + CONTACT_US_PAGE + SUFFIX;
			}
			else
			{
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "contact.us.enquiry.failed", new Object[]
				{ form.getEmail() });
			}
		}
		model.addAttribute("breadcrumbs", contactUsBreadcrumbBuilder.getBreadcrumbs(null));
		storeCmsPageInModel(model, getContentPageForLabelOrId(CONTACT_US_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONTACT_US_PAGE));
		model.addAttribute(form);
		return getViewForPage(model);

	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " "; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}
}
