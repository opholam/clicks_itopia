/**
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.enums.HealthPageListingTypesEnum;
import de.hybris.clicks.core.model.HPListingComponentModel;
import de.hybris.clicks.facade.hpcondition.HpConditionFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.commercefacades.user.data.HealthConditionData;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author anshul/Swapnil
 *
 */
@SuppressWarnings("deprecation")
@Controller("HPListingComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.HPListingComponent)
public class HPListingComponentController extends AbstractCMSComponentController<HPListingComponentModel>
{

	@Resource(name = "hpConditionFacade")
	private HpConditionFacade hpConditionFacade;

	@SuppressWarnings("boxing")
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final HPListingComponentModel component)
	{
		final Collection<HealthPageListingTypesEnum> listType = component.getListType();
		for (final HealthPageListingTypesEnum healthPageListingTypesEnum : listType)
		{
			model.addAttribute("conditon_url_prefix", "/conditionList?id=viewall&type=" + healthPageListingTypesEnum.getCode()
					+ "&name=all");
			final List<HealthConditionData> conditions = hpConditionFacade
					.findConditionsByType(healthPageListingTypesEnum.getCode());
			model.addAttribute("HPListingData", conditions);
			model.addAttribute("listSize", conditions.size());
			model.addAttribute("type", healthPageListingTypesEnum.getCode());
		}
	}

	@Override
	protected String getView(final HPListingComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(HPListingComponentModel._TYPECODE);
	}


}
