/**
 *
 */
package de.hybris.clicks.storefront.edit.profile.processor;

import de.hybris.clicks.storefront.forms.LinkCCForm;
import de.hybris.platform.commercefacades.user.data.CustomerData;



/**
 * @author shruti.jhamb
 *
 */
public interface LinkCCAccountProcessor
{

	public void populateLinkedClubCardCustData(LinkCCForm linkccForm, CustomerData customer);

	//populate linked account details to customer data
	public CustomerData populateLinkedClubCardCustData(final LinkCCForm linkCCForm, final CustomerData customerData,
			final CustomerData currentCustomerData);
}
