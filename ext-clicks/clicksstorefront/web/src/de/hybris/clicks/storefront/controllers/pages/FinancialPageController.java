/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.financial.FinancialFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * The Class FinancialPageController.
 * 
 * @author ashish.vyas
 */
@Controller
@Scope("tenant")
@RequestMapping("/clubcard/financial")
public class FinancialPageController extends AbstractLoginPageController
{
	private static final Logger LOG = Logger.getLogger(FinancialPageController.class);
	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "financialFacade")
	FinancialFacade financialFacade;

	@Resource(name = "userService")
	UserService userService;


	/*
	 * @Resource(name = "userService") UserService userService;
	 */


	/**
	 * Gets the financial service.
	 * 
	 * @param model
	 *           the model
	 * @return the financial service
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getFinancialService(final Model model)
	{
		try
		{
			//final List<ClicksFinancialServiceData> financialServiceModels = financialFacade.getFinancialService();
			model.addAttribute("financialServiceModels", financialFacade.getFinancialService());
			model.addAttribute("errorMsg", null);

			storeCmsPageInModel(model, getContentPageForLabelOrId("financialServicePage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("financialServicePage"));

		}
		catch (final Exception e)
		{
			LOG.error("Error while getting Financial Services : " + e);
		}
		return ControllerConstants.Views.Pages.Financial.financialServicePage;
	}

	/**
	 * Activate link.
	 * 
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param session
	 *           the session
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/activelink", method = RequestMethod.GET)
	public String activateLink(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{
		try
		{
			final CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
			/*
			 * if (currentUser.getUid().equalsIgnoreCase("anonymous")) {
			 * 
			 * System.out.println("Request : " + request); System.out.println("Response : " + response);
			 * System.out.println("Session : " + session); request.setAttribute("targetUrlParameter",
			 * REDIRECT_TO_OTHER_PAGE); session.setAttribute("targetUrlParameter", REDIRECT_TO_OTHER_PAGE);
			 * 
			 * return getDefaultLoginPage(false, session, model); }
			 */


			if (StringUtils.isNotEmpty(currentUser.getMemberID()))
			{

				return getUrl(request, response) + "?memberID=" + currentUser.getMemberID();
			}
			else if (StringUtils.isEmpty(currentUser.getMemberID()))
			{
				return getUrl(request, response);
			}
			else
			{
				//final List<FinancialServiceModel> financialServiceModels = financialFacade.getFinancialService();
				model.addAttribute("financialServiceModels", financialFacade.getFinancialService());
				model.addAttribute("errorMsg", null);

				storeCmsPageInModel(model, getContentPageForLabelOrId("financialServicePage"));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId("financialServicePage"));
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while getting Financial Services : " + e);
		}
		return ControllerConstants.Views.Pages.Financial.financialServicePage;
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return getUrl(request, response);
	}

	@Override
	protected String getView()
	{
		return REDIRECT_PREFIX + ControllerConstants.Views.Pages.Account.AccountLoginPage;
	}

	private String getUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		final String regentURL = Config.getParameter("financial.regent.contactme.url");
		return REDIRECT_PREFIX + regentURL;
	}

}
