/**
 *
 */
package de.hybris.clicks.storefront.bookAppointment.processor;

import de.hybris.clicks.storefront.forms.ClinicBookingForm;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;


/**
 * @author shruti.jhamb
 *
 */
public interface ClinicBookingProcessor
{


	/**
	 * @param form
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	ClinicBookingData populateDataProcess(ClinicBookingForm form, BindingResult bindingResult, Model model,
			HttpServletRequest request, HttpServletResponse response);

}
