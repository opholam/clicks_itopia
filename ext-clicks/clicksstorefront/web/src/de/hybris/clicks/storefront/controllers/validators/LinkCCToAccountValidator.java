/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.platform.clicksstorefrontcommons.forms.RegisterCustomerForm;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates registration forms.
 */

public class LinkCCToAccountValidator implements Validator
{
	//public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.CUSTOM_NAME_REGEX;
	private static final String ALPHA_NUMERIC_REGEX = FormPropertyConstants.Validator.Regex.ALPHA_NUMERIC_REGEX;
	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterCustomerForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegisterCustomerForm registerCustomerForm = (RegisterCustomerForm) object;
		final String clubcardNumber = registerCustomerForm.getClubcardNumber();
		final String eMailAddress = registerCustomerForm.geteMailAddress();

		if (!ValidatorUtils.validateRegex(ALPHA_NUMERIC_REGEX, clubcardNumber))
		{
			errors.rejectValue("clubcardNumber", "register.CC.IDNumber.empty");
		}
		else if (clubcardNumber.length() > 13)
		{
			errors.rejectValue("clubCardNumber", "register.CC.IDNumber.empty");
		}
		if (!ValidatorUtils.validateRegex(EMAIL_REGEX, eMailAddress))
		{
			errors.rejectValue("eMailAddress", "register.email.invalid");
		}
		else if (StringUtils.length(eMailAddress) > 50)
		{
			errors.rejectValue("eMailAddress", "register.email.invalid");
		}




	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

}
