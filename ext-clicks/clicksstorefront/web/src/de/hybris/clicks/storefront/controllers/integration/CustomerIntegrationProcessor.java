/**
 *
 */
package de.hybris.clicks.storefront.controllers.integration;

import de.hybris.clicks.webclient.customer.client.CustomerDetailsRequestClient;
import de.hybris.clicks.webclient.customer.client.CustomerEnrollmentClient;
import de.hybris.clicks.webclient.customer.client.CustomerRegistrationClient;
import de.hybris.clicks.webclient.customer.client.CustomerUpdateClient;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.clicks.webservices.customer.impl.DefaultCustomerImportEndpoint;
import de.hybris.clicks.webservices.customer.populators.CustomerRequestPopulator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import Omni.BizTalk.Clicks.Schema.Customer.Response;





/**
 * @author abhaydh
 *
 */
public class CustomerIntegrationProcessor
{
	@Resource
	CustomerUpdateClient customerUpdateClient;
	@Resource
	CustomerEnrollmentClient customerEnrollmentClient;
	@Resource
	CustomerRegistrationClient customerRegistrationClient;
	@Resource
	CustomerDetailsRequestClient customerDetailsRequestClient;
	@Resource
	CustomerRequestPopulator customerRequestPopulator;
	@Resource
	DefaultCustomerImportEndpoint defaultCustomerImportEndpoint;
	CustomerDataPopulator customerDataPopulator;

	protected static final Logger LOG = Logger.getLogger(CustomerIntegrationProcessor.class);

	public ResultData processsEnrollRequest(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		LOG.info("In processsEnrollRequest of integ processor");
		ResultData result = new ResultData();
		result.setCode(new Integer(999));

		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return result;
		}

		CustomerData customer = new CustomerData();
		customerDataPopulator = new CustomerDataPopulator();
		customer = customerDataPopulator.populateEnrollData(form, customer);


		try
		{
			LOG.info("-- calling enroll client---");
			result = customerEnrollmentClient.enrollCustomer(customer);
		}
		catch (final Exception e)
		{
			LOG.error(e);
		}
		return result;
		//		if (result)
		//		{
		//			return "success";
		//		}
		//		else
		//		{
		//			return "failed";
		//		}
	}

	public ResultData processsRegisterCCRequest(final ClicksRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response)
	{
		ResultData result = new ResultData();
		result.setCode(new Integer(999));

		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return result;
		}
		customerDataPopulator = new CustomerDataPopulator();
		CustomerData customer = new CustomerData();
		customer = customerDataPopulator.populateRegisterCCData(form, customer);

		LOG.info("-- calling registration CC client---");
		result = customerRegistrationClient.registerCCcustomer(customer);
		return result;
		//		if (result)
		//		{
		//			return "success";
		//		}
		//		else
		//		{
		//			return "failed";
		//		}
	}

	public ResultData processsRegisterNonCCRequest(final ClicksRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response)
	{
		ResultData result = new ResultData();
		result.setCode(new Integer(999));

		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return result;
		}
		customerDataPopulator = new CustomerDataPopulator();
		CustomerData customer = new CustomerData();
		customer = customerDataPopulator.populateRegisterNonCCData(form, customer);

		LOG.info("-- calling registration Non-CC client---");
		result = customerRegistrationClient.registerNonCCcustomer(customer);
		return result;

	}


	public ClicksRegisterForm processsRetrievalRequest(final ClicksRegisterForm form, final BindingResult bindingResult,
			final Model model)
	{
		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return null;
		}
		ClicksRegisterForm registerClubCardForm = new ClicksRegisterForm();
		customerDataPopulator = new CustomerDataPopulator();
		CustomerData customer = new CustomerData();
		customer = customerDataPopulator.populateDetailsRequestData(form, customer);
		LOG.info("-- calling customer details requesr web servic client---");
		customer = customerDetailsRequestClient.getCC_CustomerDetails(customer);

		if (null == customer)
		{
			bindingResult.rejectValue("clubcardNumber", "register.CC.clubcard.empty");
			model.addAttribute(form);
			GlobalMessages.addErrorMessage(model, "form.register.failed");
			return null;
		}
		else
		{
			if (null != customer.getResult().getCode() && customer.getResult().getCode().intValue() == 0)
			{
				registerClubCardForm.setResult(true);
			}
			else
			{
				registerClubCardForm.setResult(false);
			}
			registerClubCardForm = customerDataPopulator.reversePopulateEnrollForm(registerClubCardForm, customer);
			return registerClubCardForm;
		}
	}

	public Response linkCC_RetrievalRequest(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model)
	{
		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			//GlobalMessages.addErrorMessage(model, "form.global.error");
			return null;
		}
		//final RegisterClubCardForm registerClubCardForm = new RegisterClubCardForm();
		customerDataPopulator = new CustomerDataPopulator();
		Response customerResponse = new Response();
		CustomerData customer = new CustomerData();
		customer = customerDataPopulator.populateDetailsRequestData(form, customer);
		LOG.info("-- calling customer details requesr web servic client---");
		customerResponse = customerDetailsRequestClient.linkCC_CustomerDetails(customer);
		if (null == customerResponse)
		{
			bindingResult.rejectValue("clubcardNumber", "register.CC.clubcard.empty");
			model.addAttribute(form);
			GlobalMessages.addErrorMessage(model, "form.register.failed");
			return null;
		}
		//registerClubCardForm = customerDataPopulator.reversePopulateFromResponse(registerClubCardForm, customerResponse);
		return customerResponse;
	}


	public ResultData updateLinkCCRequest(final ClicksRegisterForm registerClubCardForm, CustomerData customer,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		ResultData result = new ResultData();
		result.setCode(new Integer(999));

		if (bindingResult.hasErrors())
		{
			model.addAttribute(registerClubCardForm);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return result;
		}
		customerDataPopulator = new CustomerDataPopulator();
		//CustomerData customer = new CustomerData();
		customer = customerDataPopulator.populateRegisterCCData(registerClubCardForm, customer);
		LOG.info("-- calling registration update client---");
		result = customerUpdateClient.updateCustomer(customer);
		return result;

	}
}
