/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


/**
 * @author ashish.vyas
 *
 */
@Controller
@Scope("tenant")
public class JoinBabyController extends AbstractPageController
{
	private static final String JOIN_BABY_CLUB_LOGIN_PAGE = "joinBabyClubLoginPage";

	//@RequestMapping(value = "/join-babyclub-login", method = RequestMethod.GET)
	public String getJoinBabyLoginPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("loginForm", new LoginForm());

		storeCmsPageInModel(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_LOGIN_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_LOGIN_PAGE));
		return getViewForPage(model);
	}

}
