/**
 *
 */
package de.hybris.clicks.storefront.forms;

/**
 * @author shruti.jhamb
 *
 */
public class ClinicBookingForm
{
	private String firstName;
	private String lastName;
	private String contactNumber;
	private String region;
	private String preferredClinic;
	private String alternativeClinic;

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}



	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *           the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the region
	 */
	public String getRegion()
	{
		return region;
	}

	/**
	 * @param region
	 *           the region to set
	 */
	public void setRegion(final String region)
	{
		this.region = region;
	}

	/**
	 * @return the preferredClinic
	 */
	public String getPreferredClinic()
	{
		return preferredClinic;
	}

	/**
	 * @param preferredClinic
	 *           the preferredClinic to set
	 */
	public void setPreferredClinic(final String preferredClinic)
	{
		this.preferredClinic = preferredClinic;
	}

	/**
	 * @return the alternativeClinic
	 */
	public String getAlternativeClinic()
	{
		return alternativeClinic;
	}

	/**
	 * @param alternativeClinic
	 *           the alternativeClinic to set
	 */
	public void setAlternativeClinic(final String alternativeClinic)
	{
		this.alternativeClinic = alternativeClinic;
	}

}