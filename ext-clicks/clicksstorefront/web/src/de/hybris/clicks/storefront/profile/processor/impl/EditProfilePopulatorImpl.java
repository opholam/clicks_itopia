/**
 *
 */
package de.hybris.clicks.storefront.profile.processor.impl;

import de.hybris.clicks.storefront.edit.profile.processor.EditProfilePopulator;
import de.hybris.clicks.storefront.forms.EditClubCardForm;
import de.hybris.clicks.storefront.forms.EditNonClubCardForm;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;



/**
 * @author shruti.jhamb
 *
 */

public class EditProfilePopulatorImpl implements EditProfilePopulator
{
	@Resource
	UserService userService;
	private static final Logger LOG = Logger.getLogger(EditProfilePopulatorImpl.class);

	/* populating customer data to the form for club card user changes starts */
	@Override
	public EditClubCardForm populateClubCardAccountForm(final CustomerData customer, final Model model,
			final EditClubCardForm editClubCardForm)
	{
		int isSAResident = 0;
		final Calendar cal = Calendar.getInstance();
		if (null != customer.getMemberID())
		{
			editClubCardForm.setMemberID(customer.getMemberID());
		}
		if (null != customer.getFirstName())
		{
			editClubCardForm.setFirstName(getTitleCase(customer.getFirstName()));
		}
		if (null != customer.getLastName())
		{
			editClubCardForm.setLastName(getTitleCase(customer.getLastName()));
		}
		if (null != customer.getUid())
		{
			editClubCardForm.seteMailAddress(getTitleCase(customer.getUid()));
		}
		if (null != customer.getPreferedName())
		{
			editClubCardForm.setPreferedName(getTitleCase(customer.getPreferedName()));
		}
		if (null != customer.getSAResident())
		{
			isSAResident = 1;
			if (customer.getSAResident().booleanValue())
			{
				editClubCardForm.setIsSAResident(isSAResident);
			}
			else
			{
				editClubCardForm.setIsSAResident(0);
			}
			if (null != customer.getIDnumber() && editClubCardForm.getIsSAResident() == 1)
			{
				editClubCardForm.setIDNumber(customer.getIDnumber());
			}
			if (null != customer.getNonRSA_DOB())
			{
				editClubCardForm.setNonRSA_DOB(String.valueOf(customer.getNonRSA_DOB().getTime()));
				cal.setTime(customer.getNonRSA_DOB());
				final int day = cal.get(Calendar.DAY_OF_MONTH);
				editClubCardForm.setSA_DOB_day(String.valueOf(day));
				final int year = cal.get(Calendar.YEAR);
				editClubCardForm.setSA_DOB_year(String.valueOf(year));
				final int month = cal.get(Calendar.MONTH);
				editClubCardForm.setSA_DOB_month(String.valueOf(month + 1));
			}
		}


		if (null != customer.getGender())
		{
			if (customer.getGender().equalsIgnoreCase("MALE"))
			{
				editClubCardForm.setGender(1);
			}
			else if (customer.getGender().equalsIgnoreCase("FEMALE"))
			{
				editClubCardForm.setGender(2);
			}
		}
		try
		{
			if (null != customer.getAddresses() & customer.getAddresses().size() > 0)
			{
				for (final AddressData address : customer.getAddresses())
				{
					if (null != address.getLine1())
					{
						editClubCardForm.setAddressLine1(getTitleCase(address.getLine1()));
					}
					if (null != address.getLine2())
					{
						editClubCardForm.setAddressLine2(getTitleCase(address.getLine2()));
					}
					if (null != address.getCity())
					{
						final String town = address.getCity();
						final String[] city = town.split(",");
						try
						{
							editClubCardForm.setCity(getTitleCase(city[0]));
						}
						catch (final ArrayIndexOutOfBoundsException indexExc)
						{
							LOG.error(indexExc);
						}
					}
					if (null != address.getCountry() && null != address.getCountry().getIsocode())
					{
						editClubCardForm.setCountry(address.getCountry().getIsocode());
					}
					if (null != address.getPostalCode())
					{
						editClubCardForm.setPostalCode(address.getPostalCode());
					}
					if (null != address.getProvince())
					{
						editClubCardForm.setProvince(address.getProvince());
					}
					if (null != address.getSuburb())
					{
						editClubCardForm.setSuburb(getTitleCase(address.getSuburb()));
					}
				}
			}

		}
		catch (final Exception addrex)
		{
			LOG.error("error while saving address" + addrex);
		}
		if (null != customer.getContactDetails())
		{
			for (final ContactDetail contact : customer.getContactDetails())
			{
				if (null != contact.getTypeID() && contact.getTypeID().equalsIgnoreCase("2"))
				{
					editClubCardForm.setContactNumber(contact.getNumber());
				}
				if (null != contact.getTypeID() && contact.getTypeID().equalsIgnoreCase("4"))
				{
					editClubCardForm.setAlternateNumber(contact.getNumber());
				}
			}
		}
		editClubCardForm.setMarketingEmail(true);
		editClubCardForm.setAccinfoEmail(true);
		if (null != customer.getMarketingConsent_email())
		{
			editClubCardForm.setMarketingEmail(customer.getMarketingConsent_email().booleanValue());
		}

		if (null != customer.getMarketingConsent_SMS())
		{
			editClubCardForm.setMarketingSMS(customer.getMarketingConsent_SMS().booleanValue());
		}
		if (null != customer.getAccinfo_email())
		{
			editClubCardForm.setAccinfoEmail(customer.getAccinfo_email().booleanValue());
		}
		if (null != customer.getAccinfo_SMS())
		{
			editClubCardForm.setAccinfoSMS(customer.getAccinfo_SMS().booleanValue());
		}
		return editClubCardForm;
		/* populating customer data to the form for club card user changes ends */
	}

	/**
	 * @param customer
	 * @param customerModel
	 */
	private CustomerData populateCustomerAddress(final CustomerData customerData, final CustomerModel customerModel)
	{
		final List<AddressData> addresses = new ArrayList<AddressData>();

		if (CollectionUtils.isNotEmpty(customerModel.getAddresses()))
		{
			for (final AddressModel addressModel : customerModel.getAddresses())
			{
				if ((Boolean.FALSE.equals(addressModel.getShippingAddress()))
						&& (Boolean.FALSE.equals(addressModel.getBillingAddress())))
				{

					final AddressData address = new AddressData();

					if (null != addressModel.getBuilding())
					{
						address.setLine1(getTitleCase(addressModel.getBuilding()));
					}
					if (null != addressModel.getStreetname())
					{
						address.setLine2(getTitleCase(addressModel.getStreetname()));
					}
					if (null != addressModel.getTypeID())
					{
						address.setTypeID(addressModel.getTypeID());
					}
					if (null != addressModel.getStreetnumber())
					{
						address.setSuburb(getTitleCase(addressModel.getStreetnumber()));
					}
					if (null != addressModel.getTown())
					{
						address.setCity(getTitleCase(addressModel.getTown()));
					}

					if (null != addressModel.getCountry())
					{
						final CountryData countryData = new CountryData();
						countryData.setIsocode(addressModel.getCountry().getIsocode());
						address.setCountry(countryData);
					}
					if (null != addressModel.getProvince())
					{
						address.setProvince(addressModel.getProvince());
					}
					if (null != addressModel.getPostalcode())
					{
						address.setPostalCode(addressModel.getPostalcode());
					}
					//		if (null != addressModel.getProvince())
					//		{
					//			address.setProvince(ccForm.getProvince());
					//		}

					addresses.add(address);
				}
			}
		}
		if (addresses.size() > 0)
		{
			customerData.setAddresses(addresses);
		}
		return customerData;
	}

	/* populating customer data to the form for non club card user changes starts */
	@Override
	public EditNonClubCardForm populateNonClubCardData(final CustomerData customer, final Model model,
			final EditNonClubCardForm nonCCForm)
	{

		int isSAResident = 0;
		final Calendar cal = Calendar.getInstance();
		if (null != customer.getFirstName())
		{
			nonCCForm.setFirstName(getTitleCase(customer.getFirstName()));
		}
		if (null != customer.getLastName())
		{
			nonCCForm.setLastName(getTitleCase(customer.getLastName()));
		}
		if (null != customer.getUid())
		{
			nonCCForm.seteMailAddress(getTitleCase(customer.getUid()));
		}
		if (null != customer.getPreferedName())
		{
			nonCCForm.setPreferedName(getTitleCase(customer.getPreferedName()));
		}
		if (null != customer.getSAResident())
		{
			isSAResident = 1;
			if (customer.getSAResident().booleanValue())
			{
				nonCCForm.setIsSAResident(isSAResident);
			}
			else if (!customer.getSAResident().booleanValue())
			{
				nonCCForm.setIsSAResident(0);
			}

			if (null != customer.getIDnumber() && nonCCForm.getIsSAResident() == 1)
			{
				nonCCForm.setIDNumber(customer.getIDnumber());
			}
			if (null != customer.getNonRSA_DOB())
			{
				nonCCForm.setSA_DOB(String.valueOf(customer.getNonRSA_DOB().getTime()));
				cal.setTime(customer.getNonRSA_DOB());
				final int day = cal.get(Calendar.DAY_OF_MONTH);
				nonCCForm.setSA_DOB_day(String.valueOf(day));
				final int year = cal.get(Calendar.YEAR);
				nonCCForm.setSA_DOB_year(String.valueOf(year));
				final int month = cal.get(Calendar.MONTH) + 1;
				nonCCForm.setSA_DOB_month(String.valueOf(month));
			}
		}
		return nonCCForm;
	}

	/* populating customer data to the form for non club card user changes ends */


	@Override
	public CustomerData populateClubCardCustomerData(final EditClubCardForm ccForm, final CustomerData customerData,
			final CustomerData currentCustomerData)
	{
		if (null != currentCustomerData.getCustomerID())
		{
			customerData.setCustomerID(currentCustomerData.getCustomerID());
		}
		if (null != ccForm.getFirstName())
		{
			customerData.setFirstName(getTitleCase(ccForm.getFirstName()));
		}
		if (null != ccForm.getLastName())
		{
			customerData.setLastName(getTitleCase(ccForm.getLastName()));
		}
		if (null != ccForm.geteMailAddress())
		{
			customerData.setEmailID(getTitleCase(ccForm.geteMailAddress()));
		}
		/*
		 * if (null != customerData.getEmailID()) { customerData.setUid(getTitleCase(customerData.getEmailID())); }
		 */
		if (null != ccForm.getPreferedName() && !StringUtils.isEmpty(ccForm.getPreferedName()))
		{
			customerData.setPreferedName(getTitleCase(ccForm.getPreferedName()));
		}
		else if (ccForm.getPreferedName() == null || StringUtils.isEmpty(ccForm.getPreferedName()))
		{
			if (null != currentCustomerData.getPreferedName() || !StringUtils.isEmpty(currentCustomerData.getPreferedName()))
			{
				customerData.setPreferedName(getTitleCase(currentCustomerData.getPreferedName()));
			}
			else if (null != customerData.getFirstName() && !StringUtils.isEmpty(customerData.getFirstName()))
			{
				customerData.setPreferedName(getTitleCase(customerData.getFirstName()));
			}
		}
		if (ccForm.getIsSAResident() == 1)
		{
			customerData.setSAResident(Boolean.TRUE);
			if (StringUtils.isNotBlank(ccForm.getIDNumber()))
			{
				customerData.setIDnumber(ccForm.getIDNumber());
			}
		}
		else
		{
			customerData.setSAResident(Boolean.FALSE);
		}
		if (null != ccForm.getSA_DOB_day() && null != ccForm.getSA_DOB_month() && null != ccForm.getSA_DOB_year())
		{
			final String date = ccForm.getSA_DOB_month() + "/" + ccForm.getSA_DOB_day() + "/" + ccForm.getSA_DOB_year();
			customerData.setNonRSA_DOB(new Date(date));
		}
		customerData.setGender(new Integer(ccForm.getGender()).toString());

		final List<AddressData> addresses = new ArrayList<AddressData>();
		final AddressData address = new AddressData();

		if (null != ccForm.getAddressLine1())
		{
			address.setLine1(getTitleCase(ccForm.getAddressLine1()));
		}
		if (null != ccForm.getAddressLine2())
		{
			address.setLine2(getTitleCase(ccForm.getAddressLine2()));
		}
		if (null != ccForm.getAddressTypeId())
		{
			address.setTypeID(ccForm.getAddressTypeId());
		}
		if (null != ccForm.getSuburb())
		{
			address.setSuburb(getTitleCase(ccForm.getSuburb()));
		}
		if (null != ccForm.getCity())
		{
			address.setCity(getTitleCase(ccForm.getCity()));
		}

		if (null != ccForm.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setIsocode(ccForm.getCountry());
			address.setCountry(countryData);
		}

		if (null != ccForm.getPostalCode())
		{
			address.setPostalCode(ccForm.getPostalCode());
		}
		if (null != ccForm.getProvince())
		{
			address.setProvince(ccForm.getProvince());
		}

		addresses.add(address);
		if (addresses.size() > 0)
		{
			customerData.setAddresses(addresses);
		}
		if (ccForm.isMarketingEmail())
		{
			customerData.setMarketingConsent_email(new Boolean(ccForm.isMarketingEmail()));
		}
		if (ccForm.isMarketingSMS())
		{
			customerData.setMarketingConsent_SMS(new Boolean(ccForm.isMarketingSMS()));
		}
		if (ccForm.isAccinfoEmail())
		{
			customerData.setAccinfo_email(new Boolean(ccForm.isAccinfoEmail()));
		}
		if (ccForm.isAccinfoSMS())
		{
			customerData.setAccinfo_SMS(new Boolean(ccForm.isAccinfoSMS()));
		}

		if (StringUtils.isNotBlank(ccForm.getContactNumber()) || StringUtils.isNotBlank(ccForm.getAlternateNumber()))
		{
			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
			if (StringUtils.isNotBlank(ccForm.getContactNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(ccForm.getContactNumber());
				contact.setTypeID("2");
				contactDetailList.add(contact);
			}
			if (StringUtils.isNotBlank(ccForm.getAlternateNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(ccForm.getAlternateNumber());
				contact.setTypeID("4");
				contactDetailList.add(contact);
			}
			if (contactDetailList.size() > 0)
			{
				customerData.setContactDetails(contactDetailList);
			}
		}
		if (null != currentCustomerData.getUid())
		{
			customerData.setUid(currentCustomerData.getUid());
		}
		if (null != currentCustomerData.getDisplayUid())
		{
			customerData.setDisplayUid(currentCustomerData.getDisplayUid());
		}
		if (null != currentCustomerData.getMemberID())
		{
			customerData.setMemberID(currentCustomerData.getMemberID());
		}

		if (null != currentCustomerData.getBabyClub())
		{
			customerData.setBabyClub(currentCustomerData.getBabyClub());
		}
		return customerData;
	}


	@Override
	public CustomerData populateNonClubCardCustomerData(final EditNonClubCardForm nonCCForm, final CustomerData customerData,
			final CustomerData currentCustomerData)
	{
		if (null != currentCustomerData.getCustomerID())
		{
			customerData.setCustomerID(currentCustomerData.getCustomerID());
		}

		if (null != nonCCForm.getFirstName())
		{
			customerData.setFirstName(getTitleCase(nonCCForm.getFirstName()));
		}
		if (null != nonCCForm.getLastName())
		{
			customerData.setLastName(getTitleCase(nonCCForm.getLastName()));
		}
		if (null != nonCCForm.geteMailAddress())
		{
			customerData.setEmailID(getTitleCase(nonCCForm.geteMailAddress()));
		}

		//		if (null != customerData.getEmailID())
		//		{
		//			customerData.setUid(getTitleCase(customerData.getEmailID()));
		//		}

		/*
		 * if (null != customerData.getEmailID()) { customerData.setDisplayUid(getTitleCase(customerData.getEmailID())); }
		 */
		if (null != nonCCForm.getPreferedName() && !StringUtils.isEmpty(nonCCForm.getPreferedName()))
		{
			customerData.setPreferedName(getTitleCase(nonCCForm.getPreferedName()));
		}
		else if (nonCCForm.getPreferedName() == null || StringUtils.isEmpty(nonCCForm.getPreferedName()))
		{
			if (null != currentCustomerData.getPreferedName() || !StringUtils.isEmpty(currentCustomerData.getPreferedName()))
			{
				customerData.setPreferedName(getTitleCase(currentCustomerData.getPreferedName()));
			}
			else if (null != customerData.getFirstName() && !StringUtils.isEmpty(customerData.getFirstName()))
			{
				customerData.setPreferedName(getTitleCase(customerData.getFirstName()));
			}
		}
		if (nonCCForm.getIsSAResident() == 1)
		{
			customerData.setSAResident(Boolean.TRUE);
			if (null != nonCCForm.getIDNumber())
			{
				customerData.setIDnumber(nonCCForm.getIDNumber());
			}
		}
		else
		{
			customerData.setSAResident(Boolean.FALSE);
			if (null != nonCCForm.getSA_DOB_day() && null != nonCCForm.getSA_DOB_month() && null != nonCCForm.getSA_DOB_year())
			{
				final String date = nonCCForm.getSA_DOB_month() + "/" + nonCCForm.getSA_DOB_day() + "/" + nonCCForm.getSA_DOB_year();
				customerData.setNonRSA_DOB(new Date(date));
			}
		}
		return customerData;
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.clicks.storefront.edit.profile.processor.EditProfilePopulator#populateClubCardAccountData(de.hybris.
	 * platform.commercefacades.user.data.CustomerData, de.hybris.platform.core.model.user.CustomerModel)
	 */
	@Override
	public CustomerData populateClubCardAccountData(CustomerData customer, final CustomerModel customerModel)
	{
		if (null != customerModel && null != customerModel.getCustomerID())
		{
			customer.setCustomerID(customerModel.getCustomerID());
		}
		if (null != populateCustomerAddress(customer, customerModel))
		{
			customer = populateCustomerAddress(customer, customerModel);
		}
		return customer;
	}
}
