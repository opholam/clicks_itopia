/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;



import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * @author Swapnil Desai
 * 
 */
@Controller
@Scope("tenant")
public class NewsLetterController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(NewsLetterController.class);


	/**
	 * Gets the newsletter response.
	 * 
	 * @param redirectModel
	 *           the redirect model
	 * @param request
	 *           the request
	 * @return the newsletter response
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@SuppressWarnings("boxing")
	@RequestMapping(value = "/newsletter", method = RequestMethod.GET)
	public String getNewsletterResponse(final RedirectAttributes redirectModel, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		final String status = request.getParameter(ControllerConstants.Actions.NewsLetter.Newsletter_Request_Parameter);
		LOG.info("Newsletter Response Controller : " + status);
		String referer = request.getHeader("referer");
		if (StringUtils.isEmpty(referer) || StringUtils.isBlank(referer) || "null".equalsIgnoreCase(referer))
		{
			referer = "/";
		}
		LOG.info("Referrer is : " + referer);
		if (StringUtils.isNotBlank(status))
		{
			if ("true".equals(status))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						ControllerConstants.Actions.NewsLetter.Newsletter_Success);
			}
			else if ("error".equals(status))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						ControllerConstants.Actions.NewsLetter.Newsletter_Error);
			}
			else if ("pending".equals(status))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						ControllerConstants.Actions.NewsLetter.Newsletter_Pending);
			}
			else if ("exists".equals(status))
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						ControllerConstants.Actions.NewsLetter.Newsletter_Exists);
			}
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					ControllerConstants.Actions.NewsLetter.Newsletter_Generic_Error);
		}

		return REDIRECT_PREFIX + referer;
	}
}
