/**
 *
 */
package de.hybris.clicks.storefront.store.populator;

import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.storefront.forms.PickUpStoreForm;
import de.hybris.platform.clicksstorefrontcommons.forms.AddressForm;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;



/**
 * @author shruti.jhamb
 *
 */
public class ClicksDeliveryAddressPopulator
{

	@Resource(name = "clicksCheckoutFacade")
	ClicksCheckoutFacade clicksCheckoutFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	/*
	 * Populating pick up in store form from address data
	 */
	public PickUpStoreForm populatePickUpFormAddress(final AddressData address, final PickUpStoreForm pickUpStoreForm)
	{
		if (null != address)
		{
			if (null != address.getLine1())
			{
				pickUpStoreForm.setLine1(address.getLine1());
			}
			if (null != address.getLine2())
			{
				pickUpStoreForm.setLine2(address.getLine2());
			}
			if (null != address.getCountry() && null != address.getCountry().getName())
			{
				pickUpStoreForm.setCountryName(address.getCountry().getName());
			}
			if (null != address.getSuburb())
			{
				pickUpStoreForm.setSuburb(address.getSuburb());
			}
			if (null != address.getProvince())
			{
				pickUpStoreForm.setProvince(address.getProvince());
			}
			if (null != address.getDepartment())
			{
				pickUpStoreForm.setDepartment(address.getDepartment());
			}
			if (null != address.getApartment())
			{
				pickUpStoreForm.setApartment(address.getApartment());
			}
			pickUpStoreForm.setIsDeliverable(Boolean.TRUE);
			if (null != address.getPostalCode())
			{
				pickUpStoreForm.setPostcode(address.getPostalCode());
			}
			pickUpStoreForm.setStoreAddress(true);
		}

		return pickUpStoreForm;


	}

	/**
	 * @param pos
	 * @param pickUpStoreForm
	 * @return
	 */

	/*
	 * Populating form from point of service model
	 */
	public PickUpStoreForm populatePickUpForm(final PointOfServiceModel pos, final PickUpStoreForm pickUpStoreForm)
	{
		if (null != pos.getAddress())
		{
			if (null != pos.getAddress().getLine1())
			{
				pickUpStoreForm.setLine1(pos.getAddress().getLine1());
			}
			if (null != pos.getAddress().getBuilding())
			{
				pickUpStoreForm.setLine2(pos.getAddress().getBuilding());
			}
			if (null != pos.getAddress().getCountry() && null != pos.getAddress().getCountry().getName())
			{
				pickUpStoreForm.setCountryName(pos.getAddress().getCountry().getName());
			}
			if (null != pos.getAddress().getSuburb())
			{
				pickUpStoreForm.setSuburb(pos.getAddress().getSuburb());
			}
			else if (StringUtils.isNotEmpty(pos.getAddress().getLine2()))
			{
				pickUpStoreForm.setSuburb(pos.getAddress().getLine2());
			}
			if (null != pos.getAddress().getTown())
			{
				pickUpStoreForm.setTown(pos.getAddress().getTown());
			}
			if (null != pos.getAddress().getProvince())
			{
				pickUpStoreForm.setProvince(pos.getAddress().getProvince());
			}
			if (null != pos.getAddress().getDepartment())
			{
				pickUpStoreForm.setDepartment(pos.getAddress().getDepartment());
			}
			if (null != pos.getAddress().getAppartment())
			{
				pickUpStoreForm.setApartment(pos.getAddress().getAppartment());
			}
			if (null != pos.getAddress().getRegion() && null != pos.getAddress().getRegion().getIsocode())
			{
				pickUpStoreForm.setRegionIso(pos.getAddress().getRegion().getIsocode());
			}
			pickUpStoreForm.setIsDeliverable(Boolean.TRUE);
			if (null != pos.getAddress().getPostalcode())
			{
				pickUpStoreForm.setPostcode(pos.getAddress().getPostalcode());
			}
			if (null != pos.getAddress().getPk())
			{
				pickUpStoreForm.setAddressId(pos.getAddress().getPk().toString());
			}
			if (null != pos.getDisplayName())
			{
				pickUpStoreForm.setStoreDisplayName(pos.getDisplayName());
			}
			pickUpStoreForm.setStoreAddress(true);
		}

		return pickUpStoreForm;


	}

	/**
	 * @param model
	 * @param pickUpStoreForm
	 * @param newAddress
	 * @return
	 */
	/*
	 * populating address data from pickup in store form
	 */
	public AddressData populateStoreDelAddress(final Model model, final PickUpStoreForm pickUpStoreForm,
			final AddressData newAddress)
	{
		if (null != pickUpStoreForm.getStoreDisplayName())
		{
			newAddress.setCompanyName(pickUpStoreForm.getStoreDisplayName());
		}
		if (null != pickUpStoreForm.getPosName())
		{
			newAddress.setStoreId(pickUpStoreForm.getPosName());
		}
		if (null != pickUpStoreForm.getLine1())
		{
			newAddress.setLine1(pickUpStoreForm.getLine1());
		}
		if (null != pickUpStoreForm.getLine2())
		{
			newAddress.setLine2(pickUpStoreForm.getLine2());
		}
		if (null != pickUpStoreForm.getTown())
		{
			newAddress.setTown(pickUpStoreForm.getTown());
		}
		if (null != pickUpStoreForm.getPostcode())
		{
			newAddress.setPostalCode(pickUpStoreForm.getPostcode());
		}
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);

		// start - added for personal details
		//newAddress.setTitleCode("mr");
		if (null != pickUpStoreForm.getEmail())
		{
			newAddress.setEmail(pickUpStoreForm.getEmail());
		}
		if (null != pickUpStoreForm.getDepartment())
		{
			newAddress.setDepartment(pickUpStoreForm.getDepartment());
		}
		if (null != pickUpStoreForm.getApartment())
		{
			newAddress.setApartment(pickUpStoreForm.getApartment());
		}
		if (null != pickUpStoreForm.getSuburb())
		{
			newAddress.setSuburb(pickUpStoreForm.getSuburb());
		}
		if (null != pickUpStoreForm.getProvince())
		{
			newAddress.setProvince(pickUpStoreForm.getProvince());
		}
		if (null != pickUpStoreForm.getPreferredContactMethod())
		{
			// 0- email , 1- sms
			newAddress.setPreferredContactMethod(pickUpStoreForm.getPreferredContactMethod());
		}
		if (null != pickUpStoreForm.getPhone())
		{
			newAddress.setPhone(pickUpStoreForm.getPhone());
		}
		if (null != pickUpStoreForm.getAlternatePhone())
		{
			newAddress.setAlternatePhone(pickUpStoreForm.getAlternatePhone());
		}
		// end - added for personal details

		// set delivery method
		if (StringUtils.isNotEmpty(pickUpStoreForm.getSelectedDeliveryMode()))
		{
			clicksCheckoutFacade.setDeliveryMode(pickUpStoreForm.getSelectedDeliveryMode());
		}
		// end - set delivery method

		if (pickUpStoreForm.getCountryIso() != null && StringUtils.isAlpha(pickUpStoreForm.getCountryIso()))
		{
			final CountryData countryData = i18NFacade.getCountryForIsocode(pickUpStoreForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (pickUpStoreForm.getRegionIso() != null && !StringUtils.isEmpty(pickUpStoreForm.getRegionIso()))
		{
			final RegionData regionData = i18NFacade.getRegion(pickUpStoreForm.getCountryIso(), pickUpStoreForm.getRegionIso());
			newAddress.setRegion(regionData);
		}
		newAddress.setVisibleInAddressBook(false);
		newAddress.setIsStoreAddress(Boolean.TRUE);
		newAddress.setShippingAddress(true);
		newAddress.setBillingAddress(false);
		return newAddress;
	}

	/**
	 * @param newAddress
	 * @param addressForm
	 * @return
	 */
	/*
	 * populating address data from address form
	 */
	public AddressData populateAddressData(final AddressData newAddress, final AddressForm addressForm)
	{
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setIsStoreAddress(Boolean.FALSE);

		// start - added for personal details
		newAddress.setTitleCode("mr");
		if (null != addressForm.getEmail())
		{
			newAddress.setEmail(addressForm.getEmail());
		}
		if (null != addressForm.getSuburb())
		{
			newAddress.setSuburb(addressForm.getSuburb());
		}
		if (null != addressForm.getProvince())
		{
			newAddress.setProvince(addressForm.getProvince());
		}
		if (null != addressForm.getPreferredContactMethod())
		{
			// 0- email , 1- sms
			newAddress.setPreferredContactMethod(addressForm.getPreferredContactMethod());
		}
		if (null != addressForm.getPhone())
		{
			newAddress.setPhone(addressForm.getPhone());
		}
		if (null != addressForm.getAlternatePhone())
		{
			newAddress.setAlternatePhone(addressForm.getAlternatePhone());
		}
		if (null != addressForm.getDeliveryInstructions())
		{
			newAddress.setDeliveryInstructions(addressForm.getDeliveryInstructions());
		}
		// end - added for personal details

		// set delivery method
		//if (StringUtils.isNotEmpty(addressForm.getSelectedDeliveryMode()))
		//{
		//clicksCheckoutFacade.setDeliveryMode(addressForm.getSelectedDeliveryMode());
		//}
		// end - set delivery method

		if (addressForm.getCountryIso() != null && StringUtils.isAlpha(addressForm.getCountryIso()))
		{
			final CountryData countryData = i18NFacade.getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = i18NFacade.getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			newAddress.setRegion(regionData);
		}
		return newAddress;

	}

	/**
	 * @param addressForm
	 * @param cartData
	 * @return
	 */
	public AddressForm populateAddressForm(final AddressForm addressForm, final CartData cartData)
	{

		addressForm.setIsDeliverable(new Boolean(true));
		addressForm.setPreferredContactMethod(Boolean.FALSE);
		if (!checkoutCustomerStrategy.isAnonymousCheckout())
		{
			final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
			addressForm.setEmail(currentCustomerData.getUid());
			addressForm.setFirstName(currentCustomerData.getFirstName());
			addressForm.setLastName(currentCustomerData.getLastName());
			if (CollectionUtils.isNotEmpty(currentCustomerData.getContactDetails()))
			{
				for (final ContactDetail contactDetail : currentCustomerData.getContactDetails())
				{
					if (("2").equals(contactDetail.getTypeID()))
					{
						addressForm.setPhone(contactDetail.getNumber());
					}
					if (("4").equals(contactDetail.getTypeID()))
					{
						addressForm.setAlternatePhone(contactDetail.getNumber());
					}
				}
			}
		}
		addressForm.setSelectedDeliveryMode(cartData.getDeliveryMode() != null ? cartData.getDeliveryMode().getCode() : null);
		return addressForm;
	}

	/**
	 * @param pickUpStoreForm
	 * @param cartData
	 * @return
	 */
	public PickUpStoreForm populatePickUpInStoreForm(final PickUpStoreForm pickUpStoreForm, final CartData cartData)
	{

		pickUpStoreForm.setIsDeliverable(Boolean.TRUE);
		pickUpStoreForm.setPreferredContactMethod(Boolean.FALSE);
		if (!checkoutCustomerStrategy.isAnonymousCheckout())
		{
			final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
			if (null != cartData.getDeliveryAddress() && null != cartData.getDeliveryAddress().getEmail())
			{
				pickUpStoreForm.setEmail(cartData.getDeliveryAddress().getEmail());
			}
			else
			{
				pickUpStoreForm.setEmail(currentCustomerData.getUid());
			}
			if (null != cartData.getDeliveryAddress() && null != cartData.getDeliveryAddress().getCompanyName())
			{
				pickUpStoreForm.setStoreDisplayName(cartData.getDeliveryAddress().getCompanyName());
			}
			//addressForm.setLastName(currentCustomerData.getLastName());
			if (CollectionUtils.isNotEmpty(currentCustomerData.getContactDetails()))
			{
				for (final ContactDetail contactDetail : currentCustomerData.getContactDetails())
				{
					if (("2").equals(contactDetail.getTypeID()))
					{
						pickUpStoreForm.setPhone(contactDetail.getNumber());
					}
					if (("4").equals(contactDetail.getTypeID()))
					{
						pickUpStoreForm.setAlternatePhone(contactDetail.getNumber());
					}
				}
			}
		}
		pickUpStoreForm.setSelectedDeliveryMode(cartData.getDeliveryMode() != null ? cartData.getDeliveryMode().getCode() : null);
		return pickUpStoreForm;

	}

	/**
	 * @param pickUpStoreForm
	 * @param addressData
	 * @return
	 */
	public PickUpStoreForm populateInstoreForm(final PickUpStoreForm pickUpStoreForm, final AddressData addressData)
	{
		pickUpStoreForm.setAddressId(addressData.getId());
		pickUpStoreForm.setStoreAddress(addressData.getIsStoreAddress().booleanValue());
		pickUpStoreForm.setStoreDisplayName(addressData.getCompanyName());
		pickUpStoreForm.setQ(addressData.getPostalCode());
		if (null != addressData.getDepartment())
		{
			pickUpStoreForm.setDepartment(addressData.getDepartment());
		}
		if (null != addressData.getApartment())
		{
			pickUpStoreForm.setApartment(addressData.getApartment());
		}
		pickUpStoreForm.setPosName(addressData.getStoreId());
		pickUpStoreForm.setLine1(addressData.getLine1());
		pickUpStoreForm.setLine2(addressData.getLine2());
		pickUpStoreForm.setTown(addressData.getTown());
		pickUpStoreForm.setPostcode(addressData.getPostalCode());
		pickUpStoreForm.setCountryName(addressData.getCountry().getName());
		pickUpStoreForm.setEmail(addressData.getEmail());
		pickUpStoreForm.setSuburb(addressData.getSuburb());
		pickUpStoreForm.setProvince(addressData.getProvince());
		pickUpStoreForm.setPhone(addressData.getPhone());
		pickUpStoreForm.setAlternatePhone(addressData.getAlternatePhone());
		pickUpStoreForm.setPreferredContactMethod(addressData.getPreferredContactMethod());
		if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
		{
			pickUpStoreForm.setRegionIso(addressData.getRegion().getIsocode());
		}
		return pickUpStoreForm;
	}

	/**
	 * @param addressForm
	 * @param addressData
	 * @return
	 */
	public AddressForm editPopulateAddressForm(final AddressForm addressForm, final AddressData addressData)
	{
		addressForm.setAddressId(addressData.getId());
		addressForm.setTitleCode(addressData.getTitleCode());
		addressForm.setFirstName(addressData.getFirstName());
		addressForm.setLastName(addressData.getLastName());
		addressForm.setLine1(addressData.getLine1());
		addressForm.setLine2(addressData.getLine2());
		addressForm.setTownCity(addressData.getTown());
		addressForm.setPostcode(addressData.getPostalCode());
		addressForm.setCountryIso(addressData.getCountry().getIsocode());
		addressForm.setSaveInAddressBook(Boolean.valueOf(addressData.isVisibleInAddressBook()));
		addressForm.setShippingAddress(Boolean.valueOf(addressData.isShippingAddress()));
		addressForm.setBillingAddress(Boolean.valueOf(addressData.isBillingAddress()));
		addressForm.setEmail(addressData.getEmail());
		addressForm.setDeliveryInstructions(clicksCheckoutFacade.getDeliveryInstructionFromCart());
		addressForm.setCountryName(addressData.getCountry().getName());
		addressForm.setSuburb(addressData.getSuburb());
		addressForm.setProvince(addressData.getProvince());
		addressForm.setPhone(addressData.getPhone());
		addressForm.setAlternatePhone(addressData.getAlternatePhone());
		addressForm.setPreferredContactMethod(addressData.getPreferredContactMethod());
		return addressForm;
	}

	/**
	 * @param newAddress
	 * @param addressForm
	 * @return
	 */
	public AddressData editPopulateAddressData(final AddressData newAddress, final AddressForm addressForm)
	{
		newAddress.setId(addressForm.getAddressId());
		newAddress.setTitleCode(addressForm.getTitleCode());
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		// start - added for personal details
		newAddress.setTitleCode("mr");
		if (null != addressForm.getEmail())
		{
			newAddress.setEmail(addressForm.getEmail());
		}
		if (null != addressForm.getSuburb())
		{
			newAddress.setSuburb(addressForm.getSuburb());
		}
		if (null != addressForm.getProvince())
		{
			newAddress.setProvince(addressForm.getProvince());
		}
		if (null != addressForm.getPreferredContactMethod())
		{
			// 0- email , 1- sms
			newAddress.setPreferredContactMethod(addressForm.getPreferredContactMethod());
		}
		if (null != addressForm.getPhone())
		{
			newAddress.setPhone(addressForm.getPhone());
		}
		if (null != addressForm.getAlternatePhone())
		{
			newAddress.setAlternatePhone(addressForm.getAlternatePhone());
		}
		if (null != addressForm.getDeliveryInstructions())
		{
			newAddress.setDeliveryInstructions(addressForm.getDeliveryInstructions());
		}
		// end - added for personal details

		// set delivery method
		if (StringUtils.isNotEmpty(addressForm.getSelectedDeliveryMode()))
		{
			clicksCheckoutFacade.setDeliveryMode(addressForm.getSelectedDeliveryMode());
		}
		// end - set delivery method
		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = i18NFacade.getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = i18NFacade.getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			newAddress.setRegion(regionData);
		}

		if (addressForm.getSaveInAddressBook() == null)
		{
			newAddress.setVisibleInAddressBook(true);
		}
		else
		{
			newAddress.setVisibleInAddressBook(Boolean.TRUE.equals(addressForm.getSaveInAddressBook()));
		}
		return newAddress;
	}


}
