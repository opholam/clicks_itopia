/**
 *
 */
package de.hybris.clicks.storefront.bookAppointment.processor;

import de.hybris.clicks.storefront.forms.ClinicBookingForm;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;


/**
 * @author shruti.jhamb
 *
 */

public class DefaultClinicBookingProcessor implements ClinicBookingProcessor
{

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.clicks.storefront.bookAppointment.processor.ClinicBookingProcessor#processsBookingRequest(de.hybris.
	 * clicks.storefront.forms.ClinicBookingForm, org.springframework.validation.BindingResult,
	 * org.springframework.ui.Model, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ClinicBookingData populateDataProcess(final ClinicBookingForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response)
	{
		ClinicBookingData bookingData = new ClinicBookingData();
		bookingData = populateClinicBooking(form, bookingData);
		return bookingData;
	}

	@SuppressWarnings("boxing")
	private ClinicBookingData populateClinicBooking(final ClinicBookingForm source, final ClinicBookingData target)
	{
		if (null != source.getFirstName())
		{
			target.setFirstName(source.getFirstName());
		}
		if (null != source.getLastName())
		{
			target.setLastName(source.getLastName());
		}
		if (null != source.getContactNumber())
		{
			target.setContactNumber(source.getContactNumber());
		}
		//target.setContactNumber(source.getContactNumber());

		if (null != source.getRegion())
		{
			target.setRegion(source.getRegion());
		}
		if (null != source.getPreferredClinic())
		{
			target.setPreferredClinic(source.getPreferredClinic());
		}
		if (null != source.getAlternativeClinic())
		{
			target.setAlternativeClinic(source.getAlternativeClinic());
		}
		return target;
	}

}
