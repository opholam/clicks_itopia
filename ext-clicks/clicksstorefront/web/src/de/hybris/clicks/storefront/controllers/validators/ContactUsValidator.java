/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.clicks.storefront.forms.ContactUsForm;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author ashish.vyas
 *
 *         Validate ContactUsForm
 */

public class ContactUsValidator implements Validator
{
	//public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
	//public static final String MOBILE_REGEX = "^[-+]?[0-9]*";
	public static final String MOBILE_REGEX = "\\d[0-9]*";

	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.CUSTOM_NAME_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;
	private boolean flag;

	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;

	public boolean isFlag()
	{
		return flag;
	}

	public void setFlag(final boolean flag)
	{
		this.flag = flag;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ContactUsForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ContactUsForm contactUsForm = (ContactUsForm) object;
		final String firstName = contactUsForm.getFirstName();
		final String lastName = contactUsForm.getLastName();
		final String email = contactUsForm.getEmail();
		final String contactNo = contactUsForm.getContactNo();
		final String message = contactUsForm.getMessage();
		final String topic = contactUsForm.getTopic();

		//final ContactUsValidator contactUsValidator = new ContactUsValidator();

		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			errors.rejectValue("firstName", "register.CC.firstname.empty");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			errors.rejectValue("lastName", "register.CC.lastname.empty");
		}
		else if (StringUtils.length(lastName) > 30)
		{
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}

		if (!ValidatorUtils.validateRegex(EMAIL_REGEX, email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}

		if (StringUtils.isEmpty(contactNo))
		{
			errors.rejectValue("contactNo", "register.CC.contactNo.empty");
		}
		else if (!ValidatorUtils.validateRegex(NUMBER_REGEX, contactNo))
		{
			errors.rejectValue("contactNo", "register.CC.contactNo.contains.alphabet");
		}

		if (StringUtils.isEmpty(topic))
		{
			errors.rejectValue("topic", "register.CC.topic.empty");
		}

		if (StringUtils.isEmpty(message))
		{
			errors.rejectValue("message", "register.CC.message.empty");
		}
		if (StringUtils.length(message) > 255)
		{
			errors.rejectValue("message", "register.CC.message.length");
		}
	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
