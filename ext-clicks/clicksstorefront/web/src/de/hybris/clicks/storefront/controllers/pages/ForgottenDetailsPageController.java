/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.forgottendetails.ForgottenDetailsFacade;
import de.hybris.clicks.storefront.controllers.validators.ForgottenDetailsFormValidator;
import de.hybris.clicks.storefront.forms.ForgottenDetailsForm;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.core.model.user.CustomerModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * @author ashish.vyas
 */
@Controller
@Scope("tenant")
public class ForgottenDetailsPageController extends AbstractPageController
{
	private static final String FORGOTTEN_DETAILS_PAGE = "forgottenDetails";
	private static final String REDIRECT_TO_FORGOTTEN_DETAILS_PAGE = REDIRECT_PREFIX + "/forgotten-details";

	@Resource(name = "forgottenDetailsFormValidator")
	private ForgottenDetailsFormValidator forgottenDetailsFormValidator;

	@Resource(name = "forgottenDetailsFacade")
	private ForgottenDetailsFacade forgottenDetailsFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	/**
	 * fetch the forgotten details form for customer.
	 *
	 * @param model
	 *           the model
	 * @return the forgotten details page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/forgotten-details", method = RequestMethod.GET)
	public String getForgottenDetailsPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("forgottenDetailsForm", new ForgottenDetailsForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
		return getViewForPage(model);

	}

	/**
	 * fetching the details for customer with the inputs provided by customer in forgotten details form.
	 *
	 * @param forgottenDetailsForm
	 *           the forgotten details form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the forgotten details
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * If the customer has forgotten all the details required to log in , the details can be retrieved using club card
	 * number or SA ID
	 */
	@RequestMapping(value = "/forgotten-details", method = RequestMethod.POST)
	public String getForgottenDetails(@ModelAttribute("forgottenDetailsForm") final ForgottenDetailsForm forgottenDetailsForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		//validating the details entered by customer
		forgottenDetailsFormValidator.validate(forgottenDetailsForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			model.addAttribute("forgottenDetailsForm", forgottenDetailsForm);
			storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.forgotdetails.error",
					null);
			return getViewForPage(model);
		}

		final String clubcardNumber = forgottenDetailsForm.getClubCardNumber();
		final boolean isResident = forgottenDetailsForm.isResident();
		CustomerModel customer = null;

		String residentNumber = null;
		String stringDate = null;
		if (isResident)
		{
			residentNumber = forgottenDetailsForm.getResidentNumber();
			customer = forgottenDetailsFacade.checkClubcardResidentExists(clubcardNumber, residentNumber);
		}
		else
		{
			final String day = forgottenDetailsForm.getDay();
			final String month = forgottenDetailsForm.getMonth();
			final String year = forgottenDetailsForm.getYear();

			final Calendar calendar = Calendar.getInstance();
			calendar.clear();
			calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
			calendar.set(Calendar.MONTH, Integer.parseInt(month));
			calendar.set(Calendar.YEAR, Integer.parseInt(year));
			final Date date = calendar.getTime();
			stringDate = convertDate(date);
			customer = forgottenDetailsFacade.checkClubcardDOBExists(clubcardNumber, date);
		}
		if (customer != null)
		{
			final String emailId = customer.getUid();
			if (null != emailId)
			{
				customerFacade.forgottenPassword(emailId);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"forgot.details.success.message", new Object[]
						{ emailId });
			}
		}
		else
		{
			CustomerModel residentCustomer = null;
			if (StringUtils.isNotEmpty(residentNumber) && StringUtils.isNotEmpty(clubcardNumber))
			{
				residentCustomer = forgottenDetailsFacade.checkResidentExists(clubcardNumber, residentNumber);
			}
			if (residentCustomer != null)
			{
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "forgot.details.saresident.notfound.message",
						new Object[]
						{ residentNumber, clubcardNumber });
			}

			else
			{
				if (isResident)
				{
					GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "forgot.details.resident.notfound.message",
							new Object[]
							{ clubcardNumber, residentNumber });
				}
				else
				{
					GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "forgot.details.dob.notfound.message",
							new Object[]
							{ clubcardNumber, stringDate });
				}
			}
			model.addAttribute("forgottenDetailsForm", forgottenDetailsForm);
			storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
			return getViewForPage(model);
		}

		return REDIRECT_TO_FORGOTTEN_DETAILS_PAGE;
	}


	public String convertDate(final Date date)
	{
		final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");
		return DATE_FORMAT.format(date);
	}

	public String sendForgottenDetails(final ForgottenDetailsForm forgottenDetailsForm, final BindingResult bindingResult,
			final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("forgottenDetailsForm", forgottenDetailsForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FORGOTTEN_DETAILS_PAGE));
		model.addAttribute(forgottenDetailsForm);
		return getViewForPage(model);
	}
}
