/**
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.core.model.MedicineSupplementListComponentModel;
import de.hybris.clicks.facade.hpcondition.HpConditionFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author Swapnil Desai
 *
 */
//TODO : Need to remove this class as it is of no use.
@Controller("MedicineSupplementListComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.MedicineSupplementListComponent)
public class MedicineSupplementListComponentController<T extends AbstractItemModel> extends
		AbstractCMSComponentController<MedicineSupplementListComponentModel>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.storefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.
	 * HttpServletRequest, org.springframework.ui.Model,
	 * de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
	 */
	private static Logger log = Logger.getLogger(MedicineSupplementListComponentController.class.getName());
	@Resource(name = "hpConditionFacade")
	private HpConditionFacade hpConditionFacade;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final MedicineSupplementListComponentModel component)
	{
		final String id = request.getParameter("id");
		final String type = request.getParameter("type");
		log.info("Entered into MedicineSupplementListComponentController id: " + id + " type: " + type);
		if (StringUtils.isNotBlank(id))
		{
			//			final HMedicinesVSModel hMedicinesModel = hpConditionFacade.getMedicinesAndSupplements(id, type);
			//			prepareMap(hMedicinesModel.getRelatedMedicinesSupplements(), model);
			//			model.addAttribute("glossaryList", hMedicinesModel.getRelatedGlossary());
		}
		else if (StringUtils.isNotBlank(type))
		{
			//			final List<ConditionsModel> hMedicinesModelList = hpConditionFacade.findAllMedicinesAndVSByType(type);
			//			prepareMap(hMedicines  ModelList, model);
			//			model.addAttribute("glossaryList", hMedicinesModel.getRelatedGlossary());
		}
	}

	@Override
	protected String getView(final MedicineSupplementListComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix
				+ StringUtils.lowerCase(MedicineSupplementListComponentModel._TYPECODE);
	}

	@SuppressWarnings("boxing")
	private void prepareMap(final Collection<ConditionsModel> condList, final Model model)
	{
		final List<ConditionsModel> sortedList = new ArrayList<ConditionsModel>(condList);
		log.info("+++++++++++++++++++Preparing map for Condition List Page+++++++++++++++++++");
		final String str = "ABCDEFGHIJKLM";
		final LinkedHashMap firstTabMap = new LinkedHashMap();
		final LinkedHashMap secondTabMap = new LinkedHashMap();
		List<ConditionsModel> firstTabList = null;
		List<ConditionsModel> secondTabList = null;

		Collections.sort(sortedList, new Comparator<ConditionsModel>()
		{

			@Override
			public int compare(final ConditionsModel o1, final ConditionsModel o2)
			{
				return o1.getConditionName().compareTo(o2.getConditionName());
			}
		});

		for (final ConditionsModel obj : sortedList)
		{
			if (null != obj.getStartingAlpha() && !obj.getStartingAlpha().isEmpty())
			{
				if (str.contains(obj.getStartingAlpha().toUpperCase()))
				{
					if (null == firstTabMap.get(obj.getStartingAlpha().toUpperCase()))
					{
						firstTabList = new ArrayList<ConditionsModel>();
						firstTabList.add(obj);
						firstTabMap.put(obj.getStartingAlpha().toUpperCase(), firstTabList);
					}
					else
					{
						final List<ConditionsModel> existingList = (List<ConditionsModel>) firstTabMap.get(obj.getStartingAlpha()
								.toUpperCase());
						existingList.add(obj);
						firstTabMap.put(obj.getStartingAlpha().toUpperCase(), existingList);
					}

				}

				else
				{
					if (null == secondTabMap.get(obj.getStartingAlpha().toUpperCase()))
					{
						secondTabList = new ArrayList<ConditionsModel>();
						secondTabList.add(obj);
						secondTabMap.put(obj.getStartingAlpha().toUpperCase(), secondTabList);
					}
					else
					{
						final List<ConditionsModel> existingList = (List<ConditionsModel>) secondTabMap.get(obj.getStartingAlpha()
								.toUpperCase());
						existingList.add(obj);
						secondTabMap.put(obj.getStartingAlpha().toUpperCase(), existingList);
					}
				}
			}
		}
		model.addAttribute("firstTabMap", firstTabMap);
		model.addAttribute("secondTabMap", secondTabMap);
	}
}
