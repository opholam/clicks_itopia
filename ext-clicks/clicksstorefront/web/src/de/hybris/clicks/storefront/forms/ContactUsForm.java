/**
 *
 */
package de.hybris.clicks.storefront.forms;




/**
 * @author shruti.jhamb
 *
 */
public class ContactUsForm
{
	//@NotBlank(message = "{traveldeal.contact.firstname.invalid}")
	private String firstName;

	//@NotBlank(message = "{traveldeal.contact.lastname.invalid}")
	private String lastName;

	//@NotBlank(message = "{traveldeal.contact.email.invalid}")
	//@Email(message = "{traveldeal.contact.email.format.invalid}")
	private String email;

	//@NotBlank(message = "{traveldeal.contact.contactno.invalid}")
	private String contactNo;

	//@NotBlank(message = "{traveldeal.contact.clubcardno.invalid}")
	//@Max(value = 13, message = "{traveldeal.contact.clubcardno.max.invalid}")
	private String clubcardNo;

	private String topic;

	private String message;

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the contactNo
	 */
	public String getContactNo()
	{
		return contactNo;
	}

	/**
	 * @param contactNo
	 *           the contactNo to set
	 */
	public void setContactNo(final String contactNo)
	{
		this.contactNo = contactNo;
	}

	/**
	 * @return the clubcardNo
	 */
	public String getClubcardNo()
	{
		return clubcardNo;
	}

	/**
	 * @param clubcardNo
	 *           the clubcardNo to set
	 */
	public void setClubcardNo(final String clubcardNo)
	{
		this.clubcardNo = clubcardNo;
	}

	/**
	 * @return the topic
	 */


	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * @return the topic
	 */
	public String getTopic()
	{
		return topic;
	}

	/**
	 * @param topic
	 *           the topic to set
	 */
	public void setTopic(final String topic)
	{
		this.topic = topic;
	}



}
