/**
 *
 */
package de.hybris.clicks.storefront.controllers.validators;

import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.clicks.storefront.forms.ChildForm;
import de.hybris.clicks.storefront.forms.JoinBabyClubForm;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author sridhar.reddy
 * 
 */
public class JoinBabyClubValidator implements Validator
{

	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.NAME_REGEX;

	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;

	@Override
	public boolean supports(final Class<?> aclass)
	{

		return JoinBabyClubForm.class.equals(aclass);
	}


	@Override
	public void validate(final Object object, final Errors errors)
	{
		final JoinBabyClubForm joinBabyClubForm = (JoinBabyClubForm) object;
		final List<ChildForm> childs = joinBabyClubForm.getChildren();

		if (childs != null && childs.size() > 0)
		{
			for (final ChildForm child : childs)
			{
				try
				{
					final String firstName = child.getFirstName();
					final String lastName = child.getLastName();
					final String date = child.getDate();

					if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
					{
						errors.rejectValue("firstName", "register.CC.firstname.empty");
					}
					else if (StringUtils.length(firstName) > 30)
					{
						errors.rejectValue("firstName", "register.CC.firstname.invalid");
					}
					if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
					{
						errors.rejectValue("lastName", "register.CC.lastname.empty");
					}
					else if (StringUtils.length(lastName) > 30)
					{
						errors.rejectValue("lastName", "register.CC.lastname.invalid");
					}

					if (StringUtils.isEmpty(date))
					{
						errors.rejectValue("date", "register.CC.DOB.empty");
					}
				}
				catch (final Exception e)
				{
					//
				}
			}
		}

	}

}
