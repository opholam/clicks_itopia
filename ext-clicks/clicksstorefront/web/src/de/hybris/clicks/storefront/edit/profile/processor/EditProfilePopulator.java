/**
 *
 */
package de.hybris.clicks.storefront.edit.profile.processor;

import de.hybris.clicks.storefront.forms.EditClubCardForm;
import de.hybris.clicks.storefront.forms.EditNonClubCardForm;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;

import org.springframework.ui.Model;


/**
 * @author shruti.jhamb
 *
 */
public interface EditProfilePopulator
{
	//populate club card user details
	public EditClubCardForm populateClubCardAccountForm(final CustomerData customer, final Model model,
			final EditClubCardForm editClubCardForm);

	//populate club card user details
	public CustomerData populateClubCardAccountData(final CustomerData customer, final CustomerModel customerModel);

	//populate non club card user details
	public EditNonClubCardForm populateNonClubCardData(final CustomerData customer, final Model model,
			final EditNonClubCardForm editNonClubCardForm);

	//save club card customer data
	public CustomerData populateClubCardCustomerData(final EditClubCardForm ccForm, final CustomerData customerData,
			final CustomerData currentCustomerData);

	//save non club card customer data
	public CustomerData populateNonClubCardCustomerData(final EditNonClubCardForm nonCCForm, final CustomerData customerData,
			final CustomerData currentCustomerData);

}
