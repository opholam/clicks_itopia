/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.model.BrandLandinPageModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.FormatFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author praveen.babu
 *
 */
@Controller
@Scope("tenant")
@RequestMapping("/brand")
public class BrandPageController extends DefaultPageController
{


	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "formatFactory")
	protected FormatFactory formatFactory;

	private static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/{brandLabel:.*}";

	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
			ProductOption.DESCRIPTION, ProductOption.PROMOTIONS, ProductOption.STOCK);

	//
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String account(@PathVariable("brandLabel") final String brandLabel, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException,
			CMSItemNotFoundException
	{
		final BrandLandinPageModel pageForRequest = (BrandLandinPageModel) getContentPageForRequest(request);
		final List<ProductData> bestSellersProducts = new ArrayList<ProductData>();
		final List<ProductData> rangeProducts = new ArrayList<ProductData>();

		for (final ProductModel productModel : pageForRequest.getBestSellerProdList())
		{
			final ProductData pd = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);






			/*
			 * if (productModel.getPromotions() != null && productModel.getPromotions().size() > 0) {
			 * getDiscountPromotionFormat(productModel, pd); }
			 */


			pd.setRemarks(productModel.getRemarks());


			bestSellersProducts.add(pd);

		}


		for (final ProductModel productModel : pageForRequest.getBrandRange())
		{
			final ProductData pd = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);

			/*
			 * if (productModel.getPromotions() != null && productModel.getPromotions().size() > 0) {
			 * getDiscountPromotionFormat(productModel, pd); }
			 */

			rangeProducts.add(pd);

		}

		model.addAttribute("bestSellersProducts", bestSellersProducts);
		model.addAttribute("rangeProducts", rangeProducts);

		storeCmsPageInModel(model, pageForRequest);
		setUpMetaDataForContentPage(model, pageForRequest);
		//model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);


	}

	/*
	 * void getDiscountPromotionFormat(final ProductModel productModel, final ProductData pd) {
	 *
	 * String formattedValue = null; Double grossPriceWithPromotionApplied = null;
	 *
	 * for (final ProductPromotionModel productPromotionModel : productModel.getPromotions()) { if
	 * (!(productPromotionModel instanceof ProductBOGOFPromotionModel)) { if
	 * (Boolean.TRUE.equals(productPromotionModel.getEnabled())) { if
	 * (GrossPriceUtil.applyRestrictions(productPromotionModel)) {
	 *
	 * grossPriceWithPromotionApplied = GrossPriceUtil.getGrossPrice(productModel, true); formattedValue =
	 * formatFactory.createCurrencyFormat().format(grossPriceWithPromotionApplied);
	 * pd.getPrice().setGrossPriceValueWithPromotionAppliedFormattedValue(formattedValue);
	 * pd.getPrice().setGrossPriceWithPromotionApplied(grossPriceWithPromotionApplied); //final
	 * ProductPercentageDiscountPromotionModel discountPromo = (ProductPercentageDiscountPromotionModel)
	 * productPromotionModel; //pd.setPromoBadge("Save " +
	 * String.valueOf(discountPromo.getPercentageDiscount().intValue()) + "%");
	 *
	 *
	 *
	 * } }
	 *
	 *
	 * } else { pd.setPromoBadge(productPromotionModel.getTitle()); }
	 *
	 * break;
	 *
	 *
	 *
	 *
	 *
	 *
	 * }
	 *
	 * }
	 */
}
