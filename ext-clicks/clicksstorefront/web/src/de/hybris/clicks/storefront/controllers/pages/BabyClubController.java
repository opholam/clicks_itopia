/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.babyclub.BabyClubCardFacade;
import de.hybris.clicks.storefront.controllers.validators.JoinBabyClubValidator;
import de.hybris.clicks.storefront.forms.ChildForm;
import de.hybris.clicks.storefront.forms.JoinBabyClubForm;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.BabyClubData;
import de.hybris.platform.commercefacades.user.data.ChildData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



/**
 * BabyClubController for managing or joining baby club
 *
 */
@Controller
@Scope("tenant")
public class BabyClubController extends AbstractLoginPageController
{
	private static final Logger LOG = Logger.getLogger(BabyClubController.class);

	private static final String REDIRECT_TO_JOIN_BABY_LOGIN_PAGE = REDIRECT_PREFIX + "/login/baby";
	private static final String REDIRECT_TO_JOIN_BABY_PAGE = REDIRECT_PREFIX + "/baby/my-account/join-babyclub";
	private static final String REDIRECT_TO_BABY_CLUB_LANDING_PAGE = REDIRECT_PREFIX + "/baby/baby-club";
	private static final String JOIN_BABY_CLUB_LOGIN_PAGE = "joinBabyClubLoginPage";
	private static final String BABY_CLUB_LANDING_PAGE = "babyClubLandingPage";

	private static final String JOIN_BABY_CLUB_PAGE = "joinBabyClubPage";
	private static final String JOIN_REGISTER_PAGE = REDIRECT_PREFIX + "/register/joincc";

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "joinBabyClubValidator")
	JoinBabyClubValidator joinBabyClubValidator;

	@Resource(name = "babyClubCardFacade")
	BabyClubCardFacade babyClubCardFacade;

	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	/**
	 * @return the httpSessionRequestCache
	 */
	public HttpSessionRequestCache getHttpSessionRequestCache()
	{
		return httpSessionRequestCache;
	}

	/**
	 * @param httpSessionRequestCache
	 *           the httpSessionRequestCache to set
	 */
	public void setHttpSessionRequestCache(final HttpSessionRequestCache httpSessionRequestCache)
	{
		this.httpSessionRequestCache = httpSessionRequestCache;
	}


	/**
	 * Gets the join baby login page.
	 *
	 * @param model
	 *           the model
	 * @return the join baby login page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/login/baby", method = RequestMethod.GET)
	public String getJoinBabyLoginPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("loginForm", new LoginForm());
		//model.addAttribute("joinBabyClubForm", new JoinBabyClubForm());

		storeCmsPageInModel(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_LOGIN_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_LOGIN_PAGE));
		return getViewForPage(model);
	}

	/**
	 * Gets the join baby club page.
	 *
	 * @param model
	 *           the model
	 * @return the join baby club page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/baby/my-account/join-babyclub", method = RequestMethod.GET)
	public String getJoinBabyClubPage(final Model model) throws CMSItemNotFoundException
	{
		if (userFacade.isAnonymousUser())
		{
			return REDIRECT_TO_JOIN_BABY_LOGIN_PAGE;
		}
		else
		{
			final CustomerData customer = customerFacade.getCurrentCustomer();
			final String menberID = customer.getMemberID();

			if (StringUtils.isNotEmpty(menberID))
			{
				final CustomerData customerData = babyClubCardFacade.populateBabyClubForm();
				JoinBabyClubForm joinBabyClubForm = new JoinBabyClubForm();
				if (null != customerData && null != customerData.getBabyClub())
				{
					model.addAttribute("isUpdate", "yes");
					model.addAttribute(WebConstants.BREADCRUMBS_KEY,
							resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.updateBabyClub"));
					joinBabyClubForm = populateBabyClubForm(customerData);
					joinBabyClubForm.setAlreadyBabyClubcardMember(true);
				}
				else
				{
					model.addAttribute(WebConstants.BREADCRUMBS_KEY,
							resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.joinBabyClub"));
					model.addAttribute("isUpdate", "no");
				}
				//joinBabyClubForm.setPregnancyStatus("yes");
				model.addAttribute("joinBabyClubForm", joinBabyClubForm);

				storeCmsPageInModel(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_PAGE));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_PAGE));

				return getViewForPage(model);
			}
			else
			{
				return JOIN_REGISTER_PAGE;
			}
		}

	}

	/**
	 * Gets the baby club page.
	 *
	 * @param model
	 *           the model
	 * @return the baby club page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/baby/baby-club", method = RequestMethod.GET)
	public String getBabyClubPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(BABY_CLUB_LANDING_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(BABY_CLUB_LANDING_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.babyClub"));

		if (!userFacade.isAnonymousUser() && StringUtils.isNotEmpty(customerFacade.getCurrentCustomer().getMemberID()))
		{
			model.addAttribute("isBabyClubCard", true);
		}
		return getViewForPage(model);
	}

	/**
	 * Join baby club.
	 *
	 * @param joinBabyClubForm
	 *           the join baby club form
	 * @param model
	 *           the model
	 * @param bindingResult
	 *           the binding result
	 * @param redirectAttrs
	 *           the redirect attrs
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/baby/my-account/join-babyclub", method = RequestMethod.POST)
	public String joinBabyClub(@ModelAttribute("joinBabyClubForm") final JoinBabyClubForm joinBabyClubForm, final Model model,
			final BindingResult bindingResult, final RedirectAttributes redirectAttrs) throws CMSItemNotFoundException
	{
		joinBabyClubValidator.validate(joinBabyClubForm, bindingResult);
		final String alreadyPregnantVal = Config.getParameter("already.pregnant.boolean.value");
		final String[] alreadyPregnantList = alreadyPregnantVal.split(",");
		if (bindingResult.hasErrors() && joinBabyClubForm.getPregnancyStatus().equals(alreadyPregnantList[1]))
		{
			GlobalMessages.addErrorMessage(model, "text.joinbaby.errorMsg");
			return showForm(model);
		}
		else if (StringUtils.isEmpty(joinBabyClubForm.getEstimatedDeliveryDate())
				&& joinBabyClubForm.getPregnancyStatus().equals(alreadyPregnantList[0]))
		{
			GlobalMessages.addErrorMessage(model, "text.joinbaby.date");
			bindingResult.rejectValue("estimatedDeliveryDate", "text.joinbaby.date");
			return showForm(model);
		}

		final CustomerData customerData = new CustomerData();
		final BabyClubData babyClubData = new BabyClubData();

		if (null != joinBabyClubForm.getEstimatedDeliveryDate()
				&& !alreadyPregnantList[1].equals(joinBabyClubForm.getPregnancyStatus()))
		{
			final boolean alreadyPregnant = true;

			babyClubData.setIsAlreadyPregnant(Boolean.valueOf(alreadyPregnant));
			babyClubData.setBabyClubDueDate(joinBabyClubForm.getEstimatedDeliveryDate());
			babyClubData.setGender(joinBabyClubForm.getExpectedGender());
		}
		else if (null != joinBabyClubForm.getChildren() && joinBabyClubForm.getChildren().size() > 0)
		{
			final boolean alreadyParent = true;
			ChildData child = null;
			final List<ChildData> children = new ArrayList<ChildData>();

			if (!CollectionUtils.isEmpty(joinBabyClubForm.getChildren()))
			{
				for (final ChildForm childForm : joinBabyClubForm.getChildren())
				{
					//String dateAttr = request.getAttribute("date").toString();
					//childForm.setDate(dateAttr);
					child = new ChildData();
					child.setFirstName(childForm.getFirstName());
					child.setLastName(childForm.getLastName());
					child.setGender(childForm.getGender());
					//if (StringUtils.isNotEmpty(childForm.getMonth()) && StringUtils.isNotEmpty(childForm.getDay())
					//	&& StringUtils.isNotEmpty(childForm.getYear()))
					//{
					if (StringUtils.isNotEmpty(childForm.getDate()))
					{
						//final DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
						//final DateTime date = formatter.parseDateTime(childForm.getDate());
						//final String childDate = date.toString();
						child.setChildDOB(childForm.getDate());
						child.setChildDOB2(new Date(childForm.getDate()));
					}
					//	}

					children.add(child);
				}
			}
			babyClubData.setChildren(children);
			babyClubData.setIsAlreadyParent(Boolean.valueOf(alreadyParent));

		}
		try
		{
			customerData.setBabyClub(babyClubData);
			babyClubCardFacade.joinBabyClub(customerData);
			if (joinBabyClubForm.isAlreadyBabyClubcardMember())
			{
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER,
						"form.babyclubcard.alreadyMember.success");
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "form.babyclubcard.success");
			}
			if (null != customerData.getBabyClub())
			{
				model.addAttribute(WebConstants.BREADCRUMBS_KEY,
						resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.updateBabyClub"));
			}
			else
			{
				model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.joinBabyClub"));
			}

			return REDIRECT_TO_BABY_CLUB_LANDING_PAGE;
		}
		catch (final Exception e)
		{
			if (null != e && StringUtils.containsIgnoreCase(e.getMessage(), "OMNIUPD"))
			{
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER, "customer.edit.profile.failed");
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.babyclubcard.failed");
			}
			LOG.error(e.getMessage());
			if (null != customerData.getBabyClub())
			{
				model.addAttribute(WebConstants.BREADCRUMBS_KEY,
						resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.updateBabyClub"));
			}
			else
			{
				model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.joinBabyClub"));
			}
		}
		return REDIRECT_TO_JOIN_BABY_PAGE;
	}

	/*
	 * @RequestMapping(value = "/my-account/join-babyclub", method = RequestMethod.GET) public String
	 * joinBabyClubPage(@ModelAttribute("joinBabyClubForm") final JoinBabyClubForm joinBabyClubForm, final Model model)
	 * throws CMSItemNotFoundException { storeCmsPageInModel(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_PAGE));
	 * setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_PAGE)); return getViewForPage(model);
	 * }
	 */
	public String showForm(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JOIN_BABY_CLUB_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.joinBabyClub"));
		return getViewForPage(model);
	}


	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(JOIN_BABY_CLUB_LOGIN_PAGE);
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return REDIRECT_TO_JOIN_BABY_PAGE;
	}

	@Override
	protected String getView()
	{
		return "pages/layout/babyClubLoginPage";
	}

	/**
	 * Populate baby club form.
	 *
	 * @param customerData
	 *           the customer data
	 * @return the join baby club form
	 */
	JoinBabyClubForm populateBabyClubForm(final CustomerData customerData)
	{
		final JoinBabyClubForm joinBabyClubForm = new JoinBabyClubForm();
		final String property = Config.getParameter("baby.club.gender");
		final String[] genderList = property.split(",");
		if (null != customerData && null != customerData.getBabyClub())
		{
			final BabyClubData babyClubData = customerData.getBabyClub();
			if (null != babyClubData.getBabyClubDueDate())
			{
				joinBabyClubForm.setEstimatedDeliveryDate(babyClubData.getBabyClubDueDate());
			}
			if (null != babyClubData.getGender())
			{
				if (babyClubData.getGender().equalsIgnoreCase(genderList[0]) || babyClubData.getGender().equalsIgnoreCase("male"))
				{
					final String gender = getMessageSource().getMessage("text.gender.boy", null, getI18nService().getCurrentLocale());
					joinBabyClubForm.setGender(gender);
					joinBabyClubForm.setExpectedGender(gender);
				}
				else if (babyClubData.getGender().equalsIgnoreCase(genderList[1])
						|| babyClubData.getGender().equalsIgnoreCase("female"))
				{
					final String gender = getMessageSource().getMessage("text.gender.girl", null, getI18nService().getCurrentLocale());
					joinBabyClubForm.setGender(gender);
					joinBabyClubForm.setExpectedGender(gender);
				}
				else if (babyClubData.getGender().equalsIgnoreCase("unknown"))
				{
					joinBabyClubForm.setGender("Unkown");
					joinBabyClubForm.setExpectedGender("Unkown");
				}
			}
			final String alreadyPregnant = Config.getParameter("already.pregnant.boolean.value");
			final String[] valueList = alreadyPregnant.split(",");
			if (null != babyClubData.getIsAlreadyPregnant() && babyClubData.getIsAlreadyPregnant().booleanValue())
			{
				joinBabyClubForm.setAlreadyPregnant(valueList[0]);
			}


			if (null != babyClubData.getChildren() && !CollectionUtils.isEmpty(babyClubData.getChildren()))
			{
				joinBabyClubForm.setPregnancyStatus(valueList[1]);
				final List<ChildForm> childFormList = new ArrayList<ChildForm>();

				for (final ChildData childData : babyClubData.getChildren())
				{
					final ChildForm childForm = new ChildForm();
					if (null != childData.getChildDOB())
					{
						try
						{
							final DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
							final DateTime date = formatter.parseDateTime(childData.getChildDOB());
							childForm.setDay(new Integer(date.getDayOfMonth()).toString());
							childForm.setMonth(new Integer(date.getMonthOfYear()).toString());
							childForm.setYear(new Integer(date.getYear()).toString());
							childForm.setDate(childData.getChildDOB());
						}
						catch (final Exception e)
						{
							//
						}
					}
					if (StringUtils.isEmpty(childForm.getYear()) && StringUtils.isNotBlank(childData.getChildDOB()))
					{
						try
						{
							String[] dob = null;
							if (childData.getChildDOB().contains("-"))
							{
								dob = StringUtils.split(childData.getChildDOB(), "-");
							}
							else if (childData.getChildDOB().contains("/"))
							{
								dob = StringUtils.split(childData.getChildDOB(), "/");
							}
							if (null != dob)
							{
								childForm.setDay(dob.length > 2 ? dob[2] : "");
								childForm.setMonth(dob.length > 1 ? dob[1] : "");
								childForm.setYear(dob.length > 0 ? dob[0] : "");
								childForm.setDate(dob[1] + "/" + dob[2] + "/" + dob[0]);
							}
						}
						catch (final Exception e)
						{
							//
						}
					}
					if (null != childData.getFirstName() && StringUtils.isNotBlank(childData.getFirstName()))
					{
						childForm.setFirstName(childData.getFirstName());
					}
					if (null != childData.getGender() && StringUtils.isNotBlank(childData.getGender()))
					{
						//childForm.setGender(childData.getGender());
						if (childData.getGender().equalsIgnoreCase(genderList[0]))
						{
							childForm.setGender(getMessageSource().getMessage("text.gender.boy", null,
									getI18nService().getCurrentLocale()));
						}
						if (childData.getGender().equalsIgnoreCase(genderList[1]))
						{
							childForm.setGender(getMessageSource().getMessage("text.gender.girl", null,
									getI18nService().getCurrentLocale()));
						}
					}
					if (null != childData.getLastName() && StringUtils.isNotBlank(childData.getLastName()))
					{
						childForm.setLastName(childData.getLastName());
					}
					childFormList.add(childForm);
				}
				if (childFormList.size() > 0)
				{
					joinBabyClubForm.setChildren(childFormList);
				}
			}
			else
			{
				final List<ChildForm> childFormList = new ArrayList<ChildForm>();
				final ChildForm childForm = new ChildForm();
				childFormList.add(childForm);
				joinBabyClubForm.setChildren(childFormList);
			}
		}


		return joinBabyClubForm;
	}
}
