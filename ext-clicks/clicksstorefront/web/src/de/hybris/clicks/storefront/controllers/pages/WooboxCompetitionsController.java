package de.hybris.clicks.storefront.controllers.pages;


import de.hybris.clicks.core.model.WooBoxModel;
import de.hybris.clicks.core.woobox.facade.WooBoxFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@Scope("tenant")
@RequestMapping(value = "/competitions/view")
public class WooboxCompetitionsController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(WooboxCompetitionsController.class);
	private static final String WOOBOX_DETAIL_PAGE = "/{wooboxCode}";

	@Resource
	private WooBoxFacade wooBoxFacade;
	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	/**
	 * Gets the woo box competitions.
	 * 
	 * @param model
	 *           the model
	 * @return the woo box competitions
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getWooBoxCompetitions(final Model model) throws CMSItemNotFoundException
	{
		final String maxNoOfCompetitionsShown = (String) configurationService.getConfiguration().getProperty(
				"competitions.landingpage.show");
		LOG.debug("maxNoOfArticlesShown" + maxNoOfCompetitionsShown);
		final List<WooBoxModel> competitions = wooBoxFacade.getAllCompetitions();
		if (null != competitions && !competitions.isEmpty())
		{
			model.addAttribute("competitions", competitions);
			model.addAttribute("maxNoOfCompetitionsShown", maxNoOfCompetitionsShown);
			model.addAttribute("error", false);
		}
		else
		{
			model.addAttribute("error", true);
			LOG.debug("No Competitions to load");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId("competitionsLandingPage"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("competitionsLandingPage"));
		return ControllerConstants.Views.Pages.WooBox.CompetitionsLandingPage;
	}//getWooBoxCompetitions


	/**
	 * Load more woo box competitions.
	 * 
	 * @param pageSize
	 *           the page size
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/loadMore", method = RequestMethod.GET)
	public String loadMoreWooBoxCompetitions(@RequestParam("pageSize") final int pageSize, final Model model)
			throws CMSItemNotFoundException
	{
		final String maxNoOfCompetitionsShown = (String) configurationService.getConfiguration().getProperty(
				"competitions.landingpage.show");
		LOG.debug("maxNoOfArticlesShown" + maxNoOfCompetitionsShown);
		final List<WooBoxModel> competitions = wooBoxFacade.getAllCompetitions();
		if (null != competitions && !competitions.isEmpty())
		{

			model.addAttribute("competitions", competitions);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("maxNoOfCompetitionsShown", maxNoOfCompetitionsShown);
			model.addAttribute("error", false);
		}
		else
		{
			model.addAttribute("error", true);
			LOG.debug("No Competitions to load");
		}
		//storeCmsPageInModel(model, getContentPageForLabelOrId("competitionsLandingPage"));
		//setUpMetaDataForContentPage(model, getContentPageForLabelOrId("competitionsLandingPage"));
		return ControllerConstants.Views.Pages.WooBox.loadMoreCompetitions;
	}//loadMoreWooBoxCompetitions

	/**
	 * Gets the details for data offer.
	 * 
	 * @param wooboxCode
	 *           the woobox code
	 * @param model
	 *           the model
	 * @return the details for data offer
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = WOOBOX_DETAIL_PAGE, method = RequestMethod.GET)
	public String getDetailsForDataOffer(@PathVariable("wooboxCode") final String wooboxCode, final Model model)
			throws CMSItemNotFoundException
	{
		LOG.info("wooboxCode is " + wooboxCode);
		model.addAttribute("wooboxCode", wooboxCode);
		final WooBoxModel competition = wooBoxFacade.getCompetitionsByCode(wooboxCode);
		try
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("competitionsDetailPage"));
			if (null != competition)
			{
				model.addAttribute("competition", competition);
				updatePageTitle(model, competition.getTitle() + " | Clicks");
				setUpMetaDataForContentPage(model, competition.getTitle());
				if (null != competition.getEndDate())
				{
					if ((competition.getEndDate().after(Calendar.getInstance().getTime()))
							|| (competition.getEndDate().equals(Calendar.getInstance().getTime())))
					{
						model.addAttribute("competitionStatus", "Open");
						final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy");
						final String date = DATE_FORMAT.format(competition.getEndDate());
						model.addAttribute("competitionClosedDate", date);
					}
					else
					{
						model.addAttribute("competitionStatus", "Closed");
					}
				}
				LOG.info("value for dataoffer is : " + competition.getDataoffer());
			}
			//			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("competitionsDetailPage"));

		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.error("Error while loading data fro woobox code :" + wooboxCode + e);
		}
		final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.competitions");
		breadCrumbs.get(0).setUrl("/competitions/view");
		breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.competitionsDetail"));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
		return ControllerConstants.Views.Pages.WooBox.CompetitionsDetailPage;
	}

	/**
	 * @param model
	 * @param title
	 */
	private void updatePageTitle(final Model model, final String title)
	{
		model.addAttribute(CMS_PAGE_TITLE, title);

	}


	/**
	 * Sets the up meta data for content page.
	 * 
	 * @param model
	 *           the model
	 * @param title
	 *           the title
	 */
	private void setUpMetaDataForContentPage(final Model model, final String title)
	{
		final StringBuilder descriptionBuilder = new StringBuilder();
		descriptionBuilder.append("Enter the ");
		descriptionBuilder.append(title);
		descriptionBuilder.append(" at Clicks today!");
		setUpMetaData(model, "", descriptionBuilder.toString());
	}
}