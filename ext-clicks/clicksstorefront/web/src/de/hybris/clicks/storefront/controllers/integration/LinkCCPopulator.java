/**
 *
 */
package de.hybris.clicks.storefront.controllers.integration;

import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 * @author shruti.jhamb
 *
 */

public interface LinkCCPopulator
{
	//populating club card data to the account
	public boolean populateClubcardData(CustomerData customerData);

	// saving updated details to customer model
	public boolean saveUpdatedDetails(final CustomerData customer);
}
