/**
 *
 */
package de.hybris.clicks.storefront.forms;

import java.util.List;



/**
 * @author ashish.vyas
 *
 */
public class JoinBabyClubForm
{
	private List<ChildForm> children;

	//private String firstName;
	//private String lastName;
	private String gender;
	private String alreadyParent;
	private String alreadyPregnant;
	private String expectedGender;
	private String pregnancyStatus;
	private String estimatedDeliveryDate;
	private boolean alreadyBabyClubcardMember;
	private String date;

	/**
	 * @return the date
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * @param date
	 *           the date to set
	 */
	public void setDate(final String date)
	{
		this.date = date;
	}

	/**
	 * @return the children
	 */
	public List<ChildForm> getChildren()
	{
		return children;
	}

	/**
	 * @param childFormList
	 *           the children to set
	 */
	public void setChildren(final List<ChildForm> childFormList)
	{
		this.children = childFormList;
	}

	/**
	 * @return the gender
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * @param gender
	 *           the gender to set
	 */
	public void setGender(final String gender)
	{
		this.gender = gender;
	}

	/**
	 * @return the alreadyParent
	 */
	public String getAlreadyParent()
	{
		return alreadyParent;
	}

	/**
	 * @param alreadyParent
	 *           the alreadyParent to set
	 */
	public void setAlreadyParent(final String alreadyParent)
	{
		this.alreadyParent = alreadyParent;
	}

	/**
	 * @return the alreadyPregnant
	 */
	public String getAlreadyPregnant()
	{
		return alreadyPregnant;
	}

	/**
	 * @param alreadyPregnant
	 *           the alreadyPregnant to set
	 */
	public void setAlreadyPregnant(final String alreadyPregnant)
	{
		this.alreadyPregnant = alreadyPregnant;
	}

	/**
	 * @return the expectedGender
	 */
	public String getExpectedGender()
	{
		return expectedGender;
	}

	/**
	 * @param expectedGender
	 *           the expectedGender to set
	 */
	public void setExpectedGender(final String expectedGender)
	{
		this.expectedGender = expectedGender;
	}

	/**
	 * @return the pregnancyStatus
	 */
	public String getPregnancyStatus()
	{
		return pregnancyStatus;
	}

	/**
	 * @param pregnancyStatus
	 *           the pregnancyStatus to set
	 */
	public void setPregnancyStatus(final String pregnancyStatus)
	{
		this.pregnancyStatus = pregnancyStatus;
	}

	/**
	 * @return the estimatedDeliveryDate
	 */
	public String getEstimatedDeliveryDate()
	{
		return estimatedDeliveryDate;
	}

	/**
	 * @param estimatedDeliveryDate
	 *           the estimatedDeliveryDate to set
	 */
	public void setEstimatedDeliveryDate(final String estimatedDeliveryDate)
	{
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}

	/**
	 * @return the alreadyBabyClubcardMember
	 */
	public boolean isAlreadyBabyClubcardMember()
	{
		return alreadyBabyClubcardMember;
	}

	/**
	 * @param alreadyBabyClubcardMember
	 *           the alreadyBabyClubcardMember to set
	 */
	public void setAlreadyBabyClubcardMember(final boolean alreadyBabyClubcardMember)
	{
		this.alreadyBabyClubcardMember = alreadyBabyClubcardMember;
	}



}
