/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.util.Config;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates registration forms.
 */

public class RegistrationNonClubcardValidator implements Validator
{
	protected static final Logger LOG = Logger.getLogger(RegistrationNonClubcardValidator.class);
	public static final String ALPHA_NUMERIC_REGEX = FormPropertyConstants.Validator.Regex.ALPHA_NUMERIC_REGEX;
	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;
	public static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.CUSTOM_NAME_REGEX;
	private static final String SA_ID_REGEX = FormPropertyConstants.Validator.Regex.SA_ID_REGEX;

	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ClicksRegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ClicksRegisterForm registerNonCCForm = (ClicksRegisterForm) object;

		final String hybrisRetailerId = registerNonCCForm.getHybrisRetailerId();
		final String IDNumber = registerNonCCForm.getIDNumber();
		final int SAResident = registerNonCCForm.getIsSAResident();
		final String customerId = registerNonCCForm.getCustomerId();
		final int isClubcard = registerNonCCForm.getIsClubCard();
		final String clubcardNumber = registerNonCCForm.getMemberID();
		final String firstName = registerNonCCForm.getFirstName();
		final String preferedName = registerNonCCForm.getPreferedName();
		final String lastName = registerNonCCForm.getLastName();
		final String eMailAddress = registerNonCCForm.geteMailAddress();
		final int gender = registerNonCCForm.getGender();



		if (SAResident == 1)
		{
			if (!StringUtils.isEmpty(IDNumber))
			{
				if (StringUtils.length(IDNumber) != 13)
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
				if (Config.getBoolean("SA.ID.Validation", true) && !ValidatorUtils.validateRegex(SA_ID_REGEX, IDNumber))
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
			}
			if (StringUtils.isEmpty(IDNumber))
			{
				errors.rejectValue("IDNumber", "register.CC.IDNumber.empty");
			}
		}
		else
		{
			if (StringUtils.isEmpty(registerNonCCForm.getSA_DOB_day()))
			{
				errors.rejectValue("SA_DOB_day", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(registerNonCCForm.getSA_DOB_month()))
			{
				errors.rejectValue("SA_DOB_month", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(registerNonCCForm.getSA_DOB_year()))
			{
				errors.rejectValue("SA_DOB_year", "register.CC.DOB.empty");
			}
			if (!StringUtils.isEmpty(registerNonCCForm.getSA_DOB_day()) && !StringUtils.isEmpty(registerNonCCForm.getSA_DOB_month())
					&& !StringUtils.isEmpty(registerNonCCForm.getSA_DOB_year()))
			{
				final String date = registerNonCCForm.getSA_DOB_month() + "/" + registerNonCCForm.getSA_DOB_day() + "/"
						+ registerNonCCForm.getSA_DOB_year();
				try
				{
					final Date dobDate = new Date(date);
					final Date compareDate = new Date();
					compareDate.setYear(new Date().getYear() - 18);
					if (dobDate.after(compareDate))
					{
						// show error msg here -- age should be more than 18
						errors.rejectValue("SA_DOB_day", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_month", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_year", "register.CC.age.invalid");
					}
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
		}

		if (isClubcard == 1)
		{
			if (!ValidatorUtils.validateRegex(ALPHA_NUMERIC_REGEX, clubcardNumber))
			{
				errors.rejectValue("memberID", "register.CC.clubcard.empty");
			}
			if (!StringUtils.isEmpty(clubcardNumber))
			{
				if (clubcardNumber.length() > 13)
				{
					errors.rejectValue("memberID", "register.CC.clubcard.invalid");
				}
			}
		}
		else
		{

			if (StringUtils.isEmpty(registerNonCCForm.getConfirmPassword()))
			{
				errors.rejectValue("password", "register.CC.password.invalid");
			}
			if (StringUtils.isEmpty(registerNonCCForm.getPassword()))
			{
				errors.rejectValue("confirmPassword", "register.CC.password.invalid");
			}
			if (!StringUtils.isEmpty(registerNonCCForm.getPassword()))
			{
				if (registerNonCCForm.getPassword().length() < 6)
				{
					errors.rejectValue("password", "register.CC.password.length");
				}
			}
			if (!StringUtils.isEmpty(registerNonCCForm.getConfirmPassword())
					&& !StringUtils.isEmpty(registerNonCCForm.getPassword()))
			{
				if (!registerNonCCForm.getPassword().equalsIgnoreCase(registerNonCCForm.getConfirmPassword()))
				{
					errors.rejectValue("confirmPassword", "register.CC.password.misMatch");
				}
			}
			if (!ValidatorUtils.validateRegex(EMAIL_REGEX, eMailAddress))
			{
				errors.rejectValue("eMailAddress", "register.email.invalid");
			}
			else if (StringUtils.length(eMailAddress) > 50 || !validateEmailAddress(eMailAddress))
			{
				errors.rejectValue("eMailAddress", "register.email.invalid");
			}

			if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
			{
				errors.rejectValue("firstName", "register.CC.firstname.empty");
			}
			else if (StringUtils.length(firstName) > 30)
			{
				errors.rejectValue("firstName", "register.CC.firstname.invalid");
			}
			if (ValidatorUtils.validateRegex(NAME_REGEX, preferedName))
			{
				if (StringUtils.length(preferedName) > 50)
				{
					errors.rejectValue("preferedName", "register.CC.preferedname.invalid");
				}
			}
			if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
			{
				errors.rejectValue("lastName", "register.CC.lastname.empty");
			}
			else if (StringUtils.length(lastName) > 30)
			{
				errors.rejectValue("lastName", "register.CC.lastname.invalid");
			}

		}
	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

}
