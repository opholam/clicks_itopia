/**
 *
 */
package de.hybris.clicks.storefront.forms;


/**
 * @author shruti.jhamb
 *
 */
public class EditClubCardForm
{
	private String IDNumber;
	private int isSAResident = 1;
	private int emailId;
	private String ccNumber;
	private String userId;

	private String hybrisRetailerId;
	private String hybrisCustomerId;
	private String password;
	private String confirmPassword;

	private String loyaltyRetailerId;
	private String memberID;

	private String firstName;
	private String preferedName;
	private String lastName;
	private String eMailAddress;
	private int gender;

	private String SA_DOB_day;
	private String SA_DOB_month;
	private String SA_DOB_year;
	private String nonRSA_DOB;

	private String addressTypeId;
	private String addressLine1;

	private String addressLine2;
	private String suburb;
	private String city;
	private String province;
	private String country = "ZA";

	private String postalCode;

	private String alternateNumber;
	private String contactNumber;

	private boolean marketingEmail;
	private boolean marketingSMS;
	private boolean accinfoEmail;
	private boolean accinfoSMS;

	private String childFirstName;
	private String childSurname;
	private String childDOB;
	private int childGender;
	private String customerID;

	private boolean emailIDEditable;



	/**
	 * @return the isSAResident
	 */
	public int getIsSAResident()
	{
		return isSAResident;
	}

	/**
	 * @param isSAResident
	 *           the isSAResident to set
	 */
	public void setIsSAResident(final int isSAResident)
	{
		this.isSAResident = isSAResident;
	}

	/**
	 * @return the ccNumber
	 */
	public String getCcNumber()
	{
		return ccNumber;
	}

	/**
	 * @param ccNumber
	 *           the ccNumber to set
	 */
	public void setCcNumber(final String ccNumber)
	{
		this.ccNumber = ccNumber;
	}

	/**
	 * @return the accinfoEmail
	 */
	public boolean isAccinfoEmail()
	{
		return accinfoEmail;
	}

	/**
	 * @param accinfoEmail
	 *           the accinfoEmail to set
	 */
	public void setAccinfoEmail(final boolean accinfoEmail)
	{
		this.accinfoEmail = accinfoEmail;
	}

	/**
	 * @return the accinfoSMS
	 */
	public boolean isAccinfoSMS()
	{
		return accinfoSMS;
	}

	/**
	 * @param accinfoSMS
	 *           the accinfoSMS to set
	 */
	public void setAccinfoSMS(final boolean accinfoSMS)
	{
		this.accinfoSMS = accinfoSMS;
	}

	/**
	 * @return the sA_DOB_day
	 */
	public String getSA_DOB_day()
	{
		return SA_DOB_day;
	}

	/**
	 * @param sA_DOB_day
	 *           the sA_DOB_day to set
	 */
	public void setSA_DOB_day(final String sA_DOB_day)
	{
		SA_DOB_day = sA_DOB_day;
	}

	/**
	 * @return the sA_DOB_month
	 */
	public String getSA_DOB_month()
	{
		return SA_DOB_month;
	}

	/**
	 * @param sA_DOB_month
	 *           the sA_DOB_month to set
	 */
	public void setSA_DOB_month(final String sA_DOB_month)
	{
		SA_DOB_month = sA_DOB_month;
	}

	/**
	 * @return the sA_DOB_year
	 */
	public String getSA_DOB_year()
	{
		return SA_DOB_year;
	}

	/**
	 * @param sA_DOB_year
	 *           the sA_DOB_year to set
	 */
	public void setSA_DOB_year(final String sA_DOB_year)
	{
		SA_DOB_year = sA_DOB_year;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *           the password to set
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword()
	{
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *           the confirmPassword to set
	 */
	public void setConfirmPassword(final String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the preferedName
	 */
	public String getPreferedName()
	{
		return preferedName;
	}

	/**
	 * @param preferedName
	 *           the preferedName to set
	 */
	public void setPreferedName(final String preferedName)
	{
		this.preferedName = preferedName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the eMailAddress
	 */
	public String geteMailAddress()
	{
		return eMailAddress;
	}

	/**
	 * @param eMailAddress
	 *           the eMailAddress to set
	 */
	public void seteMailAddress(final String eMailAddress)
	{
		this.eMailAddress = eMailAddress;
	}

	/**
	 * @return the gender
	 */
	public int getGender()
	{
		return gender;
	}

	/**
	 * @param gender
	 *           the gender to set
	 */
	public void setGender(final int gender)
	{
		this.gender = gender;
	}

	/**
	 * @return the nonRSA_DOB
	 */
	public String getNonRSA_DOB()
	{
		return nonRSA_DOB;
	}

	/**
	 * @param nonRSA_DOB
	 *           the nonRSA_DOB to set
	 */
	public void setNonRSA_DOB(final String nonRSA_DOB)
	{
		this.nonRSA_DOB = nonRSA_DOB;
	}

	/**
	 * @return the addressTypeId
	 */
	public String getAddressTypeId()
	{
		return addressTypeId;
	}

	/**
	 * @param addressTypeId
	 *           the addressTypeId to set
	 */
	public void setAddressTypeId(final String addressTypeId)
	{
		this.addressTypeId = addressTypeId;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *           the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *           the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the suburb
	 */
	public String getSuburb()
	{
		return suburb;
	}

	/**
	 * @param suburb
	 *           the suburb to set
	 */
	public void setSuburb(final String suburb)
	{
		this.suburb = suburb;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the province
	 */
	public String getProvince()
	{
		return province;
	}

	/**
	 * @param province
	 *           the province to set
	 */
	public void setProvince(final String province)
	{
		this.province = province;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}



	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @return the alternateNumber
	 */
	public String getAlternateNumber()
	{
		return alternateNumber;
	}

	/**
	 * @param alternateNumber
	 *           the alternateNumber to set
	 */
	public void setAlternateNumber(final String alternateNumber)
	{
		this.alternateNumber = alternateNumber;
	}

	/**
	 * @param contactNumber
	 *           the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the marketingEmail
	 */
	public boolean isMarketingEmail()
	{
		return marketingEmail;
	}

	/**
	 * @param marketingEmail
	 *           the marketingEmail to set
	 */
	public void setMarketingEmail(final boolean marketingEmail)
	{
		this.marketingEmail = marketingEmail;
	}

	/**
	 * @return the marketingSMS
	 */
	public boolean isMarketingSMS()
	{
		return marketingSMS;
	}

	/**
	 * @param marketingSMS
	 *           the marketingSMS to set
	 */
	public void setMarketingSMS(final boolean marketingSMS)
	{
		this.marketingSMS = marketingSMS;
	}

	/**
	 * @return the childFirstName
	 */
	public String getChildFirstName()
	{
		return childFirstName;
	}

	/**
	 * @param childFirstName
	 *           the childFirstName to set
	 */
	public void setChildFirstName(final String childFirstName)
	{
		this.childFirstName = childFirstName;
	}

	/**
	 * @return the childSurname
	 */
	public String getChildSurname()
	{
		return childSurname;
	}

	/**
	 * @param childSurname
	 *           the childSurname to set
	 */
	public void setChildSurname(final String childSurname)
	{
		this.childSurname = childSurname;
	}

	/**
	 * @return the childDOB
	 */
	public String getChildDOB()
	{
		return childDOB;
	}

	/**
	 * @param childDOB
	 *           the childDOB to set
	 */
	public void setChildDOB(final String childDOB)
	{
		this.childDOB = childDOB;
	}

	/**
	 * @return the childGender
	 */
	public int getChildGender()
	{
		return childGender;
	}

	/**
	 * @param childGender
	 *           the childGender to set
	 */
	public void setChildGender(final int childGender)
	{
		this.childGender = childGender;
	}

	/**
	 * @return the customerID
	 */
	public String getCustomerID()
	{
		return customerID;
	}

	/**
	 * @param customerID
	 *           the customerID to set
	 */
	public void setCustomerID(final String customerID)
	{
		this.customerID = customerID;
	}


	/**
	 * @return the iDNumber
	 */
	public String getIDNumber()
	{
		return IDNumber;
	}

	/**
	 * @param iDNumber
	 *           the iDNumber to set
	 */
	public void setIDNumber(final String iDNumber)
	{
		IDNumber = iDNumber;
	}



	/**
	 * @return the hybrisRetailerId
	 */
	public String getHybrisRetailerId()
	{
		return hybrisRetailerId;
	}

	/**
	 * @param hybrisRetailerId
	 *           the hybrisRetailerId to set
	 */
	public void setHybrisRetailerId(final String hybrisRetailerId)
	{
		this.hybrisRetailerId = hybrisRetailerId;
	}

	/**
	 * @return the hybrisCustomerId
	 */
	public String getHybrisCustomerId()
	{
		return hybrisCustomerId;
	}

	/**
	 * @param hybrisCustomerId
	 *           the hybrisCustomerId to set
	 */
	public void setHybrisCustomerId(final String hybrisCustomerId)
	{
		this.hybrisCustomerId = hybrisCustomerId;
	}

	/**
	 * @return the loyaltyRetailerId
	 */
	public String getLoyaltyRetailerId()
	{
		return loyaltyRetailerId;
	}

	/**
	 * @param loyaltyRetailerId
	 *           the loyaltyRetailerId to set
	 */
	public void setLoyaltyRetailerId(final String loyaltyRetailerId)
	{
		this.loyaltyRetailerId = loyaltyRetailerId;
	}

	/**
	 * @return the memberID
	 */
	public String getMemberID()
	{
		return memberID;
	}

	/**
	 * @param memberID
	 *           the memberID to set
	 */
	public void setMemberID(final String memberID)
	{
		this.memberID = memberID;
	}

	/**
	 * @return the emailId
	 */
	public int getEmailId()
	{
		return emailId;
	}

	/**
	 * @param emailId
	 *           the emailId to set
	 */
	public void setEmailId(final int emailId)
	{
		this.emailId = emailId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *           the userId to set
	 */
	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the emailIDEditable
	 */
	public boolean isEmailIDEditable()
	{
		return emailIDEditable;
	}

	/**
	 * @param emailIDEditable
	 *           the emailIDEditable to set
	 */
	public void setEmailIDEditable(final boolean emailIDEditable)
	{
		this.emailIDEditable = emailIDEditable;
	}



}
