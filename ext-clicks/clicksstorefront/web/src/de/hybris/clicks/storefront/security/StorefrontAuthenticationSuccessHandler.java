/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.security;

import de.hybris.clicks.webclient.customer.client.CustomerDetailsRequestClient;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;


/**
 * Success handler initializing user settings, restoring or merging the cart and ensuring the cart is handled correctly.
 * Cart restoration is stored in the session since the request coming in is that to j_spring_security_check and will be
 * redirected
 */
public class StorefrontAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler
{
	private static final Logger LOG = Logger.getLogger(StorefrontAuthenticationSuccessHandler.class);

	private CustomerFacade customerFacade;
	private UiExperienceService uiExperienceService;
	private CartFacade cartFacade;
	private SessionService sessionService;
	private BruteForceAttackCounter bruteForceAttackCounter;
	private Map<UiExperienceLevel, Boolean> forceDefaultTargetForUiExperienceLevel;
	private List<String> restrictedPages;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;
	@Resource(name = "cartService")
	private CartService cartService;

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	private static String CHECKOUT_URL = "/checkout";
	private static String CART_URL = "/cart";
	private static String CART_MERGED = "cartMerged";

	@Resource
	CustomerDetailsRequestClient customerDetailsRequestClient;
	@Resource
	UserService userService;
	@Resource
	TaskService taskService;
	@Resource
	ModelService modelService;
	@Resource
	CMSSiteService cmsSiteService;




	/**
	 * @return the customerDetailsRequestClient
	 */
	public CustomerDetailsRequestClient getCustomerDetailsRequestClient()
	{
		return customerDetailsRequestClient;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	CustomerData customer;


	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{

		getCustomerFacade().loginSuccess();
		request.setAttribute(CART_MERGED, Boolean.FALSE);


		if (!getCartFacade().hasSessionCart() || getCartFacade().getSessionCart().getEntries().isEmpty())
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(null));
			}
			catch (final CommerceCartRestorationException e)
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
						WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
		else if (getCartFacade().hasSessionCart() && !getCartFacade().getSessionCart().getEntries().isEmpty()
				&& getMostRecentSavedCart(getCartFacade().getSessionCart()) != null)
		{
			final CartData existingCart = getMostRecentSavedCart(getCartFacade().getSessionCart());
			if (null != existingCart && !existingCart.isIsPaymentProcessing())
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
				try
				{
					getSessionService().setAttribute(
							WebConstants.CART_RESTORATION,
							getCartFacade().restoreCartAndMerge(getMostRecentSavedCart(getCartFacade().getSessionCart()).getGuid(),
									getCartFacade().getSessionCart().getGuid()));
					request.setAttribute(CART_MERGED, Boolean.TRUE);
				}
				catch (final CommerceCartRestorationException e)
				{
					getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
							WebConstants.CART_RESTORATION_ERROR_STATUS);
				}
				catch (final CommerceCartMergingException e)
				{
					LOG.error("User saved cart could not be merged");
				}
			}
			else if (null != existingCart)
			{
				try
				{
					final CartModel cartModel = commerceCartService.getCartForCodeAndUser(existingCart.getCode(),
							userService.getCurrentUser());
					if (null != cartModel)
					{
						cartService.setSessionCart(cartModel);
					}
					getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS, "paymentInProgress");
				}
				catch (final Exception e)
				{
					LOG.error("User saved cart could not be restored");
				}
			}
		}

		getBruteForceAttackCounter().resetUserCounter(getCustomerFacade().getCurrentCustomer().getUid());

		// start - added for update thru service after user login
		// start - added for update thru service after user login
		try
		{
			final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
			if (null != cmsSiteModel && !Boolean.TRUE.equals(cmsSiteModel.getMaintenance()))
			{
				final CustomerModel customerModel = (CustomerModel) getUserService().getCurrentUser();
				if (null == customerModel.getSAResident())
				{
					customerModel.setSAResident(false);
					modelService.save(customerModel);
				}
				if (StringUtils.isNotEmpty(customerModel.getMemberID()))
				{
					final TaskModel task = new TaskModel();
					task.setContextItem(customerModel);
					task.setRunnerBean("CRMCustomerRefreshTaskRunner");
					task.setExecutionDate(new Date());
					final int nodeId = Config.getInt("crm.customer.refresh.node.id", 0);
					if (0 != nodeId)
					{
						task.setNodeId(nodeId);
					}
					final int retryCount = Config.getInt("crm.customer.refresh.retry.count", 0);
					if (0 != retryCount)
					{
						task.setRetry(retryCount);
					}
					modelService.save(task);
					taskService.scheduleTask(task);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.info(e);
		}
		// end - added for update thru service after user login

		super.onAuthenticationSuccess(request, response, authentication);
	}

	/**
	 * @param customerModel
	 * @param customer
	 */
	private CustomerData populateDetailsRequestData(final CustomerModel form, final CustomerData customer)
	{
		if (null != form.getMemberID())
		{
			customer.setMemberID(form.getMemberID());
		}
		if (null != form.getUid())
		{
			customer.setEmail(getTitleCase(form.getUid()));
		}
		if (null != form.getSAResident() && form.getSAResident().booleanValue())
		{
			customer.setSAResident(Boolean.TRUE);
		}
		else
		{
			customer.setSAResident(Boolean.FALSE);
		}
		if (null != form.getRSA_ID())
		{
			customer.setIDnumber(form.getRSA_ID());
		}
		if (null != form.getNonRSA_DOB())
		{
			customer.setNonRSA_DOB(form.getNonRSA_DOB());
		}
		if (null != form.getCustomerID())
		{
			customer.setCustomerID(form.getCustomerID());
		}
		return customer;

	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	protected List<String> getRestrictedPages()
	{
		return restrictedPages;
	}

	public void setRestrictedPages(final List<String> restrictedPages)
	{
		this.restrictedPages = restrictedPages;
	}

	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	@Required
	public void setCartFacade(final CartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	@Required
	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	/*
	 * @see org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler#
	 * isAlwaysUseDefaultTargetUrl()
	 */
	@Override
	protected boolean isAlwaysUseDefaultTargetUrl()
	{
		final UiExperienceLevel uiExperienceLevel = getUiExperienceService().getUiExperienceLevel();
		if (getForceDefaultTargetForUiExperienceLevel().containsKey(uiExperienceLevel))
		{
			return Boolean.TRUE.equals(getForceDefaultTargetForUiExperienceLevel().get(uiExperienceLevel));
		}
		else
		{
			return false;
		}
	}

	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		String targetUrl = super.determineTargetUrl(request, response);
		if (CollectionUtils.isNotEmpty(getRestrictedPages()))
		{
			for (final String restrictedPage : getRestrictedPages())
			{
				// When logging in from a restricted page, return user to homepage.
				if (targetUrl.contains(restrictedPage))
				{
					targetUrl = super.getDefaultTargetUrl();
				}
			}
		}
		/*
		 * If the cart has been merged and the user logging in through checkout, redirect to the cart page to display the
		 * new cart
		 */
		if (StringUtils.equals(targetUrl, CHECKOUT_URL) && ((Boolean) request.getAttribute(CART_MERGED)).booleanValue())
		{
			targetUrl = CART_URL;
		}

		return targetUrl;
	}

	/**
	 * Determine the most recent saved cart of a user for the site that is not the current session cart. The current
	 * session cart is already owned by the user and for the merging functionality to work correctly the most recently
	 * saved cart must be determined. getCartsForCurrentUser() returns an ordered by modified time list of the saved
	 * carts of the user.
	 *
	 * @param currentCart
	 * @return most recently saved cart
	 */
	protected CartData getMostRecentSavedCart(final CartData currentCart)
	{
		final List<CartData> userCarts = getCartFacade().getCartsForCurrentUser();
		if (CollectionUtils.isNotEmpty(userCarts))
		{
			for (final CartData cart : userCarts)
			{
				if (!StringUtils.equals(cart.getGuid(), currentCart.getGuid()))
				{
					return cart;
				}
			}
		}
		return null;
	}

	protected Map<UiExperienceLevel, Boolean> getForceDefaultTargetForUiExperienceLevel()
	{
		return forceDefaultTargetForUiExperienceLevel;
	}

	@Required
	public void setForceDefaultTargetForUiExperienceLevel(
			final Map<UiExperienceLevel, Boolean> forceDefaultTargetForUiExperienceLevel)
	{
		this.forceDefaultTargetForUiExperienceLevel = forceDefaultTargetForUiExperienceLevel;
	}

	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	protected UiExperienceService getUiExperienceService()
	{
		return uiExperienceService;
	}

	@Required
	public void setUiExperienceService(final UiExperienceService uiExperienceService)
	{
		this.uiExperienceService = uiExperienceService;
	}

	/**
	 * @param commerceCartService
	 *           the commerceCartService to set
	 */
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @param cmsSiteService
	 *           the cmsSiteService to set
	 */
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	/**
	 * @return the taskService
	 */
	public TaskService getTaskService()
	{
		return taskService;
	}

	/**
	 * @param taskService
	 *           the taskService to set
	 */
	public void setTaskService(final TaskService taskService)
	{
		this.taskService = taskService;
	}
}
