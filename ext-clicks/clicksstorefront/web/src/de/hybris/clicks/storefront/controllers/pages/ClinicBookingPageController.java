package de.hybris.clicks.storefront.controllers.pages;



import de.hybris.clicks.facade.clinicBooking.ClinicBookingFacade;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.clicks.storefront.bookAppointment.processor.ClinicBookingProcessor;
import de.hybris.clicks.storefront.controllers.validators.ClinicBookingValidator;
import de.hybris.clicks.storefront.forms.ClinicBookingForm;
import de.hybris.clicks.storefront.sort.StoreSort;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * @author shruti.jhamb
 * 
 *         Booking an online clinic appointment
 * 
 */

@Controller
@Scope("tenant")
@RequestMapping(value = "/clinicBooking")
public class ClinicBookingPageController extends AbstractPageController
{
	private static final String CLINIC_BOOKING_PAGE = "clinicBooking";

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "clinicBookingValidator")
	ClinicBookingValidator clinicBookingValidator;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	@Resource(name = "clinicBookingFacade")
	ClinicBookingFacade clinicBookingFacade;

	@Resource(name = "clinicBookingProcessor")
	ClinicBookingProcessor clinicBookingProcessor;

	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		final Collection<ProvinceData> provinceList = clinicBookingFacade.getProvincesList();
		return provinceList;
	}

	/**
	 * Gets the preferred clinic list.
	 * 
	 * @param code
	 *           the code
	 * @return the preferred clinic list
	 */
	/*
	 * fetching preferred clinic list
	 */
	@ResponseBody
	@RequestMapping(value = "/preferredClinic")
	public Collection<PreferredClinicData> getPreferredClinicList(@RequestParam("provinceName") final String code)
	{
		final List<PreferredClinicData> preferredClinicDataList = clinicBookingFacade.getPreferredClinicsList(code);
		Collections.sort(preferredClinicDataList, new StoreSort());
		return preferredClinicDataList;
	}

	/**
	 * Send enquiry.
	 * 
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * populating details for clinic booking appointment
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String sendEnquiry(final Model model) throws CMSItemNotFoundException
	{
		final ClinicBookingForm clinicBookingForm = new ClinicBookingForm();
		if (userFacade.isAnonymousUser())
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(CLINIC_BOOKING_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CLINIC_BOOKING_PAGE));
			model.addAttribute("metaRobots", "noindex,nofollow");
			model.addAttribute("clinicBookingForm", clinicBookingForm);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					contentPageBreadcrumbBuilder.getBreadcrumbs(getContentPageForLabelOrId(CLINIC_BOOKING_PAGE)));
			return getViewForPage(model);
		}
		else
		{
			final CustomerData customerData = customerFacade.getCurrentCustomer();
			clinicBookingForm.setFirstName(customerData.getFirstName());
			clinicBookingForm.setLastName(customerData.getLastName());
			if (null != customerData.getContactNumber())
			{
				clinicBookingForm.setContactNumber(customerData.getContactNumber());
			}

			storeCmsPageInModel(model, getContentPageForLabelOrId(CLINIC_BOOKING_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CLINIC_BOOKING_PAGE));
			model.addAttribute("metaRobots", "noindex,nofollow");
			model.addAttribute("clinicBookingForm", clinicBookingForm);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					contentPageBreadcrumbBuilder.getBreadcrumbs(getContentPageForLabelOrId(CLINIC_BOOKING_PAGE)));
			return getViewForPage(model);
		}

	}

	/**
	 * Clinic booking.
	 * 
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Saving details for clinic booking appointment
	 */
	@RequestMapping(value = "/clinicBookingProcessor", method = RequestMethod.POST)
	public String clinicBooking(final ClinicBookingForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		/*
		 * validating the clinic booking details
		 */
		clinicBookingValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			model.addAttribute("formMessage", "failed");
		}
		else
		{
			/*
			 * populating data from form to clinic booking data
			 */
			final ClinicBookingData bookingData = clinicBookingProcessor.populateDataProcess(form, bindingResult, model, request,
					response);
			final String result = clinicBookingFacade.processsBookingRequest(bookingData);
			if (result.equalsIgnoreCase("success"))
			{
				model.addAttribute("formMessage", result);
			}
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(CLINIC_BOOKING_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CLINIC_BOOKING_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				contentPageBreadcrumbBuilder.getBreadcrumbs(getContentPageForLabelOrId(CLINIC_BOOKING_PAGE)));
		return getViewForPage(model);
	}
}
