/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.annotation.Resource;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


public class newAccountPageController extends AbstractSearchPageController
{

	private static final String REDIRECT_NEW_ACCOUNT = REDIRECT_PREFIX + "/myaccount";
	private static final String NEW_ACCOUNT_CMS_PAGE = "/myaccount";

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@RequestMapping(value = "/myaccount", method = RequestMethod.GET)
	@RequireHardLogIn
	public String account(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(NEW_ACCOUNT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(NEW_ACCOUNT_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

}
