/**
 *
 */
package de.hybris.clicks.storefront.tags;

import java.io.IOException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 *
 */
public class DateFormatTag extends TagSupport
{

	private static final Logger LOG = Logger.getLogger(DateFormatTag.class);

	private String input;
	private String format;
	private String conversionType;


	@Override
	public int doStartTag() throws JspException
	{
		LOG.debug("Calling date operation taglib");
		try
		{
			String output = "";
			//Get the writer object for output.
			final JspWriter out = pageContext.getOut();

			if (StringUtils.isNotBlank(conversionType))
			{
				if ("date".equals(conversionType) && StringUtils.isNotBlank(format) && StringUtils.isNotBlank(input))
				{
					final Format outputFormat = new SimpleDateFormat(format);

					final SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
					output = outputFormat.format(inputFormat.parse(input));
				}
				else if ("rand".equals(conversionType))
				{
					LOG.debug("Input is : " + input);
					output = input;
					if (input.indexOf(".") != -1)
					{
						final List<String> splitList = Arrays.asList(input.split("\\."));
						String fraction = splitList.get(1);
						if (splitList.get(1).length() == 1)
						{
							fraction = splitList.get(1) + "0";
						}

						output = splitList.get(0) + "<sup>" + fraction + "</sup>";
					}
				}
				else if ("dateDifference".equals(conversionType) && StringUtils.isNotBlank(input))
				{
					LOG.debug("Calculating date difference in days : " + input);
					final SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
					final Date lastDate = inputFormat.parse(input);
					final Date currentDate = new Date();

					output = String.valueOf(((lastDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24)));
					LOG.debug("difference in days is : " + output);
				}
			}


			out.println(output);
		}
		catch (final ParseException e)
		{
			LOG.error("Error in parsing date" + e.getMessage());
		}
		catch (final IOException e)
		{
			LOG.error("IO Operation error while writing to JspWriter" + e.getMessage());
		}
		return SKIP_BODY;
	}

	public String getInput()
	{
		return input;
	}

	public void setInput(final String input)
	{
		this.input = input;
	}


	/**
	 * @return the format
	 */
	public String getFormat()
	{
		return format;
	}

	/**
	 * @param format
	 *           the format to set
	 */
	public void setFormat(final String format)
	{
		this.format = format;
	}

	/**
	 * @return the conversionType
	 */
	public String getConversionType()
	{
		return conversionType;
	}

	/**
	 * @param conversionType
	 *           the conversionType to set
	 */
	public void setConversionType(final String conversionType)
	{
		this.conversionType = conversionType;
	}
}
