/**
 *
 */
package de.hybris.clicks.storefront.controllers;

/**
 * @author jayant.premchand
 *
 */
public interface FormPropertyConstants
{
	interface Validator
	{
		interface Regex
		{
			String NUMBER_REGEX = "^\\+?[0-9]{9,15}$";
			String NAME_REGEX = "[a-zA-Z]*";
			String CUSTOM_NAME_REGEX = "[a-zA-Z0-9,.;:_'\\s-]*";
			String CUSTOM_SPL_CHAR_REGEX = "^[A-Za-z\\s-]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
			String ALPHA_NUMERIC_REGEX = "[a-zA-Z0-9]*";
			String SPL_CHAR_REGEX = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
			String SPL_CHAR_ALLOW_NUM_REGEX = "^[A-Za-z\\s0-9_]{1,}[\\.]{0,1}[A-Za-z\\s0-9_]{0,}$";
			String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			String PASSWORD_REGEX = "((?=.*[0-9!@#$%^&*\\\\(\\\\)_\\-\\+=\\{\\}\\[\\]\\,\\.\\/\\:\\;\\'\\\"\\`\\~\\|\\\\])(?=.*[a-z])(?=.*[A-Z]).{8,})";
			String SA_ID_REGEX = "(((\\d{2}((0[13578]|1[02])(0[1-9]|[12]\\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\\d|30)|02(0[1-9]|1\\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\\d{4})( |-)(\\d{3})|(\\d{7}))";
		}

		interface MaxLength
		{
			int NAME_FIELD = 255;
			int TITLE_FIELD = 255;
			int EMAIL_FIELD = 150;
		}
	}
}
