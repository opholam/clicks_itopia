/**
 *
 */
package de.hybris.clicks.storefront.controllers.validators;

import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.clicks.storefront.forms.ContactUsForm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author siva.reddy
 * 
 */
@Component(value = "travelDealContactUsFormValidator")
public class TravelDealContactUsFormValidator implements Validator
{
	//public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
	public static final String MOBILE_REGEX = "\\d{10}";
	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.NAME_REGEX;

	@Override
	public void validate(final Object obj, final Errors errors)
	{
		final ContactUsForm contactUsForm = (ContactUsForm) obj;
		final String email = contactUsForm.getEmail();
		final String clubcardNo = contactUsForm.getClubcardNo();
		final String contactNo = contactUsForm.getContactNo();
		final String firstName = contactUsForm.getFirstName();
		final String lastName = contactUsForm.getLastName();

		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			errors.rejectValue("firstName", "traveldeal.contact.firstname.invalid");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			errors.rejectValue("firstName", "traveldeal.contact.firstname.invalid");
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			errors.rejectValue("lastName", "traveldeal.contact.lastname.invalid");
		}
		if (!ValidatorUtils.validateRegex(EMAIL_REGEX, email))
		{
			errors.rejectValue("email", "traveldeal.contact.email.invalid");
		}
		else if (StringUtils.length(email) > 255)
		{
			errors.rejectValue("email", "traveldeal.contact.email.invalid");
		}
		if (!StringUtils.isEmpty(clubcardNo) && clubcardNo.length() > 13)
		{
			errors.rejectValue("clubcardNo", "traveldeal.contact.clubcardno.max.invalid");
		}
		if (!ValidatorUtils.validateRegex(NUMBER_REGEX, contactNo))
		{
			errors.rejectValue("contactNo", "traveldeal.contact.contactno.invalid");
		}
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ContactUsForm.class.equals(aClass);
	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

}
