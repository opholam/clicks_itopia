/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.clicks.facade.productpromotion.ProductPromotionSearchFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.clicksstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.clicksstorefrontcommons.forms.StoreFinderForm;
import de.hybris.platform.clicksstorefrontcommons.forms.validation.ReviewValidator;
import de.hybris.platform.clicksstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.clicksstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.UnsupportedEncodingException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Controller for product details page
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/**/p")
public class ProductPageController extends AbstractPageController
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ProductPageController.class);

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String REVIEWS_PATH_VARIABLE_PATTERN = "{numberOfReviews:.*}";

	@Resource(name = "productModelUrlResolver")
	private UrlResolver<ProductModel> productModelUrlResolver;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "reviewValidator")
	private ReviewValidator reviewValidator;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "productPromotionSearchFacade")
	private ProductPromotionSearchFacade productPromotionSearchFacade;

	@Resource(name = "promotionsService")
	private PromotionsService promotionsService;
	@Resource(name = "timeService")
	private TimeService timeService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	/**
	 * Product detail.
	 *
	 * @param productCode
	 *           the product code
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 * @throws UnsupportedEncodingException
	 *            the unsupported encoding exception
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException,
			UnsupportedEncodingException
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		if (null != productModel.getVariants() && productModel.getVariants().size() > 0)
		{
			//Fetch first size variant and redirect to it.
			String sizeVariantURL = "";
			for (final VariantProductModel variantModel : productModel.getVariants())
			{
				if (StringUtils.isNotBlank(variantModel.getCode()))
				{
					//sizeVariantURL = "/p/" + productModel.getVariants().iterator().next().getCode();
					final ProductData productData = productFacade.getProductForOptions(productModel.getVariants().iterator().next(),
							null);
					sizeVariantURL = productData.getUrl();
					break;
				}
			}
			return REDIRECT_PREFIX + sizeVariantURL;
		}
		final String redirection = checkRequestUrl(request, response, productModelUrlResolver.resolve(productModel));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}
		updatePageTitle(productModel, model);
		populateProductDetailForDisplay(productModel, model, request);
		//for(StockLevelModel s:productModel.getStockLevels()){
		//	s.getWarehouse()
		//}

		model.addAttribute(new ReviewForm());
		final AddToCartForm addToCartForm = new AddToCartForm();
		if (null != cartFacade.getSessionCart() && null != cartFacade.getSessionCart().getEntries())
		{
			for (final OrderEntryData entrydata : cartFacade.getSessionCart().getEntries())
			{
				if (productCode.equalsIgnoreCase(entrydata.getProduct().getCode()))
				{
					//addToCartForm.setQty(entrydata.getQuantity());
					model.addAttribute("quantity", entrydata.getQuantity());
				}
			}
		}
		model.addAttribute("addToCartForm", addToCartForm);
		final StoreFinderForm storeFinderForm = new StoreFinderForm();
		if (!userFacade.isAnonymousUser())
		{
			final CustomerData customerData = customerFacade.getCurrentCustomer();
			storeFinderForm.setEmail(customerData.getUid());
		}
		model.addAttribute("storeFinderForm", storeFinderForm);

		/*
		 * final List<ProductReferenceData> productReferences = productFacade.getProductReferencesForCode(productCode,
		 * Arrays.asList(ProductReferenceTypeEnum.CROSSELLING), Arrays.asList(ProductOption.BASIC, ProductOption.PRICE),
		 * null); model.addAttribute("productReferences", productReferences);
		 */
		model.addAttribute("pageType", PageType.PRODUCT.name());
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productModel.getKeywords());
		String metaDescription = "";
		if (productModel instanceof ClicksApparelSizeVariantModel)
		{
			metaDescription = MetaSanitizerUtil.sanitizeDescription(((ClicksApparelSizeVariantModel) productModel)
					.getCopyWriteDescription());
		}

		metaDescription = getMetaDescription(productModel, metaDescription);
		setUpMetaData(model, metaKeywords, metaDescription);
		model.addAttribute("disclaimerMessage", Config.getParameter("product.stock.disclaimer.message"));
		model.addAttribute("productCappingMessage", Config.getParameter("product.details.capping.message"));
		return getViewForPage(model);
	}

	/**
	 * Populate add to cart.
	 *
	 * @param productCodePost
	 *           the product code post
	 * @param model
	 *           the model
	 * @param addToCartForm
	 *           the add to cart form
	 * @param bindingErrors
	 *           the binding errors
	 * @param request
	 *           the request
	 * @return the string
	 */
	@RequestMapping(value = "/cart/add", method = RequestMethod.GET, produces = "application/json")
	public String populateAddToCart(
			@RequestParam(value = "productCodePost", defaultValue = "productCodePost") final String productCodePost,
			final Model model, @Valid final AddToCartForm addToCartForm, final BindingResult bindingErrors,
			final HttpServletRequest request)
	{
		for (final OrderEntryData entrydata : cartFacade.getSessionCart().getEntries())
		{
			if (productCodePost.equalsIgnoreCase(entrydata.getProduct().getCode()))
			{
				addToCartForm.setQty(entrydata.getQuantity());
			}
		}
		model.addAttribute("addToCartForm", addToCartForm);
		model.addAttribute("qty", addToCartForm.getQty());
		return getViewForPage(model);

	}

	/**
	 * @param productModel
	 * @param metaDescription
	 * @return
	 */
	private String getMetaDescription(final ProductModel productModel, String metaDescription)
	{
		if (StringUtils.isNotBlank(metaDescription) && metaDescription.length() > 160)
		{
			metaDescription = metaDescription.substring(0, 160);
		}


		if (CollectionUtils.isNotEmpty(productModel.getPromotions()))
		{
			final StringBuilder builder = new StringBuilder(productModel.getName());
			for (final ProductPromotionModel productPromotion : productModel.getPromotions())
			{
				if (null != productPromotion.getEndDate())
				{
					builder.append(". Offer expires ");
					final Format outputFormat = new SimpleDateFormat("dd MMMM yyyy");
					final SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
					String promoExpireDate = "";
					try
					{
						promoExpireDate = outputFormat.format(inputFormat.parse(productPromotion.getEndDate().toString()));
					}
					catch (final ParseException e)
					{
						LOG.error("Error in converting date" + e.getMessage());
					}
					builder.append(promoExpireDate);
					builder.append(".At selected stores only");
					metaDescription = builder.toString();
					break;
				}
			}
		}
		return metaDescription;
	}

	/**
	 * Show zoom images.
	 *
	 * @param productCode
	 *           the product code
	 * @param galleryPosition
	 *           the gallery position
	 * @param model
	 *           the model
	 * @return the string
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/zoomImages", method = RequestMethod.GET)
	public String showZoomImages(@PathVariable("productCode") final String productCode,
			@RequestParam(value = "galleryPosition", required = false) final String galleryPosition, final Model model)
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final ProductData productData = productFacade.getProductForOptions(productModel,
				Collections.singleton(ProductOption.GALLERY));
		final List<Map<String, ImageData>> images = getGalleryImages(productData);
		populateProductData(productData, model);
		if (galleryPosition != null)
		{
			try
			{
				model.addAttribute("zoomImageUrl", images.get(Integer.parseInt(galleryPosition)).get("zoom").getUrl());
			}
			catch (final IndexOutOfBoundsException | NumberFormatException ioebe)
			{
				model.addAttribute("zoomImageUrl", "");
			}
		}
		return ControllerConstants.Views.Fragments.Product.ZoomImagesPopup;
	}

	/**
	 * Show quick view.
	 *
	 * @param productCode
	 *           the product code
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView", method = RequestMethod.GET)
	public String showQuickView(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request)
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final ProductData productData = productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC,
				ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.STOCK,
				ProductOption.REVIEW, ProductOption.DELIVERY_MODE_AVAILABILITY));

		populateProductData(productData, model);
		getRequestContextData(request).setProduct(productModel);

		return ControllerConstants.Views.Fragments.Product.QuickViewPopup;
	}

	/**
	 * Post review.
	 *
	 * @param productCode
	 *           the product code
	 * @param form
	 *           the form
	 * @param result
	 *           the result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param redirectAttrs
	 *           the redirect attrs
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/review", method =
	{ RequestMethod.GET, RequestMethod.POST })
	public String postReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		getReviewValidator().validate(form, result);

		final ProductModel productModel = productService.getProductForCode(productCode);

		if (result.hasErrors())
		{
			updatePageTitle(productModel, model);
			GlobalMessages.addErrorMessage(model, "review.general.error");
			model.addAttribute("showReviewForm", Boolean.TRUE);
			populateProductDetailForDisplay(productModel, model, request);
			storeCmsPageInModel(model, getPageForProduct(productModel));
			return getViewForPage(model);
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
	}

	/**
	 * Load more review for pdp.
	 *
	 * @param productCode
	 *           the product code
	 * @return the collection
	 */
	@RequestMapping(value = "/loadMoreReviewForPdp", method =
	{ RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Collection<ReviewData> loadMoreReviewForPdp(@RequestParam("productCode") final String productCode)
	{
		if (StringUtils.isNotBlank(productCode))
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			final ProductData productData = productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.REVIEW));
			return productData.getReviews();
		}
		else
		{
			return Collections.EMPTY_LIST;
		}

	}

	/**
	 * Review html.
	 *
	 * @param productCode
	 *           the product code
	 * @param numberOfReviews
	 *           the number of reviews
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/reviewhtml/" + REVIEWS_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String reviewHtml(@PathVariable("productCode") final String productCode,
			@PathVariable("numberOfReviews") final String numberOfReviews, final Model model, final HttpServletRequest request)
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final List<ReviewData> reviews;
		final ProductData productData = productFacade.getProductForOptions(productModel,
				Arrays.asList(ProductOption.BASIC, ProductOption.REVIEW));

		if ("all".equals(numberOfReviews))
		{
			reviews = productFacade.getReviews(productCode);
		}
		else
		{
			final int reviewCount = Math.min(Integer.parseInt(numberOfReviews), (productData.getNumberOfReviews() == null ? 0
					: productData.getNumberOfReviews().intValue()));
			reviews = productFacade.getReviews(productCode, Integer.valueOf(reviewCount));
		}

		getRequestContextData(request).setProduct(productModel);
		model.addAttribute("reviews", reviews);
		model.addAttribute("reviewsTotal", productData.getNumberOfReviews());
		model.addAttribute(new ReviewForm());

		return ControllerConstants.Views.Fragments.Product.ReviewsTab;
	}

	/**
	 * Write review.
	 *
	 * @param productCode
	 *           the product code
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.GET)
	public String writeReview(@PathVariable final String productCode, final Model model) throws CMSItemNotFoundException
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		model.addAttribute(new ReviewForm());
		setUpReviewPage(model, productModel);
		return ControllerConstants.Views.Pages.Product.WriteReview;
	}

	protected void setUpReviewPage(final Model model, final ProductModel productModel) throws CMSItemNotFoundException
	{
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productModel.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productModel.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, getPageForProduct(productModel));
		model.addAttribute("product", productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC)));
		updatePageTitle(productModel, model);
	}

	/**
	 * Write review.
	 *
	 * @param productCode
	 *           the product code
	 * @param form
	 *           the form
	 * @param result
	 *           the result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param redirectAttrs
	 *           the redirect attrs
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.POST)
	public String writeReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		getReviewValidator().validate(form, result);

		final ProductModel productModel = productService.getProductForCode(productCode);

		if (result.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "review.general.error");
			populateProductDetailForDisplay(productModel, model, request);
			setUpReviewPage(model, productModel);
			return ControllerConstants.Views.Pages.Product.WriteReview;
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
	}

	/**
	 * Handle unknown identifier exception.
	 *
	 * @param exception
	 *           the exception
	 * @param request
	 *           the request
	 * @return the string
	 */
	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		String basePath = "/404";
		final String servletPath = request.getRequestURI();
		if ((StringUtils.isNotBlank(servletPath) && !servletPath.equals(XSSFilterUtil.filter(request.getRequestURI()))))
		{
			return REDIRECT_PREFIX + basePath;
		}
		if (!request.getRequestURI().contains("404"))
		{
			basePath = basePath + XSSFilterUtil.filter(request.getServletPath());
			return REDIRECT_PREFIX + basePath;
		}
		return FORWARD_PREFIX + "/404";
	}

	/**
	 * Update page title.
	 *
	 * @param productModel
	 *           the product model
	 * @param model
	 *           the model
	 */
	protected void updatePageTitle(final ProductModel productModel, final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveProductPageTitle(productModel));
	}

	/**
	 * Populate product detail for display.
	 *
	 * @param productModel
	 *           the product model
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected void populateProductDetailForDisplay(final ProductModel productModel, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		getRequestContextData(request).setProduct(productModel);

		final ProductData productData = productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC,
				ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.GALLERY, ProductOption.CATEGORIES, ProductOption.REVIEW,
				ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
				ProductOption.DELIVERY_MODE_AVAILABILITY));
		// for PDP Tab display
		sortVariantOptionData(productData);
		storeCmsPageInModel(model, getPageForProduct(productModel));
		populateProductData(productData, model);
		populatePromotionalProducts(productModel, productData, model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productModel));
	}

	/**
	 * Populate product data.
	 *
	 * @param productData
	 *           the product data
	 * @param model
	 *           the model
	 */
	protected void populateProductData(final ProductData productData, final Model model)
	{
		model.addAttribute("galleryImages", getGalleryImages(productData));
		model.addAttribute("product", productData);
	}

	protected void sortVariantOptionData(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getBaseOptions()))
		{
			for (final BaseOptionData baseOptionData : productData.getBaseOptions())
			{
				if (CollectionUtils.isNotEmpty(baseOptionData.getOptions()))
				{
					Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(productData.getVariantOptions()))
		{
			Collections.sort(productData.getVariantOptions(), variantSortStrategy);
		}
	}

	/**
	 * Gets the gallery images.
	 *
	 * @param productData
	 *           the product data
	 * @return the gallery images
	 */
	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			Collections.sort(images, new Comparator<ImageData>()
			{
				@Override
				public int compare(final ImageData image1, final ImageData image2)
				{
					return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
				}
			});

			if (CollectionUtils.isNotEmpty(images))
			{
				int currentIndex = images.get(0).getGalleryIndex().intValue();
				Map<String, ImageData> formats = new HashMap<String, ImageData>();
				for (final ImageData image : images)
				{
					if (currentIndex != image.getGalleryIndex().intValue())
					{
						galleryImages.add(formats);
						formats = new HashMap<>();
						currentIndex = image.getGalleryIndex().intValue();
					}
					formats.put(image.getFormat(), image);
				}
				if (!formats.isEmpty())
				{
					galleryImages.add(formats);
				}
			}
		}
		return galleryImages;
	}

	protected ReviewValidator getReviewValidator()
	{
		return reviewValidator;
	}

	/**
	 * Gets the page for product.
	 *
	 * @param product
	 *           the product
	 * @return the page for product
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	protected AbstractPageModel getPageForProduct(final ProductModel product) throws CMSItemNotFoundException
	{
		return cmsPageService.getPageForProduct(product);
	}

	/**
	 * Populate promotional products.
	 *
	 * @param productModel
	 *           the product model
	 * @param productData
	 *           the product data
	 * @param model
	 *           the model
	 */
	private void populatePromotionalProducts(final ProductModel productModel, final ProductData productData, final Model model)
	{
		try
		{
			List<ProductData> productDataList = null;
			final Collection<ProductModel> productCollection = new ArrayList<ProductModel>();
			final String categoryCode = productData.getCategories().iterator().next().getCode();
			final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);
			int promoCodeForPromotions = 0;
			int count = 0;
			final List<ProductPromotionModel> promotions = getProductPromotions(productModel);
			if (CollectionUtils.isNotEmpty(promotions))
			{
				productCollection.addAll(promotions.iterator().next().getProducts());
			}
			else if (null != category)
			{
				final List<ProductModel> productsForCategory = productPromotionSearchFacade.getPromotionsForCategory(category);
				productCollection.addAll(productsForCategory);
			}
			if (CollectionUtils.isNotEmpty(productCollection) && CollectionUtils.isNotEmpty(category.getProducts()))
			{
				productCollection.retainAll(category.getProducts());
				promoCodeForPromotions = productCollection.iterator().next().getPromotions().iterator().next().getPromotionTypeID();
				productDataList = new ArrayList<ProductData>();
			}
			for (final ProductModel prodModel : productCollection)
			{
				if (count == 10)
				{
					break;
				}
				if (!productData.getCode().equalsIgnoreCase(prodModel.getCode()))
				{
					final ProductData promoProductData = productFacade.getProductForOptions(prodModel, Arrays.asList(
							ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.GALLERY, ProductOption.STOCK,
							ProductOption.VOLUME_PRICES));
					productDataList.add(promoProductData);
					count++;
				}
			}
			model.addAttribute("productListForPromotions", productDataList);
			model.addAttribute("promoCodeForPromotions", promoCodeForPromotions);
		}
		catch (final Exception e)
		{
			//
		}
	}

	private List<ProductPromotionModel> getProductPromotions(final ProductModel productModel)
	{
		try
		{
			final BaseSiteModel baseSiteModel = getBaseSiteService().getCurrentBaseSite();
			if (baseSiteModel != null)
			{
				final PromotionGroupModel defaultPromotionGroup = baseSiteModel.getDefaultPromotionGroup();
				final Date currentTimeRoundedToMinute = DateUtils.round(getTimeService().getCurrentTime(), Calendar.MINUTE);

				if (defaultPromotionGroup != null)
				{
					return getPromotionsService().getProductPromotions(Collections.singletonList(defaultPromotionGroup), productModel,
							true, currentTimeRoundedToMinute);
				}
			}
		}
		catch (final Exception e)
		{
			//
		}
		return null;
	}

	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * @param promotionsService
	 *           the promotionsService to set
	 */
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}

	/**
	 * @return the timeService
	 */
	public TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * @param timeService
	 *           the timeService to set
	 */
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}
