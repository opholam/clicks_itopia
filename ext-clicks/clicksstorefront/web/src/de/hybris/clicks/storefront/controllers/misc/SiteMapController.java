/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.misc;

import de.hybris.clicks.facade.sitemap.stores.SitemapStoresFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.category.attribute.CategoryAllSubcategories;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@Scope("tenant")
public class SiteMapController extends AbstractPageController
{
	private static final String SITE_MAP_CMS_PAGE = "siteMapPage";
	private static final String SITE_MAP_STORES_PAGE = "siteMapStoresPage";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "siteBaseUrlResolutionService")
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource
	private CategoryAllSubcategories categoryAllSubcategories;

	@Resource(name = "sitemapStoresFacade")
	private SitemapStoresFacade sitemapStoresFacade;

	@RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET, produces = "application/xml")
	public String getSitemapXml(final Model model, final HttpServletResponse response)
	{


		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		final String mediaUrlForSite = siteBaseUrlResolutionService.getMediaUrlForSite(currentSite, false, "");

		final List<String> siteMapUrls = new ArrayList<>();

		final Collection<MediaModel> siteMaps = currentSite.getSiteMaps();
		for (final MediaModel siteMap : siteMaps)
		{
			siteMapUrls.add(mediaUrlForSite + siteMap.getURL());
		}
		model.addAttribute("siteMapUrls", siteMapUrls);
		model.addAttribute("subcategories", siteMapUrls);

		return ControllerConstants.Views.Pages.Misc.MiscSiteMapPage;
	}

	@RequestMapping(value = "/sitemap", method = RequestMethod.GET)
	public String getSitemapPage(final Model model, @RequestParam(value = "code", defaultValue = "OH1") final String categoryCode)
			throws CMSItemNotFoundException
	{
		model.addAttribute("subcategories", getAllSubcategoriesForCode(categoryCode));
		storeCmsPageInModel(model, getContentPageForLabelOrId(SITE_MAP_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SITE_MAP_CMS_PAGE));
		return getViewForPage(model);
		//return ControllerConstants.Views.Pages.Misc.MiscSiteMapPage;
	}

	@RequestMapping(value = "/sitemap/loadMoreSubCategories", method = RequestMethod.POST)
	public @ResponseBody List<CategoryModel> loadMoreReviewForPdp(@RequestParam("code") final String categoryCode)
	{
		if (StringUtils.isNotBlank(categoryCode))
		{
			return getAllSubcategoriesForCode(categoryCode);
		}
		else
		{
			return Collections.EMPTY_LIST;
		}

	}

	@RequestMapping(value = "/sitemap/stores", method = RequestMethod.GET)
	public String getSitemapStorePage(final Model model) throws CMSItemNotFoundException
	{

		model.addAttribute("pos", sitemapStoresFacade.getSitemapStores());
		storeCmsPageInModel(model, getContentPageForLabelOrId(SITE_MAP_STORES_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SITE_MAP_STORES_PAGE));
		return getViewForPage(model);
		//return ControllerConstants.Views.Pages.Misc.MiscSiteMapPage;
	}

	private List<CategoryModel> getAllSubcategoriesForCode(final String code)
	{
		final CategoryModel category = commerceCategoryService.getCategoryForCode(code);

		return category.getCategories();
	}
}
