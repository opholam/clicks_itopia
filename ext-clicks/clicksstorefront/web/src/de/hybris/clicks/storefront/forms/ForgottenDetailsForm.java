/**
 *
 */
package de.hybris.clicks.storefront.forms;

/**
 *
 */
public class ForgottenDetailsForm
{
	private String clubCardNumber;
	private String residentNumber;
	private String day;
	private String month;
	private String year;
	private boolean resident = true;

	/**
	 * @return the resident
	 */
	public boolean isResident()
	{
		return resident;
	}

	/**
	 * @param resident
	 *           the resident to set
	 */
	public void setResident(final boolean resident)
	{
		this.resident = resident;
	}

	/**
	 * @return the clubCardNumber
	 */
	public String getClubCardNumber()
	{
		return clubCardNumber;
	}

	/**
	 * @param clubCardNumber
	 *           the clubCardNumber to set
	 */
	public void setClubCardNumber(final String clubCardNumber)
	{
		this.clubCardNumber = clubCardNumber;
	}

	/**
	 * @return the residentNumber
	 */
	public String getResidentNumber()
	{
		return residentNumber;
	}

	/**
	 * @param residentNumber
	 *           the residentNumber to set
	 */
	public void setResidentNumber(final String residentNumber)
	{
		this.residentNumber = residentNumber;
	}

	/**
	 * @return the day
	 */
	public String getDay()
	{
		return day;
	}

	/**
	 * @param day
	 *           the day to set
	 */
	public void setDay(final String day)
	{
		this.day = day;
	}

	/**
	 * @return the month
	 */
	public String getMonth()
	{
		return month;
	}

	/**
	 * @param month
	 *           the month to set
	 */
	public void setMonth(final String month)
	{
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public String getYear()
	{
		return year;
	}

	/**
	 * @param year
	 *           the year to set
	 */
	public void setYear(final String year)
	{
		this.year = year;
	}



}
