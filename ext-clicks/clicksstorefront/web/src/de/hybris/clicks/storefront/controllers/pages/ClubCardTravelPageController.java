/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.model.TravelDealModel;
import de.hybris.clicks.facades.clubcard.ClubCardTravelDealFacade;
import de.hybris.clicks.facades.data.ClicksTravelDealData;
import de.hybris.clicks.storefront.contactUs.processor.ContactUsProcessor;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.controllers.validators.TravelDealContactUsFormValidator;
import de.hybris.clicks.storefront.forms.ContactUsForm;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * The Class ClubCardTravelPageController.
 * 
 * @author muthukumar.s
 */

@Controller
@Scope("tenant")
@RequestMapping("/clubcardtravel")
public class ClubCardTravelPageController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(ClubCardTravelPageController.class);

	private static final String TRAVEL_DETAIL_PAGE = "/traveldeals/{travelDealCode}";
	private static final String LOAD_MORE_TRAVEL_DEALS_PAGE = "/traveldeals/loadMore";
	private static final String TRAVEL_DETAIL_SEND_ENQUIRY = "/traveldeals/sendenquiry/{travelDealCode}";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "clubCardTravelDealFacade")
	private ClubCardTravelDealFacade clubCardTravelDealFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "travelDealContactUsFormValidator")
	private TravelDealContactUsFormValidator travelDealContactUsFormValidator;

	@Resource(name = "contactUsProcessor")
	private ContactUsProcessor contactUsProcessor;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	/**
	 * Gets the travel deals.
	 * 
	 * @param model
	 *           the model
	 * @return the travel deals
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getTravelDeals(final Model model)
	{
		try
		{
			final Map<String, Object> params = new HashMap<String, Object>();
			//params.put(TravelDealModel.ENDDATE, new Date());
			model.addAttribute("errorMsg", null);
			model.addAttribute("travelDeals", clubCardTravelDealFacade.getTravelDeals(params));
			final String maxNoOfTraveldealsShown = (String) configurationService.getConfiguration().getProperty(
					"travel.traveldeals.show");
			model.addAttribute("maxNoOfTraveldealsShown", maxNoOfTraveldealsShown);
			storeCmsPageInModel(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.clubCard"));
		}
		catch (final Exception e)
		{
			LOG.error("Error while loading the travel deals" + e);
		}
		return ControllerConstants.Views.Pages.ClubCard.ClubCardTravelDealsPage;
	}

	/**
	 * Load more travel deals.
	 * 
	 * @param initialValue
	 *           the initial value
	 * @param model
	 *           the model
	 * @return the string
	 */
	@SuppressWarnings("boxing")
	@RequestMapping(value = LOAD_MORE_TRAVEL_DEALS_PAGE, method = RequestMethod.GET)
	public String loadMoreTravelDeals(
			@RequestParam(value = "initialValue", defaultValue = "0", required = false) final int initialValue, final Model model)
	{
		try
		{
			final String maxNoOfTraveldealsShown = (String) configurationService.getConfiguration().getProperty(
					"travel.traveldeals.show");
			final int endValue = initialValue + Integer.parseInt(maxNoOfTraveldealsShown);
			LOG.debug("maxNoOfTraveldealsShown--" + maxNoOfTraveldealsShown + "initialValue-- " + initialValue + "endValue--"
					+ endValue);

			model.addAttribute("initialValue", initialValue);
			model.addAttribute("endValue", endValue);

			final Map<String, Object> params = new HashMap<String, Object>();
			//params.put(TravelDealModel.ENDDATE, new Date());
			model.addAttribute("errorMsg", null);
			model.addAttribute("travelDeals", clubCardTravelDealFacade.getTravelDeals(params));
			model.addAttribute("maxNoOfTraveldealsShown", maxNoOfTraveldealsShown);
			storeCmsPageInModel(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.clubCard"));
		}
		catch (final Exception e)
		{
			LOG.error("Error while loading the load more travel deals" + e);
		}
		return ControllerConstants.Views.Pages.ClubCard.loadMoreClubCardTravelDealsPage;
	}

	/**
	 * Gets the travel deal by code.
	 * 
	 * @param travelDealCode
	 *           the travel deal code
	 * @param model
	 *           the model
	 * @return the travel deal by code
	 */
	@RequestMapping(value = TRAVEL_DETAIL_PAGE, method = RequestMethod.GET)
	public String getTravelDealByCode(@PathVariable("travelDealCode") final String travelDealCode, final Model model)
	{
		try
		{
			if (StringUtils.isNotBlank(travelDealCode))
			{
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put(TravelDealModel.REFERENCENO, travelDealCode);
				model.addAttribute("errorMsg", null);
				final List<ClicksTravelDealData> travelDeals = clubCardTravelDealFacade.getTravelDeals(params);
				model.addAttribute("travelDeal", travelDeals.get(0));

				final ContactUsForm contactUsForm = new ContactUsForm();

				if (!userService.isAnonymousUser(userService.getCurrentUser()))
				{
					final CustomerData customer = customerFacade.getCurrentCustomer();
					contactUsForm.setFirstName(customer.getFirstName());
					contactUsForm.setLastName(customer.getLastName());
					contactUsForm.setEmail(customer.getUid());
					contactUsForm.setClubcardNo(customer.getMemberID());

					if (null != customer.getContactDetails())
					{
						for (final ContactDetail contact : customer.getContactDetails())
						{
							if (null != contact.getTypeID() && contact.getTypeID().equalsIgnoreCase("2"))
							{
								contactUsForm.setContactNo(contact.getNumber());
							}
						}
					}
				}

				model.addAttribute("contactUsForm", contactUsForm);

				storeCmsPageInModel(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
				final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.clubCard");
				breadCrumbs.get(0).setUrl("/clubcardtravel");
				breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.travelDealDetail"));
				model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);

			}
			else
			{
				return getTravelDeals(model);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while loading the  travel deal with travel code:" + travelDealCode + " " + e);
		}
		return ControllerConstants.Views.Pages.ClubCard.ClubCardTravelDealDetailPage;
	}



	/**
	 * Send enquiry.
	 * 
	 * @param travelDealCode
	 *           the travel deal code
	 * @param contactUsForm
	 *           the contact us form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = TRAVEL_DETAIL_SEND_ENQUIRY, method = RequestMethod.POST)
	public String sendEnquiry(@PathVariable("travelDealCode") final String travelDealCode,
			@ModelAttribute final ContactUsForm contactUsForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		LOG.info("travelDealCode" + travelDealCode);
		try
		{
			if (StringUtils.isNotBlank(travelDealCode))
			{
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put(TravelDealModel.REFERENCENO, travelDealCode);
				model.addAttribute("errorMsg", null);
				final List<ClicksTravelDealData> travelDeals = clubCardTravelDealFacade.getTravelDeals(params);
				model.addAttribute("travelDeal", travelDeals.get(0));
				//validate contact us form
				travelDealContactUsFormValidator.validate(contactUsForm, bindingResult);

				if (bindingResult.hasErrors())
				{
					GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.forgotdetails.error", null);
					model.addAttribute("contactUsForm", contactUsForm);
					storeCmsPageInModel(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
					return ControllerConstants.Views.Pages.ClubCard.ClubCardTravelDealDetailPage;
				}
				else
				{
					//send email
					final boolean result = contactUsProcessor.processsContactUsData(contactUsForm);
					if (result)
					{//display success Message
						GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
								"contact.us.enquiry.success", null);
						return REDIRECT_PREFIX + "/clubcardtravel/traveldeals/" + travelDealCode;
					}
					else
					{
						GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "contact.us.enquiry.failed", null);
						model.addAttribute("contactUsForm", contactUsForm);
						storeCmsPageInModel(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
						setUpMetaDataForContentPage(model, getContentPageForLabelOrId("clubCardTravelDealsPage"));
						return ControllerConstants.Views.Pages.ClubCard.ClubCardTravelDealDetailPage;
					}
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while sending contact us request from the travel deal with travel code:" + travelDealCode + " " + e);
		}
		return getTravelDeals(model);
	}


	/**
	 * @return the clubCardTravelDealFacade
	 */
	public ClubCardTravelDealFacade getClubCardTravelDealFacade()
	{
		return clubCardTravelDealFacade;
	}

	/**
	 * @param clubCardTravelDealFacade
	 *           the clubCardTravelDealFacade to set
	 */
	public void setClubCardTravelDealFacade(final ClubCardTravelDealFacade clubCardTravelDealFacade)
	{
		this.clubCardTravelDealFacade = clubCardTravelDealFacade;
	}

	/**
	 * @return the resourceBreadcrumbBuilder
	 */
	public ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	/**
	 * @param resourceBreadcrumbBuilder
	 *           the resourceBreadcrumbBuilder to set
	 */
	public void setResourceBreadcrumbBuilder(final ResourceBreadcrumbBuilder resourceBreadcrumbBuilder)
	{
		this.resourceBreadcrumbBuilder = resourceBreadcrumbBuilder;
	}
}
