/**
 * 
 */
package de.hybris.clicks.storefront.filters;

import de.hybris.platform.servicelayer.web.XSSFilter;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


/**
 * @author sridhar.reddy
 * 
 */
public class ClicksXssFilter extends XSSFilter
{
	protected static final Logger LOG = Logger.getLogger(ClicksXssFilter.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.servicelayer.web.XSSFilter#setRejectResponseCodes(javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void setRejectResponseCodes(final HttpServletResponse httpResponse) throws IOException
	{
		LOG.debug("Malicious content found redirecting to 404");
		httpResponse.setStatus(404);
		httpResponse.sendRedirect("/404");
	}

}
