/**
 *
 */
package de.hybris.clicks.storefront.controllers.magazine;

import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;
import de.hybris.clicks.facades.magazine.MagazineLandingFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author siva.reddy
 *
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/clubcard-magazine")
public class MagazinePageController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(MagazinePageController.class);

	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "magazineLandingFacade")
	private MagazineLandingFacade magazineLandingFacade;

	@SuppressWarnings("boxing")
	@RequestMapping(method = RequestMethod.GET)
	public String getAllArticlesToMagazinePage(@RequestParam(value = "id", defaultValue = "all") final String tagId,
			@RequestParam(value = "pageType", defaultValue = "magazineLandingPage") final String pageType, final Model model)
					throws CMSItemNotFoundException
	{
		final String maxNoOfArticlesShown = (String) configurationService.getConfiguration()
				.getProperty("magazine.landingpage.articles.show");

		LOG.debug("maxNoOfArticlesShown" + maxNoOfArticlesShown);
		LOG.info("Requested Magazine type   " + tagId);
		final List<ArticleTagsModel> articleTags = magazineLandingFacade.getArticleTagsToMagazinePage();
		final List<ArticleModel> articles = magazineLandingFacade.getAllArticlesToMagazinePage(tagId, pageType);
		model.addAttribute("articleTags", articleTags);
		if (!articles.isEmpty())
		{
			model.addAttribute("articles", articles);
			model.addAttribute("totalNoOfArticles", articles.size());
			model.addAttribute("maxNoOfArticlesShown", maxNoOfArticlesShown);
		}
		model.addAttribute("tagId", tagId);

		if ("healthyLivingPage".equalsIgnoreCase(pageType))
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("healthyLivingPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("healthyLivingPage"));
		}
		else if ("magazineLandingPage".equalsIgnoreCase(pageType))
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("magazineLandingPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("magazineLandingPage"));
		}
		return ControllerConstants.Views.Pages.Magazine.MagazineLandingPage;
	}

	@SuppressWarnings("boxing")
	@RequestMapping(value = "/getMoreArticles", method = RequestMethod.GET)
	public String getMoreArticlesToMagazinePage(@RequestParam(value = "id", defaultValue = "all") final String tagId,
			@RequestParam(value = "pageType", defaultValue = "magazineLandingPage") final String pageType,
			@RequestParam(value = "initialValue", defaultValue = "0", required = false) final int initialValue, final Model model)
					throws CMSItemNotFoundException
	{
		final String maxNoOfArticlesShown = (String) configurationService.getConfiguration()
				.getProperty("magazine.landingpage.articles.show");

		if ("healthyLivingPage".equalsIgnoreCase(pageType))
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("healthyLivingPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("healthyLivingPage"));
		}
		else if ("magazineLandingPage".equalsIgnoreCase(pageType))
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("magazineLandingPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("magazineLandingPage"));
		}

		LOG.debug("maxNoOfArticlesShown" + maxNoOfArticlesShown);
		LOG.debug("initialValue" + initialValue);
		final int endValue = initialValue + Integer.parseInt(maxNoOfArticlesShown);
		LOG.debug("endValue" + endValue);

		model.addAttribute("initialValue", initialValue);
		model.addAttribute("endValue", endValue);

		LOG.info("Requested Magazine type   " + tagId);
		final List<ArticleTagsModel> articleTags = magazineLandingFacade.getArticleTagsToMagazinePage();
		final List<ArticleModel> articles = magazineLandingFacade.getAllArticlesToMagazinePage(tagId, pageType);
		model.addAttribute("articleTags", articleTags);
		if (!articles.isEmpty())
		{
			model.addAttribute("articles", articles);
			model.addAttribute("totalNoOfArticles", articles.size());
			model.addAttribute("maxNoOfArticlesShown", maxNoOfArticlesShown);
		}
		model.addAttribute("tagId", tagId);
		return ControllerConstants.Views.Pages.Magazine.LoadMoreArticlesMagazineLandingPage;
	}
}
