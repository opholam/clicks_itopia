/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.clicksstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;


/**
 * Controller for CMS ProductReferencesComponent.
 */
@Controller("ProductCarouselComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.ProductCarouselComponent)
public class ProductCarouselComponentController extends AbstractCMSComponentController<ProductCarouselComponentModel>
{
	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
			ProductOption.REVIEW, ProductOption.STOCK);

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ProductCarouselComponentModel component)
	{
		final List<ProductData> products = new ArrayList<ProductData>();
		final List<ProductData> gaProducts = new ArrayList<ProductData>();

		final Gson gson = new Gson();

		products.addAll(collectLinkedProducts(component));
		products.addAll(collectSearchProducts(component));

		populateProductAttributesForGaTracking(gaProducts, products);

		String productJson = gson.toJson(gaProducts);
		if (StringUtils.isNotBlank(productJson) && productJson.contains("\\u0027"))
		{
			productJson = productJson.replaceAll("\\u0027", "\\\u0027");
		}

		//		productJson = CharMatcher.JAVA_ISO_CONTROL.removeFrom(productJson);
		model.addAttribute("productJson", productJson);
		model.addAttribute("title", component.getTitle());
		model.addAttribute("productData", products);
		model.addAttribute("addToCartForm", new AddToCartForm());
	}

	//@RequestMapping(value = "/cart/add", method = RequestMethod.GET, produces = "application/json")
	//protected void addQuantity(final HttpServletRequest request, final Model model, final ProductCarouselComponentModel component)
	//{
	//model.addAttribute("addToCartForm", new AddToCartForm());
	//}

	/**
	 * @param gaProducts
	 * @param products
	 */
	private void populateProductAttributesForGaTracking(final List<ProductData> gaProducts, final List<ProductData> products)
	{
		for (final ProductData productData : products)
		{
			final ProductData product = new ProductData();
			product.setCode(productData.getCode());
			product.setSizeVariantTitle(productData.getSizeVariantTitle());
			product.setBrand(productData.getBrand());
			gaProducts.add(product);
		}
	}

	protected List<ProductData> collectLinkedProducts(final ProductCarouselComponentModel component)
	{
		final List<ProductData> products = new ArrayList<ProductData>();

		for (final ProductModel productModel : component.getProducts())
		{
			final ProductData productData = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);
			if (StringUtils.isNotEmpty(productModel.getRemarks()))
			{
				productData.setRemarks(productModel.getRemarks());
			}
			products.add(productData);
		}

		for (final CategoryModel categoryModel : component.getCategories())
		{
			for (final ProductModel productModel : categoryModel.getProducts())
			{
				final ProductData productData = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);
				if (StringUtils.isNotEmpty(productModel.getRemarks()))
				{
					productData.setRemarks(productModel.getRemarks());
				}
				products.add(productData);
			}
		}

		return products;
	}

	protected List<ProductData> collectSearchProducts(final ProductCarouselComponentModel component)
	{
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(component.getSearchQuery());
		final String categoryCode = component.getCategoryCode();

		if (searchQueryData.getValue() != null && categoryCode != null)
		{
			final SearchStateData searchState = new SearchStateData();
			searchState.setQuery(searchQueryData);

			final PageableData pageableData = new PageableData();
			pageableData.setPageSize(100); // Limit to 100 matching results

			return productSearchFacade.categorySearch(categoryCode, searchState, pageableData).getResults();
		}

		return Collections.emptyList();
	}
}
