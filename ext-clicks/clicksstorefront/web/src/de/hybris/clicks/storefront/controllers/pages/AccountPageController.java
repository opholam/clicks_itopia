/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.ClamScanner;
import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.facades.customer.ClicksCustomerFacade;
import de.hybris.clicks.facades.myaccount.MyAccountFacade;
import de.hybris.clicks.facades.product.data.DayData;
import de.hybris.clicks.facades.product.data.MonthData;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.clicks.facades.product.data.YearData;
import de.hybris.clicks.storefront.contactUs.processor.ContactUsProcessor;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.controllers.integration.CustomerDataPopulator;
import de.hybris.clicks.storefront.controllers.integration.CustomerIntegrationProcessor;
import de.hybris.clicks.storefront.controllers.integration.LinkCCPopulator;
import de.hybris.clicks.storefront.controllers.validators.ContactUsValidator;
import de.hybris.clicks.storefront.controllers.validators.EditProfileClubcardValidator;
import de.hybris.clicks.storefront.controllers.validators.EditProfileNonClubcardValidator;
import de.hybris.clicks.storefront.controllers.validators.LinkCCToAccountValidator;
import de.hybris.clicks.storefront.controllers.validators.RegistrationNonClubcardValidator;
import de.hybris.clicks.storefront.controllers.validators.RetrieveLinkCCToAccountValidator;
import de.hybris.clicks.storefront.edit.profile.processor.EditProfilePopulator;
import de.hybris.clicks.storefront.edit.profile.processor.LinkCCAccountProcessor;
import de.hybris.clicks.storefront.edit.profile.processor.UpdateProfile;
import de.hybris.clicks.storefront.forms.ContactUsForm;
import de.hybris.clicks.storefront.forms.EditClubCardForm;
import de.hybris.clicks.storefront.forms.EditNonClubCardForm;
import de.hybris.clicks.storefront.forms.FileUpload;
import de.hybris.clicks.webclient.customer.response.populator.CustomerRegistrationResponseProcessor;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.clicksstorefrontcommons.forms.AddressForm;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.clicksstorefrontcommons.forms.RegisterClubCardForm;
import de.hybris.platform.clicksstorefrontcommons.forms.RegisterCustomerForm;
import de.hybris.platform.clicksstorefrontcommons.forms.UpdateEmailForm;
import de.hybris.platform.clicksstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.clicksstorefrontcommons.forms.UpdateProfileForm;
import de.hybris.platform.clicksstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.clicksstorefrontcommons.forms.validation.EmailValidator;
import de.hybris.platform.clicksstorefrontcommons.forms.validation.PasswordValidator;
import de.hybris.platform.clicksstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.platform.clicksstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.clicksstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commercefacades.user.data.TopicsData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.media.MediaIOException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import Omni.BizTalk.Clicks.Schema.Customer.Response;


/**
 * Controller for all account pages
 */
@Controller
@Scope("tenant")
@RequestMapping("/my-account")
public class AccountPageController extends AbstractSearchPageController
{
	// Internal Redirects
	private static final String REDIRECT_MY_ACCOUNT = REDIRECT_PREFIX + "/my-account";
	private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + "/my-account/address-book";
	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";
	private static final String REDIRECT_TO_PROFILE_PAGE = REDIRECT_PREFIX + "/my-account/profile";
	private static final String REDIRECT_TO_CHANGE_PASSWORD_PAGE = REDIRECT_PREFIX + "/my-account/change-password";
	private static final String REDIRECT_TO_SAVED_ADDRESS_PAGE = REDIRECT_PREFIX + "/my-account/saved-addresses";
	protected static final String UPDATE_PASSWORD_EMAIL_PROCESS = "updatePasswordEmailProcess";
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN = "{addressCode:.*}";

	// CMS Pages
	private static final String ACCOUNT_CMS_PAGE = "/my-account";
	private static final String REWARDS_ACTIVITY_PAGE = "rewards-activity";
	private static final String PROFILE_CMS_PAGE = "profile";
	private static final String UPDATE_PASSWORD_CMS_PAGE = "updatePassword";
	private static final String UPDATE_PROFILE_CMS_PAGE = "update-profile";
	private static final String UPDATE_EMAIL_CMS_PAGE = "update-email";
	private static final String ADDRESS_BOOK_CMS_PAGE = "address-book";
	private static final String ADD_EDIT_ADDRESS_CMS_PAGE = "add-edit-address";
	private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";
	private static final String ORDER_HISTORY_CMS_PAGE = "orders";
	private static final String ORDER_DETAIL_CMS_PAGE = "order";
	private static final String CHANGE_PASSWORD_CMS_PAGE = "changePassword";
	private static final String SAVED_ADDRESSES_CMS_PAGE = "SavedAddressesPage";
	private static final String ACTIVATE_VITALITY_CMS_PAGE = "activateVitalityPage";
	private static final String MY_VITALITY_HEALTHCARE_CMS_PAGE = "myVitalityHealthyCarePage";
	//personal details form changes
	private static final String EDIT_PERSONAL_DETAILS_CMS_PAGE = "editPersonalPage";
	private static final String EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE = "editNonCCPersonalPage";
	private static final String REPORT_STOLEN_CARD_CMS_PAGE = "reportLostStolenCardPage";
	private static final String LINK_CC_ACCOUNT = "linkExistingClubCardPage";
	private static final String LINK_EMAIL_TO_CC_ACCOUNT = "linkEmailToExistingClubCardPage";

	private static final Logger LOG = Logger.getLogger(AccountPageController.class);

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource
	private CustomerIntegrationProcessor customerIntegrationProcessor;

	@Resource(name = "contactUsProcessor")
	ContactUsProcessor contactUsProcessor;

	@Resource(name = "updateProfile")
	UpdateProfile updateProfile;
	@Resource
	CustomerRegistrationResponseProcessor customerRegistrationResponseProcessor;

	@Resource(name = "contactUsValidator")
	ContactUsValidator contactUsValidator;

	@Resource(name = "editProfilePopulator")
	EditProfilePopulator editProfilePopulator;

	@Resource(name = "customerData")
	CustomerData customerData;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "contactUsBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder contactUsBreadcrumbBuilder;

	@Resource(name = "myAccountFacade")
	private MyAccountFacade myAccountFacade;

	@Resource(name = "clicksCheckoutFacade")
	private ClicksCheckoutFacade clicksCheckoutFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "passwordValidator")
	private PasswordValidator passwordValidator;

	@Resource(name = "addressValidator")
	private AddressValidator addressValidator;

	@Resource(name = "profileValidator")
	private ProfileValidator profileValidator;

	@Resource(name = "emailValidator")
	private EmailValidator emailValidator;

	@Resource(name = "registrationNonClubcardValidator")
	RegistrationNonClubcardValidator registrationNonClubcardValidator;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "clicksCustomerFacade")
	ClicksCustomerFacade clicksCustomerFacade;

	@Resource(name = "addressVerificationFacade")
	private AddressVerificationFacade addressVerificationFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;
	@Resource(name = "linkCCPopulator")
	LinkCCPopulator linkCCPopulator;
	@Resource(name = "editProfileClubcardValidator")
	EditProfileClubcardValidator editProfileClubcardValidator;
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "editProfileNonClubcardValidator")
	EditProfileNonClubcardValidator editProfileNonClubcardValidator;

	@Resource(name = "linkCCAccountProcessor")
	LinkCCAccountProcessor linkCCAccountProcessor;

	@Resource(name = "linkCCToAccountValidator")
	LinkCCToAccountValidator linkCCToAccountValidator;
	@Resource(name = "retrieveLinkCCToAccountValidator")
	RetrieveLinkCCToAccountValidator retrieveLinkCCToAccountValidator;
	@Resource
	private FlexibleSearchService flexibleSearchService;
	CustomerDataPopulator customerDataPopulator;
	Response customerResopnse;
	@Resource
	private MediaService mediaService;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "clamScanner")
	private ClamScanner clamScanner;

	protected PasswordValidator getPasswordValidator()
	{
		return passwordValidator;
	}

	protected AddressValidator getAddressValidator()
	{
		return addressValidator;
	}

	protected ProfileValidator getProfileValidator()
	{
		return profileValidator;
	}

	protected EmailValidator getEmailValidator()
	{
		return emailValidator;
	}

	protected I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}

	protected AddressVerificationFacade getAddressVerificationFacade()
	{
		return addressVerificationFacade;
	}

	protected AddressVerificationResultHandler getAddressVerificationResultHandler()
	{
		return addressVerificationResultHandler;
	}

	/**
	 * Gets the countries.
	 *
	 * @return the countries
	 */
	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return checkoutFacade.getDeliveryCountries();
	}

	/**
	 * Gets the titles.
	 *
	 * @return the titles
	 */
	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	/**
	 * Gets the country data map.
	 *
	 * @return the country data map
	 */
	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<>();
		for (final CountryData countryData : getCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}

	/**
	 * Gets the country list.
	 *
	 * @return the country list
	 */
	@ModelAttribute("country")
	public Collection<de.hybris.clicks.facades.product.data.CountryData> getCountryList()
	{
		Collection<de.hybris.clicks.facades.product.data.CountryData> countryList = new ArrayList<de.hybris.clicks.facades.product.data.CountryData>();
		countryList = myAccountFacade.fetchCountryData(countryList);
		return countryList;
	}


	/**
	 * Gets the topic list.
	 *
	 * @return the topic list
	 */
	@ModelAttribute("Topic")
	public Collection<TopicsData> getTopicList()
	{
		Collection<TopicsData> topicList = new ArrayList<TopicsData>();
		topicList = myAccountFacade.fetchTopicData(topicList, Boolean.FALSE.booleanValue());
		return topicList;
	}

	/**
	 * Gets the province list.
	 *
	 * @return the province list
	 */
	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		provinceList = myAccountFacade.fetchProvinceData(provinceList);
		return provinceList;
	}

	/**
	 * Gets the days.
	 *
	 * @return the days
	 */
	@ModelAttribute("days")
	public Collection<DayData> getDays()
	{
		final Collection<DayData> days = new ArrayList<DayData>();
		DayData day;
		for (int i = 1; i <= 31; i++)
		{
			day = new DayData();
			day.setCode(new Integer(i).toString());
			day.setName(new Integer(i).toString());
			days.add(day);
		}
		return days;
	}

	/**
	 * Gets the years.
	 *
	 * @return the years
	 */
	@ModelAttribute("years")
	public Collection<YearData> getYears()
	{
		final Collection<YearData> years = new ArrayList<YearData>();
		YearData year;
		final Calendar now = Calendar.getInstance();
		final int currentYear = now.get(Calendar.YEAR);
		for (int i = currentYear - 100; i <= currentYear; i++)
		{
			year = new YearData();
			year.setCode(new Integer(i).toString());
			year.setName(new Integer(i).toString());
			years.add(year);
		}
		return years;
	}

	/**
	 * Gets the months.
	 *
	 * @return the months
	 */
	@ModelAttribute("months")
	public Collection<MonthData> getMonths()
	{
		final Collection<MonthData> months = new ArrayList<MonthData>();
		MonthData month;
		String monthString = null;
		for (int i = 1; i <= 12; i++)
		{
			switch (i)
			{
				case 1:
					monthString = "Jan";
					break;
				case 2:
					monthString = "Feb";
					break;
				case 3:
					monthString = "Mar";
					break;
				case 4:
					monthString = "Apr";
					break;
				case 5:
					monthString = "May";
					break;
				case 6:
					monthString = "Jun";
					break;
				case 7:
					monthString = "Jul";
					break;
				case 8:
					monthString = "Aug";
					break;
				case 9:
					monthString = "Sep";
					break;
				case 10:
					monthString = "Oct";
					break;
				case 11:
					monthString = "Nov";
					break;
				case 12:
					monthString = "Dec";
					break;
			}
			month = new MonthData();
			month.setCode(new Integer(i).toString());
			month.setName(monthString);
			months.add(month);
		}

		return months;
	}

	/**
	 * Gets the country address form.
	 *
	 * @param addressCode
	 *           the address code
	 * @param countryIsoCode
	 *           the country iso code
	 * @param model
	 *           the model
	 * @return the country address form
	 */
	@RequestMapping(value = "/addressform", method = RequestMethod.GET)
	public String getCountryAddressForm(@RequestParam("addressCode") final String addressCode,
			@RequestParam("countryIsoCode") final String countryIsoCode, final Model model)
	{
		model.addAttribute("supportedCountries", getCountries());
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIsoCode));
		model.addAttribute("country", countryIsoCode);


		final AddressForm addressForm = new AddressForm();
		model.addAttribute("addressForm", addressForm);
		for (final AddressData addressData : userFacade.getAddressBook())
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode)
					&& countryIsoCode.equals(addressData.getCountry().getIsocode()))
			{
				model.addAttribute("addressData", addressData);
				addressForm.setAddressId(addressData.getId());
				addressForm.setTitleCode(addressData.getTitleCode());
				addressForm.setFirstName(addressData.getFirstName());
				addressForm.setLastName(addressData.getLastName());
				addressForm.setLine1(addressData.getLine1());
				addressForm.setLine2(addressData.getLine2());
				addressForm.setTownCity(addressData.getTown());
				addressForm.setPostcode(addressData.getPostalCode());
				addressForm.setCountryIso(addressData.getCountry().getIsocode());

				if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
				{
					addressForm.setRegionIso(addressData.getRegion().getIsocode());
				}

				break;
			}
		}
		return ControllerConstants.Views.Fragments.Account.CountryAddressForm;
	}

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String account(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Orders.
	 *
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model)
			throws CMSItemNotFoundException
	{
		final int count = Integer.parseInt(Config.getParameter("orderHistory.orders.max.show"));
		model.addAttribute("orderHisCount", count);
		model.addAttribute("recentOrders", myAccountFacade.getRecentOrders(count));
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	@ExceptionHandler(SystemException.class)
	public String handleUnknownIdentifierException(final Exception exception, final HttpServletRequest request, final Model model)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	@RequestMapping(value = "/change-password", method = RequestMethod.GET)
	@RequireHardLogIn
	public String changePassword(final Model model) throws CMSItemNotFoundException
	{
		final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

		model.addAttribute("updatePasswordForm", updatePasswordForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(CHANGE_PASSWORD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHANGE_PASSWORD_CMS_PAGE));


		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	@RequestMapping(value = "/change-password", method = RequestMethod.POST)
	@RequireHardLogIn
	public String changePassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getPasswordValidator().validate(updatePasswordForm, bindingResult);
		if (!bindingResult.hasErrors())
		{
			if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword()))
			{
				try
				{
					customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
				}
				catch (final PasswordMismatchException localException)
				{
					bindingResult.rejectValue("currentPassword", "profile.currentPassword.invalid", new Object[] {},
							"profile.currentPassword.invalid");
				}
			}
			else
			{
				bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[] {},
						"validation.checkPwd.equals");
			}
		}

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(CHANGE_PASSWORD_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHANGE_PASSWORD_CMS_PAGE));

			model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
			return getViewForPage(model);
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.confirmation.password.updated", null);
			final CustomerModel currCustomer = (CustomerModel) userService.getCurrentUser();
			clicksCustomerFacade.processEmail(currCustomer, UPDATE_PASSWORD_EMAIL_PROCESS);
			return REDIRECT_TO_CHANGE_PASSWORD_PAGE;
		}
	}

	/**
	 * Gets the personal details.
	 *
	 * @param model
	 *           the model
	 * @return the personal details
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * populate personal details of a logged in user
	 */
	@RequestMapping(value = "/edit-personal-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getPersonalDetails(final Model model) throws CMSItemNotFoundException
	{
		CustomerData customer = customerFacade.getCurrentCustomer();
		final FileUpload fileUploadForm = new FileUpload();
		model.addAttribute("fileUploadForm", fileUploadForm);
		if (null != customer.getProfilePicture())
		{
			model.addAttribute("profilePicURL", customer.getProfilePicture().getUrl());
		}
		/*
		 * check for club card user
		 */
		if (null != customer.getMemberID())
		{
			EditClubCardForm editClubCardForm = new EditClubCardForm();
			/*
			 * populate club card user details
			 */
			final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
			customer = editProfilePopulator.populateClubCardAccountData(customer, customerModel);
			editClubCardForm = editProfilePopulator.populateClubCardAccountForm(customer, model, editClubCardForm);
			model.addAttribute("editClubCardForm", editClubCardForm);
			model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.editPersonalDetails"));
			storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
			return getViewForPage(model);
		}
		else
		{
			/*
			 * populate non club card user details
			 */
			EditNonClubCardForm editNonClubCardForm = new EditNonClubCardForm();
			editNonClubCardForm = editProfilePopulator.populateNonClubCardData(customer, model, editNonClubCardForm);
			model.addAttribute("editNonClubCardForm", editNonClubCardForm);
			model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.editPersonalDetails"));
			storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE));
			return getViewForPage(model);
		}
	}



	/**
	 * Update personal details.
	 *
	 * @param model
	 *           the model
	 * @param editNonClubCardForm
	 *           the edit non club card form
	 * @param bindingResult2
	 *           the binding result2
	 * @param editClubCardForm
	 *           the edit club card form
	 * @param bindingResult
	 *           the binding result
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * saving profile data to customer model
	 */
	@RequestMapping(value = "/edit-personal-details", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePersonalDetails(final Model model, final EditNonClubCardForm editNonClubCardForm,
			final BindingResult bindingResult2, final EditClubCardForm editClubCardForm, final BindingResult bindingResult,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		CustomerData customerData = new CustomerData();
		final FileUpload fileUploadForm = new FileUpload();
		model.addAttribute("fileUploadForm", fileUploadForm);
		if (null != currentCustomerData.getProfilePicture())
		{
			model.addAttribute("profilePicURL", currentCustomerData.getProfilePicture().getUrl());
		}

		if (currentCustomerData.getUid() != null && editNonClubCardForm.geteMailAddress() == null)
		{
			editNonClubCardForm.seteMailAddress(currentCustomerData.getUid());
		}
		if (currentCustomerData.getUid() != null && editClubCardForm.geteMailAddress() == null)
		{
			editClubCardForm.seteMailAddress(currentCustomerData.getUid());
		}

		if (null != currentCustomerData.getMemberID())
		{
			model.addAttribute("editClubCardForm", editClubCardForm);
			editProfileClubcardValidator.validate(editClubCardForm, bindingResult);
			if (bindingResult.hasErrors())
			{
				GlobalMessages.addErrorMessage(model, "form.global.error.update");
				storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
				model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.editPersonalDetails"));
			}
			else
			{
				customerData.setUid(currentCustomerData.getUid());
				customerData.setDisplayUid(currentCustomerData.getDisplayUid());
				if (editClubCardForm.isEmailIDEditable())
				{
					if (editClubCardForm.getPassword() != null && !editClubCardForm.getPassword().isEmpty())
					{
						try
						{
							myAccountFacade.changeUid(editClubCardForm.geteMailAddress(), editClubCardForm.getPassword());

							final String newUid = customerFacade.getCurrentCustomer().getUid();
							final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
							final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
									newUid, null, oldAuthentication.getAuthorities());
							newAuthentication.setDetails(oldAuthentication.getDetails());
							SecurityContextHolder.getContext().setAuthentication(newAuthentication);
						}
						catch (final DuplicateUidException e)
						{
							setCCPreviousEmailIdDetail(model, editClubCardForm, currentCustomerData);
							GlobalMessages.addErrorMessage(model, "edit.error.account.exists.with.email.address.title");
							getEditCCPersonalPage(model);
							return getViewForPage(model);
						}
						catch (final de.hybris.platform.commerceservices.customer.PasswordMismatchException e)
						{
							setCCPreviousEmailIdDetail(model, editClubCardForm, currentCustomerData);
							GlobalMessages.addErrorMessage(model, "edit.error.account.password.invalid");
							getEditCCPersonalPage(model);
							return getViewForPage(model);
						}
					}
					else
					{
						setCCPreviousEmailIdDetail(model, editClubCardForm, currentCustomerData);
						GlobalMessages.addErrorMessage(model, "edit.error.account.password.invalid");
						getEditCCPersonalPage(model);
						return getViewForPage(model);
					}
				}
				customerData = editProfilePopulator.populateClubCardCustomerData(editClubCardForm, customerData, currentCustomerData);
				final boolean result = updateProfile.saveUpdatedClubCardData(customerData);
				editClubCardForm.setEmailIDEditable(false);
				model.addAttribute("editClubCardForm", editClubCardForm);
				if (result)
				{
					GlobalMessages.addConfMessage(model, "customer.edit.profile.success");
					storeCmsPageInModel(model, getContentPageForLabelOrId(REWARDS_ACTIVITY_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REWARDS_ACTIVITY_PAGE));

					final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("text.account.myClubCard");
					breadCrumbs.get(0).setUrl("/my-account");
					breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("text.account.profile.rewardsActivity"));
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
				}
				else
				{
					GlobalMessages.addErrorMessage(model, "customer.edit.profile.failed");
					storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
				}
			}
			return getViewForPage(model);
		}
		else
		{

			editProfileNonClubcardValidator.validate(editNonClubCardForm, bindingResult2);
			model.addAttribute("editNonClubCardForm", editNonClubCardForm);
			if (bindingResult2.hasErrors())
			{
				GlobalMessages.addErrorMessage(model, "form.global.error.update");
			}


			else
			{
				customerData.setUid(currentCustomerData.getUid());
				customerData.setDisplayUid(currentCustomerData.getDisplayUid());
				if (editNonClubCardForm.isEmailIDEditable())
				{
					if (editNonClubCardForm.getPassword() != null && !editNonClubCardForm.getPassword().isEmpty())
					{
						try
						{
							myAccountFacade.changeUid(editNonClubCardForm.geteMailAddress(), editNonClubCardForm.getPassword());
							final String newUid = customerFacade.getCurrentCustomer().getUid();
							final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
							final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
									newUid, null, oldAuthentication.getAuthorities());
							newAuthentication.setDetails(oldAuthentication.getDetails());
							SecurityContextHolder.getContext().setAuthentication(newAuthentication);
						}
						catch (final DuplicateUidException e)
						{
							setNonCCPreviousEmailIdDetail(model, editNonClubCardForm, currentCustomerData);
							GlobalMessages.addErrorMessage(model, "edit.error.account.exists.with.email.address.title");
							getEditNonCCPersonalPage(model);
							return getViewForPage(model);

						}
						catch (final de.hybris.platform.commerceservices.customer.PasswordMismatchException e)
						{
							setNonCCPreviousEmailIdDetail(model, editNonClubCardForm, currentCustomerData);
							GlobalMessages.addErrorMessage(model, "edit.error.account.password.invalid");
							getEditNonCCPersonalPage(model);
							return getViewForPage(model);
						}
					}
					else
					{
						setNonCCPreviousEmailIdDetail(model, editNonClubCardForm, currentCustomerData);
						GlobalMessages.addErrorMessage(model, "edit.error.account.password.invalid");
						getEditNonCCPersonalPage(model);
						return getViewForPage(model);
					}
				}
				customerData = editProfilePopulator.populateNonClubCardCustomerData(editNonClubCardForm, customerData,
						currentCustomerData);
				editNonClubCardForm.setEmailIDEditable(false);
				model.addAttribute("editNonClubCardForm", editNonClubCardForm);
				final boolean result = updateProfile.saveUpdatedNonClubCardData(customerData);
				if (result)
				{
					GlobalMessages.addConfMessage(model, "customer.edit.profile.success");
				}
				else
				{
					GlobalMessages.addErrorMessage(model, "customer.edit.profile.failed");
				}
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE));
			model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.editPersonalDetails"));
			return getViewForPage(model);
		}
	}

	/**
	 * Load orders.
	 *
	 * @param count
	 *           the count
	 * @return the list
	 */
	@RequestMapping(value = "/loadOrders", method = RequestMethod.GET, produces =
	{ "application/json" })
	@ResponseBody
	public List<OrderHistoryData> loadOrders(@RequestParam(value = "count", defaultValue = "10") final int count)
	{
		try
		{
			LOG.info("++++++++++Entered into load orders method++++++++++++");
			return myAccountFacade.loadOrders(count);

		}
		catch (final Exception e)
		{
			LOG.error("++++++++++Failed to load orders++++++++++++");
		}
		return null;
	}

	/**
	 * Order.
	 *
	 * @param orderCode
	 *           the order code
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String order(@PathVariable("orderCode") final String orderCode, final Model model) throws CMSItemNotFoundException
	{
		try
		{
			final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
			final int divisor = Integer.parseInt(Config.getParameter("order.clubCardPointVal.divisor"));
			model.addAttribute("orderData", orderDetails);
			model.addAttribute("clubCardPointVal", orderDetails.getSubTotal().getValue().intValue() / divisor);
			model.addAttribute("addToCartForm", new AddToCartForm());
			final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("/my-account/orders", getMessageSource().getMessage("text.account.orderHistory", null,
					getI18nService().getCurrentLocale()), null));
			breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.order.orderBreadcrumb", new Object[]
			{ orderDetails.getClicksOrderCode() }, "Order {0}", getI18nService().getCurrentLocale()), null));
			model.addAttribute("breadcrumbs", breadcrumbs);

		}
		catch (final Exception e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String profile(final Model model) throws CMSItemNotFoundException
	{
		final List<TitleData> titles = userFacade.getTitles();

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		if (customerData.getTitleCode() != null)
		{
			model.addAttribute("title", findTitleForCode(titles, customerData.getTitleCode()));
		}

		model.addAttribute("customerData", customerData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	protected TitleData findTitleForCode(final List<TitleData> titles, final String code)
	{
		if (code != null && !code.isEmpty() && titles != null && !titles.isEmpty())
		{
			for (final TitleData title : titles)
			{
				if (code.equals(title.getCode()))
				{
					return title;
				}
			}
		}
		return null;
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editEmail(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final UpdateEmailForm updateEmailForm = new UpdateEmailForm();

		updateEmailForm.setEmail(customerData.getDisplayUid());

		model.addAttribute("updateEmailForm", updateEmailForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateEmail(final UpdateEmailForm updateEmailForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getEmailValidator().validate(updateEmailForm, bindingResult);

		String returnAction = REDIRECT_TO_PROFILE_PAGE;

		if (!bindingResult.hasErrors() && !updateEmailForm.getEmail().equals(updateEmailForm.getChkEmail()))
		{
			bindingResult.rejectValue("chkEmail", "validation.checkEmail.equals", new Object[] {}, "validation.checkEmail.equals");
		}

		if (bindingResult.hasErrors())
		{
			returnAction = errorUpdatingEmail(model);
		}
		else
		{
			try
			{
				customerFacade.changeUid(updateEmailForm.getEmail(), updateEmailForm.getPassword());
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

				// Replace the spring security authentication with the new UID
				final String newUid = customerFacade.getCurrentCustomer().getUid().toLowerCase();
				final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
				final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(newUid, null,
						oldAuthentication.getAuthorities());
				newAuthentication.setDetails(oldAuthentication.getDetails());
				SecurityContextHolder.getContext().setAuthentication(newAuthentication);
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "profile.email.unique");
				returnAction = errorUpdatingEmail(model);
			}
			catch (final PasswordMismatchException passwordMismatchException)
			{
				bindingResult.rejectValue("password", "profile.currentPassword.invalid");
				returnAction = errorUpdatingEmail(model);
			}
		}

		return returnAction;
	}

	protected String errorUpdatingEmail(final Model model) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_EMAIL_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("titleData", userFacade.getTitles());

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final UpdateProfileForm updateProfileForm = new UpdateProfileForm();

		updateProfileForm.setTitleCode(customerData.getTitleCode());
		updateProfileForm.setFirstName(getTitleCase(customerData.getFirstName()));
		updateProfileForm.setLastName(getTitleCase(customerData.getLastName()));

		model.addAttribute("updateProfileForm", updateProfileForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(final UpdateProfileForm updateProfileForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getProfileValidator().validate(updateProfileForm, bindingResult);

		String returnAction = ControllerConstants.Views.Pages.Account.AccountProfileEditPage;
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final CustomerData customerData = new CustomerData();
		customerData.setTitleCode(updateProfileForm.getTitleCode());
		customerData.setFirstName(getTitleCase(updateProfileForm.getFirstName()));
		customerData.setLastName(getTitleCase(updateProfileForm.getLastName()));
		customerData.setUid(getTitleCase(currentCustomerData.getUid()));
		customerData.setDisplayUid(currentCustomerData.getDisplayUid());

		model.addAttribute("titleData", userFacade.getTitles());

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
		}
		else
		{
			try
			{
				customerFacade.updateProfile(customerData);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);
				returnAction = REDIRECT_TO_PROFILE_PAGE;
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				GlobalMessages.addErrorMessage(model, "form.global.error");
			}
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PROFILE_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		return returnAction;
	}

	@RequestMapping(value = "/update-password", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updatePassword(final Model model) throws CMSItemNotFoundException
	{
		final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

		model.addAttribute("updatePasswordForm", updatePasswordForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));

		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-password", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getPasswordValidator().validate(updatePasswordForm, bindingResult);
		if (!bindingResult.hasErrors())
		{
			if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword()))
			{
				try
				{
					customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
				}
				catch (final PasswordMismatchException localException)
				{
					bindingResult.rejectValue("currentPassword", "profile.currentPassword.invalid", new Object[] {},
							"profile.currentPassword.invalid");
				}
			}
			else
			{
				bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[] {},
						"validation.checkPwd.equals");
			}
		}

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));

			model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
			return getViewForPage(model);
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.confirmation.password.updated", null);
			return REDIRECT_TO_PROFILE_PAGE;
		}
	}

	/**
	 * Gets the saved addresses.
	 *
	 * @param model
	 *           the model
	 * @return the saved addresses
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Fetching Saved Delivery Address from address book
	 */
	@RequestMapping(value = "/saved-addresses", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSavedAddresses(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("addressData", userFacade.getAddressBook());
		final AddressForm addressForm = getPreparedAddressForm();
		model.addAttribute("addressForm", addressForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.savedAddresses"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Save addresses.
	 *
	 * @param addressForm
	 *           the address form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Saving Saved Delivery Address to address book
	 */
	@RequestMapping(value = "/saved-addresses", method = RequestMethod.POST)
	@RequireHardLogIn
	public String saveAddresses(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpAddressFormAfterError(addressForm, model);
			return getViewForPage(model);
		}

		final AddressData newAddress = new AddressData();
		//newAddress.setTitleCode(addressForm.getTitleCode());
		final CustomerData currentCustomer = customerFacade.getCurrentCustomer();
		if (null != currentCustomer.getTitleCode())
		{
			newAddress.setTitleCode(currentCustomer.getTitleCode());
		}
		else
		{
			newAddress.setTitleCode(Config.getParameter("address.title.code"));
		}
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setVisibleInAddressBook(true);
		newAddress.setSuburb(addressForm.getSuburb());
		newAddress.setProvince(addressForm.getProvince());
		if (null != addressForm.getEmail())
		{
			newAddress.setEmail(addressForm.getEmail());
		}
		if (null != addressForm.getPhone())
		{
			newAddress.setPhone(addressForm.getPhone());
		}
		if (null != addressForm.getAlternatePhone())
		{
			newAddress.setAlternatePhone(addressForm.getAlternatePhone());
		}
		if (null != addressForm.getDeliveryInstructions())
		{
			newAddress.setDeliveryInstructions(addressForm.getDeliveryInstructions());
		}
		// end - added for personal details

		if (addressForm.getCountryIso() != null && StringUtils.isAlpha(addressForm.getCountryIso()))
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		//newAddress.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));

		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			newAddress.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
		}

		if (Boolean.TRUE.equals(addressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.added");

		if (addressRequiresReview)
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
			storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			return getViewForPage(model);
		}

		userFacade.addAddress(newAddress);

		return REDIRECT_TO_SAVED_ADDRESS_PAGE;
	}

	/**
	 * Gets the address book.
	 *
	 * @param model
	 *           the model
	 * @return the address book
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/address-book", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAddressBook(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("addressData", userFacade.getAddressBook());

		storeCmsPageInModel(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.addressBook"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Adds the address.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * form for adding a new address to the address book
	 */
	@RequestMapping(value = "/add-address", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addAddress(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		final AddressForm addressForm = getPreparedAddressForm();
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		addressForm.setEmail(currentCustomerData.getUid());
		addressForm.setFirstName(currentCustomerData.getFirstName());
		addressForm.setLastName(currentCustomerData.getLastName());
		addressForm.setPhone(currentCustomerData.getContactNumber());
		model.addAttribute("addressForm", addressForm);
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute("isDefaultAddress", Boolean.FALSE);
		storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-account/address-book", getMessageSource().getMessage("text.account.addressBook", null,
				getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	protected AddressForm getPreparedAddressForm()
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final AddressForm addressForm = new AddressForm();
		addressForm.setFirstName(currentCustomerData.getFirstName());
		addressForm.setLastName(currentCustomerData.getLastName());
		addressForm.setTitleCode(currentCustomerData.getTitleCode());
		addressForm.setEmail(currentCustomerData.getUid());
		addressForm.setPhone(currentCustomerData.getContactNumber());
		return addressForm;
	}

	/**
	 * Adds the address.
	 *
	 * @param addressForm
	 *           the address form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Adding a new address to the saved address book
	 */
	@RequestMapping(value = "/add-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpAddressFormAfterError(addressForm, model);
			return getViewForPage(model);
		}

		final AddressData newAddress = new AddressData();
		//newAddress.setTitleCode(addressForm.getTitleCode());
		final CustomerData currentCustomer = customerFacade.getCurrentCustomer();
		if (null != currentCustomer.getTitleCode())
		{
			newAddress.setTitleCode(currentCustomer.getTitleCode());
		}
		else
		{
			newAddress.setTitleCode(Config.getParameter("address.title.code"));
		}
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setVisibleInAddressBook(true);
		newAddress.setSuburb(addressForm.getSuburb());
		newAddress.setProvince(addressForm.getProvince());
		if (null != addressForm.getEmail())
		{
			newAddress.setEmail(addressForm.getEmail());
		}
		if (null != addressForm.getPhone())
		{
			newAddress.setPhone(addressForm.getPhone());
		}
		if (null != addressForm.getAlternatePhone())
		{
			newAddress.setAlternatePhone(addressForm.getAlternatePhone());
		}
		if (null != addressForm.getDeliveryInstructions())
		{
			newAddress.setDeliveryInstructions(addressForm.getDeliveryInstructions());
		}
		// end - added for personal details

		if (addressForm.getCountryIso() != null && StringUtils.isAlpha(addressForm.getCountryIso()))
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		//newAddress.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));

		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			newAddress.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
		}

		if (Boolean.TRUE.equals(addressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.added");

		if (addressRequiresReview)
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
			storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			return getViewForPage(model);
		}

		userFacade.addAddress(newAddress);

		return REDIRECT_TO_SAVED_ADDRESS_PAGE;
	}

	protected void setUpAddressFormAfterError(final AddressForm addressForm, final Model model)
	{
		model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute("isDefaultAddress", Boolean.valueOf(isDefaultAddress(addressForm.getAddressId())));
		if (addressForm.getCountryIso() != null)
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
		}
	}

	/**
	 * Edits the address.
	 *
	 * @param addressCode
	 *           the address code
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/saved-addresses/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddress(@PathVariable("addressCode") final String addressCode, final Model model)
			throws CMSItemNotFoundException
	{
		final AddressForm addressForm = new AddressForm();
		model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		model.addAttribute("addressForm", addressForm);
		addressForm.setIsDeliverable(new Boolean(true));
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));
		final String returnAction = ControllerConstants.Views.Pages.Account.SavedAddressPage;
		final List<AddressData> addressdataList = userFacade.getAddressBook();
		model.addAttribute("addressData", addressdataList);
		for (final AddressData addressData : userFacade.getAddressBook())
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode))
			{
				model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressData.getCountry().getIsocode()));
				model.addAttribute("country", addressData.getCountry().getIsocode());
				model.addAttribute("addrData", addressData);
				addressForm.setAddressId(addressData.getId());
				addressForm.setTitleCode(addressData.getTitleCode());
				addressForm.setFirstName(getTitleCase(addressData.getFirstName()));
				addressForm.setLastName(getTitleCase(addressData.getLastName()));
				addressForm.setLine1(getTitleCase(addressData.getLine1()));
				if (null != addressData.getLine2())
				{
					addressForm.setLine2(getTitleCase(addressData.getLine2()));
				}
				addressForm.setSuburb(addressData.getSuburb());
				if (null != addressData.getProvince())
				{
					addressForm.setProvince(addressData.getProvince());
				}
				if (null != addressData.getTown())
				{
					addressForm.setTownCity(addressData.getTown());
				}
				addressForm.setPostcode(addressData.getPostalCode());
				addressForm.setCountryIso(addressData.getCountry().getIsocode());
				addressForm.setCountryName(addressData.getCountry().getName());
				addressForm.setEmail(addressData.getEmail());
				addressForm.setPhone(addressData.getPhone());
				if (null != clicksCheckoutFacade.getDeliveryInstructionFromCart())
				{
					addressForm.setDeliveryInstructions(clicksCheckoutFacade.getDeliveryInstructionFromCart());
				}
				if (null != addressData.getAlternatePhone())
				{
					addressForm.setAlternatePhone(addressData.getAlternatePhone());
				}
				if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
				{
					addressForm.setRegionIso(addressData.getRegion().getIsocode());
				}
				if (null != addressData.getPreferredContactMethod())
				{
					addressForm.setPreferredContactMethod(addressData.getPreferredContactMethod());
				}

				if (isDefaultAddress(addressData.getId()))
				{
					addressForm.setDefaultAddress(Boolean.TRUE);
					model.addAttribute("isDefaultAddress", Boolean.TRUE);
				}
				else
				{
					addressForm.setDefaultAddress(Boolean.FALSE);
					model.addAttribute("isDefaultAddress", Boolean.FALSE);
				}
				model.addAttribute("addressForm", addressForm);
				break;
			}
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-account/address-book", getMessageSource().getMessage("text.account.addressBook", null,
				getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("edit", Boolean.TRUE);
		return getViewForPage(model);
		//return returnAction;
	}

	/**
	 * Method checks if address is set as default
	 *
	 * @param addressId
	 *           - identifier for address to check
	 * @return true if address is default, false if address is not default
	 */
	protected boolean isDefaultAddress(final String addressId)
	{
		final AddressData defaultAddress = userFacade.getDefaultAddress();
		return (defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressId));
	}

	@RequestMapping(value = "/saved-addresses/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	@RequireHardLogIn
	public String editAddress(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getAddressValidator().validate(addressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpAddressFormAfterError(addressForm, model);
			return getViewForPage(model);
		}
		model.addAttribute("metaRobots", "noindex,nofollow");

		final AddressData newAddress = new AddressData();
		newAddress.setId(addressForm.getAddressId());
		//newAddress.setTitleCode(addressForm.getTitleCode());
		final CustomerData currentCustomer = customerFacade.getCurrentCustomer();
		if (null != currentCustomer.getTitleCode())
		{
			newAddress.setTitleCode(currentCustomer.getTitleCode());
		}
		else
		{
			newAddress.setTitleCode(Config.getParameter("address.title.code"));
		}
		newAddress.setFirstName(getTitleCase(addressForm.getFirstName()));
		newAddress.setLastName(getTitleCase(addressForm.getLastName()));
		newAddress.setLine1(getTitleCase(addressForm.getLine1()));
		newAddress.setLine2(getTitleCase(addressForm.getLine2()));
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setSuburb(addressForm.getSuburb());
		newAddress.setProvince(addressForm.getProvince());

		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setVisibleInAddressBook(true);
		// start - added for personal details

		if (null != addressForm.getEmail())
		{
			newAddress.setEmail(addressForm.getEmail());
		}
		if (null != addressForm.getPhone())
		{
			newAddress.setPhone(addressForm.getPhone());
		}
		if (null != addressForm.getAlternatePhone())
		{
			newAddress.setAlternatePhone(addressForm.getAlternatePhone());
		}
		if (null != addressForm.getDeliveryInstructions())
		{
			newAddress.setDeliveryInstructions(addressForm.getDeliveryInstructions());
		}
		// end - added for personal details

		if (addressForm.getCountryIso() != null && StringUtils.isAlpha(addressForm.getCountryIso()))
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		//newAddress.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));

		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			newAddress.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
		}
		if (null != addressForm.getPreferredContactMethod())
		{
			newAddress.setPreferredContactMethod(addressForm.getPreferredContactMethod());
		}
		if (Boolean.TRUE.equals(addressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		if (addressRequiresReview)
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
			model.addAttribute("edit", Boolean.TRUE);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SAVED_ADDRESSES_CMS_PAGE));
			return getViewForPage(model);
		}
		//userFacade.editAddress(newAddress);
		if (null != newAddress)
		{
			clicksCheckoutFacade.editDeliveryAddress(newAddress);
			model.addAttribute("address", newAddress);
		}
		return REDIRECT_TO_SAVED_ADDRESS_PAGE;
	}

	@RequestMapping(value = "/select-suggested-address", method = RequestMethod.POST)
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils.commaDelimitedListToSet(Config
				.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = new AddressData();
		selectedAddress.setId(addressForm.getAddressId());
		selectedAddress.setTitleCode(addressForm.getTitleCode());
		selectedAddress.setFirstName(addressForm.getFirstName());
		selectedAddress.setLastName(addressForm.getLastName());
		selectedAddress.setLine1(addressForm.getLine1());
		selectedAddress.setLine2(addressForm.getLine2());
		selectedAddress.setTown(addressForm.getTownCity());
		selectedAddress.setPostalCode(addressForm.getPostcode());
		selectedAddress.setBillingAddress(false);
		selectedAddress.setShippingAddress(true);
		selectedAddress.setVisibleInAddressBook(true);

		final CountryData countryData = i18NFacade.getCountryForIsocode(addressForm.getCountryIso());
		selectedAddress.setCountry(countryData);

		if (resolveCountryRegions.contains(countryData.getIsocode()))
		{
			if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
			{
				final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
				selectedAddress.setRegion(regionData);
			}
		}

		if (resolveCountryRegions.contains(countryData.getIsocode()))
		{
			if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
			{
				final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
				selectedAddress.setRegion(regionData);
			}
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			selectedAddress.setDefaultAddress(Boolean.TRUE.equals(addressForm.getDefaultAddress())
					|| userFacade.getAddressBook().size() <= 1);
			userFacade.editAddress(selectedAddress);
		}
		else
		{
			selectedAddress.setDefaultAddress(Boolean.TRUE.equals(addressForm.getDefaultAddress())
					|| userFacade.isAddressBookEmpty());
			userFacade.addAddress(selectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added");

		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	/**
	 * Removes the address.
	 *
	 * @param addressCode
	 *           the address code
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 */
	/*
	 * Removing delivery address from saved address book page
	 */
	@RequestMapping(value = "/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		userFacade.removeAddress(addressData);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		return REDIRECT_TO_SAVED_ADDRESS_PAGE;
	}

	/**
	 * Delete address.
	 *
	 * @param addressCode
	 *           the address code
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 */
	/*
	 * Modifying delivery address from saved address book page
	 */
	@RequestMapping(value = "/saved-addresses/edit-address/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String deleteAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		userFacade.removeAddress(addressData);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		return REDIRECT_TO_SAVED_ADDRESS_PAGE;
	}

	/**
	 * Sets the default address.
	 *
	 * @param addressCode
	 *           the address code
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 */
	@RequestMapping(value = "/set-default-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String setDefaultAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setDefaultAddress(true);
		addressData.setVisibleInAddressBook(true);
		addressData.setId(addressCode);
		userFacade.setDefaultAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.default.address.changed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/payment-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentDetails(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("customerData", customerFacade.getCurrentCustomer());
		model.addAttribute("paymentInfoData", userFacade.getCCPaymentInfos(true));
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.paymentDetails"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	@RequestMapping(value = "/set-default-payment-details", method = RequestMethod.POST)
	@RequireHardLogIn
	public String setDefaultPaymentDetails(@RequestParam final String paymentInfoId)
	{
		CCPaymentInfoData paymentInfoData = null;
		if (StringUtils.isNotBlank(paymentInfoId))
		{
			paymentInfoData = userFacade.getCCPaymentInfoForCode(paymentInfoId);
		}
		userFacade.setDefaultPaymentInfo(paymentInfoData);
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	@RequestMapping(value = "/remove-payment-method", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removePaymentMethod(final Model model, @RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		userFacade.unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	/**
	 * Activate vitality.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Activate vitality Content Page, if not a vitality member can activate using it
	 */
	@RequestMapping(value = "/activate-vitality", method = RequestMethod.GET)
	@RequireHardLogIn
	public String activateVitality(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACTIVATE_VITALITY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACTIVATE_VITALITY_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.activeVitality"));
		return getViewForPage(model);

	}

	/**
	 * My vitality healthcare.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * If the account is linked to vitality health care, this content page will display
	 */
	@RequestMapping(value = "/my-vitality-healthcare", method = RequestMethod.GET)
	@RequireHardLogIn
	public String myVitalityHealthcare(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_VITALITY_HEALTHCARE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_VITALITY_HEALTHCARE_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.myVitalityHealthCare"));
		return getViewForPage(model);

	}

	/**
	 * My rewards activity.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Rewards Activity Page, will display the reward points and related content for the customer
	 */
	@RequestMapping(value = "/my-rewards-activity", method = RequestMethod.GET)
	@RequireHardLogIn
	public String myRewardsActivity(final Model model) throws CMSItemNotFoundException
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		model.addAttribute("cashbackDate", currentSite.getCashbackDate());
		//currentSite.getCashbackDate()
		model.addAttribute("GATrakingCA", sessionService.getAttribute("gaTraking"));
		sessionService.removeAttribute("gaTraking");

		storeCmsPageInModel(model, getContentPageForLabelOrId(REWARDS_ACTIVITY_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REWARDS_ACTIVITY_PAGE));

		final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("text.account.myClubCard");
		breadCrumbs.get(0).setUrl("/my-account");
		breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("text.account.profile.rewardsActivity"));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
		//model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.rewardsActivity"));
		return getViewForPage(model);

	}

	/**
	 * Gets the report loss of card page.
	 *
	 * @param model
	 *           the model
	 * @return the contact us page
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Fetching Report loss of card query form for customer
	 */
	@RequestMapping(value = "/report-card", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getContactUsPage(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		if (null != currentCustomerData.getMemberID())
		{
			final ContactUsForm contactUsForm = new ContactUsForm();
			//final CustomerData customer = customerFacade.getCurrentCustomer();
			contactUsForm.setFirstName(getTitleCase(currentCustomerData.getFirstName()));
			contactUsForm.setLastName(getTitleCase(currentCustomerData.getLastName()));
			contactUsForm.setEmail(getTitleCase(currentCustomerData.getUid()));
			contactUsForm.setClubcardNo(currentCustomerData.getMemberID());
			if (null != currentCustomerData.getContactDetails())
			{
				for (final ContactDetail contact : currentCustomerData.getContactDetails())
				{
					if (contact.getTypeID().equalsIgnoreCase("2"))
					{
						contactUsForm.setContactNo(contact.getNumber());
					}
				}
			}
			contactUsForm.setTopic("02");
			model.addAttribute("contactUsForm", contactUsForm);
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_STOLEN_CARD_CMS_PAGE));
			model.addAttribute("breadcrumbs", contactUsBreadcrumbBuilder.getBreadcrumbs(null));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_STOLEN_CARD_CMS_PAGE));
			return getViewForPage(model);
		}
		else
		{
			return REDIRECT_MY_ACCOUNT;
		}
	}

	/**
	 * Send message.
	 *
	 * @param form
	 *           the form
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Sending Report loss of card query from customer to Clicks team
	 */
	@RequestMapping(value = "/report-card/contactUsProcessor", method = RequestMethod.POST)
	public String sendMessage(final ContactUsForm form, final RedirectAttributes redirectAttributes,
			final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException
	{
		contactUsValidator.validate(form, bindingResult);

		if (contactUsValidator.isFlag())
		{
			GlobalMessages.addErrorMessage(model, "contact.us.enquiry.failed");
		}
		else
		{
			final boolean result = contactUsProcessor.processsContactUsData(form);
			if (!bindingResult.hasErrors() && result)
			{
				GlobalMessages.addConfMessage(model, "contact.us.enquiry.success");
				redirectAttributes.addFlashAttribute("reportCardMessage", "contact.us.enquiry.success");
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, "contact.us.enquiry.success");
			}

			else
			{
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, "contact.us.enquiry.failed", new Object[]
				{ form.getEmail() });
			}

		}
		model.addAttribute("breadcrumbs", contactUsBreadcrumbBuilder.getBreadcrumbs(null));
		storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_STOLEN_CARD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_STOLEN_CARD_CMS_PAGE));
		model.addAttribute(form);
		return getViewForPage(model);
	}

	/**
	 * Gets the link club card.
	 *
	 * @param model
	 *           the model
	 * @return the link club card
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/* Linking club card to an account changes starts */
	@RequestMapping(value = "/link-cc", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getLinkClubCard(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new RegisterCustomerForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(LINK_CC_ACCOUNT));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LINK_CC_ACCOUNT));
		return getViewForPage(model);
	}

	/**
	 * Link club card.
	 *
	 * @param model
	 *           the model
	 * @param registerCustomerForm
	 *           the register customer form
	 * @param bindingResult
	 *           the binding result
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Link an existing account to the club card number using email id and cc number
	 */
	@RequestMapping(value = "/link-cc", method = RequestMethod.POST)
	@RequireHardLogIn
	public String linkClubCard(final Model model, final ClicksRegisterForm registerCustomerForm,
			final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		ClicksRegisterForm registerClubCardForm = new ClicksRegisterForm();
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		customerDataPopulator = new CustomerDataPopulator();
		linkCCToAccountValidator.validate(registerCustomerForm, bindingResult);
		customerResopnse = customerIntegrationProcessor.linkCC_RetrievalRequest(registerCustomerForm, bindingResult, model);
		if (null != customerResopnse && !bindingResult.hasErrors())
		{
			registerClubCardForm = customerDataPopulator.reversePopulateFromResponse(registerClubCardForm, registerCustomerForm,
					bindingResult, customerResopnse, model);
			if (null != registerClubCardForm)
			{
				registerClubCardForm.setUserId(currentCustomerData.getUid());
				//customerRegistrationResponseProcessor.processCustomerResponse(customerResopnse);
				model.addAttribute(registerClubCardForm);
				storeCmsPageInModel(model, getContentPageForLabelOrId(LINK_EMAIL_TO_CC_ACCOUNT));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LINK_EMAIL_TO_CC_ACCOUNT));
			}
			else
			{
				storeCmsPageInModel(model, getContentPageForLabelOrId(LINK_CC_ACCOUNT));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LINK_CC_ACCOUNT));
			}
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "form.register.failed");
			storeCmsPageInModel(model, getContentPageForLabelOrId(LINK_CC_ACCOUNT));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LINK_CC_ACCOUNT));
		}
		return getViewForPage(model);

	}

	/**
	 * Gets the link email to club card.
	 *
	 * @param model
	 *           the model
	 * @return the link email to club card
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Populating linked account details in the form
	 */
	@RequestMapping(value = "/link-email-to-cc", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getLinkEmailToClubCard(final Model model) throws CMSItemNotFoundException
	{

		final RegisterClubCardForm registerClubCardForm = new RegisterClubCardForm();
		model.addAttribute(registerClubCardForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(LINK_EMAIL_TO_CC_ACCOUNT));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LINK_EMAIL_TO_CC_ACCOUNT));
		return getViewForPage(model);
		//return null;
	}

	/**
	 * Link email to club card.
	 *
	 * @param model
	 *           the model
	 * @param registerClubCardForm
	 *           the register club card form
	 * @param bindingResult
	 *           the binding result
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Saving linked account details
	 */
	@RequestMapping(value = "/link-email-to-cc/clubcardProcessor", method = RequestMethod.POST)
	@RequireHardLogIn
	public String linkEmailToClubCard(final Model model, final ClicksRegisterForm registerClubCardForm,
			final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final CustomerData customer = customerFacade.getCurrentCustomer();
		if (null != customer.getCustomerID())
		{
			registerClubCardForm.setCustomerId(customer.getCustomerID());
		}
		if (null != customer.getMemberID())
		{
			registerClubCardForm.setMemberID(customer.getMemberID());
		}
		if (null != customer.getUid())
		{
			registerClubCardForm.setUserId(customer.getUid());
		}
		retrieveLinkCCToAccountValidator.validate(registerClubCardForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
		}
		else
		{
			if (registerClubCardForm.getEmailId() == 1 && null != registerClubCardForm.geteMailAddress())
			{
				customer.setEmailID(registerClubCardForm.geteMailAddress());
				customer.setUid(registerClubCardForm.geteMailAddress());
				customer.setDisplayUid(registerClubCardForm.geteMailAddress());
			}
			else if (registerClubCardForm.getEmailId() == 2 && null != registerClubCardForm.getUserId())
			{
				customer.setEmailID(registerClubCardForm.getUserId());
				customer.setUid(registerClubCardForm.getUserId());
				customer.setDisplayUid(registerClubCardForm.getUserId());
			}
			else
			{
				if (null != registerClubCardForm.geteMailAddress())
				{
					customer.setEmailID(registerClubCardForm.geteMailAddress());
				}
			}
			try
			{
				updateProfile.saveUpdatedClubCardData(customer);
				customerResopnse = customerDataPopulator.updateCustomerResponse(customerResopnse, registerClubCardForm);
				customerRegistrationResponseProcessor.processCustomerResponse(customerResopnse);
				final ResultData result = customerIntegrationProcessor.updateLinkCCRequest(registerClubCardForm, customer,
						bindingResult, model, request, response);
				model.addAttribute(registerClubCardForm);
				if (!bindingResult.hasErrors() && result.getCode().intValue() == 0)
				{
					GlobalMessages.addConfMessage(model, "customer.edit.profile.success");
				}
			}
			catch (final ModelSavingException saveexp)
			{
				bindingResult.reject("uid");
				bindingResult.reject("memberID");
				GlobalMessages.addErrorMessage(model, "form.global.error");
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(LINK_EMAIL_TO_CC_ACCOUNT));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LINK_EMAIL_TO_CC_ACCOUNT));
		}
		return getViewForPage(model);
		/* Saving linked accounts details to model changes ends */
	}

	/**
	 * Upload picture.
	 *
	 * @param fileUploadForm
	 *           the file upload form
	 * @param request
	 *           the request
	 * @param model
	 *           the model
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Uploading profile pic to account page
	 */
	@RequestMapping(value = "/uploadPicture", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	@Transactional
	public String uploadPicture(@ModelAttribute(value = "fileUploadForm") final FileUpload fileUploadForm,
			final HttpServletRequest request, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		File file = null;
		try
		{
			final CatalogVersionModel catalogVersionModelStaged = catalogVersionService.getCatalogVersion("clicksContentCatalog",
					"Staged");
			final CatalogVersionModel catalogVersionModelOnline = catalogVersionService.getCatalogVersion("clicksContentCatalog",
					"Online");
			final MultipartFile multipartFile = fileUploadForm.getFile();
			final String fileName = multipartFile.getOriginalFilename();
			if (StringUtils.isNotBlank(fileName)
					&& (multipartFile.getContentType().contains("png") || multipartFile.getContentType().contains("jpg") || multipartFile
							.getContentType().contains("jpeg"))
					&& multipartFile.getSize() <= Config.getLong("profile.picture.max.file.size", 2097152))
			{
				file = validateFile(redirectAttributes, multipartFile);
				if (null == file || !file.exists() || !file.isFile())
				{
					redirectAttributes.addFlashAttribute("profilePicMessage",
							"Error in changing profile picture. Please select picture again in a mentioned format and size");
					return REDIRECT_MY_ACCOUNT + "/edit-personal-details";
				}
				MediaModel mediaModelOnline = null;
				MediaModel mediaModelStaged = null;
				mediaModelStaged = modelService.create(MediaModel.class);
				mediaModelOnline = modelService.create(MediaModel.class);
				mediaModelStaged.setCatalogVersion(catalogVersionModelStaged);
				mediaModelOnline.setCatalogVersion(catalogVersionModelOnline);
				final String code = "/65Wx65H/" + fileName;
				mediaModelStaged.setCode(code);
				mediaModelOnline.setCode(code);

				//Code to save media in Staged version
				try
				{
					mediaModelStaged = flexibleSearchService.getModelByExample(mediaModelStaged);
				}
				catch (final Exception e)
				{
					LOG.info("Media model not found with the code: " + code);
					if (null == mediaModelStaged.getURL())
					{
						//mediaModelStaged.setMime("image/png");
						mediaModelStaged.setMediaFormat(mediaService.getFormat("65Wx65H"));

					}
				}
				modelService.save(mediaModelStaged);
				modelService.refresh(mediaModelStaged);

				try
				{
					mediaService.setStreamForMedia(mediaModelStaged, new FileInputStream(file));
					modelService.refresh(mediaModelStaged);
					if (!StringUtils.containsIgnoreCase(mediaModelStaged.getMime(),
							Config.getString("profile.pic.allowed.mime", "image")))
					{
						modelService.remove(mediaModelStaged);
						redirectAttributes.addFlashAttribute("profilePicMessage",
								"Error in changing profile picture. Please select picture again in a mentioned format and size");
						return REDIRECT_MY_ACCOUNT + "/edit-personal-details";
					}
					LOG.info("Picture media inserted in Staged version successfully");

					//Code to save media in Online version
					try
					{
						mediaModelOnline = flexibleSearchService.getModelByExample(mediaModelOnline);
					}
					catch (final Exception e)
					{
						LOG.error("Media model not found with the code: " + code);
						if (null == mediaModelOnline.getURL())
						{
							//mediaModelOnline.setMime("image/png");
							mediaModelOnline.setMediaFormat(mediaService.getFormat("65Wx65H"));

						}
					}
					modelService.save(mediaModelOnline);
					try
					{
						mediaService.setStreamForMedia(mediaModelOnline, new FileInputStream(file));
						final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
						customerModel.setProfilePicture(mediaModelOnline);
						modelService.save(customerModel);
						LOG.info("Picture name is:" + fileName);
						redirectAttributes.addFlashAttribute("profilePicMessage", "Picture Uploaded successfully");
					}
					catch (MediaIOException | IllegalArgumentException | IOException e)
					{
						LOG.error("Cannot set Media input stream");
						redirectAttributes.addFlashAttribute("profilePicMessage", "Error in changing profile picture");
					}

				}
				catch (MediaIOException | IllegalArgumentException | IOException e)
				{
					LOG.error("Cannot set Media input stream");
					redirectAttributes.addFlashAttribute("profilePicMessage", "Error in changing profile picture");
				}
			}
			else
			{
				redirectAttributes.addFlashAttribute("profilePicMessage",
						"Error in changing profile picture. Please select picture again in a mentioned format and size");
			}
		}
		finally
		{
			if (null != file && file.exists())
			{
				FileUtils.deleteQuietly(file);
			}
		}
		return REDIRECT_MY_ACCOUNT + "/edit-personal-details";
	}

	/**
	 * @param redirectAttributes
	 * @param multipartFile
	 * @param file
	 * @return
	 */
	private File validateFile(final RedirectAttributes redirectAttributes, final MultipartFile multipartFile)
	{
		File file = null;
		try
		{
			file = new File(Config.getString("profile.pic.scan.temp.file.path", "/home/hybris/hybris/temp/")
					+ multipartFile.getOriginalFilename());
			multipartFile.transferTo(file);
			if (isFileInfected(file))
			{
				FileUtils.deleteQuietly(file);
				return null;
			}
		}
		catch (final Exception e)
		{
			//
		}
		return file;
	}

	/**
	 * Removes the profile picture.
	 *
	 * @param request
	 *           the request
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	/*
	 * Removing profile picture from account page
	 */
	@RequestMapping(value = "/removeProfilePicture", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	@Transactional
	public String removeProfilePicture(final HttpServletRequest request, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		if (null != customerModel && null != customerModel.getProfilePicture()
				&& null != customerModel.getProfilePicture().getCode())
		{
			final String code = customerModel.getProfilePicture().getCode();
			final CatalogVersionModel catalogVersionModelStaged = catalogVersionService.getCatalogVersion(
					Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
			final CatalogVersionModel catalogVersionModelOnline = catalogVersionService.getCatalogVersion(
					Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.online.version"));
			MediaModel removeMediaModelStaged = modelService.create(MediaModel.class);
			MediaModel removeMediaModelOnline = modelService.create(MediaModel.class);

			//Code to remove media from Staged version
			removeMediaModelStaged.setCode(code);
			removeMediaModelStaged.setCatalogVersion(catalogVersionModelStaged);
			try
			{
				removeMediaModelStaged = flexibleSearchService.getModelByExample(removeMediaModelStaged);
				modelService.remove(removeMediaModelStaged);
				try
				{
					//Code to remove media from Online version
					removeMediaModelOnline.setCode(code);
					removeMediaModelOnline.setCatalogVersion(catalogVersionModelOnline);
					removeMediaModelOnline = flexibleSearchService.getModelByExample(removeMediaModelOnline);
					modelService.remove(removeMediaModelOnline);
					redirectAttributes.addFlashAttribute("profilePicMessage", "Profile picture removed successfully");
				}
				catch (final Exception e)
				{
					LOG.error("Not able to remove profile picture");
					redirectAttributes.addFlashAttribute("profilePicMessage", "Not able to remove profile picture. Please try again.");
				}
			}
			catch (final Exception e)
			{
				LOG.error("Not able to remove profile picture");
				redirectAttributes.addFlashAttribute("profilePicMessage", "Not able to remove profile picture. Please try again.");
			}
		}
		else
		{
			redirectAttributes.addFlashAttribute("profilePicMessage",
					"Profile picture is not uploaded yet. Please click on Change photo");
		}
		return REDIRECT_MY_ACCOUNT + "/edit-personal-details";
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	public void setNonCCPreviousEmailIdDetail(final Model model, final EditNonClubCardForm editNonClubCardForm,
			final CustomerData customerData)
	{
		editNonClubCardForm.seteMailAddress(customerData.getUid());
		editNonClubCardForm.setEmailIDEditable(false);
		model.addAttribute("editNonClubCardForm", editNonClubCardForm);
	}

	public void setCCPreviousEmailIdDetail(final Model model, final EditClubCardForm editClubCardForm,
			final CustomerData customerData)
	{
		editClubCardForm.seteMailAddress(customerData.getUid());
		editClubCardForm.setEmailIDEditable(false);
		model.addAttribute("editClubCardForm", editClubCardForm);
	}

	public String getEditNonCCPersonalPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_NON_CC_PERSONAL_DETAILS_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.editPersonalDetails"));
		return getViewForPage(model);
	}

	public String getEditCCPersonalPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EDIT_PERSONAL_DETAILS_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.editPersonalDetails"));
		return getViewForPage(model);
	}


	/**
	 * @param multipartFile
	 */
	private boolean isFileInfected(final File file)
	{
		try
		{
			LOG.info("in isFileInfected for " + file.getName());
			LOG.info("in isFileInfected file size " + file.length());
			if (Config.getBoolean("profile.picture.scan.enabled", true) && clamScanner.scanFile(file.getCanonicalPath()))
			{
				return true;
			}
		}
		catch (final Exception e)
		{
			//
		}
		return false;
	}
}
