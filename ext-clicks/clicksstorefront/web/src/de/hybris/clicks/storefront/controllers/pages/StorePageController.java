/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.resolver.ClicksPageTitleResolver;
import de.hybris.clicks.storefront.util.MetaSanitizerUtil;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.impl.StoreBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.StoreFinderForm;
import de.hybris.platform.clicksstorefrontcommons.forms.StorePositionForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.storefinder.StoreFinderFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.storelocator.exception.GeoLocatorException;
import de.hybris.platform.storelocator.exception.MapServiceException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;


/**
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/**/store")
public class StorePageController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(StorePageController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String STORE_CODE_PATH_VARIABLE_PATTERN = "/{storeCode:.*}";
	private static final String REDIRECT_STORE_FINDER = REDIRECT_PREFIX + "/store-finder";

	private static final String STORE_FINDER_CMS_PAGE_LABEL = "storefinder";
	private static final String GOOGLE_API_KEY_ID = "googleApiKey";
	private static final String GOOGLE_API_VERSION = "googleApiVersion";

	private static final String LAST_LINK_CLASS = "active";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "storeBreadcrumbBuilder")
	private StoreBreadcrumbBuilder storeBreadcrumbBuilder;

	@Resource(name = "storeFinderFacade")
	private StoreFinderFacade storeFinderFacade;

	@ModelAttribute("googleApiVersion")
	public String getGoogleApiVersion()
	{
		return configurationService.getConfiguration().getString(GOOGLE_API_VERSION);
	}

	@ModelAttribute("googleApiKey")
	public String getGoogleApiKey(final HttpServletRequest request)
	{
		final String googleApiKey = getHostConfigService().getProperty(GOOGLE_API_KEY_ID, request.getServerName());
		if (StringUtils.isEmpty(googleApiKey))
		{
			LOG.warn("No Google API key found for server: " + request.getServerName());
		}
		return googleApiKey;
	}

	/**
	 * Store detail.
	 *
	 * @param storeCode
	 *           the store code
	 * @param sourceLatitude
	 *           the source latitude
	 * @param sourceLongitude
	 *           the source longitude
	 * @param locationQuery
	 *           the location query
	 * @param model
	 *           the model
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "**/" + STORE_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String storeDetail(@PathVariable("storeCode") final String storeCode,
			@RequestParam(value = "lat", required = false) final Double sourceLatitude,
			@RequestParam(value = "long", required = false) final Double sourceLongitude,
			@RequestParam(value = "q", required = false) final String locationQuery, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final StoreFinderForm storeFinderForm = new StoreFinderForm();
		model.addAttribute("storeFinderForm", storeFinderForm);
		final StorePositionForm storePositionForm = new StorePositionForm();
		model.addAttribute("storePositionForm", storePositionForm);

		if (sourceLatitude != null && sourceLongitude != null)
		{
			final GeoPoint geoPoint = new GeoPoint();
			geoPoint.setLatitude(sourceLatitude.doubleValue());
			geoPoint.setLongitude(sourceLongitude.doubleValue());

			// Get the point of service data with the formatted distance
			try
			{
				final PointOfServiceData pointOfServiceData = storeFinderFacade.getPointOfServiceForNameAndPosition(storeCode,
						geoPoint);
				if (pointOfServiceData == null)
				{
					return handleStoreNotFoundCase(redirectModel);
				}
				pointOfServiceData.setUrl("/store/" + pointOfServiceData.getDisplayName());
				model.addAttribute("store", pointOfServiceData);

				if (locationQuery != null && !locationQuery.isEmpty())
				{
					model.addAttribute("locationQuery", locationQuery);

					// Build URL to location query
					final String storeFinderSearchUrl = UriComponentsBuilder.fromPath("/store-finder").queryParam("q", locationQuery)
							.build().toString();
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pointOfServiceData, storeFinderSearchUrl));
				}
				else
				{
					// Build URL to position query
					final String storeFinderSearchUrl = UriComponentsBuilder.fromPath("/store-finder/position")
							.queryParam("lat", sourceLatitude).queryParam("long", sourceLongitude).build().toString();
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pointOfServiceData, storeFinderSearchUrl));
				}
				setUpMetaData(model, pointOfServiceData);
			}
			catch (final ModelNotFoundException e)
			{
				return handleStoreNotFoundCase(redirectModel);
			}
		}
		else
		{
			// No source point specified - just lookup the POS by name
			try
			{
				final PointOfServiceData pointOfServiceData = storeFinderFacade.getPointOfServiceForName(storeCode);
				pointOfServiceData.setUrl("/store/" + pointOfServiceData.getName());
				model.addAttribute("store", pointOfServiceData);
				model.addAttribute(WebConstants.BREADCRUMBS_KEY, storeBreadcrumbBuilder.getBreadcrumbs(pointOfServiceData));
				setUpMetaData(model, pointOfServiceData);
			}
			catch (final ModelNotFoundException e)
			{
				return handleStoreNotFoundCase(redirectModel);
			}
		}

		storeCmsPageInModel(model, getStoreFinderPage());
		return ControllerConstants.Views.Pages.StoreFinder.StoreFinderDetailsPage;
	}

	/**
	 * Sets the up meta data.
	 *
	 * @param model
	 *           the model
	 * @param pointOfServiceData
	 *           the point of service data
	 */
	protected void setUpMetaData(final Model model, final PointOfServiceData pointOfServiceData)
	{
		final String metaKeywords = createMetaKeywords(pointOfServiceData);
		//		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(pointOfServiceData.getDescription());
		final StringBuilder descBuilder = new StringBuilder();
		descBuilder.append("Find directions, physical address, trading hours and contact information for Clicks ");
		descBuilder.append(pointOfServiceData.getDisplayName());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(descBuilder.toString());
		setUpMetaData(model, metaKeywords, metaDescription);
	}

	/**
	 * View map.
	 *
	 * @param storeCode
	 *           the store code
	 * @param model
	 *           the model
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws GeoLocatorException
	 *            the geo locator exception
	 * @throws MapServiceException
	 *            the map service exception
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = STORE_CODE_PATH_VARIABLE_PATTERN + "/map", method = RequestMethod.GET)
	public String viewMap(@PathVariable("storeCode") final String storeCode, final Model model,
			final RedirectAttributes redirectModel) throws GeoLocatorException, MapServiceException, CMSItemNotFoundException
	{
		final StoreFinderForm storeFinderForm = new StoreFinderForm();
		model.addAttribute("storeFinderForm", storeFinderForm);
		final StorePositionForm storePositionForm = new StorePositionForm();
		model.addAttribute("storePositionForm", storePositionForm);

		try
		{
			final PointOfServiceData pointOfServiceData = storeFinderFacade.getPointOfServiceForName(storeCode);
			pointOfServiceData.setUrl("/store/" + pointOfServiceData.getName());
			model.addAttribute("store", pointOfServiceData);

			storeCmsPageInModel(model, getStoreFinderPage());
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					storeBreadcrumbBuilder.getBreadcrumbsForSubPage(pointOfServiceData, "storeDetails.map.link"));
		}
		catch (final ModelNotFoundException e)
		{
			return handleStoreNotFoundCase(redirectModel);
		}
		return ControllerConstants.Views.Pages.StoreFinder.StoreFinderViewMapPage;
	}

	/**
	 * Handle store not found case.
	 *
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 */
	protected String handleStoreNotFoundCase(final RedirectAttributes redirectModel)
	{
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "storelocator.error.no.results.title");
		return REDIRECT_STORE_FINDER;
	}

	protected AbstractPageModel getStoreFinderPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(STORE_FINDER_CMS_PAGE_LABEL);
	}

	/**
	 * Creates the meta keywords.
	 *
	 * @param pointOfServiceData
	 *           the point of service data
	 * @return the string
	 */
	protected String createMetaKeywords(final PointOfServiceData pointOfServiceData)
	{
		final AddressData address = pointOfServiceData.getAddress();

		final String[] keywords =
		{ address.getTown(), address.getPostalCode(), address.getCountry().getName() };
		return StringUtils.join(keywords, ',');
	}

	/**
	 * Gets the breadcrumbs.
	 *
	 * @param pointOfServiceData
	 *           the point of service data
	 * @param storeFinderUrl
	 *           the store finder url
	 * @return the breadcrumbs
	 * @throws IllegalArgumentException
	 *            the illegal argument exception
	 */
	public List<Breadcrumb> getBreadcrumbs(final PointOfServiceData pointOfServiceData, final String storeFinderUrl)
			throws IllegalArgumentException
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final String storeFinderLinkName = getMessageSource().getMessage("storeFinder.link", null,
				getI18nService().getCurrentLocale());
		final String resolvedStoreFinderUrl = storeFinderUrl == null ? "/store-finder" : storeFinderUrl;
		breadcrumbs.add(new Breadcrumb(resolvedStoreFinderUrl, storeFinderLinkName, null));

		breadcrumbs.add(new Breadcrumb("#", pointOfServiceData.getDisplayName(), null));

		if (!breadcrumbs.isEmpty())
		{
			breadcrumbs.get(breadcrumbs.size() - 1).setLinkClass(LAST_LINK_CLASS);
		}

		return breadcrumbs;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController#storeCmsPageInModel(org.
	 * springframework.ui.Model, de.hybris.platform.cms2.model.pages.AbstractPageModel)
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		if (model != null && cmsPage != null)
		{
			model.addAttribute(CMS_PAGE_MODEL, cmsPage);
			if (cmsPage instanceof ContentPageModel)
			{
				storeContentPageTitleInModel(model,
						((ClicksPageTitleResolver) getPageTitleResolver()).resolveContentPageTitle(cmsPage.getTitle(), true));
			}
		}
	}


}
