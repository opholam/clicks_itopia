/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.platform.clicksstorefrontcommons.forms.RegisterClubCardForm;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates registration forms.
 */

public class RetrieveLinkCCToAccountValidator implements Validator
{
	//public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.NAME_REGEX;

	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterClubCardForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegisterClubCardForm registerClubCardForm = (RegisterClubCardForm) object;
		final String memberID = registerClubCardForm.getMemberID();
		final String eMailAddress = registerClubCardForm.geteMailAddress();
		final String addressLine1 = registerClubCardForm.getAddressLine1();
		final String country = registerClubCardForm.getCountry();
		final String firstName = registerClubCardForm.getFirstName();
		final String lastName = registerClubCardForm.getLastName();
		final String contactNumber = registerClubCardForm.getContactNumber();
		final String suburb = registerClubCardForm.getSuburb();
		final String postalCode = registerClubCardForm.getPostalCode();
		final String IDNumber = registerClubCardForm.getIDNumber();
		if (StringUtils.isEmpty(memberID))
		{
			registerClubCardForm.setMemberID(messageSource.getMessage("register.CC.IDNumber.empty", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("memberID", "register.CC.IDNumber.empty");
		}
		if (!ValidatorUtils.validateRegex(EMAIL_REGEX, eMailAddress))
		{
			registerClubCardForm.seteMailAddress(messageSource.getMessage("register.email.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("eMailAddress", "register.email.invalid");
		}
		else if (StringUtils.length(eMailAddress) > 50)
		{
			registerClubCardForm.seteMailAddress(messageSource.getMessage("register.email.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("eMailAddress", "register.email.invalid");
		}

		if (StringUtils.isEmpty(addressLine1))
		{
			registerClubCardForm.setAddressLine1(messageSource.getMessage("register.enroll.addressLine1.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		else if (StringUtils.length(addressLine1) > 30)
		{
			registerClubCardForm.seteMailAddress(messageSource.getMessage("register.enroll.addressLine1.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		if (StringUtils.isEmpty(country))
		{
			registerClubCardForm.setCountry(messageSource.getMessage("register.enroll.country.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			registerClubCardForm.setFirstName(messageSource.getMessage("register.CC.firstname.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			registerClubCardForm.seteMailAddress(messageSource.getMessage("register.CC.firstname.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			registerClubCardForm.setLastName(messageSource.getMessage("register.CC.lastname.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}
		else if (StringUtils.length(lastName) > 30)
		{
			registerClubCardForm.seteMailAddress(messageSource.getMessage("register.CC.lastname.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}
		if (!ValidatorUtils.validateRegex(NUMBER_REGEX, contactNumber))
		{
			registerClubCardForm.setContactNumber(messageSource.getMessage("register.CC.contactNo.empty", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("contactNumber", "register.CC.contactNo.empty");
		}
		if (StringUtils.isEmpty(suburb))
		{
			registerClubCardForm.setSuburb(messageSource.getMessage("register.enroll.suburb.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		if (StringUtils.isEmpty(postalCode))
		{
			registerClubCardForm.setPostalCode(messageSource.getMessage("register.enroll.postalCode.invalid", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		}
		if (StringUtils.isEmpty(IDNumber))
		{
			registerClubCardForm.setIDNumber(messageSource.getMessage("register.CC.IDNumber.empty", null,
					i18nService.getCurrentLocale()));
			errors.rejectValue("IDNumber", "register.CC.IDNumber.empty");
		}
	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

}
