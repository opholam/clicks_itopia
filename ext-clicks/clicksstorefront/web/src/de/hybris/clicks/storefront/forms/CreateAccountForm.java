/**
 *
 */
package de.hybris.clicks.storefront.forms;

/**
 * @author abhaydh
 *
 */
public class CreateAccountForm
{

	private int isClubCard;
	private int isSAResident;
	private Boolean joinClubCard;

	private String clubcardNumber;
	private String SA_IDNumber;

	private String SA_DOB_day;
	private String SA_DOB_month;
	private String SA_DOB_year;

	private String SA_DOB;

	/**
	 * @return the isClubCard
	 */
	public int getIsClubCard()
	{
		return isClubCard;
	}

	/**
	 * @param isClubCard
	 *           the isClubCard to set
	 */
	public void setIsClubCard(final int isClubCard)
	{
		this.isClubCard = isClubCard;
	}

	/**
	 * @return the isSAResident
	 */
	public int getIsSAResident()
	{
		return isSAResident;
	}

	/**
	 * @param isSAResident
	 *           the isSAResident to set
	 */
	public void setIsSAResident(final int isSAResident)
	{
		this.isSAResident = isSAResident;
	}


	/**
	 * @return the clubcardNumber
	 */
	public String getClubcardNumber()
	{
		return clubcardNumber;
	}

	/**
	 * @param clubcardNumber
	 *           the clubcardNumber to set
	 */
	public void setClubcardNumber(final String clubcardNumber)
	{
		this.clubcardNumber = clubcardNumber;
	}

	/**
	 * @return the joinClubCard
	 */
	public Boolean getJoinClubCard()
	{
		return joinClubCard;
	}

	/**
	 * @param joinClubCard
	 *           the joinClubCard to set
	 */
	public void setJoinClubCard(final Boolean joinClubCard)
	{
		this.joinClubCard = joinClubCard;
	}

	/**
	 * @return the sA_IDNumber
	 */
	public String getSA_IDNumber()
	{
		return SA_IDNumber;
	}

	/**
	 * @param sA_IDNumber
	 *           the sA_IDNumber to set
	 */
	public void setSA_IDNumber(final String sA_IDNumber)
	{
		SA_IDNumber = sA_IDNumber;
	}

	/**
	 * @return the sA_DOB_day
	 */
	public String getSA_DOB_day()
	{
		return SA_DOB_day;
	}

	/**
	 * @param sA_DOB_day
	 *           the sA_DOB_day to set
	 */
	public void setSA_DOB_day(final String sA_DOB_day)
	{
		SA_DOB_day = sA_DOB_day;
	}

	/**
	 * @return the sA_DOB_month
	 */
	public String getSA_DOB_month()
	{
		return SA_DOB_month;
	}

	/**
	 * @param sA_DOB_month
	 *           the sA_DOB_month to set
	 */
	public void setSA_DOB_month(final String sA_DOB_month)
	{
		SA_DOB_month = sA_DOB_month;
	}

	/**
	 * @return the sA_DOB_year
	 */
	public String getSA_DOB_year()
	{
		return SA_DOB_year;
	}

	/**
	 * @param sA_DOB_year
	 *           the sA_DOB_year to set
	 */
	public void setSA_DOB_year(final String sA_DOB_year)
	{
		SA_DOB_year = sA_DOB_year;
	}

	/**
	 * @return the sA_DOB
	 */
	public String getSA_DOB()
	{
		return SA_DOB;
	}

	/**
	 * @param sA_DOB
	 *           the sA_DOB to set
	 */
	public void setSA_DOB(final String sA_DOB)
	{
		SA_DOB = sA_DOB;
	}


}
