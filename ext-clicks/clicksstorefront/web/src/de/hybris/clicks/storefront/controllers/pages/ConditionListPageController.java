/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * The Class ConditionListPageController.
 * 
 * @author sridhar.reddy
 */
@Controller
@Scope("tenant")
public class ConditionListPageController extends AbstractPageController
{
	private static final String CONDITION_LIST_PAGE = "conditionList";
	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;


	/**
	 * Condition list.
	 * 
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/conditionList", method = RequestMethod.GET)
	public String conditionList(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String type = request.getParameter("type");
		final String name = request.getParameter("name");

		storeCmsPageInModel(model, getContentPageForLabelOrId(CONDITION_LIST_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CONDITION_LIST_PAGE));

		if (StringUtils.isNotEmpty(type))
		{
			getBasePageBreadcum(type, model, name);
		}
		else
		{
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					contentPageBreadcrumbBuilder.getBreadcrumbs(getContentPageForLabelOrId(CONDITION_LIST_PAGE)));
		}
		return getViewForPage(model);
	}

	/**
	 * Gets the base page breadcum.
	 * 
	 * @param type
	 *           the type
	 * @param model
	 *           the model
	 * @param name
	 *           the name
	 * @return the base page breadcum
	 */
	private void getBasePageBreadcum(final String type, final Model model, final String name)
	{
		String pageTitle = "";
		String metaDescription = "";
		if (type.contains("MEDICINES"))
		{

			pageTitle = getMessageSource().getMessage("text.conditionList.medicines.pageTitle", new Object[]
			{ name }, "{0}", getI18nService().getCurrentLocale()); //"Medicines associated with " + name + " | Clicks Health Hub";

			metaDescription = getMessageSource().getMessage("text.conditionList.medicines.metaDescription", new Object[]
			{ name }, "{0}", getI18nService().getCurrentLocale());//	"Read more about medicines associated with " + name + ". Clicks Health Hub";

			if (StringUtils.isNotBlank(name) && "all".equalsIgnoreCase(name))
			{
				pageTitle = getMessageSource().getMessage("text.conditionList.medicines.all", null,
						getI18nService().getCurrentLocale()); //"All Medicines | Clicks";

				metaDescription = getMessageSource().getMessage("text.conditionList.medicines.all.metaDescription", null,
						getI18nService().getCurrentLocale()); // "Find a list of all medicine information in the Clicks Health Hub";

				if (type.contains("BY_TYPE"))
				{
					pageTitle = getMessageSource().getMessage("text.conditionList.medicines.bytype", null,
							getI18nService().getCurrentLocale()); //"Medicines by type | Clicks";

					metaDescription = getMessageSource().getMessage("text.conditionList.medicines.bytype.metaDescription", null,
							getI18nService().getCurrentLocale()); // "Medicines segmented by type";
				}
				else if (type.contains("BY_CONDITION"))
				{
					pageTitle = getMessageSource().getMessage("text.conditionList.medicines.bycondition", null,
							getI18nService().getCurrentLocale()); //"Medicines by condition | Clicks";

					metaDescription = getMessageSource().getMessage("text.conditionList.medicines.bycondition.metaDescription", null,
							getI18nService().getCurrentLocale()); //"Medicines to be used by certain condition types";
				}
			}

			final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.medicinearticle");
			breadCrumbs.get(0).setUrl("/medicinesPage");
			//			breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.medicines.medicinesList"));
			final Breadcrumb medicineName = new Breadcrumb("#", name, "active");
			breadCrumbs.add(medicineName);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
		}
		else if (type.contains("VITAMINSNSUPPLEMENTS"))
		{
			pageTitle = getMessageSource().getMessage("text.conditionList.Supplements.pageTitle", new Object[]
			{ name }, "{0}", getI18nService().getCurrentLocale()); //"Supplements associated with " + name + " | Clicks Health Hub";

			metaDescription = getMessageSource().getMessage("text.conditionList.Supplements.metaDescription", new Object[]
			{ name }, "{0}", getI18nService().getCurrentLocale()); // "Read more about supplements associated with " + name + ". Clicks Health Hub";

			if (StringUtils.isNotBlank(name) && "all".equalsIgnoreCase(name))
			{
				pageTitle = getMessageSource().getMessage("text.conditionList.Supplements.all", null,
						getI18nService().getCurrentLocale()); // "All Vitamins & Supplements | Clicks";

				metaDescription = getMessageSource().getMessage("text.conditionList.supplements.all.metaDescription", null,
						getI18nService().getCurrentLocale()); //"Find a list of all vitamins and supplements in the Clicks Health Hub";

				if (type.contains("WELLBEING"))
				{
					pageTitle = getMessageSource().getMessage("text.conditionList.Supplements.wellbeing", null,
							getI18nService().getCurrentLocale()); // "Vitamins & Supplements for well-being | Clicks";

					metaDescription = getMessageSource().getMessage("text.conditionList.Supplements.wellbeing.metaDescription", null,
							getI18nService().getCurrentLocale()); // "Find a list of Vitamins & Supplements for well-being in the Clicks Health Hub";
				}
				else if (type.contains("CONDITION"))
				{
					pageTitle = getMessageSource().getMessage("text.conditionList.Supplements.condition", null,
							getI18nService().getCurrentLocale()); //"Vitamins & Supplements for certain conditions | Clicks";

					metaDescription = getMessageSource().getMessage("text.conditionList.Supplements.condition.metaDescription", null,
							getI18nService().getCurrentLocale()); //"Vitamins & Supplements for certain conditions in the Clicks Health Hub";
				}
			}
			final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.vitNDsup");
			breadCrumbs.get(0).setUrl("/health/info-list/category/vitamins-supplements");
			//			breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.vNs.vNssList"));
			final Breadcrumb vsName = new Breadcrumb("#", name, "active");
			breadCrumbs.add(vsName);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
		}
		else if (type.contains("CONDITION"))
		{
			pageTitle = getMessageSource().getMessage("text.conditionList.condition.pageTitle", new Object[]
			{ name }, "{0}", getI18nService().getCurrentLocale()); // "Conditions associated with " + name + " | Clicks Health Hub";

			metaDescription = getMessageSource().getMessage("text.conditionList.condition.metaDescription", new Object[]
			{ name }, "{0}", getI18nService().getCurrentLocale());// "Read more about conditions associated with " + name + ". Clicks Health Hub";

			if (StringUtils.isNotBlank(name) && "all".equalsIgnoreCase(name))
			{
				pageTitle = getMessageSource().getMessage("text.conditionList.condition.all", null,
						getI18nService().getCurrentLocale()); // "All Conditions | Clicks";

				metaDescription = getMessageSource().getMessage("text.conditionList.condition.all.metaDescription", null,
						getI18nService().getCurrentLocale()); // "Find a list of all conditions in the Clicks Health Hub";

				if (type.contains("BY_TYPE"))
				{
					pageTitle = getMessageSource().getMessage("text.conditionList.condition.bytpe", null,
							getI18nService().getCurrentLocale()); // "Conditions by type | Clicks";

					metaDescription = getMessageSource().getMessage("text.conditionList.condition.bytpe.metaDescription", null,
							getI18nService().getCurrentLocale()); // "Conditions segmented by type";
				}
				else if (type.contains("BY_BODY_AREA"))
				{
					pageTitle = getMessageSource().getMessage("text.conditionList.condition.byBodyArea", null,
							getI18nService().getCurrentLocale()); //"Conditions by body area | Clicks";

					metaDescription = getMessageSource().getMessage("text.conditionList.condition.byBodyArea.metaDescription", null,
							getI18nService().getCurrentLocale()); // "Find a list of conditions associated with a particular body area";
				}
			}
			final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.conditions");
			breadCrumbs.get(0).setUrl("/health/info-list/category/conditions");
			//			breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.conditions.conditionList"));
			final Breadcrumb conditionName = new Breadcrumb("#", name, "active");
			breadCrumbs.add(conditionName);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
		}

		updatePageTitle(model, pageTitle);
		setUpMetaData(model, "", metaDescription);
	}

	/**
	 * Update page title.
	 * 
	 * @param model
	 *           the model
	 * @param pageTitle
	 *           the page title
	 */
	protected void updatePageTitle(final Model model, final String pageTitle)
	{
		//		storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(pageTitle));
		model.addAttribute(CMS_PAGE_TITLE, pageTitle);
	}
}
