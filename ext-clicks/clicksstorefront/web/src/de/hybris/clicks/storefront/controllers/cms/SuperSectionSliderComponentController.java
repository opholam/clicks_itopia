/**
 * 
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.model.SliderLinkComponentModel;
import de.hybris.clicks.core.model.SuperSectionSliderComponentModel;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author sreedhar
 * 
 */
@Controller("SuperSectionSliderComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.SuperSectionSliderComponent)
public class SuperSectionSliderComponentController<T extends AbstractItemModel> extends
		AbstractCMSComponentController<SuperSectionSliderComponentModel>
{
	static Logger log = Logger.getLogger(ConditionListComponentController.class.getName());

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final SuperSectionSliderComponentModel component)
	{
		try
		{
			final List<String> urls = new ArrayList<String>();
			final List<SliderLinkComponentModel> sliderLinkComponentModel = component.getSliderContent();
			for (final SliderLinkComponentModel ssModel : sliderLinkComponentModel)
			{
				urls.add(ssModel.getUrl());
			}
			model.addAttribute("sliderUrls", urls);
		}
		catch (final Exception e)
		{
			log.debug(e.getMessage());
		}
	}
}
