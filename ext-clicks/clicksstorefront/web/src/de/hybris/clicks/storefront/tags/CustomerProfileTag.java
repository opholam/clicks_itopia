/**
 *
 */
package de.hybris.clicks.storefront.tags;

import de.hybris.platform.commercefacades.user.data.SegmentData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 *
 */
public class CustomerProfileTag extends TagSupport
{

	private static final Logger LOG = Logger.getLogger(CustomerProfileTag.class);

	private List<SegmentData> segments;
	private static final String BABY_CLUB_CARD_ID = "100020";
	private static final String SENIOR_CLUB_CARD_ID = "100007";
	private static final String GOLD_CLUB_CARD_ID = "100011";
	private static final String BLUE_CLUB_CARD_ID = "100012";

	private static final String BLUE_CLUB_CARD_MEMBER = "Blue ClubCard member";
	private static final String GOLD_CLUB_CARD_MEMBER = "Gold ClubCard member";
	private static final String SENIOR_CLUB_CARD_MEMBER = "Senior ClubCard member";
	private static final String BABY_CLUB_CARD_MEMBER = "Baby ClubCard member";

	@Override
	public int doStartTag() throws JspException
	{
		LOG.debug("Calling CustomerProfileTag");
		LOG.debug("Segments are : " + segments);
		try
		{
			String output = "";
			//Get the writer object for output.
			final JspWriter out = pageContext.getOut();
			final List<String> segmentIds = new ArrayList<String>(2);
			if (CollectionUtils.isNotEmpty(segments))
			{
				for (final SegmentData segmentData : segments)
				{
					LOG.debug("Segment Id's associated to User are : " + segmentData.getSegmentID());
					segmentIds.add(String.valueOf(segmentData.getSegmentID()));
				}
			}

			if (CollectionUtils.isNotEmpty(segmentIds))
			{
				if (segmentIds.contains(GOLD_CLUB_CARD_ID))
				{
					if (segmentIds.contains(BABY_CLUB_CARD_ID))
					{
						output = BABY_CLUB_CARD_MEMBER;
					}
					else if (segmentIds.contains(SENIOR_CLUB_CARD_ID))
					{
						output = SENIOR_CLUB_CARD_MEMBER;
					}
					else
					{
						output = GOLD_CLUB_CARD_MEMBER;
					}
				}
				else if (segmentIds.contains(BLUE_CLUB_CARD_ID))
				{
					if (segmentIds.contains(BABY_CLUB_CARD_ID))
					{
						output = BABY_CLUB_CARD_MEMBER;
					}
					else if (segmentIds.contains(SENIOR_CLUB_CARD_ID))
					{
						output = SENIOR_CLUB_CARD_MEMBER;
					}
					else
					{
						output = BLUE_CLUB_CARD_MEMBER;
					}
				}
				else if (segmentIds.contains(SENIOR_CLUB_CARD_ID))
				{
					output = SENIOR_CLUB_CARD_MEMBER;
				}
				else if (segmentIds.contains(BABY_CLUB_CARD_ID))
				{
					output = BLUE_CLUB_CARD_MEMBER;
				}
			}
			out.println(output);
		}
		catch (final IOException e)
		{
			LOG.error("IO Operation error while writing to JspWriter" + e.getMessage());
		}
		return SKIP_BODY;
	}


	/**
	 * @return the segments
	 */
	public List<SegmentData> getSegments()
	{
		return segments;
	}

	/**
	 * @param segments
	 *           the segments to set
	 */
	public void setSegments(final List<SegmentData> segments)
	{
		this.segments = segments;
	}
}
