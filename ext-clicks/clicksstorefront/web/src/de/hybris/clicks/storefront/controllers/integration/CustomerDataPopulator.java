/**
 *
 */
package de.hybris.clicks.storefront.controllers.integration;

import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsentCommsType;
import Omni.BizTalk.Clicks.Schema.Customer.Response;


/**
 * @author abhaydh
 *
 */
public class CustomerDataPopulator
{
	private static final Logger LOG = Logger.getLogger(CustomerDataPopulator.class);

	@SuppressWarnings("deprecation")
	public CustomerData populateEnrollData(final ClicksRegisterForm form, final CustomerData customer)
	{

		final List<AddressData> addresses = new ArrayList<AddressData>();
		final AddressData address = new AddressData();

		if (null != form.getAddressLine1())
		{
			address.setLine1(getTitleCase(form.getAddressLine1()));
		}
		if (null != form.getAddressLine2())
		{
			address.setLine2(getTitleCase(form.getAddressLine2()));
		}
		if (null != form.getAddressTypeId())
		{
			address.setTypeID(form.getAddressTypeId());
		}
		if (null != form.getSuburb())
		{
			address.setSuburb(getTitleCase(form.getSuburb()));
		}
		if (null != form.getCity())
		{
			address.setCity(getTitleCase(form.getCity()));
		}

		if (null != form.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setIsocode(form.getCountry());
			address.setCountry(countryData);
		}

		if (null != form.getPostalCode())
		{
			address.setPostalCode(form.getPostalCode());
		}
		if (null != form.getProvince())
		{
			address.setProvince(form.getProvince());
		}

		addresses.add(address);
		if (addresses.size() > 0)
		{
			customer.setAddresses(addresses);
		}

		if (null != form.getFirstName())
		{
			customer.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getPreferedName())
		{
			customer.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		if (null != form.getLastName())
		{
			customer.setLastName(getTitleCase(form.getLastName()));
		}
		customer.setGender(new Integer(form.getGender()).toString());
		if (null != form.geteMailAddress())
		{
			customer.setEmailID(getTitleCase(form.geteMailAddress()));
		}
		if (form.getIsSAResident() == 0)
		{
			customer.setSAResident(Boolean.FALSE);
		}
		if (form.getIsSAResident() == 1)
		{
			customer.setSAResident(Boolean.TRUE);
		}
		if (null != form.getIDNumber())
		{
			customer.setIDnumber(form.getIDNumber());
		}

		if (StringUtils.isNotBlank(form.getHybrisRetailerId()))
		{
			customer.setRetailerID_hybris(form.getHybrisRetailerId());
		}
		else
		{
			customer.setRetailerID_hybris("1");
		}
		if (null != form.getCustomerId())
		{
			customer.setCustomerID(form.getCustomerId());
		}
		if (null != form.getMemberID())
		{
			customer.setMemberID(form.getMemberID());
		}
		if (null != form.getMemberIDPlasticCard())
		{
			customer.setMemberID(form.getMemberIDPlasticCard());
		}

		if (null != form.getSA_DOB_day() && StringUtils.isNotEmpty(form.getSA_DOB_day()) && null != form.getSA_DOB_month()
				&& StringUtils.isNotEmpty(form.getSA_DOB_month()) && null != form.getSA_DOB_year()
				&& StringUtils.isNotEmpty(form.getSA_DOB_year()))
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			customer.setNonRSA_DOB(new Date(date));
		}

		customer.setMarketingConsent_email(new Boolean(form.isMarketingEmail()));
		customer.setMarketingConsent_SMS(new Boolean(form.isMarketingSMS()));
		customer.setAccinfo_email(new Boolean(form.isAccinfoEmail()));
		customer.setAccinfo_SMS(new Boolean(form.isAccinfoSMS()));

		if (StringUtils.isNotBlank(form.getContactNumber()) || StringUtils.isNotBlank(form.getAlternateNumber()))
		{
			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
			if (StringUtils.isNotBlank(form.getContactNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getContactNumber());
				contact.setTypeID("2");
				contactDetailList.add(contact);
			}
			if (StringUtils.isNotBlank(form.getAlternateNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getAlternateNumber());
				contact.setTypeID("4");
				contactDetailList.add(contact);
			}
			if (contactDetailList.size() > 0)
			{
				customer.setContactDetails(contactDetailList);
			}
		}
		return customer;
	}

	@SuppressWarnings("deprecation")
	public CustomerData populateRegisterCCData(final ClicksRegisterForm form, final CustomerData customer)
	{
		if (null != form.getCustomerId())
		{
			customer.setCustomerID(form.getCustomerId());
		}
		if (StringUtils.isNotBlank(form.getHybrisRetailerId()))
		{
			customer.setRetailerID_hybris(form.getHybrisRetailerId());
		}
		else
		{
			customer.setRetailerID_hybris("1");
		}
		customer.setSAResident(new Boolean(form.isSAResident()));
		if (null != form.getIDNumber())
		{
			customer.setIDnumber(form.getIDNumber());
		}

		if (form.getEmailId() == 1)
		{
			customer.setEmailID(getTitleCase(form.geteMailAddress()));
			customer.setUid(getTitleCase(form.geteMailAddress()));
		}
		else if (form.getEmailId() == 2)
		{
			customer.setEmailID(getTitleCase(form.getUserId()));
			customer.setUid(form.getUserId());
		}
		else
		{
			customer.setEmailID(getTitleCase(form.geteMailAddress()));
		}

		if (null != form.getLoyaltyRetailerId())
		{
			customer.setRetailerID_Loyalty(form.getLoyaltyRetailerId());
		}
		if (null != form.getMemberID())
		{
			customer.setMemberID(form.getMemberID());
		}
		if (null != form.getSA_DOB_day() && StringUtils.isNotEmpty(form.getSA_DOB_day()) && null != form.getSA_DOB_month()
				&& StringUtils.isNotEmpty(form.getSA_DOB_month()) && null != form.getSA_DOB_year()
				&& StringUtils.isNotEmpty(form.getSA_DOB_year()))
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			customer.setNonRSA_DOB(new Date(date));
		}
		final List<AddressData> addresses = new ArrayList<AddressData>();
		final AddressData address = new AddressData();

		if (null != form.getAddressLine1())
		{
			address.setLine1(getTitleCase(form.getAddressLine1()));
		}
		if (null != form.getAddressLine2())
		{
			address.setLine2(getTitleCase(form.getAddressLine2()));
		}
		if (null != form.getAddressTypeId())
		{
			address.setTypeID(form.getAddressTypeId());
		}
		if (null != form.getSuburb())
		{
			address.setSuburb(getTitleCase(form.getSuburb()));
		}
		if (null != form.getCity())
		{
			address.setCity(getTitleCase(form.getCity()));
		}

		if (null != form.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setIsocode(form.getCountry());
			address.setCountry(countryData);
		}

		if (null != form.getPostalCode())
		{
			address.setPostalCode(form.getPostalCode());
		}
		if (null != form.getProvince())
		{
			address.setProvince(form.getProvince());
		}

		addresses.add(address);
		if (addresses.size() > 0)
		{
			customer.setAddresses(addresses);
		}

		if (null != form.getFirstName())
		{
			customer.setFirstName(getTitleCase(form.getFirstName()));
		}
		if (null != form.getPreferedName())
		{
			customer.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		if (null != form.getLastName())
		{
			customer.setLastName(getTitleCase(form.getLastName()));
		}
		customer.setGender(new Integer(form.getGender()).toString());

		if (form.getIsSAResident() == 0)
		{
			customer.setSAResident(Boolean.FALSE);
		}
		if (form.getIsSAResident() == 1)
		{
			customer.setSAResident(Boolean.TRUE);
		}
		if (null != form.getIDNumber())
		{
			customer.setIDnumber(form.getIDNumber());
		}

		if (StringUtils.isNotBlank(form.getHybrisRetailerId()))
		{
			customer.setRetailerID_hybris(form.getHybrisRetailerId());
		}
		else
		{
			customer.setRetailerID_hybris("1");
		}
		if (null != form.getCustomerId())
		{
			customer.setCustomerID(form.getCustomerId());
		}

		customer.setMarketingConsent_email(new Boolean(form.isMarketingEmail()));
		customer.setMarketingConsent_SMS(new Boolean(form.isMarketingSMS()));
		customer.setAccinfo_email(new Boolean(form.isAccinfoEmail()));
		customer.setAccinfo_SMS(new Boolean(form.isAccinfoSMS()));

		if (StringUtils.isNotBlank(form.getContactNumber()) || StringUtils.isNotBlank(form.getAlternateNumber()))
		{
			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
			if (StringUtils.isNotBlank(form.getContactNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getContactNumber());
				contact.setTypeID("2");
				contactDetailList.add(contact);
			}
			if (StringUtils.isNotBlank(form.getAlternateNumber()))
			{
				final ContactDetail contact = new ContactDetail();
				contact.setNumber(form.getAlternateNumber());
				contact.setTypeID("4");
				contactDetailList.add(contact);
			}
			if (contactDetailList.size() > 0)
			{
				customer.setContactDetails(contactDetailList);
			}
		}
		return customer;
	}

	@SuppressWarnings("deprecation")
	public CustomerData populateRegisterNonCCData(final ClicksRegisterForm form, final CustomerData customer)
	{
		customer.setFirstName(getTitleCase(form.getFirstName()));
		if (null != form.getPreferedName())
		{
			customer.setPreferedName(getTitleCase(form.getPreferedName()));
		}
		customer.setLastName(getTitleCase(form.getLastName()));
		customer.setGender(new Integer(form.getGender()).toString());
		customer.setEmailID(getTitleCase(form.geteMailAddress()));
		customer.setMarketingConsent_email(new Boolean(form.isMarketingEmailNonCC()));

		if (form.getIsSAResident() == 0)
		{
			customer.setSAResident(Boolean.FALSE);
		}
		if (form.getIsSAResident() == 1)
		{
			customer.setSAResident(Boolean.TRUE);
		}
		if (null != form.getSA_DOB_day() && StringUtils.isNotEmpty(form.getSA_DOB_day()) && null != form.getSA_DOB_month()
				&& StringUtils.isNotEmpty(form.getSA_DOB_month()) && null != form.getSA_DOB_year()
				&& StringUtils.isNotEmpty(form.getSA_DOB_year()))
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			customer.setNonRSA_DOB(new Date(date));
		}
		customer.setIDnumber(form.getIDNumber());
		if (StringUtils.isNotBlank(form.getHybrisRetailerId()))
		{
			customer.setRetailerID_hybris(form.getHybrisRetailerId());
		}
		else
		{
			customer.setRetailerID_hybris("1");
		}
		customer.setCustomerID(form.getCustomerId());

		return customer;
	}

	@SuppressWarnings("deprecation")
	public CustomerData populateDetailsRequestData(final ClicksRegisterForm form, final CustomerData customer)
	{
		if (null != form.getMemberID())
		{
			customer.setMemberID(form.getMemberID());
		}
		if (null != form.geteMailAddress())
		{
			customer.setEmail(getTitleCase(form.geteMailAddress()));
		}
		if (form.getIsSAResident() == 0)
		{
			customer.setSAResident(Boolean.FALSE);
		}
		if (form.getIsSAResident() == 1)
		{
			customer.setSAResident(Boolean.TRUE);
		}
		if (null != form.getIDNumber())
		{
			customer.setIDnumber(form.getIDNumber());
		}
		if (null != form.getSA_DOB_day() && StringUtils.isNotEmpty(form.getSA_DOB_day()) && null != form.getSA_DOB_month()
				&& StringUtils.isNotEmpty(form.getSA_DOB_month()) && null != form.getSA_DOB_year()
				&& StringUtils.isNotEmpty(form.getSA_DOB_year()))
		{
			final String date = form.getSA_DOB_month() + "/" + form.getSA_DOB_day() + "/" + form.getSA_DOB_year();
			customer.setNonRSA_DOB(new Date(date));
		}
		return customer;
	}


	public ClicksRegisterForm reversePopulateEnrollForm(final ClicksRegisterForm registerCCForm, final CustomerData customer)
	{
		if (null != customer)
		{
			if (null != customer.getMemberID())
			{
				registerCCForm.setMemberID(customer.getMemberID());
			}
			if (null != customer.getRetailerID_hybris())
			{
				registerCCForm.setHybrisRetailerId(customer.getRetailerID_hybris());
			}
			if (null != customer.getCustomerID())
			{
				registerCCForm.setCustomerId(customer.getCustomerID());
			}
			try
			{
				if (null != customer.getAddresses() & customer.getAddresses().size() > 0)
				{
					for (final AddressData address : customer.getAddresses())
					{
						if (null != address.getLine1())
						{
							registerCCForm.setAddressLine1(getTitleCase(address.getLine1()));
						}
						if (null != address.getLine2())
						{
							registerCCForm.setAddressLine2(getTitleCase(address.getLine2()));
						}
						if (null != address.getCity())
						{
							registerCCForm.setCity(getTitleCase(address.getCity()));
						}
						if (null != address.getCountry().getIsocode())
						{
							registerCCForm.setCountry(address.getCountry().getIsocode());
						}
						if (null != address.getPostalCode())
						{
							registerCCForm.setPostalCode(address.getPostalCode());
						}
						if (null != address.getProvince())
						{
							registerCCForm.setProvince(address.getProvince());
						}
						if (null != address.getSuburb())
						{
							registerCCForm.setSuburb(getTitleCase(address.getSuburb()));
						}
					}
				}
			}
			catch (final Exception e)
			{
				LOG.error(e);
			}
			if (null != customer.getFirstName())
			{
				registerCCForm.setFirstName(getTitleCase(customer.getFirstName()));
			}
			if (null != customer.getLastName())
			{
				registerCCForm.setLastName(getTitleCase(customer.getLastName()));
			}
			if (null != customer.getPreferedName())
			{
				registerCCForm.setPreferedName(getTitleCase(customer.getPreferedName()));
			}
			if (null != customer.getEmailID())
			{
				registerCCForm.seteMailAddress(getTitleCase(customer.getEmailID()));
			}
			if (null != customer.getGender())
			{
				registerCCForm.setGender(Integer.parseInt(customer.getGender()));
			}


			if (null != customer.getMarketingConsent_email())
			{
				registerCCForm.setMarketingEmail(customer.getMarketingConsent_email().booleanValue());
			}
			if (null != customer.getMarketingConsent_SMS())
			{
				registerCCForm.setMarketingSMS(customer.getMarketingConsent_SMS().booleanValue());
			}
			if (null != customer.getAccinfo_email())
			{
				registerCCForm.setAccinfoEmail(customer.getAccinfo_email().booleanValue());
			}
			if (null != customer.getAccinfo_SMS())
			{
				registerCCForm.setAccinfoSMS(customer.getAccinfo_SMS().booleanValue());
			}

			if (null != customer.getSAResident())
			{
				if (customer.getSAResident().booleanValue())
				{
					registerCCForm.setSAResident(customer.getSAResident().booleanValue());
				}
			}
			if (null != customer.getIDnumber())
			{
				registerCCForm.setIDNumber(customer.getIDnumber());
			}
			if (null != customer.getNonRSA_DOB())
			{
				registerCCForm.setDOB(String.valueOf(customer.getNonRSA_DOB().getTime()));
			}

			if (null != customer.getContactDetails())
			{
				for (final ContactDetail contact : customer.getContactDetails())
				{
					if (contact.getTypeID().equalsIgnoreCase("2"))
					{
						registerCCForm.setContactNumber(contact.getNumber());
					}
					if (contact.getTypeID().equalsIgnoreCase("4"))
					{
						registerCCForm.setAlternateNumber(contact.getNumber());
					}
				}
			}

		}
		return registerCCForm;
	}



	public ClicksRegisterForm reversePopulateFromResponse(final ClicksRegisterForm registerCCForm,
			final ClicksRegisterForm registerCustomerForm, final BindingResult bindingResult, final Response customer,
			final Model model)
	{
		if (null != customer)
		{
			if (null != customer.getBody().getCustomer())
			{
				if (null != customer.getBody().getCustomer().getLoyalty().getMemberId())
				{
					registerCCForm.setMemberID(customer.getBody().getCustomer().getLoyalty().getMemberId());
				}
				if (null != customer.getBody().getCustomer().getHybris().getRetailerId())
				{
					registerCCForm.setHybrisRetailerId(customer.getBody().getCustomer().getHybris().getRetailerId());
				}
				if (null != customer.getBody().getCustomer().getHybris().getCustomerId())
				{
					registerCCForm.setCustomerId(customer.getBody().getCustomer().getHybris().getCustomerId());
				}
				try
				{
					if (null != customer.getBody().getCustomer().getCoreDetails().getAddresses()
							& customer.getBody().getCustomer().getCoreDetails().getAddresses().length > 0)
					{
						for (final CustomerCoreDetailsAddressesAddress address : customer.getBody().getCustomer().getCoreDetails()
								.getAddresses())
						{
							if (null != address.getAddressLine1())
							{
								registerCCForm.setAddressLine1(getTitleCase(address.getAddressLine1()));
							}
							if (null != address.getAddressLine2())
							{
								registerCCForm.setAddressLine2(getTitleCase(address.getAddressLine2()));
							}
							if (null != address.getCity())
							{
								registerCCForm.setCity(getTitleCase(address.getCity()));
							}
							if (null != address.getCountry())
							{
								registerCCForm.setCountry(address.getCountry());
							}
							if (null != address.getPostalCode())
							{
								registerCCForm.setPostalCode(address.getPostalCode());
							}
							if (null != address.getProvince())
							{
								registerCCForm.setProvince(address.getProvince());
							}
							if (null != address.getSuburb())
							{
								registerCCForm.setSuburb(getTitleCase(address.getSuburb()));
							}
						}
					}
				}
				catch (final Exception e)
				{
					LOG.error(e);
				}
				if (null != customer.getBody().getCustomer().getCoreDetails().getFirstName())
				{
					registerCCForm.setFirstName(customer.getBody().getCustomer().getCoreDetails().getFirstName());
				}
				if (null != customer.getBody().getCustomer().getCoreDetails().getLastName())
				{
					registerCCForm.setLastName(customer.getBody().getCustomer().getCoreDetails().getLastName());
				}
				if (null != customer.getBody().getCustomer().getCoreDetails().getPreferedName())
				{
					registerCCForm.setPreferedName(customer.getBody().getCustomer().getCoreDetails().getPreferedName());
				}
				if (null != customer.getBody().getCustomer().getCoreDetails().getEMailAddress())
				{
					registerCCForm.seteMailAddress(customer.getBody().getCustomer().getCoreDetails().getEMailAddress());
				}
				registerCCForm.setGender(customer.getBody().getCustomer().getCoreDetails().getGender().getValue());


				if (null != customer.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent()
						&& customer.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent().length > 0)
				{
					for (final CustomerLoyaltyOptInsMarketingConsentConsent consent : customer.getBody().getCustomer().getLoyalty()
							.getOptIns().getMarketingConsent())
					{
						if (consent.getConsentType().equalsIgnoreCase("ACCINFO"))
						{
							for (final CustomerLoyaltyOptInsMarketingConsentConsentCommsType type : consent.getComms())
							{
								if (type.getTypeId() == 1)
								{
									registerCCForm.setAccinfoSMS(type.isAccepted());
								}
								if (type.getTypeId() == 2)
								{
									registerCCForm.setAccinfoEmail(type.isAccepted());
								}
							}
						}
						if (consent.getConsentType().equalsIgnoreCase("MARKETING"))
						{
							for (final CustomerLoyaltyOptInsMarketingConsentConsentCommsType type : consent.getComms())
							{
								if (type.getTypeId() == 1)
								{
									registerCCForm.setMarketingSMS(type.isAccepted());
								}
								if (type.getTypeId() == 2)
								{
									registerCCForm.setMarketingEmail(type.isAccepted());
								}
							}
						}

					}
				}


				registerCCForm.setSAResident(customer.getBody().getCustomer().getCoreDetails().isSAResident());

				if (null != customer.getBody().getCustomer().getCoreDetails().getIDNumber())
				{
					registerCCForm.setIDNumber(customer.getBody().getCustomer().getCoreDetails().getIDNumber());
				}
				if (null != customer.getBody().getCustomer().getCoreDetails().getDateOfBirth())
				{
					registerCCForm
							.setDOB(String.valueOf(customer.getBody().getCustomer().getCoreDetails().getDateOfBirth().getTime()));
				}

				if (null != customer.getBody().getCustomer().getCoreDetails().getContactDetails()
						& customer.getBody().getCustomer().getCoreDetails().getContactDetails().length > 0)
				{
					for (final CustomerCoreDetailsContactDetailsContactDetail contact : customer.getBody().getCustomer()
							.getCoreDetails().getContactDetails())
					{

						if (null != contact.getTypeId() && contact.getTypeId().equalsIgnoreCase("2"))
						{
							if (null != contact.getNumber())
							{
								registerCCForm.setContactNumber(contact.getNumber());
							}
						}
						if (null != contact.getTypeId() && contact.getTypeId().equalsIgnoreCase("4"))
						{
							if (null != contact.getNumber())
							{
								registerCCForm.setAlternateNumber(contact.getNumber());
							}
						}
					}
				}
			}
			else
			{
				if (null != customer.getBody().getMessage())
				{
					bindingResult.rejectValue("clubcardNumber", "register.CC.clubcard.empty");
					model.addAttribute(registerCustomerForm);
					GlobalMessages.addErrorMessage(model, customer.getBody().getMessage());
				}
				else
				{
					bindingResult.rejectValue("clubcardNumber", "register.CC.clubcard.empty");
					model.addAttribute(registerCustomerForm);
					GlobalMessages.addErrorMessage(model, "form.clubcard.invalid.error");
				}
				return null;
			}
		}
		return registerCCForm;
	}

	public Response updateCustomerResponse(final Response customerResopnse, final ClicksRegisterForm registerClubCardForm)
	{
		if (null != registerClubCardForm)
		{
			if (null != registerClubCardForm.getMemberID())
			{
				customerResopnse.getBody().getCustomer().getLoyalty().setMemberId((registerClubCardForm.getMemberID()));
			}

			try
			{
				if (null != registerClubCardForm.getAddressLine1())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0].setAddressLine1(registerClubCardForm
							.getAddressLine1());
				}
				if (null != registerClubCardForm.getAddressLine2())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0].setAddressLine2(registerClubCardForm
							.getAddressLine2());
				}
				if (null != registerClubCardForm.getCity())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0]
							.setCity(registerClubCardForm.getCity());
				}
				if (null != registerClubCardForm.getCountry())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0].setCountry(registerClubCardForm
							.getCountry());
				}
				if (null != registerClubCardForm.getPostalCode())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0].setPostalCode(registerClubCardForm
							.getPostalCode());
				}
				if (null != registerClubCardForm.getProvince())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0].setProvince(registerClubCardForm
							.getProvince());
				}
				if (null != registerClubCardForm.getSuburb())
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().getAddresses()[0].setSuburb(registerClubCardForm
							.getSuburb());
				}

			}
			catch (final Exception e)
			{
				LOG.error(e);
			}
			if (null != registerClubCardForm.getFirstName())
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setFirstName(registerClubCardForm.getFirstName());
			}
			if (null != registerClubCardForm.getLastName())
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setLastName(registerClubCardForm.getLastName());
			}
			if (null != registerClubCardForm.getPreferedName())
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setPreferedName(registerClubCardForm.getPreferedName());
			}
			if (registerClubCardForm.getEmailId() == 1 && null != registerClubCardForm.geteMailAddress())
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setEMailAddress(registerClubCardForm.geteMailAddress());
			}
			else if (registerClubCardForm.getEmailId() == 2 && null != registerClubCardForm.getUserId())
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setEMailAddress(registerClubCardForm.getUserId());
			}
			else
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setEMailAddress(registerClubCardForm.geteMailAddress());
			}

			if (registerClubCardForm.getGender() == 1)
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setGender(CustomerCoreDetailsGender.value1);
			}
			if (registerClubCardForm.getGender() == 2)
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setGender(CustomerCoreDetailsGender.value2);
			}

			if (customerResopnse.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent().length > 0)
			{
				for (final CustomerLoyaltyOptInsMarketingConsentConsent marketingConsent : customerResopnse.getBody().getCustomer()
						.getLoyalty().getOptIns().getMarketingConsent())
				{
					if (marketingConsent.getConsentType().equalsIgnoreCase("ACCINFO"))
					{
						if (marketingConsent.getComms().length > 0)
						{
							for (final CustomerLoyaltyOptInsMarketingConsentConsentCommsType type : marketingConsent.getComms())
							{
								if (type.getTypeId() == 1)
								{
									if (registerClubCardForm.isAccinfoSMS())
									{
										type.setAccepted(true);
									}
									else
									{
										type.setAccepted(false);
									}
								}
								if (type.getTypeId() == 2)
								{
									if (registerClubCardForm.isAccinfoEmail())
									{
										type.setAccepted(true);
									}
									else
									{
										type.setAccepted(false);
									}
								}
							}
						}
					}
					if (marketingConsent.getConsentType().equalsIgnoreCase("MARKETING"))
					{
						if (marketingConsent.getComms().length > 0)
						{
							for (final CustomerLoyaltyOptInsMarketingConsentConsentCommsType type : marketingConsent.getComms())
							{
								if (type.getTypeId() == 1)
								{
									if (registerClubCardForm.isMarketingSMS())
									{
										type.setAccepted(true);
									}
									else
									{
										type.setAccepted(false);
									}
								}
								if (type.getTypeId() == 2)
								{
									if (registerClubCardForm.isMarketingEmail())
									{
										type.setAccepted(true);
									}
									else
									{
										type.setAccepted(false);
									}
								}
							}
						}
					}

				}
			}

			customerResopnse.getBody().getCustomer().getCoreDetails().setSAResident(registerClubCardForm.isSAResident());

			if (null != registerClubCardForm.getIDNumber())
			{
				customerResopnse.getBody().getCustomer().getCoreDetails().setIDNumber(registerClubCardForm.getIDNumber());
			}
			if (null != registerClubCardForm.getDOB())
			{
				try
				{
					customerResopnse.getBody().getCustomer().getCoreDetails().setDateOfBirth(new Date(registerClubCardForm.getDOB()));
				}
				catch (final Exception e)
				{
					LOG.error(e);
				}
			}

			if (customerResopnse.getBody().getCustomer().getCoreDetails().getContactDetails().length > 0)
			{
				for (final CustomerCoreDetailsContactDetailsContactDetail conatct : customerResopnse.getBody().getCustomer()
						.getCoreDetails().getContactDetails())
				{
					if (conatct.getTypeId().equalsIgnoreCase("2"))
					{
						if (null != registerClubCardForm.getContactNumber())
						{
							conatct.setNumber(registerClubCardForm.getContactNumber());
						}
					}
					if (conatct.getTypeId().equalsIgnoreCase("4"))
					{
						if (null != registerClubCardForm.getAlternateNumber())
						{
							conatct.setNumber(registerClubCardForm.getAlternateNumber());
						}
					}

				}
			}
		}
		return customerResopnse;
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}
}
