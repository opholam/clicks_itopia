/**
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.model.MedicineSupplementHeaderComponentModel;
import de.hybris.clicks.facade.hpcondition.HpConditionFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author Swapnil Desai
 *
 */
@Controller("MedicineSupplementHeaderComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.MedicineSupplementHeaderComponent)
public class MedicineSupplementHeaderComponentController<T extends AbstractItemModel> extends
		AbstractCMSComponentController<MedicineSupplementHeaderComponentModel>
{
	static Logger log = Logger.getLogger(MedicineSupplementHeaderComponentController.class.getName());
	@Resource(name = "hpConditionFacade")
	private HpConditionFacade hpConditionFacade;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final MedicineSupplementHeaderComponentModel component)
	{
		final String id = request.getParameter("id");
		final String type = request.getParameter("type");
		log.info("Entered into MedicineSupplementHeaderComponentController id: " + id + " type: " + type);
		if (StringUtils.isNotBlank(id))
		{
			//			final HMedicinesVSModel hMedicinesVSModel = hpConditionFacade.getMedicinesAndSupplements(id, type);
			//			model.addAttribute("headerForCond", hMedicinesVSModel.getName());
		}
	}

	@Override
	protected String getView(final MedicineSupplementHeaderComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix
				+ StringUtils.lowerCase(MedicineSupplementHeaderComponentModel._TYPECODE);
	}
}
