/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.core.model.ConditionsStageModel;
import de.hybris.clicks.facades.article.ConditionsFacade;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.annotation.Resource;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author baskar.lakshmanan
 *
 */



@RequestMapping("/articles")
public class ConditionsPageController extends AbstractPageController
{
	@Resource(name = "conditionsFacade")
	private ConditionsFacade conditionsFacade;

	private static final String CONDITIONS_ARITCLE = "/conditions/{conditionCode}";
	private static final String CONDITIONS_BY_STAGE = "/conditionsbystage/{conditionCode}";
	private static final String CONDITIONS_BY_AREA = "/conditionsbyarea/{conditionCode}";

	@RequestMapping(value = CONDITIONS_ARITCLE, method = RequestMethod.GET)
	public String getConditionsDetail(@PathVariable("conditionCode") final String conditionCode, final Model model)
			throws CMSItemNotFoundException
	{

		final ConditionsModel conditionsModel = conditionsFacade.getConditionDetail(conditionCode);

		if (conditionsModel != null)
		{
			model.addAttribute("errorMsg", null);
			model.addAttribute("conditionsData", conditionsModel);
		}
		else
		{
			model.addAttribute("errorMsg", "Requested Condition Is Not Available");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId("conditionsPage"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("conditionsPage"));

		return getViewForPage(model);
	}

	@RequestMapping(value = CONDITIONS_BY_STAGE, method = RequestMethod.GET)
	public String getConditionsByStageDetail(@PathVariable("conditionCode") final String conditionCode, final Model model)
			throws CMSItemNotFoundException
	{

		final ConditionsStageModel conditionsStageModel = conditionsFacade.getConditionByStageDetail(conditionCode);

		if (conditionsStageModel != null)
		{
			model.addAttribute("errorMsg", null);
			model.addAttribute("conditionsData", conditionsStageModel);
		}
		else
		{
			model.addAttribute("errorMsg", "Requested conditionsStageModel Is Not Available");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId("conditionsPage"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("conditionsPage"));

		return getViewForPage(model);
	}

	@RequestMapping(value = CONDITIONS_BY_AREA, method = RequestMethod.GET)
	public String getConditionsByArea(@PathVariable("conditionCode") final String conditionCode, final Model model)
			throws CMSItemNotFoundException
	{

		final ConditionsAreaModel conditionsAreaModel = conditionsFacade.getConditionByAreaDetail(conditionCode);

		if (conditionsAreaModel != null)
		{
			model.addAttribute("errorMsg", null);
			model.addAttribute("conditionsData", conditionsAreaModel);
		}
		else
		{
			model.addAttribute("errorMsg", "Requested Condition Is Not Available");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId("conditionsPage"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("conditionsPage"));

		return getViewForPage(model);
	}

}
