/**
 *
 */
package de.hybris.clicks.storefront.controllers.misc;


import de.hybris.platform.clicksstorefrontcommons.controllers.AbstractController;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author abhaydh
 *
 */
public class ClicksCustomerController extends AbstractController
{
	private static final String ERROR_MSG_TYPE = "errorMsg";

	@RequestMapping(value = "/customer/enrollCustomer", method = RequestMethod.POST)
	public String addToCart(@RequestParam("customer") final String code, final Model model, final BindingResult bindingErrors)
	{
		if (bindingErrors.hasErrors())
		{
			return getViewWithBindingErrorMessages(model, bindingErrors);
		}
		return "pages/views/customer/error";
	}

	protected String getViewWithBindingErrorMessages(final Model model, final BindingResult bindingErrors)
	{
		for (final ObjectError error : bindingErrors.getAllErrors())
		{
			model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
		}
		return "pages/views/customer/error";
	}


}
