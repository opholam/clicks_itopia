/**
 *
 */
package de.hybris.clicks.storefront.resolver;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 *
 */
public class ClicksPageTitleResolver extends PageTitleResolver
{

	private static final Logger LOG = Logger.getLogger(ClicksPageTitleResolver.class);

	private static final String SEPARATOR = "-";
	private static final String SPACE = " ";

	@Override
	public String resolveProductPageTitle(final ProductModel product)
	{

		LOG.info("Calling Clicks Product Page Title Resolver");

		// Lookup site (or store)
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		// Construct page title
		final String identifier = product.getName();
		final String articleNumber = product.getCode();
		final String productName = StringUtils.isEmpty(identifier) ? articleNumber : identifier;
		String brandName = "";

		if (product instanceof ClicksApparelSizeVariantModel)
		{
			final ClicksApparelSizeVariantModel sizeVariantModel = (ClicksApparelSizeVariantModel) product;
			final ApparelStyleVariantProductModel apparelStyleModel = (ApparelStyleVariantProductModel) sizeVariantModel
					.getBaseProduct();
			if (null != apparelStyleModel.getBaseProduct())
			{
				for (final CategoryModel categoryModel : apparelStyleModel.getBaseProduct().getSupercategories())
				{
					for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
					{
						if ("brands".equalsIgnoreCase(innerCategoryModel.getCode()))
						{
							brandName = categoryModel.getName();
						}
					}
				}
			}
		}
		else if (product instanceof ApparelStyleVariantProductModel)
		{
			final ApparelStyleVariantProductModel styleVariantModel = (ApparelStyleVariantProductModel) product;
			if (null != styleVariantModel.getBaseProduct())
			{
				for (final CategoryModel categoryModel : styleVariantModel.getBaseProduct().getSupercategories())
				{
					for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
					{
						if ("brands".equalsIgnoreCase(innerCategoryModel.getCode()))
						{
							brandName = categoryModel.getName();
						}
					}
				}
			}
		}

		StringBuilder builder = new StringBuilder(productName);

		if (CollectionUtils.isEmpty(product.getPromotions()))
		{
			builder = new StringBuilder(brandName + " " + productName);
		}


		if (CollectionUtils.isNotEmpty(product.getPromotions()))
		{
			for (final ProductPromotionModel productPromotion : product.getPromotions())
			{
				if (null != productPromotion.getEndDate())
				{
					builder.append(". Offer expires ");
					final Format outputFormat = new SimpleDateFormat("dd MMMM yyyy");
					final SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
					String promoExpireDate = "";
					try
					{
						promoExpireDate = outputFormat.format(inputFormat.parse(productPromotion.getEndDate().toString()));
					}
					catch (final ParseException e)
					{
						LOG.error("Error in converting date" + e.getMessage());
					}
					builder.append(promoExpireDate);
					break;
				}
			}
		}

		if (currentSite != null)
		{
			builder.append(" " + SEPARATOR + " ").append(currentSite.getName());
		}

		return StringEscapeUtils.escapeHtml(builder.toString());
	}


	@Override
	public String resolveCategoryPageTitle(final CategoryModel category)
	{
		final StringBuilder stringBuilder = new StringBuilder();
		//		final List<CategoryModel> categories = this.getCategoryPath(category);
		//		for (final CategoryModel c : categories)
		//		{
		//			stringBuilder.append(c.getName());
		//		}

		if (StringUtils.isNotBlank(category.getName()))
		{
			stringBuilder.append(category.getName());
			stringBuilder.append(SPACE);
		}

		if (CollectionUtils.isNotEmpty(category.getPromotions()))
		{
			stringBuilder.append("- specials at Clicks");
		}
		else
		{
			stringBuilder.append("products at Clicks");
		}


		return StringEscapeUtils.escapeHtml(stringBuilder.toString());
	}

	public String resolveCategoryPageTitle(final String categoryName)
	{
		final StringBuilder stringBuilder = new StringBuilder();
		final String promotionTitle = "Promotions";
		if (StringUtils.isNotBlank(categoryName))
		{
			stringBuilder.append(categoryName);
			stringBuilder.append(SPACE);
			stringBuilder.append("- specials at Clicks");
		}
		else
		{
			stringBuilder.append(promotionTitle);
			stringBuilder.append(SPACE);
			stringBuilder.append("- specials at Clicks");
		}
		return StringEscapeUtils.escapeHtml(stringBuilder.toString());
	}

	public String resolveContentPageTitle(final String title, final String articleCategory)
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		final StringBuilder builder = new StringBuilder();
		if (!StringUtils.isEmpty(title))
		{
			builder.append(title).append(TITLE_WORD_SEPARATOR);
		}

		if (StringUtils.isNotBlank(articleCategory))
		{
			builder.append(articleCategory).append(SEPARATOR);
		}
		builder.append(currentSite.getName());
		return StringEscapeUtils.escapeHtml(builder.toString());
	}


	//Title for Store Detail page
	public String resolveContentPageTitle(final String title, final boolean flag)
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		final StringBuilder builder = new StringBuilder();
		builder.append(currentSite.getName());
		builder.append(SPACE);
		if (!StringUtils.isEmpty(title))
		{
			builder.append(title).append(SEPARATOR);
		}

		builder.append(" Store Details");
		return StringEscapeUtils.escapeHtml(builder.toString());
	}

	public String resolveContentPageTitleForHealthPages(final String articleType, final String articleName)
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();

		final StringBuilder builder = new StringBuilder();
		if (StringUtils.isNotBlank(articleType))
		{
			if (articleType.equals("CONDITIONS"))
			{
				builder.append(articleName);
				builder.append(SPACE);
				builder.append("Causes, Symptoms & Treatment ");
			}
			else if (articleType.equals("MEDICINES"))
			{
				builder.append(articleName);
				builder.append(SPACE);
				builder.append("Benefits and Side Effects ");
			}
			else if (articleType.equals("VITAMINS"))
			{
				builder.append(articleName);
				builder.append(SEPARATOR);
				builder.append(SPACE);
				builder.append("Vitamins and Supplements ");
			}

			builder.append(SEPARATOR);
			builder.append(currentSite.getName());
			builder.append(SPACE);
			builder.append("Health Hub");
		}
		return StringEscapeUtils.escapeHtml(builder.toString());
	}

}
