/**
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.model.RecentOrdersComponentModel;
import de.hybris.clicks.facades.myaccount.MyAccountFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.servicelayer.exceptions.AttributeNotSupportedException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



/**
 * @author ashish.vyas
 *
 */
@Controller("RecentOrdersComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.RecentOrdersComponent)
public class RecentOrdersComponentController extends AbstractCMSComponentController<RecentOrdersComponentModel>
{
	@Resource(name = "myAccountFacade")
	private MyAccountFacade myAccountFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.glanbia.agri.storefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.
	 * HttpServletRequest, org.springframework.ui.Model,
	 * de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final RecentOrdersComponentModel component)
	{
		// YTODO Auto-generated method stub
		for (final String property : getCmsComponentService().getEditorProperties(component))
		{
			try
			{
				final Object value = modelService.getAttributeValue(component, property);
				model.addAttribute(property, value);
			}
			catch (final AttributeNotSupportedException ignore)
			{
				// ignore
			}
		}
		final int count = Integer.parseInt(Config.getParameter("recentOrderHistory.orders.max.show"));
		model.addAttribute("orders", myAccountFacade.getRecentOrders(count));
	}
}
