/**
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.model.ConditionListComponentModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.facade.hpcondition.HpConditionFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.commercefacades.user.data.HealthConditionData;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author mbhargava
 *
 */
@Controller("ConditionListComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.ConditionListComponent)
public class ConditionListComponentController<T extends AbstractItemModel> extends
		AbstractCMSComponentController<ConditionListComponentModel>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.storefront.controllers.cms.AbstractCMSComponentController#fillModel(javax.servlet.http.
	 * HttpServletRequest, org.springframework.ui.Model,
	 * de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
	 */
	static Logger log = Logger.getLogger(ConditionListComponentController.class.getName());

	@Resource(name = "hpConditionFacade")
	private HpConditionFacade hpConditionFacade;

	@Resource(name = "conditionsBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder conditionsBreadcrumbBuilder;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ConditionListComponentModel component)
	{
		final String id = request.getParameter("id");
		final String type = request.getParameter("type");
		log.info("Entered into ConditionListComponentController id: " + id + " type: " + type);

		model.addAttribute("basePage", getBasePage(type));
		if (StringUtils.isNotBlank(id) && id.equalsIgnoreCase("viewall"))
		{
			final List<HealthConditionData> conditionList = hpConditionFacade.findConditionsByType(type);
			model.addAttribute("conditionList", conditionList);
			model.addAttribute("headerForCond", type);
			//			model.addAttribute("basePage", getBasePage(type));
		}
		else if (StringUtils.isNotBlank(id) && id.equalsIgnoreCase("all"))
		{
			final List<ConditionsModel> conditionModelList = hpConditionFacade.findAllConditions(type);
			prepareMap(conditionModelList, model);
			model.addAttribute("headerForCond", "ALL_" + type);
			//			model.addAttribute("basePage", getBasePage(type));
		}
		else
		{
			final ItemModel itemModel = hpConditionFacade.getConditionsListByConditionId(id);
			if (itemModel instanceof ConditionsModel)
			{
				final ConditionsModel conditionsModel = (ConditionsModel) itemModel;
				prepareMap(conditionsModel.getRelatedConditions(), model);
				model.addAttribute("glossaryList", conditionsModel.getRelatedGlossary());
				model.addAttribute("headerForCond", conditionsModel.getConditionName());
			}
			model.addAttribute("breadcrumbs", conditionsBreadcrumbBuilder.getBreadcrumbs(null));
		}
	}

	private String getBasePage(final String type)
	{
		if (StringUtils.isNotBlank(type))
		{
			if (type.contains("MEDICINES"))
			{
				return "MEDICINES";
			}
			else if (type.contains("VITAMINSNSUPPLEMENTS"))
			{
				return "VITAMINSNSUPPLEMENTS";
			}
			else if (type.contains("CONDITION"))
			{
				return "CONDITION";
			}
		}

		return "";
	}

	@Override
	protected String getView(final ConditionListComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(ConditionListComponentModel._TYPECODE);
	}

	@SuppressWarnings("boxing")
	private void prepareMap(final Collection<ConditionsModel> condList, final Model model)
	{
		final List<ConditionsModel> sortedList = new ArrayList<ConditionsModel>(condList);
		log.info("+++++++++++++++++++Preparing map for Condition List Page+++++++++++++++++++");
		final String str = "ABCDEFGHIJKLM";
		final LinkedHashMap firstTabMap = new LinkedHashMap();
		final LinkedHashMap secondTabMap = new LinkedHashMap();
		List<ConditionsModel> firstTabList = null;
		List<ConditionsModel> secondTabList = null;

		Collections.sort(sortedList, new Comparator<ConditionsModel>()
		{

			@Override
			public int compare(final ConditionsModel o1, final ConditionsModel o2)
			{
				return o1.getConditionName().compareTo(o2.getConditionName());
			}
		});

		for (final ConditionsModel obj : sortedList)
		{
			if (null != obj.getStartingAlpha() && !obj.getStartingAlpha().isEmpty())
			{
				if (str.contains(obj.getStartingAlpha().toUpperCase()))
				{
					if (null == firstTabMap.get(obj.getStartingAlpha().toUpperCase()))
					{
						firstTabList = new ArrayList<ConditionsModel>();
						firstTabList.add(obj);
						firstTabMap.put(obj.getStartingAlpha().toUpperCase(), firstTabList);
					}
					else
					{
						final List<ConditionsModel> existingList = (List<ConditionsModel>) firstTabMap.get(obj.getStartingAlpha()
								.toUpperCase());
						existingList.add(obj);
						firstTabMap.put(obj.getStartingAlpha().toUpperCase(), existingList);
					}

				}

				else
				{
					if (null == secondTabMap.get(obj.getStartingAlpha().toUpperCase()))
					{
						secondTabList = new ArrayList<ConditionsModel>();
						secondTabList.add(obj);
						secondTabMap.put(obj.getStartingAlpha().toUpperCase(), secondTabList);
					}
					else
					{
						final List<ConditionsModel> existingList = (List<ConditionsModel>) secondTabMap.get(obj.getStartingAlpha()
								.toUpperCase());
						existingList.add(obj);
						secondTabMap.put(obj.getStartingAlpha().toUpperCase(), existingList);
					}
				}
			}
		}
		model.addAttribute("firstTabMap", firstTabMap);
		model.addAttribute("secondTabMap", secondTabMap);
	}
}
