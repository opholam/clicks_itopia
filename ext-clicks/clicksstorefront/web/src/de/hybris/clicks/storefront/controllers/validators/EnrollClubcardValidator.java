/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.core.model.DeliveryAreaModel;
import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates registration forms.
 */

public class EnrollClubcardValidator implements Validator
{
	//public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
	protected static final Logger LOG = Logger.getLogger(EnrollClubcardValidator.class);
	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.CUSTOM_NAME_REGEX;
	private static final String SA_ID_REGEX = FormPropertyConstants.Validator.Regex.SA_ID_REGEX;

	@Resource
	private MessageSource messageSource;

	@Resource(name = "clicksCheckoutFacade")
	private ClicksCheckoutFacade clicksCheckoutFacade;

	@Resource
	private I18NService i18nService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ClicksRegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{

		final ClicksRegisterForm enrollForm = (ClicksRegisterForm) object;

		final String hybrisRetailerId = enrollForm.getHybrisRetailerId();
		final String IDNumber = enrollForm.getIDNumber();
		final String customerId = enrollForm.getCustomerId();

		final int SAResident = enrollForm.getIsSAResident();
		final String firstName = enrollForm.getFirstName();
		final String preferedName = enrollForm.getPreferedName();
		final String lastName = enrollForm.getLastName();
		final String eMailAddress = enrollForm.geteMailAddress();
		final int gender = enrollForm.getGender();

		final String addressTypeId = enrollForm.getAddressTypeId();
		final String addressLine1 = enrollForm.getAddressLine1();
		//final String addressLine2 = enrollForm.getAddressLine2();
		final String suburb = enrollForm.getSuburb();
		final String city = enrollForm.getCity();
		final String province = enrollForm.getProvince();
		final String country = enrollForm.getCountry();
		final String postalCode = enrollForm.getPostalCode();


		// hybris details validation

		if (SAResident == 1)
		{
			if (!StringUtils.isEmpty(IDNumber))
			{
				if (StringUtils.length(IDNumber) != 13)
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
				if (Config.getBoolean("SA.ID.Validation", true) && !ValidatorUtils.validateRegex(SA_ID_REGEX, IDNumber))
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
			}
			if (StringUtils.isEmpty(IDNumber))
			{
				errors.rejectValue("IDNumber", "register.CC.IDNumber.empty");
			}
		}
		else
		{
			if (StringUtils.isEmpty(enrollForm.getSA_DOB_day()))
			{
				errors.rejectValue("SA_DOB_day", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(enrollForm.getSA_DOB_month()))
			{
				errors.rejectValue("SA_DOB_month", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(enrollForm.getSA_DOB_year()))
			{
				errors.rejectValue("SA_DOB_year", "register.CC.DOB.empty");
			}
			if (!StringUtils.isEmpty(enrollForm.getSA_DOB_day()) && !StringUtils.isEmpty(enrollForm.getSA_DOB_month())
					&& !StringUtils.isEmpty(enrollForm.getSA_DOB_year()))
			{
				final String date = enrollForm.getSA_DOB_month() + "/" + enrollForm.getSA_DOB_day() + "/"
						+ enrollForm.getSA_DOB_year();
				try
				{
					final Date dobDate = new Date(date);
					final Date compareDate = new Date();
					compareDate.setYear(new Date().getYear() - 18);
					if (dobDate.after(compareDate))
					{
						// show error msg here -- age should be more than 18
						errors.rejectValue("SA_DOB_day", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_month", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_year", "register.CC.age.invalid");
					}
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}

		}

		if (!ValidatorUtils.validateRegex(EMAIL_REGEX, eMailAddress))
		{
			errors.rejectValue("eMailAddress", "register.email.invalid");
		}
		else if (StringUtils.length(eMailAddress) > 255)
		{
			errors.rejectValue("eMailAddress", "register.email.invalid");
		}
		if (StringUtils.isEmpty(enrollForm.getConfirmPassword()))
		{
			errors.rejectValue("confirmPassword", "register.CC.password.invalid");
		}
		if (StringUtils.isEmpty(enrollForm.getPassword()))
		{
			errors.rejectValue("password", "register.CC.password.invalid");
		}
		if (!StringUtils.isEmpty(enrollForm.getPassword()))
		{
			if (enrollForm.getPassword().length() < 6)
			{
				errors.rejectValue("password", "register.CC.password.length");
			}
		}
		if (!StringUtils.isEmpty(enrollForm.getConfirmPassword()) && !StringUtils.isEmpty(enrollForm.getPassword()))
		{
			if (!enrollForm.getPassword().equalsIgnoreCase(enrollForm.getConfirmPassword()))
			{
				errors.rejectValue("confirmPassword", "register.CC.password.misMatch");
			}
		}

		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			errors.rejectValue("firstName", "register.CC.firstname.empty");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}
		if (!StringUtils.isEmpty(preferedName))
		{
			if (StringUtils.length(preferedName) > 50)
			{
				errors.rejectValue("preferedName", "register.CC.preferedname.invalid");
			}
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			errors.rejectValue("lastName", "register.CC.lastname.empty");
		}
		else if (StringUtils.length(lastName) > 30)
		{
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}
		if (StringUtils.isEmpty(addressLine1))
		{
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		else if (StringUtils.length(addressLine1) > 30)
		{
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		if (StringUtils.isEmpty(suburb))
		{
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		else if (StringUtils.length(suburb) > 30)
		{
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		if (!StringUtils.isEmpty(city))
		{
			if (StringUtils.length(city) > 30)
			{
				errors.rejectValue("city", "register.enroll.city.invalid");
			}
		}
		if (!StringUtils.isEmpty(province))
		{
			if (StringUtils.length(province) > 5)
			{
				errors.rejectValue("province", "register.enroll.province.invalid");
			}
		}
		if (StringUtils.isEmpty(country))
		{
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		else if (StringUtils.length(country) > 2)
		{
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		final SearchResult<DeliveryAreaModel> result = clicksCheckoutFacade.fetchResults(postalCode);
		try
		{
			if (null != result && result.getCount() == 0)
			{
				errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
			}
			/*
			 * else if (!postcode.matches("^(0|[1-9][0-9]*)$")) { errors.rejectValue("postcode",
			 * "register.enroll.postalCode.invalid"); }
			 */
		}
		catch (final Exception exp)
		{
			errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		}
	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}


	public void validateJoinCC(final Object object, final Errors errors)
	{
		// no validation for email and password

		final ClicksRegisterForm enrollForm = (ClicksRegisterForm) object;

		final String hybrisRetailerId = enrollForm.getHybrisRetailerId();
		final String IDNumber = enrollForm.getIDNumber();
		final String customerId = enrollForm.getCustomerId();

		final int SAResident = enrollForm.getIsSAResident();
		final String firstName = enrollForm.getFirstName();
		final String preferedName = enrollForm.getPreferedName();
		final String lastName = enrollForm.getLastName();
		final String eMailAddress = enrollForm.geteMailAddress();
		final int gender = enrollForm.getGender();

		final String addressTypeId = enrollForm.getAddressTypeId();
		final String addressLine1 = enrollForm.getAddressLine1();
		//final String addressLine2 = enrollForm.getAddressLine2();
		final String suburb = enrollForm.getSuburb();
		final String city = enrollForm.getCity();
		final String province = enrollForm.getProvince();
		final String country = enrollForm.getCountry();
		final String postalCode = enrollForm.getPostalCode();


		// hybris details validation

		if (SAResident == 1)
		{
			if (!StringUtils.isEmpty(IDNumber))
			{
				if (StringUtils.length(IDNumber) != 13)
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
				if (Config.getBoolean("SA.ID.Validation", true) && !ValidatorUtils.validateRegex(SA_ID_REGEX, IDNumber))
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
			}
			if (StringUtils.isEmpty(IDNumber))
			{
				errors.rejectValue("IDNumber", "register.CC.IDNumber.empty");
			}
		}
		else
		{
			if (StringUtils.isEmpty(enrollForm.getSA_DOB_day()))
			{
				errors.rejectValue("SA_DOB_day", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(enrollForm.getSA_DOB_month()))
			{
				errors.rejectValue("SA_DOB_month", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(enrollForm.getSA_DOB_year()))
			{
				errors.rejectValue("SA_DOB_year", "register.CC.DOB.empty");
			}
			if (!StringUtils.isEmpty(enrollForm.getSA_DOB_day()) && !StringUtils.isEmpty(enrollForm.getSA_DOB_month())
					&& !StringUtils.isEmpty(enrollForm.getSA_DOB_year()))
			{
				final String date = enrollForm.getSA_DOB_month() + "/" + enrollForm.getSA_DOB_day() + "/"
						+ enrollForm.getSA_DOB_year();
				try
				{
					final Date dobDate = new Date(date);
					final Date compareDate = new Date();
					compareDate.setYear(new Date().getYear() - 18);
					if (dobDate.after(compareDate))
					{
						// show error msg here -- age should be more than 18
						errors.rejectValue("SA_DOB_day", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_month", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_year", "register.CC.age.invalid");
					}
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}

		}


		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			errors.rejectValue("firstName", "register.CC.firstname.empty");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}
		if (!StringUtils.isEmpty(preferedName))
		{
			if (StringUtils.length(preferedName) > 50)
			{
				errors.rejectValue("preferedName", "register.CC.preferedname.invalid");
			}
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			errors.rejectValue("lastName", "register.CC.lastname.empty");
		}
		else if (StringUtils.length(lastName) > 30)
		{
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}
		if (StringUtils.isEmpty(addressLine1))
		{
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		else if (StringUtils.length(addressLine1) > 30)
		{
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		if (StringUtils.isEmpty(suburb))
		{
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		else if (StringUtils.length(suburb) > 30)
		{
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		if (!StringUtils.isEmpty(city))
		{
			if (StringUtils.length(city) > 30)
			{
				errors.rejectValue("city", "register.enroll.city.invalid");
			}
		}
		if (!StringUtils.isEmpty(province))
		{
			if (StringUtils.length(province) > 5)
			{
				errors.rejectValue("province", "register.enroll.province.invalid");
			}
		}
		if (StringUtils.isEmpty(country))
		{
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		else if (StringUtils.length(country) > 2)
		{
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		final SearchResult<DeliveryAreaModel> result = clicksCheckoutFacade.fetchResults(postalCode);
		try
		{
			if (null != result && result.getCount() == 0)
			{
				errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
			}
			/*
			 * else if (!postcode.matches("^(0|[1-9][0-9]*)$")) { errors.rejectValue("postcode",
			 * "register.enroll.postalCode.invalid"); }
			 */
		}
		catch (final Exception exp)
		{
			errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		}
		/*
		 * if (StringUtils.isEmpty(postalCode)) { errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		 * }
		 * 
		 * else if (StringUtils.length(postalCode) > 10) { errors.rejectValue("postalCode",
		 * "register.enroll.postalCode.invalid"); }
		 */


	}

}
