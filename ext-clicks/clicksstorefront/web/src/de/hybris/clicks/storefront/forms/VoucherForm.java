/**
 * 
 */
package de.hybris.clicks.storefront.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author KhajaM
 * 
 */
public class VoucherForm
{

	private String voucherCode;

	/**
	 * @return the voucherCode
	 */
	@NotNull(message = "{cart.voucher.redeem.invalid}")
	@Size(min = 1, max = 20, message = "{cart.voucher.redeem.invalid}")
	public String getVoucherCode()
	{
		return voucherCode;
	}

	/**
	 * @param voucherCode
	 *           the voucherCode to set
	 */
	public void setVoucherCode(final String voucherCode)
	{
		this.voucherCode = voucherCode;
	}

}
