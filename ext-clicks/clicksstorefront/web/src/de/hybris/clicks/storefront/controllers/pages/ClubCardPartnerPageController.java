/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.clubcard.ClubCardPartnerFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * The Class ClubCardPartnerPageController.
 * 
 * @author ashish.vyas
 */
@Controller
@Scope("tenant")
@RequestMapping("/clubcardpartnerspage")
public class ClubCardPartnerPageController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(ClubCardPartnerPageController.class);

	@Resource(name = "clubCardPartnerFacade")
	ClubCardPartnerFacade clubCardPartnerFacade;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	/**
	 * Gets the club card partners.
	 * 
	 * @param model
	 *           the model
	 * @return the club card partners
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getClubCardPartners(final Model model)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("In getClubCardPartners() of ClubCardPartnerPageController");
		}

		try
		{
			//final List<ClubCardPartnerModel> clubCardPartners = clubCardPartnerFacade.getClubCardPartners();
			model.addAttribute("clubCardPartners", clubCardPartnerFacade.getClubCardPartners());
			model.addAttribute("errorMsg", null);

			storeCmsPageInModel(model, getContentPageForLabelOrId("ClubCardPartnersPage"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("ClubCardPartnersPage"));
		}
		catch (final Exception e)
		{
			LOG.error("Error while getting ClubCard Partners : " + e);
		}
		return ControllerConstants.Views.Pages.ClubCardPartners.clubCardPartnersPage;
	}
}
