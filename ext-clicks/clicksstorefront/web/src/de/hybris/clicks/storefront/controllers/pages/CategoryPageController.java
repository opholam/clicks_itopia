/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.storefront.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.category.attribute.CategoryAllSubcategories;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Controller for a category page.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/**/c")
public class CategoryPageController extends AbstractSearchPageController
{
	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
			ProductOption.DESCRIPTION, ProductOption.STOCK, ProductOption.PROMOTIONS);
	protected static final Logger LOG = Logger.getLogger(CategoryPageController.class);

	private static final String PRODUCT_GRID_PAGE = "category/productGridPage";

	private static final String PRODUCT_LIST_PAGE = "productList";
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/{categoryCode:.*}";

	@Resource
	private CategoryAllSubcategories categoryAllSubcategories;


	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;


	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "searchBreadcrumbBuilder")
	private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

	@Resource(name = "categoryModelUrlResolver")
	private UrlResolver<CategoryModel> categoryModelUrlResolver;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	/**
	 * Category.
	 *
	 * @param categoryCode
	 *           the category code
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param count
	 *           the count
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return the string
	 * @throws UnsupportedEncodingException
	 *            the unsupported encoding exception
	 */
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String category(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "count", defaultValue = "12") int count, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws UnsupportedEncodingException
	{
		final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);

		final String redirection = checkRequestUrl(request, response, categoryModelUrlResolver.resolve(category));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		final CategoryPageModel categoryPage = getCategoryPage(category);

		if (null != categoryPage && StringUtils.isNotEmpty(categoryPage.getName())
				&& !"Product List".equalsIgnoreCase(categoryPage.getName()) && StringUtils.isEmpty(searchQuery))
		{
			count = 0;
		}

		final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, XSSFilterUtil.filter(searchQuery),
				page, showMode, sortCode, categoryPage);
		categorySearch.doSearch(count);

		if (null != categoryPage && StringUtils.isNotEmpty(categoryPage.getUid())
				&& !(PRODUCT_LIST_PAGE.equalsIgnoreCase(categoryPage.getUid())) && !StringUtils.isNotEmpty(searchQuery))
		{
			if (StringUtils.isNotBlank(request.getQueryString()) && null == request.getParameter("clppage"))
			{
				return REDIRECT_PREFIX + request.getServletPath() + "?" + request.getQueryString() + "&clppage=sc";
			}
			else if (StringUtils.isBlank(request.getQueryString()))
			{
				return REDIRECT_PREFIX + request.getServletPath() + "?clppage=sc";
			}
		}

		final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = categorySearch
				.getSearchPageData();
		final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();

		storeCmsPageInModel(model, categorySearch.categoryPage);
		storeContinueUrl(request);

		populateModel(model, searchPageData, showMode);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(categoryCode, searchPageData));
		model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
		model.addAttribute("pageType", PageType.CATEGORY.name());
		model.addAttribute("userLocation", customerLocationService.getUserLocation());
		model.addAttribute("promotions", category.getPromotions());
		model.addAttribute("logo", category.getLogo());

		final List<CategoryModel> subCategories = new ArrayList<CategoryModel>();
		try
		{
			subCategories.addAll(categoryAllSubcategories.get(category));
			if (CollectionUtils.isNotEmpty(subCategories))
			{
				Collections.sort(subCategories, new Comparator<CategoryModel>()
				{
					@Override
					public int compare(final CategoryModel arg0, final CategoryModel arg1)
					{
						return arg0.getName().compareTo(arg1.getName());
					}
				});
			}
			model.addAttribute("subCategories", subCategories);
		}
		catch (final Exception e)
		{
			LOG.error("Error while getting sub categories" + e.getMessage());
			model.addAttribute("subCategories", categoryAllSubcategories.get(category));
		}
		model.addAttribute("superCategories", category.getAllSupercategories());
		model.addAttribute("categoryName", category.getName());
		model.addAttribute("categoryDescription", category.getDescription());
		model.addAttribute("categoryOthers", category.getOthers());

		model.addAttribute("categoryCode", categoryCode);
		model.addAttribute("count", count);

		final List<String> rootCategories = new ArrayList<String>();
		rootCategories.add("products");
		rootCategories.add("brands");
		rootCategories.add("collections");
		if (StringUtils.isNotBlank(category.getName()))
		{
			rootCategories.add(category.getName().toLowerCase());
		}
		model.addAttribute("rootCategories", rootCategories);
		getPromoInfo(searchQuery, model);
		/*
		 * final List<String> immediateSubCategories = new ArrayList<String>(); for (final CategoryModel categoryModel :
		 * category.getCategories()) { immediateSubCategories.add(categoryModel.getName().toLowerCase()); }
		 * model.addAttribute("immediateSubCategories", immediateSubCategories);
		 */
		for (final FacetData<SearchStateData> facetCollection : searchPageData.getFacets())
		{//Loop to fetch brands from facets to display on CLP.
			if (StringUtils.isNotBlank(facetCollection.getCode())
					&& facetCollection.getCode().equalsIgnoreCase(Config.getParameter("text.category.facet.brand")))
			{
				model.addAttribute("facetValDataList", facetCollection.getValues());
				break;
			}
		}
		try
		{
			for (final FacetData<SearchStateData> facetCollection : searchPageData.getFacets())
			{
				if (StringUtils.isNotBlank(facetCollection.getCode())
						&& facetCollection.getCode().equalsIgnoreCase(Config.getParameter("text.category.facet.category")))
				{
					final List<FacetValueData<SearchStateData>> list = new ArrayList<FacetValueData<SearchStateData>>();
					for (final FacetValueData<SearchStateData> facet : facetCollection.getValues())
					{
						for (final CategoryModel cat : categoryAllSubcategories.get(category))
						{
							if (cat.getCode().equalsIgnoreCase(facet.getCode()))
							{
								list.add(facet);
								break;
							}
						}
					}
					model.addAttribute("catFacets", list);
					//facetCollection.setValues(list);
					break;
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in CategoryPageController class at method category():" + e.getMessage());
		}
		model.addAttribute("plpPromotion", category.getPlpPromotion());

		final List<ProductData> bestSellersProducts = new ArrayList<ProductData>();
		final List<ProductData> brandRangeProducts = new ArrayList<ProductData>();
		final List<ProductData> featuredProdListm = new ArrayList<ProductData>();

		final List<ProductData> productPromotionList = new ArrayList<ProductData>();


		for (final ProductModel productModel : category.getBestSellerProdList())
		{
			final ProductData pd = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);


			pd.setRemarks(productModel.getRemarks());
			//pd.setDescription(productModel.getDescription());
			bestSellersProducts.add(pd);

		}
		for (final ProductModel productModel : category.getBrandRange())
		{
			final ProductData pd = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);

			//pd.setDescription(productModel.getDescription());
			brandRangeProducts.add(pd);

		}


		for (final ProductModel productModel : category.getFeaturedProdList())
		{
			final ProductData pd = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);

			//pd.setDescription(productModel.getDescription());
			featuredProdListm.add(pd);

		}

		model.addAttribute("bestSellerProdList", bestSellersProducts);
		model.addAttribute("brandRangeProdList", brandRangeProducts);
		model.addAttribute("featuredProdListm", featuredProdListm);
		model.addAttribute("categoryFeaturedCompo", category.getCategoryFeatureComponents());
		model.addAttribute("customDisplayComoList", category.getCustomDisplayComoList());
		model.addAttribute("cmsMediaLinkComponent", category.getCmsMediaLinkCompList());
		model.addAttribute("featuredBrandLinkCompo", category.getLinkComponents());
		model.addAttribute("brandImage", category.getPicture());
		model.addAttribute("brandProductList", category.getProducts());
		model.addAttribute("brandRange", category.getBrandRange());
		model.addAttribute("brandProductImage", category.getNormal());
		model.addAttribute("brandShortDescription", category.getShortDescription());

		/*------------------- begin Latest Promotions----------------*/

		for (final ProductModel productModel : category.getPromotionProductList())
		{
			final ProductData pd = productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS);

			productPromotionList.add(pd);

		}

		model.addAttribute("productPromotionList", productPromotionList);
		/*------------------- end   Latest Promotions----------------*/

		model.addAttribute("featuredProductComponents", category.getFeaturedProductComponents());
		model.addAttribute("clubcardProducts", category.getClubcardProducts());

		updatePageTitle(category, searchPageData.getBreadcrumbs(), model);
		final RequestContextData requestContextData = getRequestContextData(request);
		requestContextData.setCategory(category);
		requestContextData.setSearch(searchPageData);

		if (searchQuery != null)
		{
			model.addAttribute("metaRobots", "noindex,follow");
		}

		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(category.getKeywords());
		String metaDescription = MetaSanitizerUtil.sanitizeDescription(category.getDescription());

		metaDescription = getMetaDescription(category, metaDescription);
		setUpMetaData(model, metaKeywords, metaDescription);
		return getViewPage(categorySearch.categoryPage);
	}

	/**
	 * @param category
	 * @param metaDescription
	 */
	private String getMetaDescription(final CategoryModel category, String metaDescription)
	{
		metaDescription = getMessageSource().getMessage("text.browse.clicks.range", new Object[]
		{ category.getName() }, "{0}", getI18nService().getCurrentLocale());

		if (CollectionUtils.isNotEmpty(category.getPromotions()))
		{
			metaDescription = getMessageSource().getMessage("text.browse.through.available", new Object[]
			{ category.getName() }, "{0}", getI18nService().getCurrentLocale());
		}
		return metaDescription;

	}

	/**
	 * Gets the facets.
	 *
	 * @param categoryCode
	 *           the category code
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @return the facets
	 * @throws UnsupportedEncodingException
	 *            the unsupported encoding exception
	 */
	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException
	{
		final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);
		final CategoryPageModel categoryPage = getCategoryPage(category);
		final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
				sortCode, categoryPage);
		categorySearch.doSearch();

		final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = categorySearch
				.getSearchPageData();

		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
		refinement.setFacets(facets);
		refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
		refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
		return refinement;
	}

	/**
	 * Gets the results.
	 *
	 * @param categoryCode
	 *           the category code
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param count
	 *           the count
	 * @return the results
	 * @throws UnsupportedEncodingException
	 *            the unsupported encoding exception
	 */
	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "count", defaultValue = "0") final int count) throws UnsupportedEncodingException
	{
		final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);
		final CategoryPageModel categoryPage = getCategoryPage(category);
		final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
				sortCode, categoryPage);
		categorySearch.doSearch(count);

		final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = categorySearch
				.getSearchPageData();

		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		searchResultsData.setResults(searchPageData.getResults());
		searchResultsData.setPagination(searchPageData.getPagination());
		return searchResultsData;
	}

	protected boolean categoryHasDefaultPage(final CategoryPageModel categoryPage)
	{
		return Boolean.TRUE.equals(categoryPage.getDefaultPage());
	}

	protected CategoryPageModel getCategoryPage(final CategoryModel category)
	{
		try
		{
			return cmsPageService.getPageForCategory(category);
		}
		catch (final CMSItemNotFoundException ignore)
		{
			// Ignore
		}
		return null;
	}

	protected CategoryPageModel getDefaultCategoryPage()
	{
		try
		{
			return cmsPageService.getPageForCategory(null);
		}
		catch (final CMSItemNotFoundException ignore)
		{
			// Ignore
		}
		return null;
	}

	protected <QUERY> void updatePageTitle(final CategoryModel category, final List<BreadcrumbData<QUERY>> appliedFacets,
			final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveCategoryPageTitle(category));
	}

	protected String getViewPage(final CategoryPageModel categoryPage)
	{
		if (categoryPage != null)
		{
			final String targetPage = getViewForPage(categoryPage);
			if (targetPage != null && !targetPage.isEmpty())
			{
				return targetPage;
			}
		}
		return PAGE_ROOT + PRODUCT_GRID_PAGE;
	}

	/**
	 * Handle unknown identifier exception.
	 *
	 * @param exception
	 *           the exception
	 * @param request
	 *           the request
	 * @return the string
	 */
	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		String basePath = "/404";
		final String servletPath = request.getRequestURI();
		if ((StringUtils.isNotBlank(servletPath) && !servletPath.equals(XSSFilterUtil.filter(request.getRequestURI()))))
		{
			return REDIRECT_PREFIX + basePath;
		}
		if (!request.getRequestURI().contains("404"))
		{
			basePath = basePath + XSSFilterUtil.filter(request.getServletPath());
			return REDIRECT_PREFIX + basePath;
		}
		return FORWARD_PREFIX + "/404";
	}

	/**
	 * The Class CategorySearchEvaluator.
	 */
	private class CategorySearchEvaluator
	{
		private final String categoryCode;
		private final SearchQueryData searchQueryData = new SearchQueryData();
		private final int page;
		private final ShowMode showMode;
		private final String sortCode;
		private CategoryPageModel categoryPage;
		private boolean showCategoriesOnly;
		private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;

		public CategorySearchEvaluator(final String categoryCode, final String searchQuery, final int page,
				final ShowMode showMode, final String sortCode, final CategoryPageModel categoryPage)
		{
			this.categoryCode = categoryCode;
			this.searchQueryData.setValue(searchQuery);
			this.page = page;
			this.showMode = showMode;
			this.sortCode = sortCode;
			this.categoryPage = categoryPage;
		}

		public boolean isShowCategoriesOnly()
		{
			return showCategoriesOnly;
		}

		public ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getSearchPageData()
		{
			return searchPageData;
		}

		public void doSearch()
		{
			showCategoriesOnly = false;
			if (searchQueryData.getValue() == null)
			{
				// Direct category link without filtering
				searchPageData = productSearchFacade.categorySearch(categoryCode);
				if (categoryPage != null)
				{
					showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
							&& CollectionUtils.isNotEmpty(searchPageData.getSubCategories());
				}
			}
			else
			{
				// We have some search filtering
				if (categoryPage == null || !categoryHasDefaultPage(categoryPage))
				{
					// Load the default category page
					categoryPage = getDefaultCategoryPage();
				}

				final SearchStateData searchState = new SearchStateData();
				searchState.setQuery(searchQueryData);

				final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
				searchPageData = productSearchFacade.categorySearch(categoryCode, searchState, pageableData);
			}
		}


		/**
		 * Do search. Fetchimg clp pages
		 *
		 * @param count
		 *           the count
		 */
		public void doSearch(final int count)
		{
			showCategoriesOnly = false;
			if (searchQueryData.getValue() == null && count == 0)
			{
				// Direct category link without filtering
				searchPageData = productSearchFacade.categorySearch(categoryCode);
				if (categoryPage != null)
				{
					showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
							&& CollectionUtils.isNotEmpty(searchPageData.getSubCategories());
				}
			}
			else
			{
				// We have some search filtering
				if (categoryPage == null || !categoryHasDefaultPage(categoryPage))
				{
					// Load the default category page
					categoryPage = getDefaultCategoryPage();
				}

				final SearchStateData searchState = new SearchStateData();
				searchState.setQuery(searchQueryData);

				final PageableData pageableData = createPageableData(page, count, sortCode, showMode);
				searchPageData = productSearchFacade.categorySearch(categoryCode, searchState, pageableData);
			}
		}
	}

	/**
	 * Gets the promo info.
	 *
	 * @param searchQuery
	 *           the search query
	 * @param model
	 *           the model
	 * @return the promo info
	 */
	private void getPromoInfo(final String searchQuery, final Model model)
	{
		try
		{
			if (StringUtils.isNotEmpty(searchQuery))
			{
				String promoCode = "";
				{
					final String[] split = searchQuery.split(":");
					for (int i = 2; (i + 1) < split.length; i += 2)
					{
						if ("allPromotions".equalsIgnoreCase(split[i]))
						{
							promoCode = (split[i + 1]);
							break;
						}
					}

				}
				if (StringUtils.isNotBlank(promoCode))
				{
					LOG.info("Promo code in PLP>> " + promoCode);
					AbstractPromotionModel promotionModel = new AbstractPromotionModel();
					promotionModel.setCode(promoCode);
					promotionModel.setEnabled(Boolean.TRUE);
					promotionModel = flexibleSearchService.getModelByExample(promotionModel);
					if (null != promotionModel)
					{
						model.addAttribute("promoOffer", promotionModel.getOffer());
						model.addAttribute("promoTitle", promotionModel.getTitle());
					}
				}
			}
		}
		catch (final Exception e)
		{
			//
		}
	}
}
