/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.clicks.storefront.forms.EditClubCardForm;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates registration forms.
 */

public class EditProfileClubcardValidator implements Validator
{
	//public static final String EMAIL_REGEX = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.CUSTOM_NAME_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;
	private static final Logger LOG = Logger.getLogger(EditProfileClubcardValidator.class);
	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;
	@Resource(name = "clicksCheckoutFacade")
	private ClicksCheckoutFacade clicksCheckoutFacade;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return EditClubCardForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final EditClubCardForm registerCCForm = (EditClubCardForm) object;

		final String hybrisCustomerId = registerCCForm.getHybrisCustomerId();
		final String hybrisRetailerId = registerCCForm.getHybrisRetailerId();
		final String IDNumber = registerCCForm.getIDNumber();
		final String loyaltyRetailerId = registerCCForm.getLoyaltyRetailerId();
		final String memberID = registerCCForm.getMemberID();
		final int SAResident = registerCCForm.getIsSAResident();

		final String firstName = registerCCForm.getFirstName();
		//final String preferedName = registerCCForm.getPreferedName();
		final String lastName = registerCCForm.getLastName();
		//final String eMailAddress = registerCCForm.geteMailAddress();
		final int gender = registerCCForm.getGender();
		final String contactNumber = registerCCForm.getContactNumber();
		final String addressTypeId = registerCCForm.getAddressTypeId();
		final String addressLine1 = registerCCForm.getAddressLine1();
		//	final String addressLine2 = registerCCForm.getAddressLine2();
		final String suburb = registerCCForm.getSuburb();
		//final String city = registerCCForm.getCity();
		//final String province = registerCCForm.getProvince();
		final String country = registerCCForm.getCountry();
		final String postalCode = registerCCForm.getPostalCode();


		if (SAResident == 1)
		{
			if (!StringUtils.isEmpty(IDNumber))
			{
				if (StringUtils.length(IDNumber) != 13)
				{
					errors.rejectValue("IDNumber", "register.CC.IDNumber.invalid");
				}
			}
			if (StringUtils.isEmpty(IDNumber))
			{
				errors.rejectValue("IDNumber", "register.CC.IDNumber.empty");
			}
		}
		else if (SAResident == 0)
		{
			if (StringUtils.isEmpty(registerCCForm.getSA_DOB_day()))
			{
				errors.rejectValue("SA_DOB_day", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(registerCCForm.getSA_DOB_month()))
			{
				errors.rejectValue("SA_DOB_month", "register.CC.DOB.empty");
			}
			if (StringUtils.isEmpty(registerCCForm.getSA_DOB_year()))
			{
				errors.rejectValue("SA_DOB_year", "register.CC.DOB.empty");
			}
			if (!StringUtils.isEmpty(registerCCForm.getSA_DOB_day()) && !StringUtils.isEmpty(registerCCForm.getSA_DOB_month())
					&& !StringUtils.isEmpty(registerCCForm.getSA_DOB_year()))
			{
				final String date = registerCCForm.getSA_DOB_month() + "/" + registerCCForm.getSA_DOB_day() + "/"
						+ registerCCForm.getSA_DOB_year();
				try
				{
					final Date dobDate = new Date(date);
					final Date compareDate = new Date();
					compareDate.setYear(new Date().getYear() - 18);
					if (dobDate.after(compareDate))
					{
						// show error msg here -- age should be more than 18
						errors.rejectValue("SA_DOB_day", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_month", "register.CC.age.invalid");
						errors.rejectValue("SA_DOB_year", "register.CC.age.invalid");
					}
				}
				catch (final Exception e)
				{

					LOG.error(e);
				}
			}

		}
		/*
		 * if (!ValidatorUtils.validateRegex(EMAIL_REGEX, eMailAddress)) { errors.rejectValue("eMailAddress",
		 * "register.email.invalid"); } else if (StringUtils.length(registerCCForm.geteMailAddress()) > 255) {
		 * errors.rejectValue("eMailAddress", "register.email.invalid"); }
		 */
		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			errors.rejectValue("firstName", "register.CC.firstname.empty");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}
		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			errors.rejectValue("lastName", "register.CC.lastname.empty");
		}
		else if (StringUtils.length(lastName) > 30)
		{
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}
		if (StringUtils.isEmpty(addressLine1))
		{
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		else if (StringUtils.length(addressLine1) > 30)
		{
			errors.rejectValue("addressLine1", "register.enroll.addressLine1.invalid");
		}
		if (StringUtils.isEmpty(suburb))
		{
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		else if (StringUtils.length(suburb) > 30)
		{
			errors.rejectValue("suburb", "register.enroll.suburb.invalid");
		}
		if (!ValidatorUtils.validateRegex(NUMBER_REGEX, contactNumber))
		{
			errors.rejectValue("contactNumber", "register.enroll.phone.invalid");
		}
		else if (StringUtils.length(contactNumber) > 30)
		{
			errors.rejectValue("contactNumber", "register.enroll.phone.invalid");
		}
		if (StringUtils.isEmpty(country))
		{
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		else if (StringUtils.length(country) > 2)
		{
			errors.rejectValue("country", "register.enroll.country.invalid");
		}
		if (StringUtils.isEmpty(postalCode))
		{
			errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		}
		else if (StringUtils.length(postalCode) > 4)
		{
			errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		}
		else if (clicksCheckoutFacade.getDeliveryAreaDetails(postalCode) == null)
		{
			errors.rejectValue("postalCode", "register.enroll.postalCode.invalid");
		}

	}

	public boolean validateEmailAddress(final String email)
	{
		final Pattern pattern = Pattern.compile(EMAIL_REGEX);
		final Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
