/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.validators;


import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.clicks.storefront.forms.ClinicBookingForm;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author shruti.jhamb
 *
 */

/**
 * Validates clinic booking form.
 */

public class ClinicBookingValidator implements Validator
{
	public static final String MOBILE_REGEX = "\\d{10}";
	public static final String EMAIL_REGEX = FormPropertyConstants.Validator.Regex.EMAIL_REGEX;
	private static final String NAME_REGEX = FormPropertyConstants.Validator.Regex.CUSTOM_NAME_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;

	@Resource
	private MessageSource messageSource;

	@Resource
	private I18NService i18nService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ClinicBookingForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ClinicBookingForm clinicBookingForm = (ClinicBookingForm) object;
		final String firstName = clinicBookingForm.getFirstName();
		final String lastName = clinicBookingForm.getLastName();
		final String contactNumber = clinicBookingForm.getContactNumber();
		final String preferredClinic = clinicBookingForm.getPreferredClinic();
		//final String alternativeClinic = clinicBookingForm.getAlternativeClinic();
		final String region = clinicBookingForm.getRegion();

		if (!ValidatorUtils.validateRegex(NAME_REGEX, firstName))
		{
			errors.rejectValue("firstName", "register.CC.firstname.empty");
		}
		else if (StringUtils.length(firstName) > 30)
		{
			errors.rejectValue("firstName", "register.CC.firstname.invalid");
		}

		if (!ValidatorUtils.validateRegex(NAME_REGEX, lastName))
		{
			errors.rejectValue("lastName", "register.CC.lastname.empty");
		}
		else if (StringUtils.length(lastName) > 30)
		{
			errors.rejectValue("lastName", "register.CC.lastname.invalid");
		}

		if (!ValidatorUtils.validateRegex(NUMBER_REGEX, contactNumber))
		{
			errors.rejectValue("contactNumber", "clinic.booking.contactNumber.empty");
		}

		if (StringUtils.isEmpty(preferredClinic))
		{
			errors.rejectValue("preferredClinic", "text.clinicbooking.preferredClinic");
		}

		if (StringUtils.isEmpty(region))
		{
			errors.rejectValue("region", "text.clinicbooking.region");
		}
	}
}
