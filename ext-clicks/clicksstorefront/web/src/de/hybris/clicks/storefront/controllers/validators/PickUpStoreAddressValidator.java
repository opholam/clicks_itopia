/**
 *
 */
package de.hybris.clicks.storefront.controllers.validators;

import de.hybris.clicks.facades.checkout.ClicksCheckoutFacade;
import de.hybris.clicks.storefront.forms.PickUpStoreForm;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author Shruti Jhamb
 *
 */
public class PickUpStoreAddressValidator implements Validator
{
	private static final int MAX_ADDRESS_LENGTH = 255;
	private static final int MAX_POSTCODE_LENGTH = 10;
	static Logger LOG = Logger.getLogger(PickUpStoreAddressValidator.class.getName());

	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;
	@Resource(name = "clicksCheckoutFacade")
	private ClicksCheckoutFacade clicksCheckoutFacade;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return PickUpStoreForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final PickUpStoreForm pickUpStoreForm = (PickUpStoreForm) object;
		validateStandardFields(pickUpStoreForm, errors);
	}




	protected void validateStandardFields(final PickUpStoreForm pickUpStoreForm, final Errors errors)
	{
		validateStringField(pickUpStoreForm.getLine1(), AddressField.LINE1, MAX_ADDRESS_LENGTH, errors);
		validateStringFieldForLength(pickUpStoreForm.getLine2(), AddressField.LINE2, MAX_ADDRESS_LENGTH, errors);
		validateStringField(pickUpStoreForm.getPostcode(), AddressField.EMAIL, MAX_POSTCODE_LENGTH, errors);
	}



	protected static void validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}

		else if (StringUtils.length(addressField) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), "address.field.length.invalid");
		}
	}


	protected static void validateStringFieldForLength(final String addressField, final AddressField fieldType,
			final int maxFieldLength, final Errors errors)
	{
		if (StringUtils.length(addressField) > maxFieldLength)
		{
			if (fieldType.getFieldKey().equalsIgnoreCase("deliveryInstructions"))
			{
				errors.rejectValue(fieldType.getFieldKey(), "address.deliveryInstructions.length.invalid");
			}
			else
			{
				errors.rejectValue(fieldType.getFieldKey(), "address.field.length.invalid");
			}
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		USA("US"), CANADA("CA"), JAPAN("JP"), CHINA("CN"), BRITAIN("GB"), GERMANY("DE"), DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName",
				"address.lastName.invalid"), LINE1("line1", "address.line1.invalid"), LINE2("line2", "address.line2.invalid"), POSTCODE(
				"postcode", "address.postcode.invalid"), REGION("regionIso", "address.regionIso.invalid"), COUNTRY("countryIso",
				"address.country.invalid"), EMAIL("email", "address.email.invalid"), PHONE("phone", "address.phone.invalid"), COUNTRYNAME(
				"countryName", "address.country.invalid"), SUBURB("suburb", "address.suburb.invalid"), TOWNCITY("townCity",
				"address.townCity.invalid"), DELIVERYINSTRUCTIONS("deliveryInstructions",
				"address.deliveryInstructions.length.invalid");

		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}

}
