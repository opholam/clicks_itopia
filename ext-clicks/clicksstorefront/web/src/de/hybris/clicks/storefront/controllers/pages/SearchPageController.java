/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.article.ArticleSearchFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ArticleSearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * The Class SearchPageController.
 */
@Controller
@Scope("tenant")
@RequestMapping("/search")
public class SearchPageController extends AbstractSearchPageController
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(SearchPageController.class);

	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";

	private static final String SEARCH_CMS_PAGE_ID = "search";
	private static final String SEARCH_PROMOTION_CMS_PAGE_ID = "promotionsPage";
	private static final String NO_RESULTS_CMS_PAGE_ID = "searchEmpty";

	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "searchBreadcrumbBuilder")
	private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "articleSearchFacade")
	private ArticleSearchFacade articleSearchFacade;

	@Resource
	private ConfigurationService configurationService;

	/**
	 * Text search.
	 * 
	 * @param searchText
	 *           the search text
	 * @param count
	 *           the count
	 * @param request
	 *           the request
	 * @param model
	 *           the model
	 * @param redirectAttributes
	 *           the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET, params = "!q")
	public String textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText,
			@RequestParam(value = "count", defaultValue = "12") final int count, final HttpServletRequest request,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		if (StringUtils.isNotBlank(searchText))
		{
			final String size = (String) configurationService.getConfiguration().getProperty("search.max.articles.show");
			final int articlesSize = Integer.parseInt(size);
			model.addAttribute("maxNoOfSearchArticles" + articlesSize);

			final PageableData pageableData = createPageableData(0, count, null, ShowMode.Page);

			final SearchStateData searchState = new SearchStateData();
			final SearchQueryData searchQueryData = new SearchQueryData();
			searchQueryData.setValue(searchText);
			searchState.setQuery(searchQueryData);

			//search articles
			final List<ArticleSearchPageData> healthHubData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(searchText),
					articlesSize, "true", model);
			final List<ArticleSearchPageData> inspirationData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(searchText),
					articlesSize, "false", model);

			final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
					pageableData);
			if (searchPageData == null && healthHubData.isEmpty() && inspirationData.isEmpty())
			{
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
			}
			else if (searchPageData.getKeywordRedirectUrl() != null)
			{
				// if the search engine returns a redirect, just
				return "redirect:" + searchPageData.getKeywordRedirectUrl();
			}
			else if (searchPageData.getPagination().getTotalNumberOfResults() == 0 && healthHubData.isEmpty()
					&& inspirationData.isEmpty())
			{
				if (!request.getQueryString().contains("nsr=1"))
				{
					redirectAttributes.addAttribute("text", searchText);
					redirectAttributes.addAttribute("nsr", "1");
					return REDIRECT_PREFIX + "/search";
				}

				model.addAttribute("searchPageData", searchPageData);
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
				updatePageTitle(searchText, model);

			}
			else
			{
				storeContinueUrl(request);
				populateModel(model, searchPageData, ShowMode.Page);
				model.addAttribute("healthHubData", healthHubData);
				model.addAttribute("inspirationData", inspirationData);
				model.addAttribute("articlesSize", articlesSize);
				storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
				updatePageTitle(searchText, model);
			}
			model.addAttribute("userLocation", customerLocationService.getUserLocation());
			getRequestContextData(request).setSearch(searchPageData);
			if (searchPageData != null)
			{
				model.addAttribute(
						WebConstants.BREADCRUMBS_KEY,
						searchBreadcrumbBuilder.getBreadcrumbs(null, searchText,
								CollectionUtils.isEmpty(searchPageData.getBreadcrumbs())));

				model.addAttribute("count", count);
				model.addAttribute("pLoadcount", count);
			}

		}
		else
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());
		model.addAttribute("metaRobots", "noindex,follow");


		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(
				"search.meta.description.results", null, getI18nService().getCurrentLocale())
				+ " "
				+ searchText
				+ " "
				+ getMessageSource().getMessage("search.meta.description.on", null, getI18nService().getCurrentLocale())
				+ " "
				+ getSiteName());
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewForPage(model);
	}

	/**
	 * Refine search.
	 * 
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param searchText
	 *           the search text
	 * @param count
	 *           the count
	 * @param request
	 *           the request
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET, params = "q")
	public String refineSearch(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "text", required = false) final String searchText,
			@RequestParam(value = "count", defaultValue = "12") final int count, final HttpServletRequest request, final Model model)
			throws CMSItemNotFoundException
	{

		final String size = (String) configurationService.getConfiguration().getProperty("search.max.articles.show");
		final int articlesSize = Integer.parseInt(size);
		model.addAttribute("maxNoOfSearchArticles" + articlesSize);

		model.addAttribute("count", count);
		model.addAttribute("articlesSize", articlesSize);
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, count);

		populateModel(model, searchPageData, showMode);
		model.addAttribute("userLocation", customerLocationService.getUserLocation());
		//search articles
		final List<ArticleSearchPageData> healthHubData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(searchText),
				articlesSize, "true", model);
		final List<ArticleSearchPageData> inspirationData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(searchText),
				articlesSize, "false", model);
		model.addAttribute("healthHubData", healthHubData);
		model.addAttribute("inspirationData", inspirationData);
		if (searchPageData.getPagination().getTotalNumberOfResults() == 0 && healthHubData.isEmpty() && inspirationData.isEmpty())
		{
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		else
		{
			storeContinueUrl(request);
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
		}
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(null, searchPageData));
		model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());

		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(
				"search.meta.description.results", null, getI18nService().getCurrentLocale())
				+ " "
				+ searchText
				+ " "
				+ getMessageSource().getMessage("search.meta.description.on", null, getI18nService().getCurrentLocale())
				+ " "
				+ getSiteName());
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewForPage(model);
	}

	/**
	 * Load more product search.
	 * 
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param count
	 *           the count
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/productsLoadMore", method = RequestMethod.GET, params = "q")
	public String loadMoreProductSearch(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "count", defaultValue = "12") final int count, final Model model) throws CMSItemNotFoundException
	{

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, count);
		populateModel(model, searchPageData, showMode);
		storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
		return ControllerConstants.Views.Pages.Search.loadMoreSearchProducts;
	}

	protected ProductSearchPageData<SearchStateData, ProductData> performSearch(final String searchQuery, final int page,
			final ShowMode showMode, final String sortCode, final int pageSize)
	{
		final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);

		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		return productSearchFacade.textSearch(searchState, pageableData);
	}

	/**
	 * Json search results.
	 * 
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @return the search results data
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@ResponseBody
	@RequestMapping(value = "/results", method = RequestMethod.GET)
	public SearchResultsData<ProductData> jsonSearchResults(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException
	{
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, getSearchPageSize());
		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		searchResultsData.setResults(searchPageData.getResults());
		searchResultsData.setPagination(searchPageData.getPagination());
		return searchResultsData;
	}

	/**
	 * Gets the facets.
	 * 
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @return the facets
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@ResponseBody
	@RequestMapping(value = "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException
	{
		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
				createPageableData(page, getSearchPageSize(), sortCode, showMode));
		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
		refinement.setFacets(facets);
		refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
		refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
		return refinement;
	}

	/**
	 * Gets the autocomplete suggestions.
	 * 
	 * @param componentUid
	 *           the component uid
	 * @param term
	 *           the term
	 * @param model
	 *           the model
	 * @return the autocomplete suggestions
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@ResponseBody
	@RequestMapping(value = "/autocomplete/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid,
			@RequestParam("term") final String term, final Model model) throws CMSItemNotFoundException
	{
		final AutocompleteResultData resultData = new AutocompleteResultData();

		final String size = (String) configurationService.getConfiguration().getProperty("search.max.articles.show");
		final int articlesSize = Integer.parseInt(size);
		model.addAttribute("maxNoOfSearchArticles" + articlesSize);

		final SearchBoxComponentModel component = (SearchBoxComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);

		if (component.isDisplaySuggestions())
		{
			resultData.setSuggestions(subList(productSearchFacade.getAutocompleteSuggestions(term), component.getMaxSuggestions()));
		}

		if (component.isDisplayProducts())
		{
			resultData.setProducts(subList(productSearchFacade.textSearch(term).getResults(), component.getMaxProducts()));
		}
		//get the no of articles from component
		final int maxArticles = component.getMaxArticles();
		//search and get the articles
		final List<ArticleSearchPageData> healthHubData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(term),
				articlesSize, "true", model);
		final List<ArticleSearchPageData> inspirationData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(term),
				articlesSize, "false", model);

		final List<ArticleSearchPageData> list = new ArrayList<ArticleSearchPageData>();
		list.addAll(subList(inspirationData, maxArticles / 2));
		list.addAll(subList(healthHubData, maxArticles / 2));

		resultData.setArticles(list);

		return resultData;
	}

	protected <E> List<E> subList(final List<E> list, final int maxElements)
	{
		if (CollectionUtils.isEmpty(list))
		{
			return Collections.emptyList();
		}

		if (list.size() > maxElements)
		{
			return list.subList(0, maxElements);
		}

		return list;
	}

	/**
	 * Update page title.
	 * 
	 * @param searchText
	 *           the search text
	 * @param model
	 *           the model
	 */
	protected void updatePageTitle(final String searchText, final Model model)
	{
		storeContentPageTitleInModel(
				model,
				getPageTitleResolver().resolveContentPageTitle(
						getMessageSource().getMessage("search.meta.title", null, getI18nService().getCurrentLocale()) + " "
								+ searchText));
	}

	/**
	 * Load more article search.
	 * 
	 * @param searchText
	 *           the search text
	 * @param articlesSize
	 *           the articles size
	 * @param isHealthArticle
	 *           the is health article
	 * @param request
	 *           the request
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/loadMore", method = RequestMethod.GET)
	public String loadMoreArticleSearch(@RequestParam(value = "text", required = false) final String searchText,
			@RequestParam(value = "articlesSize", defaultValue = "3") final int articlesSize,
			@RequestParam(value = "isHealthArticle", defaultValue = "false") final String isHealthArticle,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		//search articles
		if (isHealthArticle.equals("true"))
		{
			final List<ArticleSearchPageData> healthHubData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(searchText),
					articlesSize, isHealthArticle, model);
			model.addAttribute("healthHubData", healthHubData);
			model.addAttribute("articlesSize", articlesSize);
			return ControllerConstants.Views.Pages.Search.loadMoreHealthHubPage;
		}
		else
		{
			final List<ArticleSearchPageData> inspirationData = articleSearchFacade.searchArticles(XSSFilterUtil.filter(searchText),
					articlesSize, isHealthArticle, model);
			model.addAttribute("inspirationData", inspirationData);
			model.addAttribute("articlesSize", articlesSize);
			return ControllerConstants.Views.Pages.Search.loadMoreInspirationPage;
		}
	}
}
