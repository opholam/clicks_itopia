/**
 *
 */
package de.hybris.clicks.storefront.controllers.cms;

import de.hybris.clicks.core.model.ConditionHeaderComponentModel;
import de.hybris.clicks.facade.hpcondition.HpConditionFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author mbhargava
 *
 */
@Controller("ConditionHeaderComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.ConditionHeaderComponent)
public class ConditionHeaderComponentController<T extends AbstractItemModel> extends
		AbstractCMSComponentController<ConditionHeaderComponentModel>
{
	static Logger log = Logger.getLogger(ConditionHeaderComponentController.class.getName());
	@Resource(name = "hpConditionFacade")
	private HpConditionFacade hpConditionFacade;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ConditionHeaderComponentModel component)
	{
		//		final String id = request.getParameter("id");
		//		final String type = request.getParameter("type");
		//		log.info("+++++++++++++++++++Entered into ConditionHeaderComponentController id: " + id + " type: " + type
		//				+ "+++++++++++++++++++");
		//		if ("CA".equalsIgnoreCase(type) && StringUtils.isNotBlank(id))
		//		{
		//			final ConditionsAreaModel conditionsAreaModel = hpConditionFacade.getConditionAreaAndStage(id, type);
		//			model.addAttribute("headerForCond", conditionsAreaModel.getConditionAreaName());
		//		}
		//		else if ("CG".equalsIgnoreCase(type) && StringUtils.isNotBlank(id))
		//		{
		//			final ConditionsStageModel conditionsStageModel = hpConditionFacade.getConditionAreaAndStage(id, type);
		//			model.addAttribute("headerForCond", conditionsStageModel.getConditionStageName());
		//		}
		//		else if ("CC".equalsIgnoreCase(type) && StringUtils.isNotBlank(id))
		//		{
		//			final ConditionsModel conditionsModel = hpConditionFacade.getConditionAreaAndStage(id, type);
		//			model.addAttribute("headerForCond", conditionsModel.getConditionName());
		//		}
		//		else if ("ALL".equalsIgnoreCase(type))
		//		{
		//			model.addAttribute("headerForCond", "All Conditions");
		//		}
	}

	@Override
	protected String getView(final ConditionHeaderComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(ConditionHeaderComponentModel._TYPECODE);
	}
}
