/**
 *
 */
package de.hybris.clicks.storefront.forms;


/**
 * @author shruti.jhamb
 *
 */
public class EditNonClubCardForm
{
	private String firstName;
	private String preferedName;
	private String lastName;
	private String eMailAddress;
	private String IDNumber;
	private String hybrisRetailerId;
	private String customerId;
	private int isSAResident = 1;
	private String SA_DOB_day;
	private String SA_DOB_month;
	private String SA_DOB_year;
	private String password;
	private boolean emailIDEditable;


	private String SA_DOB;

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the preferedName
	 */
	public String getPreferedName()
	{
		return preferedName;
	}

	/**
	 * @param preferedName
	 *           the preferedName to set
	 */
	public void setPreferedName(final String preferedName)
	{
		this.preferedName = preferedName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the eMailAddress
	 */
	public String geteMailAddress()
	{
		return eMailAddress;
	}

	/**
	 * @param eMailAddress
	 *           the eMailAddress to set
	 */
	public void seteMailAddress(final String eMailAddress)
	{
		this.eMailAddress = eMailAddress;
	}

	/**
	 * @return the iDNumber
	 */
	public String getIDNumber()
	{
		return IDNumber;
	}

	/**
	 * @param iDNumber
	 *           the iDNumber to set
	 */
	public void setIDNumber(final String iDNumber)
	{
		IDNumber = iDNumber;
	}


	/**
	 * @return the hybrisRetailerId
	 */
	public String getHybrisRetailerId()
	{
		return hybrisRetailerId;
	}

	/**
	 * @param hybrisRetailerId
	 *           the hybrisRetailerId to set
	 */
	public void setHybrisRetailerId(final String hybrisRetailerId)
	{
		this.hybrisRetailerId = hybrisRetailerId;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	/**
	 * @return the isSAResident
	 */
	public int getIsSAResident()
	{
		return isSAResident;
	}

	/**
	 * @param isSAResident
	 *           the isSAResident to set
	 */
	public void setIsSAResident(final int isSAResident)
	{
		this.isSAResident = isSAResident;
	}


	/**
	 * @return the sA_DOB_day
	 */
	public String getSA_DOB_day()
	{
		return SA_DOB_day;
	}

	/**
	 * @param sA_DOB_day
	 *           the sA_DOB_day to set
	 */
	public void setSA_DOB_day(final String sA_DOB_day)
	{
		SA_DOB_day = sA_DOB_day;
	}

	/**
	 * @return the sA_DOB_month
	 */
	public String getSA_DOB_month()
	{
		return SA_DOB_month;
	}

	/**
	 * @param sA_DOB_month
	 *           the sA_DOB_month to set
	 */
	public void setSA_DOB_month(final String sA_DOB_month)
	{
		SA_DOB_month = sA_DOB_month;
	}

	/**
	 * @return the sA_DOB_year
	 */
	public String getSA_DOB_year()
	{
		return SA_DOB_year;
	}

	/**
	 * @param sA_DOB_year
	 *           the sA_DOB_year to set
	 */
	public void setSA_DOB_year(final String sA_DOB_year)
	{
		SA_DOB_year = sA_DOB_year;
	}

	/**
	 * @return the sA_DOB
	 */
	public String getSA_DOB()
	{
		return SA_DOB;
	}

	/**
	 * @param sA_DOB
	 *           the sA_DOB to set
	 */
	public void setSA_DOB(final String sA_DOB)
	{
		SA_DOB = sA_DOB;
	}

	/**
	 * @return the emailIDEditable
	 */
	public boolean isEmailIDEditable()
	{
		return emailIDEditable;
	}

	/**
	 * @param emailIDEditable
	 *           the emailIDEditable to set
	 */
	public void setEmailIDEditable(final boolean emailIDEditable)
	{
		this.emailIDEditable = emailIDEditable;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}



}
