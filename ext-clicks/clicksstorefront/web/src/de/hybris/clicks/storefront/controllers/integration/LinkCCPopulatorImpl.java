/**
 *
 */
package de.hybris.clicks.storefront.controllers.integration;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.clicks.core.model.AccountsCBRModel;
import de.hybris.clicks.core.model.AccountsPSModel;
import de.hybris.clicks.core.model.BenefitModel;
import de.hybris.clicks.core.model.CBRtransactionModel;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.PS_PartnerPointModel;
import de.hybris.clicks.core.model.PS_PointsBucketModel;
import de.hybris.clicks.core.model.PStransactionModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.BenefitData;
import de.hybris.platform.commercefacades.user.data.BucketData;
import de.hybris.platform.commercefacades.user.data.CBRAccountData;
import de.hybris.platform.commercefacades.user.data.CBRtransactionData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.PS_AccountData;
import de.hybris.platform.commercefacades.user.data.PS_transactionData;
import de.hybris.platform.commercefacades.user.data.PS_transactionsData;
import de.hybris.platform.commercefacades.user.data.PartnerData;
import de.hybris.platform.commercefacades.user.data.PartnerPointsData;
import de.hybris.platform.commercefacades.user.data.PointStatementData;
import de.hybris.platform.commercefacades.user.data.PointsBucketsData;
import de.hybris.platform.commercefacades.user.data.SegmentData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author shruti.jhamb
 *
 */
public class LinkCCPopulatorImpl implements LinkCCPopulator
{

	@Resource
	private ModelService modelService;
	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;
	@Resource
	private UserService userService;
	@Resource
	FlexibleSearchService flexibleSearchService;
	@Resource
	private CustomerNameStrategy customerNameStrategy;

	@Override
	public boolean populateClubcardData(CustomerData customerData)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		final CustomerModel source = (CustomerModel) userService.getUserForUID(customerData.getEmailID());
		customerModel.setOriginalUid(customerData.getDisplayUid());
		customerData = populateCurrentCustModel(source, customerData);
		modelService.remove(source);
		final boolean result = populateLinkCCCustModel(customerData, customerModel);
		modelService.save(customerModel);
		modelService.refresh(customerModel);
		return result;
	}


	public CustomerData reversePopulateCCData(CustomerData customerData)
	{
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		customerData = populateCurrentCustModel(customerModel, customerData);
		return customerData;
	}

	//save linked account details to model
	@Override
	public boolean saveUpdatedDetails(final CustomerData customer)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		customerModel.setOriginalUid(customer.getDisplayUid());
		final boolean result = populateLinkedCCCustModel(customer, customerModel);
		modelService.save(customerModel);
		return result;
	}

	/**
	 * @param customer
	 * @param customerModel
	 * @return
	 */
	private boolean populateLinkedCCCustModel(final CustomerData customer, final CustomerModel customerModel)
	{
		if (null != customer.getFirstName())
		{
			customerModel.setFirstName(customer.getFirstName());
		}
		if (null != customer.getPreferedName())
		{
			customerModel.setName(customer.getPreferedName());
		}
		if (null != customer.getLastName())
		{
			customerModel.setLastName(customer.getLastName());
		}
		if (null != customer.getEmailID())
		{
			customerModel.setEmailID(customer.getEmailID());
		}
		if (null != customer.getGender())
		{
			if (null != customer.getGender() && customer.getGender().equalsIgnoreCase("male"))
			{
				customerModel.setGender(Gender.MALE);
			}
			if (null != customer.getGender() && customer.getGender().equalsIgnoreCase("female"))
			{
				customerModel.setGender(Gender.FEMALE);
			}
		}
		if (null != customer.getSAResident())
		{
			customerModel.setSAResident(customer.getSAResident());
		}
		if (customerModel.getSAResident().booleanValue() && null != customer.getIDnumber())
		{
			customerModel.setRSA_ID(customer.getIDnumber());
		}
		if ( null != customer.getNonRSA_DOB())
		{
			customerModel.setNonRSA_DOB(customer.getNonRSA_DOB());
		}
		if (CollectionUtils.isNotEmpty(customer.getAddresses()))
		{
			final List<AddressModel> address = new ArrayList<AddressModel>();
			for (final AddressData addressData : customer.getAddresses())
			{
				//addressData.setOwner(customer);
				final AddressModel addressModel = new AddressModel();
				addressModel.setOwner(customerModel);
				if (null != addressData.getLine1())
				{
					addressModel.setBuilding(addressData.getLine1());
				}
				if (null != addressData.getLine2())
				{
					addressModel.setStreetname(addressData.getLine2());
				}
				if (null != addressData.getSuburb())
				{
					addressModel.setStreetnumber(addressData.getSuburb());
				}
				if (null != addressData.getCity() && null != addressData.getProvince())
				{
					addressModel.setTown(addressData.getCity() + ", " + addressData.getProvince());
				}
				else if (null != addressData.getCity())
				{
					addressModel.setTown(addressData.getCity());
				}
				else
				{
					addressModel.setTown(addressData.getProvince());
				}
				if (null != addressData.getPostalCode())
				{
					addressModel.setPostalcode(addressData.getPostalCode());
				}
				if (null != addressData.getCountry())
				{
					CountryModel country = new CountryModel();
					modelService.attach(country);
					country.setIsocode(addressData.getCountry().getIsocode());
					country = flexibleSearchService.getModelByExample(country);
					if (null != country)
					{
						addressModel.setCountry(country);
					}
				}
				address.add(addressModel);
			}
			customerModel.setAddresses(address);
		}
		final List<ContactDetailsModel> contactDetailsList = new ArrayList<ContactDetailsModel>();
		if (customer.getContactDetails().size() > 0)
		{
			for (final ContactDetail contactDetail : customer.getContactDetails())
			{
				final ContactDetailsModel contactDetailsModel = new ContactDetailsModel();
				contactDetailsModel.setTypeID(contactDetail.getTypeID());
				contactDetailsModel.setNumber(contactDetail.getNumber());
				contactDetailsList.add(contactDetailsModel);
			}
		}
		if (contactDetailsList.size() > 0)
		{
			customerModel.setContactDetails(contactDetailsList);
		}
		return true;


	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private CustomerData populateCurrentCustModel(final CustomerModel source, final CustomerData target)
	{
		if (null != source.getEmailID() && target.getEmailID() == null)
		{
			target.setEmailID(source.getEmailID());
		}
		if (null != source.getFirstName() && target.getFirstName() == null)
		{
			target.setFirstName(source.getFirstName());
		}
		if (null != source.getLastName() && target.getLastName() == null)
		{
			target.setLastName(source.getLastName());
		}
		if (null != source.getSAResident() && target.getSAResident() == null && source.getSAResident().booleanValue())
		{
			target.setSAResident(source.getSAResident());
			if (null != source.getRSA_ID() && target.getRSA_ID() == null)
			{
				target.setRSA_ID(source.getRSA_ID());
			}
		}
		
			if (null != source.getNonRSA_DOB() && target.getNonRSA_DOB() == null)
			{
				target.setNonRSA_DOB(source.getNonRSA_DOB());
			}
		
		if (null != source.getGender() && target.getGender() == null)
		{
			target.setGender(source.getGender().getCode());
		}
		if (CollectionUtils.isNotEmpty(source.getContactDetails()) && target.getContactDetails() == null)
		{
			final List<ContactDetail> contactDataList = new ArrayList<ContactDetail>();
			for (final ContactDetailsModel contactDetailsModel : source.getContactDetails())
			{
				final ContactDetail contactData = new ContactDetail();
				contactData.setTypeID(contactDetailsModel.getTypeID());
				contactData.setNumber(contactDetailsModel.getNumber());
				contactDataList.add(contactData);
			}
			target.setContactDetails(contactDataList);
		}
		if (CollectionUtils.isNotEmpty(source.getAddresses()) && target.getAddresses() == null)
		{
			final List<AddressData> address = new ArrayList<AddressData>();
			for (final AddressModel addressmodel : source.getAddresses())
			{
				addressmodel.setOwner(source);
				final AddressData addressData = new AddressData();
				if (null != addressmodel.getBuilding())
				{
					addressData.setLine1(addressmodel.getBuilding());
				}
				if (null != addressmodel.getStreetname())
				{
					addressData.setLine2(addressmodel.getStreetname());
				}
				if (null != addressmodel.getStreetnumber())
				{
					addressData.setSuburb(addressmodel.getStreetnumber());
				}
				if (null != addressmodel.getTown())
				{
					final String[] townSplit = customerNameStrategy.splitName(addressmodel.getTown());
					if (townSplit != null)
					{
						addressData.setCity(townSplit[0]);
						addressData.setProvince(townSplit[1]);
					}
					else
					{
						addressData.setCity(addressmodel.getTown());
					}
				}
				if (null != addressmodel.getPostalcode())
				{
					addressData.setPostalCode(addressmodel.getPostalcode());
				}
				final CountryData country = new CountryData();
				if (null != addressmodel.getCountry() && null != addressmodel.getCountry().getIsocode())
				{
					country.setIsocode(addressmodel.getCountry().getIsocode());
					addressData.setCountry(country);
				}
				address.add(addressData);
			}
			target.setAddresses(address);
		}
		if (null != source.getPointsStatement())
		{
			final PointStatementData pointStatementData = new PointStatementData();
			pointStatementData.setStartDate(source.getPointsStatement().getStartDate());
			pointStatementData.setEndDate(source.getPointsStatement().getEndDate());

			if (null != source.getPointsStatement().getAccountsPSList())
			{
				final List<AccountsPSModel> accountsModel = source.getPointsStatement().getAccountsPSList();
				final List<PS_AccountData> accountsData = new ArrayList<PS_AccountData>(2);
				for (final AccountsPSModel accountsPSModel : accountsModel)
				{
					final PS_AccountData ps_AccountData = new PS_AccountData();
					ps_AccountData.setAccountID(accountsPSModel.getAccountID());
					ps_AccountData.setPointsBalance(accountsPSModel.getPointsBalance());
					ps_AccountData.setPointsToQualify(accountsPSModel.getPointsToQualify());
					ps_AccountData.setTotalSpent(accountsPSModel.getTotalSpent());

					final PointsBucketsData pointsBucketsData = new PointsBucketsData();
					pointsBucketsData.setBalance(accountsPSModel.getPointsBuckets().getBalance());

					final List<BucketData> bucketDataList = new ArrayList<BucketData>(2);
					for (final PS_PointsBucketModel bucketModel : accountsPSModel.getPointsBuckets().getPS_PointsBucketList())
					{
						final BucketData bucketData = new BucketData();
						bucketData.setName(bucketModel.getName());
						bucketData.setValue(bucketModel.getValue());
						bucketDataList.add(bucketData);
					}
					pointsBucketsData.setBuckets(bucketDataList);

					ps_AccountData.setClicksPoints(pointsBucketsData);

					final PartnerPointsData partnerPointsData = new PartnerPointsData();
					partnerPointsData.setBalance(accountsPSModel.getPartnerPoints().getBalance());

					final List<PartnerData> partnerDataList = new ArrayList<PartnerData>(2);

					for (final PS_PartnerPointModel partnerModel : accountsPSModel.getPartnerPoints().getPS_PartnerPointList())
					{
						final PartnerData partnerData = new PartnerData();
						partnerData.setName(partnerModel.getName());
						partnerData.setValue(partnerModel.getValue());

						partnerDataList.add(partnerData);
					}

					partnerPointsData.setPartners(partnerDataList);
					ps_AccountData.setPartnerPoints(partnerPointsData);

					final List<PS_transactionData> ps_transactionDataList = new ArrayList<PS_transactionData>(2);

					final PS_transactionsData ps_transactionsData = new PS_transactionsData();
					ps_transactionsData.setPointsBalance(accountsPSModel.getPStransactions().getBalance());


					for (final PStransactionModel pstransactionModel : accountsPSModel.getPStransactions().getPStransactionList())
					{
						final PS_transactionData ps_transactionData = new PS_transactionData();
						ps_transactionData.setDate(pstransactionModel.getDate());
						ps_transactionData.setEarned(pstransactionModel.getEarned());
						ps_transactionData.setLocation(pstransactionModel.getLocation());
						ps_transactionData.setSpent(pstransactionModel.getSpent());
						ps_transactionDataList.add(ps_transactionData);
					}
					ps_transactionsData.setTransactions(ps_transactionDataList);
					ps_AccountData.setTransactions(ps_transactionsData);

					accountsData.add(ps_AccountData);
				}
				pointStatementData.setAccounts(accountsData);
			}

			target.setPointStatement(pointStatementData);
		}

		// Population CBR Statements
		if (null != source.getCBRstatement() && CollectionUtils.isNotEmpty(source.getCBRstatement().getAccountsCBRList()))
		{
			final List<CBRAccountData> cbrstatement = new ArrayList<CBRAccountData>(2);
			for (final AccountsCBRModel accountsCBRModel : source.getCBRstatement().getAccountsCBRList())
			{
				final CBRAccountData cbrAccountData = new CBRAccountData();
				cbrAccountData.setAccountID(accountsCBRModel.getAccountID());
				cbrAccountData.setAvailableBalance(accountsCBRModel.getCBRbalance());

				final List<CBRtransactionData> cbrTransactionDataList = new ArrayList<CBRtransactionData>(2);
				for (final CBRtransactionModel cbrTransactionModel : accountsCBRModel.getCBRtransactionList())
				{
					final CBRtransactionData cbrtransactionData = new CBRtransactionData();
					cbrtransactionData.setAmountIssued(cbrTransactionModel.getAmountIssued());
					cbrtransactionData.setAvailableAmount(cbrTransactionModel.getAvailableAmount());
					cbrtransactionData.setExpiryDate(cbrTransactionModel.getExpiryDate());
					cbrtransactionData.setIssueDate(cbrTransactionModel.getIssueDate());

					cbrTransactionDataList.add(cbrtransactionData);
				}

				cbrAccountData.setCBRtransactions(cbrTransactionDataList);
				cbrstatement.add(cbrAccountData);
			}

			target.setCBRstatement(cbrstatement);
		}

		// Populating Financial Sevices
		if (CollectionUtils.isNotEmpty(source.getFinancialServices()))
		{
			final List<BenefitData> financialServices = new ArrayList<BenefitData>(2);
			for (final BenefitModel benefitModel : source.getFinancialServices())
			{
				final BenefitData benefitData = new BenefitData();
				benefitData.setBenefitAmount(benefitModel.getBenefitAmount());
				benefitData.setBenefitDate(benefitModel.getBenefitDate());
				benefitData.setBenefitId(benefitModel.getBenefitId());
				benefitData.setBenefitStatus(benefitModel.getStatus());
				financialServices.add(benefitData);
			}

			target.setFinancialServices(financialServices);
		}

		if (CollectionUtils.isNotEmpty(source.getNonLoyalty_segments()))
		{
			final List<SegmentData> segments = new ArrayList<SegmentData>(2);
			for (final UserGroupModel userGroup : source.getNonLoyalty_segments())
			{
				final SegmentData segmentData = new SegmentData();
				segmentData.setSegmentID(userGroup.getSegmentID());
				segmentData.setSegmentDescription(userGroup.getSegmentDescription());
				segmentData.setSegmentStatus(userGroup.getSegmentStatus());
				segmentData.setSegmentType(userGroup.getSegmentType());
				segments.add(segmentData);
			}

			target.setLoyalty_segments(segments);
		}

		return target;
	}

	/**
	 * @param customerData
	 * @param customerModel
	 * @return
	 */
	private boolean populateLinkCCCustModel(final CustomerData customerData, final CustomerModel customerModel)
	{
		if (null != customerData.getMemberID())
		{
			customerModel.setMemberID(customerData.getMemberID());
		}
		if (null != customerData.getFirstName())
		{
			customerModel.setFirstName(customerData.getFirstName());
		}
		if (null != customerData.getLastName())
		{
			customerModel.setLastName(customerData.getLastName());
		}
		if (null != customerData.getEmailID())
		{
			customerModel.setEmailID(customerData.getEmailID());
		}
		if (null != customerData.getSAResident())
		{
			customerModel.setSAResident(customerData.getSAResident());
			if (customerData.getSAResident().booleanValue())
			{
				customerModel.setRSA_ID(customerData.getRSA_ID());
			}
			if (null != customerData.getNonRSA_DOB())
			{
				customerModel.setNonRSA_DOB(customerData.getNonRSA_DOB());
			}
		}
		if (null != customerData.getGender())
		{
			if (null != customerData.getGender() && customerData.getGender().equalsIgnoreCase("male"))
			{
				customerModel.setGender(Gender.MALE);
			}
			if (null != customerData.getGender() && customerData.getGender().equalsIgnoreCase("female"))
			{
				customerModel.setGender(Gender.FEMALE);
			}
		}
		if (CollectionUtils.isNotEmpty(customerData.getAddresses()))
		{
			final List<AddressModel> address = new ArrayList<AddressModel>();
			for (final AddressData addressData : customerData.getAddresses())
			{
				//addressData.setOwner(customer);
				final AddressModel addressModel = new AddressModel();
				addressModel.setOwner(customerModel);
				if (null != addressData.getLine1())
				{
					addressModel.setBuilding(addressData.getLine1());
				}
				if (null != addressData.getLine2())
				{
					addressModel.setStreetname(addressData.getLine2());
				}
				if (null != addressData.getSuburb())
				{
					addressModel.setStreetnumber(addressData.getSuburb());
				}
				if (null != addressData.getCity() && null != addressData.getProvince())
				{
					addressModel.setTown(addressData.getCity() + ", " + addressData.getProvince());
				}
				else if (null != addressData.getCity())
				{
					addressModel.setTown(addressData.getCity());
				}
				else
				{
					addressModel.setTown(addressData.getProvince());
				}
				if (null != addressData.getPostalCode())
				{
					addressModel.setPostalcode(addressData.getPostalCode());
				}
				if (null != addressData.getCountry())
				{
					CountryModel country = new CountryModel();
					modelService.attach(country);
					country.setIsocode(addressData.getCountry().getIsocode());
					country = flexibleSearchService.getModelByExample(country);
					if (null != country)
					{
						addressModel.setCountry(country);
					}
				}
				address.add(addressModel);
			}
			customerModel.setAddresses(address);
		}
		final List<ContactDetailsModel> contactDetailsList = new ArrayList<ContactDetailsModel>();
		if (null != customerData.getContactDetails() && customerData.getContactDetails().size() > 0)
		{
			for (final ContactDetail contactDetail : customerData.getContactDetails())
			{
				final ContactDetailsModel contactDetailsModel = new ContactDetailsModel();
				contactDetailsModel.setTypeID(contactDetail.getTypeID());
				contactDetailsModel.setNumber(contactDetail.getNumber());
				contactDetailsList.add(contactDetailsModel);
			}
		}
		if (contactDetailsList.size() > 0)
		{
			customerModel.setContactDetails(contactDetailsList);
		}
		return true;
	}
}
