/**
 *
 */
package de.hybris.clicks.storefront.edit.profile.processor;

import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 * @author shruthi.jhamb
 *
 */
public interface UpdateProfile
{
	public boolean saveUpdatedClubCardData(CustomerData customerData);

	public boolean saveUpdatedNonClubCardData(CustomerData customerData);
}
