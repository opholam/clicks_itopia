/**
 *
 */
package de.hybris.clicks.storefront.sort;

import de.hybris.platform.commercefacades.user.data.PreferredClinicData;

import java.util.Comparator;


/**
 * @author sridhar.reddy
 *
 */
public class StoreSort implements Comparator<PreferredClinicData>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final PreferredClinicData o1, final PreferredClinicData o2)
	{
		// YTODO Auto-generated method stub
		return o1.getName().compareToIgnoreCase(o2.getName());
	}

}
