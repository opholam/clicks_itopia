/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facade.promotion.PromotionSearchFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.resolver.ClicksPageTitleResolver;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.solrfacetsearch.enums.ConverterType;
import de.hybris.platform.solrfacetsearch.search.Facet;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author mbhargava
 * 
 *         Controller for Promotions landing page
 */

@Controller
@Scope("tenant")
@RequestMapping("/specials")
public class PromotionsPageController extends AbstractSearchPageController
{
	@Resource(name = "promotionSearchFacade")
	private PromotionSearchFacade promotionSearchFacade;

	private static final String SEARCH_PROMOTION_CMS_PAGE_ID = "promotionsPage";
	private static final Logger LOG = Logger.getLogger(PromotionsPageController.class);

	/**
	 * Promotion search.
	 * 
	 * @param searchQuery
	 *           the search query
	 * @param pageSize
	 *           the page size
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param model
	 *           the model
	 * @param promotionTypeID
	 *           the promotion type id
	 * @param offset
	 *           the offset
	 * @param allCategories
	 *           the all categories
	 * @param request
	 *           the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String promotionSearch(@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "size", defaultValue = "0") int pageSize,
			@RequestParam(value = "show", defaultValue = "12") final int showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model,
			@RequestParam(value = "promotionTypeID", required = false) final String promotionTypeID,
			@RequestParam(value = "page", defaultValue = "0") final int offset,
			@RequestParam(value = "allCategories", required = false) final String allCategories, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		String queryString = StringUtils.isNotBlank(request.getQueryString()) ? request.getQueryString().replace(
				"&size=" + pageSize, "") : "";
		queryString = StringUtils.isNotBlank(request.getQueryString()) ? request.getQueryString().replace("size=" + pageSize, "")
				: "";
		queryString = StringUtils.isNotBlank(request.getQueryString()) ? request.getQueryString().replace("&sort=" + sortCode, "")
				: "";
		if (pageSize == 0)
		{
			pageSize = 12;
		}
		else
		{
			pageSize = +pageSize;
		}

		final SolrSearchResult result = promotionSearchFacade.getPromotions(queryString, pageSize, sortCode, offset);
		if (null != result)
		{
			model.addAttribute("searchResultData", result.getResultData(ConverterType.STOREFRONT));
			model.addAttribute("searchResult", result);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("searchQuery", searchQuery);
			model.addAttribute("page", result.getOffset());
			if (result.getOffset() > 0)
			{
				model.addAttribute("hideLoadMore", result.getNumberOfPages() == (result.getOffset() + 1) ? true : false);
				return ControllerConstants.Views.Pages.Search.loadMoreSearchPromotions;
			}
		}
		else if (offset > 0)
		{
			return "";
		}
		String facetCode = null;
		String displayName = null;
		String promotionsPageTitle = null;

		if (StringUtils.isNotEmpty(allCategories))
		{
			final List<String> categoryNames = Arrays.asList(allCategories.split(","));
			if (!CollectionUtils.isEmpty(categoryNames))
			{
				facetCode = categoryNames.get(0);
			}
		}
		else if (StringUtils.isNotEmpty(promotionTypeID))
		{
			final List<String> promotionTypes = Arrays.asList(promotionTypeID.split(","));
			if (!CollectionUtils.isEmpty(promotionTypes))
			{
				facetCode = promotionTypes.get(0);
			}

		}

		if (null != result)
		{
			Facet facet = null;
			String displayNameSuffix = "";
			if (StringUtils.isNotBlank(allCategories))
			{
				facet = result.getFacet("allCategories");
				displayNameSuffix = " Promotions";
			}
			else if (StringUtils.isNotBlank(promotionTypeID))
			{
				facet = result.getFacet("promotionTypeID");
			}

			if (null != facet)
			{
				for (final FacetValue facetValue : facet.getFacetValues())
				{
					if (StringUtils.isNotBlank(facetCode) && facetCode.equalsIgnoreCase(facetValue.getName())
							&& facetValue.isSelected())
					{
						displayName = facetValue.getDisplayName();
						break;
					}
				}
			}
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Promotion for " + displayName);
			}
			if (null == displayName)
			{
				promotionsPageTitle = "Promotions";
			}
			else
			{
				promotionsPageTitle = displayName + displayNameSuffix;
			}
			model.addAttribute("promotionsPageTitle", promotionsPageTitle);

		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_PROMOTION_CMS_PAGE_ID));
		model.addAttribute("metatags", new ArrayList<MetaElementData>());
		updatePageTitle(displayName, model);

		final String metaDescription = getMetaDescription(displayName);
		setUpMetaData(model, "", metaDescription);
		return getViewForPage(model);
	}


	/**
	 * Update page title.
	 * 
	 * @param <QUERY>
	 *           the generic type
	 * @param categoryName
	 *           the category name
	 * @param model
	 *           the model
	 */
	protected <QUERY> void updatePageTitle(final String categoryName, final Model model)
	{
		storeContentPageTitleInModel(model,
				((ClicksPageTitleResolver) getPageTitleResolver()).resolveCategoryPageTitle(categoryName));

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Updated Page Title");
		}
	}

	/**
	 * Gets the meta description.
	 * 
	 * @param displayName
	 *           the display name
	 * @return the meta description
	 */
	private String getMetaDescription(String displayName)
	{
		if (StringUtils.isNotEmpty(displayName))
		{
			final StringBuilder promoCategory = new StringBuilder("Browse through the available ");
			promoCategory.append(displayName);
			promoCategory.append(" specials offers at Clicks.");
			displayName = promoCategory.toString();
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Page Description : " + displayName);
			}
		}
		else
		{
			displayName = "Browse through hundreds of special offers at Clicks";
		}
		return displayName;
	}
}
