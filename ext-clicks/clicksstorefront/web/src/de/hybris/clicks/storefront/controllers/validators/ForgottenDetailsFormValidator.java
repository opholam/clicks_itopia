/**
 *
 */
package de.hybris.clicks.storefront.controllers.validators;

import de.hybris.clicks.storefront.controllers.FormPropertyConstants;
import de.hybris.clicks.storefront.forms.ForgottenDetailsForm;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author siva.reddy
 *
 */
@Component("forgottenDetailsFormValidator")
public class ForgottenDetailsFormValidator implements Validator
{

	public static final String ALPHA_NUMERIC_REGEX = FormPropertyConstants.Validator.Regex.ALPHA_NUMERIC_REGEX;
	public static final String NUMBER_REGEX = FormPropertyConstants.Validator.Regex.NUMBER_REGEX;

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ForgottenDetailsForm forgottenDetailsForm = (ForgottenDetailsForm) object;
		final String clubcardNumber = forgottenDetailsForm.getClubCardNumber();
		final boolean isResident = forgottenDetailsForm.isResident();
		final String residentNumber = forgottenDetailsForm.getResidentNumber();
		final String day = forgottenDetailsForm.getDay();
		final String month = forgottenDetailsForm.getMonth();
		final String year = forgottenDetailsForm.getYear();

		if (!ValidatorUtils.validateRegex(ALPHA_NUMERIC_REGEX, clubcardNumber))
		{
			errors.rejectValue("clubCardNumber", "forgotten.details.clubcardnumber.invalid");
		}
		else if (clubcardNumber.length() > 13)
		{
			errors.rejectValue("clubCardNumber", "forgotten.details.clubcardnumber.maxlength.invalid");
		}
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ForgottenDetailsForm.class.equals(aClass);
	}

}
