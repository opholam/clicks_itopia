/**
 *
 */
package de.hybris.clicks.storefront.forms;



/**
 * @author shruti.jhamb
 *
 */
public class LinkCCForm
{
	private String ccNumber;
	private String emailAddress;
	private String firstName;
	private String preferedName;
	private String lastName;
	private int gender;
	private int email;
	private String SA_DOB_day;
	private String SA_DOB_month;
	private String SA_DOB_year;
	private String nonRSA_DOB;
	private String addressTypeId;
	private String addressLine1;

	private String addressLine2;
	private String suburb;
	private String city;
	private String province;
	private String country = "ZA";

	private String postalCode;
	private String IDNumber;
	private String alternateNumber;
	private String contactNumber;

	private boolean marketingEmail;
	private boolean marketingSMS;
	private boolean accinfoEmail;
	private boolean accinfoSMS;
	private boolean SAResident;


	/**
	 * @return the ccNumber
	 */
	public String getCcNumber()
	{
		return ccNumber;
	}

	/**
	 * @param ccNumber
	 *           the ccNumber to set
	 */
	public void setCcNumber(final String ccNumber)
	{
		this.ccNumber = ccNumber;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *           the emailAddress to set
	 */
	public void setEmailAddress(final String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the preferedName
	 */
	public String getPreferedName()
	{
		return preferedName;
	}

	/**
	 * @param preferedName
	 *           the preferedName to set
	 */
	public void setPreferedName(final String preferedName)
	{
		this.preferedName = preferedName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the gender
	 */
	public int getGender()
	{
		return gender;
	}

	/**
	 * @param gender
	 *           the gender to set
	 */
	public void setGender(final int gender)
	{
		this.gender = gender;
	}

	/**
	 * @return the sA_DOB_day
	 */
	public String getSA_DOB_day()
	{
		return SA_DOB_day;
	}

	/**
	 * @param sA_DOB_day
	 *           the sA_DOB_day to set
	 */
	public void setSA_DOB_day(final String sA_DOB_day)
	{
		SA_DOB_day = sA_DOB_day;
	}

	/**
	 * @return the sA_DOB_month
	 */
	public String getSA_DOB_month()
	{
		return SA_DOB_month;
	}

	/**
	 * @param sA_DOB_month
	 *           the sA_DOB_month to set
	 */
	public void setSA_DOB_month(final String sA_DOB_month)
	{
		SA_DOB_month = sA_DOB_month;
	}

	/**
	 * @return the sA_DOB_year
	 */
	public String getSA_DOB_year()
	{
		return SA_DOB_year;
	}

	/**
	 * @param sA_DOB_year
	 *           the sA_DOB_year to set
	 */
	public void setSA_DOB_year(final String sA_DOB_year)
	{
		SA_DOB_year = sA_DOB_year;
	}

	/**
	 * @return the nonRSA_DOB
	 */
	public String getNonRSA_DOB()
	{
		return nonRSA_DOB;
	}

	/**
	 * @param nonRSA_DOB
	 *           the nonRSA_DOB to set
	 */
	public void setNonRSA_DOB(final String nonRSA_DOB)
	{
		this.nonRSA_DOB = nonRSA_DOB;
	}

	/**
	 * @return the addressTypeId
	 */
	public String getAddressTypeId()
	{
		return addressTypeId;
	}

	/**
	 * @param addressTypeId
	 *           the addressTypeId to set
	 */
	public void setAddressTypeId(final String addressTypeId)
	{
		this.addressTypeId = addressTypeId;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *           the addressLine1 to set
	 */
	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *           the addressLine2 to set
	 */
	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the suburb
	 */
	public String getSuburb()
	{
		return suburb;
	}

	/**
	 * @param suburb
	 *           the suburb to set
	 */
	public void setSuburb(final String suburb)
	{
		this.suburb = suburb;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the province
	 */
	public String getProvince()
	{
		return province;
	}

	/**
	 * @param province
	 *           the province to set
	 */
	public void setProvince(final String province)
	{
		this.province = province;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *           the postalCode to set
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the alternateNumber
	 */
	public String getAlternateNumber()
	{
		return alternateNumber;
	}

	/**
	 * @param alternateNumber
	 *           the alternateNumber to set
	 */
	public void setAlternateNumber(final String alternateNumber)
	{
		this.alternateNumber = alternateNumber;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *           the contactNumber to set
	 */
	public void setContactNumber(final String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the marketingEmail
	 */
	public boolean isMarketingEmail()
	{
		return marketingEmail;
	}

	/**
	 * @param marketingEmail
	 *           the marketingEmail to set
	 */
	public void setMarketingEmail(final boolean marketingEmail)
	{
		this.marketingEmail = marketingEmail;
	}

	/**
	 * @return the marketingSMS
	 */
	public boolean isMarketingSMS()
	{
		return marketingSMS;
	}

	/**
	 * @param marketingSMS
	 *           the marketingSMS to set
	 */
	public void setMarketingSMS(final boolean marketingSMS)
	{
		this.marketingSMS = marketingSMS;
	}

	/**
	 * @return the accinfoEmail
	 */
	public boolean isAccinfoEmail()
	{
		return accinfoEmail;
	}

	/**
	 * @param accinfoEmail
	 *           the accinfoEmail to set
	 */
	public void setAccinfoEmail(final boolean accinfoEmail)
	{
		this.accinfoEmail = accinfoEmail;
	}

	/**
	 * @return the accinfoSMS
	 */
	public boolean isAccinfoSMS()
	{
		return accinfoSMS;
	}

	/**
	 * @param accinfoSMS
	 *           the accinfoSMS to set
	 */
	public void setAccinfoSMS(final boolean accinfoSMS)
	{
		this.accinfoSMS = accinfoSMS;
	}

	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the sAResident
	 */
	public boolean isSAResident()
	{
		return SAResident;
	}

	/**
	 * @param sAResident
	 *           the sAResident to set
	 */
	public void setSAResident(final boolean sAResident)
	{
		SAResident = sAResident;
	}

	/**
	 * @return the iDNumber
	 */
	public String getIDNumber()
	{
		return IDNumber;
	}

	/**
	 * @param iDNumber
	 *           the iDNumber to set
	 */
	public void setIDNumber(final String iDNumber)
	{
		IDNumber = iDNumber;
	}

	/**
	 * @return the email
	 */
	public int getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final int email)
	{
		this.email = email;
	}


}
