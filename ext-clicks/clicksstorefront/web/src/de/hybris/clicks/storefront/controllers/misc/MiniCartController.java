/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.misc;

import de.hybris.clicks.facade.cartPage.CartPageFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Controller for MiniCart functionality which is not specific to a page.
 */
@Controller
@Scope("tenant")
public class MiniCartController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(MiniCartController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String TOTAL_DISPLAY_PATH_VARIABLE_PATTERN = "{totalDisplay:.*}";
	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource
	private CartService cartService;

	@Resource(name = "cartPageFacade")
	private CartPageFacade cartPageFacade;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@RequestMapping(value = "/cart/miniCart/" + TOTAL_DISPLAY_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String getMiniCart(@PathVariable final String totalDisplay, final Model model)
	{
		if (cartService.hasSessionCart())
		{
			CartModel cartmodel = cartService.getSessionCart();
			if (!Boolean.FALSE.equals(cartmodel.getBasketEditable()))
			{
				cartmodel = cartPageFacade.calculateCart(cartmodel);
			}
			final CartData cartData = cartFacade.getMiniCart();
			model.addAttribute("totalPrice", cartData.getTotalPrice());
			model.addAttribute("subTotal", cartData.getSubTotal());
			if (cartData.getDeliveryCost() != null)
			{
				final PriceData withoutDelivery = cartData.getDeliveryCost();
				withoutDelivery.setValue(cartData.getTotalPrice().getValue().subtract(cartData.getDeliveryCost().getValue()));
				model.addAttribute("totalNoDelivery", withoutDelivery);
			}
			else
			{
				model.addAttribute("totalNoDelivery", cartData.getTotalPrice());
			}
			model.addAttribute("totalItems", cartData.getTotalUnitCount());
			model.addAttribute("totalDisplay", totalDisplay);
		}
		return ControllerConstants.Views.Fragments.Cart.MiniCartPanel;
	}

	@RequestMapping(value = "/cart/rollover/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String rolloverMiniCartPopup(@PathVariable final String componentUid, final Model model)
			throws CMSItemNotFoundException
	{
		if (cartService.hasSessionCart())
		{
			CartModel cartmodel = cartService.getSessionCart();
			if (!Boolean.FALSE.equals(cartmodel.getBasketEditable()))
			{
				cartmodel = cartPageFacade.calculateCart(cartmodel);
			}
			final CartData cartData = cartFacade.getSessionCart();
			model.addAttribute("cartData", cartData);

			final MiniCartComponentModel component = (MiniCartComponentModel) cmsComponentService
					.getSimpleCMSComponent(componentUid);

			final List entries = cartData.getEntries();
			if (entries != null)
			{
				Collections.reverse(entries);
				model.addAttribute("entries", entries);
				model.addAttribute("component", component);

				model.addAttribute("numberItemsInCart", Integer.valueOf(entries.size()));
				if (entries.size() < component.getShownProductCount())
				{
					model.addAttribute("numberShowing", Integer.valueOf(entries.size()));
				}
				else
				{
					model.addAttribute("numberShowing", Integer.valueOf(component.getShownProductCount()));
				}
			}

			model.addAttribute("lightboxBannerComponent", component.getLightboxBannerComponent());

			final CartData cartSessionData = cartFacade.getSessionCartWithEntryOrdering(true);
			boolean hasPickUpCartEntries = false;
			if (cartSessionData.getEntries() != null && !cartSessionData.getEntries().isEmpty())
			{
				for (final OrderEntryData entry : cartSessionData.getEntries())
				{
					if (!hasPickUpCartEntries && entry.getDeliveryPointOfService() != null)
					{
						hasPickUpCartEntries = true;
					}
					final UpdateQuantityForm uqf = new UpdateQuantityForm();
					uqf.setQuantity(entry.getQuantity());
					model.addAttribute("updateQuantityForm" + entry.getEntryNumber(), uqf);
				}
			}
		}
		return ControllerConstants.Views.Fragments.Cart.CartPopup;
	}

	@SuppressWarnings("boxing")
	@RequestMapping(value = "/cart/remove", method = RequestMethod.POST)
	public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model,
			@Valid final UpdateQuantityForm form, final BindingResult bindingResult, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				if (error.getCode().equals("typeMismatch"))
				{
					GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
				}
				else
				{
					GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
				}
			}
		}
		else if (cartFacade.getSessionCart().getEntries() != null
				&& !Boolean.FALSE.equals(cartService.getSessionCart().getBasketEditable()))
		{
			try
			{
				final CartModificationData cartModification = cartFacade.updateCartEntry(entryNumber, form.getQuantity().longValue());
				if (cartModification.getQuantity() == form.getQuantity().longValue())
				{
					// Success

					if (cartModification.getQuantity() == 0)
					{
						// Success in removing entry
						//GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "basket.page.message.remove");
					}
					else
					{
						// Success in update quantity
						//form.setQuantity(form.getQuantity() + 1);
						//cartFacade.updateCartEntry(entryNumber, form.getQuantity().longValue());
						model.addAttribute("quantity", form.getQuantity());
						//GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "basket.page.message.update");
					}
				}
				else if (cartModification.getQuantity() > 0)
				{
					// Less than successful
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"basket.page.message.update.reducedNumberOfItemsAdded.lowStock", new Object[]
							{ cartModification.getEntry().getProduct().getName(), cartModification.getQuantity(), form.getQuantity(),
									request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
				}
				else
				{
					// No more stock available
					GlobalMessages.addFlashMessage(
							redirectModel,
							GlobalMessages.ERROR_MESSAGES_HOLDER,
							"basket.page.message.update.reducedNumberOfItemsAdded.noStock",
							new Object[]
							{ cartModification.getEntry().getProduct().getName(),
									request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
				}
			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
			}
		}
		return REDIRECT_PREFIX + request.getHeader("Referer");
	}
}
