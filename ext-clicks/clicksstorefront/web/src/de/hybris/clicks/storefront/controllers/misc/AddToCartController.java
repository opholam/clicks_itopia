/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.misc;

import de.hybris.clicks.facade.cartPage.CartPageFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.clicksstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.clicksstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.promotion.CommercePromotionFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
@Scope("tenant")
public class AddToCartController extends AbstractPageController
{
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";

	protected static final Logger LOG = Logger.getLogger(AddToCartController.class);

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource
	private CartService cartService;

	@Resource(name = "cartPageFacade")
	private CartPageFacade cartPageFacade;

	@Resource
	private PromotionsService promotionsService;

	@Resource
	private ModelService modelService;

	@Resource
	private CommercePromotionFacade commercePromotionFacade;

	@RequestMapping(value = "/cart/add", method =
	{ RequestMethod.GET, RequestMethod.POST }, produces = "application/json")
	public String addToCart(@RequestParam("productCodePost") final String code, final Model model,
			@Valid final AddToCartForm addToCartForm, final BindingResult bindingErrors, final HttpServletRequest request)
	{
		final String returnAction = ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
		try
		{
			final String referer = request.getHeader("referer");
			/*
			 * url for continue shopping, navigates to the previous page
			 */
			if (null != referer)
			{
				model.addAttribute("shoppingUrl", referer);
			}
			final CartModel cart = cartService.getSessionCart();
			if (Boolean.FALSE.equals(cart.getBasketEditable()))
			{
				model.addAttribute(ERROR_MSG_TYPE, "addtocart.paymentProgress");
				model.addAttribute("quantity", Long.valueOf(0L));
				return returnAction;
			}
			/*
			 * re-calculating promotions in cart
			 */
			try
			{
				final Collection<PromotionGroupModel> promotionGroup = new ArrayList<PromotionGroupModel>();
				promotionGroup.add(getCmsSiteService().getCurrentSite().getDefaultPromotionGroup());
				promotionsService.updatePromotions(promotionGroup, cart);
				modelService.refresh(cart);
			}
			catch (final Exception e)
			{
				//
			}

			if (bindingErrors.hasErrors())
			{
				return getViewWithBindingErrorMessages(model, bindingErrors);
			}
			final long qty = addToCartForm.getQty();


			if (qty <= 0)
			{
				model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
				model.addAttribute("quantity", Long.valueOf(0L));
			}
			else
			{
				try
				{
					final CartModificationData cartModification = cartFacade.addToCart(code, qty);
					//final CartData cartData = cartFacade.getSessionCart();
					model.addAttribute("quantity", Long.valueOf(cartModification.getQuantityAdded()));
					model.addAttribute("entry", cartModification.getEntry());

					if (cartModification.getQuantityAdded() == 0L)
					{
						if (cartModification.getStatusCode().equalsIgnoreCase("maxOrderQuantityExceeded"))
						{
							model.addAttribute(ERROR_MSG_TYPE,
									"basket.information.quantity.noItems.added." + cartModification.getStatusCode());
						}
						else
						{
							model.addAttribute(ERROR_MSG_TYPE,
									"basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
						}
					}
					else if (cartModification.getQuantityAdded() < qty)
					{
						model.addAttribute(ERROR_MSG_TYPE,
								"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
					}
					final CartData cartSessionData = cartFacade.getSessionCartWithEntryOrdering(true);
					boolean hasPickUpCartEntries = false;
					if (cartSessionData.getEntries() != null && !cartSessionData.getEntries().isEmpty())
					{
						for (final OrderEntryData entry : cartSessionData.getEntries())
						{
							if (!hasPickUpCartEntries && entry.getDeliveryPointOfService() != null)
							{
								hasPickUpCartEntries = true;
							}
							final UpdateQuantityForm uqf = new UpdateQuantityForm();
							uqf.setQuantity(entry.getQuantity());
							model.addAttribute("updateQuantityForm" + entry.getEntryNumber(), uqf);
						}
					}
					else
					{
						final UpdateQuantityForm uqf = new UpdateQuantityForm();
						uqf.setQuantity(cartModification.getQuantityAdded());
						model.addAttribute("updateQuantityForm", uqf);
					}
					// fetching the category url
					for (final OrderEntryData entrydata : cartFacade.getSessionCart().getEntries())
					{
						for (final CategoryData category : entrydata.getProduct().getCategories())
						{
							if (category.getUrl().contains("OH"))
							{
								model.addAttribute("url", category.getUrl());
							}
						}
					}
					model.addAttribute("cartData", cartFacade.getSessionCart());
				}
				catch (final CommerceCartModificationException ex)
				{
					model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
					model.addAttribute("quantity", Long.valueOf(0L));
				}
			}
			model.addAttribute("addToCartForm", addToCartForm);
			model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));
		}
		catch (final Exception e)
		{
			model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
			model.addAttribute("quantity", Long.valueOf(0L));
		}
		return returnAction;
	}


	protected String getViewWithBindingErrorMessages(final Model model, final BindingResult bindingErrors)
	{
		for (final ObjectError error : bindingErrors.getAllErrors())
		{
			if (isTypeMismatchError(error))
			{
				model.addAttribute(ERROR_MSG_TYPE, QUANTITY_INVALID_BINDING_MESSAGE_KEY);
			}
			else
			{
				model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
			}
		}
		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	protected boolean isTypeMismatchError(final ObjectError error)
	{
		return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
	}

}
