/**
 *
 */
package de.hybris.clicks.storefront.forms;

import de.hybris.platform.commercefacades.storelocator.data.OpeningScheduleData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;

import java.util.List;


/**
 * @author Shruti Jhamb
 *
 */
public class PickUpStoreForm
{
	private List<PointOfServiceData> pointOfServiceData;
	public String q;
	private Boolean isDeliverable;
	private double longitude;
	private double latitude;
	private String addressId;
	private String distance;
	private String email;
	private String phone;
	private String alternatePhone;
	private String line1;
	private String line2;
	private String town;
	private String regionIso;
	private String postcode;
	private String province;
	private final String countryIso = "ZA";
	private String countryName;
	private String suburb;
	private String shopNumber;
	private String selectedStore;
	private String selectedDeliveryMode;
	private int count;
	private Boolean preferredContactMethod;
	private String posName;
	private String storeDisplayName;
	private boolean isStoreAddress;
	private String department;
	private String apartment;
	private List<OpeningScheduleData> openingHoursList;




	/**
	 * @return the isDeliverable
	 */
	public Boolean getIsDeliverable()
	{
		return isDeliverable;
	}


	/**
	 * @param isDeliverable
	 *           the isDeliverable to set
	 */
	public void setIsDeliverable(final Boolean isDeliverable)
	{
		this.isDeliverable = isDeliverable;
	}


	/**
	 * @return the openingHoursList
	 */
	public List<OpeningScheduleData> getOpeningHoursList()
	{
		return openingHoursList;
	}


	/**
	 * @param openingHoursList
	 *           the openingHoursList to set
	 */
	public void setOpeningHoursList(final List<OpeningScheduleData> openingHoursList)
	{
		this.openingHoursList = openingHoursList;
	}


	/**
	 * @return the province
	 */
	public String getProvince()
	{
		return province;
	}


	/**
	 * @param province
	 *           the province to set
	 */
	public void setProvince(final String province)
	{
		this.province = province;
	}


	/**
	 * @return the distance
	 */
	public String getDistance()
	{
		return distance;
	}


	/**
	 * @param distance
	 *           the distance to set
	 */
	public void setDistance(final String distance)
	{
		this.distance = distance;
	}


	/**
	 * @return the suburb
	 */
	public String getSuburb()
	{
		return suburb;
	}


	/**
	 * @param suburb
	 *           the suburb to set
	 */
	public void setSuburb(final String suburb)
	{
		this.suburb = suburb;
	}


	/**
	 * @return the department
	 */
	public String getDepartment()
	{
		return department;
	}


	/**
	 * @param department
	 *           the department to set
	 */
	public void setDepartment(final String department)
	{
		this.department = department;
	}


	/**
	 * @return the apartment
	 */
	public String getApartment()
	{
		return apartment;
	}


	/**
	 * @param apartment
	 *           the apartment to set
	 */
	public void setApartment(final String apartment)
	{
		this.apartment = apartment;
	}


	/**
	 * @return the isStoreAddress
	 */
	public boolean isStoreAddress()
	{
		return isStoreAddress;
	}


	/**
	 * @param isStoreAddress
	 *           the isStoreAddress to set
	 */
	public void setStoreAddress(final boolean isStoreAddress)
	{
		this.isStoreAddress = isStoreAddress;
	}


	/**
	 * @return the storeDisplayName
	 */
	public String getStoreDisplayName()
	{
		return storeDisplayName;
	}


	/**
	 * @param storeDisplayName
	 *           the storeDisplayName to set
	 */
	public void setStoreDisplayName(final String storeDisplayName)
	{
		this.storeDisplayName = storeDisplayName;
	}


	/**
	 * @return the pointOfServiceData
	 */
	public List<PointOfServiceData> getPointOfServiceData()
	{
		return pointOfServiceData;
	}


	/**
	 * @param pointOfServiceData
	 *           the pointOfServiceData to set
	 */
	public void setPointOfServiceData(final List<PointOfServiceData> pointOfServiceData)
	{
		this.pointOfServiceData = pointOfServiceData;
	}


	/**
	 * @return the posName
	 */
	public String getPosName()
	{
		return posName;
	}


	/**
	 * @param posName
	 *           the posName to set
	 */
	public void setPosName(final String posName)
	{
		this.posName = posName;
	}


	/**
	 * @return the preferredContactMethod
	 */
	public Boolean getPreferredContactMethod()
	{
		return preferredContactMethod;
	}


	/**
	 * @param preferredContactMethod
	 *           the preferredContactMethod to set
	 */
	public void setPreferredContactMethod(final Boolean preferredContactMethod)
	{
		this.preferredContactMethod = preferredContactMethod;
	}


	/**
	 * @return the count
	 */
	public int getCount()
	{
		return count;
	}


	/**
	 * @param count
	 *           the count to set
	 */
	public void setCount(final int count)
	{
		this.count = count;
	}


	/**
	 * @return the selectedStore
	 */
	public String getSelectedStore()
	{
		return selectedStore;
	}


	/**
	 * @param selectedStore
	 *           the selectedStore to set
	 */
	public void setSelectedStore(final String selectedStore)
	{
		this.selectedStore = selectedStore;
	}


	/**
	 * @return the selectedDeliveryMode
	 */
	public String getSelectedDeliveryMode()
	{
		return selectedDeliveryMode;
	}


	/**
	 * @param selectedDeliveryMode
	 *           the selectedDeliveryMode to set
	 */
	public void setSelectedDeliveryMode(final String selectedDeliveryMode)
	{
		this.selectedDeliveryMode = selectedDeliveryMode;
	}


	/**
	 * @return the longitude
	 */
	public double getLongitude()
	{
		return longitude;
	}


	/**
	 * @param longitude
	 *           the longitude to set
	 */
	public void setLongitude(final double longitude)
	{
		this.longitude = longitude;
	}


	/**
	 * @return the latitude
	 */
	public double getLatitude()
	{
		return latitude;
	}


	/**
	 * @param latitude
	 *           the latitude to set
	 */
	public void setLatitude(final double latitude)
	{
		this.latitude = latitude;
	}


	/**
	 * @return the q
	 */
	public String getQ()
	{
		return q;
	}


	/**
	 * @param q
	 *           the q to set
	 */
	public void setQ(final String q)
	{
		this.q = q;
	}










	/**
	 * @return the addressId
	 */
	public String getAddressId()
	{
		return addressId;
	}


	/**
	 * @param addressId
	 *           the addressId to set
	 */
	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}


	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}


	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}


	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}


	/**
	 * @param phone
	 *           the phone to set
	 */
	public void setPhone(final String phone)
	{
		this.phone = phone;
	}


	/**
	 * @return the alternatePhone
	 */
	public String getAlternatePhone()
	{
		return alternatePhone;
	}


	/**
	 * @param alternatePhone
	 *           the alternatePhone to set
	 */
	public void setAlternatePhone(final String alternatePhone)
	{
		this.alternatePhone = alternatePhone;
	}


	/**
	 * @return the line1
	 */
	public String getLine1()
	{
		return line1;
	}


	/**
	 * @param line1
	 *           the line1 to set
	 */
	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}


	/**
	 * @return the line2
	 */
	public String getLine2()
	{
		return line2;
	}


	/**
	 * @param line2
	 *           the line2 to set
	 */
	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}


	/**
	 * @return the town
	 */
	public String getTown()
	{
		return town;
	}


	/**
	 * @param town
	 *           the town to set
	 */
	public void setTown(final String town)
	{
		this.town = town;
	}


	/**
	 * @return the regionIso
	 */
	public String getRegionIso()
	{
		return regionIso;
	}


	/**
	 * @param regionIso
	 *           the regionIso to set
	 */
	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}


	/**
	 * @return the postcode
	 */
	public String getPostcode()
	{
		return postcode;
	}


	/**
	 * @param postcode
	 *           the postcode to set
	 */
	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}


	/**
	 * @return the countryName
	 */
	public String getCountryName()
	{
		return countryName;
	}


	/**
	 * @param countryName
	 *           the countryName to set
	 */
	public void setCountryName(final String countryName)
	{
		this.countryName = countryName;
	}


	/**
	 * @return the shopNumber
	 */
	public String getShopNumber()
	{
		return shopNumber;
	}


	/**
	 * @param shopNumber
	 *           the shopNumber to set
	 */
	public void setShopNumber(final String shopNumber)
	{
		this.shopNumber = shopNumber;
	}


	/**
	 * @return the countryIso
	 */
	public String getCountryIso()
	{
		return countryIso;
	}




}
