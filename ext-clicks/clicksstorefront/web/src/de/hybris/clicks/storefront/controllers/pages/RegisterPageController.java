/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.facades.customer.ClicksCustomerFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.controllers.integration.CustomerIntegrationProcessor;
import de.hybris.clicks.storefront.controllers.validators.EnrollClubcardValidator;
import de.hybris.clicks.storefront.controllers.validators.RegistrationClubcardValidator;
import de.hybris.clicks.storefront.controllers.validators.RegistrationNonClubcardValidator;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractRegisterPageController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.ClicksRegisterForm;
import de.hybris.platform.clicksstorefrontcommons.forms.RegisterClubCardForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Register Controller. Handles login and register for the account flow.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/register")
public class RegisterPageController extends AbstractRegisterPageController
{
	private HttpSessionRequestCache httpSessionRequestCache;
	private static final Logger LOG = Logger.getLogger(RegisterPageController.class);
	private static final String REDIRECT_NEW_ACCOUNT = REDIRECT_PREFIX + "/my-account/my-rewards-activity";
	protected static final String CLUB_CARD_LINKED_TO_ACCOUNT_EMAIL_PROCESS = "clubcardLinkedToAccountEmailProcess";
	protected static final String NEW_ACCOUNT_NEW_CLUB_CARD_EMAIL_PROCESS = "newAccountNewCCEmailProcess";
	protected static final String NEW_ACCOUNT_EXISTING_CLUB_CARD_EMAIL_PROCESS = "newAccountExistingCCEmailProcess";
	protected static final String NON_CLUB_CARD_REGISTRATION = "customerRegistrationEmailProcess";
	private static final String EDIT_PERSONAL_DETAILS_CMS_PAGE = REDIRECT_PREFIX + "/my-account/edit-personal-details";


	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource
	private CustomerIntegrationProcessor customerIntegrationProcessor;

	@Resource(name = "registrationClubcardValidator")
	RegistrationClubcardValidator registrationClubcardValidator;

	@Resource(name = "accountPageController")
	AccountPageController accountPageController;

	@Resource(name = "registrationNonClubcardValidator")
	RegistrationNonClubcardValidator registrationNonClubcardValidator;

	@Resource(name = "enrollClubcardValidator")
	EnrollClubcardValidator enrollClubcardValidator;

	@Resource(name = "loginPageController")
	LoginPageController loginPageController;

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "clicksCustomerFacade")
	ClicksCustomerFacade clicksCustomerFacade;
	private static final String HOMEPAGE_ID = "homepage";
	private static final String REDIRECT_CHECKOUT = "redirect:/checkout/multi/delivery-address/add";

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractRegisterPageController#getCmsPage()
	 */
	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("register");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractRegisterPageController#getSuccessRedirect
	 * (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return Config.getParameter("register.controller.myAccount");
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Account.AccountRegisterPage;
	}

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	/**
	 * Do register.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String doRegister(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		return getDefaultRegistrationPage(model);
	}

	/**
	 * Do register.
	 *
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/newcustomer", method = RequestMethod.POST)
	public String doRegister(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		return processRegisterUserRequest(null, form, bindingResult, model, request, response, redirectModel);
	}


	/**
	 * Gets the customer layout.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the customer layout
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	// -------- Enroll for clubcard-------------
	@RequestMapping(value = "/registerCustomer", method = RequestMethod.GET)
	public String getCustomerLayout(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		createAccountRequest(model, new ClicksRegisterForm());
		return Config.getParameter("register.controller.customerPageLayout");
	}

	/**
	 * Creates the account request.
	 *
	 * @param model
	 *           the model
	 * @param form
	 *           the form
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	void createAccountRequest(final Model model, final ClicksRegisterForm form) throws CMSItemNotFoundException
	{
		LOG.info("-----------create Account get method-------------");
		storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		model.addAttribute(form);
		//model.addAttribute("action", "/register/createAccount");
		model.addAttribute("action", Config.getParameter("register.controller.enroll"));
		model.addAttribute("RegisterAction", Config.getParameter("register.controller.registerNonCC"));

	}

	/**
	 * Enroll request.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param form
	 *           the form
	 * @param isJoinCC
	 *           the is join cc
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/getEnrollForm", method = RequestMethod.GET)
	String enrollRequest(final Model model, final HttpServletRequest request, final ClicksRegisterForm form, final boolean isJoinCC)
			throws CMSItemNotFoundException
	{
		LOG.info("-----------enroll get method-------------");
		if (isJoinCC)
		{
			if (null != form.getMemberIDPlasticCard() && StringUtils.isNotEmpty(form.getMemberIDPlasticCard()))
			{
				form.setJoinClubCard(false);
				form.setIsClubCard(1);
				form.setMemberID(form.getMemberIDPlasticCard());
				form.setMemberIDPlasticCard(form.getMemberIDPlasticCard());
			}
			else
			{
				form.setJoinClubCard(true);
				form.setIsClubCard(0);
			}
			model.addAttribute(form);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		model.addAttribute("action", Config.getParameter("register.controller.enroll"));
		return Config.getParameter("register.controller.customerPageLayout");
	}


	/**
	 * Join club card request form.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@SuppressWarnings("boxing")
	@RequestMapping(value = "/joincc", method = RequestMethod.GET)
	@RequireHardLogIn
	String joinClubCardRequestForm(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		LOG.info("-----Join Club Card Method------");
		final CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
		final ClicksRegisterForm enrollForm = new ClicksRegisterForm();
		if (null != currentUser)
		{
			populateEnrollForm(currentUser, enrollForm);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		model.addAttribute(enrollForm);
		model.addAttribute("action", Config.getParameter("register.controller.joincc"));
		//		model.addAttribute("joincc", "true");
		return Config.getParameter("register.controller.customerPageLayout");
	}

	/**
	 * Populate enroll form.
	 *
	 * @param currentUser
	 *           the current user
	 * @param enrollForm
	 *           the enroll form
	 */
	@SuppressWarnings(
	{ "deprecation", "boxing" })
	private void populateEnrollForm(final CustomerModel currentUser, final ClicksRegisterForm enrollForm)
	{
		enrollForm.setFirstName(getTitleCase(currentUser.getFirstName()));
		enrollForm.setLastName(getTitleCase(currentUser.getLastName()));
		enrollForm.setPreferedName(getTitleCase(currentUser.getName()));
		enrollForm.seteMailAddress(getTitleCase(currentUser.getUid()));
		enrollForm.setSAResident(true);
		if (null != currentUser.getSAResident())
		{
			enrollForm.setSAResident(true);
		}
		enrollForm.setIDNumber(currentUser.getRSA_ID());
		if (null != currentUser.getDateofbirth())
		{
			final Format outFormat = new SimpleDateFormat(Config.getParameter("register.controller.outDateFormat"));
			final SimpleDateFormat inputFormat = new SimpleDateFormat(Config.getParameter("register.controller.inputDateFormat"));
			String dob = "";
			try
			{
				dob = outFormat.format(inputFormat.parse(currentUser.getDateofbirth().toString()));
				enrollForm.setSA_DOB_day(dob.split("/")[0]);
				enrollForm.setSA_DOB_month(dob.split("/")[1]);
				enrollForm.setSA_DOB_year(dob.split("/")[2]);
			}
			catch (final ParseException e)
			{
				LOG.error("Error in RegisterPageController class at populateEnrollForm() method : " + e.getMessage());
			}
		}
		int gender = 1;
		if (null != currentUser.getGender() && "FEMALE".equalsIgnoreCase(currentUser.getGender().getCode()))
		{
			gender = 2;
		}
		enrollForm.setGender(gender);
		enrollForm.setAccinfoEmail(true);
		enrollForm.setMarketingEmail(true);
	}

	/**
	 * Join club card submit form.
	 *
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/joincc", method = RequestMethod.POST)
	public String joinClubCardSubmitForm(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		LOG.info("-----------Join Club Card post method-------------");
		enrollClubcardValidator.validateJoinCC(form, bindingResult);
		boolean escapeErrors = false;
		if (bindingResult.hasErrors())
		{
			final List<String> escapeErrorFieldsForJoinCC = new ArrayList<String>(5);
			escapeErrorFieldsForJoinCC.add(Config.getParameter("register.controller.eMailAddress"));
			escapeErrorFieldsForJoinCC.add(Config.getParameter("register.controller.password"));
			escapeErrorFieldsForJoinCC.add(Config.getParameter("register.controller.confirmPassword"));
			//			escapeErrorFieldsForJoinCC.add("province");

			for (final FieldError fieldError : bindingResult.getFieldErrors())
			{
				if (escapeErrorFieldsForJoinCC.contains(fieldError.getField()))
				{
					escapeErrors = true;
				}
			}
			GlobalMessages.addErrorMessage(model, "form.global.error");
		}
		if (bindingResult.hasErrors() && !escapeErrors)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.joincc"));
			model.addAttribute("joincc", "true");
			return Config.getParameter("register.controller.customerPageLayout");
		}

		LOG.info("Escape errors : " + escapeErrors);

		final String referer = null;
		final CustomerModel currCustomer = (CustomerModel) userService.getCurrentUser();
		form.seteMailAddress(currCustomer.getEmailID());
		loginPageController.doJoinCCToCustomer(referer, form, model, request, response, redirectModel, currCustomer, escapeErrors);
		form.setCustomerId(currCustomer.getCustomerID());
		LOG.info("Calling ESB service" + form.getCustomerId());
		final ResultData result = customerIntegrationProcessor.processsEnrollRequest(form, bindingResult, model, request, response);

		if (result.getCode().intValue() == 0)
		{
			clicksCustomerFacade.processEmail(currCustomer, CLUB_CARD_LINKED_TO_ACCOUNT_EMAIL_PROCESS);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "form.register.success");
		}
		else
		{
			if (result.getCode().intValue() == 3 || result.getCode().intValue() == 42)
			{
				GlobalMessages.addErrorMessage(model, "response.exception.message.enroll.duplicate");
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"response.exception.message.register");
				GlobalMessages.addErrorMessage(model, "response.exception.message.register");
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.joincc"));
			model.addAttribute("joincc", "true");
			model.addAttribute(form);
			return Config.getParameter("register.controller.customerPageLayout");

		}
		if (form.isCheckoutForm())
		{
			return REDIRECT_CHECKOUT;
		}
		return REDIRECT_PREFIX + "/";
	}

	/**
	 * Retrieve request.
	 *
	 * @param model
	 *           the model
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	void retrieveRequest(final Model model, final ClicksRegisterForm form, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		LOG.info("-----------retrived get method-------------" + form.getCustomerId());
		registrationNonClubcardValidator.validate(form, bindingResult);
		final ClicksRegisterForm registerClubCardForm = customerIntegrationProcessor.processsRetrievalRequest(form, bindingResult,
				model);
		if (bindingResult.hasErrors() || !registerClubCardForm.getResult().booleanValue())
		{
			GlobalMessages.addErrorMessage(model, "response.exception.message.detailsRequest");
			createAccountRequest(model, form);
		}
		else
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute(registerClubCardForm);
			model.addAttribute("action", Config.getParameter("register.controller.registerCC"));
		}
	}

	/**
	 * Register cc request.
	 *
	 * @param model
	 *           the model
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	void registerCCRequest(final Model model) throws CMSItemNotFoundException
	{
		LOG.info("-----------2 RegisterClubCardForm get method-------------");
		storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		model.addAttribute(new RegisterClubCardForm());
		model.addAttribute("action", Config.getParameter("register.controller.registerCC"));
	}

	/**
	 * Creates the account.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/createAccount", method = RequestMethod.GET)
	public String createAccount(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new ClicksRegisterForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
		//model.addAttribute("action", "/register/createAccount");
		model.addAttribute("action", Config.getParameter("register.controller.enroll"));
		model.addAttribute("RegisterAction", Config.getParameter("register.controller.registerNonCC"));
		return Config.getParameter("register.controller.customerPageLayout");
	}


	/**
	 * Enroll customer.
	 *
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/enroll", method = RequestMethod.POST)
	public String enrollCustomer(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		LOG.info("-----------enroll post method-------------" + form.getCustomerId());
		if (form.isCheckoutForm())
		{
			model.addAttribute("checkoutForm", Boolean.TRUE);
		}
		LOG.info("-----------enroll post method-------------" + form.getCustomerId());
		if (form.getIsClubCard() == 1 && form.getMemberIDPlasticCard() == null)
		{
			//retrieveRequest(model, form, bindingResult);
			final String view = registerClubCardCustomer(form, bindingResult, model, request, response, redirectModel);
			return view;
		}
		if (form.getIsClubCard() == 2)
		{
			LOG.info("doing online only reg");
			final String view = registerNonClubCardCustomer(form, bindingResult, model, request, response, redirectModel);
			return view;
		}
		enrollClubcardValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			LOG.info("in enrollCustomer has error" + bindingResult.getAllErrors());
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.enroll"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		final String referer = null;
		LOG.info("after validate");
		loginPageController.doRegisterForEnroll(referer, form, bindingResult, model, request, response, redirectModel);
		LOG.info("after doRegisterForEnroll");
		if (bindingResult.hasErrors())
		{
			LOG.info("after doRegisterForEnroll errors" + bindingResult.hasErrors());
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.enroll"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		LOG.info("b4 processsEnrollRequest");
		final ResultData result = customerIntegrationProcessor.processsEnrollRequest(form, bindingResult, model, request, response);
		LOG.info("response message for enroll ---> " + result.getCode() + "----" + result.getMessage());
		if (result.getCode().intValue() == 0)
		{
			//join cc user
			getAutoLoginStrategy().login(form.geteMailAddress().toLowerCase(), form.getPassword(), request, response);
			final CustomerModel currCustomer = (CustomerModel) userService.getCurrentUser();
			clicksCustomerFacade.processEmail(currCustomer, NEW_ACCOUNT_NEW_CLUB_CARD_EMAIL_PROCESS);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "form.register.success");
		}
		else
		{
			removeUser(form.geteMailAddress().toLowerCase());
			if (result.getCode().intValue() == 3 || result.getCode().intValue() == 42)
			{
				GlobalMessages.addErrorMessage(model, "response.exception.message.enroll.duplicate");
			}
			else
			{
				GlobalMessages.addErrorMessage(model, result.getCode().toString());
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.enroll"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		if (form.isCheckoutForm())
		{
			sessionService.setAttribute("gaTrakingCR", true);
			return REDIRECT_CHECKOUT;

		}
		sessionService.setAttribute("gaTraking", true);
		return REDIRECT_NEW_ACCOUNT;
	}

	/**
	 * Register club card customer.
	 *
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	// -------- Register Clubcard customer-------------
	@RequestMapping(value = "/registerCC", method = RequestMethod.POST)
	public String registerClubCardCustomer(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		LOG.info("-----------RegisterCC post method-------------");
		// start - dummy data -- for hybris user creation- will be replaced after service call
		form.setFirstName(Config.getParameter("register.controller.test"));
		form.setLastName(Config.getParameter("register.controller.test"));
		form.setPreferedName(Config.getParameter("register.controller.test"));
		// end - dummy data
		registrationClubcardValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.registerCC"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		final String referer = null;
		loginPageController.doRegisterCC(referer, form, bindingResult, model, request, response, redirectModel);
		if (bindingResult.hasErrors())
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.registerCC"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		final ResultData result = customerIntegrationProcessor.processsRegisterCCRequest(form, bindingResult, model, request,
				response);
		LOG.info("response message for register CC ---> " + result.getCode() + "----" + result.getMessage());

		if (result.getCode().intValue() == 0)
		{
			//already having cc
			getAutoLoginStrategy().login(form.geteMailAddress().toLowerCase(), form.getPassword(), request, response);
			final CustomerModel currCustomer = (CustomerModel) userService.getCurrentUser();
			clicksCustomerFacade.processEmail(currCustomer, NEW_ACCOUNT_EXISTING_CLUB_CARD_EMAIL_PROCESS);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "form.register.success.cc");
		}
		else if (result.getCode().intValue() == 54)
		{
			removeUser(form.geteMailAddress().toLowerCase());
			GlobalMessages.addErrorMessage(model, "response.exception.message.register.notEnrolled");
			final ClicksRegisterForm clicksRegisterForm = populateForPlasticCard(form);
			final String enrollView = enrollRequest(model, request, clicksRegisterForm, true);
			return enrollView;
		}
		else
		{
			removeUser(form.geteMailAddress().toLowerCase());
			if (result.getCode().intValue() == 51)
			{
				GlobalMessages.addErrorMessage(model, "response.exception.message.register.linkedCard");
				bindingResult.rejectValue("SA_DOB_day", "register.CC.DOB.empty");
				bindingResult.rejectValue("SA_DOB_month", "register.CC.DOB.empty");
				bindingResult.rejectValue("SA_DOB_year", "register.CC.DOB.empty");
			}
			else
			{
				GlobalMessages.addErrorMessage(model, "response.exception.message.register");
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.registerCC"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		if (form.isCheckoutForm())
		{
			return REDIRECT_CHECKOUT;
		}
		return EDIT_PERSONAL_DETAILS_CMS_PAGE;
	}

	/**
	 * Populate for plastic card.
	 *
	 * @param form
	 *           the form
	 * @return the clicks register form
	 */
	private ClicksRegisterForm populateForPlasticCard(final ClicksRegisterForm form)
	{
		final ClicksRegisterForm clicksRegisterForm = new ClicksRegisterForm();
		if (null != form.geteMailAddress())
		{
			clicksRegisterForm.seteMailAddress(form.geteMailAddress());
		}
		if (null != form.getSA_DOB_day())
		{
			clicksRegisterForm.setSA_DOB_day(form.getSA_DOB_day());
		}
		if (null != form.getSA_DOB_month())
		{
			clicksRegisterForm.setSA_DOB_month(form.getSA_DOB_month());
		}
		if (null != form.getSA_DOB_year())
		{
			clicksRegisterForm.setSA_DOB_year(form.getSA_DOB_year());
		}
		if (null != form.getIDNumber())
		{
			clicksRegisterForm.setIDNumber(form.getIDNumber());
		}
		if (null != form.getMemberID() && StringUtils.isNotEmpty(form.getMemberID()))
		{
			clicksRegisterForm.setMemberIDPlasticCard(form.getMemberID());
		}
		clicksRegisterForm.setIsSAResident(form.getIsSAResident());
		return clicksRegisterForm;
	}

	/**
	 * Register non club card customer.
	 *
	 * @param form
	 *           the form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param redirectModel
	 *           the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	// -------- Register Non Clubcard customer -------------
	@RequestMapping(value = "/registerNonCC", method = RequestMethod.POST)
	public String registerNonClubCardCustomer(final ClicksRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		LOG.info("-----------Register non CC post method-------------" + form.getCustomerId());
		registrationNonClubcardValidator.validate(form, bindingResult);

		if (bindingResult.hasErrors())
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.enroll"));
			model.addAttribute("RegisterAction", Config.getParameter("register.controller.registerNonCC"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		final String referer = null;
		loginPageController.doRegisterNonCC(referer, form, bindingResult, model, request, response, redirectModel);
		if (bindingResult.hasErrors())
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.enroll"));
			model.addAttribute("RegisterAction", Config.getParameter("register.controller.registerNonCC"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		final ResultData result = customerIntegrationProcessor.processsRegisterNonCCRequest(form, bindingResult, model, request,
				response);
		LOG.info("response messgae in NON-CC controller ----- >" + result.getCode() + "----" + result.getMessage());
		if (result.getCode().intValue() == 0)
		{
			getAutoLoginStrategy().login(form.geteMailAddress().toLowerCase(), form.getPassword(), request, response);
			//non cc
			final CustomerModel currCustomer = (CustomerModel) userService.getCurrentUser();
			clicksCustomerFacade.processEmail(currCustomer, NON_CLUB_CARD_REGISTRATION);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "form.register.success");
		}
		else if (result.getCode().intValue() == 54)
		{
			removeUser(form.geteMailAddress().toLowerCase());
			GlobalMessages.addErrorMessage(model, "response.exception.message.register.notEnrolled");
			final boolean isJoinCC = true;
			final String enrollView = enrollRequest(model, request, form, isJoinCC);
			return enrollView;
		}
		else
		{
			removeUser(form.geteMailAddress().toLowerCase());
			if (result.getCode().intValue() == 51)
			{
				GlobalMessages.addErrorMessage(model, "response.exception.message.register.linkedCard");
				bindingResult.rejectValue("SA_DOB_day", "register.CC.DOB.empty");
				bindingResult.rejectValue("SA_DOB_month", "register.CC.DOB.empty");
				bindingResult.rejectValue("SA_DOB_year", "register.CC.DOB.empty");
			}
			else
			{
				GlobalMessages.addErrorMessage(model, "response.exception.message.register");
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(Config.getParameter("register.controller.clicksCustomer")));
			model.addAttribute("action", Config.getParameter("register.controller.enroll"));
			model.addAttribute("RegisterAction", Config.getParameter("register.controller.registerNonCC"));
			return Config.getParameter("register.controller.customerPageLayout");
		}
		if (form.isCheckoutForm())
		{
			return REDIRECT_CHECKOUT;
		}
		return REDIRECT_PREFIX + "/";
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}
}
