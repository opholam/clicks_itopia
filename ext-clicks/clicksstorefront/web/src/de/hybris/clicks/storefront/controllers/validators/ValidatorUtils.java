/**
 * 
 */
package de.hybris.clicks.storefront.controllers.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;


/**
 * @author jayant.premchand
 * 
 */
public class ValidatorUtils
{
	public static boolean validateRegex(final String regex, final String text)
	{
		if (StringUtils.isEmpty(text))
		{
			return false;
		}

		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}
}
