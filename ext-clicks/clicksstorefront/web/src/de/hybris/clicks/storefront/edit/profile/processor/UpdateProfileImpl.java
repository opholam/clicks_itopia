/**
 *
 */
package de.hybris.clicks.storefront.edit.profile.processor;

import de.hybris.clicks.facade.myaccount.editProfile.UpdateProfileProcessor;
import de.hybris.clicks.webclient.customer.client.CustomerUpdateClient;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author shruthi.jhamb
 *
 */
public class UpdateProfileImpl implements UpdateProfile
{
	@Resource(name = "updateProfileProcessor")
	UpdateProfileProcessor updateProfileProcessor;
	@Resource
	CustomerUpdateClient customerUpdateClient;

	private static final Logger LOG = Logger.getLogger(UpdateProfileImpl.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.storefront.edit.profile.processor.UpdateProfile#saveUpdatedData()
	 */
	@Override
	public boolean saveUpdatedClubCardData(final CustomerData customerData)
	{
		ResultData resultData = null;
		boolean result = false;
		try
		{
			resultData = customerUpdateClient.updateCustomer(customerData);
			if ((null != resultData && null != resultData.getCode() && resultData.getCode().intValue() == 0))
			{
				result = updateProfileProcessor.saveUpdatedCCData(customerData);
			}
			return result;
		}
		catch (final Exception e)
		{
			LOG.info(e.getMessage());
			return result;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.clicks.storefront.edit.profile.processor.UpdateProfile#saveUpdatedNonClubCardData(de.hybris.platform
	 * .commercefacades.user.data.CustomerData)
	 */
	@Override
	public boolean saveUpdatedNonClubCardData(final CustomerData customerData)
	{
		ResultData resultData = null;
		boolean result = false;
		try
		{
			resultData = customerUpdateClient.updateCustomer(customerData);
			if ((null != resultData && null != resultData.getCode() && resultData.getCode().intValue() == 0))
			{
				result = updateProfileProcessor.saveUpdatedNonCCData(customerData);
			}
			return result;
		}
		catch (final Exception e)
		{
			LOG.info(e.getMessage());
			return result;
		}
	}

}
