/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.clicksstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.clicksstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.clicksstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.clicksstorefrontcommons.forms.GuestRegisterForm;
import de.hybris.platform.clicksstorefrontcommons.forms.validation.GuestRegisterValidator;
import de.hybris.platform.clicksstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * CheckoutController
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/checkout")
public class CheckoutController extends AbstractCheckoutController
{
	protected static final Logger LOG = Logger.getLogger(CheckoutController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";

	private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";

	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "cartService")
	private CartService cartService;
	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "guestRegisterValidator")
	private GuestRegisterValidator guestRegisterValidator;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@ExceptionHandler(ModelNotFoundException.class)
	public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request)
	{
		if (request.getHeader("referer").contains("/checkout/orderConfirmation/"))
		{
			return REDIRECT_PREFIX + "/my-account";
		}
		else
		{
			request.setAttribute("message", exception.getMessage());
			return FORWARD_PREFIX + "/404";
		}
	}

	@ModelAttribute("province")
	public Collection<ProvinceData> getProvinceList()
	{
		final Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String checkout(final RedirectAttributes redirectModel, final HttpServletRequest request)
	{
		//final String referrer = request.getHeader("referer");
		//getSessionService().setAttribute("returnToShopping", referrer);

		final String referer = request.getHeader("referer");
		//model.addAttribute("shoppingUrl", referer);
		getSessionService().setAttribute("returnToShopping", referer);
		/*
		 * if (userFacade.isAnonymousUser()) { getSessionService().setAttribute("returnToShopping", "/"); }
		 */

		final String sessionReferer = getSessionService().getAttribute("returnToShopping");

		if (referer.contains("/p/") || referer.contains("/c/") || referer.contains("/search"))
		{
			getSessionService().setAttribute("returnToShopping", referer);
		}
		else if (StringUtils.isEmpty(sessionReferer)
				|| (StringUtils.isNotEmpty(sessionReferer) && !sessionReferer.contains("/p/") && !sessionReferer.contains("/c/") || !sessionReferer
						.contains("/search")))
		{
			getSessionService().setAttribute("returnToShopping", "/");
		}
		else if (sessionReferer.contains("/login/"))
		{
			getSessionService().setAttribute("returnToShopping", "/");
		}

		if (Boolean.FALSE.equals(cartService.getSessionCart().getBasketEditable()))
		{
			return REDIRECT_PREFIX + "/cart";
		}
		if (getCheckoutFlowFacade().hasValidCart())
		{
			if (validateCart(redirectModel))
			{
				return REDIRECT_PREFIX + "/cart";
			}
			else
			{
				checkoutFacade.prepareCartForCheckout();
				return getCheckoutRedirectUrl();
			}
		}

		LOG.info("Missing, empty or unsupported cart");

		// No session cart or empty session cart. Bounce back to the cart page.
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderConfirmation(@PathVariable("orderCode") final String orderCode, final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException
	{
		LOG.info("referer URL" + request.getHeader("referer"));
		if (orderCode.contains("j_spring_security_check"))
		{
			return REDIRECT_PREFIX + "/my-account";
		}
		else
		{
			SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
			model.addAttribute("addToCartForm", new AddToCartForm());
			return processOrderCode(orderCode, model, request);
		}

	}


	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	public String orderConfirmation(final GuestRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getGuestRegisterValidator().validate(form, bindingResult);
		return processRegisterGuestUserRequest(form, bindingResult, model, request, response, redirectModel);
	}

	/**
	 * Method used to determine the checkout redirect URL that will handle the checkout process.
	 * 
	 * @return A <code>String</code> object of the URL to redirect to.
	 */
	protected String getCheckoutRedirectUrl()
	{
		if (getUserFacade().isAnonymousUser())
		{
			return REDIRECT_PREFIX + "/login/checkout";
		}

		// Default to the multi-step checkout
		return REDIRECT_PREFIX + "/checkout/multi";
	}

	protected String processRegisterGuestUserRequest(final GuestRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return processOrderCode(form.getOrderCode(), model, request);
		}
		try
		{
			getCustomerFacade().changeGuestToCustomer(form.getPwd(), form.getOrderCode());
			getAutoLoginStrategy().login(getCustomerFacade().getCurrentCustomer().getUid(), form.getPwd(), request, response);
			getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
		}
		catch (final DuplicateUidException e)
		{
			// User already exists
			LOG.warn("guest registration failed: " + e);
			model.addAttribute(new GuestRegisterForm());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"guest.checkout.existingaccount.register.error", new Object[]
					{ form.getUid() });
			return REDIRECT_PREFIX + request.getHeader("Referer");
		}

		return REDIRECT_PREFIX + "/my-account";
	}

	protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		LOG.info("orderCode :" + orderCode);
		if (orderCode.contains("j_spring_security_check"))
		{
			return REDIRECT_PREFIX + "/my-account";
		}
		else
		{
			final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
			if (orderDetails.isGuestCustomer()
					&& !StringUtils.substringBefore(orderDetails.getUser().getUid(), "|").equals(
							getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID)))
			{
				return getCheckoutRedirectUrl();
			}

			if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty())
			{
				for (final OrderEntryData entry : orderDetails.getEntries())
				{
					final String productCode = entry.getProduct().getCode();
					final ProductData product = productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(
							ProductOption.BASIC, ProductOption.PRICE, ProductOption.PROMOTIONS, ProductOption.GALLERY,
							ProductOption.VOLUME_PRICES));
					entry.setProduct(product);
				}
			}

			orderDetails.setNet(true);
			model.addAttribute("clubCardPointVal", orderDetails.getSubTotal().getValue().intValue() / 5);
			model.addAttribute("orderCode", orderCode);
			model.addAttribute("orderData", orderDetails);
			model.addAttribute("allItems", orderDetails.getEntries());
			model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
			model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
			model.addAttribute("paymentInfo", orderDetails.getRootPaymentInfo());
			model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

			try
			{
				model.addAttribute("gaOderConfirmation", sessionService.getAttribute("gaTrackingOderConfirmation"));
				sessionService.removeAttribute("gaTrackingOderConfirmation");
			}
			catch (final Exception e)
			{
				//
			}
			final String uid;

			if (orderDetails.isGuestCustomer() && !model.containsAttribute("guestRegisterForm"))
			{
				final GuestRegisterForm guestRegisterForm = new GuestRegisterForm();
				guestRegisterForm.setOrderCode(orderDetails.getGuid());
				uid = orderDetails.getDeliveryAddress().getEmail();
				guestRegisterForm.setUid(uid);
				model.addAttribute(guestRegisterForm);
			}
			else
			{
				uid = orderDetails.getDeliveryAddress().getEmail() != null ? orderDetails.getDeliveryAddress().getEmail()
						: orderDetails.getUser().getUid();
			}
			model.addAttribute("email", uid);

			final AbstractPageModel cmsPage = getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
			storeCmsPageInModel(model, cmsPage);
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL));
			model.addAttribute("metaRobots", "noindex,nofollow");

			return ControllerConstants.Views.Pages.Checkout.CheckoutConfirmationPage;
		}
	}

	protected GuestRegisterValidator getGuestRegisterValidator()
	{
		return guestRegisterValidator;
	}

	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
