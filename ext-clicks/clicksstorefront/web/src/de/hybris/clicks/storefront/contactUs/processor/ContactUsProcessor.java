/**
 *
 */
package de.hybris.clicks.storefront.contactUs.processor;

import de.hybris.clicks.facades.contactUs.impl.ContactUsFacade;
import de.hybris.clicks.storefront.forms.ContactUsForm;
import de.hybris.platform.commercefacades.user.data.ContactUsMessageData;

import javax.annotation.Resource;


/**
 * @author shruti.jhamb
 *
 */

public class ContactUsProcessor
{

	protected static final String CONTACT_US_EMAIL_PROCESS = "contactUsEmailProcess";

	@Resource
	ContactUsFacade contactUsFacade;


	public boolean processsContactUsData(final ContactUsForm form)
	{
		ContactUsMessageData contactUsData = new ContactUsMessageData();
		contactUsData = populateContactUsData(form, contactUsData);
		/*
		 * setting the process for email process
		 */
		contactUsData.setProcessName(CONTACT_US_EMAIL_PROCESS);
		final boolean customerCareResult = contactUsFacade.processContactUs(contactUsData);
		//final boolean customerSentResult2 = contactUsFacade.processContactUs(contactUsData, false);
		return customerCareResult;
	}

	/*
	 * Populating data from form to contact us data
	 */
	private ContactUsMessageData populateContactUsData(final ContactUsForm source, final ContactUsMessageData target)
	{
		if (null != source.getFirstName())
		{
			target.setFirstName(source.getFirstName());
		}
		if (null != source.getLastName())
		{
			target.setLastName(source.getLastName());
		}
		if (null != source.getEmail())
		{
			target.setEmailAddress(source.getEmail());
		}

		if (null != source.getContactNo())
		{
			target.setContactNumber(source.getContactNo());
		}

		if (null != source.getClubcardNo())
		{
			target.setClubCardNumber(source.getClubcardNo());
		}
		if (null != source.getMessage())
		{
			target.setMessage(source.getMessage());
		}
		if (null != source.getTopic())
		{
			target.setTopics(source.getTopic());
		}

		return target;
	}

}
