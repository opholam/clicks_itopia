/**
 *
 */
package de.hybris.clicks.storefront.controllers.pages;

import de.hybris.clicks.core.enums.ArticleTypesEnum;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;
import de.hybris.clicks.facades.article.ArticleFacade;
import de.hybris.clicks.storefront.controllers.ControllerConstants;
import de.hybris.clicks.storefront.resolver.ClicksPageTitleResolver;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.clicksstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.clicksstorefrontcommons.constants.WebConstants;
import de.hybris.platform.clicksstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.util.Config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author siva.reddy
 * 
 *         for managing all the articles in health hub
 * 
 */
@Controller
@Scope("tenant")
public class ArticlePageController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(ArticlePageController.class);
	private static final String MAGAZINE_ARTICLE_PAGE = "/clubcard-magazine/article-view/{articleTitle}";
	private static final String HEALTH_ARTICLE_PAGE = "/health/article-view/{articleTitle}";
	private static final String MEDICINES_ARTICLE_PAGE = "/health/medicines/article-view/{articleTitle}";
	private static final String CONDITION_ARTICLE_PAGE = "/health/conditions/article-view/{articleTitle}";
	private static final String VITAMIN_ARTICLE_PAGE = "/health/supplements/article-view/{articleTitle}";
	private static final String LAST_LINK_CLASS = "active";

	@Resource(name = "articleFacade")
	private ArticleFacade articleFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	private static final String HEALTH_ARTICLE_CMS_PAGE = "healthArticlePage";
	private static final String MAGAZINE_ARTICLE_CMS_PAGE = "magazineArticlePage";

	/**
	 * fetching the article by article code.
	 * 
	 * @param articleTitle
	 *           the article title
	 * @param model
	 *           the model
	 * @return the article by article code
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@SuppressWarnings("boxing")
	@RequestMapping(value =
	{ MAGAZINE_ARTICLE_PAGE, HEALTH_ARTICLE_PAGE, CONDITION_ARTICLE_PAGE, MEDICINES_ARTICLE_PAGE, VITAMIN_ARTICLE_PAGE }, method = RequestMethod.GET)
	public String getArticleByArticleCode(@PathVariable("articleTitle") final String articleTitle, final Model model)
			throws CMSItemNotFoundException
	{
		LOG.info("Article Title is ==" + articleTitle);

		model.addAttribute("articleTitle", articleTitle);
		try
		{
			final List<ArticleModel> articles = articleFacade.getArticleByArticleCode(articleTitle);
			final String property = Config.getParameter("article.type.value");
			final String[] articleTypeList = property.split(",");
			if (!articles.isEmpty() && articles.size() == 1)
			{
				final ArticleModel article = articles.get(0);
				getRelatedArticles(article, model);
				if (article.getContentDate() != null)
				{
					final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy");
					final String date = DATE_FORMAT.format(article.getContentDate());
					model.addAttribute("contentDate", date);
				}
				model.addAttribute("article", article);
				model.addAttribute("error", false);
				LOG.debug("Only one Article is found with article title " + articleTitle);

				if (!article.isIsActive())
				{
					model.addAttribute("metaRobots", "noindex,nofollow");
				}
				final ArticleTypesEnum articleType = article.getArticleType();
				if (ArticleTypesEnum.HEALTH == articleType)
				{
					storeCmsPageInModel(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE), article);
					model.addAttribute(WebConstants.BREADCRUMBS_KEY,
							resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.healtharticle"));
					model.addAttribute("basePage", articleTypeList[0]);
					return ControllerConstants.Views.Pages.Article.articleHealthMagazinePage;
				}
				else if (ArticleTypesEnum.MAGAZINES == articleType)
				{
					storeCmsPageInModel(model, getContentPageForLabelOrId(MAGAZINE_ARTICLE_CMS_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MAGAZINE_ARTICLE_CMS_PAGE), article);
					final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.clubcard");
					breadCrumbs.get(0).setUrl("/clubcard-magazine");
					breadCrumbs.addAll(resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.magazinearticle"));
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
					//	model.addAttribute("basePage", articleTypeList[1]);
					model.addAttribute("basePage", articleTypeList[1]);
					for (final ArticleTagsModel tagModel : article.getArticleTags())
					{
						model.addAttribute("cc" + tagModel.getTagName(), tagModel.getTagName().toUpperCase());
					}
					return ControllerConstants.Views.Pages.Article.articleHealthMagazinePage;
				}
				else if (ArticleTypesEnum.CONDITIONS == articleType)
				{
					storeCmsPageInModelForHealthPages(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE), article);
					final List<Breadcrumb> breadCrumbs = resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.conditions");
					breadCrumbs.get(0).setUrl("/health/info-list/category/conditions");
					breadCrumbs.add(new Breadcrumb("#", article.getArticleTitle(), LAST_LINK_CLASS));
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
					model.addAttribute("basePage", articleTypeList[2]);
					return ControllerConstants.Views.Pages.Article.articleConditionsPage;
				}
				else if (ArticleTypesEnum.MEDICINES == articleType)
				{
					storeCmsPageInModelForHealthPages(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE), article);
					model.addAttribute(WebConstants.BREADCRUMBS_KEY,
							resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.medicinearticle"));
					model.addAttribute("basePage", articleTypeList[3]);
					return ControllerConstants.Views.Pages.Article.articleConditionsPage;
				}
				else if (ArticleTypesEnum.VITAMINS == articleType)
				{
					storeCmsPageInModelForHealthPages(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE));
					setUpMetaDataForContentPage(model, getContentPageForLabelOrId(HEALTH_ARTICLE_CMS_PAGE), article);
					model.addAttribute(WebConstants.BREADCRUMBS_KEY,
							resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.vitaminarticle"));
					model.addAttribute("basePage", articleTypeList[4]);
					return ControllerConstants.Views.Pages.Article.articleConditionsPage;
				}
			}
			else if (articles.size() > 1)
			{
				model.addAttribute("articlesSize", articles.size());
				model.addAttribute("error", true);
				model.addAttribute("message", "More than one Article is found with article title : " + articleTitle);
				LOG.info("More than one Article is found with article title " + articleTitle);
			}
			else
			{
				model.addAttribute("error", true);
				model.addAttribute("message", "No Article is found with article title :" + articleTitle);
				LOG.debug("No Article is found with article title " + articleTitle);
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(null));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		}
		catch (final Exception e)
		{
			LOG.error(" Exception while searching for article with article title" + articleTitle + " " + e);
			model.addAttribute("error", true);
		}

		return ControllerConstants.Views.Pages.Article.articleErrorPage;
	} //method

	/**
	 * Gets the related articles.
	 * 
	 * @param article
	 *           the article
	 * @param model
	 *           the model
	 * @return the related articles
	 */
	private void getRelatedArticles(final ArticleModel article, final Model model)
	{
		try
		{
			if (CollectionUtils.isNotEmpty(article.getRelatedArticles()))
			{
				model.addAttribute("relatedConditions", CollectionUtils.select(article.getRelatedArticles(),
						new BeanPropertyValueEqualsPredicate("articleType", ArticleTypesEnum.CONDITIONS, true)));
				final Collection<ArticleModel> relatedMedicinesAndSupp = new ArrayList<ArticleModel>();
				relatedMedicinesAndSupp.addAll(CollectionUtils.select(article.getRelatedArticles(),
						new BeanPropertyValueEqualsPredicate("articleType", ArticleTypesEnum.MEDICINES, true)));
				relatedMedicinesAndSupp.addAll(CollectionUtils.select(article.getRelatedArticles(),
						new BeanPropertyValueEqualsPredicate("articleType", ArticleTypesEnum.VITAMINS, true)));
				model.addAttribute("relatedMedicinesSupmnt", relatedMedicinesAndSupp);
				model.addAttribute("relatedHealthArticles", CollectionUtils.select(article.getRelatedArticles(),
						new BeanPropertyValueEqualsPredicate("articleType", ArticleTypesEnum.HEALTH, true)));
			}
		}
		catch (final Exception e)
		{
			//
		}
	}

	protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage, final ArticleModel article)
	{
		String description = article.getTitleContent();
		if (StringUtils.isNotBlank(description) && description.length() > 160)
		{
			description = description.substring(0, 160);
		}

		if (article.getArticleType().getCode().equals("CONDITIONS"))
		{
			description = "The Clicks Health Hub provides information on " + article.getArticleTitle()
					+ ", including the causes, symptoms and treatment.";
		}
		else if (article.getArticleType().getCode().equals("MEDICINES") || article.getArticleType().getCode().equals("VITAMINS"))
		{
			description = "The Clicks Health Hub provides information on " + article.getArticleTitle()
					+ " including the benefits and side effects.";
		}
		setUpMetaData(model, contentPage.getKeywords(), description);
	}

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		if (model != null && cmsPage != null)
		{
			model.addAttribute(CMS_PAGE_MODEL, cmsPage);
			if (cmsPage instanceof ContentPageModel)
			{
				String articleCategory = "";
				final ArticleModel article = (ArticleModel) model.asMap().get(Config.getParameter("cms.page.article.name"));
				if (null != article && CollectionUtils.isNotEmpty(article.getArticleTags()))
				{
					articleCategory = article.getArticleTags().iterator().next().getTagName();
				}

				storeContentPageTitleInModel(model, ((ClicksPageTitleResolver) getPageTitleResolver()).resolveContentPageTitle(
						article.getTitleName(), articleCategory));
			}
		}
	}

	protected void storeCmsPageInModelForHealthPages(final Model model, final AbstractPageModel cmsPage)
	{
		if (model != null && cmsPage != null)
		{
			model.addAttribute(CMS_PAGE_MODEL, cmsPage);
			if (cmsPage instanceof ContentPageModel)
			{
				final ArticleModel article = (ArticleModel) model.asMap().get(Config.getParameter("cms.page.article.name"));
				storeContentPageTitleInModel(model,
						((ClicksPageTitleResolver) getPageTitleResolver()).resolveContentPageTitleForHealthPages(article
								.getArticleType().getCode(), article.getTitleName()));
			}
		}
	}
} //class
