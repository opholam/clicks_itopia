/**
 *
 */
package de.hybris.clicks.cronjob;

import de.hybris.clicks.core.productclickssizevariant.populator.ProductClicksSizeVariantRemove;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.FilenameFilter;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */
public class RemoveProductSizeVariantCronjob extends AbstractJobPerformable<CronJobModel>
{



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel
	 * )
	 */
	static Logger log = Logger.getLogger(RemoveProductSizeVariantCronjob.class.getName());
	@Resource
	ProductClicksSizeVariantRemove productClicksSizeVariantRemove;

	private static final String HIRTANDCARTERXMLFILEPATH = "hirtAndCarterXmlFileAndImagePath";
	private static final String HIRTANDCARTERXMLFILENAME = "hirtAndCarterXmlFileName";
	private static final String HIRTANDCARTERXMLPROCESSINGFOLDER = "hirtAndCarterProcessingFolderLocation";
	private static final String HIRTANDCARTERXMLERRORFOLDER = "hirtAndCarterErrorFolderLocation";

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		log.info("++++++++++++++++++++++++++++++++Entered into ProductSizeVariantCronjob++++++++++++++++++++++++++++++++");
		final String workingDirectory = System.getProperty("user.dir");
		final String[] temp = workingDirectory.split("bin");
		final File dir = new File(temp[0] + Config.getParameter(HIRTANDCARTERXMLFILEPATH));
		log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the xml file:  " + dir
				+ "  ++++++++++++++++++++++++++++++++");
		JAXBContext jaxbContext;
		final File[] files = dir.listFiles(new FilenameFilter()
		{
			@Override
			public boolean accept(final File dir, final String name)
			{
				return name.startsWith(Config.getParameter(HIRTANDCARTERXMLFILENAME));
			}
		});
		if (null != files && files.length > 0)
		{
			for (final File file : files)
			{
				try
				{
					jaxbContext = JAXBContext.newInstance(ProductSizeVariantData.class);
					final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					final ProductSizeVariantData productSizeVariantData = (ProductSizeVariantData) jaxbUnmarshaller.unmarshal(file);
					productClicksSizeVariantRemove.removeProductSizeVariant(productSizeVariantData);
				}
				catch (final JAXBException e)
				{
					return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
				}
			}
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		else
		{
			log.error("\n +++++++++++++++++++++ XML files with the name: " + Config.getParameter(HIRTANDCARTERXMLFILENAME)
					+ " not found on the location: " + dir + "+++++++++++++++++++++\n");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
	}


}
