/**
 *
 */
package de.hybris.clicks.cronjob;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.jalo.SyncItemCronJob;
import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncCronJob;
import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncJob;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncCronJobModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForPageModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSContentSlotDao;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 * 
 */
public class CMSWorkflowApproveAction implements AutomatedWorkflowTemplateJob
{
	private static final Logger LOG = Logger.getLogger(CMSWorkflowApproveAction.class);
	@Resource(name = "workflowProcessingService")
	private WorkflowProcessingService workflowProcessingService;
	@Resource(name = "cronJobService")
	private CronJobService cronJobService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "catalogService")
	private CatalogService catalogService;
	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;
	@Resource(name = "cmsContentSlotDao")
	private CMSContentSlotDao cmsContentSlotDao;

	//	@Resource(name = "catalogManager")
	//	private CatalogManager catalogManager;

	/**
	 * @return the workflowProcessingService
	 */
	public WorkflowProcessingService getWorkflowProcessingService()
	{
		return workflowProcessingService;
	}

	/**
	 * @param workflowProcessingService
	 *           the workflowProcessingService to set
	 */
	public void setWorkflowProcessingService(final WorkflowProcessingService workflowProcessingService)
	{
		this.workflowProcessingService = workflowProcessingService;
	}

	private CatalogVersionSyncJobModel createSyncJob(final CatalogVersionModel source, final CatalogVersionModel target)
	{
		//		try
		//		{
		//			final CatalogVersion sourceObject = modelService.getSource(source);
		//			final CatalogVersion targetObject = modelService.getSource(target);
		//			final SyncItemJobModel itemJobModel = null;
		//			//modelService.get(catalogManager.getSyncJob(sourceObject, targetObject));
		//			//	if (null != itemJobModel)
		//			//	{
		//			//	return (CatalogVersionSyncJobModel) itemJobModel;
		//			//	}
		//		}
		//		catch (final Exception e)
		//		{
		//			e.printStackTrace();
		//		}
		return createSyncJob(source, target, getMaxThreads());
	}

	private CatalogVersionSyncJobModel createSyncJob(final CatalogVersionModel source, final CatalogVersionModel target,
			final int numberOfThreads)
	{
		final CatalogVersionSyncJobModel job = modelService.create(CatalogVersionSyncJobModel.class);
		job.setCode(RandomStringUtils.randomAlphanumeric(10));
		job.setSourceVersion(source);
		job.setTargetVersion(target);
		job.setRemoveMissingItems(false);
		job.setCreateNewItems(true);
		job.setMaxThreads(numberOfThreads);

		modelService.save(job);

		return job;
	}

	private CatalogVersionSyncCronJobModel createSyncCronJob(final CatalogVersionSyncJobModel job,
			final Collection<ItemModel> attachments)
	{
		final CatalogVersionSyncJob jobItem = modelService.getSource(job);
		final SyncItemCronJob cronJob = jobItem.newExecution();
		jobItem.configureFullVersionSync(cronJob);
		final List pkArrayList = new ArrayList();
		PK[] pkArray;
		for (final ItemModel product : attachments)
		{
			pkArray = new PK[2];
			pkArray[0] = product.getPk();
			pkArray[1] = null;
			pkArrayList.add(pkArray);
			if (product instanceof AbstractPageModel)
			{
				try
				{
					final Collection<ContentSlotForPageModel> contentSlots = cmsContentSlotDao
							.findAllContentSlotRelationsForPage((AbstractPageModel) product);
					for (final ContentSlotForPageModel contentSlotForPageModel : contentSlots)
					{
						pkArray = new PK[2];
						pkArray[0] = contentSlotForPageModel.getPk();
						pkArray[1] = null;
						pkArrayList.add(pkArray);
						if (null != contentSlotForPageModel.getContentSlot())
						{
							pkArray = new PK[2];
							pkArray[0] = contentSlotForPageModel.getContentSlot().getPk();
							pkArray[1] = null;
							pkArrayList.add(pkArray);
							for (final AbstractCMSComponentModel components : contentSlotForPageModel.getContentSlot()
									.getCmsComponents())
							{
								pkArray = new PK[2];
								pkArray[0] = components.getPk();
								pkArray[1] = null;
								pkArrayList.add(pkArray);
							}
						}
					}
					final Collection<ContentSlotForPageModel> datas = cmsPageService
							.getOwnContentSlotsForPage((AbstractPageModel) product);
					final Collection<ContentSlotForTemplateModel> contentSlotForTemplateModels = cmsPageService
							.getContentSlotsForPageTemplate(((AbstractPageModel) product).getMasterTemplate());
					for (final ContentSlotForTemplateModel contentSlotForTemplateModel : contentSlotForTemplateModels)
					{
						pkArray = new PK[2];
						pkArray[0] = contentSlotForTemplateModel.getPk();
						pkArray[1] = null;
						pkArrayList.add(pkArray);
						if (null != contentSlotForTemplateModel.getContentSlot())
						{
							pkArray = new PK[2];
							pkArray[0] = contentSlotForTemplateModel.getContentSlot().getPk();
							pkArray[1] = null;
							pkArrayList.add(pkArray);
							for (final AbstractCMSComponentModel components : contentSlotForTemplateModel.getContentSlot()
									.getCmsComponents())
							{
								pkArray = new PK[2];
								pkArray[0] = components.getPk();
								pkArray[1] = null;
								pkArrayList.add(pkArray);
							}
						}
					}

					for (final ContentSlotForPageModel contentSlot : datas)
					{
						pkArray = new PK[2];
						pkArray[0] = contentSlot.getPk();
						pkArray[1] = null;
						pkArrayList.add(pkArray);
						if (null != contentSlot.getContentSlot())
						{
							pkArray = new PK[2];
							pkArray[0] = contentSlot.getContentSlot().getPk();
							pkArray[1] = null;
							pkArrayList.add(pkArray);
							for (final AbstractCMSComponentModel components : contentSlot.getContentSlot().getCmsComponents())
							{
								pkArray = new PK[2];
								pkArray[0] = components.getPk();
								pkArray[1] = null;
								pkArrayList.add(pkArray);
							}
						}
					}
				}
				catch (final Exception e)
				{
					LOG.error("Error in CMSWorkflowApproveAction class and method is createSyncCronJob() :" + e.getMessage());
				}
				LOG.info("pkArrayList" + pkArrayList.size());
				((AbstractPageModel) product).setApprovalStatus(CmsApprovalStatus.APPROVED);
				modelService.save(product);
			}
		}
		((CatalogVersionSyncCronJob) cronJob).addPendingItems(pkArrayList);
		return modelService.get(cronJob.getPK());
	}

	private Integer getMaxThreads()
	{
		return Integer.valueOf(CatalogVersionSyncJob.getDefaultMaxThreads(Registry.getCurrentTenantNoFallback()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob#perform(de.hybris.platform.workflow.model.
	 * WorkflowActionModel)
	 */
	@Override
	public WorkflowDecisionModel perform(final WorkflowActionModel action)
	{
		if (null != action && null != action.getWorkflow())
		{
			try
			{
				final List<ItemModel> attachments = action.getAttachmentItems();
				if (CollectionUtils.isNotEmpty(attachments))
				{
					final CatalogModel catalogModel = catalogService
							.getCatalogForId(Config.getParameter("clicks.contentCatalog.name"));
					CatalogVersionModel source = new CatalogVersionModel();
					source.setCatalog(catalogModel);
					source.setVersion(Config.getParameter("clicks.catalog.staged.version"));
					source = flexibleSearchService.getModelByExample(source);
					CatalogVersionModel target = new CatalogVersionModel();
					target.setCatalog(catalogModel);
					target.setVersion(Config.getParameter("clicks.catalog.online.version"));
					target = flexibleSearchService.getModelByExample(target);

					final CatalogVersionSyncJobModel syncJob = createSyncJob(source, target);
					final CatalogVersionSyncCronJobModel syncCronJob = createSyncCronJob(syncJob, attachments);
					syncCronJob.setPendingItems(attachments);
					cronJobService.performCronJob(syncCronJob, true);
				}
				workflowProcessingService.endWorkflow(action.getWorkflow());
			}
			catch (final Exception e)
			{
				LOG.error("Error in CMSWorkflowApproveAction class and method is perform() :" + e.getMessage());
				return CollectionUtils.isNotEmpty(action.getIncomingDecisions()) ? action.getIncomingDecisions().iterator().next()
						: null;
			}
		}
		return null;
	}

	/**
	 * @return the cronJobService
	 */
	public CronJobService getCronJobService()
	{
		return cronJobService;
	}

	/**
	 * @param cronJobService
	 *           the cronJobService to set
	 */
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the catalogService
	 */
	public CatalogService getCatalogService()
	{
		return catalogService;
	}

	/**
	 * @param catalogService
	 *           the catalogService to set
	 */
	public void setCatalogService(final CatalogService catalogService)
	{
		this.catalogService = catalogService;
	}

	/**
	 * @return the cmsPageService
	 */
	public CMSPageService getCmsPageService()
	{
		return cmsPageService;
	}

	/**
	 * @param cmsPageService
	 *           the cmsPageService to set
	 */
	public void setCmsPageService(final CMSPageService cmsPageService)
	{
		this.cmsPageService = cmsPageService;
	}



}
