/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.cronjob;

import de.hybris.clicks.core.model.StockNotificationModel;
import de.hybris.clicks.core.user.event.StockAvailabilityNotifyEvent;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;


/**
 * @author shruti.jhamb
 *
 */

public class NotifyStockStatusJob extends AbstractJobPerformable<CronJobModel>
{
	@SuppressWarnings("unused")
	//private static final Logger LOG = Logger.getLogger(NotifyStockStatusJob.class);
	protected EventService eventService;
	//private CMSSiteService cmsSiteService;
	private BaseSiteService baseSiteService;
	//private EventService eventService;
	BaseStoreService baseStoreService;
	protected CommonI18NService commonI18NService;
	UserService userService;


	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		final List<StockNotificationModel> storeResults = getUsersListForStore();
		processEmailForStores(storeResults);
		final List<StockNotificationModel> results = getUsersListForOnlineStore();
		processEmailForOnlineStore(results);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	/**
	 * @return
	 */
	private List<StockNotificationModel> getUsersListForOnlineStore()
	{
		final String query = "select {sn:PK}"
				+ "from {StockNotification AS sn JOIN Warehouse AS wh ON {sn:StoreID}={wh:code} JOIN StockLevel AS sl ON {sn:ProductCode}={sl:ProductCode} AND {sl:Available}>0  AND {sl:Warehouse}={wh:PK} }"
				+ "WHERE {sn:StockChangeFlag}=true";
		/*
		 * result set for stock availability in warehouse/online store
		 */
		final SearchResultImpl<StockNotificationModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query);
		final List<StockNotificationModel> result = searchResult.getResult();
		return result;
	}


	/**
	 * @param results
	 */
	private void processEmailForStores(final List<StockNotificationModel> results)
	{
		if (results.size() > 0)
		{
			for (final StockNotificationModel stockNotification : results)
			{
				processEmail(stockNotification);
			}
		}

	}

	private void processEmailForOnlineStore(final List<StockNotificationModel> results)
	{
		if (results.size() > 0)
		{
			for (final StockNotificationModel stockNotification : results)
			{
				processEmail(stockNotification);
			}
		}
	}


	/**
	 * @param stockNotification
	 */
	private void processEmail(final StockNotificationModel stockNotification)
	{
		try
		{
			/*
			 * publishing event for sending email to the subscribed customers
			 */
			eventService.publishEvent(initializeEvent(new StockAvailabilityNotifyEvent(), stockNotification));
			stockNotification.setStockChangeFlag(Boolean.FALSE);
			//stockNotification.getModifiedtime().getDay()
			modelService.save(stockNotification);
		}
		catch (final ModelSavingException saveexc)
		{
			//LOG.error(saveexc);
		}
		catch (final ModelNotFoundException e)
		{
			//LOG.info("stock Notification Model not found with the email: " + stockNotification.getEmailID());
		}

	}


	/*
	 * fetch users list which are subscribed and stock status has changed
	 */
	protected List<StockNotificationModel> getUsersListForStore()
	{
		/*
		 * result set for local store availability
		 */
		//final List<StockNotificationModel> resultList = searchResult.getResult();
		final String queryForstore = "select {sn:PK}"
				+ "from {StockNotification AS sn JOIN PointOfService AS pos ON {sn:StoreID}={pos:Name} JOIN StockLevel AS sl ON {sn:ProductCode}={sl:ProductCode} AND {sl:Available}>0 JOIN Warehouse AS wh ON {sl:Warehouse}={wh:PK} JOIN PoS2WarehouseRel as p2w on {p2w:target}={wh:pk} AND {p2w:source}={pos:pk}}"
				+ "WHERE {sn:StockChangeFlag}=true";
		final SearchResultImpl<StockNotificationModel> storeSearchResult = (SearchResultImpl) flexibleSearchService
				.search(queryForstore);
		final List<StockNotificationModel> result = storeSearchResult.getResult();
		//searchResultForWarehouse.get

		return result;
	}

	/*
	 * event to trigger email to the subscribed customers
	 */
	protected StockAvailabilityNotifyEvent initializeEvent(final StockAvailabilityNotifyEvent event,
			final StockNotificationModel stockNotification)
	{
		event.setBaseStore(baseStoreService.getAllBaseStores().iterator().next());
		event.setSite(baseSiteService.getAllBaseSites().iterator().next());
		/*
		 * fetchStoreName(stockNotification.getStoreID()) will final always give one result as store id is unique
		 */
		for (final PointOfServiceModel pos : fetchStoreName(stockNotification.getStoreID()))
		{
			event.setStoreName(pos.getDisplayName());
		}
		event.setProductCode(stockNotification.getProductCode());
		CustomerModel customer = null;
		try
		{
			customer = (CustomerModel) userService.getUserForUID(stockNotification.getEmailID());
			event.setCustomer(customer);
		}
		catch (final Exception exp)
		{
			customer = modelService.create(CustomerModel.class);
			customer.setUid(stockNotification.getEmailID());
			customer.setEmailID(stockNotification.getEmailID());
		}
		if (customer.getName() == null)
		{
			customer.setName("user");
		}
		event.setCustomer(customer);
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrency("ZAR"));
		return event;
	}


	/**
	 * @param storeID
	 * @return
	 */
	private List<PointOfServiceModel> fetchStoreName(final String storeID)
	{
		final String query = "select {PK} from {PointOfService} WHERE {Name}=" + storeID + "";
		final SearchResultImpl<PointOfServiceModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query);
		final List<PointOfServiceModel> result = searchResult.getResult();
		return result;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}


	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
