/**
 *
 */
package de.hybris.clicks.cronjob;

import de.hybris.clicks.webclient.product.client.OnlineCatalogueClient;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;


/**
 * @author abhaydh
 *
 */
public class OnlineHirarchyPublishCronjob extends AbstractJobPerformable<CronJobModel>
{
	@Resource
	OnlineCatalogueClient onlineCatalogueClient;

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		String property= Config.getParameter("product.catalogue.client.action");
		final String[] catalogueList=property.split(",");
		onlineCatalogueClient.productCatalogueClient(catalogueList[2]);

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
