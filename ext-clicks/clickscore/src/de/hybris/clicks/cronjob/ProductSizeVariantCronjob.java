/**
 *
 */
package de.hybris.clicks.cronjob;

import de.hybris.clicks.core.productclickssizevariant.populator.ProductClicksSizeVariantPopulator;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;






/**
 * @author mbhargava
 * @param <CronJobModel>
 *
 */
public class ProductSizeVariantCronjob extends AbstractJobPerformable<CronJobModel>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel
	 * )
	 */
	static Logger log = Logger.getLogger(ProductSizeVariantCronjob.class.getName());
	@Resource
	ProductClicksSizeVariantPopulator productClicksSizeVariantPopulator;

	private static final String HIRTANDCARTERXMLFILEPATH = "hirtAndCarterXmlFileAndImagePath";
	private static final String HIRTANDCARTERXMLFILENAME = "hirtAndCarterXmlFileName";
	private static final String HIRTANDCARTERXMLPROCESSINGFOLDER = "hirtAndCarterProcessingFolderLocation";
	private static final String HIRTANDCARTERXMLERRORFOLDER = "hirtAndCarterErrorFolderLocation";

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		log.info("++++++++++++++++++++++++++++++++Entered into ProductSizeVariantCronjob++++++++++++++++++++++++++++++++");
		final String workingDirectory = System.getProperty("user.dir");
		final String[] temp = workingDirectory.split("bin");
		final File dir = new File(temp[0] + Config.getParameter(HIRTANDCARTERXMLFILEPATH));
		final List<String> skuIdTotalList = new ArrayList<String>();
		log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the xml file:  " + dir
				+ "  ++++++++++++++++++++++++++++++++");
		JAXBContext jaxbContext;
		final String fileNamePattern = Config.getParameter(HIRTANDCARTERXMLFILENAME);
		final File[] files = dir.listFiles(new FileFilter()
		{

			@Override
			public boolean accept(final File pathname)
			{
				// YTODO Auto-generated method stub
				if (StringUtils.isNotEmpty(fileNamePattern))
				{
					return pathname.getName().startsWith(fileNamePattern);
				}
				return !pathname.isDirectory();
			}
		});
		if (null != files && files.length > 0)
		{
			log.info("\n++++++++++++++++++++++++++++++++Number of Product size variants file to execute:  " + files.length
					+ "  ++++++++++++++++++++++++++++++++\n");
			for (final File file : files)
			{
				try
				{
					jaxbContext = JAXBContext.newInstance(ProductSizeVariantData.class);
					final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					final ProductSizeVariantData productSizeVariantData = (ProductSizeVariantData) jaxbUnmarshaller.unmarshal(file);
					skuIdTotalList.addAll(productClicksSizeVariantPopulator.populateAndSaveProductSizeVariant(productSizeVariantData));
					final File folder = new File(temp[0] + Config.getParameter(HIRTANDCARTERXMLPROCESSINGFOLDER));
					if (!folder.exists())
					{
						new File(temp[0] + Config.getParameter(HIRTANDCARTERXMLPROCESSINGFOLDER)).mkdir();
					}
					try
					{
						FileUtils.moveFileToDirectory(file, folder, true);
					}
					catch (final Exception e)
					{
						log.error("\r\n File: " + file + " is failed to move to the location :" + folder + e.getMessage());
					}

				}
				catch (final JAXBException e)
				{
					log.error("\r\n+++++++++++++ ProductSizeVariantCronjob has been failed because of file: " + file
							+ "  +++++++++++++++\n");
					final File folder = new File(temp[0] + Config.getParameter(HIRTANDCARTERXMLERRORFOLDER));
					if (!folder.exists())
					{
						new File(temp[0] + Config.getParameter(HIRTANDCARTERXMLERRORFOLDER)).mkdir();
					}
					try
					{
						FileUtils.moveFileToDirectory(file, folder, true);
					}
					catch (final Exception e1)
					{
						log.error("\r\n +++++++++++++++++++++ File: " + file + " is failed to move to the location :" + folder
								+ " +++++++++++++++++++++" + e1.getMessage());
					}
					return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
				}
			}
			log.info("\r\n +++++++++++++++++++++ Number of SKU's for which images have been added through Cronjob: "
					+ skuIdTotalList.size() + " +++++++++++++++++++++\n");
			log.info("\r\n +++++++++++++++++++++ SKU id's detail: " + skuIdTotalList + " +++++++++++++++++++++");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		else
		{
			log.error("\n +++++++++++++++++++++ XML files with the name: " + Config.getParameter(HIRTANDCARTERXMLFILENAME)
					+ " not found on the location: " + dir + "+++++++++++++++++++++\n");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
	}
}
