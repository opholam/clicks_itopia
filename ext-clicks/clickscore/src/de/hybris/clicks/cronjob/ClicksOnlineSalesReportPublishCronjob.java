/**
 *
 */
package de.hybris.clicks.cronjob;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.clicks.integration.service.impl.OnlineSalesReportServiceImpl;


/**
 * @author manish.bhargava
 *
 */
public class ClicksOnlineSalesReportPublishCronjob extends AbstractJobPerformable<CronJobModel>
{

	static Logger log = Logger.getLogger(ClicksOnlineSalesReportPublishCronjob.class.getName());
	private final String LIKE_CHARACTER = "%";
	private final String DATE_PATTERN = "YYYY-MM-dd";


	@Resource
	OnlineSalesReportServiceImpl onlineSalesReportService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		String date = null;
		final SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
		final Date reportDate = arg0.getReportDate();
		if (null != reportDate)
		{
			try
			{
				date = format.format(reportDate);
			}
			catch (final Exception e)
			{
				log.error("Incorrect date format for reportDate. Exception while publishing online sales report", e);
				return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
			}
		}
		else
		{
			final Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			date = format.format(cal.getTime());
		}
		String query = "";
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("creationDate", LIKE_CHARACTER + date + LIKE_CHARACTER);
		query = "select DISTINCT {pk} from {Order} where {creationtime} LIKE ?creationDate";
		final SearchResultImpl<OrderModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<OrderModel> result = searchResult.getResult();
		try
		{
			if (CollectionUtils.isNotEmpty(result))
			{
				onlineSalesReportService.publishOnlineSalesReportData(result);
			}
			else
			{
				log.info("++++++++++++++++No orders available to publish Online sales report because order collection is empty++++++++++++++++");
			}
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			log.error("Exception while publishing online sales report", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
	}

}
