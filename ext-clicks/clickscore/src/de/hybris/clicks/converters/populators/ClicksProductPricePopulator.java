/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.converters.populators;

import de.hybris.clicks.core.util.GrossPriceUtil;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductBOGOFPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.FormatFactory;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;


/**
 * Populate the product data with the price information
 */
public class ClicksProductPricePopulator extends ProductPricePopulator<ProductModel, ProductData>
{
	@Resource(name = "formatFactory")
	protected FormatFactory formatFactory;
	private PromotionsService promotionsService;
	private TimeService timeService;
	private BaseSiteService baseSiteService;

	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * @param promotionsService
	 *           the promotionsService to set
	 */
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}

	/**
	 * @return the timeService
	 */
	public TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * @param timeService
	 *           the timeService to set
	 */
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProduct(productModel);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getFromPriceForProduct(productModel);
		}

		if (info != null)
		{
			final PriceData priceData = getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
					info.getPriceValue().getCurrencyIso());
			productData.setPrice(priceData);

			String formattedValue = null;
			Double grossPriceWithPromotionApplied = null;

			List<Object> promoPrices = null;

			if (CollectionUtils.isEmpty(productModel.getPromotions()) && CollectionUtils.isNotEmpty(productModel.getVariants()))
			{
				for (final VariantProductModel sizeVariant : productModel.getVariants())
				{
					promoPrices = getPromoPrice(sizeVariant, productData, priceData);
					if (CollectionUtils.isNotEmpty(promoPrices))
					{
						break;
					}
				}
			}
			else
			{
				promoPrices = getPromoPrice(productModel, productData, priceData);
			}
			formattedValue = (String) promoPrices.get(0);
			grossPriceWithPromotionApplied = (Double) promoPrices.get(1);
			if (null != grossPriceWithPromotionApplied && grossPriceWithPromotionApplied.compareTo(0.0) > 0)
			{
				productData.getPrice().setGrossPriceValueWithPromotionAppliedFormattedValue(formattedValue);
				productData.getPrice().setGrossPriceWithPromotionApplied(grossPriceWithPromotionApplied);
			}
		}
		else
		{
			productData.setPurchasable(Boolean.FALSE);
		}
	}

	/**
	 * @param productModel
	 * @param productData
	 * @param priceData
	 * @param grossPriceWithPromotionApplied
	 * @param formattedValue
	 */
	private List<Object> getPromoPrice(final ProductModel productModel, final ProductData productData, final PriceData priceData)
	{
		final List<Object> prices = new ArrayList<Object>(1);
		Double grossPriceWithPromotionApplied = null;
		String formattedValue = null;

		final BaseSiteModel baseSiteModel = getBaseSiteService().getCurrentBaseSite();
		if (baseSiteModel != null)
		{
			final PromotionGroupModel defaultPromotionGroup = baseSiteModel.getDefaultPromotionGroup();
			final Date currentTimeRoundedToMinute = DateUtils.round(getTimeService().getCurrentTime(), Calendar.MINUTE);

			if (defaultPromotionGroup != null)
			{
				final List<ProductPromotionModel> promotions = getPromotionsService().getProductPromotions(
						Collections.singletonList(defaultPromotionGroup), productModel, true, currentTimeRoundedToMinute);

				for (final ProductPromotionModel promo : promotions)
				{
					if (!(promo instanceof ProductBOGOFPromotionModel))
					{
						if (Boolean.TRUE.equals(promo.getEnabled()))
						{
							if (GrossPriceUtil.applyRestrictions(promo))
							{
								grossPriceWithPromotionApplied = GrossPriceUtil.getGrossPrice(promo, priceData.getValue());
								if (null != priceData.getValue() && null != grossPriceWithPromotionApplied
										&& grossPriceWithPromotionApplied > 0.0
										&& (priceData.getValue().compareTo(BigDecimal.valueOf(grossPriceWithPromotionApplied)) > 0))
								{
									formattedValue = formatFactory.createCurrencyFormat().format(grossPriceWithPromotionApplied);
									break;
									/*
									 * productData.getPrice().setGrossPriceValueWithPromotionAppliedFormattedValue(formattedValue)
									 * ;
									 * productData.getPrice().setGrossPriceWithPromotionApplied(grossPriceWithPromotionApplied);
									 */
								}
							}
						}
					}
				}
				//productData.setPotentialPromotions(Converters.convertAll(promotions, getPromotionsConverter()));
			}
		}

		prices.add(formattedValue);
		prices.add(grossPriceWithPromotionApplied);

		return prices;
	}
}
