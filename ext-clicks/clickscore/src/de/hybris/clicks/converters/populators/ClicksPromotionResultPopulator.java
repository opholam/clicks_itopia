/**
 *
 */
package de.hybris.clicks.converters.populators;

import de.hybris.platform.commercefacades.order.converters.populator.PromotionResultPopulator;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;


/**
 * @author Shruthi.Jhamb
 *
 */
public class ClicksPromotionResultPopulator extends PromotionResultPopulator
{

	@Override
	public void populate(final PromotionResultModel source, final PromotionResultData target)
	{
		if (source.getPromotion() instanceof ProductPerfectPartnerBundlePromotionModel
				&& null != ((ProductPerfectPartnerBundlePromotionModel) source.getPromotion()).getPartnerProducts())
		{
			String description = null;
			if (null != getPromotionResultService().getDescription(source)
					&& getPromotionResultService().getDescription(source).contains("PartnerProductName")
					&& null != ((ProductPerfectPartnerBundlePromotionModel) source.getPromotion()).getPartnerProducts())
			{
				final StringBuilder buff = new StringBuilder();
				for (final ProductModel product : ((ProductPerfectPartnerBundlePromotionModel) source.getPromotion())
						.getPartnerProducts())
				{
					buff.append(product.getName());
					buff.append(",");
				}
				description = getPromotionResultService().getDescription(source).replaceAll(
						"PartnerProductName, PartnerProductName, PartnerProductName", buff.toString());
			}
			if (null != ((ProductPerfectPartnerBundlePromotionModel) source.getPromotion()).getQualifyingCount())
			{
				description = description.replaceAll("X products",
						((ProductPerfectPartnerBundlePromotionModel) source.getPromotion()).getQualifyingCount().toString()
								+ " products");
			}
			if (null != ((ProductPerfectPartnerBundlePromotionModel) source.getPromotion()).getBaseProduct()
					&& getPromotionResultService().getDescription(source).contains("ProductName"))
			{
				description = description.replaceAll("ProductName",
						((ProductPerfectPartnerBundlePromotionModel) source.getPromotion()).getBaseProduct().getName());
			}
			//source.getPromotion().setDescription(description);
			//modelService.save(source);
			target.setDescription(description);
		}
		else if (source.getPromotion() instanceof ProductPerfectPartnerPromotionModel
				&& null != ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getPartnerProducts())
		{
			String description = null;
			if (null != getPromotionResultService().getDescription(source)
					&& getPromotionResultService().getDescription(source).contains("PartnerProductName")
					&& null != ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getPartnerProducts())
			{
				final StringBuilder buff = new StringBuilder();
				if (null != ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getPartnerProducts())
				{
					for (final ProductModel product : ((ProductPerfectPartnerPromotionModel) source.getPromotion())
							.getPartnerProducts())
					{
						buff.append(product.getName());
						buff.append(",");
					}
				}
				description = getPromotionResultService().getDescription(source).replaceAll("PartnerProductName", buff.toString());
			}
			if (null != getPromotionResultService().getDescription(source)
					&& getPromotionResultService().getDescription(source).contains("ProductName")
					&& null != ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getProducts())
			{
				final StringBuilder buff = new StringBuilder();
				if (null != ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getProducts())
				{
					for (final ProductModel product : ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getProducts())
					{
						buff.append(product.getName());
						buff.append(",");
					}
					description = description.replaceAll("ProductName", buff.toString());
				}
			}
			/*
			 * if (null != ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getProducts()) { for (final
			 * ProductModel product : ((ProductPerfectPartnerPromotionModel) source.getPromotion()).getProducts()) {
			 * buff.append(product.getName()); buff.append(","); } } description =
			 * getPromotionResultService().getDescription(source).replaceAll("ProductName", buff.toString());
			 */
			//source.getPromotion().setDescription(description);
			//getPromotionResultService().getDescription(source);
			//modelService.save(source);
			target.setDescription(description);

		}
		else
		{
			target.setDescription(getPromotionResultService().getDescription(source));
		}
		target.setPromotionData(getPromotionsConverter().convert(source.getPromotion()));
		target.setConsumedEntries(Converters.convertAll(source.getConsumedEntries(), getPromotionOrderEntryConsumedConverter()));
	}
}
