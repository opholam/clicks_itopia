/**
 *
 */
package de.hybris.clicks.converters.populators;

import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.ProductModelToSiteMapUrlDataPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collections;

import org.apache.commons.lang.StringEscapeUtils;


/**
 * @author Swapnil Desai
 *
 */
public class ClicksProductModelToSiteMapUrlDataPopulator extends ProductModelToSiteMapUrlDataPopulator
{

	@Override
	public void populate(final ProductModel productModel, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		ClicksApparelSizeVariantModel sizeVariant;
		if (productModel instanceof ClicksApparelSizeVariantModel)
		{
			sizeVariant = (ClicksApparelSizeVariantModel) productModel;
			final String relUrl = StringEscapeUtils.escapeXml(getUrlResolver().resolve(sizeVariant));
			siteMapUrlData.setLoc(relUrl);
			if (sizeVariant.getPicture() != null)
			{
				siteMapUrlData.setImages(Collections.singletonList(sizeVariant.getPicture().getURL()));
			}
		}



	}

}
