/**
 *
 */
package de.hybris.clicks.converters.populators;

import de.hybris.platform.acceleratorservices.jalo.promotions.AcceleratorProductMultiBuyPromotion;
import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductOneToOnePerfectPartnerPromotion;
import de.hybris.platform.promotions.jalo.ProductPerfectPartnerBundlePromotion;
import de.hybris.platform.promotions.jalo.ProductPerfectPartnerPromotion;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryConsumed;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author Shruti.Jhamb
 *
 */
public class ClicksCartEntryPopulator extends OrderEntryPopulator
{
	private static final Logger LOG = Logger.getLogger(ClicksCartEntryPopulator.class);
	@Resource
	private PromotionsService promotionsService;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
	{
		super.populate(source, target);
		addAppliedProductPromotions(source.getOrder(), target);
		addPotentialProductPromotions(source.getOrder(), target);
	}


	/**
	 * @param order
	 * @param target
	 */
	private void addPotentialProductPromotions(final AbstractOrderModel order, final OrderEntryData target)
	{
		try
		{
			final PromotionOrderResults results = promotionsService.getPromotionResults(order);
			final List<PromotionOrderEntryConsumed> list = new ArrayList<PromotionOrderEntryConsumed>();

			for (final PromotionResult promotionResult : results.getPotentialProductPromotions())
			{
				list.addAll(CollectionUtils.select(promotionResult.getConsumedEntries(), new BeanPropertyValueEqualsPredicate(
						"orderEntry.entryNumber", target.getEntryNumber(), true)));
			}

			try
			{
				Collection<String> potentialPromoDescription = new ArrayList<String>();
				potentialPromoDescription = editPotentialPromotionDescription(list, target);
				if (CollectionUtils.isNotEmpty(potentialPromoDescription))
				{
					target.setPotentialPromoDescription(potentialPromoDescription);
				}
			}
			catch (final NullPointerException nullexception)
			{
				LOG.error(nullexception);
			}
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}



	}


	/**
	 * @param list
	 * @param target
	 * @return
	 */
	private Collection<String> editPotentialPromotionDescription(final List<PromotionOrderEntryConsumed> list,
			final OrderEntryData target)
	{

		final Collection<String> promoDescription = new ArrayList<String>();
		final Map<String, Double> map = new HashMap<String, Double>();
		String key = "";
		if (CollectionUtils.isNotEmpty(list))
		{
			for (final PromotionOrderEntryConsumed promotionOrderEntryConsumed : list)
			{
				if (null != promotionOrderEntryConsumed.getPromotionResult().getDescription())
				{
					key = replaceProductName(promotionOrderEntryConsumed);
					if (map.containsKey(key))
					{
						//final double discount = promotionOrderEntryConsumed.getAdjustedEntryPrice() * entry.getQuantity().doubleValue()
						//	+ MapUtils.getDouble(map, key, 0.0);
						map.put(
								key,
								(((promotionOrderEntryConsumed.getEntryPrice() * promotionOrderEntryConsumed.getQuantity()) - promotionOrderEntryConsumed
										.getAdjustedEntryPrice()) + MapUtils.getDouble(map, key, 0.0)));
					}
					else
					{
						map.put(key, (promotionOrderEntryConsumed.getEntryPrice() * promotionOrderEntryConsumed.getQuantity())
								- promotionOrderEntryConsumed.getAdjustedEntryPrice());
					}
				}
			}
			String desc = "";
			for (final Entry<String, Double> entryVal : map.entrySet())
			{
				desc = entryVal.getKey().replace("{5}",
						"R" + String.valueOf(BigDecimal.valueOf(entryVal.getValue()).setScale(2, RoundingMode.HALF_UP)));
				promoDescription.add(desc);
			}
		}
		return promoDescription;
	}


	protected void addAppliedProductPromotions(final AbstractOrderModel abstractOrder, final OrderEntryData entry)
	{
		try
		{
			final PromotionOrderResults results = promotionsService.getPromotionResults(abstractOrder);
			final List<PromotionOrderEntryConsumed> list = new ArrayList<PromotionOrderEntryConsumed>();

			for (final PromotionResult promotionResult : results.getAppliedProductPromotions())
			{
				list.addAll(CollectionUtils.select(promotionResult.getConsumedEntries(), new BeanPropertyValueEqualsPredicate(
						"orderEntry.entryNumber", entry.getEntryNumber(), true)));
			}

			try
			{
				Collection<String> appliedPromoDescription = new ArrayList<String>();
				appliedPromoDescription = editPromotionDescription(list, entry);
				if (CollectionUtils.isNotEmpty(appliedPromoDescription))
				{
					entry.setAppliedPromoDescription(appliedPromoDescription);
				}
			}
			catch (final NullPointerException nullexception)
			{
				LOG.error(nullexception);
			}
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * @param promotionResult
	 */
	private Collection<String> editPromotionDescription(final List<PromotionOrderEntryConsumed> list, final OrderEntryData entry)
	{
		final Collection<String> promoDescription = new ArrayList<String>();
		final Map<String, Double> map = new HashMap<String, Double>();
		String key = "";
		if (CollectionUtils.isNotEmpty(list))
		{
			for (final PromotionOrderEntryConsumed promotionOrderEntryConsumed : list)
			{
				if (null != promotionOrderEntryConsumed.getPromotionResult().getDescription())
				{
					key = replaceProductName(promotionOrderEntryConsumed);
					if (map.containsKey(key))
					{
						//final double discount = promotionOrderEntryConsumed.getAdjustedEntryPrice() * entry.getQuantity().doubleValue()
						//	+ MapUtils.getDouble(map, key, 0.0);
						if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof AcceleratorProductMultiBuyPromotion)
						{
							map.put(key, (promotionOrderEntryConsumed.getAdjustedEntryPrice() + MapUtils.getDouble(map, key, 0.0)));
						}
						else
						{
							map.put(key,
									(promotionOrderEntryConsumed.getEntryPrice() - promotionOrderEntryConsumed.getAdjustedEntryPrice())
											+ MapUtils.getDouble(map, key, 0.0));
						}
					}
					else
					{
						if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof AcceleratorProductMultiBuyPromotion)
						{
							map.put(key, promotionOrderEntryConsumed.getAdjustedEntryPrice());
						}
						else
						{
							map.put(key,
									(promotionOrderEntryConsumed.getEntryPrice() - promotionOrderEntryConsumed.getAdjustedEntryPrice()));
						}
					}
				}
			}
			String desc = "";
			for (final Entry<String, Double> entryVal : map.entrySet())
			{
				if (entryVal.getKey().contains("{5}"))
				{
					desc = entryVal.getKey().replace("{5}",
							"R" + String.valueOf(BigDecimal.valueOf(entryVal.getValue()).setScale(2, RoundingMode.HALF_UP)));
				}
				else
				{
					desc = entryVal.getKey();
				}
				promoDescription.add(desc);
			}
		}
		return promoDescription;


	}

	/**
	 * @param promotionOrderEntryConsumed
	 * @return
	 */
	private String replaceProductName(final PromotionOrderEntryConsumed promotionOrderEntryConsumed)
	{
		String key = promotionOrderEntryConsumed.getPromotionResult().getDescription();
		if (key.contains("You have saved"))
		{
			key = StringUtils.substringBefore(key, "You have saved");
			key = key.concat("You have saved {5}");
		}
		//System.out.println("key value" + key);
		try
		{
			//if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof AcceleratorProductMultiBuyPromotion)
			//{
			//key = promotionOrderEntryConsumed.getPromotionResult().getDescription();
			//}
			if (StringUtils.containsIgnoreCase(key, "PartnerProductName"))
			{
				String perfProdName = "";
				final StringBuilder buff = new StringBuilder();
				if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof ProductPerfectPartnerPromotion)
				{
					for (final Product product : ((ProductPerfectPartnerPromotion) (promotionOrderEntryConsumed.getPromotionResult()
							.getPromotion())).getPartnerProducts())
					{
						buff.append(product.getName());
						buff.append(",");
					}
					perfProdName = buff.toString();
				}
				else if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof ProductOneToOnePerfectPartnerPromotion)
				{
					perfProdName = ((ProductOneToOnePerfectPartnerPromotion) (promotionOrderEntryConsumed.getPromotionResult()
							.getPromotion())).getPartnerProduct().getName();
				}
				else if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof ProductPerfectPartnerBundlePromotion)
				{
					for (final Product product : ((ProductPerfectPartnerBundlePromotion) (promotionOrderEntryConsumed
							.getPromotionResult().getPromotion())).getPartnerProducts())
					{
						buff.append(product.getName());
						buff.append(",");
					}
					perfProdName = buff.toString();
				}
				if (key.contains("PartnerProductName, PartnerProductName, PartnerProductName"))
				{
					key = key.replaceAll("PartnerProductName, PartnerProductName, PartnerProductName", perfProdName);
				}
				else
				{
					key = key.replaceAll("PartnerProductName", perfProdName);
				}
			}
			if (StringUtils.containsIgnoreCase(key, "productname"))
			{
				key = StringUtils.replace(key, "ProductName", promotionOrderEntryConsumed.getProduct().getName());
			}
			if (StringUtils.containsIgnoreCase(key, "X products"))
			{
				key = StringUtils.replace(key, "X products", promotionOrderEntryConsumed.getQuantity() + "products");
			}
		}
		catch (final Exception e)
		{
			//
		}
		return key;
	}

	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * @param promotionsService
	 *           the promotionsService to set
	 */
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}
}
