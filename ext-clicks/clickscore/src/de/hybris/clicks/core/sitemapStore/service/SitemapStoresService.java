/**
 * 
 */
package de.hybris.clicks.core.sitemapStore.service;

import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;


/**
 * @author sridhar.reddy
 * 
 */
public interface SitemapStoresService
{
	public abstract List<PointOfServiceModel> getSitemapStores();
}
