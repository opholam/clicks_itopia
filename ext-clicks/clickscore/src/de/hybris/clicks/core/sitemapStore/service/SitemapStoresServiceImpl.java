/**
 * 
 */
package de.hybris.clicks.core.sitemapStore.service;

import de.hybris.clicks.core.sitemapStore.dao.SitemapStoresDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import javax.annotation.Resource;


/**
 * @author sridhar.reddy
 * 
 */

public class SitemapStoresServiceImpl implements SitemapStoresService
{

	@Resource(name = "sitemapStoresDao")
	private SitemapStoresDao sitemapStoresDao;

	@Override
	public List<PointOfServiceModel> getSitemapStores()
	{

		return sitemapStoresDao.getSitemapStores();
	}
}
