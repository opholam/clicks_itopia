/**
 *
 */
package de.hybris.clicks.core.sitemapStore.dao;

import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;


/**
 * @author sridhar.reddy
 *
 */
public interface SitemapStoresDao
{

	/**
	 * Gets the sitemap stores.
	 *
	 * @return the sitemap stores
	 */
	public abstract List<PointOfServiceModel> getSitemapStores();
}
