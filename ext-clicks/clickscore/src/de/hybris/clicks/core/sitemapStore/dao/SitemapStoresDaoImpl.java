/**
 *
 */
package de.hybris.clicks.core.sitemapStore.dao;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author sridhar.reddy
 *
 */

public class SitemapStoresDaoImpl implements SitemapStoresDao
{

	protected static final Logger LOG = Logger.getLogger(SitemapStoresDaoImpl.class);

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.sitemapStore.dao.SitemapStoresDao#getSitemapStores()
	 */
	@Override
	public List<PointOfServiceModel> getSitemapStores()
	{
		LOG.debug("Sitemap store Dao Impl ");

		final String query = "SELECT {" + PointOfServiceModel.PK + "} FROM {" + PointOfServiceModel._TYPECODE + "}ORDER BY {"
				+ PointOfServiceModel.DISPLAYNAME + "} ASC";

		final SearchResultImpl<PointOfServiceModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query);
		final List<PointOfServiceModel> result = searchResult.getResult();
		return result == null ? Collections.EMPTY_LIST : result;

	}
}
