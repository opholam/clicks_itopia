/**
 *
 */
package de.hybris.clicks.core.custom.service.impl;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.security.CoreUserDetails;
import de.hybris.platform.spring.security.CoreUserDetailsService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


/**
 * @author manikandan.gopalan
 *
 */
public class ClicksCustomCoreUserDetailService extends CoreUserDetailsService
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.user.impl.DefaultUserService#getUserForUID(java.lang.String)
	 */
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @return the rolePrefix
	 */
	public String getRolePrefix()
	{
		return rolePrefix;
	}

	private String rolePrefix = "ROLE_";

	@Override
	public CoreUserDetails loadUserByUsername(final String username)
	{
		if (username == null)
		{
			return null;
		}

		//User user = null;
		UserModel userModel = null;
		try
		{
			userModel = userService.getUserForUID(username);
		}
		catch (final Exception localJaloItemNotFoundException)
		{
			userModel = getUserByMemberId(username);
		}
		if (userModel == null)
		{
			throw new UsernameNotFoundException("User not found!");
		}
		//user = modelService.getSource(userModel);

		final boolean enabled = !(userModel.isLoginDisabled());
		String password = userModel.getEncodedPassword();
		//String password = user.getEncodedPassword(JaloSession.getCurrentSession().getSessionContext());

		if (password == null)
		{
			password = "";
		}

		final CoreUserDetails userDetails = new CoreUserDetails(userModel.getUid(), password, enabled, true, true, true,
				getAuthorities(userModel), "");

		return userDetails;
	}

	/**
	 * @param username
	 */
	private UserModel getUserByMemberId(final String username)
	{
		try
		{
			CustomerModel cusModel = new CustomerModel();
			cusModel.setMemberID(username);
			cusModel = flexibleSearchService.getModelByExample(cusModel);
			return cusModel;
		}
		catch (final Exception e)
		{
			//
		}
		return null;
	}

	private Collection<GrantedAuthority> getAuthorities(final UserModel user)
	{
		final Set groups = user.getGroups();
		final Collection authorities = new ArrayList(groups.size());
		final Iterator itr = groups.iterator();
		while (itr.hasNext())
		{
			final PrincipalGroupModel group = (PrincipalGroupModel) itr.next();
			authorities.add(new SimpleGrantedAuthority(this.rolePrefix + group.getUid().toUpperCase()));
			for (final PrincipalGroupModel gr : group.getAllGroups())
			{
				authorities.add(new SimpleGrantedAuthority(this.rolePrefix + gr.getUid().toUpperCase()));
			}
		}

		return authorities;
	}

	@Override
	public void setRolePrefix(final String rolePrefix)
	{
		this.rolePrefix = rolePrefix;
	}
}
