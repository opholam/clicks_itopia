/**
 *
 */
package de.hybris.clicks.core.custom.service.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.TaxValue;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 * 
 */
public class ExtCalculationService extends DefaultCalculationService
{
	private static final Logger LOG = Logger.getLogger(ExtCalculationService.class);
	private OrderRequiresCalculationStrategy orderRequiresCalStrategy;

	private CommonI18NService commonI18Service;

	@Override
	protected void calculateTotals(final AbstractOrderModel order, final boolean recalculate,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws CalculationException
	{
		if (!Config.getBoolean("calculate.delivery.tax", true))
		{
			super.calculateTotals(order, recalculate, taxValueMap);
			return;
		}
		if (recalculate || orderRequiresCalStrategy.requiresCalculation(order))
		{
			final CurrencyModel curr = order.getCurrency();
			final int digits = curr.getDigits().intValue();
			// subtotal
			final double subtotal = order.getSubtotal().doubleValue();
			// discounts

			final double totalDiscounts = calculateDiscountValues(order, recalculate);
			final double roundedTotalDiscounts = commonI18Service.roundCurrency(totalDiscounts, digits);
			order.setTotalDiscounts(Double.valueOf(roundedTotalDiscounts));
			// set total
			final double total = subtotal + order.getPaymentCost().doubleValue() + order.getDeliveryCost().doubleValue()
					- roundedTotalDiscounts;
			final double totalRounded = commonI18Service.roundCurrency(total, digits);
			order.setTotalPrice(Double.valueOf(totalRounded));
			// taxes

			double totalTaxes = calculateTotalTaxValues(//
					order, recalculate, //
					digits, //
					1, //
					taxValueMap);//
			try
			{
				final double deliveryTaxRate = Config.getDouble("delivery.cost.tax.rate.percent", .14);
				final double deliveryTax = null != order.getDeliveryCost() ? order.getDeliveryCost().doubleValue() * deliveryTaxRate
						/ (1 + deliveryTaxRate) : 0;
				totalTaxes = totalTaxes + deliveryTax;
			}
			catch (final Exception e)
			{
				LOG.error("Error in ExtCalculationService class at calculateTotals() method : " + e.getMessage());
			}
			final double totalRoundedTaxes = commonI18Service.roundCurrency(totalTaxes, digits);
			order.setTotalTax(Double.valueOf(totalRoundedTaxes));
			getModelService().save(order);
			setCalculatedStatus(order);
		}

	}

	private void setCalculatedStatus(final AbstractOrderModel order)
	{
		order.setCalculated(Boolean.TRUE);
		getModelService().save(order);
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				entry.setCalculated(Boolean.TRUE);
			}
			getModelService().saveAll(entries);
		}
	}

	/**
	 * @return the orderRequiresCalStrategy
	 */
	public OrderRequiresCalculationStrategy getOrderRequiresCalStrategy()
	{
		return orderRequiresCalStrategy;
	}

	/**
	 * @param orderRequiresCalStrategy
	 *           the orderRequiresCalStrategy to set
	 */
	public void setOrderRequiresCalStrategy(final OrderRequiresCalculationStrategy orderRequiresCalStrategy)
	{
		this.orderRequiresCalStrategy = orderRequiresCalStrategy;
	}

	/**
	 * @return the commonI18Service
	 */
	public CommonI18NService getCommonI18Service()
	{
		return commonI18Service;
	}

	/**
	 * @param commonI18Service
	 *           the commonI18Service to set
	 */
	public void setCommonI18Service(final CommonI18NService commonI18Service)
	{
		this.commonI18Service = commonI18Service;
	}


}
