/**
 *
 */
package de.hybris.clicks.core.custom.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */
public class CustomDefaultUserDao extends DefaultUserDao
{

	private static final Logger LOG = Logger.getLogger(CustomDefaultUserDao.class);

	public CustomDefaultUserDao()
	{
		super();
	}

	@Override
	public UserModel findUserByUID(final String uid)
	{
		UserModel model = super.findUserByUID(uid);
		if (null == model)
		{
			try
			{
				String query = "";
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put("uid", uid);
				query = "select {PK} from {user} where lower({uid})=lower(?uid)";
				final SearchResultImpl<UserModel> searchResult = (SearchResultImpl) getFlexibleSearchService().search(query, params);
				final List<UserModel> result = searchResult.getResult();
				if (CollectionUtils.isNotEmpty(result))
				{
					model = result.get(0);
				}
			}
			catch (final Exception e)
			{
				LOG.error(e);
			}
		}
		return model;
	}
}
