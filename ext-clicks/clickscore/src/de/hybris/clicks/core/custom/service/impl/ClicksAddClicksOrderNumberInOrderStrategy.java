/**
 *
 */
package de.hybris.clicks.core.custom.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.numberseries.NumberSeriesManager;
import de.hybris.platform.order.strategies.SubmitOrderStrategy;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author manish.bhargava
 *
 */
public class ClicksAddClicksOrderNumberInOrderStrategy implements SubmitOrderStrategy
{

	@Resource
	private ModelService modelService;
	private PersistentKeyGenerator keyTillGenerator;
	private PersistentKeyGenerator keyTxnGenerator;
	private static final Logger LOG = Logger.getLogger(ClicksAddClicksOrderNumberInOrderStrategy.class);


	/**
	 * @return the keyTillGenerator
	 */
	public PersistentKeyGenerator getKeyTillGenerator()
	{
		return keyTillGenerator;
	}



	/**
	 * @param keyTillGenerator
	 *           the keyTillGenerator to set
	 */
	public void setKeyTillGenerator(final PersistentKeyGenerator keyTillGenerator)
	{
		this.keyTillGenerator = keyTillGenerator;
	}



	/**
	 * @return the keyTxnGenerator
	 */
	public PersistentKeyGenerator getKeyTxnGenerator()
	{
		return keyTxnGenerator;
	}



	/**
	 * @param keyTxnGenerator
	 *           the keyTxnGenerator to set
	 */
	public void setKeyTxnGenerator(final PersistentKeyGenerator keyTxnGenerator)
	{
		this.keyTxnGenerator = keyTxnGenerator;
	}

	/* This method will generate CicksOrderCode which follows {DATE}-{TILL}-{TRANSACTION} i.e. YYMMDD-001-0001 pattern */
	@Override
	public void submitOrder(final OrderModel order)
	{
		final String pattern = "yyMMdd";
		final SimpleDateFormat format = new SimpleDateFormat(pattern);
		final String date = format.format(new Date());
		String currentTillValue = "";
		final NumberSeriesManager nsm = NumberSeriesManager.getInstance();
		getKeyTxnGenerator().setKey(date + "_" + Config.getParameter("keygen.order.code.txn.name"));
		String generatedTxnValue = (String) getKeyTxnGenerator().generate();
		/*
		 * This condition will reset Transaction key value when it exceeds 9999. This condition will also handle server
		 * startup buffer condition.
		 */
		if (Integer.parseInt(generatedTxnValue) > 9999)
		{
			getKeyTxnGenerator().setStart("0001");
			getKeyTxnGenerator().reset();
			generatedTxnValue = (String) getKeyTxnGenerator().generate();
		}

		LOG.info("++++++++++++++++++++Current TXN value is: " + generatedTxnValue);

		if (Integer.parseInt(generatedTxnValue) == Integer.parseInt(Config.getParameter("keygen.order.code.txn.start")))
		{
			getKeyTillGenerator().setKey(date + "_" + Config.getParameter("keygen.order.code.till.name"));
			//	getKeyTillGenerator().setStart("000");
			//	getKeyTillGenerator().reset();
			getKeyTillGenerator().generate();
		}
		try
		{
			currentTillValue = nsm.getNumberSeries(date + "_" + Config.getParameter("keygen.order.code.till.name")).getFormatted(3);
		}
		catch (final Exception e)
		{
			getKeyTillGenerator().setKey(date + "_" + Config.getParameter("keygen.order.code.till.name"));
			getKeyTillGenerator().setStart("000");
			//	getKeyTillGenerator().reset();
			getKeyTillGenerator().generate();
			currentTillValue = nsm.getNumberSeries(date + "_" + Config.getParameter("keygen.order.code.till.name")).getFormatted(3);
		}
		/*
		 * This condition will reset Till key value when it equals or exceeds 999.
		 */
		if (Integer.parseInt(currentTillValue) > 999)
		{
			getKeyTillGenerator().setStart("000");
			getKeyTillGenerator().reset();
			currentTillValue = (String) getKeyTillGenerator().generate();
		}
		LOG.info("++++++++++++++++++++Current TILL value is: " + currentTillValue);

		final String generatedOrderNumber = getFormattedOrderNumber(date, generatedTxnValue, currentTillValue);
		order.setClicksOrderCode(generatedOrderNumber);
		LOG.info("Generated order number is: " + generatedOrderNumber);
		modelService.save(order);
		modelService.refresh(order);
	}



	/**
	 * @param date
	 * @param generatedTxnValue
	 * @param currentTillValue
	 * @return Formatted String
	 */
	private String getFormattedOrderNumber(final String date, final String generatedTxnValue, final String currentTillValue)
	{
		final StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(date).append("-").append(currentTillValue).append("-").append(generatedTxnValue);
		return strBuilder.toString();
	}
}
