/**
 *
 */
package de.hybris.clicks.core.custom.service.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author manikandan.gopalan
 *
 */
public class ClicksUserManager extends UserManager
{
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public Collection<? extends User> findUsers(final ComposedType type, final String uid, final String name,
			final String description, final boolean disableRestrictions)
	{
		if (StringUtils.isNotEmpty(uid))
		{
			final Collection users = new ArrayList();
			UserModel userModel = null;
			try
			{
				userModel = userService.getUserForUID(uid);
				if (userModel != null)
				{
					final User user = modelService.getSource(userModel);
					if (null != user)
					{
						users.add(user);
						return users;
					}
				}
			}
			catch (final Exception Exception)
			{
				//
			}

		}
		return super.findUsers(type, uid, name, description, disableRestrictions);
	}
}
