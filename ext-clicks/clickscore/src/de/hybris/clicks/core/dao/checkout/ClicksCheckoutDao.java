/**
 *
 */
package de.hybris.clicks.core.dao.checkout;

import de.hybris.clicks.core.model.DeliveryAreaModel;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class ClicksCheckoutDao
{
	static Logger log = Logger.getLogger(ClicksCheckoutDao.class.getName());
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;

	public AddressData getDeliveryAreaDetails(final String postcode)
	{
		AddressData addressData = null;
		boolean isDeliverable = false;
		DeliveryAreaModel deliveryAreaModel = modelService.create(DeliveryAreaModel.class);
		List<DeliveryAreaModel> deliveryAreaModelList = new ArrayList<DeliveryAreaModel>();
		deliveryAreaModel.setPostalCode(postcode);
		try
		{
			deliveryAreaModelList = flexibleSearchService.getModelsByExample(deliveryAreaModel);
			if (deliveryAreaModelList.size() > 0)
			{
				isDeliverable = true;
				for (final DeliveryAreaModel deliveryArea : deliveryAreaModelList)
				{
					addressData = new AddressData();
					addressData = populateAddressData(deliveryArea, addressData);
				}
			}
		}
		catch (final Exception e)
		{
			log.error("Exception while fetching DeliveryArea using Postcode -- > " + e);
		}

		if (!isDeliverable && Config.getBoolean("delivery.area.searchby.suburb", false))
		{
			deliveryAreaModel = modelService.create(DeliveryAreaModel.class);
			deliveryAreaModel.setSuburb(postcode);
			try
			{
				deliveryAreaModelList = flexibleSearchService.getModelsByExample(deliveryAreaModel);
				if (deliveryAreaModelList.size() > 0)
				{
					for (final DeliveryAreaModel deliveryArea : deliveryAreaModelList)
					{
						addressData = new AddressData();
						addressData = populateAddressData(deliveryArea, addressData);
					}
				}
			}
			catch (final Exception e2)
			{

				log.error("Exception while fetching DeliveryArea using suberb-- > " + e2);
			}
		}
		return addressData;
	}

	/**
	 * @param deliveryArea
	 * @param addressData
	 * @return
	 */
	private AddressData populateAddressData(final DeliveryAreaModel deliveryArea, final AddressData addressData)
	{

		if (null != deliveryArea.getPostalCode())
		{
			addressData.setPostalCode(deliveryArea.getPostalCode());
		}
		if (null != deliveryArea.getSuburb())
		{
			addressData.setSuburb(deliveryArea.getSuburb());
		}
		if (null != deliveryArea.getCountry())
		{
			final CountryData countryData = new CountryData();
			countryData.setName(deliveryArea.getCountry().getName());
			countryData.setIsocode(deliveryArea.getCountry().getIsocode());
			addressData.setCountry(countryData);
		}

		return addressData;
	}

	/**
	 * @param postcode
	 * @return
	 */
	public SearchResult<DeliveryAreaModel> fetchResults(final String postcode)
	{
		try
		{
			final Map<String, Object> map = new HashMap<String, Object>();
			map.put(DeliveryAreaModel.POSTALCODE, postcode);
			final String query = "select {PK} from {DeliveryArea} where {postalCode} = ?postalCode";
			final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query, map);
			final SearchResult<DeliveryAreaModel> result = flexibleSearchService.search(searchQuery);
			return result;
		}
		catch (final Exception e)
		{
			log.error("Error at fetchResults() method" + e.getMessage());
			return null;
		}
	}

	/**
	 * @param provinceList
	 * @return
	 */
	public Collection<ProvinceData> getProvinceList(final Collection<ProvinceData> provinceList)
	{
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

}
