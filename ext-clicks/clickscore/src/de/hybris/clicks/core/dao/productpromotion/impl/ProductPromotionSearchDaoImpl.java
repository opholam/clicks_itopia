/**
 *
 */
package de.hybris.clicks.core.dao.productpromotion.impl;

import de.hybris.clicks.core.dao.productpromotion.ProductPromotionSearchDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author mbhargava
 *
 */
public class ProductPromotionSearchDaoImpl implements ProductPromotionSearchDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.core.dao.productpromotion.ProductPromotionSearchDao#getPromotionsForCategory(de.hybris.platform
	 * .category.model.CategoryModel)
	 */
	@Override
	public List<ProductModel> getPromotionsForCategory(final CategoryModel category)
	{

		String query = "";
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("pk", category.getPk());
		query = "select {pp:target} as Promotion from {ProductPromotionRelation as pp} where {pp:source} in ({{select {cp:target} from {CategoryProductRelation as cp} where {cp:source} = ?pk }}) GROUP BY {pp:target} order by count({pp:target}) desc";
		final SearchResultImpl<ProductPromotionModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<ProductPromotionModel> result = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(result))
		{
			return (List<ProductModel>) result.get(0).getProducts();
		}
		else
		{
			return Collections.EMPTY_LIST;
		}
	}

}
