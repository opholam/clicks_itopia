/**
 *
 */
package de.hybris.clicks.core.dao.order;

import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

import java.util.HashMap;
import java.util.Map;


/**
 * @author manish.bhargava
 *
 */
public class ClicksDefaultCustomerAccountDao extends DefaultCustomerAccountDao
{
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao#findOrderByCustomerAndCodeAndStore
	 * (de.hybris.platform.core.model.user.CustomerModel, java.lang.String, de.hybris.platform.store.BaseStoreModel)
	 */
	@Override
	public OrderModel findOrderByCustomerAndCodeAndStore(final CustomerModel customerModel, final String code,
			final BaseStoreModel store)
	{
		ServicesUtil.validateParameterNotNull(customerModel, "Customer must not be null");
		ServicesUtil.validateParameterNotNull(code, "Code must not be null");
		ServicesUtil.validateParameterNotNull(store, "Store must not be null");
		final Map queryParams = new HashMap();
		queryParams.put("customer", customerModel);
		queryParams.put("code", code);
		queryParams.put("store", store);
		final OrderModel result = (OrderModel) getFlexibleSearchService()
				.searchUnique(
						new FlexibleSearchQuery(
								"SELECT {pk}, {creationtime}, {code}, {clicksOrderCode} FROM {Order} WHERE ({clicksOrderCode} = ?code OR {code} = ?code) AND {versionID} IS NULL AND {user} = ?customer AND {store} = ?store",
								queryParams));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao#findOrderByCodeAndStore(java.lang
	 * .String, de.hybris.platform.store.BaseStoreModel)
	 */
	@Override
	public OrderModel findOrderByCodeAndStore(final String code, final BaseStoreModel store)
	{
		ServicesUtil.validateParameterNotNull(code, "Code must not be null");
		ServicesUtil.validateParameterNotNull(store, "Store must not be null");
		final Map queryParams = new HashMap();
		queryParams.put("code", code);
		queryParams.put("store", store);
		final OrderModel result = (OrderModel) getFlexibleSearchService()
				.searchUnique(
						new FlexibleSearchQuery(
								"SELECT {pk}, {creationtime}, {code} FROM {Order} WHERE ({clicksOrderCode} = ?code OR {code} = ?code) AND {versionID} IS NULL AND {store} = ?store",
								queryParams));
		return result;
	}
}
