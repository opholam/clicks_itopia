/**
 *
 */
package de.hybris.clicks.core.dao.productpromotion;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * @author mbhargava
 *
 */
public interface ProductPromotionSearchDao
{
	public List<ProductModel> getPromotionsForCategory(CategoryModel category);
}
