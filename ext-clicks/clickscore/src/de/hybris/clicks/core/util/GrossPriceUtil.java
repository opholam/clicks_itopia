/**
 *
 */
package de.hybris.clicks.core.util;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.ProductFixedPricePromotionModel;
import de.hybris.platform.promotions.model.ProductPercentageDiscountPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionPriceRowModel;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author praveen.babu
 *
 */
public class GrossPriceUtil
{

	protected static final Logger LOG = Logger.getLogger(GrossPriceUtil.class);


	/**
	 * @param productModel
	 * @param currentProductPrice
	 * @return
	 */
	public static Double getGrossPrice(final ProductModel productModel, final BigDecimal currentProductPrice)
	{
		double netPrice = null != currentProductPrice ? currentProductPrice.doubleValue() : 0.0;
		double grossValue = 0.0;
		double tax = 0.0;
		LOG.info(productModel.getCode());
		if (0.0 == netPrice)
		{
			for (final PriceRowModel prm : productModel.getEurope1Prices())
			{
				netPrice = prm.getPrice().doubleValue() / prm.getUnitFactor().doubleValue();
			}
		}
		//change logic by adding tax first and then calculate promo
		tax = getTaxValue(productModel, netPrice);
		netPrice = netPrice + tax;
		//Apply promotions if any
		grossValue = getPromotionAppliedNetPrice(productModel, netPrice).doubleValue();
		return new Double(grossValue);
	}


	public static Double getGrossPrice(final ProductModel productModel, final boolean isPromotionApplied,
			final BigDecimal currentProductPrice)
	{
		double netPrice = null != currentProductPrice ? currentProductPrice.doubleValue() : 0.0;
		double grossValue = 0.0;
		double tax = 0.0;
		if (0.0 == netPrice)
		{
			for (final PriceRowModel prm : productModel.getEurope1Prices())
			{
				netPrice = prm.getPrice().doubleValue() / prm.getUnitFactor().doubleValue();
			}
		}
		tax = getTaxValue(productModel, netPrice);
		netPrice = netPrice + tax;
		//Apply promotions if any
		if (isPromotionApplied)
		{
			grossValue = getPromotionAppliedNetPrice(productModel, netPrice).doubleValue();
		}
		return new Double(grossValue);
	}


	public static Double getGrossPrice(final AbstractPromotionModel promotionModel, final BigDecimal currentProductPrice)
	{
		final double netPrice = null != currentProductPrice ? currentProductPrice.doubleValue() : 0.0;
		double grossValue = 0.0;
		if (promotionModel instanceof ProductPercentageDiscountPromotionModel)
		{
			grossValue = processPercentageDiscountPromotion((ProductPromotionModel) promotionModel, netPrice);
		}
		if (promotionModel instanceof ProductFixedPricePromotionModel)
		{
			grossValue = processFixedPricePromotion((ProductPromotionModel) promotionModel, netPrice);
		}
		return new Double(grossValue);
	}


	public static double getTaxValue(final ProductModel productModel, final double netPrice)
	{
		double tax = 0.0;
		if (Config.getBoolean("product.tax.price.inclusive", true))
		{
			return tax;
		}
		if (!productModel.getEurope1Taxes().isEmpty())
		{
			final Collection<TaxRowModel> taxRowModelList = productModel.getEurope1Taxes();
			for (final TaxRowModel trm : taxRowModelList)
			{
				//tax = trm.getValue();

				if (trm.getAbsolute().booleanValue())
				{
					tax = trm.getTax().getValue().doubleValue();
				}
				else
				{
					tax = (netPrice * trm.getTax().getValue().doubleValue() / 100);
				}
			}
		}
		return tax;

	}

	public static double getTaxValueWithGrossPrice(final ProductModel productModel, final double grossprice)
	{
		double tax = 0.0;
		if (Config.getBoolean("product.tax.price.inclusive", true))
		{
			return tax;
		}
		if (!productModel.getEurope1Taxes().isEmpty())
		{
			final Collection<TaxRowModel> taxRowModelList = productModel.getEurope1Taxes();
			for (final TaxRowModel trm : taxRowModelList)
			{
				//tax = trm.getValue();

				if (trm.getAbsolute().booleanValue())
				{
					tax = trm.getTax().getValue().doubleValue();
				}
				else
				{
					tax = grossprice - ((grossprice * 100) / (100 + trm.getTax().getValue().doubleValue()));
				}
			}
		}
		return tax;

	}

	public static Double getPromotionAppliedNetPrice(final ProductModel productModel, final double netPrice)
	{
		double gross = 0.0;
		final String version = Config.getParameter("grossPriceUtil.catalog.version");
		//To apply promotions if any
		final Collection<CategoryModel> superCategoryList = productModel.getSupercategories();
		if (superCategoryList != null && superCategoryList.size() > 0)
		{
			for (final CategoryModel categoryModel : superCategoryList)
			{
				if (version.equals(categoryModel.getCatalogVersion().getVersion()))
				{
					gross = validateAndCalculatePromotionAppliedPrice(categoryModel, netPrice);
				}
			}
		}
		gross = validateAndCalculatePromotionAppliedPrice(productModel, netPrice);
		return new Double(gross);
	}

	public static boolean applyRestrictions(final ProductPromotionModel productPromotionModel)
	{
		boolean isApplyRestriction = false;
		final Calendar currentDate = Calendar.getInstance();
		final Calendar startDate = Calendar.getInstance();
		final Calendar endDate = Calendar.getInstance();
		if (productPromotionModel.getStartDate() != null)
		{
			startDate.setTime(productPromotionModel.getStartDate());
		}
		if (productPromotionModel.getEndDate() != null)
		{
			endDate.setTime(productPromotionModel.getEndDate());
		}
		if (startDate.before(currentDate) && endDate.after(currentDate))
		{
			isApplyRestriction = true;
		}
		return isApplyRestriction;
	}


	public static double validateAndCalculatePromotionAppliedPrice(final Object object, final double netPrice)
	{
		double gross = netPrice;
		Collection<ProductPromotionModel> productPromotionModelList = null;
		if (object instanceof CategoryModel)
		{
			final CategoryModel categoryModel = (CategoryModel) object;
			productPromotionModelList = categoryModel.getPromotions();
		}
		if (object instanceof ProductModel)
		{
			final ProductModel productModel = (ProductModel) object;
			productPromotionModelList = productModel.getPromotions();
		}
		if (productPromotionModelList != null && productPromotionModelList.size() > 0)
		{
			for (final ProductPromotionModel productPromotionModel : productPromotionModelList)
			{
				if (Boolean.TRUE.equals(productPromotionModel.getEnabled()))
				{
					if (applyRestrictions(productPromotionModel))
					{
						if (productPromotionModel instanceof ProductPercentageDiscountPromotionModel)
						{
							gross = processPercentageDiscountPromotion(productPromotionModel, netPrice);
						}
						if (productPromotionModel instanceof ProductFixedPricePromotionModel)
						{
							gross = processFixedPricePromotion(productPromotionModel, netPrice);
						}
					}
				}
			}
		}

		return gross;
	}

	public static double processPercentageDiscountPromotion(final ProductPromotionModel productPromotionModel, double netPrice)
	{
		double discount = 0.0;

		final ProductPercentageDiscountPromotionModel productPercentageDiscountPromotionModel = (ProductPercentageDiscountPromotionModel) productPromotionModel;
		final Double percentageDiscount = productPercentageDiscountPromotionModel.getPercentageDiscount();
		if (null != percentageDiscount && percentageDiscount > 0)
		{
			discount = netPrice * (percentageDiscount.doubleValue() / 100);
			netPrice = netPrice - discount;
			return netPrice;
		}
		return 0.0;
	}

	public static double processFixedPricePromotion(final ProductPromotionModel productPromotionModel, double netPrice)
	{
		final ProductFixedPricePromotionModel productFixedPricePromotionModel = (ProductFixedPricePromotionModel) productPromotionModel;
		final Collection<PromotionPriceRowModel> promotionPriceRowModelList = productFixedPricePromotionModel
				.getProductFixedUnitPrice();
		if (CollectionUtils.isNotEmpty(promotionPriceRowModelList))
		{
			for (final PromotionPriceRowModel promotionPriceRowModel : promotionPriceRowModelList)
			{
				netPrice = promotionPriceRowModel.getPrice().doubleValue();
			}
			return netPrice;
		}
		return 0.0;
	}


}
