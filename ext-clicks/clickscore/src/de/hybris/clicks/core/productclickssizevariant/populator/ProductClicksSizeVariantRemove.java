/**
 *
 */
package de.hybris.clicks.core.productclickssizevariant.populator;

import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData.NewProduct;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */

public class ProductClicksSizeVariantRemove
{
	static Logger log = Logger.getLogger(ProductClicksSizeVariantRemove.class.getName());
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;


	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private MediaService mediaService;

	public void removeProductSizeVariant(final ProductSizeVariantData source)
	{

		log.info("++++++++++++++++++++++++++++++++Entered into ProductClicksSizeVariantPopulator++++++++++++++++++++++++++++++++");
		final String query = "SELECT {pk} FROM {Media} WHERE {code} LIKE ?code";
		final Map<String, Object> params = new HashMap<String, Object>();
		for (final Iterator<NewProduct> iterator = source.getNewProduct().iterator(); iterator.hasNext();)
		{
			final NewProduct newProduct = iterator.next();
			params.put("code", "%" + newProduct.getBarcode() + "%");
			//flexiQuery.addQueryParameter("code", "%" + newProduct.getBarcode() + "%");
			final SearchResult<MediaModel> result = flexibleSearchService.search(query, params);
			if (null != result && result.getCount() > 0)
			{
				for (final MediaModel prodModel : result.getResult())
				{
					if (null != prodModel.getMediaContainer())
					{
						modelService.remove(prodModel.getMediaContainer());
					}
					modelService.remove(prodModel);
				}
			}
		}
	}
}
