/**
 *
 */
package de.hybris.clicks.core.productclickssizevariant.populator;

import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.clicks.core.model.ListItemsSizeVariantModel;
import de.hybris.clicks.core.model.NutrientItemsModel;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData.NewProduct;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData.NewProduct.Images.Path;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData.NewProduct.ProductData.ListItems;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData.NewProduct.ProductData.NutritionalTables.NutritionalFacts;
import de.hybris.clicks.core.productclickssizevariant.schemas.ProductSizeVariantData.NewProduct.ProductData.NutritionalTables.NutritionalFacts.NutrientItem;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */
public class ProductClicksSizeVariantPopulator
{
	static Logger log = Logger.getLogger(ProductClicksSizeVariantPopulator.class.getName());
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;


	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private MediaService mediaService;

	@Resource
	private MediaContainerService mediaContainerService;

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	String newID;

	private static final String HIRTANDCARTERIMAGEPATH = "hirtAndCarterXmlFileAndImagePath";

	public List<String> populateAndSaveProductSizeVariant(final ProductSizeVariantData source)
	{

		log.info("++++++++++++++++++++++++++++++++Entered into ProductClicksSizeVariantPopulator++++++++++++++++++++++++++++++++");
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
		final String query = "SELECT {pk} FROM {ClicksApparelSizeVariant} WHERE {code} = ?code and {catalogVersion} = ?catalogVersion";
		final Map<String, Object> params = new HashMap<String, Object>();
		final List<String> skuIdList = new ArrayList<String>();
		for (final Iterator<NewProduct> iterator = source.getNewProduct().iterator(); iterator.hasNext();)
		{
			ClicksApparelSizeVariantModel clicksApparelSizeVariantModel = null;
			final NewProduct newProduct = iterator.next();
			params.put(ClicksApparelSizeVariantModel.CODE, newProduct.getSkuID());
			params.put(ClicksApparelSizeVariantModel.CATALOGVERSION, catalogVersionModel);
			final SearchResult<ClicksApparelSizeVariantModel> result = flexibleSearchService.search(query, params);
			if (null != result && result.getCount() > 0)
			{
				for (final ClicksApparelSizeVariantModel prodModel : result.getResult())
				{
					if ("Staged".equalsIgnoreCase(prodModel.getCatalogVersion().getVersion()))
					{
						try
						{
							clicksApparelSizeVariantModel = prodModel;
							populateItem(newProduct, clicksApparelSizeVariantModel);
							skuIdList.add(newProduct.getSkuID());
						}
						catch (final Exception e)
						{
							log.error("Prodcut errored with code: " + newProduct.getSkuID() + " " + e.getMessage());
						}
					}
					break;
				}
			}
			else
			{
				log.error("Prodcut skipped with code: " + newProduct.getSkuID());
			}
		}
		return skuIdList;
	}

	private void populateItem(final NewProduct newProduct, final ClicksApparelSizeVariantModel sizeModel)
	{
		log.info("++++++++++Populating ClicksApparelSizeVariant for Barcode: " + newProduct.getBarcode() + "+++++++++++++++++++++");

		if (StringUtils.isNotBlank(newProduct.getBarcode()))
		{
			sizeModel.setBarcode(newProduct.getBarcode());
		}

		if (StringUtils.isNotBlank(newProduct.getManufacturer()))
		{
			sizeModel.setManufacturerName(newProduct.getManufacturer());
		}

		if (StringUtils.isNotBlank(newProduct.getManufacturerAddress()))
		{
			sizeModel.setManufacturerAddress(newProduct.getManufacturerAddress());
		}

		if (StringUtils.isNotBlank(newProduct.getManufacturerPhone()))
		{
			sizeModel.setManufacturerPhone(newProduct.getManufacturerPhone());
		}

		if (StringUtils.isNotBlank(newProduct.getManufacturerEmail()))
		{
			sizeModel.setManufacturerEmail(newProduct.getManufacturerEmail());
		}

		if (StringUtils.isNotBlank(newProduct.getManufacturerURL()))
		{
			sizeModel.setManufacturerURL(newProduct.getManufacturerURL());
		}

		if (StringUtils.isNotBlank(newProduct.getMarketingDescription()))
		{
			sizeModel.setMarketingDescription(newProduct.getMarketingDescription());
		}

		if (StringUtils.isNotBlank(newProduct.getDistributer()))
		{
			sizeModel.setDistributor(newProduct.getDistributer());
		}

		if (StringUtils.isNotBlank(newProduct.getDistributorAddress()))
		{
			sizeModel.setDistributorAddress(newProduct.getDistributorAddress());
		}

		if (StringUtils.isNotBlank(newProduct.getDistributorPhone()))
		{
			sizeModel.setDistributorPhone(newProduct.getDistributorPhone());
		}

		if (StringUtils.isNotBlank(newProduct.getDistributorEmail()))
		{
			sizeModel.setDistributorEmail(newProduct.getDistributorEmail());
		}

		if (StringUtils.isNotBlank(newProduct.getDistributorURL()))
		{
			sizeModel.setDistributorURL(newProduct.getDistributorURL());
		}

		if (StringUtils.isNotBlank(newProduct.getOtherDiscription()))
		{
			sizeModel.setOtherDescription(newProduct.getOtherDiscription());
		}


		if (StringUtils.isNotBlank(newProduct.getBrand()))
		{
			sizeModel.setBrandName(newProduct.getBrand());
		}


		if (StringUtils.isNotBlank(newProduct.getSubDept()))
		{
			sizeModel.setSubDept(newProduct.getSubDept());
		}

		if (StringUtils.isNotBlank(newProduct.getSkuClass()))
		{
			sizeModel.setSkuClass((newProduct.getSkuClass()));
		}

		sizeModel.setLastUpdated(new Date());
		if (null != newProduct.getProductData())
		{

			if (StringUtils.isNotBlank(newProduct.getProductData().getIngredients()))
			{
				sizeModel.setIngredients(newProduct.getProductData().getIngredients());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getSequenceId()))
			{
				sizeModel.setSequenceId(Long.valueOf(newProduct.getProductData().getSequenceId()));
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getDescription()))
			{
				sizeModel.setDescription(newProduct.getProductData().getDescription());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getStorageInstructions()))
			{
				sizeModel.setStorageInstructions(newProduct.getProductData().getStorageInstructions());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getWarnings()))
			{
				sizeModel.setWarnings(newProduct.getProductData().getWarnings());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getCountryOfOrigin()))
			{
				sizeModel.setCountryOfOrigin(newProduct.getProductData().getCountryOfOrigin());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getEndorsements()))
			{
				sizeModel.setEndorsements(newProduct.getProductData().getEndorsements());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getFlavor()))
			{
				sizeModel.setFlavor(newProduct.getProductData().getFlavor());
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getUsageInstructions()))
			{
				sizeModel.setUsageInstructions(newProduct.getProductData().getUsageInstructions());
			}

			if (null != newProduct.getProductData().getListItems())
			{

				final List<ListItemsSizeVariantModel> listItemModelList = new ArrayList<ListItemsSizeVariantModel>();
				ListItemsSizeVariantModel listItemModel = null;

				for (final ListItems listItems : newProduct.getProductData().getListItems())
				{
					if (null != listItems.getItem() && listItems.getItem().size() > 0)
					{
						final List<String> strList = new ArrayList<String>();
						listItemModel = modelService.create(ListItemsSizeVariantModel.class);
						listItemModel.setName(listItems.getName());
						for (final String string : listItems.getItem())
						{
							if (null != string && StringUtils.isNotBlank(string.trim()))
							{
								strList.add(string);
							}
						}
						listItemModel.setItemValList(strList);
						listItemModelList.add(listItemModel);
					}
				}
				sizeModel.setListItemsForProduct(listItemModelList);
			}

			if (StringUtils.isNotBlank(newProduct.getProductData().getPackSize()))
			{
				sizeModel.setPackSize(newProduct.getProductData().getPackSize());
			}


			if (StringUtils.isNotBlank(newProduct.getProductData().getQtyInPackage()))
			{
				sizeModel.setQtyInPackage(newProduct.getProductData().getQtyInPackage());
			}

			if (null != newProduct.getProductData().getNutritionalTables()
					&& null != newProduct.getProductData().getNutritionalTables().getNutritionalFacts())
			{

				final NutritionalFacts nutritionalFacts = newProduct.getProductData().getNutritionalTables().getNutritionalFacts();
				if (null != nutritionalFacts.getServingSize())
				{
					if (StringUtils.isNotBlank(nutritionalFacts.getServingSize().getMeasure()))
					{
						sizeModel.setMeasure(nutritionalFacts.getServingSize().getMeasure());
					}

					if (StringUtils.isNotBlank(nutritionalFacts.getServingSize().getServingSizeFull()))
					{
						sizeModel.setServingSizeFull(nutritionalFacts.getServingSize().getServingSizeFull());
					}

					if (StringUtils.isNotBlank(nutritionalFacts.getServingSize().getNumberOfServings()))
					{
						sizeModel.setNumberOfServings(nutritionalFacts.getServingSize().getNumberOfServings());
					}
				}

				if (null != nutritionalFacts.getTableHeadings() && null != nutritionalFacts.getTableHeadings().getTableHeading()
						&& nutritionalFacts.getTableHeadings().getTableHeading().size() > 0)
				{
					final List<String> tableHeadings = new ArrayList<String>();
					for (final String str : nutritionalFacts.getTableHeadings().getTableHeading())
					{
						if (null != str && StringUtils.isNotBlank(str.trim()))
						{
							tableHeadings.add(str);
						}
					}
					sizeModel.setTableHeadingList(tableHeadings);
				}

				if (null != nutritionalFacts.getNutrientItem() && nutritionalFacts.getNutrientItem().size() > 0)
				{

					final List<NutrientItemsModel> nutrientItemList = new ArrayList<NutrientItemsModel>();
					NutrientItemsModel nutrientItems = null;
					for (final NutrientItem item : nutritionalFacts.getNutrientItem())
					{
						nutrientItems = modelService.create(NutrientItemsModel.class);
						if (StringUtils.isNotBlank(item.getItemName()))
						{
							nutrientItems.setNutrientItemName(item.getItemName());
						}

						if (StringUtils.isNotBlank(item.getItemUOM()))
						{
							nutrientItems.setNutrientItemUOM(item.getItemUOM());
						}
						if (null != item.getItemValues() && null != item.getItemValues().getValue()
								&& item.getItemValues().getValue().size() > 0)
						{
							final List<String> nutrientValList = new ArrayList<String>();
							for (final String string : item.getItemValues().getValue())
							{
								if (null != string && StringUtils.isNotEmpty(string.trim()))
								{
									nutrientValList.add(string);
								}
							}
							nutrientItems.setItemValueList(nutrientValList);
						}
						nutrientItemList.add(nutrientItems);
					}
					if (nutrientItemList.size() > 0)
					{
						sizeModel.setNutrientItemList(nutrientItemList);
					}
				}

			}
		}
		populateMediaModelAndSaveSizeModel(newProduct.getImages().getPath(), sizeModel);
	}

	private void populateMediaModelAndSaveSizeModel(final List<Path> path, final ClicksApparelSizeVariantModel sizeModel)
	{
		log.info("++++++++++Inserting media and mediacontainer for ClicksApparelSizeVariant+++++++++++++++++++++");

		boolean thumbNailMediaAdded = false;
		boolean pictureMediaAdded = false;
		final Set<MediaContainerModel> hashsetForMediaContainer = new HashSet<MediaContainerModel>();
		//final List<MediaContainerModel> mediaContainerList = new ArrayList<MediaContainerModel>();
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.staged.version"));
		List<MediaModel> mediaModelList = null;
		final String workingDirectory = System.getProperty("user.dir");
		final String[] temp = workingDirectory.split("bin");
		FileInputStream inputStream = null;
		MediaFolderModel mediaFolderModel = modelService.create(MediaFolderModel.class);
		mediaFolderModel.setQualifier("product-images");

		try
		{
			mediaFolderModel = flexibleSearchService.getModelByExample(mediaFolderModel);
		}
		catch (final Exception e)
		{
			log.error("Media folder not found with the qualifier: product-images");
		}


		if (null != sizeModel.getBaseProduct())
		{
			//final ProductModel styleVariantModel = sizeModel.getBaseProduct();
			for (final Path path1 : path)
			{
				MediaContainerModel mediaContainerModel = null;
				mediaModelList = new ArrayList<MediaModel>();
				final String pathVal = path1.getValue();
				final String pathName = path1.getName();
				final String clicks = "clicks_";
				final String[] pathArray = pathVal.split(";");
				MediaModel mediaModel = null;

				if (StringUtils.isNotBlank(pathName))
				{
					for (final String str : pathArray)
					{
						mediaModel = modelService.create(MediaModel.class);
						mediaModel.setCatalogVersion(catalogVersionModel);
						String code = "";
						if (str.contains("_60x"))
						{
							code = "/65Wx65H/" + clicks + str;
							mediaModel.setCode(code);
							//saveMediaModel(mediaModel, "65Wx65H", str, mediaModelList);
							try
							{
								mediaModel = flexibleSearchService.getModelByExample(mediaModel);
							}
							catch (final Exception e)
							{
								log.error("Media model not found with the code: " + code);
								if (null == mediaModel.getURL())
								{
									//mediaModel.setMime("image/png");
									mediaModel.setMediaFormat(mediaService.getFormat("65Wx65H"));
								}
							}
							if (!thumbNailMediaAdded && StringUtils.containsIgnoreCase(pathName, "Marketing_Straight"))
							{
								sizeModel.setThumbnail(mediaModel);
								thumbNailMediaAdded = true;
							}
							if (null != mediaFolderModel.getPk())
							{
								mediaModel.setFolder(mediaFolderModel);
							}
							modelService.save(mediaModel);
							final String absoluteFilePath = temp[0] + Config.getParameter(HIRTANDCARTERIMAGEPATH) + str;
							try
							{
								inputStream = new FileInputStream(absoluteFilePath);
								mediaService.setStreamForMedia(mediaModel, inputStream);
								mediaModelList.add(mediaModel);
								log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the Image:  "
										+ absoluteFilePath + "  ++++++++++++++++++++++++++++++++");
							}
							catch (final FileNotFoundException e)
							{
								log.error("Image not found at mentioned path: " + absoluteFilePath);
							}
							finally
							{
								IOUtils.closeQuietly(inputStream);
							}
						}

						else if (str.contains("_180x"))
						{
							code = "/180Wx180H/" + clicks + str;
							mediaModel.setCode(code);
							//saveMediaModel(mediaModel, "180Wx180H", str, mediaModelList);
							try
							{
								mediaModel = flexibleSearchService.getModelByExample(mediaModel);
							}
							catch (final Exception e)
							{
								log.error("Media model not found with the code: " + code);
								if (null == mediaModel.getURL())
								{
									//mediaModel.setMime("image/png");
									mediaModel.setMediaFormat(mediaService.getFormat("180Wx180H"));
								}
							}
							if (null != mediaFolderModel.getPk())
							{
								mediaModel.setFolder(mediaFolderModel);
							}
							modelService.save(mediaModel);
							final String absoluteFilePath = temp[0] + Config.getParameter(HIRTANDCARTERIMAGEPATH) + str;
							try
							{
								inputStream = new FileInputStream(absoluteFilePath);
								mediaService.setStreamForMedia(mediaModel, inputStream);
								mediaModelList.add(mediaModel);
								log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the Image:  "
										+ absoluteFilePath + "  ++++++++++++++++++++++++++++++++");
							}
							catch (final FileNotFoundException e)
							{
								log.error("Image not found at mentioned path: " + absoluteFilePath);
							}
							finally
							{
								IOUtils.closeQuietly(inputStream);
							}
						}
						else if (str.contains("_1000x"))
						{
							code = "/1200Wx1200H/" + clicks + str;
							mediaModel.setCode(code);
							//saveMediaModel(mediaModel, "1200Wx1200H", str, mediaModelList);
							try
							{
								mediaModel = flexibleSearchService.getModelByExample(mediaModel);
							}
							catch (final Exception e)
							{
								log.error("Media model not found with the code: " + code);
								if (null == mediaModel.getURL())
								{
									//mediaModel.setMime("image/png");
									mediaModel.setMediaFormat(mediaService.getFormat("1200Wx1200H"));
								}
							}
							if (null != mediaFolderModel.getPk())
							{
								mediaModel.setFolder(mediaFolderModel);
							}
							modelService.save(mediaModel);
							final String absoluteFilePath = temp[0] + Config.getParameter(HIRTANDCARTERIMAGEPATH) + str;
							try
							{
								inputStream = new FileInputStream(absoluteFilePath);
								mediaService.setStreamForMedia(mediaModel, inputStream);
								mediaModelList.add(mediaModel);
								log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the Image:  "
										+ absoluteFilePath + "  ++++++++++++++++++++++++++++++++");
							}
							catch (final FileNotFoundException e)
							{
								log.error("Image not found at mentioned path: " + absoluteFilePath);
							}
							finally
							{
								IOUtils.closeQuietly(inputStream);
							}
						}
						else if (str.contains("_28x"))
						{
							code = "/30Wx30H/" + clicks + str;
							mediaModel.setCode(code);
							//saveMediaModel(mediaModel, "30Wx30H", str, mediaModelList);
							try
							{
								mediaModel = flexibleSearchService.getModelByExample(mediaModel);
							}
							catch (final Exception e)
							{
								log.error("Media model not found with the code: " + code);
								if (null == mediaModel.getURL())
								{
									//mediaModel.setMime("image/png");
									mediaModel.setMediaFormat(mediaService.getFormat("30Wx30H"));
								}
							}
							if (null != mediaFolderModel.getPk())
							{
								mediaModel.setFolder(mediaFolderModel);
							}
							modelService.save(mediaModel);
							final String absoluteFilePath = temp[0] + Config.getParameter(HIRTANDCARTERIMAGEPATH) + str;
							try
							{
								inputStream = new FileInputStream(absoluteFilePath);
								mediaService.setStreamForMedia(mediaModel, inputStream);
								mediaModelList.add(mediaModel);
								log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the Image:  "
										+ absoluteFilePath + "  ++++++++++++++++++++++++++++++++");
							}
							catch (final FileNotFoundException e)
							{
								log.error("Image not found at mentioned path: " + absoluteFilePath);
							}
							finally
							{
								IOUtils.closeQuietly(inputStream);
							}
						}

						else if (str.contains("_500x"))
						{
							code = "/300Wx300H/" + clicks + str;
							mediaModel.setCode(code);
							//saveMediaModel(mediaModel, "300Wx300H", str, mediaModelList);
							try
							{
								mediaModel = flexibleSearchService.getModelByExample(mediaModel);
							}
							catch (final Exception e)
							{
								log.error("Media model not found with the code: " + code);
								if (null == mediaModel.getURL())
								{
									//mediaModel.setMime("image/png");
									mediaModel.setMediaFormat(mediaService.getFormat("300Wx300H"));
								}
							}
							if (null != mediaFolderModel.getPk())
							{
								mediaModel.setFolder(mediaFolderModel);
							}
							modelService.save(mediaModel);
							if (!pictureMediaAdded && StringUtils.containsIgnoreCase(pathName, "Marketing_Straight"))
							{
								sizeModel.setPicture(mediaModel);
								pictureMediaAdded = true;
							}
							final String absoluteFilePath = temp[0] + Config.getParameter(HIRTANDCARTERIMAGEPATH) + str;
							try
							{
								inputStream = new FileInputStream(absoluteFilePath);
								mediaService.setStreamForMedia(mediaModel, inputStream);
								mediaModelList.add(mediaModel);
								log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the Image:  "
										+ absoluteFilePath + "  ++++++++++++++++++++++++++++++++");
							}
							catch (final FileNotFoundException e)
							{
								log.error("Image not found at mentioned path: " + absoluteFilePath);
							}
							finally
							{
								IOUtils.closeQuietly(inputStream);
							}
						}
					}
					if (mediaModelList.size() > 0)
					{
						log.info("Number of media inserted: " + mediaModelList.size());
						final String tempPathName = pathName.replaceAll("\\s", "_");
						for (final MediaContainerModel containerModel : sizeModel.getGalleryImages())
						{
							if (null != containerModel.getQualifier() && containerModel.getQualifier().contains(tempPathName))
							{
								mediaContainerModel = containerModel;
								break;
							}
						}
						if (null != mediaContainerModel && CollectionUtils.isNotEmpty(mediaContainerModel.getMedias()))
						{//This condition is added to remove all the existing media with png format. Previously all png images have been loaded
						 // but due to performance issues all png needs to be change in jpeg format.
							final Collection<MediaModel> mediaCollectionToDelete = new ArrayList<MediaModel>();
							mediaCollectionToDelete.addAll(mediaContainerModel.getMedias());
							mediaCollectionToDelete.removeAll(mediaModelList);
							if (CollectionUtils.isNotEmpty(mediaCollectionToDelete))
							{
								modelService.removeAll(mediaCollectionToDelete);
							}
						}
						if (null == mediaContainerModel)
						{
							final String mediaContainerKey = (String) myNumberseriesGenerator.generate() + "_" + sizeModel.getCode()
									+ "_" + tempPathName + ".png";
							mediaContainerModel = modelService.create(MediaContainerModel.class);
							mediaContainerModel.setQualifier(mediaContainerKey);
							mediaContainerModel.setCatalogVersion(catalogVersionModel);
						}
						mediaContainerModel.setMedias(mediaModelList);
						modelService.save(mediaContainerModel);
						modelService.refresh(mediaContainerModel);
						//mediaContainerList.add(mediaContainerModel);
						hashsetForMediaContainer.add(mediaContainerModel);
					}
				}
			}
			final List<MediaContainerModel> mediaContainerList = new ArrayList<MediaContainerModel>(hashsetForMediaContainer);
			if (CollectionUtils.isNotEmpty(mediaContainerList))
			{
				sizeModel.setGalleryImages(mediaContainerList);
				sizeModel.setApprovalStatus(ArticleApprovalStatus.IMAGESUPLOADED);
			}
			modelService.save(sizeModel);
			modelService.refresh(sizeModel);
			//modelService.save(styleVariantModel);
			log.info("\n++++++++++ ApparelStyleVariant and ClicksApparelSizeVariant saved successfully+++++++++++++++++++++\n");
		}
		else
		{
			log.error("\n++++++++++Skipped ClicksApparelSizeVariant with sku id: " + sizeModel.getCode()
					+ " because style variant not found +++++++++++++++++++++\n");
		}
	}
	/*
	 * private void saveMediaModel(MediaModel mediaModel, final String mediaFormat, final String str, final
	 * List<MediaModel> mediaModelList) { final String workingDirectory = System.getProperty("user.dir"); final String[]
	 * temp = workingDirectory.split("bin"); FileInputStream inputStream; try { mediaModel =
	 * flexibleSearchService.getModelByExample(mediaModel); } catch (final Exception e) {
	 * log.error("Media model not found with the code: " + mediaModel.getCode()); if (null == mediaModel.getURL()) {
	 * mediaModel.setMime("image/png"); mediaModel.setMediaFormat(mediaService.getFormat(mediaFormat)); } }
	 * modelService.save(mediaModel); final String absoluteFilePath = temp[0] +
	 * Config.getParameter(HIRTANDCARTERIMAGEPATH) + str; try { inputStream = new FileInputStream(absoluteFilePath);
	 * mediaService.setStreamForMedia(mediaModel, inputStream); mediaModelList.add(mediaModel);
	 * log.info("++++++++++++++++++++++++++++++++Path from where server will pick up the Image:  " + absoluteFilePath +
	 * "  ++++++++++++++++++++++++++++++++"); } catch (final FileNotFoundException e) {
	 * log.error("Image not found at mentioned path: " + absoluteFilePath); }
	 * 
	 * }
	 */
}
