/**
 *
 */
package de.hybris.clicks.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;



public class ClamScanner
{
	private static final Logger LOG = Logger.getLogger(ClamScanner.class);

	public boolean scanFile(final String filePath)
	{
		boolean isInfected = true;
		LOG.info("in scanFile for " + filePath);
		if (StringUtils.isNotEmpty(filePath))
		{
			final Runtime rt = Runtime.getRuntime();
			BufferedReader reader = null;
			try
			{
				final Process proc = rt.exec("clamscan -i -o " + filePath);

				final StringBuffer output = new StringBuffer();

				proc.waitFor();
				reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

				String line = "";
				while ((line = reader.readLine()) != null)
				{
					output.append(line + "\n");
				}
				LOG.info("scan result " + output.toString());
				if (output.toString().contains("Infected files: 0"))
				{
					isInfected = false;
				}
			}
			catch (final Exception e)
			{
				e.printStackTrace();
				isInfected = false;
			}
			finally
			{
				if (null != reader)
				{
					IOUtils.closeQuietly(reader);
				}
			}
		}
		return isInfected;
	}
}