/**
 *
 */
package de.hybris.clicks.core.cartPage;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;


/**
 * @author shruthi.jhamb
 *
 */
public class DefaultCartPageService implements CartPageService
{

	@Resource
	private CartService cartService;
	@Resource
	private ModelService modelService;
	@Resource
	private PromotionsService promotionsService;
	@Resource
	private CommerceCartService commerceCartService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.core.cartPage.CartPageService#populateData(de.hybris.platform.commercefacades.order.data.CartData
	 * , java.lang.String)
	 */
	@Override
	public CartData populateData(final CartData cartData, final String cartClubcardPoints)
	{
		cartData.setClubcardPoints(cartClubcardPoints);
		if (cartService.hasSessionCart())
		{
			CartModel cartModel = cartService.getSessionCart();
			cartModel = populate(cartModel, cartData);
			modelService.save(cartModel);
		}
		return cartData;
	}

	/**
	 * @param cartModel
	 * @param cartData
	 * @return
	 */
	private CartModel populate(final CartModel cartModel, final CartData cartData)
	{
		cartModel.setClubcardPoints(cartData.getClubcardPoints());
		return cartModel;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * recalculating cart along with promotions
	 * 
	 * @see
	 * de.hybris.clicks.core.cartPage.CartPageService#calculatePromotions(de.hybris.platform.core.model.order.CartModel)
	 */
	@Override
	public CartModel calculateCart(final CartModel cartmodel)
	{
		commerceCartService.calculateCart(cartmodel);
		modelService.save(cartmodel);
		return cartmodel;
	}
}
