/**
 *
 */
package de.hybris.clicks.core.cartPage;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;


/**
 * @author shruthi.jhamb
 *
 */
public interface CartPageService
{
	/**
	 * @param cartData
	 * @param cartClubcardPoints
	 * @return
	 */
	CartData populateData(CartData cartData, String cartClubcardPoints);

	/**
	 * @param cartmodel
	 * @return
	 */
	CartModel calculateCart(CartModel cartmodel);
}
