/**
 *
 */
package de.hybris.clicks.core.account.dao;

import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.core.model.TopicModel;
import de.hybris.clicks.facades.product.data.CountryData;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.TopicsData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;

import javax.annotation.Resource;


/**
 * @author shruti.jhamb
 *
 */
public class DefaultAccountPageDao implements AccountPageDao
{

	@Resource
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.account.dao.AccountPageDao#fetchCountryData(java.util.Collection)
	 */
	@Override
	public Collection<CountryData> fetchCountryData(final Collection<CountryData> countryList)
	{
		de.hybris.clicks.facades.product.data.CountryData country;
		final String query = "SELECT {PK} FROM {Country}";
		final SearchResult<CountryModel> result = flexibleSearchService.search(query);
		if (result.getCount() > 0)
		{
			for (final CountryModel countryModel : result.getResult())
			{
				country = new de.hybris.clicks.facades.product.data.CountryData();
				country.setCode(countryModel.getIsocode());
				country.setName(countryModel.getName());
				countryList.add(country);
			}
		}
		return countryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.account.dao.AccountPageDao#fetchTopicData(java.util.Collection)
	 */
	@Override
	public Collection<TopicsData> fetchTopicData(final Collection<TopicsData> topicList, final boolean isMobileAppReq)
	{
		TopicsData topics;
		final StringBuilder query = new StringBuilder(300);
		query.append("SELECT {PK} FROM {Topic}");
		if (!isMobileAppReq)
		{//For mobile app we need to display Pharmacy topic but for site Pharmacy topic should not come.
			query.append("where {visible}= true");
		}
		final SearchResult<TopicModel> result = flexibleSearchService.search(query.toString());

		if (result.getCount() > 0)
		{
			for (final TopicModel topicModel : result.getResult())
			{
				topics = new TopicsData();
				topics.setCode(topicModel.getCode());
				topics.setName(topicModel.getName());
				if (!topicModel.isVisible())
				{
					topics.setPharmacyRelated(Boolean.TRUE.booleanValue());
				}
				topicList.add(topics);
			}
		}
		return topicList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.account.dao.AccountPageDao#fetchProvinceData(java.util.Collection)
	 */
	@Override
	public Collection<ProvinceData> fetchProvinceData(final Collection<ProvinceData> provinceList)
	{
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

}
