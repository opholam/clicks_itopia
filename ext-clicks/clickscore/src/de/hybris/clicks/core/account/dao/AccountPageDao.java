/**
 *
 */
package de.hybris.clicks.core.account.dao;

import de.hybris.clicks.facades.product.data.CountryData;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.TopicsData;

import java.util.Collection;


/**
 * @author keyur.patel
 *
 */
public interface AccountPageDao
{
	public Collection<CountryData> fetchCountryData(final Collection<de.hybris.clicks.facades.product.data.CountryData> countryList);

	public Collection<TopicsData> fetchTopicData(Collection<TopicsData> topicList, boolean isMobileAppReq);

	public Collection<ProvinceData> fetchProvinceData(Collection<ProvinceData> provinceList);
}
