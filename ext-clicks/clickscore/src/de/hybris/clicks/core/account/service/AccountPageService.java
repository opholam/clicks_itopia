/**
 *
 */
package de.hybris.clicks.core.account.service;

import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.TopicsData;

import java.util.Collection;


/**
 * @author Shruti.jhamb
 *
 */
public interface AccountPageService
{
	public Collection<de.hybris.clicks.facades.product.data.CountryData> fetchCountryData(
			Collection<de.hybris.clicks.facades.product.data.CountryData> countryList);

	public Collection<TopicsData> fetchTopicData(Collection<TopicsData> topicList, boolean isMobileAppReq);

	public Collection<ProvinceData> fetchProvinceData(Collection<ProvinceData> provinceList);
}
