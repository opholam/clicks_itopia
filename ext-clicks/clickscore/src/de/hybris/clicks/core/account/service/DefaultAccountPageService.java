/**
 *
 */
package de.hybris.clicks.core.account.service;

import de.hybris.clicks.core.account.dao.AccountPageDao;
import de.hybris.clicks.facades.product.data.CountryData;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.TopicsData;

import java.util.Collection;

import javax.annotation.Resource;


/**
 * @author shruti.jhamb
 *
 */
public class DefaultAccountPageService implements AccountPageService
{

	@Resource()
	private AccountPageDao accountPageDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.core.account.AccountPageService#fetchCountryData(de.hybris.clicks.facades.product.data.CountryData
	 * )
	 */
	@Override
	public Collection<CountryData> fetchCountryData(Collection<de.hybris.clicks.facades.product.data.CountryData> countryList)
	{

		countryList = accountPageDao.fetchCountryData(countryList);
		// YTODO Auto-generated method stub
		return countryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.account.service.AccountPageService#fetchTopicData(java.util.Collection)
	 */
	@Override
	public Collection<TopicsData> fetchTopicData(Collection<TopicsData> topicList, final boolean isMobileAppReq)
	{
		// YTODO Auto-generated method stub
		topicList = accountPageDao.fetchTopicData(topicList, isMobileAppReq);
		return topicList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.account.service.AccountPageService#fetchProvinceData(java.util.Collection)
	 */
	@Override
	public Collection<ProvinceData> fetchProvinceData(Collection<ProvinceData> provinceList)
	{
		provinceList = accountPageDao.fetchProvinceData(provinceList);
		// YTODO Auto-generated method stub
		return provinceList;
	}

}
