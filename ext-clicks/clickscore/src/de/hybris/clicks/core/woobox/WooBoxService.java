/**
 *
 */
package de.hybris.clicks.core.woobox;

import de.hybris.clicks.core.model.WooBoxModel;

import java.util.List;


/**
 * @author Kirti.chopra
 *
 */
public interface WooBoxService
{

	/**
	 * Gets the all competitions.
	 *
	 * @return the all competitions
	 */
	public abstract List<WooBoxModel> getAllCompetitions();

	/**
	 * Gets the competitions by code.
	 *
	 * @param code
	 *           the code
	 * @return the competitions by code
	 */
	public abstract WooBoxModel getCompetitionsByCode(String code);
}
