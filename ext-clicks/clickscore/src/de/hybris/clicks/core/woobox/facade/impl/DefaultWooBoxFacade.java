/**
 *
 */
package de.hybris.clicks.core.woobox.facade.impl;

import de.hybris.clicks.core.model.WooBoxModel;
import de.hybris.clicks.core.woobox.WooBoxService;
import de.hybris.clicks.core.woobox.facade.WooBoxFacade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;


/**
 * @author Kirti.chopra
 * 
 */
public class DefaultWooBoxFacade implements WooBoxFacade
{

	@Resource
	private WooBoxService wooBoxService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.woobox.facade.WooBoxFacade#getAllCompetitions()
	 */
	@Override
	public List<WooBoxModel> getAllCompetitions()
	{
		return sortCompitions(wooBoxService.getAllCompetitions());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.woobox.facade.WooBoxFacade#getCompetitionsByCode(java.lang.String)
	 */
	@Override
	public WooBoxModel getCompetitionsByCode(final String code)
	{
		return wooBoxService.getCompetitionsByCode(code);
	}


	public List<WooBoxModel> sortCompitions(final List<WooBoxModel> competitions)
	{
		final List<WooBoxModel> competitions1 = new ArrayList<WooBoxModel>();
		try
		{
			competitions1.addAll(CollectionUtils.select(competitions, new Predicate()
			{
				@Override
				public boolean evaluate(final Object arg0)
				{
					if (((WooBoxModel) arg0).getEndDate().before(Calendar.getInstance().getTime()))
					{
						return false;
					}
					return true;
				}
			}));

			competitions1.addAll(CollectionUtils.selectRejected(competitions, new Predicate()
			{
				@Override
				public boolean evaluate(final Object arg0)
				{
					if (((WooBoxModel) arg0).getEndDate().before(Calendar.getInstance().getTime()))
					{
						return false;
					}
					return true;
				}
			}));
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		return competitions1;
	}
}
