/**
 *
 */
package de.hybris.clicks.core.woobox.dao.impl;

import de.hybris.clicks.core.enums.WooBoxTypesEnum;
import de.hybris.clicks.core.model.WooBoxModel;
import de.hybris.clicks.core.woobox.dao.WooBoxDao;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class DefaultWooBoxDao extends DefaultGenericDao<WooBoxModel> implements WooBoxDao
{

	/**
	 * @param typecode
	 */

	public DefaultWooBoxDao(final String typecode)
	{
		super(typecode);
		// YTODO Auto-generated constructor stub
	}

	/**
	 * @param typecode
	 */
	//public DefaultWooBoxDao(final String typecode)
	//{
	//super(typecode);
	// YTODO Auto-generated constructor stub
	//}



	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.woobox.dao.WooBoxDao#getAllCompetitions()
	 */
	@Override
	public List<WooBoxModel> getAllCompetitions()
	{
		return find(Collections.singletonMap(WooBoxModel.TYPE, WooBoxTypesEnum.COMPETITIONS),
				SortParameters.singletonDescending(WooBoxModel.CREATIONTIME));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.woobox.dao.WooBoxDao#getCompetitionsByCode(java.lang.String)
	 */
	@Override
	public WooBoxModel getCompetitionsByCode(final String code)
	{
		List<WooBoxModel> wooBoxModelList = new ArrayList<WooBoxModel>();
		wooBoxModelList = (find(Collections.singletonMap(WooBoxModel.CODE, code)));
		if (wooBoxModelList.size() > 0)
		{
			for (final WooBoxModel wooboxModel : wooBoxModelList)
			{
				return wooboxModel;
			}
		}
		//return null;
		return null;

	}

}
