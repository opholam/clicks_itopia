/**
 *
 */
package de.hybris.clicks.core.woobox.impl;

import de.hybris.clicks.core.model.WooBoxModel;
import de.hybris.clicks.core.woobox.WooBoxService;
import de.hybris.clicks.core.woobox.dao.WooBoxDao;

import java.util.List;

import javax.annotation.Resource;



public class DefaultWooBoxService implements WooBoxService
{


	@Resource
	WooBoxDao wooBoxDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.woobox.WooBoxService#getAllCompetitions()
	 */
	@Override
	public List<WooBoxModel> getAllCompetitions()
	{
		return wooBoxDao.getAllCompetitions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.woobox.WooBoxService#getCompetitionsByCode(java.lang.String)
	 */
	@Override
	public WooBoxModel getCompetitionsByCode(final String code)
	{
		return wooBoxDao.getCompetitionsByCode(code);
	}
}
