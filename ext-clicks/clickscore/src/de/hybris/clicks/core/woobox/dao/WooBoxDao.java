/**
 *
 */
package de.hybris.clicks.core.woobox.dao;

import de.hybris.clicks.core.model.WooBoxModel;

import java.util.List;


/**
 * @author Kirti.chopra
 *
 */
public interface WooBoxDao
{
	public abstract List<WooBoxModel> getAllCompetitions();

	/**
	 * Gets the competitions by code.
	 *
	 * @param code
	 *           the code
	 * @return the competitions by code
	 */
	public abstract WooBoxModel getCompetitionsByCode(String code);
}
