/**
 *
 */
package de.hybris.clicks.core.woobox.facade;

import de.hybris.clicks.core.model.WooBoxModel;
import java.util.List;



public interface WooBoxFacade
{
	public abstract List<WooBoxModel> getAllCompetitions();

	public abstract WooBoxModel getCompetitionsByCode(String code);
}
