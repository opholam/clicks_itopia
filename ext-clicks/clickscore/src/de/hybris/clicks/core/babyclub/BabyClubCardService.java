/**
 *
 */
package de.hybris.clicks.core.babyclub;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;



/**
 * @author ashish.vyas
 *
 */
public interface BabyClubCardService
{
	public void joinBabyClub(final CustomerData customerData) throws ModelSavingException;

	public CustomerData getBabyClubDetails();
}
