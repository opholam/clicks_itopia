/**
 *
 */
package de.hybris.clicks.core.babyclub.impl;

import de.hybris.clicks.core.babyclub.BabyClubCardService;
import de.hybris.clicks.core.model.BabyClubModel;
import de.hybris.clicks.core.model.ChildModel;
import de.hybris.clicks.webclient.customer.client.CustomerUpdateClient;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.BabyClubData;
import de.hybris.platform.commercefacades.user.data.ChildData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;


/**
 * @author ashish.vyas
 *
 */
public class DefaultBabyClubCardService implements BabyClubCardService
{

	private static final Logger LOG = Logger.getLogger(DefaultBabyClubCardService.class);
	@Resource(name = "modelService")
	ModelService modelService;

	@Resource(name = "userService")
	UserService userService;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource
	CustomerUpdateClient customerUpdateClient;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.babyclub.BabyClubCardService#joinBabyClub(de.hybris.platform.commercefacades.user.data.
	 * CustomerData)
	 */

	@Override
	public void joinBabyClub(CustomerData customerData) throws ModelSavingException
	{
		customerData = populateClubCardCustomerData(customerData);
		if (!updateCustomer(customerData))
		{
			throw new ModelSavingException("Error in OMNIUPD");
		}
		try
		{
			CustomerModel customer = (CustomerModel) userService.getCurrentUser();

			CustomerModel customerModel = modelService.create(CustomerModel.class);
			customerModel.setUid(customer.getUid());
			customerModel.setGroups(customer.getGroups());
			try
			{
				customerModel = flexibleSearchService.getModelByExample(customerModel);
				customer = customerModel;
			}
			catch (final Exception e)
			{
				LOG.info(e.getMessage());
			}

			final List<BabyClubModel> babyClubList = new ArrayList<BabyClubModel>();
			BabyClubModel babyClubModel = null;

			if (null != customer.getBabyClub() && !CollectionUtils.isEmpty(customer.getBabyClub())
					&& customer.getBabyClub().size() > 0)
			{
				final Collection<BabyClubModel> existingBabyClubList = customer.getBabyClub();
				for (final BabyClubModel babyClub : existingBabyClubList)
				{
					babyClubModel = babyClub;
					break;
				}
			}
			else
			{
				babyClubModel = modelService.create(BabyClubModel.class);
			}

			babyClubList.add(getBabyDetails(customerData, babyClubModel));
			customer.setBabyClub(babyClubList);
			modelService.saveAll(customer);
			modelService.refresh(customer);

			PrincipalGroupModel principalGroupModel = modelService.create(PrincipalGroupModel.class);
			principalGroupModel.setUid("babyClubGroup");
			try
			{
				principalGroupModel = flexibleSearchService.getModelByExample(principalGroupModel);
			}
			catch (final Exception e)
			{
				principalGroupModel.setName("babyClubGroup");
				modelService.save(principalGroupModel);
			}
			final Set<PrincipalGroupModel> pricipalList2 = new HashSet<PrincipalGroupModel>();
			pricipalList2.addAll(customer.getGroups());
			pricipalList2.add(principalGroupModel);
			customer.setGroups(pricipalList2);
			modelService.save(customer);

		}
		catch (final ModelSavingException e)
		{
			LOG.error("Error in saving customer model while joining baby club :" + e.getMessage());
			throw new ModelSavingException("Error in saving customer model while joining baby club");
		}
	}

	public BabyClubModel getBabyDetails(final CustomerData customerData, final BabyClubModel babyClubModel)
	{

		if (customerData.getBabyClub() != null && null != customerData.getBabyClub().getBabyClubDueDate()
				&& StringUtils.isNotEmpty(customerData.getBabyClub().getBabyClubDueDate()))
		{
			babyClubModel.setBabyClubDueDate(customerData.getBabyClub().getBabyClubDueDate());
			if (null != customerData.getBabyClub().getIsAlreadyPregnant())
			{
				babyClubModel.setIsAlreadyPregnant(customerData.getBabyClub().getIsAlreadyPregnant());
			}
			if (null != customerData.getBabyClub().getGender() && StringUtils.isNotEmpty(customerData.getBabyClub().getGender()))
			{
				babyClubModel.setGender(Enum.valueOf(Gender.class, customerData.getBabyClub().getGender().toUpperCase()));
			}
		}
		else if (null != customerData.getBabyClub() && null != customerData.getBabyClub().getChildren()
				&& customerData.getBabyClub().getChildren().size() > 0)
		{
			final List<ChildData> childData = customerData.getBabyClub().getChildren();
			final List<ChildModel> childs = new ArrayList<ChildModel>();
			for (final ChildData childData2 : childData)
			{
				final ChildModel childmodel = new ChildModel();
				childmodel.setFirstName(childData2.getFirstName());
				childmodel.setLastName(childData2.getLastName());
				childmodel.setChildDOB(childData2.getChildDOB());
				childmodel.setGender(Enum.valueOf(Gender.class, childData2.getGender().toUpperCase()));
				childs.add(childmodel);
			}

			//		final List<ChildModel> existingListChildModel = babyClubModel.getChildren();
			//			if (!CollectionUtils.isEmpty(existingListChildModel))
			//			{
			//				for (final ChildModel existingChildModel : existingListChildModel)
			//				{
			//					childs.add(existingChildModel);
			//				}
			//			}
			if (childs.size() > 0)
			{
				babyClubModel.setChildren(childs);
			}
			babyClubModel.setIsAlreadyParent(customerData.getBabyClub().getIsAlreadyParent());
		}
		if (null != customerData.getBabyClub().getGender())
		{
			babyClubModel.setGender(Gender.valueOf(customerData.getBabyClub().getGender().toUpperCase()));
		}
		modelService.save(babyClubModel);
		return babyClubModel;
	}

	@Override
	public CustomerData getBabyClubDetails()
	{
		final CustomerModel customer = (CustomerModel) userService.getCurrentUser();
		final Collection<BabyClubModel> babyClubList = customer.getBabyClub();
		final CustomerData customerData = new CustomerData();
		if (!CollectionUtils.isEmpty(babyClubList))
		{
			final BabyClubData babyClubData = new BabyClubData();
			for (final BabyClubModel babyClubModel : babyClubList)
			{
				if (null != babyClubModel.getBabyClubDueDate())
				{
					babyClubData.setBabyClubDueDate(babyClubModel.getBabyClubDueDate());
				}
				if (null != babyClubModel.getGender())
				{
					babyClubData.setGender(babyClubModel.getGender().name());
				}
				if (null != babyClubModel.getIsAlreadyPregnant())
				{
					babyClubData.setIsAlreadyPregnant(babyClubModel.getIsAlreadyPregnant());
				}

				if (null != babyClubModel.getChildren() && !CollectionUtils.isEmpty(babyClubModel.getChildren()))
				{
					final List<ChildData> childDataList = new ArrayList<ChildData>();
					babyClubModel.setIsAlreadyParent(new Boolean(true));
					modelService.save(babyClubModel);
					babyClubData.setIsAlreadyParent(new Boolean(true));
					for (final ChildModel childModel : babyClubModel.getChildren())
					{
						final ChildData childData = new ChildData();
						if (null != childModel.getChildDOB())
						{
							childData.setChildDOB(childModel.getChildDOB());
						}
						if (null != childModel.getFirstName() && StringUtils.isNotBlank(childModel.getFirstName()))
						{
							childData.setFirstName(childModel.getFirstName());
						}
						if (null != childModel.getGender() && StringUtils.isNotBlank(childModel.getGender().name()))
						{
							childData.setGender(childModel.getGender().name());
						}
						if (null != childModel.getLastName() && StringUtils.isNotBlank(childModel.getLastName()))
						{
							childData.setLastName(childModel.getLastName());
						}
						childDataList.add(childData);
					}
					if (childDataList.size() > 0)
					{
						babyClubData.setChildren(childDataList);
					}
				}

			}
			customerData.setBabyClub(babyClubData);
			return customerData;
		}

		return null;
	}

	public CustomerData populateClubCardCustomerData(final CustomerData customerData)
	{
		CustomerModel customerModel = null;
		final CustomerData customer = customerFacade.getCurrentCustomer();
		try
		{
			customerModel = (CustomerModel) userService.getCurrentUser();
		}
		catch (final Exception e)
		{
			LOG.info(e);
		}
		if (null != customer.getMarketingConsent_email())
		{
			customerData.setMarketingConsent_email(customer.getMarketingConsent_email());
		}
		if (null != customer.getMarketingConsent_SMS())
		{
			customerData.setMarketingConsent_SMS(customer.getMarketingConsent_SMS());
		}
		if (null != customer.getAccinfo_email())
		{
			customerData.setAccinfo_email(customer.getAccinfo_email());
		}
		if (null != customer.getAccinfo_SMS())
		{
			customerData.setAccinfo_SMS(customer.getAccinfo_SMS());
		}
		if (null != customer.getContactDetails())
		{
			//customerData.setContactDetails(customer.getContactDetails());
			final List<ContactDetail> contactDetails = new ArrayList<ContactDetail>();
			ContactDetail contactDetail;
			for (final ContactDetail contact : customer.getContactDetails())
			{
				if (StringUtils.isNotBlank(contact.getNumber()))
				{
					contactDetail = new ContactDetail();
					contactDetail.setTypeID(contact.getTypeID());
					contactDetail.setNumber(contact.getNumber());
					contactDetails.add(contactDetail);
				}
			}
			if (contactDetails.size() > 0)
			{
				customerData.setContactDetails(contactDetails);
			}

		}
		if (null != customerModel && null != customerModel.getCustomerID())
		{
			customerData.setCustomerID(customerModel.getCustomerID());
		}
		if (null != customerModel.getFirstName())
		{
			customerData.setFirstName(getTitleCase(customerModel.getFirstName()));
		}
		if (null != customerModel.getLastName())
		{
			customerData.setLastName(getTitleCase(customerModel.getLastName()));
		}
		if (null != customerModel.getUid())
		{
			customerData.setEmailID(getTitleCase(customerModel.getUid()));
		}
		/*
		 * if (null != customerData.getEmailID()) { customerData.setUid(getTitleCase(customerData.getEmailID())); }
		 */
		if (null != customerModel.getName() && !StringUtils.isEmpty(customerModel.getName()))
		{
			customerData.setPreferedName(getTitleCase(customerModel.getName()));
		}

		if (null != customerModel.getSAResident() && customerModel.getSAResident().booleanValue())
		{
			customerData.setSAResident(Boolean.TRUE);
			if (null != customerModel.getRSA_ID())
			{
				customerData.setIDnumber(customerModel.getRSA_ID());
			}
		}
		else
		{
			customerData.setSAResident(Boolean.FALSE);
			if (null != customerModel.getNonRSA_DOB())
			{
				//final String date = customerModel.getNonRSA_DOB().toString();
				customerData.setNonRSA_DOB(customerModel.getNonRSA_DOB());
			}
		}
		//customerData.setGender(new Integer(customerModel.getGender().getCode()).toString());

		if (null != customerModel.getGender())
		{
			if (customerModel.getGender().getCode().equalsIgnoreCase("male"))
			{
				customerData.setGender("1");
			}
			if (customerModel.getGender().getCode().equalsIgnoreCase("female"))
			{
				customerData.setGender("2");
			}
		}

		final List<AddressData> addresses = new ArrayList<AddressData>();

		for (final AddressModel addressModel : customerModel.getAddresses())
		{
			final AddressData address = new AddressData();

			if (null != addressModel.getBuilding())
			{
				address.setLine1(getTitleCase(addressModel.getBuilding()));
			}
			if (null != addressModel.getStreetname())
			{
				address.setLine2(getTitleCase(addressModel.getStreetname()));
			}
			if (null != addressModel.getTypeID())
			{
				address.setTypeID(addressModel.getTypeID());
			}
			if (null != addressModel.getStreetnumber())
			{
				address.setSuburb(getTitleCase(addressModel.getStreetnumber()));
			}
			if (null != addressModel.getTown())
			{
				address.setCity(getTitleCase(addressModel.getTown()));
			}

			if (null != addressModel.getCountry())
			{
				final CountryData countryData = new CountryData();
				countryData.setIsocode(addressModel.getCountry().getIsocode());
				address.setCountry(countryData);
			}

			if (null != addressModel.getPostalcode())
			{
				address.setPostalCode(addressModel.getPostalcode());
			}
			//		if (null != addressModel.getProvince())
			//		{
			//			address.setProvince(ccForm.getProvince());
			//		}

			addresses.add(address);
		}
		if (addresses.size() > 0)
		{
			customerData.setAddresses(addresses);
		}
		//		if (ccForm.isMarketingEmail())
		//		{
		//			customerData.setMarketingConsent_email(new Boolean(ccForm.isMarketingEmail()));
		//		}
		//		if (ccForm.isMarketingSMS())
		//		{
		//			customerData.setMarketingConsent_SMS(new Boolean(ccForm.isMarketingSMS()));
		//		}
		//		if (ccForm.isAccinfoEmail())
		//		{
		//			customerData.setAccinfo_email(new Boolean(ccForm.isAccinfoEmail()));
		//		}
		//		if (ccForm.isAccinfoSMS())
		//		{
		//			customerData.setAccinfo_SMS(new Boolean(ccForm.isAccinfoSMS()));
		//		}

		//		if (null != ccForm.getContactNumber() || null != ccForm.getAlternateNumber())
		//		{
		//			final List<ContactDetail> contactDetailList = new ArrayList<ContactDetail>();
		//			if (null != ccForm.getContactNumber())
		//			{
		//				final ContactDetail contact = new ContactDetail();
		//				contact.setNumber(ccForm.getContactNumber());
		//				contact.setTypeID("2");
		//				contactDetailList.add(contact);
		//			}
		//			if (null != ccForm.getAlternateNumber())
		//			{
		//				final ContactDetail contact = new ContactDetail();
		//				contact.setNumber(ccForm.getAlternateNumber());
		//				contact.setTypeID("4");
		//				contactDetailList.add(contact);
		//			}
		//			if (contactDetailList.size() > 0)
		//			{
		//				customerData.setContactDetails(contactDetailList);
		//			}
		//		}
		if (null != customerModel.getUid())
		{
			customerData.setUid(customerModel.getUid());
		}
		if (null != customerModel.getMemberID())
		{
			customerData.setMemberID(customerModel.getMemberID());
		}
		return customerData;
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	public boolean updateCustomer(final CustomerData customerData)
	{
		ResultData resultData = null;
		boolean result = Boolean.FALSE.booleanValue();
		try
		{
			resultData = customerUpdateClient.updateCustomer(customerData);
			if ((null != resultData && null != resultData.getCode() && resultData.getCode().intValue() == 0))
			{
				result = Boolean.TRUE.booleanValue();
			}
			return result;
		}
		catch (final Exception e)
		{
			LOG.info(e.getMessage());
			return result;
		}

	}


}
