/**
 *
 */
package de.hybris.clicks.core.financial.impl;

import de.hybris.clicks.core.financial.FinancialService;
import de.hybris.clicks.core.financial.dao.FinancialDao;
import de.hybris.clicks.core.model.FinancialServiceModel;

import java.util.List;

import javax.annotation.Resource;


/**
 * @author ashish.vyas
 *
 */
public class DefaultFinancialService implements FinancialService
{
	@Resource(name = "financialDao")
	FinancialDao financialDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.financial.FinancialService#getFinancialService()
	 */
	public List<FinancialServiceModel> getFinancialService()
	{
		return financialDao.getFinancialService();
	}
}
