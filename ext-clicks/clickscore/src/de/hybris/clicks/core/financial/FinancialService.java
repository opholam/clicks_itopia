/**
 *
 */
package de.hybris.clicks.core.financial;

import de.hybris.clicks.core.model.FinancialServiceModel;

import java.util.List;


/**
 * @author ashish.vyas
 *
 */
public interface FinancialService
{

	/**
	 * Gets the financial service.
	 *
	 * @return the financial service
	 */
	public List<FinancialServiceModel> getFinancialService();
}
