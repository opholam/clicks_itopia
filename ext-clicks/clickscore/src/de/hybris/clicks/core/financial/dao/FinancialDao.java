/**
 *
 */
package de.hybris.clicks.core.financial.dao;

import de.hybris.clicks.core.model.FinancialServiceModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * @author ashish.vyas
 *
 */
public interface FinancialDao extends Dao
{
	public List<FinancialServiceModel> getFinancialService();
}
