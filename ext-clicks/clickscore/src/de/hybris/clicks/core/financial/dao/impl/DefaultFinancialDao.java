/**
 *
 */
package de.hybris.clicks.core.financial.dao.impl;

import de.hybris.clicks.core.financial.dao.FinancialDao;
import de.hybris.clicks.core.model.FinancialServiceModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;


/**
 * @author ashish.vyas
 *
 */
public class DefaultFinancialDao extends DefaultGenericDao<FinancialServiceModel> implements FinancialDao
{

	/**
	 * @param typecode
	 */
	public DefaultFinancialDao(final String typecode)
	{
		super(typecode);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.financial.dao.FinancialDao#getFinancialService()
	 */
	@Override
	public List<FinancialServiceModel> getFinancialService()
	{
		return find();
	}
}
