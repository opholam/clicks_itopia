/**
 *
 */
package de.hybris.clicks.core.storefinder.Dao;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.List;


/**
 * @author karthikeyan.n
 * @param <M>
 *
 */
public interface ClicksStorefinderGenericDao<PointOfServiceModel> extends GenericDao
{

	/**
	 * Find pos by store name.
	 *
	 * @param storeName
	 *           the store name
	 * @return the list
	 */
	public List<PointOfServiceModel> findPOSByStoreName(String storeName);
}
