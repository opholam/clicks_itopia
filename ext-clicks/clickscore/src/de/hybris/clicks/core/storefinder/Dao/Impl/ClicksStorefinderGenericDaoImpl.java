/**
 *
 */
package de.hybris.clicks.core.storefinder.Dao.Impl;

import de.hybris.clicks.core.storefinder.Dao.ClicksStorefinderGenericDao;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


/**
 * @author karthikeyan.n
 * 
 */
public class ClicksStorefinderGenericDaoImpl extends DefaultGenericDao implements ClicksStorefinderGenericDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @param typecode
	 */
	public ClicksStorefinderGenericDaoImpl(final String typecode)
	{
		super(typecode);
		// YTODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.storefinder.Dao.ClicksStorefinderGenericDao#findPOSByStoreName(java.lang.String)
	 */
	@Override
	public List findPOSByStoreName(final String storeName)
	{
		final FlexibleSearchQuery query = createFlexibleSearchQuery(storeName);
		return getFlexibleSearchService().search(query).getResult();
	}

	private FlexibleSearchQuery createFlexibleSearchQuery(final String storeName)
	{
		final Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("type", PointOfServiceTypeEnum.STORE);
		final StringBuilder builder = createQueryString();
		appendWhereClausesToBuilder(builder, storeName);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(builder.toString(), paramMap);

		query.setNeedTotal(true);
		return query;
	}

	private StringBuilder createQueryString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("SELECT {c:").append("pk").append("} ");
		builder.append("FROM {").append(PointOfServiceModel._TYPECODE).append(" AS c} ");
		return builder;
	}

	private void appendWhereClausesToBuilder(final StringBuilder builder, final String params)
	{
		if ((params == null) || (params.isEmpty()))
		{
			return;
		}
		builder.append("WHERE lower({ c:displayname }) like '%");
		builder.append(params.toLowerCase());
		builder.append("%' and {c:type}= ?type");

	}
}
