/**
 *
 */
package de.hybris.clicks.core.storefinder.impl;

import de.hybris.clicks.core.storefinder.Dao.ClicksStorefinderGenericDao;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commerceservices.model.storelocator.StoreLocatorFeatureModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.commerceservices.storefinder.impl.DefaultStoreFinderService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author abhaydh
 * 
 */
public class StoreFinderFeatureService extends DefaultStoreFinderService
{

	private static final Logger LOG = Logger.getLogger(StoreFinderFeatureService.class);


	@Resource(name = "clicksStorefinderGenericDao")
	private ClicksStorefinderGenericDao clicksStorefinderGenericDao;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;


	@Override
	public StoreFinderSearchPageData locationSearch(final BaseStoreModel baseStore, final String locationText,
			final PageableData pageableData)
	{
		GeoPoint geoPoint = null;
		GeoPoint defaultGeoPoint = null;
		final List<GeoPoint> geoPoints = new ArrayList<GeoPoint>();
		if ((locationText != null) && (!(locationText.isEmpty())))
		{
			try
			{
				final List<AddressData> addressDatas = generateGeoAddressForPOSSearchQuery(baseStore, locationText);
				GPS resolvedPoint = null;
				for (final AddressData addressData : addressDatas)
				{
					try
					{
						resolvedPoint = getGeoWebServiceWrapper().geocodeAddress(addressData);
						geoPoint = new GeoPoint();
						geoPoint.setLatitude(resolvedPoint.getDecimalLatitude());
						geoPoint.setLongitude(resolvedPoint.getDecimalLongitude());
						geoPoints.add(geoPoint);

						if ("ZA".equalsIgnoreCase(addressData.getCountryCode()))
						{
							defaultGeoPoint = geoPoint;
						}
						LOG.info(addressData.getCountryCode() + ">>Lat >> " + resolvedPoint.getDecimalLatitude() + " >>>> Long>>> "
								+ resolvedPoint.getDecimalLongitude());
					}
					catch (final GeoServiceWrapperException localGeoServiceWrapperException)
					{
						LOG.info("Failed to resolve location for [" + locationText + "]"
								+ localGeoServiceWrapperException.getLocalizedMessage()
								+ localGeoServiceWrapperException.getGoogleResponseCode());
					}

				}
				if (CollectionUtils.isNotEmpty(geoPoints))

				{
					return doSearch(baseStore, locationText, geoPoints, pageableData, baseStore.getMaxRadiusForPoSSearch(),
							defaultGeoPoint);
				}
			}
			catch (final GeoServiceWrapperException localGeoServiceWrapperException)
			{
				localGeoServiceWrapperException.printStackTrace();
				LOG.info("Failed to resolve location for [" + locationText + "]");
			}
			geoPoint = new GeoPoint();
			geoPoint.setLatitude(0.0);
			geoPoint.setLongitude(0.0);
			return doSearch(baseStore, locationText, geoPoint, pageableData, baseStore.getMaxRadiusForPoSSearch());
		}

		return createSearchResult(locationText, geoPoint, Collections.emptyList(), createPaginationData());
	}

	protected List<AddressData> generateGeoAddressForPOSSearchQuery(final BaseStoreModel baseStore, final String locationText)
	{
		final List<AddressData> addressDatas = new ArrayList<AddressData>();
		if ((locationText != null) && (!(locationText.contains(","))))
		{
			//			final Map paramMap = new HashMap();
			//			paramMap.put("baseStore", baseStore);
			//			paramMap.put("type", PointOfServiceTypeEnum.STORE);
			//	final Collection allPos = getPointOfServiceGenericDao().find(paramMap);

			/*
			 * if ((allPos != null) && (allPos.iterator().hasNext())) { final PointOfServiceModel pos =
			 * (PointOfServiceModel) allPos.iterator().next(); if ((pos != null) && (pos.getAddress() != null) &&
			 * (pos.getAddress().getCountry() != null)) { country = pos.getAddress().getCountry().getIsocode(); } }
			 */

			final List<Object> countries = flexibleSearchService.search(createFlexibleSearchQuery()).getResult();
			AddressData addressData;

			for (final Object isoCode : countries)
			{
				addressData = new AddressData();
				addressData.setCity(locationText);
				addressData.setCountryCode((String) isoCode);
				addressDatas.add(addressData);
				LOG.info("Generated Geo Address Data: City[" + locationText + "] - Country[" + isoCode + "]");
			}

			return addressDatas;

		}

		//		final AddressData addressData = new AddressData();
		//		addressData.setCity(locationText);
		//		addressData.setCountryCode(country);
		//		addressDatas.add(addressData);
		//		if (LOG.isDebugEnabled())
		//		{
		//			LOG.debug("Generated Geo Address Data: City[" + locationText + "] - Country[" + country + "]");
		//		}

		return addressDatas;
	}

	private FlexibleSearchQuery createFlexibleSearchQuery()
	{

		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				"select distinct {c:isocode} from {PointOfService as pos join address as a on {pos:address}={a:pk} join country as c on {a:country}={c:pk}}");
		query.setNeedTotal(true);
		query.setResultClassList(Arrays.asList(String.class));
		return query;
	}


	protected List calculatePOSDistances(final GeoPoint centerPoint, final Collection<PointOfServiceModel> pointsOfService)
	{
		final List result = new ArrayList();

		for (final PointOfServiceModel pointOfService : pointsOfService)
		{
			final PointOfServiceDistanceData storeFinderResultData = createStoreFinderResultData();
			storeFinderResultData.setPointOfService(pointOfService);
			if (null != centerPoint)
			{
				storeFinderResultData.setDistanceKm(calculateDistance(centerPoint, pointOfService));
			}
			result.add(storeFinderResultData);
		}

		Collections.sort(result, StoreFinderResultDataComparator.INSTANCE);

		return result;
	}

	protected StoreFinderSearchPageData<PointOfServiceDistanceData> doSearch(final BaseStoreModel baseStore,
			final String locationText, final List<GeoPoint> centerPoints, final PageableData pageableData, final Double maxRadiusKm,
			final GeoPoint defaultGeoPoint)
	{
		GeoPoint centerPoint = new GeoPoint();
		centerPoint.setLatitude(0.0);
		centerPoint.setLongitude(0.0);
		final int resultRangeStart = pageableData.getCurrentPage() * pageableData.getPageSize();
		//final int resultRangeEnd = (pageableData.getCurrentPage() + 1) * pageableData.getPageSize();
		Collection posResults = new ArrayList();
		Collection<PointOfServiceModel> storeNamesList;
		final Set<PointOfServiceModel> finalPosList = new HashSet<PointOfServiceModel>();

		if (null != centerPoints)
		{
			centerPoint = null != defaultGeoPoint ? defaultGeoPoint : centerPoints.get(0);
			LOG.info("centerpoint >> " + centerPoint.getLatitude() + "   " + centerPoint.getLongitude());
			if (maxRadiusKm != null)
			{
				for (final GeoPoint centerGeoPoint : centerPoints)
				{
					posResults = getPointsOfServiceNear(centerGeoPoint, maxRadiusKm.doubleValue(), baseStore);
					finalPosList.addAll(posResults);
				}

			}
			else
			{
				final Map paramMap = new HashMap();
				paramMap.put("baseStore", baseStore);
				paramMap.put("type", PointOfServiceTypeEnum.STORE);
				posResults = getPointOfServiceGenericDao().find(paramMap);
				finalPosList.addAll(posResults);
			}


			//			if (CollectionUtils.isNotEmpty(posResults))
			//			{
			//				finalPosList.addAll(posResults);
			//			}
		}
		if (StringUtils.isNotEmpty(locationText))
		{
			storeNamesList = clicksStorefinderGenericDao.findPOSByStoreName(locationText);
			if (CollectionUtils.isNotEmpty(storeNamesList))
			{
				if (CollectionUtils.isEmpty(finalPosList))
				{
					centerPoint = new GeoPoint();

					double lat = storeNamesList.iterator().next().getLatitude();
					double lang = storeNamesList.iterator().next().getLongitude();
					//Config.getDouble("store.clicks.latitude", 0);
					//Config.getDouble("store.clicks.longitude", 0);
					lat = lat + Config.getDouble("store.clicks.latitude", 0);
					lang = lang + Config.getDouble("store.clicks.longitude", 0);
					centerPoint.setLatitude(lat);
					centerPoint.setLongitude(lang);
				}
				finalPosList.addAll(storeNamesList);
			}
		}






		// added for feature filter

		final Collection<PointOfServiceModel> posResultsFeature = new ArrayList<PointOfServiceModel>();

		if (null != pageableData.getFeature() && !pageableData.getFeature().equalsIgnoreCase("all"))
		{
			for (final Iterator<PointOfServiceModel> data = finalPosList.iterator(); data.hasNext();)
			{
				final PointOfServiceModel pos = data.next();
				for (final StoreLocatorFeatureModel feature : pos.getFeatures())
				{
					if (feature.getName().equalsIgnoreCase(pageableData.getFeature()))
					{
						posResultsFeature.add(pos);
						break;
					}
				}
			}
		}

		// end- added for feature filter
		if (posResultsFeature.size() > 0)
		{
			final List orderedResults = calculatePOSDistances(centerPoint, posResultsFeature);
			final PaginationData paginationData = createPagination(pageableData, posResultsFeature.size());

			final List orderedResultsWindow = orderedResults.subList(Math.min(orderedResults.size(), resultRangeStart),
					orderedResults.size());

			return createSearchResult(locationText, centerPoint, orderedResultsWindow, paginationData);
		}
		else if (CollectionUtils.isNotEmpty(finalPosList) && null != pageableData.getFeature()
				&& !pageableData.getFeature().equalsIgnoreCase("all"))
		{

			return createSearchResult(locationText, centerPoint, Collections.emptyList(), createPagination(pageableData, 0L));

		}
		else if (CollectionUtils.isNotEmpty(finalPosList))

		{
			final List orderedResults = calculatePOSDistances(centerPoint, finalPosList);
			//final PaginationData paginationData = createPagination(pageableData, posResults.size());
			final PaginationData paginationData = new PaginationData();
			//final List orderedResultsWindow = orderedResults.subList(Math.min(orderedResults.size(), resultRangeStart),
			//		Math.min(orderedResults.size(), resultRangeEnd));


			return createSearchResult(locationText, centerPoint, orderedResults, paginationData);
		}

		return createSearchResult(locationText, centerPoint, Collections.emptyList(), createPagination(pageableData, 0L));
	}

	@Override
	protected StoreFinderSearchPageData<PointOfServiceDistanceData> doSearch(final BaseStoreModel baseStore,
			final String locationText, GeoPoint centerPoint, final PageableData pageableData, final Double maxRadiusKm)
	{

		final int resultRangeStart = pageableData.getCurrentPage() * pageableData.getPageSize();
		//final int resultRangeEnd = (pageableData.getCurrentPage() + 1) * pageableData.getPageSize();
		Collection posResults = new ArrayList();
		Collection<PointOfServiceModel> storeNamesList;
		final Collection finalPosList = new ArrayList();

		if (null != centerPoint)
		{
			if (maxRadiusKm != null)
			{
				posResults = getPointsOfServiceNear(centerPoint, maxRadiusKm.doubleValue(), baseStore);
			}
			else
			{
				final Map paramMap = new HashMap();
				paramMap.put("baseStore", baseStore);
				paramMap.put("type", PointOfServiceTypeEnum.STORE);
				posResults = getPointOfServiceGenericDao().find(paramMap);
			}


			if (CollectionUtils.isNotEmpty(posResults))
			{
				finalPosList.addAll(posResults);
			}
		}
		if (StringUtils.isNotEmpty(locationText))
		{
			storeNamesList = clicksStorefinderGenericDao.findPOSByStoreName(locationText);
			if (CollectionUtils.isNotEmpty(storeNamesList))
			{
				centerPoint = new GeoPoint();

				double lat = storeNamesList.iterator().next().getLatitude();
				double lang = storeNamesList.iterator().next().getLongitude();
				//Config.getDouble("store.clicks.latitude", 0);
				//Config.getDouble("store.clicks.longitude", 0);
				lat = lat + Config.getDouble("store.clicks.latitude", 0);
				lang = lang + Config.getDouble("store.clicks.longitude", 0);
				centerPoint.setLatitude(lat);
				centerPoint.setLongitude(lang);

				finalPosList.addAll(storeNamesList);
			}
		}






		// added for feature filter

		final Collection<PointOfServiceModel> posResultsFeature = new ArrayList<PointOfServiceModel>();

		if (null != pageableData.getFeature() && !pageableData.getFeature().equalsIgnoreCase("all"))
		{
			for (final Iterator<PointOfServiceModel> data = finalPosList.iterator(); data.hasNext();)
			{
				final PointOfServiceModel pos = data.next();
				for (final StoreLocatorFeatureModel feature : pos.getFeatures())
				{
					if (feature.getName().equalsIgnoreCase(pageableData.getFeature()))
					{
						posResultsFeature.add(pos);
						break;
					}
				}
			}
		}

		// end- added for feature filter
		if (posResultsFeature.size() > 0)
		{
			final List orderedResults = calculatePOSDistances(centerPoint, posResultsFeature);
			final PaginationData paginationData = createPagination(pageableData, posResultsFeature.size());

			final List orderedResultsWindow = orderedResults.subList(Math.min(orderedResults.size(), resultRangeStart),
					orderedResults.size());

			return createSearchResult(locationText, centerPoint, orderedResultsWindow, paginationData);
		}
		else if (CollectionUtils.isNotEmpty(finalPosList) && null != pageableData.getFeature()
				&& !pageableData.getFeature().equalsIgnoreCase("all"))
		{

			return createSearchResult(locationText, centerPoint, Collections.emptyList(), createPagination(pageableData, 0L));

		}
		else if (CollectionUtils.isNotEmpty(finalPosList))
		{

			final List orderedResults = calculatePOSDistances(centerPoint, finalPosList);
			//final PaginationData paginationData = createPagination(pageableData, posResults.size());
			final PaginationData paginationData = new PaginationData();
			//final List orderedResultsWindow = orderedResults.subList(Math.min(orderedResults.size(), resultRangeStart),
			//		Math.min(orderedResults.size(), resultRangeEnd));


			return createSearchResult(locationText, centerPoint, orderedResults, paginationData);
		}

		return createSearchResult(locationText, centerPoint, Collections.emptyList(), createPagination(pageableData, 0L));
	}

	/**
	 * @return the clicksStorefinderGenericDao
	 */
	public ClicksStorefinderGenericDao getClicksStorefinderGenericDao()
	{
		return clicksStorefinderGenericDao;
	}

	/**
	 * @param clicksStorefinderGenericDao
	 *           the clicksStorefinderGenericDao to set
	 */
	public void setClicksStorefinderGenericDao(final ClicksStorefinderGenericDao clicksStorefinderGenericDao)
	{
		this.clicksStorefinderGenericDao = clicksStorefinderGenericDao;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
