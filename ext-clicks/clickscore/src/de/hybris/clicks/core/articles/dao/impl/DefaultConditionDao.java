/**
 *
 */
package de.hybris.clicks.core.articles.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.clicks.core.articles.dao.ConditionDao;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public class DefaultConditionDao extends DefaultGenericDao<ConditionsModel> implements ConditionDao
{

	public DefaultConditionDao(final String typecode)
	{
		super(typecode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.storefront.articles.dao.ConditionDao#findConditionByCode(java.lang.String)
	 */
	@Override
	public List<ConditionsModel> findConditionByCode(final String conditionCode)
	{
		validateParameterNotNull(conditionCode, "Condition code must not be null!");

		return find(Collections.singletonMap(ConditionsModel.CONDITIONID, (Object) conditionCode));
	}

}
