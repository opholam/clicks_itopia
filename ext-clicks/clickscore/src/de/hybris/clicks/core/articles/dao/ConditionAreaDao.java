/**
 *
 */
package de.hybris.clicks.core.articles.dao;

import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public interface ConditionAreaDao extends Dao
{
	/**
	 * @param conditionCode
	 * @return
	 */
	List<ConditionsAreaModel> findConditionByArea(String conditionCode);
}
