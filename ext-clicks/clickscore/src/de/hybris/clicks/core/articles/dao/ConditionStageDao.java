/**
 *
 */
package de.hybris.clicks.core.articles.dao;

import de.hybris.clicks.core.model.ConditionsStageModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public interface ConditionStageDao extends Dao
{
	/**
	 * @param conditionCode
	 * @return
	 */
	List<ConditionsStageModel> findConditionByStage(String conditionCode);
}
