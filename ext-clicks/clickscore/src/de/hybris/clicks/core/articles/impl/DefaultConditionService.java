/**
 *
 */
package de.hybris.clicks.core.articles.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;

import de.hybris.clicks.core.articles.ConditionService;
import de.hybris.clicks.core.articles.dao.ConditionAreaDao;
import de.hybris.clicks.core.articles.dao.ConditionDao;
import de.hybris.clicks.core.articles.dao.ConditionStageDao;
import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.core.model.ConditionsStageModel;

import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public class DefaultConditionService implements ConditionService
{
	private ConditionDao conditionDao;
	private ConditionStageDao conditionStageDao;
	private ConditionAreaDao conditionAreaDao;


	/**
	 * @return the conditionStageDao
	 */
	public ConditionStageDao getConditionStageDao()
	{
		return conditionStageDao;
	}


	/**
	 * @param conditionStageDao
	 *           the conditionStageDao to set
	 */
	public void setConditionStageDao(final ConditionStageDao conditionStageDao)
	{
		this.conditionStageDao = conditionStageDao;
	}


	/**
	 * @return the conditionAreaDao
	 */
	public ConditionAreaDao getConditionAreaDao()
	{
		return conditionAreaDao;
	}


	/**
	 * @param conditionAreaDao
	 *           the conditionAreaDao to set
	 */
	public void setConditionAreaDao(final ConditionAreaDao conditionAreaDao)
	{
		this.conditionAreaDao = conditionAreaDao;
	}


	/**
	 * @return the conditionDao
	 */
	public ConditionDao getConditionDao()
	{
		return conditionDao;
	}


	/**
	 * @param conditionDao
	 *           the conditionDao to set
	 */
	public void setConditionDao(final ConditionDao conditionDao)
	{
		this.conditionDao = conditionDao;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.storefront.articles.ConditionsService#getConditonForCode(java.lang.String)
	 */
	@Override
	public ConditionsModel getConditonForCode(final String conditionCode)
	{
		validateParameterNotNull(conditionCode, "Parameter code must not be null");
		final List<ConditionsModel> conditions = conditionDao.findConditionByCode(conditionCode);

		validateIfSingleResult(conditions, format("Condition with code '%s' not found!", conditionCode),
				format("Condition code '%s' is not unique, %d conditions found!", conditionCode, Integer.valueOf(conditions.size())));

		return conditions.get(0);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.articles.ConditionService#getConditionByAreaDetail(java.lang.String)
	 */
	@Override
	public ConditionsAreaModel getConditionByAreaDetail(final String conditionCode)
	{
		validateParameterNotNull(conditionCode, "Parameter code must not be null");
		final List<ConditionsAreaModel> conditions = conditionAreaDao.findConditionByArea(conditionCode);

		validateIfSingleResult(conditions, format("Condition with code '%s' not found!", conditionCode),
				format("Condition code '%s' is not unique, %d conditions found!", conditionCode, Integer.valueOf(conditions.size())));

		return conditions.get(0);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.articles.ConditionService#getConditionByStageDetail(java.lang.String)
	 */
	@Override
	public ConditionsStageModel getConditionByStageDetail(final String conditionCode)
	{
		validateParameterNotNull(conditionCode, "Parameter code must not be null");
		final List<ConditionsStageModel> conditions = conditionStageDao.findConditionByStage(conditionCode);

		validateIfSingleResult(conditions, format("Condition with code '%s' not found!", conditionCode),
				format("Condition code '%s' is not unique, %d conditions found!", conditionCode, Integer.valueOf(conditions.size())));

		return conditions.get(0);
	}


}
