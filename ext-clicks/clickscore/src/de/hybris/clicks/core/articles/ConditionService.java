/**
 *
 */
package de.hybris.clicks.core.articles;

import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.clicks.core.model.ConditionsStageModel;


/**
 * @author baskar.lakshmanan
 *
 */
public interface ConditionService
{
	ConditionsModel getConditonForCode(String conditionCode);

	/**
	 * @param conditionCode
	 * @return
	 */
	ConditionsAreaModel getConditionByAreaDetail(String conditionCode);

	/**
	 * @param conditionCode
	 * @return
	 */
	ConditionsStageModel getConditionByStageDetail(String conditionCode);
}
