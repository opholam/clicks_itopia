/**
 *
 */
package de.hybris.clicks.core.articles.service;

import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ConditionsModel;

import java.util.List;


/**
 * @author siva.reddy
 *
 */
public interface ArticleService
{
	List<ArticleModel> getArticleByArticleCode(final String articleCode);

	/**
	 * @param articleCode
	 * @return
	 */
	List<ConditionsModel> getRelatedConditionsForArticleCode(String articleTitle);

	/**
	 * @param articleCode
	 * @return
	 */
	List<ConditionsModel> getRelatedMedicinesForArticleCode(String articleTitle);

}
