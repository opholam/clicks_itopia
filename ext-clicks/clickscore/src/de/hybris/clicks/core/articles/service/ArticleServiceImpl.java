/**
 *
 */
package de.hybris.clicks.core.articles.service;

import de.hybris.clicks.core.articles.dao.ArticleDao;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ConditionsModel;

import java.util.List;

import javax.annotation.Resource;


/**
 * @author siva.reddy/Swapnil
 *
 */
public class ArticleServiceImpl implements ArticleService
{
	@Resource(name = "articleDao")
	private ArticleDao articleDao;

	@Override
	public List<ArticleModel> getArticleByArticleCode(final String articleCode)
	{
		return articleDao.getArticleByArticleCode(articleCode);

	}

	@Override
	public List<ConditionsModel> getRelatedConditionsForArticleCode(final String articleTitle)
	{
		return articleDao.getRelatedConditionsForArticleCode(articleTitle);

	}

	@Override
	public List<ConditionsModel> getRelatedMedicinesForArticleCode(final String articleTitle)
	{
		return articleDao.getRelatedMedicinesForArticleCode(articleTitle);

	}

}
