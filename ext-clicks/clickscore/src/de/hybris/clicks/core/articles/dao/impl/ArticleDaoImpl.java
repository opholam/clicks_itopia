/**
 *
 */
package de.hybris.clicks.core.articles.dao.impl;


import de.hybris.clicks.core.articles.dao.ArticleDao;
import de.hybris.clicks.core.enums.ConditionTypeEnum;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


/**
 * @author siva.reddy/Swapnil
 *
 */
public class ArticleDaoImpl implements ArticleDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<ArticleModel> getArticleByArticleCode(final String articleTitle)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("articleTitle", articleTitle);
		final String query = "SELECT {" + ArticleModel.PK + "} FROM {" + ArticleModel._TYPECODE + "}" + " WHERE {"
				+ ArticleModel.ARTICLETITLE + "} = ?articleTitle";
		//final String query = "select {pk} from {article} where {articleTitle}=" + articleCode;
		//final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		final SearchResultImpl<ArticleModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<ArticleModel> result = searchResult.getResult();

		return result == null ? Collections.EMPTY_LIST : result;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facades.article.dao.ArticleDao#getRelatedConditionsForArticleCode(java.lang.String)
	 */
	@Override
	public List<ConditionsModel> getRelatedConditionsForArticleCode(final String articleTitle)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("articleTitle", articleTitle);
		final String query = getQuery("CONDITIONS", "");
		final SearchResultImpl<ConditionsModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<ConditionsModel> result = searchResult.getResult();

		return result == null ? Collections.EMPTY_LIST : result;
	}

	/**
	 * @return
	 */
	private String getQuery(final String conditionType1, final String conditionType2)
	{
		final String query = "SELECT {" + ConditionsModel.PK + "} FROM {" + ConditionsModel._TYPECODE + " AS c JOIN "
				+ ConditionTypeEnum._TYPECODE + " AS ct ON {c:conditionType}={ct:pk} JOIN " + ArticleModel._TYPECODE
				+ " AS art ON {c:article} = {art:pk}}" + " WHERE {ct:code} IN ('" + conditionType1 + "', '" + conditionType2
				+ "') AND {art:articleTitle} = ?articleTitle";
		return query;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.facades.article.dao.ArticleDao#getRelatedMedicinesForArticleCode(java.lang.String)
	 */
	@Override
	public List<ConditionsModel> getRelatedMedicinesForArticleCode(final String articleTitle)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("articleTitle", articleTitle);
		final String query = getQuery("MEDICINES", "VITAMINSNSUPPLEMENTS");
		final SearchResultImpl<ConditionsModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<ConditionsModel> result = searchResult.getResult();

		return result == null ? Collections.EMPTY_LIST : result;
	}
}
