/**
 *
 */
package de.hybris.clicks.core.articles.dao.impl;

import de.hybris.clicks.core.articles.dao.ConditionAreaDao;
import de.hybris.clicks.core.model.ConditionsAreaModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public class DefaultConditionAreaDao extends DefaultGenericDao<ConditionsAreaModel> implements ConditionAreaDao
{

	public DefaultConditionAreaDao(final String typecode)
	{
		super(typecode);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.articles.dao.ConditionDao#findConditionByArea(java.lang.String)
	 */
	@Override
	public List<ConditionsAreaModel> findConditionByArea(final String conditionCode)
	{
		return find(Collections.singletonMap(ConditionsAreaModel.CONDITIONAREAID, (Object) conditionCode));
	}


}
