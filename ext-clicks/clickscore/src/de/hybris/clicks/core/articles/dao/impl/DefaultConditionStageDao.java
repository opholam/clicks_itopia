/**
 *
 */
package de.hybris.clicks.core.articles.dao.impl;

import de.hybris.clicks.core.articles.dao.ConditionStageDao;
import de.hybris.clicks.core.model.ConditionsStageModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public class DefaultConditionStageDao extends DefaultGenericDao<ConditionsStageModel> implements ConditionStageDao
{

	public DefaultConditionStageDao(final String typecode)
	{
		super(typecode);
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.articles.dao.ConditionDao#findConditionByStage(java.lang.String)
	 */
	@Override
	public List<ConditionsStageModel> findConditionByStage(final String conditionCode)
	{
		return find(Collections.singletonMap(ConditionsStageModel.CONDITIONSTAGEID, (Object) conditionCode));
	}

}
