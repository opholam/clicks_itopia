/**
 *
 */
package de.hybris.clicks.core.articles.dao;

import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * @author baskar.lakshmanan
 *
 */
public interface ConditionDao extends Dao
{
	List<ConditionsModel> findConditionByCode(String conditionCode);
}
