/**
 *
 */
package de.hybris.clicks.core.product.impl;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 * 
 */
public class ClicksProductModelUrlResolver extends DefaultProductModelUrlResolver
{
	private static final Logger LOG = Logger.getLogger(ClicksProductModelUrlResolver.class);

	private final String CACHE_KEY = ClicksProductModelUrlResolver.class.getName();

	@Override
	protected CategoryModel getPrimaryCategoryForProduct(final ProductModel product)
	{
		for (final CategoryModel category : product.getSupercategories())
		{
			if (!(category instanceof ClassificationClassModel) && category.getCode().startsWith("OH"))
			{
				return category;
			}
		}
		return null;
	}

	@Override
	protected ProductModel getBaseProduct(final ProductModel product)
	{
		final ProductModel current = product;

		//		while ((current instanceof VariantProductModel))
		//		{
		//			final ProductModel baseProduct = ((VariantProductModel) current).getBaseProduct();
		//			if (baseProduct == null)
		//			{
		//				break;
		//			}
		//
		//			current = baseProduct;
		//		}

		return current;
	}

	@Override
	protected String resolveInternal(final ProductModel source)
	{
		String url = super.resolveInternal(source);
		try
		{
			if (url.contains("{brand-name}"))
			{
				url = url.replace("{brand-name}", urlSafe(getBrandName(source)).toLowerCase());
			}

			if (url.contains("{online-title}"))
			{
				url = url.replace("{online-title}", urlSafe(getOnlineTitle(source).toLowerCase()));
			}
		}
		catch (final Exception e)
		{
			//
		}

		url = url.replaceAll("%", "");
		url = url.replaceFirst("\\?", "");
		return url;
	}

	/**
	 * @param source
	 * @return
	 */
	private String getBrandName(final ProductModel productModel)
	{
		String brandName = "";
		if (productModel instanceof ClicksApparelSizeVariantModel)
		{
			final ClicksApparelSizeVariantModel sizeVariantModel = (ClicksApparelSizeVariantModel) productModel;
			final ApparelStyleVariantProductModel apparelStyleModel = (ApparelStyleVariantProductModel) sizeVariantModel
					.getBaseProduct();
			if (null != apparelStyleModel.getBaseProduct())
			{
				for (final CategoryModel categoryModel : apparelStyleModel.getBaseProduct().getSupercategories())
				{
					for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
					{
						if ("brands".equalsIgnoreCase(innerCategoryModel.getCode()))
						{
							brandName = categoryModel.getName();
						}
					}
				}
			}
		}
		else if (productModel instanceof ApparelStyleVariantProductModel)
		{
			final ApparelStyleVariantProductModel styleVariantModel = (ApparelStyleVariantProductModel) productModel;
			if (null != styleVariantModel.getBaseProduct())
			{
				for (final CategoryModel categoryModel : styleVariantModel.getBaseProduct().getSupercategories())
				{
					for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
					{
						if ("brands".equalsIgnoreCase(innerCategoryModel.getCode()))
						{
							brandName = categoryModel.getName();
						}
					}
				}
			}

		}
		return brandName;
	}

	private String getOnlineTitle(final ProductModel productModel)
	{

		String onlineTitle = "";
		if (productModel instanceof ClicksApparelSizeVariantModel)
		{
			final ClicksApparelSizeVariantModel sizeVariant = (ClicksApparelSizeVariantModel) productModel;
			if (StringUtils.isNotBlank(sizeVariant.getSizeVariantTitle()))
			{
				onlineTitle = sizeVariant.getSizeVariantTitle();
			}
		}
		else if (productModel instanceof ApparelStyleVariantProductModel)
		{
			final ApparelStyleVariantProductModel styleVariant = (ApparelStyleVariantProductModel) productModel;
			if (StringUtils.isNotBlank(styleVariant.getStyleVariantTitle()))
			{
				onlineTitle = styleVariant.getStyleVariantTitle();
			}
		}
		else
		{
			if (StringUtils.isNotBlank(productModel.getBaseProductTitle()))
			{
				onlineTitle = productModel.getBaseProductTitle();
			}
			else
			{
				onlineTitle = productModel.getName();
			}

		}

		return onlineTitle;
	}

	@Override
	protected String urlSafe(final String text)
	{

		String cleanText = super.urlSafe(text);
		try
		{
			cleanText = URLDecoder.decode(cleanText, "UTF-8");
			if (cleanText.contains("&"))
			{
				cleanText = cleanText.replaceAll("&", "and");
			}
			cleanText = cleanText.replaceAll("[^a-zA-Z0-9] ", " ");
			cleanText = cleanText.replaceAll(" ", "-");
			cleanText = cleanText.replace("\'", "");
			cleanText = cleanText.replaceAll(",", "");
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error("Error in ClicksProductModelUrlResolver class and method is urlSafe() : " + e.getMessage());
		}
		return cleanText;
	}

	@Override
	protected String getKey(final ProductModel source)
	{
		return this.CACHE_KEY + "." + source.getPk().toString();
	}
}
