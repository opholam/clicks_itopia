/**
 *
 */
package de.hybris.clicks.core.product.impl;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.clicks.core.model.ListItemsSizeVariantModel;
import de.hybris.clicks.core.model.PartnerModel;
import de.hybris.clicks.core.model.StickerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;


/**
 * @author abhaydh
 *
 */
public class ClicksProductBasicPopulator extends ProductBasicPopulator<ProductModel, ProductData>
{

	private static final Logger LOG = Logger.getLogger(ClicksProductBasicPopulator.class);

	@Resource(name = "promotionsService")
	private PromotionsService promotionsService;
	@Resource(name = "timeService")
	private TimeService timeService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	protected PromotionsService getPromotionsService()
	{
		return promotionsService;
	}


	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}

	protected TimeService getTimeService()
	{
		return timeService;
	}


	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}


	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService
	 *           the catalogVersionService to set
	 */
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}


	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator#populate(de.hybris.platform
	 * .core.model.product.ProductModel, de.hybris.platform.commercefacades.product.data.ProductData)
	 */
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{

		productData.setName((String) getProductAttribute(productModel, ProductModel.NAME));
		populateHowToUseTab(productModel, productData);
		if (productModel.getSummary() != null)
		{
			productData.setSummary((String) getProductAttribute(productModel, ProductModel.SUMMARY));
		}

		if (productModel.getBaseProductTitle() != null)
		{
			productData.setBaseProductTitle(productModel.getBaseProductTitle());
		}

		productData.setAverageRating(productModel.getAverageRating());
		if (productModel.getVariantType() != null)
		{
			productData.setVariantType(productModel.getVariantType().getCode());
		}

		if (!productModel.getPartnerRewards().isEmpty())
		{
			productData.setProdRewardPoints(productModel.getPartnerRewards().get(0).getPointValue());
		}

		if (productModel instanceof VariantProductModel)
		{
			final VariantProductModel variantProduct = (VariantProductModel) productModel;
			productData.setBaseProduct(variantProduct.getBaseProduct() != null ? variantProduct.getBaseProduct().getCode() : null);
		}

		final List<String> nonPromoStickerUrls = new ArrayList<String>();

		if (CollectionUtils.isNotEmpty(productModel.getPartnerRewards()))
		{
			for (final PartnerModel partnerModel : productModel.getPartnerRewards())
			{
				if (partnerModel.getPartnerId().equalsIgnoreCase(Config.getParameter("clicks.property.vithc")))
				{
					final String stickerUrl = getStickerUrlFromMediaCode(Config.getParameter("clickscore.property.partnerproduct"));
					if (null != stickerUrl)
					{
						nonPromoStickerUrls.add(stickerUrl);
					}
				}
			}
		}


		if (productModel instanceof ApparelStyleVariantProductModel)
		{
			final ApparelStyleVariantProductModel styleVariantProduct = (ApparelStyleVariantProductModel) productModel;
			productData.setStyleVariantTitle(styleVariantProduct.getStyleVariantTitle() != null ? styleVariantProduct
					.getStyleVariantTitle() : null);
			productData.setStyle(styleVariantProduct.getStyle() != null ? styleVariantProduct.getStyle() : null);
			getPromotions(productModel, productData);
			if (CollectionUtils.isNotEmpty(productData.getPotentialPromotions()))
			{
				for (final VariantProductModel sizeVariant : styleVariantProduct.getVariants())
				{
					getPromotions(sizeVariant, productData);
					if (CollectionUtils.isNotEmpty(productData.getPotentialPromotions()))
					{
						break;
					}
				}
			}
		}

		if (productModel instanceof ClicksApparelSizeVariantModel)
		{
			final ClicksApparelSizeVariantModel sizeVariantProduct = (ClicksApparelSizeVariantModel) productModel;
			productData.setSizeVariantTitle(sizeVariantProduct.getSizeVariantTitle() != null ? sizeVariantProduct
					.getSizeVariantTitle() : null);
			getPromotions(productModel, productData);
			if (sizeVariantProduct.getNewness() != null)
			{
				final String stickerUrl = getStickerUrlFromMediaCode(Config.getParameter("clickscore.property.newproduct"));
				if (StringUtils.isNotBlank(stickerUrl))
				{
					nonPromoStickerUrls.add(stickerUrl);
				}
			}
		}
		if (null != productModel.getMaxOrderQuantity())
		{
			productData.setMaxOrderQuantity(productModel.getMaxOrderQuantity().toString());

		}
		if (null != productModel.getMinOrderQuantity())
		{
			productData.setMinOrderQuantity(productModel.getMinOrderQuantity().toString());

		}
		productData.setNonPromoStickerUrls(nonPromoStickerUrls);
		productData.setPurchasable(Boolean.valueOf(productModel.getVariantType() == null && isApproved(productModel)));
	}

	/**
	 * Gets the sticker url from media code.
	 *
	 * @param mediaCode
	 *           the media code
	 * @return the sticker url from media code
	 */
	private String getStickerUrlFromMediaCode(final String mediaCode)
	{
		MediaModel mediaModel = new MediaModel();
		mediaModel.setCode(mediaCode);
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.contentCatalog.name"), Config.getParameter("clicks.catalog.online.version"));
		mediaModel.setCatalogVersion(catalogVersionModel);
		try
		{
			mediaModel = flexibleSearchService.getModelByExample(mediaModel);

			if (null != mediaModel)
			{
				return mediaModel.getURL();
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in getting sticker model" + e.getMessage());
		}
		return "";

	}



	/**
	 * @param productModel
	 * @param productData
	 */
	public void getPromotions(final ProductModel productModel, final ProductData productData)
	{
		final List<ProductPromotionModel> promotions = getProductPromotions(productModel);
		if (null != promotions && !promotions.isEmpty())
		{
			final Set<String> stickerUrls = new HashSet<String>();
			final List<PromotionData> promoListData = new ArrayList<PromotionData>();
			String desc = "";
			boolean isSame = false;

			final List<String> dateSet = new ArrayList<>();
			//final List<String> disclaimerSet = new ArrayList<>();

			for (final ProductPromotionModel promo : promotions)
			{
				if (dateSet.contains(promo.getEndDate().toString()))
				{
					isSame = true;
					break;
				}
				else
				{
					dateSet.add(promo.getEndDate().toString());
				}
			}
			int index = 0;
			for (final ProductPromotionModel promo : promotions)
			{
				final PromotionData promData = new PromotionData();
				desc = promo.getDescription();
				if (StringUtils.isNotEmpty(promo.getDisclaimer()))
				{
					desc = desc + ". " + promo.getDisclaimer() + ".";
				}
				if (promo.getEndDate() != null)
				{
					final DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
					//final Date date = promo.getEndDate();
					//final Calendar cal = Calendar.getInstance();
					//cal.setTime(date);
					if (!isSame || index++ == promotions.size() - 1)
					{
						final String validity = Config.getString("text.promotion.endDate", "").concat(" ")
								.concat(dateFormat.format(promo.getEndDate()));
						desc = StringUtils.isNotEmpty(desc) ? desc + " " + validity : validity;

					}
					//	desc = StringUtils.isNotEmpty(desc) ? desc + " " + Config.getString("text.promotion.endDate", "") + " "
					//		+ dateFormat.format(promo.getEndDate()) : dateFormat.format(promo.getEndDate());
				}

				promData.setDescription(desc);

				promData.setCode(promo.getCode());
				promData.setPromotionType(promo.getPromotionType());
				if (promo.getProductBanner() != null)
				{
					final ImageData img = new ImageData();
					img.setUrl(promo.getProductBanner().getURL());
					promData.setProductBanner(img);
				}

				final Set<String> stickerDataUrls = new HashSet<String>();
				StickerModel stickerModel = null;
				for (final String stickerId : promo.getStickers())
				{
					try
					{
						stickerModel = new StickerModel();
						stickerModel.setStickerID(stickerId);
						stickerModel = flexibleSearchService.getModelByExample(stickerModel);
						if (null != stickerModel && null != stickerModel.getStickerImage()
								&& !stickerUrls.contains(stickerModel.getStickerImage().getURL()))
						{
							stickerDataUrls.add(stickerModel.getStickerImage().getURL());
							stickerUrls.add(stickerModel.getStickerImage().getURL());
						}
					}
					catch (final Exception e)
					{
						//
					}
				}
				promData.setStickerMediaUrls(stickerDataUrls);


				promoListData.add(promData);

			}

			productData.setPotentialPromotions(promoListData);
		}
	}


	/**
	 * Populate how to use tab.
	 *
	 * @param productModel
	 *           the product model
	 * @param productData
	 *           the product data
	 */
	private void populateHowToUseTab(final ProductModel productModel, final ProductData productData)
	{
		ClicksApparelSizeVariantModel sizeVariantModel = null;
		if (productModel instanceof ClicksApparelSizeVariantModel)
		{
			sizeVariantModel = (ClicksApparelSizeVariantModel) productModel;
			final ApparelStyleVariantProductModel apparelStyleModel = (ApparelStyleVariantProductModel) sizeVariantModel
					.getBaseProduct();
			if (null != apparelStyleModel.getBaseProduct())
			{
				for (final CategoryModel categoryModel : apparelStyleModel.getBaseProduct().getSupercategories())
				{
					for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
					{
						if ("brands".equalsIgnoreCase(innerCategoryModel.getCode()))
						{
							productData.setBrand(categoryModel.getName());
						}
					}
				}
			}
		}
		else if (productModel instanceof ApparelStyleVariantProductModel)
		{
			final ApparelStyleVariantProductModel styleVariantModel = (ApparelStyleVariantProductModel) productModel;
			if (null != styleVariantModel.getBaseProduct())
			{
				for (final CategoryModel categoryModel : styleVariantModel.getBaseProduct().getSupercategories())
				{
					for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
					{
						if ("brands".equalsIgnoreCase(innerCategoryModel.getCode()))
						{
							productData.setBrand(categoryModel.getName());
						}
					}
				}
			}

			if (CollectionUtils.isNotEmpty(styleVariantModel.getVariants()))
			{
				sizeVariantModel = (ClicksApparelSizeVariantModel) styleVariantModel.getVariants().iterator().next();
			}
		}
		if (null != sizeVariantModel)
		{
			if (StringUtils.isNotBlank(sizeVariantModel.getProductQuantity()))
			{
				productData.setProductQuantity(sizeVariantModel.getProductQuantity());
			}
			if (StringUtils.isNotBlank(sizeVariantModel.getCopyWriteDescription()))
			{
				productData.setCopyWriteDescription(sizeVariantModel.getCopyWriteDescription());
			}

			if (StringUtils.isNotBlank(sizeVariantModel.getUsageInstructions())
					|| StringUtils.isNotBlank(sizeVariantModel.getHowToUse())
					|| StringUtils.isNotBlank(sizeVariantModel.getServingSizeFull()))
			{
				productData.setServingSize(sizeVariantModel.getServingSizeFull());
				productData.setUsageInstructions(sizeVariantModel.getUsageInstructions());
				productData.setHowToUse(sizeVariantModel.getHowToUse());
				productData.setIsUsageDataAvailable(Boolean.TRUE);
			}
			else if (StringUtils.isNotBlank(sizeVariantModel.getStorageInstructions()))
			{
				productData.setStorageInstructions(sizeVariantModel.getStorageInstructions());
				productData.setIsStorageDataAvailable(Boolean.TRUE);
			}
			else if (StringUtils.isNotBlank(sizeVariantModel.getWarnings()))
			{
				productData.setWarnings(sizeVariantModel.getWarnings());
				productData.setIsHazardsDataAvailable(Boolean.TRUE);
			}
			populateDetailedInfoTab(sizeVariantModel, productData);
		}
	}

	private void populateDetailedInfoTab(final ClicksApparelSizeVariantModel sizeVariantModel, final ProductData productData)
	{
		for (final ListItemsSizeVariantModel lisItem : sizeVariantModel.getListItemsForProduct())
		{
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.package"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setPackageType(lisItem.getItemValList());
			}
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.kosher"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setKosherCode(lisItem.getItemValList());
			}
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.hilal"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setHilalCode(lisItem.getItemValList());
			}
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.recycle"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setRecycleCode(lisItem.getItemValList());
			}
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.life"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setLifestyle(lisItem.getItemValList());
			}
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.free"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setFreeFrom(lisItem.getItemValList());
			}
			if (StringUtils.isNotBlank(lisItem.getName())
					&& lisItem.getName().toLowerCase().contains(Config.getParameter("clickscore.property.health"))
					&& !CollectionUtils.isEmpty(lisItem.getItemValList()))
			{
				productData.setHealthClaims(lisItem.getItemValList());
			}
		}
		if (StringUtils.isNotBlank(sizeVariantModel.getOtherDescription())
				|| StringUtils.isNotBlank(sizeVariantModel.getDescription()) || StringUtils.isNotBlank(sizeVariantModel.getFlavor())
				|| StringUtils.isNotBlank(sizeVariantModel.getPackSize())
				|| StringUtils.isNotBlank(sizeVariantModel.getQtyInPackage())
				|| StringUtils.isNotBlank(sizeVariantModel.getMarketingDescription())
				|| StringUtils.isNotBlank(sizeVariantModel.getBrandName())
				|| StringUtils.isNotBlank(sizeVariantModel.getEndorsements())
				|| !CollectionUtils.isEmpty(productData.getPackageType()))
		{
			productData.setOtherDescription(sizeVariantModel.getOtherDescription());
			productData.setDescription(sizeVariantModel.getDescription());
			productData.setPackSize(sizeVariantModel.getPackSize());
			productData.setQtyinPack(sizeVariantModel.getQtyInPackage());
			productData.setMarketingDescription(sizeVariantModel.getMarketingDescription());
			productData.setEndorsements(sizeVariantModel.getEndorsements());
			productData.setIsProductDescDataAvailable(Boolean.TRUE);
		}
		else if (!CollectionUtils.isEmpty(productData.getKosherCode()) || !CollectionUtils.isEmpty(productData.getHilalCode())
				|| !CollectionUtils.isEmpty(productData.getRecycleCode()) || !CollectionUtils.isEmpty(productData.getLifestyle())
				|| !CollectionUtils.isEmpty(productData.getFreeFrom()) || !CollectionUtils.isEmpty(productData.getHealthClaims()))
		{
			productData.setIsLifestyeDataAvailable(Boolean.TRUE);
		}
		else if (StringUtils.isNotBlank(sizeVariantModel.getManufacturerName())
				|| StringUtils.isNotBlank(sizeVariantModel.getManufacturerAddress())
				|| StringUtils.isNotBlank(sizeVariantModel.getManufacturerPhone())
				|| StringUtils.isNotBlank(sizeVariantModel.getManufacturerURL())
				|| StringUtils.isNotBlank(sizeVariantModel.getManufacturerEmail())
				|| StringUtils.isNotBlank(sizeVariantModel.getDistributor())
				|| StringUtils.isNotBlank(sizeVariantModel.getDistributorPhone())
				|| StringUtils.isNotBlank(sizeVariantModel.getDistributorEmail())
				|| StringUtils.isNotBlank(sizeVariantModel.getDistributorURL())
				|| StringUtils.isNotBlank(sizeVariantModel.getDistributorAddress())
				|| StringUtils.isNotBlank(sizeVariantModel.getCountryOfOrigin()))
		{
			productData.setManufacturer(sizeVariantModel.getManufacturerName());
			productData.setManufacturerAddress(sizeVariantModel.getManufacturerAddress());
			productData.setManufacturerPhone(sizeVariantModel.getManufacturerPhone());
			productData.setManufacturerURL(sizeVariantModel.getManufacturerURL());
			productData.setManufacturerEmail(sizeVariantModel.getManufacturerEmail());
			productData.setDistributor(sizeVariantModel.getDistributor());
			productData.setDistributorPhone(sizeVariantModel.getDistributorPhone());
			productData.setDistributorEmail(sizeVariantModel.getDistributorEmail());
			productData.setDistributorURL(sizeVariantModel.getDistributorURL());
			productData.setDistributorAddress(sizeVariantModel.getDistributorAddress());
			productData.setCountryOfOrigin(productData.getCountryOfOrigin());
			productData.setIsManufacturerDataAvailable(Boolean.TRUE);
		}
		populateIngredietsTab(sizeVariantModel, productData);
	}

	private void populateIngredietsTab(final ClicksApparelSizeVariantModel sizeVariantModel, final ProductData productData)
	{
		if (StringUtils.isNotBlank(sizeVariantModel.getIngredients()))
		{
			productData.setIngredients(sizeVariantModel.getIngredients());
			productData.setIsIngredientsDataAvailable(Boolean.TRUE);
		}
		else if (StringUtils.isNotBlank(sizeVariantModel.getMeasure())
				|| StringUtils.isNotBlank(sizeVariantModel.getServingSizeFull())
				|| StringUtils.isNotBlank(sizeVariantModel.getNumberOfServings())
				|| !CollectionUtils.isEmpty(sizeVariantModel.getTableHeadingList()))
		{
			productData.setIsNutritionalDataAvailable(Boolean.TRUE);
			productData.setMeasure(sizeVariantModel.getMeasure());
			productData.setServingSizeFull(sizeVariantModel.getServingSizeFull());
			productData.setNumberOfServings(sizeVariantModel.getNumberOfServings());
			productData.setTableHeadings(sizeVariantModel.getTableHeadingList());
			productData.setNutrientItemsList(sizeVariantModel.getNutrientItemList());
		}
	}

	/**
	 * Gets the product promotions.
	 *
	 * @param productModel
	 *           the product model
	 * @return the product promotions
	 */
	private List<ProductPromotionModel> getProductPromotions(final ProductModel productModel)
	{
		try
		{
			final BaseSiteModel baseSiteModel = getBaseSiteService().getCurrentBaseSite();
			if (baseSiteModel != null)
			{
				final PromotionGroupModel defaultPromotionGroup = baseSiteModel.getDefaultPromotionGroup();
				final Date currentTimeRoundedToMinute = DateUtils.round(getTimeService().getCurrentTime(), Calendar.MINUTE);

				if (defaultPromotionGroup != null)
				{
					return getPromotionsService().getProductPromotions(Collections.singletonList(defaultPromotionGroup), productModel,
							true, currentTimeRoundedToMinute);
				}
			}
		}
		catch (final Exception e)
		{
			//
		}
		return null;
	}
}
