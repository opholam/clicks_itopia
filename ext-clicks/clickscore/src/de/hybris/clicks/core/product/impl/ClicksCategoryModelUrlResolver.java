/**
 *
 */
package de.hybris.clicks.core.product.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 * 
 */
public class ClicksCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver
{

	private static final Logger LOG = Logger.getLogger(ClicksCategoryModelUrlResolver.class);

	private final String CACHE_KEY = ClicksCategoryModelUrlResolver.class.getName();

	@Override
	protected List<CategoryModel> getCategoryPath(final CategoryModel category)
	{
		final Collection paths = getCommerceCategoryService().getPathsForCategory(category);

		final List<CategoryModel> categoryList = ((List) paths.iterator().next());
		List<CategoryModel> catList;
		LOG.debug("Category path list size : " + categoryList.size());
		if (categoryList.size() == 1 || categoryList.size() == 0)
		{
			catList = categoryList;
		}
		else if (categoryList.size() >= 4)
		{
			catList = categoryList.subList(1, 4);
		}
		else
		{
			catList = categoryList.subList(1, categoryList.size());
		}

		return catList;
	}

	@Override
	protected String urlSafe(final String text)
	{
		String cleanText = super.urlSafe(text);
		try
		{
			cleanText = URLDecoder.decode(cleanText, "UTF-8");
			if (cleanText.contains("&"))
			{
				cleanText = cleanText.replaceAll("&", "and");
			}
			cleanText = cleanText.replaceAll("[^a-zA-Z0-9] ", " ");
			cleanText = cleanText.replaceAll(" ", "-");
			cleanText = cleanText.replace("\'", "");
			cleanText = cleanText.replaceAll(",", "");
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.error("Error in ClicksCategoryModelUrlResolver class and method is urlSafe() : " + e.getMessage());
		}
		return cleanText;
	}

	@Override
	protected String buildPathString(final List<CategoryModel> path)
	{
		return super.buildPathString(path).toLowerCase();
	}

	@Override
	protected String getKey(final CategoryModel source)
	{
		return this.CACHE_KEY + "." + source.getPk().toString();
	}

}
