/**
 *
 */
package de.hybris.clicks.core.user.event;

import de.hybris.clicks.core.model.NotifyStockProcessModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author shruti.jhamb
 *
 */
public class StockAvailabilityNotifyEventListener extends AbstractSiteEventListener<StockAvailabilityNotifyEvent>
{


	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final StockAvailabilityNotifyEvent stockAvailabilityNotifyEvent)
	{
		final NotifyStockProcessModel notifyStockProcessModel = (NotifyStockProcessModel) businessProcessService.createProcess(
				"process" + System.currentTimeMillis(), "stockAvailabilityNotifyEmailProcess");
		//notifyStockProcessModel.setEmail(stockAvailabilityNotifyEvent.getEmail());
		notifyStockProcessModel.setCustomer(stockAvailabilityNotifyEvent.getCustomer());
		notifyStockProcessModel.setSite(stockAvailabilityNotifyEvent.getSite());
		notifyStockProcessModel.setStore(stockAvailabilityNotifyEvent.getBaseStore());
		notifyStockProcessModel.setStoreName(stockAvailabilityNotifyEvent.getStoreName());
		notifyStockProcessModel.setProductCode(stockAvailabilityNotifyEvent.getProductCode());
		getModelService().save(notifyStockProcessModel);
		getBusinessProcessService().startProcess(notifyStockProcessModel);
	}

	@Override
	protected boolean shouldHandleEvent(final StockAvailabilityNotifyEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2C.equals(site.getChannel());
	}
}
