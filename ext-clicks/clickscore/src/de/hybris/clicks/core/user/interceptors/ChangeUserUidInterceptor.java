package de.hybris.clicks.core.user.interceptors;

import de.hybris.clicks.core.user.event.CustomerUidChangeEvent;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;



/**
 *
 */

/**
 * @author manikandan.r
 *
 */
public class ChangeUserUidInterceptor implements PrepareInterceptor<UserModel>
{
	private static final Logger LOG = Logger.getLogger(ChangeUserUidInterceptor.class);

	private BaseStoreService baseStoreService;
	private BaseSiteService baseSiteService;
	private EventService eventService;
	private CommonI18NService commonI18NService;

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}


	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}


	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}


	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.interceptor.PrepareInterceptor#onPrepare(java.lang.Object,
	 * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
	 */
	@Override
	public void onPrepare(final UserModel user, final InterceptorContext ctx) throws InterceptorException
	{
		try
		{
			if (!ctx.isNew(user) && ctx.isModified(user, UserModel.UID))
			{
				final User userSource = ctx.getModelService().getSource(user);
				final Set<String> oldIds = new HashSet<String>();
				//oldIds.addAll(user.getOldUIDs());
				oldIds.add(userSource.getUid());
				user.setOldUIDs(oldIds);
				if (user instanceof CustomerModel && !user.getUid().equalsIgnoreCase(userSource.getUid()))
				{
					final CustomerModel customer = (CustomerModel) user;
					eventService.publishEvent(initializeEvent(new CustomerUidChangeEvent(), customer));
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception while updating old id", e);
		}

	}


	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event, final CustomerModel customerModel)
	{
		event.setBaseStore(baseStoreService.getAllBaseStores().iterator().next());
		event.setSite(baseSiteService.getAllBaseSites().iterator().next());
		event.setCustomer(customerModel);
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setCurrency(commonI18NService.getCurrency("ZAR"));
		return event;
	}

}
