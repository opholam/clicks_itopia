/**
 *
 */
package de.hybris.clicks.core.user.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * @author shruti.jhamb
 *
 */
public class StockAvailabilityNotifyEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{
	private String productCode;
	private String storeName;

	public StockAvailabilityNotifyEvent()
	{
	}

	public StockAvailabilityNotifyEvent(final String productCode, final String storeName)
	{
		this.productCode = productCode;
		this.storeName = storeName;
	}

	public String getProductCode()
	{
		return this.productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the storeName
	 */
	public String getStoreName()
	{
		return this.storeName;
	}

	/**
	 * @param storeName
	 *           the storeName to set
	 */
	public void setStoreName(final String storeName)
	{
		this.storeName = storeName;
	}

	/*
	 * private String email;
	 * 
	 * public StockAvailabilityNotifyEvent() { }
	 * 
	 * public StockAvailabilityNotifyEvent(final String email) { this.email = email; }
	 * 
	 * public String getEmail() { return this.email; }
	 * 
	 * public void setEmail(final String email) { this.email = email; }
	 */
}
