/**
 *
 */
package de.hybris.clicks.core.user.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author manikandan.r
 *
 */
public class CustomerUidChangeEventListener extends AbstractSiteEventListener<CustomerUidChangeEvent>
{


	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final CustomerUidChangeEvent customerUidChangeEvent)
	{
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = (StoreFrontCustomerProcessModel) getBusinessProcessService()
				.createProcess("customerUidChangeEmailProcess" + "-" + customerUidChangeEvent.getCustomer().getUid() + "-"
						+ System.currentTimeMillis(), "customerUidChangeEmailProcess");
		storeFrontCustomerProcessModel.setSite(customerUidChangeEvent.getSite());
		storeFrontCustomerProcessModel.setCustomer(customerUidChangeEvent.getCustomer());
		storeFrontCustomerProcessModel.setLanguage(customerUidChangeEvent.getLanguage());
		storeFrontCustomerProcessModel.setCurrency(customerUidChangeEvent.getCurrency());
		storeFrontCustomerProcessModel.setStore(customerUidChangeEvent.getBaseStore());

		final StoreFrontCustomerProcessModel oldCustomerProcessModel = (StoreFrontCustomerProcessModel) getBusinessProcessService()
				.createProcess("customerUidChangeOldEmailProcess" + "-" + customerUidChangeEvent.getCustomer().getUid() + "-"
						+ System.currentTimeMillis(), "customerUidChangeOldEmailProcess");
		oldCustomerProcessModel.setSite(customerUidChangeEvent.getSite());
		oldCustomerProcessModel.setCustomer(customerUidChangeEvent.getCustomer());
		oldCustomerProcessModel.setLanguage(customerUidChangeEvent.getLanguage());
		oldCustomerProcessModel.setCurrency(customerUidChangeEvent.getCurrency());
		oldCustomerProcessModel.setStore(customerUidChangeEvent.getBaseStore());

		getModelService().save(storeFrontCustomerProcessModel);
		getModelService().save(oldCustomerProcessModel);

		getBusinessProcessService().startProcess(storeFrontCustomerProcessModel);
		getBusinessProcessService().startProcess(oldCustomerProcessModel);
	}

	@Override
	protected boolean shouldHandleEvent(final CustomerUidChangeEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2C.equals(site.getChannel());
	}
}
