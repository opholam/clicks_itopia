/**
 *
 */
package de.hybris.clicks.core;

import de.hybris.clicks.webclient.customer.client.CustomerDetailsRequestClient;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 *
 */
public class CRMCustomerRefreshTaskRunner implements TaskRunner<TaskModel>
{
	private final static Logger LOG = Logger.getLogger(CRMCustomerRefreshTaskRunner.class);
	@Resource
	private CustomerDetailsRequestClient customerDetailsRequestClient;



	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}

	private CustomerData populateDetailsRequestData(final CustomerModel form, final CustomerData customer)
	{
		if (null != form.getMemberID())
		{
			customer.setMemberID(form.getMemberID());
		}
		if (null != form.getUid())
		{
			customer.setEmail(getTitleCase(form.getUid()));
		}
		if (null != form.getSAResident() && form.getSAResident().booleanValue())
		{
			customer.setSAResident(Boolean.TRUE);
		}
		else
		{
			customer.setSAResident(Boolean.FALSE);
		}
		if (null != form.getRSA_ID())
		{
			customer.setIDnumber(form.getRSA_ID());
		}
		if (null != form.getNonRSA_DOB())
		{
			customer.setNonRSA_DOB(form.getNonRSA_DOB());
		}
		if (null != form.getCustomerID())
		{
			customer.setCustomerID(form.getCustomerID());
		}
		return customer;

	}

	/**
	 * @return the customerDetailsRequestClient
	 */
	public CustomerDetailsRequestClient getCustomerDetailsRequestClient()
	{
		return customerDetailsRequestClient;
	}

	/**
	 * @param customerDetailsRequestClient
	 *           the customerDetailsRequestClient to set
	 */
	public void setCustomerDetailsRequestClient(final CustomerDetailsRequestClient customerDetailsRequestClient)
	{
		this.customerDetailsRequestClient = customerDetailsRequestClient;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.task.TaskRunner#handleError(de.hybris.platform.task.TaskService,
	 * de.hybris.platform.task.TaskModel, java.lang.Throwable)
	 */
	@Override
	public void handleError(final TaskService arg0, final TaskModel arg1, final Throwable arg2)
	{
		LOG.error(arg2);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.task.TaskRunner#run(de.hybris.platform.task.TaskService,
	 * de.hybris.platform.task.TaskModel)
	 */
	@Override
	public void run(final TaskService arg0, final TaskModel arg1) throws RetryLaterException
	{
		LOG.info("In task runner ");
		try
		{
			final CustomerModel customerModel = (CustomerModel) arg1.getContextItem();
			//LOG.info("-- in StorefrontAuthenticationSuccessHandler ");
			if (null != customerModel && StringUtils.isNotEmpty(customerModel.getMemberID())
					&& null != customerModel.getSAResident())
			{
				LOG.info("calling async omnireq for user " + customerModel.getUid());
				CustomerData customer = new CustomerData();
				customer = populateDetailsRequestData(customerModel, customer);
				customer = getCustomerDetailsRequestClient().getCC_CustomerDetails(customer);
			}
		}
		catch (final Exception e)
		{
			LOG.error(e);
		}
	}
}
