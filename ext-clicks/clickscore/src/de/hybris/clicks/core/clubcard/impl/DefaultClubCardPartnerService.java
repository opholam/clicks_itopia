/**
 *
 */
package de.hybris.clicks.core.clubcard.impl;

import de.hybris.clicks.core.clubcard.ClubCardPartnerService;
import de.hybris.clicks.core.clubcard.dao.ClubCardPartnerDao;
import de.hybris.clicks.core.model.ClubCardPartnerModel;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author ashish.vyas
 *
 */
public class DefaultClubCardPartnerService implements ClubCardPartnerService
{
	private static final Logger LOG = Logger.getLogger(DefaultClubCardPartnerService.class);

	@Resource(name = "clubCardPartnerDao")
	ClubCardPartnerDao clubCardPartnerDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clubcard.ClubCardPartnerService#getClubCardPartners()
	 */
	@Override
	public List<ClubCardPartnerModel> getClubCardPartners()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("In getClubCardPartners() of DefaultClubCardPartnerService");
		}
		return clubCardPartnerDao.getClubCardPartners();
	}

}
