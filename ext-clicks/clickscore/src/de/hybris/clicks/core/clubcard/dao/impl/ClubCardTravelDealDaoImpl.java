/**
 *
 */
package de.hybris.clicks.core.clubcard.dao.impl;

import de.hybris.clicks.core.clubcard.dao.ClubCardTravelDealDao;
import de.hybris.clicks.core.model.TravelDealModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;
import java.util.Map;


/**
 * @author baskar.lakshmanan
 *
 */
public class ClubCardTravelDealDaoImpl extends DefaultGenericDao<TravelDealModel> implements ClubCardTravelDealDao
{

	public ClubCardTravelDealDaoImpl(final String typecode)
	{
		super(typecode);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clubcard.dao.ClubCardTravelDealDao#getTravelDeals(java.util.Map)
	 */
	@Override
	public List<TravelDealModel> getTravelDeals(final Map<String, Object> params)
	{
		return find(params);
	}

}
