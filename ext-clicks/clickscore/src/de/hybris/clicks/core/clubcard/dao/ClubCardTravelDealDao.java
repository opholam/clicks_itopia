/**
 *
 */
package de.hybris.clicks.core.clubcard.dao;

import de.hybris.clicks.core.model.TravelDealModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;
import java.util.Map;


/**
 * @author baskar.lakshmanan
 *
 */
public interface ClubCardTravelDealDao extends Dao
{
	List<TravelDealModel> getTravelDeals(Map<String, Object> params);
}
