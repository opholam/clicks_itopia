/**
 *
 */
package de.hybris.clicks.core.clubcard.dao;

import de.hybris.clicks.core.model.ClubCardPartnerModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 *
 */
public interface ClubCardPartnerDao extends Dao
{
	public List<ClubCardPartnerModel> getClubCardPartners();
}
