/**
 *
 */
package de.hybris.clicks.core.clubcard.dao.impl;

import de.hybris.clicks.core.clubcard.dao.ClubCardPartnerDao;
import de.hybris.clicks.core.model.ClubCardPartnerModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author ashish.vyas
 *
 */
public class DefaultClubCardPartnerDao extends DefaultGenericDao<ClubCardPartnerModel> implements ClubCardPartnerDao
{
	private static final Logger LOG = Logger.getLogger(DefaultClubCardPartnerDao.class);

	/**
	 * @param typecode
	 */
	public DefaultClubCardPartnerDao(final String typecode)
	{
		super(typecode);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.clubcard.dao.ClubCardPartnerDao#getClubCardPartners()
	 */
	@Override
	public List<ClubCardPartnerModel> getClubCardPartners()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("In getClubCardPartners() of DefaultClubCardPartnerDao");
		}
		return find();
	}

}
