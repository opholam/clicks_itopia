/**
 *
 */
package de.hybris.clicks.core.clubcard.impl;

import de.hybris.clicks.core.clubcard.ClubCardTravelDealService;
import de.hybris.clicks.core.clubcard.dao.ClubCardTravelDealDao;
import de.hybris.clicks.core.model.TravelDealModel;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


/**
 * @author baskar.lakshmanan
 *
 */
public class ClubCardTravelDealServiceImpl implements ClubCardTravelDealService
{
	@Resource(name = "clubCardTravelDealDao")
	private ClubCardTravelDealDao clubCardTravelDealDao;

	/**
	 * @return the clubCardTravelDealDao
	 */
	public ClubCardTravelDealDao getClubCardTravelDealDao()
	{
		return clubCardTravelDealDao;
	}

	/**
	 * @param clubCardTravelDealDao
	 *           the clubCardTravelDealDao to set
	 */
	public void setClubCardTravelDealDao(final ClubCardTravelDealDao clubCardTravelDealDao)
	{
		this.clubCardTravelDealDao = clubCardTravelDealDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clubcard.ClubCardTravelDealService#getTravelDeals(java.util.Map)
	 */
	@Override
	public List<TravelDealModel> getTravelDeals(final Map<String, Object> params)
	{
		return clubCardTravelDealDao.getTravelDeals(params);
	}
	//
}
