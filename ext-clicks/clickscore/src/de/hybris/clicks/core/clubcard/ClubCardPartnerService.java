/**
 *
 */
package de.hybris.clicks.core.clubcard;

import de.hybris.clicks.core.model.ClubCardPartnerModel;

import java.util.List;


/**
 * @author ashish.vyas
 *
 */
public interface ClubCardPartnerService
{
	public List<ClubCardPartnerModel> getClubCardPartners();
}
