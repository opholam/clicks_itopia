/**
 *
 */
package de.hybris.clicks.core.clubcard;

import de.hybris.clicks.core.model.TravelDealModel;

import java.util.List;
import java.util.Map;


/**
 * @author baskar.lakshmanan
 *
 */
public interface ClubCardTravelDealService
{
	List<TravelDealModel> getTravelDeals(Map<String, Object> params);
}
