/**
 *
 */
package de.hybris.clicks.core.strategies.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.persistence.security.SaltedPasswordEncoder;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * @author manikandan.gopalan
 *
 */
public class ExtSaltedPasswordEncoder extends SaltedPasswordEncoder
{
	@Resource(name = "userService")
	private UserService userService;

	@Override
	public String encode(final String uid, final String password)
	{
		if (!isSaltedAlready(password, uid))
		{
			final String userSpecificSalt = generateUserSpecificSalt(uid);
			return calculateHash(userSpecificSalt.concat(password == null ? "" : password));

		}

		return calculateHash(password);
	}

	private boolean isSaltedAlready(final String password, final String uid)
	{
		if (password == null)
		{
			return false;
		}

		return password.startsWith(generateUserSpecificSalt(uid));
	}

	@Override
	protected String generateUserSpecificSalt(final String uid)
	{
		UserModel user = null;

		try
		{
			user = userService.getUserForUID(uid);
		}
		catch (final Exception e)
		{
			//
		}
		if (null != user && StringUtils.isNotBlank(user.getPasswordAnswer()))
		{
			return user.getPasswordAnswer();
		}
		else
		{
			return getSystemSpecificSalt();
		}
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
