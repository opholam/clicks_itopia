/**
 *
 */
package de.hybris.clicks.core.strategies.impl;

import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.url.impl.LocalMediaWebURLStrategy;
import de.hybris.platform.util.MediaUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import com.google.common.base.Preconditions;


/**
 * @author manikandan.gopalan
 *
 */
public class ExtLocalMediaWebURLStrategy extends LocalMediaWebURLStrategy

{

	@Override
	public String getUrlForMedia(final MediaStorageConfigService.MediaFolderConfig config, final MediaSource mediaSource)
	{
		try
		{
			Preconditions.checkArgument(config != null, "Folder config is required to perform this operation");
			Preconditions.checkArgument(mediaSource != null, "MediaSource is required to perform this operation");

			return assembleUrl(config.getFolderQualifier(), mediaSource);
		}
		catch (final Exception e)
		{
			return super.getUrlForMedia(config, mediaSource);
		}
	}

	private String assembleUrl(final String folderQualifier, final MediaSource mediaSource)
	{
		final String result = "";
		if ((!GenericValidator.isBlankOrNull(folderQualifier)) && (!GenericValidator.isBlankOrNull(mediaSource.getLocation())))
		{
			return assembleLegacyURL(folderQualifier, mediaSource);
		}
		return result;
	}

	private String assembleLegacyURL(final String folderQualifier, final MediaSource mediaSource)
	{
		final StringBuilder sb = new StringBuilder(MediaUtil.addTrailingFileSepIfNeeded(getMediaWebRootContext()));
		sb.append("sys_").append(getTenantId()).append("/");
		if (!StringUtils.containsIgnoreCase(mediaSource.getLocation(), folderQualifier))
		{
			sb.append(folderQualifier).append("/");
		}
		sb.append(mediaSource.getLocation());
		return sb.toString();
	}

}
