/**
 *
 */
package de.hybris.clicks.core.magazine.dao;

import de.hybris.clicks.core.enums.ArticleTypesEnum;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author siva.reddy
 *
 */
public class MagazineLandingDaoImpl implements MagazineLandingDao
{
	protected static final Logger LOG = Logger.getLogger(MagazineLandingDaoImpl.class);

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.magazine.dao.MagazineLandingDao#getAllArticlesToMagazinePage(java.lang.String,
	 * java.lang.String)
	 */
	/*
	 * Method to get All Articles
	 */
	@Override
	public List<ArticleModel> getAllArticlesToMagazinePage(final String tagId, final String pageType)
	{
		LOG.debug("in MagazineLandingDao Impl " + tagId + " PageType: " + pageType);
		final List<ArticleTypesEnum> list = new ArrayList<ArticleTypesEnum>();
		if ("magazineLandingPage".equalsIgnoreCase(pageType))
		{
			list.add(ArticleTypesEnum.MAGAZINES);
			list.add(ArticleTypesEnum.CLUBCARD);
		}
		else if ("healthyLivingPage".equalsIgnoreCase(pageType))
		{
			list.add(ArticleTypesEnum.HEALTH);
			//	list.add(ArticleTypesEnum.MEDICINES);
			//	list.add(ArticleTypesEnum.CONDITIONS);
		}
		if (list.size() > 0)
		{
			if ("all".equalsIgnoreCase(tagId))
			{
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put("articleType", list);
				params.put("isActive", Boolean.TRUE);

				final String query = "SELECT {" + ArticleModel.PK + "} FROM {" + ArticleModel._TYPECODE + "} WHERE  {"
						+ ArticleModel.ARTICLETYPE + "} in (?articleType)  AND {" + ArticleModel.ISACTIVE
						+ "}=?isActive ORDER BY {CREATIONTIME} DESC";
				final SearchResultImpl<ArticleModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
				final List<ArticleModel> result = searchResult.getResult();
				return result == null ? Collections.EMPTY_LIST : result;
			}
			else
			{
				final Map<String, Object> params1 = new HashMap<String, Object>();
				params1.put("tagId", tagId);
				params1.put("articleType", list);
				params1.put("isActive", Boolean.TRUE);

				final String query2 = "SELECT DISTINCT  {aa:target},{art:CREATIONTIME} FROM {ArticleTags AS at JOIN articles2articletags As aa"
						+ " ON {aa:source}={at:pk} join Article as art on {aa:target}={art:pk}} where {at:"
						+ ArticleTagsModel.TAGID
						+ "} = ?tagId AND {art:"
						+ ArticleModel.ISACTIVE
						+ "}=?isActive and {art:articleType} in (?articleType) ORDER BY {art:CREATIONTIME} DESC";
				final SearchResultImpl<ArticleModel> searchResult2 = (SearchResultImpl) flexibleSearchService.search(query2, params1);
				final List<ArticleModel> result2 = searchResult2.getResult();
				return result2 == null ? Collections.EMPTY_LIST : result2;
			}
		}
		return Collections.EMPTY_LIST;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.magazine.dao.MagazineLandingDao#getArticleTagsToMagazinePage()
	 */
	/*
	 * Method to get All Article Tags
	 */
	@Override
	public List<ArticleTagsModel> getArticleTagsToMagazinePage()
	{
		LOG.debug("in MagazineLandingDao Impl ");

		final String query = "SELECT {" + ArticleTagsModel.PK + "} FROM {" + ArticleTagsModel._TYPECODE + "} ORDER BY {"
				+ ArticleTagsModel.TAGID + "} ASC";
		final SearchResultImpl<ArticleTagsModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query);
		final List<ArticleTagsModel> result = searchResult.getResult();
		return result == null ? Collections.EMPTY_LIST : result;
	}

}
