/**
 *
 */
package de.hybris.clicks.core.magazine.dao;

import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;

import java.util.List;


/**
 * @author siva.reddy
 *
 */
public interface MagazineLandingDao
{

	/**
	 * Gets the all articles to magazine page.
	 *
	 * @param tagId
	 *           the tag id
	 * @param pageType
	 *           the page type
	 * @return the all articles to magazine page
	 */
	public List<ArticleModel> getAllArticlesToMagazinePage(String tagId, String pageType);

	/**
	 * Gets the article tags to magazine page.
	 *
	 * @return the article tags to magazine page
	 */
	public List<ArticleTagsModel> getArticleTagsToMagazinePage();

}
