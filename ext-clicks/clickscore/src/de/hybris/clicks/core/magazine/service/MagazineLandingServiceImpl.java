/**
 *
 */
package de.hybris.clicks.core.magazine.service;

import de.hybris.clicks.core.magazine.dao.MagazineLandingDao;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.clicks.core.model.ArticleTagsModel;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author siva.reddy
 *
 */
public class MagazineLandingServiceImpl implements MagazineLandingService
{
	protected static final Logger LOG = Logger.getLogger(MagazineLandingServiceImpl.class);

	@Resource(name = "magazineLandingDao")
	private MagazineLandingDao magazineLandingDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.magazine.service.MagazineLandingService#getAllArticlesToMagazinePage(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<ArticleModel> getAllArticlesToMagazinePage(final String tagId, final String pageType)
	{
		LOG.debug("in MagazineLandingService Impl " + tagId + " PageType: " + pageType);
		return magazineLandingDao.getAllArticlesToMagazinePage(tagId, pageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.magazine.service.MagazineLandingService#getArticleTagsToMagazinePage()
	 */
	@Override
	public List<ArticleTagsModel> getArticleTagsToMagazinePage()
	{
		LOG.debug("in MagazineLandingService Impl ");
		return magazineLandingDao.getArticleTagsToMagazinePage();
	}

}
