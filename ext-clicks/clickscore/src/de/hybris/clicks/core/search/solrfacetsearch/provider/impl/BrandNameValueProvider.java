/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Swapnil Desai
 *
 */
public class BrandNameValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{

	private static final Logger LOG = Logger.getLogger(BrandNameValueProvider.class);

	private FieldNameProvider fieldNameProvider;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection fieldValues = new ArrayList();
		String brandName = "";
		try
		{
			if (model instanceof ClicksApparelSizeVariantModel)
			{
				final ClicksApparelSizeVariantModel sizeVariantModel = (ClicksApparelSizeVariantModel) model;
				//LOG.info("Size variant Code is : " + sizeVariantModel.getCode());
				final ApparelStyleVariantProductModel apparelStyleModel = (ApparelStyleVariantProductModel) sizeVariantModel
						.getBaseProduct();
				if (null != apparelStyleModel.getBaseProduct())
				{
					for (final CategoryModel categoryModel : apparelStyleModel.getBaseProduct().getSupercategories())
					{
						for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
						{
							if (Config.getParameter("inner.category.code").equalsIgnoreCase(innerCategoryModel.getCode()))
							{
								brandName = categoryModel.getName();
								break;
							}
						}
						if (brandName != "")
						{
							break;
						}
					}
				}
			}
			else if (model instanceof ApparelStyleVariantProductModel)
			{
				final ApparelStyleVariantProductModel styleVariantModel = (ApparelStyleVariantProductModel) model;
				if (null != styleVariantModel.getBaseProduct())
				{
					for (final CategoryModel categoryModel : styleVariantModel.getBaseProduct().getSupercategories())
					{
						for (final CategoryModel innerCategoryModel : categoryModel.getSupercategories())
						{
							if (Config.getParameter("inner.category.code").equalsIgnoreCase(innerCategoryModel.getCode()))
							{
								brandName = categoryModel.getName();
							}
						}
					}
				}
			}

			final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
			for (final String fieldName : fieldNames)
			{
				fieldValues.add(new FieldValue(fieldName, brandName));
			}

		}
		catch (final Exception e)
		{
			String productCode = "";
			if (null != model)
			{
				productCode = ((ProductModel) model).getCode();
			}
			LOG.error("Error in getting brand name for the product code : " + productCode + "due to " + e.getMessage());
		}

		return fieldValues;
	}

}
