/**
 *
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ImageValueProvider;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 *
 */
public class ClicksProductImageValueProvider extends ImageValueProvider
{
	private static final Logger LOG = Logger.getLogger(ClicksProductImageValueProvider.class);
	String property = Config.getParameter("container.media.value");
	final String[] mediaList = property.split(",");

	@Override
	protected MediaModel findMedia(final ProductModel product, final MediaFormatModel mediaFormat)
	{
		try
		{
			if ((product != null) && (mediaFormat != null))
			{
				if ((product instanceof ClicksApparelSizeVariantModel))
				{
					final List<MediaContainerModel> galleryImages = product.getGalleryImages();
					if (CollectionUtils.isNotEmpty(galleryImages))
					{

						for (final MediaContainerModel container : galleryImages)
						{
							try
							{
								if (null != mediaList)
								{
									if (StringUtils.isNotBlank(container.getQualifier())
											&& container.getQualifier().toLowerCase().contains(mediaList[0].toLowerCase()))
									{
										final MediaModel media = getMediaContainerService().getMediaForFormat(container, mediaFormat);
										if (media != null)
										{
											return media;
										}
									}
								}
							}
							catch (final Exception localModelNotFoundException)
							{
								//localModelNotFoundException.printStackTrace();
							}

						}
						for (final MediaContainerModel container : galleryImages)
						{
							try
							{
								if (null != mediaList)
								{
									if (StringUtils.isNotBlank(container.getQualifier())
											&& container.getQualifier().toLowerCase().contains(mediaList[1].toLowerCase()))
									{
										final MediaModel media = getMediaContainerService().getMediaForFormat(container, mediaFormat);
										if (media != null)
										{
											return media;
										}
									}
								}
							}
							catch (final Exception localModelNotFoundException)
							{
								//localModelNotFoundException.printStackTrace();
							}

						}
						for (final MediaContainerModel container : galleryImages)
						{
							try
							{
								final MediaModel media = getMediaContainerService().getMediaForFormat(container, mediaFormat);
								if (media != null)
								{
									return media;
								}
							}
							catch (final Exception localModelNotFoundException)
							{
								//localModelNotFoundException.printStackTrace();
							}

						}
					}
				}
				else if ((product instanceof ApparelStyleVariantProductModel))
				{
					if (CollectionUtils.isNotEmpty(product.getVariants()))
					{
						for (final VariantProductModel sizeVariant : product.getVariants())
						{
							if (sizeVariant instanceof ClicksApparelSizeVariantModel)
							{
								try
								{
									final MediaModel media = findMedia(sizeVariant, mediaFormat);
									if (null != media)
									{
										return media;
									}
								}
								catch (final Exception e)
								{
									//
								}
							}
							break;
						}
					}
					//return null;
				}
				//	return findMedia(((VariantProductModel) product).getBaseProduct(), mediaFormat);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in image for " + product.getCode());
		}
		return null;
	}
}
