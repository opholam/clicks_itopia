/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.DefaultCategorySource;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author Swapnil Desai
 *
 */
public class SizeVariantCategorySource extends DefaultCategorySource
{
	@Resource(name = "userService")
	private UserService userService;

	@Override
	protected Set<ProductModel> getProducts(final Object model)
	{
		final Set<ProductModel> products = super.getProducts(model);
		if ((model instanceof ApparelStyleVariantProductModel))
		{
			final ApparelStyleVariantProductModel currentProduct = (ApparelStyleVariantProductModel) model;
			for (final VariantProductModel variantProductModel : currentProduct.getVariants())
			{
				products.add(variantProductModel);
			}

			products.add(currentProduct);
			return products;
		}

		return Collections.emptySet();
	}

	@Override
	protected boolean isBlockedCategory(final CategoryModel category)
	{
		try
		{
			final UserGroupModel customerGroup = userService.getUserGroupForUID("customergroup");
			if (CollectionUtils.isEmpty(category.getAllowedPrincipals())
					|| (null != customerGroup && !category.getAllowedPrincipals().contains(customerGroup)))
			{
				return true;
			}
		}
		catch (final Exception e)
		{
			//
		}
		return ((category instanceof ClassificationClassModel)) && (!isIncludeClassificationClasses());
	}

	//
	//	@Override
	//	public Collection<CategoryModel> getCategoriesForConfigAndProperty(final IndexConfig indexConfig,
	//			final IndexedProperty indexedProperty, final Object model)
	//	{
	//		final Set products = getProducts(model);
	//		final Set directCategories = getDirectSuperCategories(products);
	//		if (directCategories != null && !directCategories.isEmpty())
	//		{
	//			final Collection catalogVersions = Collections.singletonList(((ProductModel) model).getCatalogVersion());
	//			final Set rootCategories = lookupRootCategories(catalogVersions);
	//			final Set allCategories = new HashSet();
	//			CategoryModel category;
	//			for (final Iterator iterator = directCategories.iterator(); iterator.hasNext(); allCategories.addAll(getAllCategories(
	//					category, rootCategories)))
	//			{
	//				category = (CategoryModel) iterator.next();
	//			}
	//
	//			return allCategories;
	//		}
	//		else
	//		{
	//			return Collections.emptyList();
	//		}
	//	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
