package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelSizeVariantProductModel;
import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksPromotionTypeModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Swapnil Desai
 *
 */
public class PromotionTypeStickerValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider,
		Serializable
{

	private static final Logger LOG = Logger.getLogger(PromotionTypeStickerValueProvider.class);

	private FieldNameProvider fieldNameProvider;
	private PromotionsService promotionService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected PromotionsService getPromotionsService()
	{
		return this.promotionService;
	}

	@Required
	public void setPromotionsService(final PromotionsService promotionService)
	{
		this.promotionService = promotionService;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection fieldValues = new ArrayList();
		try
		{
			final Set<String> associatedPromoTypes = new HashSet<String>();
			final List<ClicksPromotionTypeModel> promoTypes = getPromoTypes();
			if (model instanceof ProductModel)
			{
				final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
				final List<ProductModel> allProducts = new ArrayList<ProductModel>();

				if (model instanceof ApparelSizeVariantProductModel)
				{
					LOG.debug("Size Variant : Fetching Promotion Type");
					final ApparelSizeVariantProductModel product = (ApparelSizeVariantProductModel) model;
					allProducts.add(product);
				}
				else if (model instanceof ApparelStyleVariantProductModel)
				{
					LOG.debug("Style Variant : Fetching Promotion Type from Size Variant");
					final Collection<VariantProductModel> products = ((ApparelStyleVariantProductModel) model).getVariants();
					allProducts.addAll(products);
				}

				for (final ProductModel productModel : allProducts)
				{
					for (final ProductPromotionModel promotion : getPromotionsService().getProductPromotions(
							Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), productModel,
							Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()))
					{
						getAssociatedPromoTypes(promotion, associatedPromoTypes, promoTypes);
					}
				}
			}
			else if (model instanceof AbstractPromotionModel)
			{
				getAssociatedPromoTypes((AbstractPromotionModel) model, associatedPromoTypes, promoTypes);
			}

			for (final String fieldName : fieldNameProvider.getFieldNames(indexedProperty, null))
			{
				for (final String promoTypeCode : associatedPromoTypes)
				{
					fieldValues.add(new FieldValue(fieldName, promoTypeCode));
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in indexing promo type " + e.getMessage());
		}
		return fieldValues;
	}

	/**
	 * @param model
	 * @param associatedPromoTypes
	 */
	private void getAssociatedPromoTypes(final AbstractPromotionModel model, final Set<String> associatedPromoTypes,
			final List<ClicksPromotionTypeModel> promoTypes)
	{
		try
		{
			if (CollectionUtils.isNotEmpty(promoTypes))
			{
				final List<String> stickers = model.getStickers();
				if (CollectionUtils.isNotEmpty(stickers))
				{
					for (final String sticker : stickers)
					{
						for (final ClicksPromotionTypeModel promo : promoTypes)
						{
							if (null != CollectionUtils.find(promo.getRelatedStickers(), new BeanPropertyValueEqualsPredicate(
									"stickerID", sticker, true)))
							{
								associatedPromoTypes.add(promo.getPromotionTypeCode());
								break;
							}
						}
					}
				}
			}
		}
		catch (final Exception e)
		{
			//
		}
	}

	private List<ClicksPromotionTypeModel> getPromoTypes()
	{
		SearchResult<ClicksPromotionTypeModel> searchResult = null;
		try
		{
			searchResult = getFlexibleSearchService().search("Select {PK} from {" + ClicksPromotionTypeModel._TYPECODE + "}");
		}
		catch (final Exception e)
		{
			//
		}
		return null != searchResult ? searchResult.getResult() : null;
	}
}
