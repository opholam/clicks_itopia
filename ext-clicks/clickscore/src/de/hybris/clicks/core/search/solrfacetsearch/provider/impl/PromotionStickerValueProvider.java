package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelSizeVariantProductModel;
import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.clicks.core.model.PartnerModel;
import de.hybris.clicks.core.model.StickerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Swapnil Desai
 *
 */
public class PromotionStickerValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{

	private static final Logger LOG = Logger.getLogger(PromotionStickerValueProvider.class);

	private FieldNameProvider fieldNameProvider;
	private PromotionsService promotionService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService
	 *           the catalogVersionService to set
	 */
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected PromotionsService getPromotionsService()
	{
		return this.promotionService;
	}

	@Required
	public void setPromotionsService(final PromotionsService promotionService)
	{
		this.promotionService = promotionService;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{

		final Collection fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();

		if (model instanceof ApparelSizeVariantProductModel)
		{
			LOG.debug("Size variant : Fetching Stickers.");
			setPromotionStickers(indexedProperty, fieldValues, baseSiteModel, (ApparelSizeVariantProductModel) model);
		}
		else if (model instanceof ApparelStyleVariantProductModel)
		{
			LOG.debug("Style variant : Fetching Stickers from Size variants");
			final Collection<VariantProductModel> variantProduct = ((ApparelStyleVariantProductModel) model).getVariants();
			for (final VariantProductModel variantProductModel : variantProduct)
			{
				setPromotionStickers(indexedProperty, fieldValues, baseSiteModel, variantProductModel);
			}
		}

		return fieldValues;
	}

	/**
	 * @param indexedProperty
	 * @param fieldValues
	 * @param baseSiteModel
	 * @param variantProductModel
	 */
	@SuppressWarnings("boxing")
	private void setPromotionStickers(final IndexedProperty indexedProperty, final Collection fieldValues,
			final BaseSiteModel baseSiteModel, final VariantProductModel variantProductModel)
	{
		StickerModel stickerModel;

		if (variantProductModel instanceof ClicksApparelSizeVariantModel)
		{
			final Boolean newProduct = ((ClicksApparelSizeVariantModel) variantProductModel).getNewness();
			LOG.debug("Value of new product is : " + newProduct);
			if (null != newProduct && newProduct)
			{
				final String stickerUrl = getStickerUrlFromMediaCode("new_product");
				if (StringUtils.isNotBlank(stickerUrl))
				{
					fieldValues.add(new FieldValue(indexedProperty.getName() + "_" + "string_mv", stickerUrl));
				}
			}
		}

		if (CollectionUtils.isNotEmpty(variantProductModel.getPartnerRewards()))
		{
			for (final PartnerModel partnerModel : variantProductModel.getPartnerRewards())
			{
				if (partnerModel.getPartnerId().equalsIgnoreCase(Config.getParameter("clicks.property.vithc")))
				{
					final String stickerUrl = getStickerUrlFromMediaCode("partner_product");
					if (null != stickerUrl)
					{
						fieldValues.add(new FieldValue(indexedProperty.getName() + "_" + "string_mv", stickerUrl));
					}
				}
			}
		}
		final Set<String> stickers = new HashSet<String>();
		for (final ProductPromotionModel promotion : getPromotionsService().getProductPromotions(
				Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), variantProductModel,
				Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()))
		{
			LOG.debug("Promotion Sticker is available");
			for (final String stickerId : promotion.getStickers())
			{
				if (StringUtils.isNotBlank(stickerId))
				{
					if ("stickerurl".equals(indexedProperty.getName()))
					{
						stickerModel = new StickerModel();
						stickerModel.setStickerID(stickerId);
						try
						{
							stickerModel = flexibleSearchService.getModelByExample(stickerModel);
							if (null != stickerModel && null != stickerModel.getStickerImage())
							{
								if (!stickers.contains(stickerModel.getStickerImage().getURL()))
								{
									fieldValues.add(new FieldValue(indexedProperty.getName() + "_" + "string_mv", stickerModel
											.getStickerImage().getURL()));
								}
								stickers.add(stickerModel.getStickerImage().getURL());
							}
						}
						catch (final Exception e)
						{
							LOG.error("Error in getting sticker model for sticker code : " + stickerId + " due to " + e.getMessage());
						}
					}
					else
					{
						fieldValues.add(new FieldValue(indexedProperty.getName() + "_" + "string_mv", stickerId));
					}

				}
			}

		}
	}

	/**
	 * @param string
	 */
	private String getStickerUrlFromMediaCode(final String mediaCode)
	{
		MediaModel mediaModel = new MediaModel();
		mediaModel.setCode(mediaCode);
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion("clicksContentCatalog", "Online");
		mediaModel.setCatalogVersion(catalogVersionModel);
		try
		{
			mediaModel = flexibleSearchService.getModelByExample(mediaModel);

			if (null != mediaModel)
			{
				return mediaModel.getURL();
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in getting media model for media code" + mediaCode + " due to " + e.getMessage());
		}
		return "";

	}

}
