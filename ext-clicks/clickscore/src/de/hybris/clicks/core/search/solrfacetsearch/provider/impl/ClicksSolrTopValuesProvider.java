/**
 *
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.DefaultTopValuesProvider;
import de.hybris.platform.util.Config;


/**
 * @author manikandan.gopalan
 *
 */
public class ClicksSolrTopValuesProvider extends DefaultTopValuesProvider
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.DefaultTopValuesProvider#getTopFacetCount
	 * ()
	 */
	@Override
	protected int getTopFacetCount()
	{
		return Config.getInt("Solr.Facet.Top.Values.count", super.getTopFacetCount());
	}
}
