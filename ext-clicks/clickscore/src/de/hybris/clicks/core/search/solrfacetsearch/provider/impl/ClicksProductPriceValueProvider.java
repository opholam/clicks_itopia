package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelSizeVariantProductModel;
import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Swapnil Desai
 *
 */
public class ClicksProductPriceValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider,
		Serializable
{
	private static final Logger LOG = Logger.getLogger(ClicksProductPriceValueProvider.class);
	private FieldNameProvider fieldNameProvider;
	private CommercePriceService commercePriceService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{

		final Collection fieldValues = new ArrayList();
		Double priceVal = null;
		String currencyCode = null;
		List<String> rangeNameList = null;
		final Date currentDate = new Date();

		if (model instanceof ApparelSizeVariantProductModel)
		{
			LOG.debug("Size Variant : Starting to Fetch price");
			for (final PriceRowModel priceRow : ((ApparelSizeVariantProductModel) model).getEurope1Prices())
			{
				if (null != priceRow.getStartTime() && null != priceRow.getEndTime())
				{
					if (priceRow.getStartTime().compareTo(currentDate) == -1 && priceRow.getEndTime().compareTo(currentDate) == 1)
					{
						priceVal = priceRow.getPrice();
						currencyCode = priceRow.getCurrency().getIsocode();
						break;
					}
				}
			}
		}
		else if (model instanceof ApparelStyleVariantProductModel)
		{
			final List<Double> allPrices = new ArrayList<Double>(2);

			LOG.debug("Style Variant : Starting to Fetch price from Variants");
			for (final VariantProductModel vPMpdel : ((ApparelStyleVariantProductModel) model).getVariants())
			{
				for (final PriceRowModel priceRow : vPMpdel.getEurope1Prices())
				{
					if (null != priceRow.getStartTime() && null != priceRow.getEndTime())
					{
						if (priceRow.getStartTime().compareTo(currentDate) == -1 && priceRow.getEndTime().compareTo(currentDate) == 1)
						{
							allPrices.add(priceRow.getPrice());
							currencyCode = priceRow.getCurrency().getIsocode();
						}
					}
				}
			}
			LOG.debug("Fetched : " + allPrices.size() + "price from Variants");
			Collections.sort(allPrices);
			if (CollectionUtils.isNotEmpty(allPrices))
			{
				priceVal = allPrices.get(0);
			}
		}

		if (null != priceVal)
		{
			rangeNameList = getRangeNameList(indexedProperty, priceVal, Config.getParameter("price.unit.value"));

			for (final String fieldName : fieldNameProvider.getFieldNames(indexedProperty, currencyCode))
			{
				if (rangeNameList.isEmpty())
				{
					fieldValues.add(new FieldValue(fieldName, priceVal));
				}
				else
				{
					for (final String rangeName : rangeNameList)
					{
						fieldValues.add(new FieldValue(fieldName, (rangeName == null) ? priceVal : rangeName));
					}
				}
			}
		}
		return fieldValues;

	}

	/**
	 * @return the commercePriceService
	 */
	public CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}

	/**
	 * @param commercePriceService
	 *           the commercePriceService to set
	 */
	public void setCommercePriceService(final CommercePriceService commercePriceService)
	{
		this.commercePriceService = commercePriceService;
	}
}
