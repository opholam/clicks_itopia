package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.OrderPromotionModel;
import de.hybris.platform.promotions.model.ProductOneToOnePerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerBundlePromotionModel;
import de.hybris.platform.promotions.model.ProductPerfectPartnerPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


public class PromotionProductApprovalValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider,
		Serializable
{
	private FieldNameProvider fieldNameProvider;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		Boolean approved = Boolean.FALSE;
		try
		{
			if (model instanceof ProductPromotionModel)
			{
				if (hasValidProduct(((ProductPromotionModel) model).getProducts()))
				{
					if (model instanceof ProductOneToOnePerfectPartnerPromotionModel)
					{
						if (isProductValid(((ProductOneToOnePerfectPartnerPromotionModel) model).getBaseProduct())
								&& isProductValid(((ProductOneToOnePerfectPartnerPromotionModel) model).getPartnerProduct()))
						{
							approved = Boolean.TRUE;
						}
					}
					else if (model instanceof ProductPerfectPartnerPromotionModel)
					{
						if (hasValidProduct(((ProductPerfectPartnerPromotionModel) model).getPartnerProducts()))
						{
							approved = Boolean.TRUE;
						}
					}
					else if (model instanceof ProductPerfectPartnerBundlePromotionModel)
					{
						if (isProductValid(((ProductPerfectPartnerBundlePromotionModel) model).getBaseProduct())
								&& hasValidProduct(((ProductPerfectPartnerBundlePromotionModel) model).getPartnerProducts()))
						{
							approved = Boolean.TRUE;
						}
					}
					else
					{
						approved = Boolean.TRUE;
					}
				}
			}
			else if (model instanceof OrderPromotionModel
					&& Config.getBoolean("order.promotion.display.promoLandingPage", Boolean.FALSE))
			{
				approved = Boolean.TRUE;
			}
		}
		catch (final Exception e)
		{
			//
		}
		return createFieldValues(indexedProperty, approved);

	}

	private boolean hasValidProduct(final Collection<ProductModel> products)
	{
		if (CollectionUtils.isNotEmpty(products))
		{
			for (final ProductModel partnerProduct : products)
			{
				if (isProductValid(partnerProduct))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param product
	 * @return
	 */
	private boolean isProductValid(final ProductModel product)
	{
		return null != product && CollectionUtils.isNotEmpty(product.getSupercategories())
				&& ArticleApprovalStatus.APPROVED.equals(product.getApprovalStatus())
				&& CollectionUtils.exists(product.getSupercategories(), new Predicate()
				{
					@Override
					public boolean evaluate(final Object arg0)
					{
						if (StringUtils.startsWith(((CategoryModel) arg0).getCode(), "OH"))
						{
							return true;
						}
						return false;
					}
				});
	}

	protected Collection<FieldValue> createFieldValues(final IndexedProperty indexedProperty, final Boolean value)
	{
		final List fieldValues = new ArrayList();

		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}

		return fieldValues;
	}

}