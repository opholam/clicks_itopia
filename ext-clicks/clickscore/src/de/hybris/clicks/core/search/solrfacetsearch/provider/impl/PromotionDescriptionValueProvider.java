package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelSizeVariantProductModel;
import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


public class PromotionDescriptionValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider,
		Serializable
{
	private static final Logger LOG = Logger.getLogger(PromotionDescriptionValueProvider.class);

	private FieldNameProvider fieldNameProvider;
	private PromotionsService promotionService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected PromotionsService getPromotionsService()
	{
		return this.promotionService;
	}

	@Required
	public void setPromotionsService(final PromotionsService promotionService)
	{
		this.promotionService = promotionService;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection fieldValues = new ArrayList();

		if (model instanceof ApparelSizeVariantProductModel)
		{
			LOG.debug("Size Variant : Fetching Promotion Description");
			createIndexValue(indexConfig, indexedProperty, fieldValues, (ApparelSizeVariantProductModel) model);
		}
		else if (model instanceof ApparelStyleVariantProductModel)
		{
			LOG.debug("Style Variant : Fetching Promotion Description from Size Variant");
			for (final VariantProductModel variantProductModel : ((ApparelStyleVariantProductModel) model).getVariants())
			{
				createIndexValue(indexConfig, indexedProperty, fieldValues, variantProductModel);
				break;
			}
		}

		return fieldValues;
	}

	/**
	 * @param indexConfig
	 * @param indexedProperty
	 * @param fieldValues
	 * @param variantProductModel
	 */
	private void createIndexValue(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Collection fieldValues, final VariantProductModel variantProductModel)
	{
		if (indexedProperty.isMultiValue())
		{
			fieldValues.addAll(createFieldValues(variantProductModel, indexConfig, indexedProperty));
		}
		else
		{
			fieldValues.addAll(createFieldValue(variantProductModel, indexConfig, indexedProperty));
		}
	}

	protected List<FieldValue> createFieldValue(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final List fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if ((baseSiteModel != null) && (baseSiteModel.getDefaultPromotionGroup() != null))
		{
			String desc = "";
			for (final ProductPromotionModel promotion : getPromotionsService().getProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product,
					Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()))
			{
				desc = desc + "\n" + promotion.getDescription();
				if (StringUtils.isNotEmpty(promotion.getDisclaimer()))
				{
					desc = desc + ". " + promotion.getDisclaimer() + ".";
				}
				if (promotion.getEndDate() != null)
				{
					final DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
					final String validity = Config.getString("text.promotion.endDate", "").concat(" ")
							.concat(dateFormat.format(promotion.getEndDate()));
					desc = StringUtils.isNotEmpty(desc) ? desc + " " + validity : validity;
				}
			}
			addFieldValues(fieldValues, indexedProperty, null, desc);
		}

		return fieldValues;
	}

	protected List<FieldValue> createFieldValues(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final List fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if ((baseSiteModel != null) && (baseSiteModel.getDefaultPromotionGroup() != null))
		{
			for (final ProductPromotionModel promotion : getPromotionsService().getProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product,
					Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()))
			{
				String desc = promotion.getDescription();
				if (StringUtils.isNotEmpty(promotion.getDisclaimer()))
				{
					desc = desc + ". " + promotion.getDisclaimer() + ".";
				}
				if (promotion.getEndDate() != null)
				{
					final DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
					final String validity = Config.getString("text.promotion.endDate", "").concat(" ")
							.concat(dateFormat.format(promotion.getEndDate()));
					desc = StringUtils.isNotEmpty(desc) ? desc + " " + validity : validity;
				}
				addFieldValues(fieldValues, indexedProperty, null, desc.concat("~").concat(promotion.getCode()));
			}
		}
		return fieldValues;
	}


	protected void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty,
			final LanguageModel language, final Object value)
	{
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty,
				(language == null) ? null : language.getIsocode());
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
	}
}