/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.DefaultCategorySource;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;


public class PromotionCategorySource extends DefaultCategorySource
{
	@Override
	protected Set<ProductModel> getProducts(final Object model)
	{
		if ((model instanceof ProductPromotionModel))
		{
			final Set products = new HashSet();
			for (final ProductModel currentProduct : ((ProductPromotionModel) model).getProducts())
			{
				//				while ((currentProduct instanceof VariantProductModel))
				//				{
				//					products.add(currentProduct);
				//					currentProduct = ((VariantProductModel) currentProduct).getBaseProduct();
				//				}
				products.add(currentProduct);
			}

			return products;
		}

		return Collections.emptySet();
	}


	@Override
	public Collection<CategoryModel> getCategoriesForConfigAndProperty(final IndexConfig indexConfig,
			final IndexedProperty indexedProperty, final Object model)
	{
		final Set allCategories = new HashSet();
		final Set<ProductModel> products = getProducts(model);
		if (CollectionUtils.isNotEmpty(products))
		{
			final Set<CategoryModel> directCategories = getDirectSuperCategories(products);

			if ((directCategories != null) && (!directCategories.isEmpty()) && CollectionUtils.isNotEmpty(products))
			{
				final Collection catalogVersions = Collections.singletonList((products.iterator().next()).getCatalogVersion());
				final Set rootCategories = lookupRootCategories(catalogVersions);
				for (final CategoryModel category : directCategories)
				{
					allCategories.addAll(getAllCategories(category, rootCategories));
				}
			}
		}
		if ((model instanceof ProductPromotionModel))
		{
			allCategories.addAll(((ProductPromotionModel) model).getCategories());
		}

		return allCategories;
	}
}
