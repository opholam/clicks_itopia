package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.PartnerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Swapnil Desai
 *
 */
public class VitalityHealthCareValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider,
		Serializable
{

	private static final Logger LOG = Logger.getLogger(VitalityHealthCareValueProvider.class);

	private FieldNameProvider fieldNameProvider;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}



	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{

		final Collection fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();

		if (model instanceof ProductModel)
		{
			LOG.debug("Vitality health care product");
			setHealthCareProducts(indexedProperty, fieldValues, baseSiteModel, (ProductModel) model);
		}

		return fieldValues;
	}

	/**
	 * @param indexedProperty
	 * @param fieldValues
	 * @param baseSiteModel
	 * @param model
	 */
	@SuppressWarnings("boxing")
	private void setHealthCareProducts(final IndexedProperty indexedProperty, final Collection fieldValues,
			final BaseSiteModel baseSiteModel, final ProductModel model)
	{

		if (CollectionUtils.isNotEmpty(model.getPartnerRewards()))
		{
			for (final PartnerModel partnerModel : model.getPartnerRewards())
			{
				if (null != partnerModel.getPartnerId()
						&& partnerModel.getPartnerId().equalsIgnoreCase(Config.getParameter("partner.key.value")))
				{
					LOG.debug("Qualified Vitality health care product");
					fieldValues.add(new FieldValue(indexedProperty.getName() + "_" + "string_mv", Config
							.getParameter("clicks.property.vithc")));
				}
			}
		}
	}

}
