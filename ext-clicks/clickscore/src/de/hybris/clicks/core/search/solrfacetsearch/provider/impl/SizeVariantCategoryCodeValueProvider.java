/**
 *
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.impl.CategoryCodeValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author Swapnil Desai
 *
 */
public class SizeVariantCategoryCodeValueProvider extends CategoryCodeValueProvider
{

	private String categoriesQualifier;
	private FieldNameProvider fieldNameProvider;
	@Resource(name = "userService")
	private UserService userService;

	/**
	 * @return the categoriesQualifier
	 */
	public String getCategoriesQualifier()
	{
		return categoriesQualifier;
	}

	/**
	 * @param categoriesQualifier
	 *           the categoriesQualifier to set
	 */
	@Override
	public void setCategoriesQualifier(final String categoriesQualifier)
	{
		this.categoriesQualifier = categoriesQualifier;
	}

	@Override
	public Collection getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		final UserGroupModel customerGroup = userService.getUserGroupForUID("customergroup");
		Collection categories = new ArrayList();
		Collection baseProductCategories = null;
		//		Collection currentCategories = new ArrayList();
		if (model instanceof VariantProductModel)
		{
			if (model instanceof ApparelStyleVariantProductModel)
			{
				final de.hybris.platform.core.model.product.ProductModel baseProduct = ((VariantProductModel) model).getBaseProduct();
				baseProductCategories = (Collection) modelService.getAttributeValue(baseProduct, categoriesQualifier);
				categories.addAll(baseProductCategories);
				final Collection<VariantProductModel> sizeVariantProducts = ((ApparelStyleVariantProductModel) model).getVariants();
				for (final VariantProductModel variantProductModel : sizeVariantProducts)
				{
					categories.addAll((Collection) modelService.getAttributeValue(variantProductModel, categoriesQualifier));
				}
			}
		}
		else
		{
			categories = (Collection) modelService.getAttributeValue(model, categoriesQualifier);
		}


		if (!categories.isEmpty())
		{
			final Collection fieldValues = new ArrayList();
			for (final Iterator iterator = categories.iterator(); iterator.hasNext();)
			{
				final CategoryModel category = (CategoryModel) iterator.next();
				fieldValues.addAll(createFieldValue(category, indexedProperty, customerGroup));
				CategoryModel superCategory;
				for (final Iterator iterator1 = category.getAllSupercategories().iterator(); iterator1.hasNext(); fieldValues
						.addAll(createFieldValue(superCategory, indexedProperty, customerGroup)))
				{
					superCategory = (CategoryModel) iterator1.next();
				}

			}

			return fieldValues;
		}
		else
		{
			return Collections.emptyList();
		}
	}

	private List createFieldValue(final CategoryModel category, final IndexedProperty indexedProperty,
			final UserGroupModel customerGroup)
	{
		if (CollectionUtils.isEmpty(category.getAllowedPrincipals())
				|| (null != customerGroup && !category.getAllowedPrincipals().contains(customerGroup)))
		{
			return Collections.emptyList();
		}
		final List fieldValues = new ArrayList();
		final Object value = getPropertyValue(category, "code");
		final Collection fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);
		String fieldName;
		for (final Iterator iterator = fieldNames.iterator(); iterator.hasNext(); fieldValues.add(new FieldValue(fieldName, value)))
		{
			fieldName = (String) iterator.next();
		}

		return fieldValues;
	}

	private Object getPropertyValue(final Object model, final String propertyName)
	{
		return modelService.getAttributeValue(model, propertyName);
	}

	@Override
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
