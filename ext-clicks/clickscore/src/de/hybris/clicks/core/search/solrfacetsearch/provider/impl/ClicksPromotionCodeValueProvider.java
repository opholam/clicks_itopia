package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelSizeVariantProductModel;
import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.PromotionCodeValueProvider;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author Swapnil Desai
 *
 */
public class ClicksPromotionCodeValueProvider extends PromotionCodeValueProvider
{

	private static final Logger LOG = Logger.getLogger(ClicksPromotionCodeValueProvider.class);

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection fieldValues = new ArrayList();

		if (model instanceof ApparelSizeVariantProductModel)
		{
			LOG.debug("Size Variant : Fetching Promotion Code");
			createIndexValue(indexConfig, indexedProperty, fieldValues, (ApparelSizeVariantProductModel) model);
		}
		else if (model instanceof ApparelStyleVariantProductModel)
		{
			LOG.debug("Style Variant : Fetching Promotion Code from Size Variant");
			for (final VariantProductModel variantProduct : ((ApparelStyleVariantProductModel) model).getVariants())
			{
				createIndexValue(indexConfig, indexedProperty, fieldValues, variantProduct);
			}
		}
		return fieldValues;
	}

	@Override
	protected List<FieldValue> createFieldValues(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final List fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if ((baseSiteModel != null) && (baseSiteModel.getDefaultPromotionGroup() != null))
		{
			for (final ProductPromotionModel promotion : getPromotionsService().getProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product,
					Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()))
			{
				addFieldValues(fieldValues, indexedProperty, null, promotion.getCode());
			}
		}
		return fieldValues;
	}

	@Override
	protected List<FieldValue> createFieldValue(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final List fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if ((baseSiteModel != null) && (baseSiteModel.getDefaultPromotionGroup() != null))
		{
			final Iterator<ProductPromotionModel> localIterator = getPromotionsService().getProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product,
					Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()).iterator();
			if (localIterator.hasNext())
			{
				final ProductPromotionModel promotion = localIterator.next();

				addFieldValues(fieldValues, indexedProperty, null, promotion.getCode());
			}
		}

		return fieldValues;
	}

	/**
	 * @param indexConfig
	 * @param indexedProperty
	 * @param fieldValues
	 * @param variantProduct
	 */
	private void createIndexValue(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Collection fieldValues, final VariantProductModel variantProduct)
	{
		if (indexedProperty.isMultiValue())
		{
			fieldValues.addAll(createFieldValues(variantProduct, indexConfig, indexedProperty));
		}
		else
		{
			fieldValues.addAll(createFieldValue(variantProduct, indexConfig, indexedProperty));
		}
	}
}