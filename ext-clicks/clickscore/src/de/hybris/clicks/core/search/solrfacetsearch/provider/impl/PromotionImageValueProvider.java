package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.MediaItemModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


public class PromotionImageValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{
	private FieldNameProvider fieldNameProvider;
	private PromotionsService promotionService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected PromotionsService getPromotionsService()
	{
		return this.promotionService;
	}

	@Required
	public void setPromotionsService(final PromotionsService promotionService)
	{
		this.promotionService = promotionService;
	}

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof AbstractPromotionModel)
		{
			String mediaUrl = null;
			for (final MediaItemModel mediaItem : ((AbstractPromotionModel) model).getMediaItems())
			{
				if (Config.getInt("promotion.image.type", 2) == mediaItem.getType())
				{
					mediaUrl = mediaItem.getValue();
					break;
				}
			}
			if (StringUtils.isEmpty(mediaUrl))
			{
				mediaUrl = getMediaUrlFromProduct(model, mediaUrl);
			}
			return createFieldValues(indexedProperty, mediaUrl);
		}
		return Collections.EMPTY_LIST;

	}

	/**
	 * @param model
	 * @param mediaUrl
	 * @return
	 */
	private String getMediaUrlFromProduct(final Object model, String mediaUrl)
	{
		try
		{
			if (model instanceof ProductPromotionModel)
			{
				final Collection<ProductModel> products = ((ProductPromotionModel) model).getProducts();
				if (CollectionUtils.isNotEmpty(products))
				{
					for (final ProductModel productModel : products)
					{
						if (CollectionUtils.isNotEmpty(productModel.getGalleryImages()))
						{
							for (final MediaContainerModel mediaContainer : productModel.getGalleryImages())
							{
								if (StringUtils.isNotBlank(mediaContainer.getQualifier())
										&& StringUtils.containsIgnoreCase(mediaContainer.getQualifier(), "straight"))
								{
									MediaModel media = (MediaModel) CollectionUtils.find(mediaContainer.getMedias(),
											new BeanPropertyValueEqualsPredicate("mediaFormat.qualifier", "180Wx180H", true));
									if (null == media)
									{
										media = (MediaModel) CollectionUtils.find(mediaContainer.getMedias(),
												new BeanPropertyValueEqualsPredicate("mediaFormat.qualifier", "300Wx300H", true));
									}
									if (null == media)
									{
										media = (MediaModel) CollectionUtils.find(mediaContainer.getMedias(),
												new BeanPropertyValueEqualsPredicate("mediaFormat.qualifier", "1200Wx1200H", true));
									}
									if (null != media)
									{
										mediaUrl = media.getURL();
										break;
									}
								}
							}
							if (StringUtils.isNotEmpty(mediaUrl))
							{
								break;
							}
						}
					}
				}
			}
		}
		catch (final Exception e)
		{
			//
		}
		return mediaUrl;
	}

	protected Collection<FieldValue> createFieldValues(final IndexedProperty indexedProperty, final String value)
	{
		final List fieldValues = new ArrayList();

		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}

		return fieldValues;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}