package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.model.ApparelSizeVariantProductModel;
import de.hybris.clicks.core.model.ApparelStyleVariantProductModel;
import de.hybris.clicks.core.util.GrossPriceUtil;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.ProductFixedPricePromotionModel;
import de.hybris.platform.promotions.model.ProductPercentageDiscountPromotionModel;
import de.hybris.platform.promotions.model.ProductPromotionModel;
import de.hybris.platform.promotions.util.Helper;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author swapnil/praveen
 *
 */
public class GrossPromotionAppliedPriceValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider,
		Serializable
{
	private static final Logger LOG = Logger.getLogger(GrossPromotionAppliedPriceValueProvider.class);
	@Resource(name = "commercePriceService")
	private CommercePriceService commercePriceService;
	private FieldNameProvider fieldNameProvider;
	private PromotionsService promotionService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected PromotionsService getPromotionsService()
	{
		return this.promotionService;
	}

	@Required
	public void setPromotionsService(final PromotionsService promotionService)
	{
		this.promotionService = promotionService;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection fieldValues = new ArrayList();

		if (model instanceof ApparelSizeVariantProductModel)
		{
			LOG.debug("Size Variant : Fetching Gross promotion");
			fieldValues.addAll(createFieldValue((ApparelSizeVariantProductModel) model, indexConfig, indexedProperty, null));
		}
		else if (model instanceof ApparelStyleVariantProductModel)
		{
			LOG.debug("Style Variant : Fetching Gross promotion from size variant");
			final ApparelStyleVariantProductModel styleVariant = (ApparelStyleVariantProductModel) model;

			final Collection<VariantProductModel> variants = ((ApparelStyleVariantProductModel) model).getVariants();
			for (final VariantProductModel variantProductModel : variants)
			{
				if (indexedProperty.isMultiValue())
				{
					fieldValues.addAll(createFieldValues(variantProductModel, indexConfig, indexedProperty));
				}
				else
				{
					fieldValues.addAll(createFieldValue(variantProductModel, indexConfig, indexedProperty, styleVariant));
					break;
				}
			}
		}

		return fieldValues;
	}

	protected List<FieldValue> createFieldValue(final VariantProductModel variantProduct, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty, final ApparelStyleVariantProductModel styleVariant)
	{
		final List<Double> allPrices = new ArrayList<Double>(2);
		final List fieldValues = new ArrayList();
		final BigDecimal currentPrice = getCurrentPrice(variantProduct);
		LOG.debug("++++CurrentPrice for variant product" + variantProduct.getCode() + "is" + currentPrice
				+ "+++++++++++++++++++++++++++++++++++");
		//Double grossPriceWithPromotionApplied = new Double(0.0);
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if ((baseSiteModel != null) && (baseSiteModel.getDefaultPromotionGroup() != null))
		{
			final Iterator localIterator = getPromotionsService().getProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), variantProduct,
					Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute()).iterator();
			if (localIterator.hasNext())
			{
				final ProductPromotionModel promotion = (ProductPromotionModel) localIterator.next();

				if (promotion instanceof ProductPercentageDiscountPromotionModel
						|| promotion instanceof ProductFixedPricePromotionModel)
				{
					final Double grossPriceWithPromotionApplied = GrossPriceUtil.getGrossPrice(promotion, currentPrice);
					addFieldValues(fieldValues, indexedProperty, null, grossPriceWithPromotionApplied);
				}
				else
				{

					addFieldValues(fieldValues, indexedProperty, null, currentPrice);
				}
			}
			else
			{
				//TODO : Swapnil : Need to change the below logic as it is double iteration on variants. Current method is in iteration and 2nd loop is below.
				Double priceVal = null;
				final Date currentDate = new Date();
				if (null == styleVariant)
				{
					for (final PriceRowModel priceRow : variantProduct.getEurope1Prices())
					{
						if (null != priceRow.getStartTime() && null != priceRow.getEndTime())
						{
							if (priceRow.getStartTime().compareTo(currentDate) == -1
									&& priceRow.getEndTime().compareTo(currentDate) == 1)
							{
								priceVal = priceRow.getPrice();
								break;
							}
						}
					}
				}
				else
				{
					for (final VariantProductModel vPMpdel : styleVariant.getVariants())
					{
						for (final PriceRowModel priceRow : vPMpdel.getEurope1Prices())
						{
							if (null != priceRow.getStartTime() && null != priceRow.getEndTime())
							{
								if (priceRow.getStartTime().compareTo(currentDate) == -1
										&& priceRow.getEndTime().compareTo(currentDate) == 1)
								{
									allPrices.add(priceRow.getPrice());
									priceVal = priceRow.getPrice();
								}
							}
						}
					}

					Collections.sort(allPrices);
					if (CollectionUtils.isNotEmpty(allPrices))
					{
						priceVal = allPrices.get(0);
					}
				}

				addFieldValues(fieldValues, indexedProperty, null, priceVal);
			}
		}

		return fieldValues;
	}

	/**
	 * @param variantProduct
	 * @return
	 */
	private BigDecimal getCurrentPrice(final ProductModel variantProduct)
	{
		BigDecimal pricevalue = BigDecimal.ZERO;
		try
		{
			final PriceInformation price = getCommercePriceService().getFromPriceForProduct(variantProduct);
			if (null != price && null != price.getPriceValue())
			{
				pricevalue = BigDecimal.valueOf(price.getPriceValue().getValue());
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while getting price in value provider " + e.getMessage());
		}
		if (null == pricevalue || BigDecimal.ZERO.equals(pricevalue))
		{
			final Date currentDate = new Date();
			for (final PriceRowModel priceRow : variantProduct.getEurope1Prices())
			{
				if (null != priceRow.getStartTime() && null != priceRow.getEndTime())
				{
					if (priceRow.getStartTime().compareTo(currentDate) == -1 && priceRow.getEndTime().compareTo(currentDate) == 1)
					{
						pricevalue = BigDecimal.valueOf(priceRow.getPrice());
						break;
					}
				}
			}
		}
		return pricevalue;
	}

	protected List<FieldValue> createFieldValues(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final BigDecimal currentPrice = getCurrentPrice(product);
		final List fieldValues = new ArrayList();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if ((baseSiteModel != null) && (baseSiteModel.getDefaultPromotionGroup() != null))
		{
			Double grossPriceWithPromotionApplied = 0.0d;
			final List<ProductPromotionModel> promotions = getPromotionsService().getProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product,
					Config.getBoolean("evaluate.promo.restriction", true), Helper.getDateNowRoundedToMinute());
			for (final ProductPromotionModel promotion : promotions)
			{
				if (promotion instanceof ProductPercentageDiscountPromotionModel
						|| promotion instanceof ProductFixedPricePromotionModel)
				{
					grossPriceWithPromotionApplied = GrossPriceUtil.getGrossPrice(promotion, currentPrice);
					if (null != grossPriceWithPromotionApplied && grossPriceWithPromotionApplied > 0.0)
					{
						break;
					}
				}
			}
			if (null != grossPriceWithPromotionApplied && grossPriceWithPromotionApplied > 0.0)
			{
				addFieldValues(fieldValues, indexedProperty, null, grossPriceWithPromotionApplied);
			}
			else
			{
				addFieldValues(fieldValues, indexedProperty, null, currentPrice);
			}
		}
		return fieldValues;
	}

	protected void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty,
			final LanguageModel language, final Object value)
	{
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty,
				(language == null) ? null : language.getIsocode());
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
	}

	/**
	 * @return the commercePriceService
	 */
	public CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}

	/**
	 * @param commercePriceService
	 *           the commercePriceService to set
	 */
	public void setCommercePriceService(final CommercePriceService commercePriceService)
	{
		this.commercePriceService = commercePriceService;
	}
}
