/**
 * 
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.clicks.core.enums.ArticleTypesEnum;
import de.hybris.clicks.core.model.ArticleModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author siva.reddy
 * 
 */
public class ArticleTypeValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable
{

	private FieldNameProvider fieldNameProvider;

	@SuppressWarnings("deprecation")
	@Override
	@Deprecated
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ArticleModel)
		{
			return createFieldValues(indexedProperty, null != ((ArticleModel) model).getArticleType() ? ((ArticleModel) model)
					.getArticleType().getCode() : "");
		}
		return Collections.EMPTY_LIST;
	}

	protected Collection<FieldValue> createFieldValues(final IndexedProperty indexedProperty, final String articletype)
	{
		if ((ArticleTypesEnum.HEALTH.getCode().equals(articletype) || ArticleTypesEnum.MEDICINES.getCode().equals(articletype) || ArticleTypesEnum.CONDITIONS
				.getCode().equals(articletype) || ArticleTypesEnum.VITAMINS.getCode().equals(
				articletype)))
		{
			return createFieldValues(indexedProperty, true);
		}
		else
		{
			return createFieldValues(indexedProperty, false);
		}
	}

	protected Collection<FieldValue> createFieldValues(final IndexedProperty indexedProperty, final boolean value)
	{
		final List fieldValues = new ArrayList();

		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}

		return fieldValues;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return this.fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

}
