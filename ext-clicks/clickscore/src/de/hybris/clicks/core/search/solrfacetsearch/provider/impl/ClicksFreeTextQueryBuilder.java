/**
 * 
 */
package de.hybris.clicks.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.querybuilder.impl.NonFuzzyFreeTextQueryBuilder;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.util.ClientUtils;


/**
 * @author sridhar.reddy
 * 
 */
public class ClicksFreeTextQueryBuilder extends NonFuzzyFreeTextQueryBuilder
{
	@Override
	protected void addFreeTextQuery(final SearchQuery searchQuery, final String field, final String value, String suffixOp,
			final double boost)
	{
		if (StringUtils.isEmpty(suffixOp))
		{
			suffixOp = "*";
		}
		searchQuery.searchInField(field, ClientUtils.escapeQueryChars(value) + "^" + boost, SearchQuery.Operator.OR);
		searchQuery
				.searchInField(field, ClientUtils.escapeQueryChars(value) + suffixOp + "^" + boost / 2d, SearchQuery.Operator.OR);
		searchQuery.searchInField(field, suffixOp + ClientUtils.escapeQueryChars(value) + suffixOp + "^" + boost / 3d,
				SearchQuery.Operator.OR);
		searchQuery
				.searchInField(field, suffixOp + ClientUtils.escapeQueryChars(value) + "^" + boost / 4d, SearchQuery.Operator.OR);
	}
}
