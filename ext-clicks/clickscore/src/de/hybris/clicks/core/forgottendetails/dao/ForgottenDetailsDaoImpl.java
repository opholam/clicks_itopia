/**
 *
 */
package de.hybris.clicks.core.forgottendetails.dao;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


/**
 * @author siva.reddy
 *
 */
public class ForgottenDetailsDaoImpl implements ForgottenDetailsDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.forgottendetails.dao.ForgottenDetailsDao#checkClubcardResidentExists(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<CustomerModel> checkClubcardResidentExists(final String clubCardNumber, final String residentNumber)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("memberID", clubCardNumber);
		params.put("RSA_ID", residentNumber);

		final String query = "SELECT {" + CustomerModel.PK + "} FROM {" + CustomerModel._TYPECODE + "}" + " WHERE {"
				+ CustomerModel.MEMBERID + "} = ?memberID AND {" + CustomerModel.RSA_ID + "}=?RSA_ID";

		final SearchResultImpl<CustomerModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<CustomerModel> result = searchResult.getResult();

		return result == null ? Collections.EMPTY_LIST : result;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.forgottendetails.dao.ForgottenDetailsDao#checkClubcardDOBExists(java.lang.String,
	 * java.util.Date)
	 */
	@Override
	public List<CustomerModel> checkClubcardDOBExists(final String clubCardNumber, final Date dob)
	{

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("MEMBERID", clubCardNumber);
		params.put("NONRSA_DOB", dob);

		final String query = "SELECT {" + CustomerModel.PK + "} FROM {" + CustomerModel._TYPECODE + "}" + " WHERE {"
				+ CustomerModel.MEMBERID + "} = ?MEMBERID AND {" + CustomerModel.NONRSA_DOB + "}=?NONRSA_DOB";

		final SearchResultImpl<CustomerModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<CustomerModel> result = searchResult.getResult();

		return result == null ? Collections.EMPTY_LIST : result;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.clicks.core.forgottendetails.dao.ForgottenDetailsDao#checkResidentExists(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<CustomerModel> checkResidentExists(final String clubCardNumber, final String residentNumber)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("memberID", clubCardNumber);
		//params.put("RSA_ID", residentNumber);

		final String query = "SELECT {" + CustomerModel.PK + "} FROM {" + CustomerModel._TYPECODE + "}" + " WHERE {"
				+ CustomerModel.MEMBERID + "} = ?memberID";

		final SearchResultImpl<CustomerModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<CustomerModel> result = searchResult.getResult();

		return result == null ? Collections.EMPTY_LIST : result;
	}

}
