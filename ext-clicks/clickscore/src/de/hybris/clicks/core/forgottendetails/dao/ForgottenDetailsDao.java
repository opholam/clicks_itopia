/**
 *
 */
package de.hybris.clicks.core.forgottendetails.dao;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Date;
import java.util.List;


/**
 * @author siva.reddy
 *
 */
public interface ForgottenDetailsDao
{

	/**
	 * Check clubcard resident exists.
	 *
	 * @param clubCardNumber
	 *           the club card number
	 * @param residentNumber
	 *           the resident number
	 * @return the list
	 */
	List<CustomerModel> checkClubcardResidentExists(final String clubCardNumber, final String residentNumber);

	/**
	 * Check clubcard dob exists.
	 *
	 * @param clubCardNumber
	 *           the club card number
	 * @param residentNumber
	 *           the resident number
	 * @return the list
	 */
	List<CustomerModel> checkClubcardDOBExists(final String clubCardNumber, final Date residentNumber);

	/**
	 * Check resident exists.
	 *
	 * @param clubCardNumber
	 *           the club card number
	 * @param residentNumber
	 *           the resident number
	 * @return the list
	 */
	List<CustomerModel> checkResidentExists(String clubCardNumber, String residentNumber);
}
