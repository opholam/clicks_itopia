/**
 *
 */
package de.hybris.clicks.core.forgottendetails;

import de.hybris.clicks.core.forgottendetails.dao.ForgottenDetailsDao;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author siva.reddy
 *
 */
public class ForgottenDetailsServiceImpl implements ForgottenDetailsService
{

	@Resource(name = "forgottenDetailsDao")
	private ForgottenDetailsDao forgottenDetailsDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.forgottendetails.ForgottenDetailsService#checkClubcardResidentExists(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public CustomerModel checkClubcardResidentExists(final String clubCardNumber, final String residentNumber)
	{
		final List<CustomerModel> customerModelList = forgottenDetailsDao.checkClubcardResidentExists(clubCardNumber,
				residentNumber);
		if (!customerModelList.isEmpty())
		{
			final CustomerModel customer = customerModelList.get(0);
			return customer;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.forgottendetails.ForgottenDetailsService#checkClubcardDOBExists(java.lang.String,
	 * java.util.Date)
	 */
	@Override
	public CustomerModel checkClubcardDOBExists(final String clubCardNumber, final Date dob)
	{
		final List<CustomerModel> customerModelList = forgottenDetailsDao.checkClubcardDOBExists(clubCardNumber, dob);
		if (!customerModelList.isEmpty())
		{
			final CustomerModel customer = customerModelList.get(0);
			return customer;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.forgottendetails.ForgottenDetailsService#checkResidentExists(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public CustomerModel checkResidentExists(final String clubcardNumber, final String residentNumber)
	{
		final List<CustomerModel> customerModelList = forgottenDetailsDao.checkResidentExists(clubcardNumber, residentNumber);
		if (!customerModelList.isEmpty())
		{
			final CustomerModel customer = customerModelList.get(0);
			return customer;
		}
		return null;
	}


}
