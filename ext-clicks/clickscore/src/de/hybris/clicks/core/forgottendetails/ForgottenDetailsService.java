/**
 *
 */
package de.hybris.clicks.core.forgottendetails;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Date;


/**
 * @author siva.reddy
 *
 */
public interface ForgottenDetailsService
{

	/**
	 * Check clubcard resident exists.
	 *
	 * @param clubCardNumber
	 *           the club card number
	 * @param residentNumber
	 *           the resident number
	 * @return the customer model
	 */
	CustomerModel checkClubcardResidentExists(final String clubCardNumber, final String residentNumber);

	/**
	 * Check clubcard dob exists.
	 *
	 * @param clubCardNumber
	 *           the club card number
	 * @param date
	 *           the date
	 * @return the customer model
	 */
	CustomerModel checkClubcardDOBExists(final String clubCardNumber, final Date date);

	/**
	 * Check resident exists.
	 *
	 * @param clubcardNumber
	 *           the clubcard number
	 * @param residentNumber
	 *           the resident number
	 * @return the customer model
	 */
	CustomerModel checkResidentExists(String clubcardNumber, String residentNumber);
}
