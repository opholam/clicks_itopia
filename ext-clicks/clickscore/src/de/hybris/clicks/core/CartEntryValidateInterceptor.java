/**
 *
 */
package de.hybris.clicks.core;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 * 
 */
public class CartEntryValidateInterceptor implements ValidateInterceptor<CartEntryModel>
{

	protected static final Logger LOG = Logger.getLogger(CartEntryValidateInterceptor.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object,
	 * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
	 */
	@Override
	public void onValidate(final CartEntryModel cartEntry, final InterceptorContext arg1) throws InterceptorException
	{
		//
		if (null != cartEntry && null != cartEntry.getOrder() && Boolean.FALSE.equals(cartEntry.getOrder().getBasketEditable())
				&& CollectionUtils.isNotEmpty(cartEntry.getOrder().getPaymentTransactions()))
		{
			LOG.error("trying to modify cart entry when payment inprogress... " + cartEntry.getOrder().getCode());
			throw new InterceptorException("Payment in progress. Cart entry cannot be modified", this);
		}
	}

}
