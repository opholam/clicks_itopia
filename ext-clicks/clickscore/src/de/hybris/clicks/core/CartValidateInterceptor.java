/**
 *
 */
package de.hybris.clicks.core;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 * 
 */
public class CartValidateInterceptor implements ValidateInterceptor<CartModel>
{
	protected static final Logger LOG = Logger.getLogger(CartValidateInterceptor.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.servicelayer.interceptor.ValidateInterceptor#onValidate(java.lang.Object,
	 * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
	 */
	@Override
	public void onValidate(final CartModel cart, final InterceptorContext arg1) throws InterceptorException
	{
		if (!arg1.isModified(cart, CartModel.BASKETEDITABLE) && Boolean.FALSE.equals(cart.getBasketEditable())
				&& CollectionUtils.isNotEmpty(cart.getPaymentTransactions()))
		{
			LOG.error("trying to modify cart when payment inprogress... " + cart.getCode());
			throw new InterceptorException("Payment in progress. Cart cannot be modified", this);
		}
	}

}
