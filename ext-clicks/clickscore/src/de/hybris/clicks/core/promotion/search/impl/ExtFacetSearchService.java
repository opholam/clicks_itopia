/**
 *
 */
package de.hybris.clicks.core.promotion.search.impl;

import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultFacetSearchService;

import org.apache.log4j.Logger;


/**
 * @author manikandan.gopalan
 *
 */
public class ExtFacetSearchService extends DefaultFacetSearchService
{
	private static final Logger LOG = Logger.getLogger(ExtFacetSearchService.class);

	@Override
	protected void checkCatalogVersions(final SearchQuery query) throws FacetSearchException
	{
		LOG.info("in custom service");
		if (query.getIndexedType().getCode().equalsIgnoreCase("AbstractPromotion"))
		{
			return;
		}
		if ((query.getCatalogVersions() == null) || (query.getCatalogVersions().isEmpty()))
		{
			//	final FacetSearchConfig config = query.getFacetSearchConfig();
			//query.setCatalogVersions(getSolrQueryCatalogVersionsResolver().resolveQueryCatalogVersions(config));
		}
	}
}
