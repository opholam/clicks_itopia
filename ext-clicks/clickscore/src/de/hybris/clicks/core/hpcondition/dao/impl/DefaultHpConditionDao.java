/**
 *
 */
package de.hybris.clicks.core.hpcondition.dao.impl;

import de.hybris.clicks.core.hpcondition.dao.HpConditionDao;
import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author anshul/Swapnil
 * 
 */
public class DefaultHpConditionDao implements HpConditionDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;



	@Override
	public List<ConditionsModel> findConditionsByType(final String conditionType)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("conditionType", conditionType);
		params.put("isActive", Boolean.TRUE);

		final String query = "select {PK} from {conditions as c join ConditionTypeEnum as ct on {c:conditionType}={ct:pk}} where {ct:code}= ?conditionType AND  {c:"
				+ ConditionsModel.ISACTIVE + "}=?isActive";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query, params);
		searchQuery.setResultClassList(Collections.singletonList(ConditionsModel.class));
		final SearchResult searchResult = flexibleSearchService.search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<ConditionsModel> findAllConditions(final String type)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("type", type);
		params.put("isActive", Boolean.TRUE);
		final String query = "select {PK} from {conditions as c join ConditionTypeEnum as ct on {c:conditionType}={ct:pk}} where {ct:code}= ?type AND { "
				+ ConditionsModel.ARTICLE + "} IS NOT NULL AND {c:" + ConditionsModel.ISACTIVE + "}=?isActive";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query, params);
		searchQuery.setResultClassList(Collections.singletonList(ConditionsModel.class));
		final SearchResult searchResult = flexibleSearchService.search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public <T extends AbstractItemModel> T getConditionsListByConditionId(final String id)
	{
		String query = "";
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		query = "SELECT {" + ConditionsModel.PK + "} FROM {" + ConditionsModel._TYPECODE + "}" + " WHERE {conditionId} = '" + id
				+ "'";

		final SearchResultImpl<AbstractItemModel> searchResult = (SearchResultImpl) flexibleSearchService.search(query, params);
		final List<AbstractItemModel> result = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(result))
		{
			return (T) result.get(0);
		}
		else
		{
			return (T) new ItemModel();
		}
	}
}
