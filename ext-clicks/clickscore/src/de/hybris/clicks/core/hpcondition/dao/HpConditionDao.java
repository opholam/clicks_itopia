/**
 *
 */
package de.hybris.clicks.core.hpcondition.dao;

import de.hybris.clicks.core.model.ConditionsModel;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.util.List;


/**
 * @author anshul/Swapnil
 *
 */
public interface HpConditionDao
{
	List<ConditionsModel> findAllConditions(String type);

	List<ConditionsModel> findConditionsByType(String conditionType);

	<T extends AbstractItemModel> T getConditionsListByConditionId(String id);
}
