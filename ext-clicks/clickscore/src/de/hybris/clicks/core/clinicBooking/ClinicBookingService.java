/**
 *
 */
package de.hybris.clicks.core.clinicBooking;

import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;

import java.util.Collection;
import java.util.List;


/**
 * @author shruthi.jhamb
 *
 */
public interface ClinicBookingService
{

	/**
	 * @return
	 */
	Collection<ProvinceData> getProvinceList();

	/**
	 * @param code
	 * @return
	 */
	List<PreferredClinicData> getpreferredClinicList(String code);

	/**
	 * @param bookingData
	 * @return
	 */
	boolean processClinicBooking(ClinicBookingData bookingData);

}
