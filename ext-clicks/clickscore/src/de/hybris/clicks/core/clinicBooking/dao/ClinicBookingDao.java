/**
 *
 */
package de.hybris.clicks.core.clinicBooking.dao;

import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.List;


/**
 * /**
 *
 * @author shruti.jhamb
 *
 */
public interface ClinicBookingDao
{
	public SearchResult<LinkModel> fetchStoreLocation(final String code);

	/**
	 * @return
	 */
	public Collection<ProvinceData> getProvincesDataList();

	/**
	 * @return
	 */
	public List<PreferredClinicData> getPreferedClinicsList(String code);
}
