/**
 *
 */
package de.hybris.clicks.core.clinicBooking;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.clicks.core.clinicBooking.dao.ClinicBookingDao;
import de.hybris.clicks.core.model.ClinicBookingAppointmentModel;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.ClinicBookingData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author shruthi.jhamb
 *
 */
public class DefaultClinicBookingService implements ClinicBookingService
{

	private static final Logger LOG = Logger.getLogger(DefaultClinicBookingService.class);
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "clinicBookingDao")
	private ClinicBookingDao clinicBookingDao;

	@Resource
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "baseStoreService")
	BaseStoreService baseStoreService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "emailService")
	private EmailService emailService;

	@Resource
	private UserService userService;
	@Resource
	private ConfigurationService configurationService;


	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clinicBooking.ClinicBookingService#getProvinceList()
	 */
	@Override
	public Collection<ProvinceData> getProvinceList()
	{
		final Collection<ProvinceData> provinceList = clinicBookingDao.getProvincesDataList();
		return provinceList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clinicBooking.ClinicBookingService#getpreferredClinicList(java.lang.String)
	 */
	@Override
	public List<PreferredClinicData> getpreferredClinicList(final String code)
	{
		final List<PreferredClinicData> preferredClinicDataList = clinicBookingDao.getPreferedClinicsList(code);
		return preferredClinicDataList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.clicks.core.clinicBooking.ClinicBookingService#processClinicBooking(de.hybris.platform.commercefacades
	 * .user.data.ClinicBookingData)
	 */
	@Override
	public boolean processClinicBooking(final ClinicBookingData bookingData)
	{
		boolean result = false;
		final String prop = (String) configurationService.getConfiguration().getProperty("email.address");
		final String[] emailList = prop.split(",");
		for (final String adminEmail : emailList)
		{
			CustomerModel admin = null;
			try
			{
				admin = (CustomerModel) userService.getUserForUID(adminEmail);
			}
			catch (final Exception exp)
			{
				admin = modelService.create(CustomerModel.class);
				admin.setUid(adminEmail);
				//admin = modelService.create(adminEmail);
			}
			validateParameterNotNullStandardMessage("bookingData", bookingData);
			final Random gen = new Random();
			final Integer custid = gen.nextInt();
			admin.getUid();
			ClinicBookingAppointmentModel clinicBookingModel = modelService.create(ClinicBookingAppointmentModel.class);


			try
			{

				admin.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
				admin.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
				clinicBookingModel = populateClinicBookingModel(bookingData, clinicBookingModel);
				admin.setName("admin");
				admin.setEmailID(adminEmail);
				admin.setCustomerID(custid.toString());
				processEmail(admin, clinicBookingModel, bookingData.getProcessName());
				modelService.save(clinicBookingModel);
				result = true;

			}
			catch (final Exception e)
			{
				LOG.error(e);
			}

		}
		return result;
	}

	/**
	 * @param admin
	 * @param clinicBookingModel
	 * @param processName
	 */
	private void processEmail(final CustomerModel admin, final ClinicBookingAppointmentModel clinicBookingModel,
			final String processName)
	{
		final String process = processName;
		try
		{
			final StoreFrontCustomerProcessModel clinicBookingProcessModel = (StoreFrontCustomerProcessModel) businessProcessService
					.createProcess("process" + System.currentTimeMillis(), process);
			clinicBookingProcessModel.setCustomer(admin);
			clinicBookingProcessModel.setLanguage(commonI18NService.getCurrentLanguage());
			clinicBookingProcessModel.setCurrency(commonI18NService.getCurrentCurrency());
			clinicBookingProcessModel.setStore(baseStoreService.getCurrentBaseStore());
			clinicBookingProcessModel.setSite(cmsSiteService.getCurrentSite());
			clinicBookingProcessModel.setClinicBookingAppointment(clinicBookingModel);
			modelService.save(clinicBookingProcessModel);
			businessProcessService.startProcess(clinicBookingProcessModel);
		}
		catch (final Exception e)
		{
			LOG.warn("Exception while starting process for sending email : ", e);
			//System.out.println("Exception while starting process for sending email : " + e);
		}
	}

	private ClinicBookingAppointmentModel populateClinicBookingModel(final ClinicBookingData source,
			final ClinicBookingAppointmentModel target)
	{
		if (null != source.getFirstName())
		{
			target.setFirstName(source.getFirstName());
		}
		if (null != source.getLastName())
		{
			target.setLastName(source.getLastName());
		}
		if (null != source.getContactNumber())
		{
			target.setContactNumber(source.getContactNumber());
		}


		if (null != source.getRegion())
		{

			ProvinceModel provinceModel = modelService.create(ProvinceModel.class);
			provinceModel.setCode(source.getRegion());
			try
			{
				provinceModel = flexibleSearchService.getModelByExample(provinceModel);
			}
			catch (final Exception e)
			{
				LOG.error(e);
			}

			target.setClinicProvince(provinceModel.getName());
		}
		if (StringUtils.isNotEmpty(source.getPreferredClinic()))
		{
			target.setPreferredClinic(source.getPreferredClinic());
		}
		if (StringUtils.isNotEmpty(source.getAlternativeClinic()))
		{
			target.setAlternativeClinic(source.getAlternativeClinic());
		}
		return target;
	}
}
