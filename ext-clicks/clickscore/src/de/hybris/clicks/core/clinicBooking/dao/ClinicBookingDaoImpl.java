/**
 *
 */
package de.hybris.clicks.core.clinicBooking.dao;

import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.facades.product.data.ProvinceData;
import de.hybris.platform.commercefacades.user.data.PreferredClinicData;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author shruti.jhamb
 *
 */
public class ClinicBookingDaoImpl implements ClinicBookingDao
{
	@Resource
	FlexibleSearchService flexibleSearchService;

	@Override
	public SearchResult<LinkModel> fetchStoreLocation(final String code)
	{
		final String query = "select {pk} from {StoreLocation2StoreLocatorFeature as s join pointofservice as pos on {s:source}={pos:pk} join StoreLocatorFeature as f on {s:target}={f:pk} join address as a on {pos:address}={a:pk}} where {f:name}='Clinic' and {a:town} like '%"
				+ code + "%'";
		final SearchResult<LinkModel> result = flexibleSearchService.search(query);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clinicBooking.dao.ClinicBookingDao#getProvincesDataList()
	 */
	@Override
	public Collection<ProvinceData> getProvincesDataList()
	{
		final Collection<ProvinceData> provinceList = new ArrayList<ProvinceData>();
		ProvinceData province;
		final String query = "SELECT {PK} FROM {Province}";
		final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);

		if (result.getCount() > 0)
		{
			for (final ProvinceModel provinceModel : result.getResult())
			{
				province = new ProvinceData();
				province.setCode(provinceModel.getCode());
				province.setName(provinceModel.getName());
				provinceList.add(province);
			}
		}
		return provinceList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.clinicBooking.dao.ClinicBookingDao#getPreferedClinicsList()
	 */
	@Override
	public List<PreferredClinicData> getPreferedClinicsList(final String code)
	{
		final List<PreferredClinicData> preferredClinicDataList = new ArrayList<PreferredClinicData>();
		PreferredClinicData preferredClinicData;
		final SearchResult<LinkModel> result = fetchStoreLocation(code);
		if (result.getCount() > 0)
		{
			for (final LinkModel linkModel : result.getResult())
			{
				preferredClinicData = new PreferredClinicData();
				final PointOfServiceModel pointOfServiceModel = (PointOfServiceModel) linkModel.getSource();
				preferredClinicData.setCode(pointOfServiceModel.getName());
				preferredClinicData.setName(pointOfServiceModel.getDisplayName());

				preferredClinicDataList.add(preferredClinicData);
			}
		}
		return preferredClinicDataList;
	}
}
