/**
 *
 */
package de.hybris.clicks.core.editProfile;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.clicks.core.model.ConsentModel;
import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.clicks.core.model.Type_consentModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;



/**
 * @author shruthi.jhamb
 *
 */
public class DefaultEditProfileService implements EditProfileService
{
	@Resource
	private UserService userService;
	@Resource
	private ModelService modelService;
	@Resource
	FlexibleSearchService flexibleSearchService;

	private static final Logger LOG = Logger.getLogger(DefaultEditProfileService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.clicks.core.editProfile.EditProfileService#saveNonClubCardData()
	 */
	@Override
	public boolean saveNonClubCardData(final CustomerData customer)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		customerModel.setOriginalUid(customer.getDisplayUid());
		final boolean result = populateNonCCCustModel(customer, customerModel);
		try
		{
			if (null != customer.getEmailID())
			{
				customerModel.setUid(customer.getEmailID());
			}

			modelService.save(customerModel);
		}
		catch (final ModelSavingException saveexp)
		{
			LOG.info("error in saving model" + saveexp);
		}
		return result;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.clicks.core.editProfile.EditProfileService#saveClubCardData(de.hybris.platform.commercefacades.user.
	 * data.CustomerData)
	 */
	@Override
	public boolean saveClubCardData(final CustomerData customer)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		customerModel.setOriginalUid(customer.getDisplayUid());
		try
		{
			final boolean result = populateCCCustModel(customer, customerModel);
			if (result)
			{
				if (null != customer.getEmailID())
				{
					customerModel.setUid(customer.getEmailID());
				}
				modelService.save(customerModel);
				// start - for separate thread
			}
			return result;
		}
		catch (final Exception saveexp)
		{
			LOG.error(saveexp.getMessage());
			return false;
		}
	}

	private boolean populateNonCCCustModel(final CustomerData customer, final CustomerModel customerModel)
	{
		if (null != customer.getFirstName())
		{
			customerModel.setFirstName(WordUtils.capitalize(customer.getFirstName().toLowerCase()));
		}
		if (null != customer.getPreferedName() || !StringUtils.isEmpty(customer.getPreferedName()))
		{
			customerModel.setName(WordUtils.capitalize(customer.getPreferedName().toLowerCase()));
		}
		else if (customer.getPreferedName() == null || StringUtils.isEmpty(customer.getPreferedName()))
		{
			if (customerModel.getName() == null || StringUtils.isEmpty(customerModel.getName()))
			{
				customerModel.setName(WordUtils.capitalize(customer.getFirstName().toLowerCase()));
			}
			else
			{
				customerModel.setName(WordUtils.capitalize(customerModel.getName().toLowerCase()));
			}
		}
		if (null != customer.getLastName())
		{
			customerModel.setLastName(WordUtils.capitalize(customer.getLastName().toLowerCase()));
		}
		//if (null != customer.getEmailID())
		//{
		//customerModel.setEmailID(customer.getEmailID());
		//}

		if (null != customer.getEmailID())
		{
			customerModel.setUid(customer.getEmailID());
		}

		if (null != customer.getSAResident())
		{
			customerModel.setSAResident(customer.getSAResident());
			if (customerModel.getSAResident().booleanValue() && null != customer.getIDnumber())
			{
				customerModel.setRSA_ID(customer.getIDnumber());
			}
			if (null != customer.getNonRSA_DOB())
			{
				customerModel.setNonRSA_DOB(customer.getNonRSA_DOB());
			}
		}
		return true;
	}

	private boolean populateCCCustModel(final CustomerData customer, final CustomerModel customerModel)
	{
		if (null != customer.getFirstName())
		{
			customerModel.setFirstName(WordUtils.capitalize(customer.getFirstName().toLowerCase()));
		}
		if (null != customer.getPreferedName())
		{
			customerModel.setName(WordUtils.capitalize(customer.getPreferedName().toLowerCase()));
		}
		if (customer.getPreferedName() == null && null != customerModel.getFirstName())
		{
			customerModel.setName(WordUtils.capitalize(customerModel.getFirstName().toLowerCase()));
		}
		if (null != customer.getLastName())
		{
			customerModel.setLastName(WordUtils.capitalize(customer.getLastName().toLowerCase()));
		}
		//if (null != customer.getEmailID())
		//{
		//customerModel.setEmailID(customer.getEmailID());
		//}

		if (null != customer.getEmailID())
		{
			customerModel.setUid(customer.getEmailID());
		}


		if (null != customer.getGender())
		{
			if (null != customer.getGender() && customer.getGender().equalsIgnoreCase("1"))
			{
				customerModel.setGender(Gender.MALE);
			}
			if (null != customer.getGender() && customer.getGender().equalsIgnoreCase("2"))
			{
				customerModel.setGender(Gender.FEMALE);
			}
		}
		if (null != customer.getSAResident())
		{
			customerModel.setSAResident(customer.getSAResident());

			if (customerModel.getSAResident().booleanValue() && null != customer.getIDnumber())
			{
				customerModel.setRSA_ID(customer.getIDnumber());
			}
			if (null != customer.getNonRSA_DOB())
			{
				customerModel.setNonRSA_DOB(customer.getNonRSA_DOB());
			}
		}
		if (CollectionUtils.isNotEmpty(customer.getAddresses()))
		{
			final List<AddressModel> address = new ArrayList<AddressModel>();
			for (final AddressData addressData : customer.getAddresses())
			{
				//addressData.setOwner(customer);
				AddressModel custAddressModel = (AddressModel) CollectionUtils.find(customerModel.getAddresses(),
						new BeanPropertyValueEqualsPredicate("type", "Postal Address", true));
				if (null == custAddressModel)
				{
					custAddressModel = new AddressModel();
				}
				custAddressModel.setOwner(customerModel);
				if (null != addressData.getLine1())
				{
					custAddressModel.setLine1(addressData.getLine1());
					custAddressModel.setBuilding(WordUtils.capitalize(addressData.getLine1().toLowerCase()));
				}
				if (null != addressData.getLine2())
				{
					custAddressModel.setLine2(addressData.getLine2());
					custAddressModel.setStreetname(WordUtils.capitalize(addressData.getLine2().toLowerCase()));
				}
				if (null != addressData.getSuburb())
				{
					custAddressModel.setSuburb(addressData.getSuburb());
					custAddressModel.setStreetnumber(WordUtils.capitalize(addressData.getSuburb().toLowerCase()));
				}
				if (null != addressData.getProvince())
				{
					//custAddressModel.setProvince(addressData.getProvince());
					//final String province = addressData.getProvince();
					//final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT * FROM {Province} where {code}=?province");
					//query.addQueryParameter("province", province);
					//final SearchResult<ProvinceModel> result = flexibleSearchService.search(query);
					String query;
					SearchResult<ProvinceModel> result;
					//query = "SELECT * FROM {Province} where {code}='" + addressData.getProvince() + "'";
					//final Map<String, Object> params = new HashMap<String, Object>();
					//params.put("province", province);
					query = "SELECT {PK} FROM {Province}";
					result = flexibleSearchService.search(query);
					if (result.getCount() > 0)
					{
						for (final ProvinceModel provinceModel : result.getResult())
						{
							if (addressData.getProvince().equalsIgnoreCase(provinceModel.getName())
									|| addressData.getProvince().equalsIgnoreCase(provinceModel.getCode()))
							{
								custAddressModel.setProvince(provinceModel.getCode());
								addressData.setProvince(provinceModel.getCode());
							}
						}
					}
				}
				if (null != addressData.getCity() && null != addressData.getProvince())
				{

					custAddressModel.setTown(WordUtils.capitalize(addressData.getCity().toLowerCase()) + ", "
							+ WordUtils.capitalize(addressData.getProvince().toLowerCase()));
				}
				else if (null != addressData.getCity())
				{
					custAddressModel.setTown(WordUtils.capitalize(addressData.getCity().toLowerCase()));
				}
				/*
				 * else if (null != addressData.getProvince()) { custAddressModel.setProvince(addressData.getProvince());
				 * custAddressModel.setTown(WordUtils.capitalize(addressData.getProvince().toLowerCase())); }
				 */
				if (null != addressData.getPostalCode())
				{
					custAddressModel.setPostalcode(addressData.getPostalCode());
				}
				if (null != addressData.getCountry())
				{
					CountryModel country = new CountryModel();
					modelService.attach(country);
					country.setIsocode(addressData.getCountry().getIsocode());
					country = flexibleSearchService.getModelByExample(country);
					if (null != country)
					{
						custAddressModel.setCountry(country);
					}
				}
				address.add(custAddressModel);
			}
			for (final AddressModel addressModel : customerModel.getAddresses())
			{
				if ((Boolean.TRUE.equals(addressModel.getShippingAddress()) || Boolean.TRUE.equals(addressModel.getBillingAddress()))
						&& Boolean.TRUE.equals(addressModel.getVisibleInAddressBook()))
				{
					address.add(addressModel);
				}
			}
			customerModel.setAddresses(address);
		}
		final List<ContactDetailsModel> contactDetailsList = new ArrayList<ContactDetailsModel>();
		if (null != customer.getContactDetails() && customer.getContactDetails().size() > 0)
		{
			for (final ContactDetail contactDetail : customer.getContactDetails())
			{
				final ContactDetailsModel contactDetailsModel = new ContactDetailsModel();
				contactDetailsModel.setTypeID(contactDetail.getTypeID());
				contactDetailsModel.setNumber(contactDetail.getNumber());
				contactDetailsList.add(contactDetailsModel);
			}
		}
		if (contactDetailsList.size() > 0)
		{
			customerModel.setContactDetails(contactDetailsList);
		}
		populateConsent(customer, customerModel);
		if (null != customer.getEmailID())
		{
			customerModel.setUid(customer.getEmailID());
		}

		return true;

	}

	/**
	 * @param customer
	 * @param customerModel
	 */
	private void populateConsent(final CustomerData customer, final CustomerModel customerModel)
	{
		try
		{
			final List<ConsentModel> consentModelList = new ArrayList<ConsentModel>();
			List<Type_consentModel> typeModelList;
			ConsentModel consentModel;
			Type_consentModel typeModel;

			if (null != customer.getAccinfo_email() || null != customer.getAccinfo_SMS())
			{
				// To populate ACCINFO consent
				consentModel = modelService.create(ConsentModel.class);
				consentModel.setConsentType("ACCINFO");

				typeModelList = new ArrayList<Type_consentModel>();

				typeModel = modelService.create(Type_consentModel.class);
				typeModel.setTypeID(new Integer(1));
				typeModel.setAccepted(customer.getAccinfo_SMS());
				typeModelList.add(typeModel);

				typeModel = modelService.create(Type_consentModel.class);
				typeModel.setTypeID(new Integer(2));
				typeModel.setAccepted(customer.getAccinfo_email());
				typeModelList.add(typeModel);
				consentModel.setComms(typeModelList);
				consentModelList.add(consentModel);
			}
			if (null != customer.getMarketingConsent_SMS() || null != customer.getMarketingConsent_email())
			{
				// To populate MARKETING consent
				typeModelList = new ArrayList<Type_consentModel>();
				consentModel = modelService.create(ConsentModel.class);
				consentModel.setConsentType("MARKETING");

				if (null != customer.getMarketingConsent_SMS())
				{
					typeModel = modelService.create(Type_consentModel.class);
					typeModel.setTypeID(new Integer(1));
					typeModel.setAccepted(customer.getMarketingConsent_SMS());
					typeModelList.add(typeModel);
				}

				typeModel = modelService.create(Type_consentModel.class);
				typeModel.setTypeID(new Integer(2));
				typeModel.setAccepted(customer.getMarketingConsent_email());
				typeModelList.add(typeModel);

				consentModel.setComms(typeModelList);

				consentModelList.add(consentModel);
			}
			if (consentModelList.size() > 0)
			{
				customerModel.setMarketingConsent(consentModelList);
			}
		}
		catch (final Exception e)
		{
			//
		}
	}
}
