/**
 *
 */
package de.hybris.clicks.core.editProfile;

import de.hybris.platform.commercefacades.user.data.CustomerData;




/**
 * @author shruthi.jhamb
 *
 */
public interface EditProfileService
{

	public boolean saveNonClubCardData(final CustomerData customer);

	/**
	 * @param customer
	 * @return
	 */
	public boolean saveClubCardData(CustomerData customer);
}
