/**
 * 
 */
package de.hybris.clicks.core.dataimport.batch.converter.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter;
import de.hybris.platform.servicelayer.exceptions.SystemException;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;


/**
 * @author arshath
 * 
 */
public class CustomerImpexConverter extends DefaultImpexConverter
{
	private static final char PLUS_CHAR = '+';
	private static final char SEQUENCE_CHAR = 'S';
	private static final String EMPTY_STRING = "";
	private static final char BRACKET_END = '}';
	private static final char BRACKET_START = '{';

	private String impexRow;

	@Override
	public String convert(final Map<Integer, String> row, final Long sequenceId)
	{
		String result = EMPTY_STRING;

		if (!MapUtils.isEmpty(row))
		{
			final StringBuilder builder = new StringBuilder();
			int copyIdx = 0;
			int idx = impexRow.indexOf(BRACKET_START);
			while (idx > -1)
			{
				final int endIdx = impexRow.indexOf(BRACKET_END, idx);
				if (endIdx < 0)
				{
					throw new SystemException("Invalid row syntax [brackets not closed]: " + impexRow);
				}
				builder.append(impexRow.substring(copyIdx, idx));
				if (impexRow.charAt(idx + 1) == SEQUENCE_CHAR)
				{
					builder.append(sequenceId);
				}
				else
				{
					final boolean mandatory = impexRow.charAt(idx + 1) == PLUS_CHAR;
					Integer mapIdx = null;
					try
					{
						mapIdx = Integer.valueOf(impexRow.substring(mandatory ? idx + 2 : idx + 1, endIdx));
					}
					catch (final NumberFormatException e)
					{
						throw new SystemException("Invalid row syntax [invalid column number]: " + impexRow, e);
					}
					final String colValue = row.get(mapIdx);
					if (mandatory && StringUtils.isBlank(colValue))
					{
						throw new IllegalArgumentException("Missing value for " + mapIdx);
					}
					if (colValue != null)
					{
						builder.append(colValue.replace(";", "%semi%"));
					}
				}
				copyIdx = endIdx + 1;
				idx = impexRow.indexOf(BRACKET_START, endIdx);
			}
			if (copyIdx < impexRow.length())
			{
				builder.append(impexRow.substring(copyIdx));
			}

			result = builder.toString();
		}

		return result;
	}

	/**
	 * @param impexRow
	 *           the impexRow to set
	 */
	@Override
	public void setImpexRow(final String impexRow)
	{
		Assert.hasText(impexRow);
		this.impexRow = impexRow;
	}
}
