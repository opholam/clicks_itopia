/**
 * 
 */
package de.hybris.clicks.core.dataimport.batch.customer;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;

import org.apache.commons.lang.StringUtils;


/**
 * @author arshath
 * 
 */
public class CustomerTranslator extends AbstractSpecialValueTranslator
{
	private static final String ADAPTER_NAME = "nameImportAdapter";
	private static final String MODIFIER_NAME_ADAPTER = "adapter";
	private CustomerImportAdapter nameImportAdapter;

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor)
	{
		String beanName = columnDescriptor.getDescriptorData().getModifier(MODIFIER_NAME_ADAPTER);
		if (StringUtils.isBlank(beanName))
		{
			beanName = ADAPTER_NAME;
		}

		nameImportAdapter = (CustomerImportAdapter) Registry.getApplicationContext().getBean(beanName);
	}

	@Override
	public void performImport(final String cellValue, final Item processedItem)
	{
		nameImportAdapter.performImport(cellValue, processedItem);
	}
}
