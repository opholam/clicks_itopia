/**
 *
 */
package de.hybris.clicks.core.dataimport.batch.customer.impl;

import de.hybris.clicks.core.dataimport.batch.customer.CustomerImportAdapter;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * @author arshath
 *
 */
public class CustomerNameImportAdapter implements CustomerImportAdapter
{
	private static final Logger LOG = Logger.getLogger(CustomerNameImportAdapter.class);
	private ModelService modelService;

	@Override
	public void performImport(final String cellValue, final Item customer)
	{
		Assert.hasText(cellValue);
		Assert.notNull(customer);

		try
		{
			final String[] values = cellValue.replace("%semi%", ";").split("\\^");
			final CustomerModel customerModel = modelService.get(customer);

			if (values.length >= 3)
			{
				customerModel.setFirstName(values[0]);
				customerModel.setLastName(values[2]);

				if (values[1] == null || values[1].isEmpty())
				{
					if (values[0] == null || values[0].isEmpty())
					{
						customerModel.setName(values[2]);
					}
					else
					{
						customerModel.setName(values[0]);
					}
				}
				else
				{
					customerModel.setName(values[1]);
				}
			}

			if (values.length >= 4)
			{
				if (values[3] != null && !values[3].isEmpty())
				{
					if (values[3].equals("M"))
					{
						customerModel.setGender(Gender.MALE);
					}
					else if (values[3].equals("F"))
					{
						customerModel.setGender(Gender.FEMALE);
					}
				}
			}
			modelService.save(customerModel);
		}
		catch (final RuntimeException e)
		{
			LOG.warn("Could not import customer " + customer + ": " + e);
			throw e;
		}
		catch (final Exception e)
		{
			LOG.warn("Could not import customer " + customer + ": " + e);
			throw new SystemException("Could not import customer " + customer, e);
		}
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}
}
