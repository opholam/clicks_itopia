/**
 *
 */
package de.hybris.clicks.core.dataimport.batch.converter.impl;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author mbhargava
 *
 */
public class ClicksImpexConverter extends DefaultImpexConverter
{
	private static final char SEMICOLON_CHAR = ';';
	private static final Logger LOG = Logger.getLogger(ClicksImpexConverter.class);

	@Override
	protected String escapeQuotes(final String input)
	{
		LOG.debug("Converter input : " + input);
		final String[] splitedInput = StringUtils.splitPreserveAllTokens(input, SEMICOLON_CHAR);
		final String[] currentSplitedInput = new String[5];
		final StringBuilder sb = new StringBuilder("");
		LOG.debug("Split input length : " + splitedInput.length);
		for (int i = 0; i <= splitedInput.length - 1; i++)
		{
			if (i > 3)
			{
				if (i == splitedInput.length - 1)
				{
					sb.append(splitedInput[i]);
				}
				else
				{
					sb.append(splitedInput[i]).append(";");
				}
			}
			if (i < 4)
			{
				currentSplitedInput[i] = splitedInput[i];
			}
		}
		LOG.debug("Current Split input length : " + currentSplitedInput.length);
		if (sb.length() > 0)
		{
			currentSplitedInput[4] = sb.insert(0, ",").toString();
		}
		final List<String> tmp = new ArrayList<String>();
		for (final String string : currentSplitedInput)
		{
			if (doesNotContainNewLine(string))
			{
				tmp.add(StringEscapeUtils.escapeCsv(string));
			}
			else
			{
				tmp.add(string);
			}
		}
		try
		{
			if (tmp.size() == 5 && tmp.get(4).length() >= 2)
			{
				tmp.set(4, "\"" + tmp.get(4).substring(2));
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while converting value line : " + e.getMessage() + " Cause : " + e.getCause());
		}

		LOG.debug("Final Output : " + StringUtils.join(tmp, SEMICOLON_CHAR));
		return StringUtils.join(tmp, SEMICOLON_CHAR);
	}
}
