/**
 * 
 */
package de.hybris.clicks.core.dataimport.batch.customer.impl;

import de.hybris.clicks.core.dataimport.batch.customer.CustomerImportAdapter;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * @author arshath
 * 
 */
public class CustomerAddressImportAdapter implements CustomerImportAdapter
{
	private static final Logger LOG = Logger.getLogger(CustomerAddressImportAdapter.class);
	private ModelService modelService;

	@Override
	public void performImport(final String cellValue, final Item address)
	{
		Assert.hasText(cellValue);
		Assert.notNull(address);

		try
		{
			final String[] values = cellValue.replace("%semi%", ";").split("\\^");
			final AddressModel addressModel = modelService.get(address);

			// Update address type based on type ID
			if (null != addressModel.getTypeID())
			{
				if (Integer.parseInt(addressModel.getTypeID()) == 1)
				{
					addressModel.setType("Postal Address");
				}
				else if (Integer.parseInt(addressModel.getTypeID()) == 2)
				{
					addressModel.setType("Physical Address");
				}
				else if (Integer.parseInt(addressModel.getTypeID()) == 3)
				{
					addressModel.setType("Delivery Address");
				}
			}

			if (values[0] != null && !values[0].isEmpty())
			{
				addressModel.setBuilding(values[0]);
			}

			if (values[1] != null && !values[1].isEmpty())
			{
				addressModel.setStreetname(values[1]);
			}

			if (values[2] != null && !values[2].isEmpty())
			{
				addressModel.setStreetnumber(values[2]);
			}

			// Update town with City & Province
			if (!values[3].isEmpty() && !values[4].isEmpty())
			{
				addressModel.setTown(values[3] + ", " + values[4]);
			}
			else if (!values[3].isEmpty())
			{
				addressModel.setTown(values[3]);
			}
			else
			{
				addressModel.setTown(values[4]);
			}

			modelService.save(addressModel);
		}
		catch (final RuntimeException e)
		{
			LOG.warn("Could not import customer " + address + ": " + e);
			throw e;
		}
		catch (final Exception e)
		{
			LOG.warn("Could not import customer " + address + ": " + e);
			throw new SystemException("Could not import customer " + address, e);
		}
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}
}
