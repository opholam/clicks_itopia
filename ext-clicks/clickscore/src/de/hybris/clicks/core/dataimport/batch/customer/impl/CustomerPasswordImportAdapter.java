/**
 * 
 */
package de.hybris.clicks.core.dataimport.batch.customer.impl;

import de.hybris.clicks.core.dataimport.batch.customer.CustomerImportAdapter;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * @author arshath
 * 
 */
public class CustomerPasswordImportAdapter implements CustomerImportAdapter
{
	private static final Logger LOG = Logger.getLogger(CustomerPasswordImportAdapter.class);
	private ModelService modelService;

	@Override
	public void performImport(final String cellValue, final Item customer)
	{
		Assert.hasText(cellValue);
		Assert.notNull(customer);

		try
		{
			final String[] values = cellValue.replace("%semi%", ";").split("\t");

			final CustomerModel customerModel = modelService.get(customer);

			customerModel.setEncodedPassword(values[0]);
			customerModel.setPasswordAnswer(values[1]);

			modelService.save(customerModel);
		}
		catch (final RuntimeException e)
		{
			LOG.warn("Could not import password for customer " + customer + ": " + e);
			throw e;
		}
		catch (final Exception e)
		{
			LOG.warn("Could not import password for " + customer + ": " + e);
			throw new SystemException("Could not import password for " + customer, e);
		}
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}
}
