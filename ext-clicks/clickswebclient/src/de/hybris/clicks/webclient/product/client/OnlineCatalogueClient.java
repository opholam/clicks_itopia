/**
 *
 */
package de.hybris.clicks.webclient.product.client;

import de.hybris.clicks.webclient.product.populator.OnlineHierarchyPublishPopulator;
import de.hybris.clicks.webclient.product.populator.OnlinePromotionPublishPopulator;
import de.hybris.clicks.webclient.product.populator.ProductInformationPublishPopulator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Config;

import java.net.URL;

import javax.annotation.Resource;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.springframework.util.ResourceUtils;

import catalog.https.esb_clicksgroup_co_za.omnichannel.catalogue.BizTalkServiceInstance;
import catalog.https.esb_clicksgroup_co_za.omnichannel.catalogue.Service;
import catalog.omni.biztalk.clicks.schema.onlinecatalogue.OnlineCatalogue;
import catalog.omni.biztalk.clicks.schema.promotion.PromotionMessage;


/**
 * @author abhaydh
 * 
 */
public class OnlineCatalogueClient
{
	protected static final Logger LOG = Logger.getLogger(OnlineCatalogueClient.class);

	OnlineCatalogue productInformation;
	OnlineCatalogue onlineHirarchy;
	PromotionMessage promotionMessage;
	BizTalkServiceInstance newStub;

	@Resource
	OnlineHierarchyPublishPopulator onlineHierarchyPublishPopulator;
	@Resource
	ProductInformationPublishPopulator productInformationPublishPopulator;
	@Resource
	private OnlinePromotionPublishPopulator onlinePromotionPublishPopulator;
	@Resource
	private ConfigurationService configurationService;

	public void productCatalogueClient(final String action)
	{


		BizTalkServiceInstance stub = null;
		Service service = null;
		String file = null;
		final String property = Config.getParameter("product.catalogue.client.action");
		final String[] actionList = property.split(",");
		try
		{
			final String wsdlPath = (String) configurationService.getConfiguration().getProperty("file.wsdl.path");
			file = ResourceUtils.getFile(wsdlPath).getCanonicalPath();
		}
		catch (final Exception e1)
		{
			LOG.error("Error in OnlineCatalogueClient class at productCatalogueClient() method : " + e1.getMessage());
		}

		try
		{
			URL url = null;
			try
			{
				url = new URL("file:" + file);
			}
			catch (final Exception e)
			{
				LOG.error("Error in OnlineCatalogueClient class at productCatalogueClient() method : " + e.getMessage());
			}

			stub = new BizTalkServiceInstance(url);
			service = stub.getBasicHttpBindingITwoWayAsyncVoid();

			((BindingProvider) service).getRequestContext().put(BindingProvider.USERNAME_PROPERTY,
					Config.getString("catalog.service.username", ""));
			((BindingProvider) service).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY,
					Config.getString("catalog.service.password", ""));

		}
		catch (final Exception e)
		{
			LOG.error("Error in OnlineCatalogueClient class at productCatalogueClient() method : " + e.getMessage());
		}


		if (action.equalsIgnoreCase(actionList[0]))
		{
			productInformation = new OnlineCatalogue();
			productInformation = productInformationPublishPopulator.populate(productInformation, "all");
		}
		if (action.equalsIgnoreCase(actionList[1]))
		{
			productInformation = new OnlineCatalogue();
			productInformation = productInformationPublishPopulator.populate(productInformation, "updated");
		}
		if (action.equalsIgnoreCase(actionList[2]))
		{
			onlineHirarchy = new OnlineCatalogue();
			onlineHirarchy = onlineHierarchyPublishPopulator.populate(onlineHirarchy);
		}
		if (action.equalsIgnoreCase("publishPromotion"))
		{
			promotionMessage = new PromotionMessage();
			onlinePromotionPublishPopulator.populate(promotionMessage);
		}

		//		try
		//		{
		//			final String parentPath = (String) configurationService.getConfiguration().getProperty("file.output.stream.drive");
		//			final String path = (String) configurationService.getConfiguration().getProperty("file.output.stream.file");
		//			final String[] pathList = path.split(",");
		//			final XStream xStream = new XStream();
		//			if (action.equalsIgnoreCase(actionList[2]))
		//			{
		//				final FileOutputStream fout = new FileOutputStream(parentPath + pathList[0]);
		//				final ObjectOutputStream oos = new ObjectOutputStream(fout);
		//				oos.writeObject(xStream.toXML(onlineHirarchy));
		//			}
		//			if (action.equalsIgnoreCase(actionList[0]))
		//			{
		//				final FileOutputStream fout2 = new FileOutputStream(parentPath + pathList[1]);
		//				final ObjectOutputStream oos2 = new ObjectOutputStream(fout2);
		//				oos2.writeObject(xStream.toXML(productInformation));
		//			}
		//			if (action.equalsIgnoreCase(actionList[1]))
		//			{
		//				final FileOutputStream fout2 = new FileOutputStream(parentPath + pathList[2]);
		//				final ObjectOutputStream oos2 = new ObjectOutputStream(fout2);
		//				oos2.writeObject(xStream.toXML(productInformation));
		//			}
		//		}
		//		catch (final IOException e1)
		//		{
		//			LOG.error("Error in OnlineCatalogueClient class at productCatalogueClient() method : " + e1.getMessage());
		//		}


		try
		{
			if (action.equalsIgnoreCase(actionList[0]))
			{
				service.publishOnlineCatalogue(productInformation);
			}
			if (action.equalsIgnoreCase(actionList[2]))
			{
				service.publishOnlineCatalogue(onlineHirarchy);
			}
			if (action.equalsIgnoreCase("publishPromotion"))
			{
				service.publishOnlinePromotion(promotionMessage);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in OnlineCatalogueClient class at productCatalogueClient() method : " + e.getMessage());
		}
	}

}
