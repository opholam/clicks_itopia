/**
 *
 */
package de.hybris.clicks.webclient.product.populator;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import catalog.omni.biztalk.clicks.schema.onlinecatalogue.OnlineCatalogue;






/**
 * @author abhaydh
 * 
 */
public class OnlineHierarchyPublishPopulator
{

	protected static final Logger LOG = Logger.getLogger(OnlineHierarchyPublishPopulator.class);

	public final String RETAILERID = "1";
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource
	private CategoryService categoryService;

	@Resource
	public PersistentKeyGenerator myNumberseriesClient;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	String newID = new String();


	public OnlineCatalogue populate(final OnlineCatalogue onlineHirarchy)
	{
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter("clicks.productcatalog.name"), Config.getParameter("clicks.catalog.online.version"));

		//--- SET Header information---
		userService.setCurrentUser(userService.getAdminUser());
		final OnlineCatalogue.Header header = new OnlineCatalogue.Header();


		newID = (String) myNumberseriesClient.generate();
		final GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		XMLGregorianCalendar date = null;
		try
		{
			date = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		}
		catch (final DatatypeConfigurationException e1)
		{
			LOG.error("Error in OnlineCatalogueClient class at productCatalogueClient() method : " + e1.getMessage());
		}

		header.setCreatedDateTime(date);
		header.setDataSource(Config.getParameter("text.data.source"));
		header.setEventCode(Config.getParameter("text.event.code"));
		//header.setEventLogId(newID);

		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		onlineHirarchy.setHeader(header);
		//--- end Header information---

		final OnlineCatalogue.Body body = new OnlineCatalogue.Body();

		CategoryModel categoryModel;


		final List<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy> items = new ArrayList<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy>();
		final OnlineCatalogue.Body.GeneralTables.OnlineHierarchy OnlineHierarchy = new OnlineCatalogue.Body.GeneralTables.OnlineHierarchy();
		final OnlineCatalogue.Body.GeneralTables generalTables = new OnlineCatalogue.Body.GeneralTables();

		Collection<CategoryModel> categoryList = null;
		List<CategoryModel> supercategoryList = null;
		//final int maxLevel = 0;
		final String property = Config.getParameter("category.fetch.code");
		final String[] categories = property.split(",");
		categoryModel = categoryService.getCategoryForCode(catalogVersionModel, categories[1]);
		categoryList = categoryService.getAllSubcategoriesForCategory(categoryModel);



		OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy itemHierarchy;
		final Iterator<CategoryModel> categoryIterator = categoryList.iterator();


		while (categoryIterator.hasNext())
		{
			categoryModel = categoryIterator.next();
			//			boolean isFromDepartments = false;
			//			Collection<CategoryModel> allSuperCategoryList = categoryService.getAllSupercategoriesForCategory(categoryModel);
			//			if(allSuperCategoryList.size() > 0)
			//			{
			//				for(CategoryModel superCat : allSuperCategoryList)
			//				{
			//					if(superCat.getCode().equalsIgnoreCase("Departments"))
			//					{
			//						isFromDepartments=true;
			//					}
			//				}
			//			}
			if (categoryModel.getCode().startsWith(categories[0]))
			{
				supercategoryList = null;

				itemHierarchy = new OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy();
				itemHierarchy.setCategoryId(categoryModel.getCode());
				if (null != categoryModel.getName() && categoryModel.getName().trim().length() > 0)
				{
					itemHierarchy.setCategoryDescription(categoryModel.getName().trim());
				}
				else
				{
					itemHierarchy.setCategoryDescription(Config.getParameter("category.empty.desc"));
				}
				if (categoryModel.getSupercategories().size() > 0)
				{
					supercategoryList = categoryModel.getSupercategories();
				}
				if (null != supercategoryList && supercategoryList.size() > 0)
				{
					final Iterator<CategoryModel> superCategoryIterator = supercategoryList.iterator();
					CategoryModel superCategoryModel = new CategoryModel();
					superCategoryModel = superCategoryIterator.next();
					itemHierarchy.setParentCategoryId(superCategoryModel.getCode());
				}
				else
				{
					itemHierarchy.setParentCategoryId(categories[2]);
				}
				items.add(itemHierarchy);
			}
		}


		OnlineHierarchy.getItemHierarchy().addAll(items);
		//OnlineHierarchy.setMaxLevels(BigInteger.valueOf(maxLevel));
		generalTables.getOnlineHierarchy().add(OnlineHierarchy);

		body.setGeneralTables(generalTables);
		//body.setCreationDate(date);
		body.setRetailerId(RETAILERID);

		onlineHirarchy.setBody(body);

		return onlineHirarchy;
	}
}
