/**
 *
 */
package de.hybris.clicks.webclient.product.populator;

import de.hybris.clicks.core.model.ClicksApparelSizeVariantModel;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import catalog.omni.biztalk.clicks.schema.onlinecatalogue.OnlineCatalogue;
import catalog.omni.biztalk.clicks.schema.onlinecatalogue.OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink;



/**
 * @author abhaydh
 *
 */
public class ProductInformationPublishPopulator
{

	private static final Logger LOG = Logger.getLogger(ProductInformationPublishPopulator.class);
	public static final String SITE_UID = "clicks";

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	public final String RETAILERID = "1";
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource
	private ProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource(name = "siteBaseUrlResolutionService")
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "myNumberseriesClient")
	public PersistentKeyGenerator myNumberseriesClient;

	@Resource
	private BaseSiteService baseSiteService;

	//	protected BaseSiteService getBaseSiteService()
	//	{
	//		return baseSiteService;
	//	}
	//
	//	@Required
	//	public void setBaseSiteService(final BaseSiteService baseSiteService)
	//	{
	//		this.baseSiteService = baseSiteService;
	//	}


	String newID;




	public OnlineCatalogue populate(final OnlineCatalogue productInformation, final String action)
	{

		//--- SET Header information---
		userService.setCurrentUser(userService.getAdminUser());

		String siteURL = "";
		if (Config.getBoolean("product.publish.url.absolute", false))
		{
			try
			{
				BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();

				final String siteQuery = "SELECT {pk} FROM {BaseSite} WHERE {uid}='" + SITE_UID + "'";
				final SearchResult<BaseSiteModel> baseSiteResult = flexibleSearchService.search(siteQuery);

				if (baseSiteResult.getCount() > 0)
				{
					for (final BaseSiteModel baseSiteModel : baseSiteResult.getResult())
					{
						currentBaseSite = baseSiteModel;
						break;
					}
				}
				siteURL = siteBaseUrlResolutionService.getWebsiteUrlForSite(currentBaseSite, true, "");
			}
			catch (final Exception e)
			{
				//
			}
		}
		final OnlineCatalogue.Header header = new OnlineCatalogue.Header();


		newID = (String) myNumberseriesClient.generate();
		final XMLGregorianCalendar date = getXmlGregCal(new Date());


		header.setCreatedDateTime(date);
		header.setDataSource("Hybris");
		header.setEventCode("OMNIPROD");
		//header.setEventLogId(newID);
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		productInformation.setHeader(header);
		//--- end Header information---

		String query = null;
		final OnlineCatalogue.Body body = new OnlineCatalogue.Body();
		final Map<String, Object> params = new HashMap<String, Object>();
		if (action.equalsIgnoreCase("all"))
		{
			query = "SELECT {pk} FROM {ClicksApparelSizeVariant}";
		}
		else
		{
			query = "SELECT {pk} FROM {ClicksApparelSizeVariant} WHERE {catalogVersion} = ?catalogVersion and {modifiedtime} = ?modifiedTime";
			params.put("modifiedTime", new Date());
		}

		//params.put("catalogVersion", catalogVersionModel);
		final SearchResult<ClicksApparelSizeVariantModel> result = flexibleSearchService.search(query, params);
		String catalogVersion = "";
		CatalogVersionModel catalogVersionModel = null;

		final List<OnlineCatalogue.Body.Items.Item> items = new ArrayList<OnlineCatalogue.Body.Items.Item>();
		final OnlineCatalogue.Body.Items itemList = new OnlineCatalogue.Body.Items();
		if (CollectionUtils.isNotEmpty(result.getResult()))
		{

			OnlineCatalogue.Body.Items.Item item;

			for (final ClicksApparelSizeVariantModel productModel : result.getResult())
			{
				try
				{
					item = new OnlineCatalogue.Body.Items.Item();
					//	item.setAction("I");
					item.setSKU(productModel.getCode());
					if (null != productModel.getDescription())
					{
						item.setOnlineDescription(productModel.getDescription());
					}

					List<CategoryModel> superCategories = new ArrayList<CategoryModel>();
					superCategories = (List<CategoryModel>) productModel.getSupercategories();
					final OnlineCatalogue.Body.Items.Item.OnlineHierarchy onlineHierarchy = new OnlineCatalogue.Body.Items.Item.OnlineHierarchy();
					final List<OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink> hierarchyLinks = new ArrayList<OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink>();
					if (superCategories.size() > 0)
					{
						final Iterator<CategoryModel> categoryIterator = superCategories.iterator();
						while (categoryIterator.hasNext())
						{
							boolean alradyInList = false;
							final CategoryModel category = categoryIterator.next();
							for (final HierarchyLink existingLink : hierarchyLinks)
							{
								if (category.getCode().equalsIgnoreCase(existingLink.getCategoryId()))
								{
									alradyInList = true;
								}
							}
							final String categories = Config.getParameter("category.fetch.code");
							final String[] categoryList = categories.split(",");
							if (!alradyInList && category.getCode().startsWith(categoryList[0]))
							{
								final OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink link = new OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink();
								link.setCategoryId(category.getCode());
								hierarchyLinks.add(link);
							}
						}
						if (hierarchyLinks.size() > 0)
						{
							LOG.debug("product hierarchyLinks list size " + hierarchyLinks.size() + " for SKU " + productModel.getCode());
							onlineHierarchy.getHierarchyLink().addAll(hierarchyLinks);
						}
						if (!onlineHierarchy.getHierarchyLink().isEmpty())
						{
							item.setOnlineHierarchy(onlineHierarchy);
						}
					}

					if (null != productModel.getBaseProduct())
					{
						//final ProductModel baseProduct = productModel.getBaseProduct();
						List<MediaContainerModel> mediaContainerList = new ArrayList<MediaContainerModel>();
						//mediaContainerList = baseProduct.getGalleryImages();
						mediaContainerList = productModel.getGalleryImages();
						if (mediaContainerList.size() > 0)
						{
							final OnlineCatalogue.Body.Items.Item.ProductImages imageList = new OnlineCatalogue.Body.Items.Item.ProductImages();
							for (final MediaContainerModel mediaContainerModel : mediaContainerList)
							{
								List<MediaModel> mediaModelList = new ArrayList<MediaModel>();
								if (null != mediaContainerModel.getMedias())
								{
									mediaModelList = (List<MediaModel>) mediaContainerModel.getMedias();
									if (mediaModelList.size() > 0)
									{
										for (final MediaModel mediaModel : mediaModelList)
										{
											final OnlineCatalogue.Body.Items.Item.ProductImages.Image image = new OnlineCatalogue.Body.Items.Item.ProductImages.Image();
											if (null != mediaModel.getURL())
											{
												image.setMainImage(StringUtils.isNotBlank(mediaModel.getCode())
														&& null != productModel.getPicture()
														&& mediaModel.getCode().equals(productModel.getPicture().getCode()) ? Boolean.TRUE
														.booleanValue() : Boolean.FALSE.booleanValue());
												image.setUrl(mediaModel.getURL());
											}
											//set type
											try
											{
												final String property = Config.getParameter("container.media.value");
												final String[] media = property.split(",");
												if (mediaContainerModel.getQualifier().contains(media[0]))
												{
													image.setType(new Integer(1));
												}
												if (mediaContainerModel.getQualifier().contains(media[2]))
												{
													image.setType(new Integer(3));
												}
												if (mediaContainerModel.getQualifier().contains(media[1]))
												{
													image.setType(new Integer(2));
												}
												//set type end
												//set size
												image.setSize(new Integer(1));
												final String mediaFormat = Config.getParameter("container.media.format");
												final String[] mediaFormatList = mediaFormat.split(",");
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[0]))
												{
													image.setSize(new Integer(4));
												}
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[1]))
												{
													image.setSize(new Integer(2));
												}
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[2]))
												{
													image.setSize(new Integer(3));
												}
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[3]))
												{
													image.setSize(new Integer(2));
												}
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[4]))
												{
													image.setSize(new Integer(3));
												}
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[5]))
												{
													image.setSize(new Integer(1));
												}
												if (mediaModel.getMediaFormat().getQualifier().contains(mediaFormatList[6]))
												{
													image.setSize(new Integer(5));
												}

											}
											catch (final Exception e)
											{
												e.printStackTrace();
											}
											//set size end
											imageList.getImage().add(image);

										}
									}
								}

							}
							if (imageList.getImage().size() > 0)
							{
								LOG.debug("product image list size " + imageList.getImage().size() + " for SKU " + productModel.getCode());
								item.setProductImages(imageList);
							}
						}
					}


					// start - add media section -productPage URL

					try
					{
						//final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

						final OnlineCatalogue.Body.Items.Item.Media media = new OnlineCatalogue.Body.Items.Item.Media();
						final List<OnlineCatalogue.Body.Items.Item.Media.MediaItem> mediaItemList = new ArrayList<OnlineCatalogue.Body.Items.Item.Media.MediaItem>();

						final OnlineCatalogue.Body.Items.Item.Media.MediaItem mediaItem = new OnlineCatalogue.Body.Items.Item.Media.MediaItem();

						mediaItem.setType(1);
						mediaItem.setValue(siteURL + "/p/" + productModel.getCode());

						mediaItemList.add(mediaItem);
						media.getMediaItem().addAll(mediaItemList);
						item.setMedia(media);
					}
					catch (final Exception e)
					{
						e.printStackTrace();
						break;
					}
					// end - add media section -productPage URL
					catalogVersionModel = productModel.getCatalogVersion();
					catalogVersion = null != catalogVersionModel && null != catalogVersionModel.getCatalog() ? (catalogVersionModel
							.getCatalog().getId() + " " + catalogVersionModel.getVersion()) : "";
					item.setCatalogueVersion(catalogVersion);
					item.setDisplayOnline(null != productModel.getDisplayOnline() ? productModel.getDisplayOnline().booleanValue()
							: Boolean.FALSE.booleanValue());
					item.setHasCopywrite(StringUtils.isNotEmpty(productModel.getCopyWriteDescription()) ? Boolean.TRUE.booleanValue()
							: Boolean.FALSE.booleanValue());
					item.setHasImage(CollectionUtils.isNotEmpty(productModel.getGalleryImages()) && null != productModel.getPicture()
							&& null != productModel.getPicture().getCode() ? Boolean.TRUE.booleanValue() : Boolean.FALSE.booleanValue());
					item.setStatus(null != productModel.getApprovalStatus() ? productModel.getApprovalStatus().getCode() : "");
					//item.setForOnline(null != productModel.getOnlineOnly() ? productModel.getOnlineOnly().toString() : "");
					item.setLastUpdatedDate(getXmlGregCal(productModel.getModifiedtime()));
					item.setCreationDate(getXmlGregCal(productModel.getCreationtime()));
					items.add(item);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
			}


		}

		itemList.getItem().addAll(items);
		body.setItems(itemList);
		//body.setCreationDate(date);
		body.setRetailerId(RETAILERID);
		productInformation.setBody(body);


		return productInformation;

	}



	/**
	 * @return
	 */
	private XMLGregorianCalendar getXmlGregCal(final Date date)
	{
		XMLGregorianCalendar xmlDate = null;
		try
		{
			final GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		}
		catch (final Exception e1)
		{
			e1.printStackTrace();
		}
		return xmlDate;
	}
}
