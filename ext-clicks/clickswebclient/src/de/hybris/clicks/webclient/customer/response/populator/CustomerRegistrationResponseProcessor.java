/**
 *
 */
package de.hybris.clicks.webclient.customer.response.populator;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import Omni.BizTalk.Clicks.Schema.Customer.Customer;
import Omni.BizTalk.Clicks.Schema.Customer.Response;


/**
 * @author abhaydh
 *
 */

public class CustomerRegistrationResponseProcessor
{
	protected static final Logger LOG = Logger.getLogger(CustomerRegistrationResponseProcessor.class);
	public static final String NAMESPACE = "http://Clicks.BizTalk.Omni/Schema/Customer";
	public static final String GET_PERSONS_REQUEST = "PublishCustomer";

	@Resource
	CustomerLoyaltyDetailsPopulator customerLoyaltyDetailsresponsePopulator;
	@Resource
	CustomerHeaderPopulator customerHeaderresponsePopulator;
	@Resource
	CustomerCoreDetailsPopulator customerCoreDetailsresponsePopulator;
	@Resource
	CustomerNonLoyaltyDetailsPopulator customerNonLoyaltyDetailsresponsePopulator;

	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private FlexibleSearchService flexibleSearchService;



	public void processCustomerResponse(final Response publishCustomer)
	{
		if (null != publishCustomer.getHeader())
		{
			customerHeaderresponsePopulator.populate(publishCustomer.getHeader());
		}

		// for customer section
		if (null != publishCustomer.getBody().getCustomer())
		{
			final Customer customer = publishCustomer.getBody().getCustomer();

			if (null != customer.getCoreDetails())
			{
				if (!publishCustomer.getHeader().getEventCode().equals("BICUSTPUB"))
				{
					CustomerModel customerModel = modelService.create(CustomerModel.class);
					if (null != customer.getCoreDetails().getEMailAddress())
					{
						try
						{
							if (null != customer.getLoyalty() && null != customer.getLoyalty().getMemberId())
							{
								SearchResult<CustomerModel> result = null;
								final String query = "SELECT {PK} FROM {Customer} WHERE {memberID}='"
										+ customer.getLoyalty().getMemberId() + "'";
								result = flexibleSearchService.search(query);
								if (null != result && result.getCount() > 0 && CollectionUtils.isNotEmpty(result.getResult()))
								{
									for (final CustomerModel cust : result.getResult())
									{
										customerModel = cust;
									}
								}
								else
								{
									LOG.info("---Can not find customer with MemberID --->" + customer.getLoyalty().getMemberId());
									customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails().getEMailAddress());
									if (StringUtils.isBlank(customerModel.getMemberID()))
									{
										customerModel.setMemberID(null);
									}

								}
							}
							else
							{
								customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails().getEMailAddress());
							}
						}
						catch (final Exception e)
						{
							customerModel.setUid(customer.getCoreDetails().getEMailAddress());
							LOG.error(e);
						}
					}
					if (null != customer.getHybris())
					{
						if (null != customer.getHybris().getRetailerId())
						{
							customerModel.setRetailerID_hybris(customer.getHybris().getRetailerId());
						}
						if (null != customer.getHybris().getCustomerId())
						{
							customerModel.setCustomerID(customer.getHybris().getCustomerId());
						}
					}
					if (null != customer.getPharmacy() && null != customer.getPharmacy().getMarconiId())
					{
						customerModel.setMarconiId(customer.getPharmacy().getMarconiId());
					}
					if (null != customer.getCoreDetails())
					{
						customerModel = customerCoreDetailsresponsePopulator.populate(customer.getCoreDetails(), customerModel);
					}
					if (null != customer.getLoyalty())
					{
						customerModel = customerLoyaltyDetailsresponsePopulator.populate(customer.getLoyalty(), customerModel);
					}
					if (null != customer.getNonLoyalty())
					{
						customerModel = customerNonLoyaltyDetailsresponsePopulator.populate(customer.getNonLoyalty(), customerModel);
					}
					try
					{
						modelService.save(customerModel);
						modelService.refresh(customerModel);
					}
					catch (final ModelSavingException saveexp)
					{
						LOG.error("model saving exception" + saveexp);
					}
				}
				else
				{
					if (null != customer.getCoreDetails().getEMailAddress())
					{
						try
						{
							CustomerModel customerModel = modelService.create(CustomerModel.class);
							if (null != customer.getLoyalty() && null != customer.getLoyalty().getMemberId())
							{
								SearchResult<CustomerModel> result = null;
								final String query = "SELECT {PK} FROM {Customer} WHERE {memberID}='"
										+ customer.getLoyalty().getMemberId() + "'";
								result = flexibleSearchService.search(query);
								if (null != result && result.getCount() > 0)
								{
									for (final CustomerModel cust : result.getResult())
									{
										customerModel = cust;
									}
								}
								else
								{
									LOG.info("---Can not find customer with MemberID --->" + customer.getLoyalty().getMemberId());
									customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails().getEMailAddress());
								}
							}
							else
							{
								customerModel = (CustomerModel) userService.getUserForUID(customer.getCoreDetails().getEMailAddress());
							}
							if (null != customer.getNonLoyalty())
							{
								customerModel = customerNonLoyaltyDetailsresponsePopulator.populate(customer.getNonLoyalty(),
										customerModel);
							}
							modelService.save(customerModel);
						}
						catch (final Exception e)
						{
							LOG.error("Error in CustomerRegistrationResponseProcessor class at processCustomerResponse() method : "
									+ e.getMessage());
						}
					}

				}
			}
			else
			{
				if (null != customer.getLoyalty() && (null != customer.getLoyalty().getMemberId()))
				{
					try
					{
						CustomerModel customerModel = null;
						SearchResult<CustomerModel> result = null;
						final String query = "SELECT {PK} FROM {Customer} WHERE {memberID}='" + customer.getLoyalty().getMemberId()
								+ "'";
						result = flexibleSearchService.search(query);
						if (null != result && result.getCount() > 0)
						{
							for (final CustomerModel cust : result.getResult())
							{
								customerModel = cust;
							}
						}
						else
						{
							LOG.info("---Can not find customer with MemberID --->" + customer.getLoyalty().getMemberId());
						}
						if (null != customerModel)
						{
							customerModel = customerLoyaltyDetailsresponsePopulator.populate(customer.getLoyalty(), customerModel);
							modelService.save(customerModel);
						}
					}
					catch (final Exception e)
					{
						LOG.error("Error in CustomerRegistrationResponseProcessor class at processCustomerResponse() method : "
								+ e.getMessage());
					}

				}
			}

		}

	}
}
