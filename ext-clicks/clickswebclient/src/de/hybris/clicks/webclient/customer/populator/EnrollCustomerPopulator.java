/**
 *
 */
package de.hybris.clicks.webclient.customer.populator;

import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ChildData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;

import Omni.BizTalk.Clicks.Schema.Customer.Customer;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerAction;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsentCommsType;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.RequestBody;
import Omni.BizTalk.Clicks.Schema.Customer.RequestHeader;



/**
 * @author abhaydh
 *
 */
public class EnrollCustomerPopulator
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;
	/**
	 * @param enrollRequest
	 * @param action
	 * @param customer2
	 * @return
	 */

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	public Request populate(final Request enrollRequest, final CustomerData customerData, final String action)
	{

		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		final CustomerCoreDetails coreDetails = new CustomerCoreDetails();
		final CustomerHybris customerHybris = new CustomerHybris();

		final CustomerCoreDetailsAddressesAddress addresses = new CustomerCoreDetailsAddressesAddress();
		final CustomerLoyalty customerLoyalty = new CustomerLoyalty();

		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);
		header.setCreatedDateTime(cal);
		if (customerData.isSourceMobileApp())
		{
			header.setDataSource("Mobile");
		}
		else
		{
			header.setDataSource("Hybris");
		}
		if (action.equalsIgnoreCase("enroll"))
		{
			header.setEventCode("OMNIENR");
		}
		if (action.equalsIgnoreCase("update"))
		{
			header.setEventCode("OMNIUPD");
		}
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);
		//header.setEventLogId("100169475");

		enrollRequest.setHeader(header);

		if (null != customerData.getSAResident() && customerData.getSAResident().booleanValue())
		{
			coreDetails.setSAResident(customerData.getSAResident().booleanValue());
		}
		else
		{
			coreDetails.setSAResident(false);
		}
		if (null != customerData.getIDnumber())
		{
			coreDetails.setIDNumber(customerData.getIDnumber());
		}
		coreDetails.setFirstName(customerData.getFirstName());
		coreDetails.setLastName(customerData.getLastName());
		coreDetails.setEMailAddress(customerData.getEmailID());
		coreDetails.setPreferedName(customerData.getPreferedName());
		if (null != customerData.getNonRSA_DOB())
		{
			coreDetails.setDateOfBirth(customerData.getNonRSA_DOB());
		}
		if (null != customerData.getGender())
		{
			if (customerData.getGender().equals("1"))
			{
				coreDetails.setGender(CustomerCoreDetailsGender.value1);
			}
			if (customerData.getGender().equals("2"))
			{
				coreDetails.setGender(CustomerCoreDetailsGender.value2);
			}
		}
		else
		{
			coreDetails.setGender(CustomerCoreDetailsGender.value1);
		}
		if (CollectionUtils.isNotEmpty(customerData.getAddresses()))
		{
			// array size for address here should be exactly same as number of address. extra size will cause error in esb webservice
			final CustomerCoreDetailsAddressesAddress[] addressesArray = new CustomerCoreDetailsAddressesAddress[1];
			for (final AddressData address : customerData.getAddresses())
			{
				if (null != address.getCity() && !address.getCity().contains(","))
				{
					if (null != address.getCity() && StringUtils.isNotEmpty(address.getCity()))
					{
						addresses.setCity(address.getCity().trim());
					}
					if (null != address.getProvince() && StringUtils.isNotEmpty(address.getProvince()))
					{

						addresses.setProvince(getProvinceCode(address.getProvince().trim()));
					}
				}
				else
				{
					String city;
					String province;
					if (null != address.getCity())
					{
						city = address.getCity();
					}
					if (null != address.getProvince())
					{
						province = address.getProvince();
					}
					if (null != address.getCity())
					{
						final String[] elements = address.getCity().split(",");
						city = elements[0];
						try
						{
							province = elements[1];

							if (null != city && StringUtils.isNotEmpty(city))
							{
								addresses.setCity(city.trim());
							}
							if (null != province && StringUtils.isNotEmpty(province))
							{
								addresses.setProvince(getProvinceCode(province.trim()));
							}
						}
						catch (final ArrayIndexOutOfBoundsException exc)
						{

						}
					}
				}

				if (null != address.getLine1() && StringUtils.isNotEmpty(address.getLine1()))
				{
					addresses.setAddressLine1(address.getLine1());
				}
				if (null != address.getLine2() && StringUtils.isNotEmpty(address.getLine2()))
				{
					addresses.setAddressLine2(address.getLine2());
				}
				addresses.setAddressTypeId(CustomerCoreDetailsAddressesAddressAddressTypeId.value1);
				if (null != address.getCountry() && StringUtils.isNotEmpty(address.getCountry().getIsocode())
						&& null != address.getCountry().getIsocode())
				{
					addresses.setCountry(address.getCountry().getIsocode());
				}
				if (null != address.getPostalCode() && StringUtils.isNotEmpty(address.getPostalCode()))
				{
					addresses.setPostalCode(address.getPostalCode());
				}
				if (null != address.getSuburb() && StringUtils.isNotEmpty(address.getSuburb()))
				{
					addresses.setSuburb(address.getSuburb());
				}
			}
			addressesArray[0] = addresses;
			coreDetails.setAddresses(addressesArray);
		}

		if (null != customerData.getContactDetails())
		{
			// array size for contacts here should be exactly same as number of contacts. extra size will cause error in esb webservice
			int contactSize = customerData.getContactDetails().size();
			try
			{
				contactSize = CollectionUtils.countMatches(customerData.getContactDetails(), new Predicate()
				{
					@Override
					public boolean evaluate(final Object arg0)
					{
						if (arg0 instanceof ContactDetail)
						{
							return StringUtils.isNotEmpty(((ContactDetail) arg0).getNumber())
									&& StringUtils.isNotEmpty(((ContactDetail) arg0).getTypeID());
						}
						return false;
					}
				});
			}
			catch (final Exception e)
			{
				//
			}
			int count = 0;
			final CustomerCoreDetailsContactDetailsContactDetail[] contactsArray = new CustomerCoreDetailsContactDetailsContactDetail[contactSize];

			for (final ContactDetail contact : customerData.getContactDetails())
			{
				if (StringUtils.isNotEmpty(contact.getNumber()) && StringUtils.isNotEmpty(contact.getTypeID()))
				{
					final CustomerCoreDetailsContactDetailsContactDetail contact1 = new CustomerCoreDetailsContactDetailsContactDetail();
					if (null != contact.getTypeID())
					{
						contact1.setTypeId(contact.getTypeID());
					}
					if (null != contact.getNumber())
					{
						contact1.setNumber(contact.getNumber());
					}
					contactsArray[count] = contact1;
					count++;
				}
			}
			coreDetails.setContactDetails(contactsArray);
		}



		customer.setAction(CustomerAction.I);
		customer.setCoreDetails(coreDetails);

		customerHybris.setRetailerId("1");
		customerHybris.setCustomerId(customerData.getCustomerID());
		customer.setHybris(customerHybris);
		if (StringUtils.isNotEmpty(customerData.getMemberID()) || CollectionUtils.isNotEmpty(customerData.getAddresses()))
		{
			final CustomerLoyaltyOptIns optIns = new CustomerLoyaltyOptIns();

			// set marketing consent here from customerData
			final CustomerLoyaltyOptInsMarketingConsentConsent[] marketingConsentArray = new CustomerLoyaltyOptInsMarketingConsentConsent[2];

			CustomerLoyaltyOptInsMarketingConsentConsent consent = new CustomerLoyaltyOptInsMarketingConsentConsent();
			CustomerLoyaltyOptInsMarketingConsentConsentCommsType[] commsArray = new CustomerLoyaltyOptInsMarketingConsentConsentCommsType[2];

			CustomerLoyaltyOptInsMarketingConsentConsentCommsType type = new CustomerLoyaltyOptInsMarketingConsentConsentCommsType();
			consent.setConsentType("ACCINFO");
			if (null != customerData.getAccinfo_SMS())
			{
				type.setAccepted(customerData.getAccinfo_SMS().booleanValue());
			}
			else
			{
				type.setAccepted(false);
			}
			type.setTypeId(1);
			commsArray[0] = type;
			type = new CustomerLoyaltyOptInsMarketingConsentConsentCommsType();
			if (null != customerData.getAccinfo_email())
			{
				type.setAccepted(customerData.getAccinfo_email().booleanValue());
			}
			else
			{
				type.setAccepted(false);
			}
			type.setTypeId(2);
			commsArray[1] = type;
			consent.setComms(commsArray);
			marketingConsentArray[0] = consent;


			consent = new CustomerLoyaltyOptInsMarketingConsentConsent();
			commsArray = new CustomerLoyaltyOptInsMarketingConsentConsentCommsType[2];

			type = new CustomerLoyaltyOptInsMarketingConsentConsentCommsType();
			consent.setConsentType("MARKETING");
			if (null != customerData.getMarketingConsent_SMS())
			{
				type.setAccepted(customerData.getMarketingConsent_SMS().booleanValue());
			}
			else
			{
				type.setAccepted(false);
			}
			type.setTypeId(1);
			commsArray[0] = type;
			type = new CustomerLoyaltyOptInsMarketingConsentConsentCommsType();
			if (null != customerData.getMarketingConsent_email())
			{
				type.setAccepted(customerData.getMarketingConsent_email().booleanValue());
			}
			else
			{
				type.setAccepted(false);
			}
			type.setTypeId(2);
			commsArray[1] = type;
			consent.setComms(commsArray);
			marketingConsentArray[1] = consent;

			optIns.setMarketingConsent(marketingConsentArray);

			// set baby details
			if (null != customerData.getBabyClub())
			{
				final Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub babyClub = new Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub();

				final Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant alreadyPregnant = new Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant();
				final Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent alreadyParent = new Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent();

				if (null != customerData.getBabyClub().getGender())
				{
					if (customerData.getBabyClub().getGender().equalsIgnoreCase("boy"))
					{
						alreadyPregnant.setGender(CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender.value1);
					}
					else if (customerData.getBabyClub().getGender().equalsIgnoreCase("girl"))
					{
						alreadyPregnant.setGender(CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender.value2);
					}
					else
					{
						alreadyPregnant.setGender(CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender.value3);
					}

				}

				if (StringUtils.isNotBlank(customerData.getBabyClub().getBabyClubDueDate()))
				{
					if (StringUtils.containsIgnoreCase(customerData.getBabyClub().getBabyClubDueDate(), "/"))
					{
						Date date;
						try
						{
							date = new SimpleDateFormat("mm/dd/yyyy").parse(customerData.getBabyClub().getBabyClubDueDate());
							alreadyPregnant.setDueDate(new SimpleDateFormat("yyyy-mm-dd").format(date));
						}
						catch (final Exception e)
						{
							//
						}
					}
					else
					{
						alreadyPregnant.setDueDate(customerData.getBabyClub().getBabyClubDueDate());
					}
				}

				if (CollectionUtils.isNotEmpty(customerData.getBabyClub().getChildren()))
				{
					final int childrenNumber = customerData.getBabyClub().getChildren().size();

					if (childrenNumber > 0)
					{
						final Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild[] children = new CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild[childrenNumber];
						int count = 0;
						for (final ChildData childData : customerData.getBabyClub().getChildren())
						{
							final Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild child = new CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild();

							if (null != childData.getChildDOB())
							{
								child.setDateOfBirth(childData.getChildDOB());
							}
							if (null != childData.getFirstName())
							{
								child.setFirstName(childData.getFirstName());
							}
							if (null != childData.getGender())
							{
								if (childData.getGender().equalsIgnoreCase("1") || childData.getGender().equalsIgnoreCase("boy")
										|| childData.getGender().equalsIgnoreCase("male"))
								{
									child.setGender(CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender.value1);
								}
								else if (childData.getGender().equalsIgnoreCase("2") || childData.getGender().equalsIgnoreCase("girl")
										|| childData.getGender().equalsIgnoreCase("female"))
								{
									child.setGender(CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender.value2);
								}
								else
								{
									child.setGender(CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender.value3);
								}
							}
							if (null != childData.getLastName())
							{
								child.setSurname(childData.getLastName());
							}

							children[count] = child;
							count++;
						}

						alreadyParent.setChildren(children);
					}
				}


				if (null != customerData.getBabyClub().getBabyClubDueDate())
				{
					babyClub.setAlreadyPregnant(alreadyPregnant);
				}
				else
				{
					babyClub.setAlreadyParent(alreadyParent);
				}

				optIns.setBabyClub(babyClub);
			}
			//end - set baby details
			customerLoyalty.setOptIns(optIns);

			if (null != customerData.getMemberID())
			{
				customerLoyalty.setMemberId(customerData.getMemberID());
			}
			if (action.equalsIgnoreCase("update"))
			{
				if (null != customerData.getRetailerID_Loyalty() && StringUtils.isNotBlank(customerData.getRetailerID_Loyalty()))
				{
					customerLoyalty.setRetailerId(customerData.getRetailerID_Loyalty());
				}
				else
				{
					customerLoyalty.setRetailerId("1");
				}
				customerLoyalty.setMemberId(customerData.getMemberID());
			}


			if (Config.getBoolean("customer.Service.add.loyalty", true))
			{
				customer.setLoyalty(customerLoyalty);
			}
		}
		else
		{
			final CustomerNonLoyalty nonLoyalty = new CustomerNonLoyalty();
			final CustomerNonLoyaltyOptIns optIns = new CustomerNonLoyaltyOptIns();
			final CustomerNonLoyaltyOptInsMarketingConsentConsent[] marketingConsentArray = new CustomerNonLoyaltyOptInsMarketingConsentConsent[1];

			final CustomerNonLoyaltyOptInsMarketingConsentConsent consent = new CustomerNonLoyaltyOptInsMarketingConsentConsent();
			final CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[] commsArray = new CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[2];

			CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType type = new CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType();
			consent.setConsentType("MARKETING");
			if (null != customerData.getMarketingConsent_SMS())
			{
				type.setAccepted(customerData.getMarketingConsent_SMS().booleanValue());
			}
			else
			{
				type.setAccepted(false);
			}
			type.setTypeId(1);
			commsArray[0] = type;
			type = new CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType();
			if (null != customerData.getMarketingConsent_email())
			{
				type.setAccepted(customerData.getMarketingConsent_email().booleanValue());
			}
			else
			{
				type.setAccepted(false);
			}
			type.setTypeId(2);
			commsArray[1] = type;
			consent.setComms(commsArray);
			marketingConsentArray[0] = consent;

			optIns.setMarketingConsent(marketingConsentArray);
			nonLoyalty.setOptIns(optIns);
			if (null != customerData.getRetailerID_Loyalty() && StringUtils.isNotBlank(customerData.getRetailerID_Loyalty()))
			{
				nonLoyalty.setRetailerId(customerData.getRetailerID_Loyalty());
			}
			else
			{
				nonLoyalty.setRetailerId("1");
			}
			customer.setNonLoyalty(nonLoyalty);
		}
		body.setCustomer(customer);
		enrollRequest.setBody(body);

		return enrollRequest;
	}

	private String getProvinceCode(final String province)
	{
		try
		{
			if (StringUtils.isNotEmpty(province))
			{
				final Map<String, String> paramMap = new HashMap<String, String>();
				paramMap.put("code", province);
				final SearchResult<ProvinceModel> provinceList = getFlexibleSearchService().search(
						"Select {PK} from {province} where {code}=?code or {name}=?code", paramMap);
				return CollectionUtils.isNotEmpty(provinceList.getResult()) ? provinceList.getResult().get(0).getCode() : province;
			}
		}
		catch (final Exception e)
		{
			//
		}
		return province;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
