/**
 *
 */
package de.hybris.clicks.webclient.customer.client;

import de.hybris.clicks.webclient.customer.populator.CustomerDetailsRequestPopulator;
import de.hybris.clicks.webclient.customer.populator.CustomerResponseDataPopulator;
import de.hybris.clicks.webclient.customer.response.populator.CustomerRegistrationResponseProcessor;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.util.Config;

import java.net.URL;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import za.co.clicksgroup.esb.OmniChannel.Customer.BasicHttpBinding_ITwoWayAsyncStub;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.Response;

import com.thoughtworks.xstream.XStream;


/**
 * @author abhaydh
 * 
 */
public class CustomerDetailsRequestClient
{
	@Resource
	CustomerDetailsRequestPopulator customerDetailsRequestPopulator;
	CustomerResponseDataPopulator customerResposeDataPopulator;
	@Resource
	CustomerRegistrationResponseProcessor customerRegistrationResponseProcessor;

	BasicHttpBinding_ITwoWayAsyncStub customerStub;
	private static final Logger LOG = Logger.getLogger(CustomerDetailsRequestClient.class);

	public CustomerData getCC_CustomerDetails(final CustomerData customer)
	{
		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.detailsRequest"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerDetailsRequestClient class at getCC_CustomerDetails() method : " + e.getMessage());
		}

		Request detailsClubCardRequest = new Request();
		Response detailsClubCardResponse = new Response();
		detailsClubCardRequest = customerDetailsRequestPopulator.populateClubCardRequest(detailsClubCardRequest, customer);

		CustomerData customerData = new CustomerData();
		final ResultData result = new ResultData();
		result.setCode(new Integer(999));
		customerData.setResult(result);

		try
		{
			customerStub.setTimeout(Config.getInt("customer.request.details.timeout", 100000));
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- details CC request formed ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(detailsClubCardRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			detailsClubCardResponse = customerStub.requestCustomer(detailsClubCardRequest);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerDetailsRequestClient class at getCC_CustomerDetails() method : " + e.getMessage());
			return customerData;
		}
		if (null != detailsClubCardResponse && null != detailsClubCardResponse.getBody())
		{
			result.setCode(new Integer(detailsClubCardResponse.getBody().getCode()));
			result.setMessage(detailsClubCardResponse.getBody().getMessage());
			customerData.setResult(result);
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- details CC Response ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(detailsClubCardResponse));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			customerRegistrationResponseProcessor.processCustomerResponse(detailsClubCardResponse);
			customerResposeDataPopulator = new CustomerResponseDataPopulator();
			customerData = customerResposeDataPopulator.populate(detailsClubCardResponse, customerData);
		}

		return customerData;

	}

	public Response linkCC_CustomerDetails(final CustomerData customer)
	{
		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.detailsRequest"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
		}

		catch (final Exception e)
		{
			LOG.error("Error in CustomerDetailsRequestClient class at getCC_CustomerDetails() method : " + e.getMessage());
		}

		Request detailsClubCardRequest = new Request();
		Response detailsClubCardResponse = new Response();
		detailsClubCardRequest = customerDetailsRequestPopulator.populateClubCardRequest(detailsClubCardRequest, customer);

		try
		{
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));

			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- details NON- CC request ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(detailsClubCardRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			detailsClubCardResponse = customerStub.requestCustomer(detailsClubCardRequest);
			if (null != detailsClubCardResponse)
			{
				if (Config.getInt("customer.service.request.print", 0) == 1)
				{
					try
					{
						LOG.info("---- details NON- CC response ----");
						final XStream xStream = new XStream();
						LOG.info(xStream.toXML(detailsClubCardResponse));
					}
					catch (final Exception e)
					{
						LOG.info(e);
					}
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerDetailsRequestClient class at linkCC_CustomerDetails() method : " + e.getMessage());
			return null;
		}
		return detailsClubCardResponse;

	}

	public CustomerData getNonCC_CustomerDetails()
	{
		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.detailsRequest"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerDetailsRequestClient class at getNonCC_CustomerDetails() method : " + e.getMessage());
		}

		Request detailsNonClubCardRequest = new Request();
		Response detailsNonClubCardResponse = new Response();
		detailsNonClubCardRequest = customerDetailsRequestPopulator.populateNonClubCardRequest(detailsNonClubCardRequest);
		LOG.info("----details Non-ClubCard request formed--");

		CustomerData customerData = new CustomerData();
		final ResultData result = new ResultData();
		result.setCode(new Integer(999));
		customerData.setResult(result);

		try
		{
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- details NON- CC request ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(detailsNonClubCardRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			detailsNonClubCardResponse = customerStub.requestCustomer(detailsNonClubCardRequest);
			if (null != detailsNonClubCardResponse)
			{
				if (Config.getInt("customer.service.request.print", 0) == 1)
				{
					try
					{
						LOG.info("---- details NON- CC response ----");
						final XStream xStream = new XStream();
						LOG.info(xStream.toXML(detailsNonClubCardResponse));
					}
					catch (final Exception e)
					{
						LOG.info(e);
					}
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerDetailsRequestClient class at getNonCC_CustomerDetails() method : " + e.getMessage());
			return customerData;
		}

		if (null != detailsNonClubCardResponse && null != detailsNonClubCardResponse.getBody())
		{
			LOG.info("----details non clubcard Response-->" + detailsNonClubCardResponse);
			result.setCode(new Integer(detailsNonClubCardResponse.getBody().getCode()));
			result.setMessage(detailsNonClubCardResponse.getBody().getMessage());
			customerData.setResult(result);
			customerResposeDataPopulator = new CustomerResponseDataPopulator();
			customerData = customerResposeDataPopulator.populate(detailsNonClubCardResponse, customerData);
		}

		return customerData;

	}

}
