/**
 *
 */
package de.hybris.clicks.webclient.customer.populator;

import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;

import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.Resource;

import Omni.BizTalk.Clicks.Schema.Customer.Customer;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerAction;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.RequestBody;
import Omni.BizTalk.Clicks.Schema.Customer.RequestHeader;


/**
 * @author abhaydh
 *
 */
public class CustomerUpdatePopulator
{

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	public Request populateClubCard(final Request updateClubCardRequest)
	{

		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		final CustomerCoreDetails coreDetails = new CustomerCoreDetails();
		final CustomerHybris customerHybris = new CustomerHybris();
		final CustomerCoreDetailsAddressesAddress addresses = new CustomerCoreDetailsAddressesAddress();
		final CustomerLoyalty customerLoyalty = new CustomerLoyalty();
		final CustomerLoyaltyOptIns optIns = new CustomerLoyaltyOptIns();
		//final CustomerLoyaltyOptInsMarketingConsent marketingConsent = new CustomerLoyaltyOptInsMarketingConsent();
		final CustomerLoyaltyOptInsBabyClub babyClub = new CustomerLoyaltyOptInsBabyClub();
		final CustomerLoyaltyOptInsBabyClubAlreadyParent alreadyParent = new CustomerLoyaltyOptInsBabyClubAlreadyParent();
		//final CustomerLoyaltyOptInsBabyClubAlreadyParentChildren children = new CustomerLoyaltyOptInsBabyClubAlreadyParentChildren();
		final CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild child = new CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild();

		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);

		header.setCreatedDateTime(cal);
		header.setDataSource("Hybris");
		header.setEventCode("OMNIUPD");
		//header.setEventLogId("100169499");
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		updateClubCardRequest.setHeader(header);

		coreDetails.setSAResident(new Boolean(true).booleanValue());
		coreDetails.setIDNumber("8112095281111");
		coreDetails.setFirstName("UC_EHendrik");
		coreDetails.setLastName("UC_EKleynhans");
		coreDetails.setEMailAddress("UC_Edkleynhans@someplace.com");
		coreDetails.setPreferedName("UC_EDirk");
		coreDetails.setGender(CustomerCoreDetailsGender.value1);

		// array size for address here should be exactly same as number of address. extra size will cause error in esb webservice
		final CustomerCoreDetailsAddressesAddress[] addressesArray = new CustomerCoreDetailsAddressesAddress[1];
		addresses.setAddressLine1("P.O.Box 31918");
		addresses.setAddressTypeId(CustomerCoreDetailsAddressesAddressAddressTypeId.value1);
		addresses.setCity("Dalpark");
		addresses.setCountry("ZA");
		addresses.setPostalCode("1552");
		addresses.setProvince("ZA-GT");
		addresses.setSuburb("Dalpark EXT 4");
		addressesArray[0] = addresses;
		coreDetails.setAddresses(addressesArray);

		// array size for address here should be exactly same as number of address. extra size will cause error in esb webservice
		final CustomerCoreDetailsContactDetailsContactDetail[] contactsArray = new CustomerCoreDetailsContactDetailsContactDetail[2];
		final CustomerCoreDetailsContactDetailsContactDetail contact1 = new CustomerCoreDetailsContactDetailsContactDetail();
		contact1.setTypeId("2");
		contact1.setNumber("0820413023");
		contactsArray[0] = contact1;

		final CustomerCoreDetailsContactDetailsContactDetail contact2 = new CustomerCoreDetailsContactDetailsContactDetail();
		contact2.setTypeId("3");
		contact2.setNumber("0119434912");
		contactsArray[1] = contact2;

		coreDetails.setContactDetails(contactsArray);
		customer.setAction(CustomerAction.I);
		customer.setCoreDetails(coreDetails);

		customerHybris.setRetailerId("107");
		customerHybris.setCustomerId("12099");
		customer.setHybris(customerHybris);

		customerLoyalty.setHouseholdId("0200541231769");
		customerLoyalty.setMemberId("0200541231769");
		customerLoyalty.setRetailerId("1");

		//marketingConsent.setEmail(new Boolean(true).booleanValue());
		//marketingConsent.setSMS(new Boolean(true).booleanValue());
		//optIns.setMarketingConsent(marketingConsent);

		child.setDateOfBirth("2015-03-14");
		child.setFirstName("Gert");
		child.setGender(CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender.value1);
		child.setSurname("Kleynhans");

		//children.setChild(child);
		//alreadyParent.setChildren(children);
		babyClub.setAlreadyParent(alreadyParent);
		optIns.setBabyClub(babyClub);

		customerLoyalty.setOptIns(optIns);
		customer.setLoyalty(customerLoyalty);

		body.setCustomer(customer);
		updateClubCardRequest.setBody(body);

		return updateClubCardRequest;
	}


	public Request populateNonClubCard(final Request updateNonClubCardRequest)
	{
		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		final CustomerCoreDetails coreDetails = new CustomerCoreDetails();
		final CustomerHybris customerHybris = new CustomerHybris();

		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);
		header.setCreatedDateTime(cal);
		header.setDataSource("Hybris");
		header.setEventCode("OMNIUPD");
		//header.setEventLogId("100169482");
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		updateNonClubCardRequest.setHeader(header);

		coreDetails.setSAResident(new Boolean(true).booleanValue());
		coreDetails.setIDNumber("8112095280011");
		coreDetails.setFirstName("U_Hendrik");
		coreDetails.setLastName("U_Kleynhans");
		coreDetails.setEMailAddress("U_dkleynhans@someplace.com");
		coreDetails.setPreferedName("U_Dirk");
		customer.setAction(CustomerAction.I);
		customer.setCoreDetails(coreDetails);

		customerHybris.setRetailerId("10010");
		customerHybris.setCustomerId("108200");
		customer.setHybris(customerHybris);

		body.setCustomer(customer);
		updateNonClubCardRequest.setBody(body);

		return updateNonClubCardRequest;

	}
}
