/**
 *
 */
package de.hybris.clicks.webclient.customer.client;

import de.hybris.clicks.webclient.customer.populator.CustomerUpdatePopulator;
import de.hybris.clicks.webclient.customer.populator.EnrollCustomerPopulator;
import de.hybris.clicks.webclient.customer.response.populator.CustomerRegistrationResponseProcessor;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.util.Config;

import java.net.URL;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import za.co.clicksgroup.esb.OmniChannel.Customer.BasicHttpBinding_ITwoWayAsyncStub;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.Response;

import com.thoughtworks.xstream.XStream;


/**
 * @author abhaydh
 * 
 */
public class CustomerUpdateClient
{
	@Resource
	CustomerUpdatePopulator customerUpdatePopulator;

	BasicHttpBinding_ITwoWayAsyncStub customerStub;

	@Resource
	CustomerRegistrationResponseProcessor customerRegistrationResponseProcessor;
	@Resource
	EnrollCustomerPopulator enrollCustomerPopulator;
	private static final Logger LOG = Logger.getLogger(CustomerRegistrationClient.class);

	public ResultData updateCustomer(final CustomerData customerData)
	{
		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.update"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
			customerStub.setTimeout(Config.getInt("customer.request.update.timeout", 100000));
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerUpdateClient class at updateCustomer() method : " + e.getMessage());
		}

		Request updateRequest = new Request();
		Response updateResponse = new Response();

		updateRequest = enrollCustomerPopulator.populate(updateRequest, customerData, "update");


		final ResultData result = new ResultData();
		result.setCode(new Integer(999));

		try
		{
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- update request formed ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(updateRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			updateResponse = customerStub.updateCustomer(updateRequest);
		}
		catch (final Exception e1)
		{
			LOG.error("Error in CustomerUpdateClient class at updateCustomer() method : " + e1.getMessage());
			return result;
		}
		if (null != updateResponse && null != updateResponse.getBody())
		{
			// set response code as result
			result.setCode(new Integer(updateResponse.getBody().getCode()));
			result.setMessage(updateResponse.getBody().getMessage());

			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- update response----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(updateResponse));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			customerRegistrationResponseProcessor.processCustomerResponse(updateResponse);
		}

		return result;

	}

}
