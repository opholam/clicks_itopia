/**
 *
 */
package de.hybris.clicks.webclient.customer.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.ContactDetail;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.SegmentData;

import java.util.ArrayList;
import java.util.List;

import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsentCommsType;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment;
import Omni.BizTalk.Clicks.Schema.Customer.Response;


/**
 * @author abhaydh
 *
 */
public class CustomerResponseDataPopulator
{
	public CustomerData populate(final Response source, final CustomerData target)
	{
		if (null != source.getBody())
		{
			if (null != source.getBody().getCustomer())
			{
				if (null != source.getBody().getCustomer().getCoreDetails())
				{
					if (null != source.getBody().getCustomer().getCoreDetails().getFirstName())
					{
						target.setFirstName(source.getBody().getCustomer().getCoreDetails().getFirstName());
					}
					if (null != source.getBody().getCustomer().getCoreDetails().getDateOfBirth())
					{
						target.setNonRSA_DOB(source.getBody().getCustomer().getCoreDetails().getDateOfBirth());
					}
					if (null != source.getBody().getCustomer().getCoreDetails().getGender())
					{
						if (source.getBody().getCustomer().getCoreDetails().getGender().getValue() == 1)
						{
							target.setGender("1");
						}
						if (source.getBody().getCustomer().getCoreDetails().getGender().getValue() == 2)
						{
							target.setGender("2");
						}
					}
					if (null != source.getBody().getCustomer().getCoreDetails().getEMailAddress())
					{
						target.setEmailID(source.getBody().getCustomer().getCoreDetails().getEMailAddress());
					}
					if (null != source.getBody().getCustomer().getCoreDetails().getLastName())
					{
						target.setLastName(source.getBody().getCustomer().getCoreDetails().getLastName());
					}
					if (null != source.getBody().getCustomer().getCoreDetails().getPreferedName())
					{
						target.setPreferedName(source.getBody().getCustomer().getCoreDetails().getPreferedName());
					}
					if (null != source.getBody().getCustomer().getCoreDetails().getIDNumber())
					{
						target.setIDnumber(source.getBody().getCustomer().getCoreDetails().getIDNumber());
					}
					if (source.getBody().getCustomer().getCoreDetails().isSAResident())
					{
						target.setSAResident(new Boolean(source.getBody().getCustomer().getCoreDetails().isSAResident()));
					}


					if (null != source.getBody().getCustomer().getCoreDetails().getAddresses()
							&& source.getBody().getCustomer().getCoreDetails().getAddresses().length > 0)
					{
						final List<AddressData> addressDataList = new ArrayList<AddressData>();
						AddressData addressData;
						for (final CustomerCoreDetailsAddressesAddress address : source.getBody().getCustomer().getCoreDetails()
								.getAddresses())
						{
							addressData = new AddressData();
							if (null != address.getAddressLine1())
							{
								addressData.setLine1(address.getAddressLine1());
							}
							if (null != address.getAddressLine2())
							{
								addressData.setLine2(address.getAddressLine2());
							}
							if (null != address.getAddressTypeId())
							{
								addressData.setTypeID(address.getAddressTypeId().getValue());
							}
							if (null != address.getCity())
							{
								addressData.setCity(address.getCity());
							}
							if (null != address.getCountry())
							{
								final CountryData countryData = new CountryData();
								countryData.setIsocode(address.getCountry());
								addressData.setCountry(countryData);
							}
							if (null != address.getProvince())
							{
								addressData.setProvince(address.getProvince());
							}
							if (null != address.getPostalCode())
							{
								addressData.setPostalCode(address.getPostalCode());
							}
							if (null != address.getSuburb())
							{
								addressData.setSuburb(address.getSuburb());
							}

							addressDataList.add(addressData);
						}
						target.setAddresses(addressDataList);
					}

					if (null != source.getBody().getCustomer().getCoreDetails().getContactDetails()
							& source.getBody().getCustomer().getCoreDetails().getContactDetails().length > 0)
					{
						final List<ContactDetail> contactDetailsList = new ArrayList<ContactDetail>();
						ContactDetail contactDetailData;
						for (final CustomerCoreDetailsContactDetailsContactDetail contact : source.getBody().getCustomer()
								.getCoreDetails().getContactDetails())
						{
							contactDetailData = new ContactDetail();

							if (null != contact.getNumber())
							{
								contactDetailData.setNumber(contact.getNumber());
							}
							if (null != contact.getTypeId())
							{
								contactDetailData.setTypeID(contact.getTypeId());
							}
							contactDetailsList.add(contactDetailData);
						}
						target.setContactDetails(contactDetailsList);
					}
				}

				if (null != source.getBody().getCustomer().getHybris())
				{
					if (null != source.getBody().getCustomer().getHybris().getCustomerId())
					{
						target.setCustomerID(source.getBody().getCustomer().getHybris().getCustomerId());
					}
					if (null != source.getBody().getCustomer().getHybris().getRetailerId())
					{
						target.setRetailerID_hybris(source.getBody().getCustomer().getHybris().getRetailerId());
					}
				}

				if (null != source.getBody().getCustomer().getNonLoyalty())
				{
					if (null != source.getBody().getCustomer().getNonLoyalty().getRetailerId())
					{
						target.setRetailerID_NonLoyalty(source.getBody().getCustomer().getNonLoyalty().getRetailerId());
					}
					if (null != source.getBody().getCustomer().getNonLoyalty().getSegments()
							& source.getBody().getCustomer().getNonLoyalty().getSegments().length > 0)
					{
						final List<SegmentData> segmentDataList = new ArrayList<SegmentData>();
						SegmentData segmentData;
						for (final CustomerNonLoyaltySegmentsSegment segment : source.getBody().getCustomer().getNonLoyalty()
								.getSegments())
						{
							segmentData = new SegmentData();
							if (segment.getSegmentId() != 0)
							{
								segmentData.setSegmentID(new Integer(segment.getSegmentId()));
							}
							if (null != segment.getStatus())
							{
								segmentData.setSegmentStatus(new Integer(segment.getStatus().getValue()));
							}
							if (null != segment.getStartDate())
							{
								segmentData.setStartDate(segment.getStartDate());
							}
							if (null != segment.getEndDate())
							{
								segmentData.setEndDate(segment.getEndDate());
							}

							segmentDataList.add(segmentData);
						}
						target.setNonLoyalty_segments(segmentDataList);
					}
				}
				if (null != source.getBody().getCustomer().getLoyalty())
				{
					if (null != source.getBody().getCustomer().getLoyalty().getMemberId())
					{
						target.setMemberID(source.getBody().getCustomer().getLoyalty().getMemberId());
					}
					if (null != source.getBody().getCustomer().getLoyalty().getRetailerId())
					{
						target.setRetailerID_Loyalty(source.getBody().getCustomer().getLoyalty().getRetailerId());
					}
					if (null != source.getBody().getCustomer().getLoyalty().getHouseholdId())
					{
						target.setHouseholdID(source.getBody().getCustomer().getLoyalty().getHouseholdId());
					}
					if (null != source.getBody().getCustomer().getLoyalty().getIsMainMember())
					{
						target.setIsMainMember(source.getBody().getCustomer().getLoyalty().getIsMainMember());
					}

					if (null != source.getBody().getCustomer().getLoyalty().getOptIns())
					{
						if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent()
								&& source.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent().length > 0)
						{
							for (final CustomerLoyaltyOptInsMarketingConsentConsent consent : source.getBody().getCustomer()
									.getLoyalty().getOptIns().getMarketingConsent())
							{
								if (consent.getConsentType().equalsIgnoreCase("ACCINFO"))
								{
									for (final CustomerLoyaltyOptInsMarketingConsentConsentCommsType type : consent.getComms())
									{
										if (type.getTypeId() == 1)
										{
											target.setAccinfo_SMS(new Boolean(type.isAccepted()));
										}
										if (type.getTypeId() == 2)
										{
											target.setAccinfo_email(new Boolean(type.isAccepted()));
										}
									}
								}
								if (consent.getConsentType().equalsIgnoreCase("MARKETING"))
								{
									for (final CustomerLoyaltyOptInsMarketingConsentConsentCommsType type : consent.getComms())
									{
										if (type.getTypeId() == 1)
										{
											target.setMarketingConsent_SMS(new Boolean(type.isAccepted()));
										}
										if (type.getTypeId() == 2)
										{
											target.setMarketingConsent_email(new Boolean(type.isAccepted()));
										}
									}
								}

							}
						}

						if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent())
						{
							//							if (source.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent().isEmail())
							//							{
							//								target.setMarketingConsent_email(new Boolean(source.getBody().getCustomer().getLoyalty().getOptIns()
							//										.getMarketingConsent().isEmail()));
							//							}
							//							if (source.getBody().getCustomer().getLoyalty().getOptIns().getMarketingConsent().isSMS())
							//							{
							//								target.setMarketingConsent_SMS(new Boolean(source.getBody().getCustomer().getLoyalty().getOptIns()
							//										.getMarketingConsent().isSMS()));
							//							}
						}

						if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub())
						{
							//final BabyClubData babyClubData = new BabyClubData();
							if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub().getAlreadyParent())
							{
								//								if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub().getAlreadyParent()
								//										.getChildren())
								//								{
								//									if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub().getAlreadyParent()
								//											.getChildren().getChild())
								//									{
								//										if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//												.getAlreadyParent().getChildren().getChild().getFirstName())
								//										{
								//											babyClubData.setFirstName(source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//													.getAlreadyParent().getChildren().getChild().getFirstName());
								//										}
								//										if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//												.getAlreadyParent().getChildren().getChild().getSurname())
								//										{
								//											babyClubData.setLastName(source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//													.getAlreadyParent().getChildren().getChild().getSurname());
								//										}
								//										if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//												.getAlreadyParent().getChildren().getChild().getDateOfBirth())
								//										{
								//											babyClubData.setChildDOB(source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//													.getAlreadyParent().getChildren().getChild().getDateOfBirth());
								//										}
								//										if (null != source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub()
								//												.getAlreadyParent().getChildren().getChild().getGender())
								//										{
								//											if (source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub().getAlreadyParent()
								//													.getChildren().getChild().getGender().getValue() == 1)
								//											{
								//												babyClubData.setGender("1");
								//											}
								//											if (source.getBody().getCustomer().getLoyalty().getOptIns().getBabyClub().getAlreadyParent()
								//													.getChildren().getChild().getGender().getValue() == 2)
								//											{
								//												babyClubData.setGender("2");
								//											}
								//										}
								//										target.setBabyClub(babyClubData);
								//									}
								//								}
							}
						}
					}
					if (null != source.getBody().getCustomer().getLoyalty().getMagazine())
					{
						if (null != source.getBody().getCustomer().getLoyalty().getMagazine().getStatus())
						{
							target.setMagazineStatus(new Integer(source.getBody().getCustomer().getLoyalty().getMagazine().getStatus()
									.getValue()));
						}
					}
					if (null != source.getBody().getCustomer().getLoyalty().getSegments()
							& source.getBody().getCustomer().getLoyalty().getSegments().length > 0)
					{
						final List<SegmentData> segmentDataList = new ArrayList<SegmentData>();
						SegmentData segmentData;
						for (final CustomerLoyaltySegmentsSegment segment : source.getBody().getCustomer().getLoyalty().getSegments())
						{
							segmentData = new SegmentData();
							if (segment.getSegmentId() != 0)
							{
								segmentData.setSegmentID(new Integer(segment.getSegmentId()));
							}
							if (null != segment.getStatus())
							{
								segmentData.setSegmentStatus(new Integer(segment.getStatus().getValue()));
							}
							if (null != segment.getStartDate())
							{
								segmentData.setStartDate(segment.getStartDate());
							}
							if (null != segment.getEndDate())
							{
								segmentData.setEndDate(segment.getEndDate());
							}
							if (null != segment.getSegmentTypeId())
							{
								segmentData.setSegmentType(segment.getSegmentTypeId().getValue());
							}
							segmentDataList.add(segmentData);
						}
						target.setLoyalty_segments(segmentDataList);

					}
				}
			}
		}
		return target;
	}
}
