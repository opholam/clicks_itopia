/**
 *
 */
package de.hybris.clicks.webclient.customer.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;

import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.Resource;

import Omni.BizTalk.Clicks.Schema.Customer.Customer;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.RequestBody;
import Omni.BizTalk.Clicks.Schema.Customer.RequestHeader;


/**
 * @author abhaydh
 *
 */
public class CustomerDetailsRequestPopulator
{

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	public Request populateClubCardRequest(final Request detailsClubCardRequest, final CustomerData customer2)
	{
		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		//final CustomerHybris customerHybris = new CustomerHybris();
		final CustomerLoyalty customerLoyalty = new CustomerLoyalty();



		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);
		header.setCreatedDateTime(cal);
		if (customer2.isSourceMobileApp())
		{
			header.setDataSource("Mobile");
		}
		else
		{
			header.setDataSource("Hybris");
		}
		header.setEventCode("OMNIREQ");
		//header.setEventLogId("100169475");
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		detailsClubCardRequest.setHeader(header);

		final CustomerCoreDetails coreDetails = new CustomerCoreDetails();

		if (customer2.getSAResident().booleanValue() && null != customer2.getIDnumber() && !customer2.getIDnumber().isEmpty())
		{
			coreDetails.setIDNumber(customer2.getIDnumber());
		}
		if (null != customer2.getEmail() && !customer2.getEmail().isEmpty())
		{
			coreDetails.setEMailAddress(customer2.getEmail());
		}

		coreDetails.setSAResident(customer2.getSAResident().booleanValue());

		if (null != customer2.getNonRSA_DOB())
		{
			coreDetails.setDateOfBirth(customer2.getNonRSA_DOB());
		}

		customer.setCoreDetails(coreDetails);

		//		customerHybris.setRetailerId("1");
		//		if (null != customer2.getCustomerID())
		//		{
		//			customerHybris.setCustomerId(customer2.getCustomerID());
		//		}
		//		else
		//		{
		//			customerHybris.setCustomerId("10466");
		//		}
		//		customer.setHybris(customerHybris);

		customerLoyalty.setRetailerId("1");
		if (null != customer2.getMemberID())
		{
			customerLoyalty.setMemberId(customer2.getMemberID());
		}
		else
		{
			customerLoyalty.setMemberId("0200541231769");
		}
		customer.setLoyalty(customerLoyalty);

		body.setCustomer(customer);
		detailsClubCardRequest.setBody(body);

		return detailsClubCardRequest;
	}

	/**
	 * @param detailsNonClubCardRequest
	 * @return
	 */
	public Request populateNonClubCardRequest(final Request detailsNonClubCardRequest)
	{
		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		final CustomerHybris customerHybris = new CustomerHybris();

		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);
		header.setCreatedDateTime(cal);
		header.setDataSource("Hybris");
		header.setEventCode("OMNIREQ");
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);
		//header.setEventLogId("100169475");

		detailsNonClubCardRequest.setHeader(header);

		customerHybris.setRetailerId("1");
		customerHybris.setCustomerId("10466");
		customer.setHybris(customerHybris);


		body.setCustomer(customer);
		detailsNonClubCardRequest.setBody(body);

		return detailsNonClubCardRequest;
	}



}
