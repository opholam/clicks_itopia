/**
 *
 */
package de.hybris.clicks.webclient.customer.response.populator;

import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.core.model.ProvinceModel;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail;


/**
 * @author abhaydh
 *
 */
public class CustomerCoreDetailsPopulator
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource
	private UserService userService;

	public CustomerModel populate(final CustomerCoreDetails coreDetails, final CustomerModel customerModel)
	{

		if (null != coreDetails.getPreferedName() && StringUtils.isNotEmpty(coreDetails.getPreferedName()))
		{
			customerModel.setName(getTitleCase(coreDetails.getPreferedName()));
		}
		else
		{
			customerModel.setName(getTitleCase(coreDetails.getFirstName()));
		}
		if (null != coreDetails.getFirstName() && StringUtils.isNotEmpty(coreDetails.getFirstName()))
		{
			customerModel.setFirstName(getTitleCase(coreDetails.getFirstName()));
		}
		if (null != coreDetails.getLastName() && StringUtils.isNotEmpty(coreDetails.getLastName()))
		{
			customerModel.setLastName(getTitleCase(coreDetails.getLastName()));
		}
		if (null != coreDetails.getEMailAddress() && StringUtils.isNotEmpty(coreDetails.getEMailAddress()))
		{
			customerModel.setEmailID(getTitleCase(coreDetails.getEMailAddress()));
		}
		if (null != coreDetails.getGender())
		{
			if (coreDetails.getGender().getValue() == 1)
			{
				customerModel.setGender(Gender.MALE);
			}
			if (coreDetails.getGender().getValue() == 2)
			{
				customerModel.setGender(Gender.FEMALE);
			}
		}

		if (coreDetails.isSAResident())
		{
			customerModel.setSAResident(Boolean.TRUE);
		}
		else
		{
			customerModel.setSAResident(Boolean.FALSE);
		}

		if (null != coreDetails.getIDNumber())
		{
			customerModel.setRSA_ID(coreDetails.getIDNumber());
		}
		if (null != coreDetails.getDateOfBirth())
		{
			customerModel.setNonRSA_DOB(coreDetails.getDateOfBirth());
		}

		//populate  Address
		final List<AddressModel> addressList = new ArrayList<AddressModel>();
		AddressModel addressModel = null;
		AddressModel existingPostalAddressModel = null;
		if (null != coreDetails.getAddresses() && coreDetails.getAddresses().length > 0)
		{
			existingPostalAddressModel = (AddressModel) CollectionUtils.find(customerModel.getAddresses(),
					new BeanPropertyValueEqualsPredicate("type", "Postal Address", true));
			for (final CustomerCoreDetailsAddressesAddress address : coreDetails.getAddresses())
			{
				addressModel = modelService.create(AddressModel.class);
				addressModel = populateAddress(address, addressModel, existingPostalAddressModel);
				addressModel.setOwner(customerModel);
				addressList.add(addressModel);
			}
		}
		if (CollectionUtils.isNotEmpty(customerModel.getAddresses()))
		{
			for (final AddressModel address : customerModel.getAddresses())
			{
				if ((Boolean.TRUE.equals(address.getShippingAddress()) || Boolean.TRUE.equals(address.getBillingAddress()))
						&& Boolean.TRUE.equals(address.getVisibleInAddressBook()))
				{
					addressList.add(address);
				}
			}
		}
		if (addressList.size() > 0)
		{
			customerModel.setAddresses(addressList);
		}
		//end of populate  Address


		//populate  Address
		final List<ContactDetailsModel> contactDetailsList = new ArrayList<ContactDetailsModel>();
		ContactDetailsModel contactDetailsModel = null;
		if (null != coreDetails.getContactDetails() && coreDetails.getContactDetails().length > 0)
		{
			for (final CustomerCoreDetailsContactDetailsContactDetail contactDetail : coreDetails.getContactDetails())
			{
				contactDetailsModel = modelService.create(ContactDetailsModel.class);
				contactDetailsModel = populateContact(contactDetail, contactDetailsModel);
				contactDetailsList.add(contactDetailsModel);
			}
		}
		if (contactDetailsList.size() > 0)
		{
			customerModel.setContactDetails(contactDetailsList);
		}
		//end of populate  Address


		modelService.save(customerModel);
		return customerModel;
	}




	/**
	 * @param contactDetail
	 * @param contactDetailsModel
	 * @return
	 */
	private ContactDetailsModel populateContact(final CustomerCoreDetailsContactDetailsContactDetail source,
			final ContactDetailsModel target)
	{
		if (null != source.getTypeId())
		{
			target.setTypeID(source.getTypeId());
			if (source.getTypeId().equalsIgnoreCase("1"))
			{
				target.setType("Home");
			}
			if (source.getTypeId().equalsIgnoreCase("2"))
			{
				target.setType("Mobile");
			}
			if (source.getTypeId().equalsIgnoreCase("3"))
			{
				target.setType("Work");
			}
			if (source.getTypeId().equalsIgnoreCase("4"))
			{
				target.setType("Other");
			}
		}
		if (null != source.getNumber())
		{
			target.setNumber(source.getNumber());
		}

		return target;
	}




	/**
	 * @param postalAddress
	 * @param address
	 * @return
	 */
	private AddressModel populateAddress(final CustomerCoreDetailsAddressesAddress source, final AddressModel target,
			final AddressModel existingPostalAddressModel)
	{
		if (null != source.getAddressTypeId())
		{
			target.setTypeID(source.getAddressTypeId().getValue());
			if (source.getAddressTypeId().getValue() == "1")
			{
				target.setType("Postal Address");
				if (null != existingPostalAddressModel && StringUtils.isBlank(source.getProvince())
						&& StringUtils.isNotBlank(existingPostalAddressModel.getProvince()))
				{
					source.setProvince(existingPostalAddressModel.getProvince());
				}
			}
			if (source.getAddressTypeId().getValue() == "2")
			{
				target.setType("Physical Address");
			}
			if (source.getAddressTypeId().getValue() == "3")
			{
				target.setType("Delivery Address");
			}
		}

		if (null != source.getAddressLine1())
		{
			target.setBuilding(getTitleCase(source.getAddressLine1()));
		}
		if (null != source.getAddressLine2())
		{
			target.setStreetname(getTitleCase(source.getAddressLine2()));
		}
		if (null != source.getSuburb())
		{
			target.setStreetnumber(getTitleCase(source.getSuburb()));
		}
		if (null != source.getPostalCode())
		{
			target.setPostalcode(source.getPostalCode());
		}
		if (null != source.getCity() && null != source.getProvince())
		{
			target.setTown(source.getCity() + ", " + source.getProvince());
		}
		else if (null != source.getCity())
		{
			target.setTown(getTitleCase(source.getCity()));
		}
		else if (StringUtils.isNotBlank(source.getProvince()))
		{
			ProvinceModel provinceModel = modelService.create(ProvinceModel.class);
			provinceModel.setCode(source.getProvince());
			try
			{
				provinceModel = flexibleSearchService.getModelByExample(provinceModel);
				target.setTown(provinceModel.getName());
			}
			catch (final Exception e)
			{
				//
			}

		}
		if (StringUtils.isNotBlank(source.getProvince()))
		{
			target.setProvince(source.getProvince());
		}

		if (null != source.getCountry())
		{
			//final CountryModel country = commonI18NService.getCountry(source.getCountry());
			CountryModel country = new CountryModel();
			modelService.attach(country);
			//country.setName(source.getCountry());
			country.setIsocode(source.getCountry());
			country = flexibleSearchService.getModelByExample(country);
			if (null != country)
			{
				target.setCountry(country);
			}
		}
		return target;
	}

	public static String getTitleCase(final String s)
	{
		final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
		final StringBuilder sb = new StringBuilder();
		boolean capNext = true;
		for (char c : s.toCharArray())
		{
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (ACTIONABLE_DELIMITERS.indexOf(c) >= 0); // explicit cast not needed
		}
		return sb.toString();
	}
}
