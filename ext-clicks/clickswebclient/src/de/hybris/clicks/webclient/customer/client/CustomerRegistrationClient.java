/**
 *
 */
package de.hybris.clicks.webclient.customer.client;

import de.hybris.clicks.webclient.customer.populator.EnrollCustomerPopulator;
import de.hybris.clicks.webclient.customer.populator.RegisterCustomerPopulator;
import de.hybris.clicks.webclient.customer.response.populator.CustomerRegistrationResponseProcessor;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.util.Config;

import java.net.URL;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import za.co.clicksgroup.esb.OmniChannel.Customer.BasicHttpBinding_ITwoWayAsyncStub;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.Response;

import com.thoughtworks.xstream.XStream;


/**
 * @author abhaydh
 * 
 */
public class CustomerRegistrationClient
{
	@Resource
	RegisterCustomerPopulator registerCustomerPopulator;
	@Resource
	CustomerRegistrationResponseProcessor customerRegistrationResponseProcessor;
	@Resource
	EnrollCustomerPopulator enrollCustomerPopulator;
	private static final Logger LOG = Logger.getLogger(CustomerRegistrationClient.class);

	BasicHttpBinding_ITwoWayAsyncStub customerStub;
	Request clubcardRequest;
	Response clubcardResponse;

	Request nonClubcardRequest;
	Response nonClubcardResponse;

	Request updateRequest;
	Response updateResponse;

	public ResultData registerCCcustomer(final CustomerData customer)
	{

		clubcardRequest = new Request();
		clubcardResponse = new Response();

		updateRequest = new Request();
		updateResponse = new Response();

		clubcardRequest = registerCustomerPopulator.populateClubcard(clubcardRequest, customer, "register");
		LOG.info("---clubcard Request formed---");
		//updateRequest = enrollCustomerPopulator.populate(updateRequest, customer, "update");

		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.register"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerRegistrationClient class at registerCCcustomer() method : " + e.getMessage());
		}

		final ResultData result = new ResultData();
		result.setCode(new Integer(999));

		try
		{
			customerStub.setTimeout(Config.getInt("customer.request.register.timeout", 100000));
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));

			// commented this since request is going twice

			//			updateResponse = customerStub.updateCustomer(updateRequest);
			//			if (null != updateResponse)
			//			{
			//				customerRegistrationResponseProcessor.processCustomerResponse(updateResponse);
			//			}
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- register CC request formed ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(clubcardRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			clubcardResponse = customerStub.registerCustomer(clubcardRequest);

		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerRegistrationClient class at registerCCcustomer() method : " + e.getMessage());
			return result;
		}
		if (null != clubcardResponse && null != clubcardResponse.getBody())
		{
			result.setCode(new Integer(clubcardResponse.getBody().getCode()));
			result.setMessage(clubcardResponse.getBody().getMessage());
			LOG.info("--- registration response--- " + clubcardResponse);
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- register CC  response ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(clubcardResponse));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			customerRegistrationResponseProcessor.processCustomerResponse(clubcardResponse);
		}
		return result;
	}

	public ResultData registerNonCCcustomer(final CustomerData customer)
	{
		nonClubcardRequest = new Request();
		nonClubcardResponse = new Response();

		nonClubcardRequest = registerCustomerPopulator.populateNonClubcard(nonClubcardRequest, customer);
		LOG.info("---non-clubcard Request formed---");

		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.register"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerRegistrationClient class at registerNonCCcustomer() method : " + e.getMessage());
		}

		final ResultData result = new ResultData();
		result.setCode(new Integer(999));

		try
		{
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- register non CC request formed ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(nonClubcardRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			nonClubcardResponse = customerStub.registerCustomer(nonClubcardRequest);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerRegistrationClient class at registerNonCCcustomer() method : " + e.getMessage());
			return result;
		}
		if (null != nonClubcardResponse && null != nonClubcardResponse.getBody())
		{
			result.setCode(new Integer(nonClubcardResponse.getBody().getCode()));
			result.setMessage(nonClubcardResponse.getBody().getMessage());
			LOG.info("--- registration response--- " + nonClubcardResponse);
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- register non CC response ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(nonClubcardResponse));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			customerRegistrationResponseProcessor.processCustomerResponse(nonClubcardResponse);
		}
		return result;
	}

}
