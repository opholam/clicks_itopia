/**
 *
 */
package de.hybris.clicks.webclient.customer.client;

import de.hybris.clicks.webclient.customer.populator.CustomerResponseDataPopulator;
import de.hybris.clicks.webclient.customer.populator.EnrollCustomerPopulator;
import de.hybris.clicks.webclient.customer.response.populator.CustomerRegistrationResponseProcessor;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.ResultData;
import de.hybris.platform.util.Config;

import java.net.URL;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import za.co.clicksgroup.esb.OmniChannel.Customer.BasicHttpBinding_ITwoWayAsyncStub;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.Response;

import com.thoughtworks.xstream.XStream;


/**
 * @author abhaydh
 * 
 */
public class CustomerEnrollmentClient
{
	@Resource
	EnrollCustomerPopulator enrollCustomerPopulator;
	@Resource
	CustomerRegistrationResponseProcessor customerRegistrationResponseProcessor;

	CustomerResponseDataPopulator customerResposeDataPopulator;
	BasicHttpBinding_ITwoWayAsyncStub customerStub;
	private static final Logger LOG = Logger.getLogger(CustomerEnrollmentClient.class);

	public ResultData enrollCustomer(final CustomerData customer)
	{
		LOG.info("--in enroll client--");
		LOG.info("--customer--" + customer);
		try
		{
			final URL endpointURL = new URL(Config.getParameter("customer.Service.URL.enroll"));
			customerStub = new BasicHttpBinding_ITwoWayAsyncStub(endpointURL, null);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerEnrollmentClient class at enrollCustomer() method : " + e.getMessage());
		}

		Request enrollRequest = new Request();
		Response enrollResponse = new Response();
		enrollRequest = enrollCustomerPopulator.populate(enrollRequest, customer, "enroll");


		final ResultData result = new ResultData();
		result.setCode(new Integer(999));

		try
		{
			customerStub.setTimeout(Config.getInt("customer.request.enroll.timeout", 100000));
			customerStub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY, Config.getParameter("customer.Service.username"));
			customerStub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY, Config.getParameter("customer.Service.password"));
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- Enroll request ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(enrollRequest));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			enrollResponse = customerStub.enrolCustomer(enrollRequest);
		}
		catch (final Exception e)
		{
			LOG.error("Error in CustomerEnrollmentClient class at enrollCustomer() method : " + e.getMessage());
			return result;
		}
		if (null != enrollResponse && null != enrollResponse.getBody())
		{
			// set response code as result
			result.setCode(new Integer(enrollResponse.getBody().getCode()));
			result.setMessage(enrollResponse.getBody().getMessage());
			if (Config.getInt("customer.service.request.print", 0) == 1)
			{
				try
				{
					LOG.info("---- Enroll Response ----");
					final XStream xStream = new XStream();
					LOG.info(xStream.toXML(enrollResponse));
				}
				catch (final Exception e)
				{
					LOG.info(e);
				}
			}
			customerRegistrationResponseProcessor.processCustomerResponse(enrollResponse);
		}
		return result;
	}
}
