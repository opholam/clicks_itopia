/**
 *
 */
package de.hybris.clicks.webclient.customer.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import Omni.BizTalk.Clicks.Schema.Customer.Customer;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType;
import Omni.BizTalk.Clicks.Schema.Customer.Request;
import Omni.BizTalk.Clicks.Schema.Customer.RequestBody;
import Omni.BizTalk.Clicks.Schema.Customer.RequestHeader;


/**
 * @author abhaydh
 *
 */
public class RegisterCustomerPopulator
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Resource(name = "eventLogIDSeries")
	private PersistentKeyGenerator eventLogIDSeries;

	public Request populateClubcard(final Request clubcardRequest, final CustomerData customerData, final String action)
	{

		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		final CustomerCoreDetails coreDetails = new CustomerCoreDetails();
		final CustomerHybris customerHybris = new CustomerHybris();
		final CustomerLoyalty customerLoyalty = new CustomerLoyalty();

		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);
		header.setCreatedDateTime(cal);
		if (customerData.isSourceMobileApp())
		{
			header.setDataSource("Mobile");
		}
		else
		{
			header.setDataSource("Hybris");
		}
		if (action.equalsIgnoreCase("register"))
		{
			header.setEventCode("OMNIREG");
		}
		if (action.equalsIgnoreCase("update"))
		{
			header.setEventCode("OMNIUPD");
		}
		//header.setEventLogId("100169475");
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		clubcardRequest.setHeader(header);

		coreDetails.setSAResident(customerData.getSAResident().booleanValue());
		coreDetails.setIDNumber(customerData.getIDnumber());

		if (null != customerData.getNonRSA_DOB())
		{
			coreDetails.setDateOfBirth(customerData.getNonRSA_DOB());
		}

		if (null != customerData.getEmailID() && StringUtils.isNotEmpty(customerData.getEmailID()))
		{
			coreDetails.setEMailAddress(customerData.getEmailID());
		}
		customer.setCoreDetails(coreDetails);

		customerHybris.setRetailerId(customerData.getRetailerID_hybris());
		customerHybris.setCustomerId(customerData.getCustomerID());
		customer.setHybris(customerHybris);
		if (null != customerData.getRetailerID_Loyalty() && StringUtils.isNotBlank(customerData.getRetailerID_Loyalty()))
		{
			customerLoyalty.setRetailerId(customerData.getRetailerID_Loyalty());
		}
		else
		{
			customerLoyalty.setRetailerId("1");
		}
		customerLoyalty.setMemberId(customerData.getMemberID());
		if (Config.getBoolean("customer.Service.add.loyalty", true))
		{
			customer.setLoyalty(customerLoyalty);
		}

		body.setCustomer(customer);

		clubcardRequest.setBody(body);

		return clubcardRequest;
	}


	public Request populateNonClubcard(final Request nonClubcardRequest, final CustomerData customerData)
	{
		final RequestHeader header = new RequestHeader();
		final RequestBody body = new RequestBody();
		final Customer customer = new Customer();
		final CustomerCoreDetails coreDetails = new CustomerCoreDetails();
		final CustomerNonLoyalty customerLoyalty = new CustomerNonLoyalty();
		final CustomerHybris customerHybris = new CustomerHybris();
		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);

		header.setCreatedDateTime(cal);
		if (customerData.isSourceMobileApp())
		{
			header.setDataSource("Hybris");
		}
		else
		{
			header.setDataSource("Hybris");
		}
		header.setEventCode("OMNIREG");
		//header.setEventLogId("100169475");
		final String eventLogID = (String) eventLogIDSeries.generate();
		header.setEventLogId(eventLogID);

		nonClubcardRequest.setHeader(header);

		coreDetails.setSAResident(customerData.getSAResident().booleanValue());
		if (null != customerData.getIDnumber() && StringUtils.isNotEmpty(customerData.getIDnumber()))
		{
			coreDetails.setIDNumber(customerData.getIDnumber());
		}
		coreDetails.setFirstName(customerData.getFirstName());
		coreDetails.setLastName(customerData.getLastName());
		coreDetails.setEMailAddress(customerData.getEmailID());
		coreDetails.setPreferedName(customerData.getPreferedName());
		if (null != customerData.getNonRSA_DOB())
		{
			coreDetails.setDateOfBirth(customerData.getNonRSA_DOB());
		}
		customer.setCoreDetails(coreDetails);

		customerHybris.setRetailerId(customerData.getRetailerID_hybris());
		customerHybris.setCustomerId(customerData.getCustomerID());
		customer.setHybris(customerHybris);


		final CustomerNonLoyaltyOptIns optIns = new CustomerNonLoyaltyOptIns();

		// set marketing consent here from customerData
		final CustomerNonLoyaltyOptInsMarketingConsentConsent[] marketingConsentArray = new CustomerNonLoyaltyOptInsMarketingConsentConsent[1];

		final CustomerNonLoyaltyOptInsMarketingConsentConsent consent = new CustomerNonLoyaltyOptInsMarketingConsentConsent();
		final CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[] commsArray = new CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[2];

		CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType type = new CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType();

		consent.setConsentType("MARKETING");
		if (null != customerData.getMarketingConsent_SMS())
		{
			type.setAccepted(customerData.getMarketingConsent_SMS().booleanValue());
		}
		else
		{
			type.setAccepted(false);
		}
		type.setTypeId(1);
		commsArray[0] = type;
		type = new CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType();
		if (null != customerData.getMarketingConsent_email())
		{
			type.setAccepted(customerData.getMarketingConsent_email().booleanValue());
		}
		else
		{
			type.setAccepted(false);
		}
		type.setTypeId(2);
		commsArray[1] = type;
		consent.setComms(commsArray);
		marketingConsentArray[0] = consent;

		optIns.setMarketingConsent(marketingConsentArray);
		customerLoyalty.setOptIns(optIns);
		if (null != customerData.getRetailerID_Loyalty() && StringUtils.isNotBlank(customerData.getRetailerID_Loyalty()))
		{
			customerLoyalty.setRetailerId(customerData.getRetailerID_Loyalty());
		}
		else
		{
			customerLoyalty.setRetailerId("1");
		}
		customer.setNonLoyalty(customerLoyalty);
		body.setCustomer(customer);
		nonClubcardRequest.setBody(body);

		return nonClubcardRequest;
	}
}
