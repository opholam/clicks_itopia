/**
 *
 */
package de.hybris.clicks.webclient.customer.response.populator;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty;
import Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment;


/**
 * @author abhaydh
 *
 */
public class CustomerNonLoyaltyDetailsPopulator
{
	protected static final Logger LOG = Logger.getLogger(CustomerNonLoyaltyDetailsPopulator.class);

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;


	/**
	 * @param nonLoyalty
	 * @param customerModel
	 * @return
	 */
	public CustomerModel populate(final CustomerNonLoyalty nonLoyalty, final CustomerModel customerModel)
	{
		final Set<PrincipalGroupModel> userGroups = new HashSet<PrincipalGroupModel>();
		if (null != nonLoyalty.getRetailerId())
		{
			customerModel.setRetailerID_NonLoyalty(nonLoyalty.getRetailerId());
		}

		if (null != nonLoyalty.getSegments() && nonLoyalty.getSegments().length > 0)
		{
			if (null != customerModel.getGroups())
			{
				Set<PrincipalGroupModel> userGroups2 = new HashSet<PrincipalGroupModel>();
				userGroups2 = customerModel.getGroups();
				userGroups.addAll(userGroups2);
			}

			final List<UserGroupModel> segmentModelList = new ArrayList<UserGroupModel>();
			List<UserGroupModel> segmentModelList2 = new ArrayList<UserGroupModel>();
			if (null != customerModel.getNonLoyalty_segments())
			{
				segmentModelList2 = customerModel.getNonLoyalty_segments();
				segmentModelList.addAll(segmentModelList2);
			}
			UserGroupModel userGroupModel = null;
			boolean isUpdate = false;
			for (final CustomerNonLoyaltySegmentsSegment segment : nonLoyalty.getSegments())
			{
				userGroupModel = modelService.create(UserGroupModel.class);
				isUpdate = false;
				if (!CollectionUtils.isEmpty(segmentModelList2))
				{
					for (final UserGroupModel userModel : segmentModelList2)
					{
						if (userModel.getUid().equals(new Integer(segment.getSegmentId()).toString()))
						{
							isUpdate = true;
						}
					}
				}
				if (segment.getSegmentId() != 0)
				{
					userGroupModel.setUid(new Integer(segment.getSegmentId()).toString());
				}
				try
				{
					userGroupModel = flexibleSearchService.getModelByExample(userGroupModel);
				}
				catch (final Exception e)
				{
					LOG.error("Error in CustomerNonLoyaltyDetailsPopulator class at populate() method : " + e.getMessage());
				}
				userGroupModel = populateSegments(segment, userGroupModel);
				modelService.save(userGroupModel);
				userGroups.add(userGroupModel);
				if (userGroups.size() > 0)
				{
					customerModel.setGroups(userGroups);
				}
				if (!isUpdate)
				{
					segmentModelList.add(userGroupModel);
				}
			}

			if (segmentModelList.size() > 0)
			{
				customerModel.setNonLoyalty_segments(segmentModelList);
			}

		}
		modelService.save(customerModel);
		return customerModel;
	}


	private UserGroupModel populateSegments(final CustomerNonLoyaltySegmentsSegment source, final UserGroupModel target)
	{

		if (source.getSegmentId() != 0)
		{
			target.setUid(new Integer(source.getSegmentId()).toString());
		}

		if (null != source.getStatus())
		{
			target.setSegmentStatus(new Integer(source.getStatus().getValue()));
		}
		if (null != source.getStartDate())
		{
			target.setStartDate(source.getStartDate());
		}
		if (null != source.getEndDate())
		{
			target.setEndDate(source.getEndDate());
		}

		return target;
	}

}
