/**
 *
 */
package de.hybris.clicks.webclient.customer.response.populator;

import de.hybris.clicks.core.model.ProdHeaderDetailsModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import Omni.BizTalk.Clicks.Schema.Customer.ResponseHeader;


/**
 * @author abhaydh
 *
 */
public class CustomerHeaderPopulator
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	public void populate(final ResponseHeader source)
	{

		final ProdHeaderDetailsModel target = modelService.create(ProdHeaderDetailsModel.class);

		if (null != source.getDataSource())
		{
			target.setDataSource(source.getDataSource());
		}
		if (null != source.getEventCode())
		{
			target.setEventCode(source.getEventCode());
		}
		if (null != source.getEventLogId())
		{
			target.setEventLogId(source.getEventLogId());
		}
		if (null != source.getEventCode())
		{
			target.setFeedType(source.getEventCode());
		}
		if (null != source.getCreatedDateTime())
		{
			target.setCreatedDateTime(source.getCreatedDateTime().getTime());
		}

		modelService.save(target);

	}
}
