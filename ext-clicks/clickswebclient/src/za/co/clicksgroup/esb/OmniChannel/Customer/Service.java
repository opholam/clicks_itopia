/**
 * Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.clicksgroup.esb.OmniChannel.Customer;

public interface Service extends java.rmi.Remote {
    public Omni.BizTalk.Clicks.Schema.Customer.Response enrolCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException;
    public Omni.BizTalk.Clicks.Schema.Customer.Response registerCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException;
    public Omni.BizTalk.Clicks.Schema.Customer.Response updateCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException;
    public Omni.BizTalk.Clicks.Schema.Customer.Response requestCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException;
}
