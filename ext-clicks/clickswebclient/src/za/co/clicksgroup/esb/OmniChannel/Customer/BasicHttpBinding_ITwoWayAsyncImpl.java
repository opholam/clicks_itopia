/**
 * BasicHttpBinding_ITwoWayAsyncImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.clicksgroup.esb.OmniChannel.Customer;

public class BasicHttpBinding_ITwoWayAsyncImpl implements za.co.clicksgroup.esb.OmniChannel.Customer.Service{
    public Omni.BizTalk.Clicks.Schema.Customer.Response enrolCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        return null;
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response registerCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        return null;
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response updateCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        return null;
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response requestCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        return null;
    }

}
