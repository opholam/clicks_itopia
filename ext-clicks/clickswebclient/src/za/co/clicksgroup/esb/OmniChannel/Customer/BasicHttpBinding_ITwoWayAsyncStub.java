/**
 * BasicHttpBinding_ITwoWayAsyncStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.clicksgroup.esb.OmniChannel.Customer;

public class BasicHttpBinding_ITwoWayAsyncStub extends org.apache.axis.client.Stub implements za.co.clicksgroup.esb.OmniChannel.Customer.Service {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[4];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EnrolCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Request"), Omni.BizTalk.Clicks.Schema.Customer.Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Response"));
        oper.setReturnClass(Omni.BizTalk.Clicks.Schema.Customer.Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegisterCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Request"), Omni.BizTalk.Clicks.Schema.Customer.Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Response"));
        oper.setReturnClass(Omni.BizTalk.Clicks.Schema.Customer.Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Request"), Omni.BizTalk.Clicks.Schema.Customer.Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Response"));
        oper.setReturnClass(Omni.BizTalk.Clicks.Schema.Customer.Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RequestCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Request"), Omni.BizTalk.Clicks.Schema.Customer.Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Response"));
        oper.setReturnClass(Omni.BizTalk.Clicks.Schema.Customer.Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

    }

    public BasicHttpBinding_ITwoWayAsyncStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public BasicHttpBinding_ITwoWayAsyncStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public BasicHttpBinding_ITwoWayAsyncStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent>Children>Child>Gender");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>CBRStatement>Accounts>Account>Transactions>Transaction");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent>Children>Child");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>OptIns>MarketingConsent>Consent>Comms>Type");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsentCommsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PartnerPoints>Partner");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPartnerPointsPartner.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PointsBuckets>Bucket");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPointsBucketsBucket.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>Transactions>Transaction");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent>Comms>Type");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>CBRStatement>Accounts>Account>Transactions");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>CBRStatement>Accounts>Account>Transactions>Transaction");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Transaction");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent>Children");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent>Children>Child");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Child");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyPregnant>Gender");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>OptIns>FinancialServices>Benefit>Status");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefitStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>OptIns>MarketingConsent>Consent>Comms");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsentCommsType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>OptIns>MarketingConsent>Consent>Comms>Type");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Type");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PartnerPoints");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPartnerPointsPartner[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PartnerPoints>Partner");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Partner");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PointsBuckets");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPointsBucketsBucket[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PointsBuckets>Bucket");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Bucket");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>PointsStatement>Accounts>Account>Transactions");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>Transactions>Transaction");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Transaction");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent>Comms");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent>Comms>Type");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Type");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Pharmacy>Scripts>Script>ScriptImages>ScriptImage");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScriptScriptImagesScriptImage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>AddressLine1");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>AddressLine2");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>AddressTypeId");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>City");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>Country");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>PostalCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>Province");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>Suburb");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>CBRStatement>Accounts>Account");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>Coupons>Coupon>Description");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>Coupons>Coupon>Type");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCouponsCouponType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>BabyClub>AlreadyPregnant");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>FinancialServices>Benefit");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefit.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>MarketingConsent>Consent");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>PointsStatement>Accounts>Account");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>Segments>Segment>SegmentTypeId");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegmentSegmentTypeId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>Segments>Segment>Status");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegmentStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>NonLoyalty>Segments>Segment>Status");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegmentStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Pharmacy>Scripts>Script>ScriptImages");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScriptScriptImagesScriptImage[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Pharmacy>Scripts>Script>ScriptImages>ScriptImage");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "ScriptImage");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>Addresses>Address");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>ContactDetails>ContactDetail");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>CBRStatement>Accounts");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccount[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>CBRStatement>Accounts>Account");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Account");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>Coupons>Coupon");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCouponsCoupon.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>Magazine>Status");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyMagazineStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>OptIns>BabyClub");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>OptIns>FinancialServices");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefit[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>FinancialServices>Benefit");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Benefit");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>OptIns>MarketingConsent");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>MarketingConsent>Consent");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Consent");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>PointsStatement>Accounts");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccount[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>PointsStatement>Accounts>Account");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Account");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>Segments>Segment");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>NonLoyalty>OptIns>MarketingConsent");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Consent");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>NonLoyalty>Segments>Segment");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Pharmacy>Scripts>Script");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScript.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Legend>MemberAccounts>MemberAccount>AccountName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Legend>MemberAccounts>MemberAccount>BalanceType");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccountBalanceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Legend>Segments>Segment>SegmentDescription");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>Addresses");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>Addresses>Address");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Address");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>ContactDetails");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>ContactDetails>ContactDetail");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "ContactDetail");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>EMailAddress");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>FirstName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>Gender");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>IDNumber");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>LastName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>PreferedName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>PrimaryMemberIDNumber");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>CBRStatement");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>Coupons");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCoupons.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>HouseholdId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>Magazine");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyMagazine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>MemberId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>OptIns");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>PointsStatement");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>Segments");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>Segments>Segment");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>NonLoyalty>OptIns");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>NonLoyalty>Segments");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>NonLoyalty>Segments>Segment");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Pharmacy>Allergies");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Pharmacy>Scripts");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScript[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Pharmacy>Scripts>Script");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Script");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>MemberAccounts>MemberAccount");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>Segments>Segment");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.LegendSegmentsSegment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>PublishCustomer>Body");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.PublishCustomerBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>PublishCustomer>Header");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.PublishCustomerHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Request>Body");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.RequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Request>Header");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.RequestHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Response>Body");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.ResponseBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Response>Header");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.ResponseHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Action");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>CoreDetails");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Hybris");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Loyalty");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>NonLoyalty");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Pharmacy");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Legend>MemberAccounts");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccount[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>MemberAccounts>MemberAccount");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "MemberAccount");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Legend>Segments");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.LegendSegmentsSegment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>Segments>Segment");
            qName2 = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">PublishCustomer");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.PublishCustomer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Request");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Response");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Customer");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.Customer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Legend");
            cachedSerQNames.add(qName);
            cls = Omni.BizTalk.Clicks.Schema.Customer.Legend.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/Message", "MessageBody");
            cachedSerQNames.add(qName);
            cls = com.microsoft.schemas.Message.MessageBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response enrolCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("EnrolCustomer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "EnrolCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {part});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) org.apache.axis.utils.JavaUtils.convert(_resp, Omni.BizTalk.Clicks.Schema.Customer.Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response registerCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("RegisterCustomer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RegisterCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {part});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) org.apache.axis.utils.JavaUtils.convert(_resp, Omni.BizTalk.Clicks.Schema.Customer.Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response updateCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("UpdateCustomer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {part});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) org.apache.axis.utils.JavaUtils.convert(_resp, Omni.BizTalk.Clicks.Schema.Customer.Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Response requestCustomer(Omni.BizTalk.Clicks.Schema.Customer.Request part) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("RequestCustomer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RequestCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {part});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (Omni.BizTalk.Clicks.Schema.Customer.Response) org.apache.axis.utils.JavaUtils.convert(_resp, Omni.BizTalk.Clicks.Schema.Customer.Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
