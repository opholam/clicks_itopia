/**
 * CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction  implements java.io.Serializable {
    private java.util.Date date;  // attribute

    private java.lang.String location;  // attribute

    private java.math.BigDecimal spent;  // attribute

    private int earned;  // attribute

    public CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction() {
    }

    public CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction(
           java.util.Date date,
           java.lang.String location,
           java.math.BigDecimal spent,
           int earned) {
           this.date = date;
           this.location = location;
           this.spent = spent;
           this.earned = earned;
    }


    /**
     * Gets the date value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @return date
     */
    public java.util.Date getDate() {
        return date;
    }


    /**
     * Sets the date value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @param date
     */
    public void setDate(java.util.Date date) {
        this.date = date;
    }


    /**
     * Gets the location value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }


    /**
     * Gets the spent value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @return spent
     */
    public java.math.BigDecimal getSpent() {
        return spent;
    }


    /**
     * Sets the spent value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @param spent
     */
    public void setSpent(java.math.BigDecimal spent) {
        this.spent = spent;
    }


    /**
     * Gets the earned value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @return earned
     */
    public int getEarned() {
        return earned;
    }


    /**
     * Sets the earned value for this CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.
     * 
     * @param earned
     */
    public void setEarned(int earned) {
        this.earned = earned;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction)) return false;
        CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction other = (CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.spent==null && other.getSpent()==null) || 
             (this.spent!=null &&
              this.spent.equals(other.getSpent()))) &&
            this.earned == other.getEarned();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getSpent() != null) {
            _hashCode += getSpent().hashCode();
        }
        _hashCode += getEarned();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>Transactions>Transaction"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("date");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Date"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("location");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Location"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("spent");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Spent"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("earned");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Earned"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
