/**
 * CustomerLoyaltyOptInsBabyClub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyOptInsBabyClub  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant alreadyPregnant;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent alreadyParent;

    public CustomerLoyaltyOptInsBabyClub() {
    }

    public CustomerLoyaltyOptInsBabyClub(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant alreadyPregnant,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent alreadyParent) {
           this.alreadyPregnant = alreadyPregnant;
           this.alreadyParent = alreadyParent;
    }


    /**
     * Gets the alreadyPregnant value for this CustomerLoyaltyOptInsBabyClub.
     * 
     * @return alreadyPregnant
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant getAlreadyPregnant() {
        return alreadyPregnant;
    }


    /**
     * Sets the alreadyPregnant value for this CustomerLoyaltyOptInsBabyClub.
     * 
     * @param alreadyPregnant
     */
    public void setAlreadyPregnant(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnant alreadyPregnant) {
        this.alreadyPregnant = alreadyPregnant;
    }


    /**
     * Gets the alreadyParent value for this CustomerLoyaltyOptInsBabyClub.
     * 
     * @return alreadyParent
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent getAlreadyParent() {
        return alreadyParent;
    }


    /**
     * Sets the alreadyParent value for this CustomerLoyaltyOptInsBabyClub.
     * 
     * @param alreadyParent
     */
    public void setAlreadyParent(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParent alreadyParent) {
        this.alreadyParent = alreadyParent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyOptInsBabyClub)) return false;
        CustomerLoyaltyOptInsBabyClub other = (CustomerLoyaltyOptInsBabyClub) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.alreadyPregnant==null && other.getAlreadyPregnant()==null) || 
             (this.alreadyPregnant!=null &&
              this.alreadyPregnant.equals(other.getAlreadyPregnant()))) &&
            ((this.alreadyParent==null && other.getAlreadyParent()==null) || 
             (this.alreadyParent!=null &&
              this.alreadyParent.equals(other.getAlreadyParent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAlreadyPregnant() != null) {
            _hashCode += getAlreadyPregnant().hashCode();
        }
        if (getAlreadyParent() != null) {
            _hashCode += getAlreadyParent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyOptInsBabyClub.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>OptIns>BabyClub"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alreadyPregnant");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "AlreadyPregnant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>BabyClub>AlreadyPregnant"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alreadyParent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "AlreadyParent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
