/**
 * CustomerNonLoyaltyOptInsMarketingConsentConsent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerNonLoyaltyOptInsMarketingConsentConsent  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[] comms;

    private java.lang.String consentType;  // attribute

    public CustomerNonLoyaltyOptInsMarketingConsentConsent() {
    }

    public CustomerNonLoyaltyOptInsMarketingConsentConsent(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[] comms,
           java.lang.String consentType) {
           this.comms = comms;
           this.consentType = consentType;
    }


    /**
     * Gets the comms value for this CustomerNonLoyaltyOptInsMarketingConsentConsent.
     * 
     * @return comms
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[] getComms() {
        return comms;
    }


    /**
     * Sets the comms value for this CustomerNonLoyaltyOptInsMarketingConsentConsent.
     * 
     * @param comms
     */
    public void setComms(Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsentCommsType[] comms) {
        this.comms = comms;
    }


    /**
     * Gets the consentType value for this CustomerNonLoyaltyOptInsMarketingConsentConsent.
     * 
     * @return consentType
     */
    public java.lang.String getConsentType() {
        return consentType;
    }


    /**
     * Sets the consentType value for this CustomerNonLoyaltyOptInsMarketingConsentConsent.
     * 
     * @param consentType
     */
    public void setConsentType(java.lang.String consentType) {
        this.consentType = consentType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerNonLoyaltyOptInsMarketingConsentConsent)) return false;
        CustomerNonLoyaltyOptInsMarketingConsentConsent other = (CustomerNonLoyaltyOptInsMarketingConsentConsent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.comms==null && other.getComms()==null) || 
             (this.comms!=null &&
              java.util.Arrays.equals(this.comms, other.getComms()))) &&
            ((this.consentType==null && other.getConsentType()==null) || 
             (this.consentType!=null &&
              this.consentType.equals(other.getConsentType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getComms() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getComms());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getComms(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getConsentType() != null) {
            _hashCode += getConsentType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerNonLoyaltyOptInsMarketingConsentConsent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("consentType");
        attrField.setXmlName(new javax.xml.namespace.QName("", "ConsentType"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comms");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Comms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent>Comms>Type"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Type"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
