/**
 * CustomerLoyaltyOptIns.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyOptIns  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent[] marketingConsent;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub babyClub;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefit[] financialServices;

    public CustomerLoyaltyOptIns() {
    }

    public CustomerLoyaltyOptIns(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent[] marketingConsent,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub babyClub,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefit[] financialServices) {
           this.marketingConsent = marketingConsent;
           this.babyClub = babyClub;
           this.financialServices = financialServices;
    }


    /**
     * Gets the marketingConsent value for this CustomerLoyaltyOptIns.
     * 
     * @return marketingConsent
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent[] getMarketingConsent() {
        return marketingConsent;
    }


    /**
     * Sets the marketingConsent value for this CustomerLoyaltyOptIns.
     * 
     * @param marketingConsent
     */
    public void setMarketingConsent(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsMarketingConsentConsent[] marketingConsent) {
        this.marketingConsent = marketingConsent;
    }


    /**
     * Gets the babyClub value for this CustomerLoyaltyOptIns.
     * 
     * @return babyClub
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub getBabyClub() {
        return babyClub;
    }


    /**
     * Sets the babyClub value for this CustomerLoyaltyOptIns.
     * 
     * @param babyClub
     */
    public void setBabyClub(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClub babyClub) {
        this.babyClub = babyClub;
    }


    /**
     * Gets the financialServices value for this CustomerLoyaltyOptIns.
     * 
     * @return financialServices
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefit[] getFinancialServices() {
        return financialServices;
    }


    /**
     * Sets the financialServices value for this CustomerLoyaltyOptIns.
     * 
     * @param financialServices
     */
    public void setFinancialServices(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefit[] financialServices) {
        this.financialServices = financialServices;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyOptIns)) return false;
        CustomerLoyaltyOptIns other = (CustomerLoyaltyOptIns) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.marketingConsent==null && other.getMarketingConsent()==null) || 
             (this.marketingConsent!=null &&
              java.util.Arrays.equals(this.marketingConsent, other.getMarketingConsent()))) &&
            ((this.babyClub==null && other.getBabyClub()==null) || 
             (this.babyClub!=null &&
              this.babyClub.equals(other.getBabyClub()))) &&
            ((this.financialServices==null && other.getFinancialServices()==null) || 
             (this.financialServices!=null &&
              java.util.Arrays.equals(this.financialServices, other.getFinancialServices())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMarketingConsent() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMarketingConsent());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMarketingConsent(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBabyClub() != null) {
            _hashCode += getBabyClub().hashCode();
        }
        if (getFinancialServices() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFinancialServices());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFinancialServices(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyOptIns.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>OptIns"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingConsent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "MarketingConsent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>MarketingConsent>Consent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Consent"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("babyClub");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "BabyClub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>OptIns>BabyClub"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("financialServices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "FinancialServices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>FinancialServices>Benefit"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Benefit"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
