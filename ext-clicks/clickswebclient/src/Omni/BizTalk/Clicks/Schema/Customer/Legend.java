/**
 * Legend.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class Legend  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.LegendSegmentsSegment[] segments;

    private Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccount[] memberAccounts;

    public Legend() {
    }

    public Legend(
           Omni.BizTalk.Clicks.Schema.Customer.LegendSegmentsSegment[] segments,
           Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccount[] memberAccounts) {
           this.segments = segments;
           this.memberAccounts = memberAccounts;
    }


    /**
     * Gets the segments value for this Legend.
     * 
     * @return segments
     */
    public Omni.BizTalk.Clicks.Schema.Customer.LegendSegmentsSegment[] getSegments() {
        return segments;
    }


    /**
     * Sets the segments value for this Legend.
     * 
     * @param segments
     */
    public void setSegments(Omni.BizTalk.Clicks.Schema.Customer.LegendSegmentsSegment[] segments) {
        this.segments = segments;
    }


    /**
     * Gets the memberAccounts value for this Legend.
     * 
     * @return memberAccounts
     */
    public Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccount[] getMemberAccounts() {
        return memberAccounts;
    }


    /**
     * Sets the memberAccounts value for this Legend.
     * 
     * @param memberAccounts
     */
    public void setMemberAccounts(Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccount[] memberAccounts) {
        this.memberAccounts = memberAccounts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Legend)) return false;
        Legend other = (Legend) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segments==null && other.getSegments()==null) || 
             (this.segments!=null &&
              java.util.Arrays.equals(this.segments, other.getSegments()))) &&
            ((this.memberAccounts==null && other.getMemberAccounts()==null) || 
             (this.memberAccounts!=null &&
              java.util.Arrays.equals(this.memberAccounts, other.getMemberAccounts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMemberAccounts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMemberAccounts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMemberAccounts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Legend.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Legend"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>Segments>Segment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("memberAccounts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "MemberAccounts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>MemberAccounts>MemberAccount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "MemberAccount"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
