/**
 * Customer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class Customer  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails coreDetails;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris hybris;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty nonLoyalty;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty loyalty;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacy pharmacy;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerAction action;  // attribute

    public Customer() {
    }

    public Customer(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails coreDetails,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris hybris,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty nonLoyalty,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty loyalty,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacy pharmacy,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerAction action) {
           this.coreDetails = coreDetails;
           this.hybris = hybris;
           this.nonLoyalty = nonLoyalty;
           this.loyalty = loyalty;
           this.pharmacy = pharmacy;
           this.action = action;
    }


    /**
     * Gets the coreDetails value for this Customer.
     * 
     * @return coreDetails
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails getCoreDetails() {
        return coreDetails;
    }


    /**
     * Sets the coreDetails value for this Customer.
     * 
     * @param coreDetails
     */
    public void setCoreDetails(Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetails coreDetails) {
        this.coreDetails = coreDetails;
    }


    /**
     * Gets the hybris value for this Customer.
     * 
     * @return hybris
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris getHybris() {
        return hybris;
    }


    /**
     * Sets the hybris value for this Customer.
     * 
     * @param hybris
     */
    public void setHybris(Omni.BizTalk.Clicks.Schema.Customer.CustomerHybris hybris) {
        this.hybris = hybris;
    }


    /**
     * Gets the nonLoyalty value for this Customer.
     * 
     * @return nonLoyalty
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty getNonLoyalty() {
        return nonLoyalty;
    }


    /**
     * Sets the nonLoyalty value for this Customer.
     * 
     * @param nonLoyalty
     */
    public void setNonLoyalty(Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyalty nonLoyalty) {
        this.nonLoyalty = nonLoyalty;
    }


    /**
     * Gets the loyalty value for this Customer.
     * 
     * @return loyalty
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty getLoyalty() {
        return loyalty;
    }


    /**
     * Sets the loyalty value for this Customer.
     * 
     * @param loyalty
     */
    public void setLoyalty(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyalty loyalty) {
        this.loyalty = loyalty;
    }


    /**
     * Gets the pharmacy value for this Customer.
     * 
     * @return pharmacy
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacy getPharmacy() {
        return pharmacy;
    }


    /**
     * Sets the pharmacy value for this Customer.
     * 
     * @param pharmacy
     */
    public void setPharmacy(Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacy pharmacy) {
        this.pharmacy = pharmacy;
    }


    /**
     * Gets the action value for this Customer.
     * 
     * @return action
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerAction getAction() {
        return action;
    }


    /**
     * Sets the action value for this Customer.
     * 
     * @param action
     */
    public void setAction(Omni.BizTalk.Clicks.Schema.Customer.CustomerAction action) {
        this.action = action;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Customer)) return false;
        Customer other = (Customer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.coreDetails==null && other.getCoreDetails()==null) || 
             (this.coreDetails!=null &&
              this.coreDetails.equals(other.getCoreDetails()))) &&
            ((this.hybris==null && other.getHybris()==null) || 
             (this.hybris!=null &&
              this.hybris.equals(other.getHybris()))) &&
            ((this.nonLoyalty==null && other.getNonLoyalty()==null) || 
             (this.nonLoyalty!=null &&
              this.nonLoyalty.equals(other.getNonLoyalty()))) &&
            ((this.loyalty==null && other.getLoyalty()==null) || 
             (this.loyalty!=null &&
              this.loyalty.equals(other.getLoyalty()))) &&
            ((this.pharmacy==null && other.getPharmacy()==null) || 
             (this.pharmacy!=null &&
              this.pharmacy.equals(other.getPharmacy()))) &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCoreDetails() != null) {
            _hashCode += getCoreDetails().hashCode();
        }
        if (getHybris() != null) {
            _hashCode += getHybris().hashCode();
        }
        if (getNonLoyalty() != null) {
            _hashCode += getNonLoyalty().hashCode();
        }
        if (getLoyalty() != null) {
            _hashCode += getLoyalty().hashCode();
        }
        if (getPharmacy() != null) {
            _hashCode += getPharmacy().hashCode();
        }
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Customer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Customer"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("action");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Action"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Action"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coreDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "CoreDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>CoreDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hybris");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Hybris"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Hybris"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonLoyalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "NonLoyalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>NonLoyalty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Loyalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Loyalty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmacy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Pharmacy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Pharmacy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
