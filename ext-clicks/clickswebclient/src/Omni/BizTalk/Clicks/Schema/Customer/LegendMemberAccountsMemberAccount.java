/**
 * LegendMemberAccountsMemberAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class LegendMemberAccountsMemberAccount  implements java.io.Serializable {
    private int accountId;  // attribute

    private java.lang.String accountName;  // attribute

    private Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccountBalanceType balanceType;  // attribute

    private java.lang.String currency;  // attribute

    public LegendMemberAccountsMemberAccount() {
    }

    public LegendMemberAccountsMemberAccount(
           int accountId,
           java.lang.String accountName,
           Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccountBalanceType balanceType,
           java.lang.String currency) {
           this.accountId = accountId;
           this.accountName = accountName;
           this.balanceType = balanceType;
           this.currency = currency;
    }


    /**
     * Gets the accountId value for this LegendMemberAccountsMemberAccount.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this LegendMemberAccountsMemberAccount.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the accountName value for this LegendMemberAccountsMemberAccount.
     * 
     * @return accountName
     */
    public java.lang.String getAccountName() {
        return accountName;
    }


    /**
     * Sets the accountName value for this LegendMemberAccountsMemberAccount.
     * 
     * @param accountName
     */
    public void setAccountName(java.lang.String accountName) {
        this.accountName = accountName;
    }


    /**
     * Gets the balanceType value for this LegendMemberAccountsMemberAccount.
     * 
     * @return balanceType
     */
    public Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccountBalanceType getBalanceType() {
        return balanceType;
    }


    /**
     * Sets the balanceType value for this LegendMemberAccountsMemberAccount.
     * 
     * @param balanceType
     */
    public void setBalanceType(Omni.BizTalk.Clicks.Schema.Customer.LegendMemberAccountsMemberAccountBalanceType balanceType) {
        this.balanceType = balanceType;
    }


    /**
     * Gets the currency value for this LegendMemberAccountsMemberAccount.
     * 
     * @return currency
     */
    public java.lang.String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this LegendMemberAccountsMemberAccount.
     * 
     * @param currency
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LegendMemberAccountsMemberAccount)) return false;
        LegendMemberAccountsMemberAccount other = (LegendMemberAccountsMemberAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.accountId == other.getAccountId() &&
            ((this.accountName==null && other.getAccountName()==null) || 
             (this.accountName!=null &&
              this.accountName.equals(other.getAccountName()))) &&
            ((this.balanceType==null && other.getBalanceType()==null) || 
             (this.balanceType!=null &&
              this.balanceType.equals(other.getBalanceType()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAccountId();
        if (getAccountName() != null) {
            _hashCode += getAccountName().hashCode();
        }
        if (getBalanceType() != null) {
            _hashCode += getBalanceType().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LegendMemberAccountsMemberAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Legend>MemberAccounts>MemberAccount"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("accountId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AccountId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("accountName");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AccountName"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Legend>MemberAccounts>MemberAccount>AccountName"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("balanceType");
        attrField.setXmlName(new javax.xml.namespace.QName("", "BalanceType"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Legend>MemberAccounts>MemberAccount>BalanceType"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("currency");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Currency"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
