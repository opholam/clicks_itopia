/**
 * CustomerNonLoyalty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerNonLoyalty  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment[] segments;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns optIns;

    private java.lang.String retailerId;  // attribute

    public CustomerNonLoyalty() {
    }

    public CustomerNonLoyalty(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment[] segments,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns optIns,
           java.lang.String retailerId) {
           this.segments = segments;
           this.optIns = optIns;
           this.retailerId = retailerId;
    }


    /**
     * Gets the segments value for this CustomerNonLoyalty.
     * 
     * @return segments
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment[] getSegments() {
        return segments;
    }


    /**
     * Sets the segments value for this CustomerNonLoyalty.
     * 
     * @param segments
     */
    public void setSegments(Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegment[] segments) {
        this.segments = segments;
    }


    /**
     * Gets the optIns value for this CustomerNonLoyalty.
     * 
     * @return optIns
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns getOptIns() {
        return optIns;
    }


    /**
     * Sets the optIns value for this CustomerNonLoyalty.
     * 
     * @param optIns
     */
    public void setOptIns(Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptIns optIns) {
        this.optIns = optIns;
    }


    /**
     * Gets the retailerId value for this CustomerNonLoyalty.
     * 
     * @return retailerId
     */
    public java.lang.String getRetailerId() {
        return retailerId;
    }


    /**
     * Sets the retailerId value for this CustomerNonLoyalty.
     * 
     * @param retailerId
     */
    public void setRetailerId(java.lang.String retailerId) {
        this.retailerId = retailerId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerNonLoyalty)) return false;
        CustomerNonLoyalty other = (CustomerNonLoyalty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.segments==null && other.getSegments()==null) || 
             (this.segments!=null &&
              java.util.Arrays.equals(this.segments, other.getSegments()))) &&
            ((this.optIns==null && other.getOptIns()==null) || 
             (this.optIns!=null &&
              this.optIns.equals(other.getOptIns()))) &&
            ((this.retailerId==null && other.getRetailerId()==null) || 
             (this.retailerId!=null &&
              this.retailerId.equals(other.getRetailerId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSegments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOptIns() != null) {
            _hashCode += getOptIns().hashCode();
        }
        if (getRetailerId() != null) {
            _hashCode += getRetailerId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerNonLoyalty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>NonLoyalty"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("retailerId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "RetailerId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>NonLoyalty>Segments>Segment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optIns");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "OptIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>NonLoyalty>OptIns"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
