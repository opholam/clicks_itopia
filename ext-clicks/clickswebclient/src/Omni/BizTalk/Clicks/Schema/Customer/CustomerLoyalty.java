/**
 * CustomerLoyalty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyalty  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns optIns;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyMagazine magazine;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment[] segments;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatement CBRStatement;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatement pointsStatement;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCoupons coupons;

    private java.lang.String retailerId;  // attribute

    private java.lang.String isMainMember;  // attribute

    private java.lang.String householdId;  // attribute

    private java.lang.String memberId;  // attribute

    public CustomerLoyalty() {
    }

    public CustomerLoyalty(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns optIns,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyMagazine magazine,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment[] segments,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatement CBRStatement,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatement pointsStatement,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCoupons coupons,
           java.lang.String retailerId,
           java.lang.String isMainMember,
           java.lang.String householdId,
           java.lang.String memberId) {
           this.optIns = optIns;
           this.magazine = magazine;
           this.segments = segments;
           this.CBRStatement = CBRStatement;
           this.pointsStatement = pointsStatement;
           this.coupons = coupons;
           this.retailerId = retailerId;
           this.isMainMember = isMainMember;
           this.householdId = householdId;
           this.memberId = memberId;
    }


    /**
     * Gets the optIns value for this CustomerLoyalty.
     * 
     * @return optIns
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns getOptIns() {
        return optIns;
    }


    /**
     * Sets the optIns value for this CustomerLoyalty.
     * 
     * @param optIns
     */
    public void setOptIns(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptIns optIns) {
        this.optIns = optIns;
    }


    /**
     * Gets the magazine value for this CustomerLoyalty.
     * 
     * @return magazine
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyMagazine getMagazine() {
        return magazine;
    }


    /**
     * Sets the magazine value for this CustomerLoyalty.
     * 
     * @param magazine
     */
    public void setMagazine(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyMagazine magazine) {
        this.magazine = magazine;
    }


    /**
     * Gets the segments value for this CustomerLoyalty.
     * 
     * @return segments
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment[] getSegments() {
        return segments;
    }


    /**
     * Sets the segments value for this CustomerLoyalty.
     * 
     * @param segments
     */
    public void setSegments(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltySegmentsSegment[] segments) {
        this.segments = segments;
    }


    /**
     * Gets the CBRStatement value for this CustomerLoyalty.
     * 
     * @return CBRStatement
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatement getCBRStatement() {
        return CBRStatement;
    }


    /**
     * Sets the CBRStatement value for this CustomerLoyalty.
     * 
     * @param CBRStatement
     */
    public void setCBRStatement(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatement CBRStatement) {
        this.CBRStatement = CBRStatement;
    }


    /**
     * Gets the pointsStatement value for this CustomerLoyalty.
     * 
     * @return pointsStatement
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatement getPointsStatement() {
        return pointsStatement;
    }


    /**
     * Sets the pointsStatement value for this CustomerLoyalty.
     * 
     * @param pointsStatement
     */
    public void setPointsStatement(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatement pointsStatement) {
        this.pointsStatement = pointsStatement;
    }


    /**
     * Gets the coupons value for this CustomerLoyalty.
     * 
     * @return coupons
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCoupons getCoupons() {
        return coupons;
    }


    /**
     * Sets the coupons value for this CustomerLoyalty.
     * 
     * @param coupons
     */
    public void setCoupons(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCoupons coupons) {
        this.coupons = coupons;
    }


    /**
     * Gets the retailerId value for this CustomerLoyalty.
     * 
     * @return retailerId
     */
    public java.lang.String getRetailerId() {
        return retailerId;
    }


    /**
     * Sets the retailerId value for this CustomerLoyalty.
     * 
     * @param retailerId
     */
    public void setRetailerId(java.lang.String retailerId) {
        this.retailerId = retailerId;
    }


    /**
     * Gets the isMainMember value for this CustomerLoyalty.
     * 
     * @return isMainMember
     */
    public java.lang.String getIsMainMember() {
        return isMainMember;
    }


    /**
     * Sets the isMainMember value for this CustomerLoyalty.
     * 
     * @param isMainMember
     */
    public void setIsMainMember(java.lang.String isMainMember) {
        this.isMainMember = isMainMember;
    }


    /**
     * Gets the householdId value for this CustomerLoyalty.
     * 
     * @return householdId
     */
    public java.lang.String getHouseholdId() {
        return householdId;
    }


    /**
     * Sets the householdId value for this CustomerLoyalty.
     * 
     * @param householdId
     */
    public void setHouseholdId(java.lang.String householdId) {
        this.householdId = householdId;
    }


    /**
     * Gets the memberId value for this CustomerLoyalty.
     * 
     * @return memberId
     */
    public java.lang.String getMemberId() {
        return memberId;
    }


    /**
     * Sets the memberId value for this CustomerLoyalty.
     * 
     * @param memberId
     */
    public void setMemberId(java.lang.String memberId) {
        this.memberId = memberId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyalty)) return false;
        CustomerLoyalty other = (CustomerLoyalty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.optIns==null && other.getOptIns()==null) || 
             (this.optIns!=null &&
              this.optIns.equals(other.getOptIns()))) &&
            ((this.magazine==null && other.getMagazine()==null) || 
             (this.magazine!=null &&
              this.magazine.equals(other.getMagazine()))) &&
            ((this.segments==null && other.getSegments()==null) || 
             (this.segments!=null &&
              java.util.Arrays.equals(this.segments, other.getSegments()))) &&
            ((this.CBRStatement==null && other.getCBRStatement()==null) || 
             (this.CBRStatement!=null &&
              this.CBRStatement.equals(other.getCBRStatement()))) &&
            ((this.pointsStatement==null && other.getPointsStatement()==null) || 
             (this.pointsStatement!=null &&
              this.pointsStatement.equals(other.getPointsStatement()))) &&
            ((this.coupons==null && other.getCoupons()==null) || 
             (this.coupons!=null &&
              this.coupons.equals(other.getCoupons()))) &&
            ((this.retailerId==null && other.getRetailerId()==null) || 
             (this.retailerId!=null &&
              this.retailerId.equals(other.getRetailerId()))) &&
            ((this.isMainMember==null && other.getIsMainMember()==null) || 
             (this.isMainMember!=null &&
              this.isMainMember.equals(other.getIsMainMember()))) &&
            ((this.householdId==null && other.getHouseholdId()==null) || 
             (this.householdId!=null &&
              this.householdId.equals(other.getHouseholdId()))) &&
            ((this.memberId==null && other.getMemberId()==null) || 
             (this.memberId!=null &&
              this.memberId.equals(other.getMemberId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOptIns() != null) {
            _hashCode += getOptIns().hashCode();
        }
        if (getMagazine() != null) {
            _hashCode += getMagazine().hashCode();
        }
        if (getSegments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSegments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSegments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCBRStatement() != null) {
            _hashCode += getCBRStatement().hashCode();
        }
        if (getPointsStatement() != null) {
            _hashCode += getPointsStatement().hashCode();
        }
        if (getCoupons() != null) {
            _hashCode += getCoupons().hashCode();
        }
        if (getRetailerId() != null) {
            _hashCode += getRetailerId().hashCode();
        }
        if (getIsMainMember() != null) {
            _hashCode += getIsMainMember().hashCode();
        }
        if (getHouseholdId() != null) {
            _hashCode += getHouseholdId().hashCode();
        }
        if (getMemberId() != null) {
            _hashCode += getMemberId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyalty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Loyalty"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("retailerId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "RetailerId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("isMainMember");
        attrField.setXmlName(new javax.xml.namespace.QName("", "IsMainMember"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("householdId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "HouseholdId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>HouseholdId"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("memberId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MemberId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>MemberId"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optIns");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "OptIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>OptIns"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("magazine");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Magazine"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>Magazine"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Loyalty>Segments>Segment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Segment"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CBRStatement");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "CBRStatement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>CBRStatement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointsStatement");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "PointsStatement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>PointsStatement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coupons");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Coupons"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Loyalty>Coupons"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
