/**
 * CustomerLoyaltyOptInsBabyClubAlreadyPregnant.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyOptInsBabyClubAlreadyPregnant  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender gender;  // attribute

    private java.lang.String dueDate;  // attribute

    public CustomerLoyaltyOptInsBabyClubAlreadyPregnant() {
    }

    public CustomerLoyaltyOptInsBabyClubAlreadyPregnant(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender gender,
           java.lang.String dueDate) {
           this.gender = gender;
           this.dueDate = dueDate;
    }


    /**
     * Gets the gender value for this CustomerLoyaltyOptInsBabyClubAlreadyPregnant.
     * 
     * @return gender
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this CustomerLoyaltyOptInsBabyClubAlreadyPregnant.
     * 
     * @param gender
     */
    public void setGender(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyPregnantGender gender) {
        this.gender = gender;
    }


    /**
     * Gets the dueDate value for this CustomerLoyaltyOptInsBabyClubAlreadyPregnant.
     * 
     * @return dueDate
     */
    public java.lang.String getDueDate() {
        return dueDate;
    }


    /**
     * Sets the dueDate value for this CustomerLoyaltyOptInsBabyClubAlreadyPregnant.
     * 
     * @param dueDate
     */
    public void setDueDate(java.lang.String dueDate) {
        this.dueDate = dueDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyOptInsBabyClubAlreadyPregnant)) return false;
        CustomerLoyaltyOptInsBabyClubAlreadyPregnant other = (CustomerLoyaltyOptInsBabyClubAlreadyPregnant) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.dueDate==null && other.getDueDate()==null) || 
             (this.dueDate!=null &&
              this.dueDate.equals(other.getDueDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getDueDate() != null) {
            _hashCode += getDueDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyOptInsBabyClubAlreadyPregnant.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>BabyClub>AlreadyPregnant"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("gender");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Gender"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyPregnant>Gender"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("dueDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DueDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
