/**
 * CustomerCoreDetailsAddressesAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerCoreDetailsAddressesAddress  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId addressTypeId;  // attribute

    private java.lang.String addressLine1;  // attribute

    private java.lang.String addressLine2;  // attribute

    private java.lang.String suburb;  // attribute

    private java.lang.String city;  // attribute

    private java.lang.String province;  // attribute

    private java.lang.String country;  // attribute

    private java.lang.String postalCode;  // attribute

    public CustomerCoreDetailsAddressesAddress() {
    }

    public CustomerCoreDetailsAddressesAddress(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId addressTypeId,
           java.lang.String addressLine1,
           java.lang.String addressLine2,
           java.lang.String suburb,
           java.lang.String city,
           java.lang.String province,
           java.lang.String country,
           java.lang.String postalCode) {
           this.addressTypeId = addressTypeId;
           this.addressLine1 = addressLine1;
           this.addressLine2 = addressLine2;
           this.suburb = suburb;
           this.city = city;
           this.province = province;
           this.country = country;
           this.postalCode = postalCode;
    }


    /**
     * Gets the addressTypeId value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return addressTypeId
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId getAddressTypeId() {
        return addressTypeId;
    }


    /**
     * Sets the addressTypeId value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param addressTypeId
     */
    public void setAddressTypeId(Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddressAddressTypeId addressTypeId) {
        this.addressTypeId = addressTypeId;
    }


    /**
     * Gets the addressLine1 value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return addressLine1
     */
    public java.lang.String getAddressLine1() {
        return addressLine1;
    }


    /**
     * Sets the addressLine1 value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param addressLine1
     */
    public void setAddressLine1(java.lang.String addressLine1) {
        this.addressLine1 = addressLine1;
    }


    /**
     * Gets the addressLine2 value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return addressLine2
     */
    public java.lang.String getAddressLine2() {
        return addressLine2;
    }


    /**
     * Sets the addressLine2 value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param addressLine2
     */
    public void setAddressLine2(java.lang.String addressLine2) {
        this.addressLine2 = addressLine2;
    }


    /**
     * Gets the suburb value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return suburb
     */
    public java.lang.String getSuburb() {
        return suburb;
    }


    /**
     * Sets the suburb value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param suburb
     */
    public void setSuburb(java.lang.String suburb) {
        this.suburb = suburb;
    }


    /**
     * Gets the city value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the province value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return province
     */
    public java.lang.String getProvince() {
        return province;
    }


    /**
     * Sets the province value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param province
     */
    public void setProvince(java.lang.String province) {
        this.province = province;
    }


    /**
     * Gets the country value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the postalCode value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @return postalCode
     */
    public java.lang.String getPostalCode() {
        return postalCode;
    }


    /**
     * Sets the postalCode value for this CustomerCoreDetailsAddressesAddress.
     * 
     * @param postalCode
     */
    public void setPostalCode(java.lang.String postalCode) {
        this.postalCode = postalCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerCoreDetailsAddressesAddress)) return false;
        CustomerCoreDetailsAddressesAddress other = (CustomerCoreDetailsAddressesAddress) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addressTypeId==null && other.getAddressTypeId()==null) || 
             (this.addressTypeId!=null &&
              this.addressTypeId.equals(other.getAddressTypeId()))) &&
            ((this.addressLine1==null && other.getAddressLine1()==null) || 
             (this.addressLine1!=null &&
              this.addressLine1.equals(other.getAddressLine1()))) &&
            ((this.addressLine2==null && other.getAddressLine2()==null) || 
             (this.addressLine2!=null &&
              this.addressLine2.equals(other.getAddressLine2()))) &&
            ((this.suburb==null && other.getSuburb()==null) || 
             (this.suburb!=null &&
              this.suburb.equals(other.getSuburb()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.province==null && other.getProvince()==null) || 
             (this.province!=null &&
              this.province.equals(other.getProvince()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.postalCode==null && other.getPostalCode()==null) || 
             (this.postalCode!=null &&
              this.postalCode.equals(other.getPostalCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddressTypeId() != null) {
            _hashCode += getAddressTypeId().hashCode();
        }
        if (getAddressLine1() != null) {
            _hashCode += getAddressLine1().hashCode();
        }
        if (getAddressLine2() != null) {
            _hashCode += getAddressLine2().hashCode();
        }
        if (getSuburb() != null) {
            _hashCode += getSuburb().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getProvince() != null) {
            _hashCode += getProvince().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getPostalCode() != null) {
            _hashCode += getPostalCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerCoreDetailsAddressesAddress.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>Addresses>Address"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("addressTypeId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AddressTypeId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>AddressTypeId"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("addressLine1");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AddressLine1"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>AddressLine1"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("addressLine2");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AddressLine2"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>AddressLine2"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("suburb");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Suburb"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>Suburb"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("city");
        attrField.setXmlName(new javax.xml.namespace.QName("", "City"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>City"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("province");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Province"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>Province"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("country");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Country"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>Country"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("postalCode");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PostalCode"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>CoreDetails>Addresses>Address>PostalCode"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
