/**
 * PublishCustomerBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class PublishCustomerBody  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.Customer[] customer;

    private Omni.BizTalk.Clicks.Schema.Customer.Legend legend;

    public PublishCustomerBody() {
    }

    public PublishCustomerBody(
           Omni.BizTalk.Clicks.Schema.Customer.Customer[] customer,
           Omni.BizTalk.Clicks.Schema.Customer.Legend legend) {
           this.customer = customer;
           this.legend = legend;
    }


    /**
     * Gets the customer value for this PublishCustomerBody.
     * 
     * @return customer
     */
    public Omni.BizTalk.Clicks.Schema.Customer.Customer[] getCustomer() {
        return customer;
    }


    /**
     * Sets the customer value for this PublishCustomerBody.
     * 
     * @param customer
     */
    public void setCustomer(Omni.BizTalk.Clicks.Schema.Customer.Customer[] customer) {
        this.customer = customer;
    }

    public Omni.BizTalk.Clicks.Schema.Customer.Customer getCustomer(int i) {
        return this.customer[i];
    }

    public void setCustomer(int i, Omni.BizTalk.Clicks.Schema.Customer.Customer _value) {
        this.customer[i] = _value;
    }


    /**
     * Gets the legend value for this PublishCustomerBody.
     * 
     * @return legend
     */
    public Omni.BizTalk.Clicks.Schema.Customer.Legend getLegend() {
        return legend;
    }


    /**
     * Sets the legend value for this PublishCustomerBody.
     * 
     * @param legend
     */
    public void setLegend(Omni.BizTalk.Clicks.Schema.Customer.Legend legend) {
        this.legend = legend;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PublishCustomerBody)) return false;
        PublishCustomerBody other = (PublishCustomerBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customer==null && other.getCustomer()==null) || 
             (this.customer!=null &&
              java.util.Arrays.equals(this.customer, other.getCustomer()))) &&
            ((this.legend==null && other.getLegend()==null) || 
             (this.legend!=null &&
              this.legend.equals(other.getLegend())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomer() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomer());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomer(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLegend() != null) {
            _hashCode += getLegend().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PublishCustomerBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>PublishCustomer>Body"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Customer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Customer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legend");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Legend"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Legend"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
