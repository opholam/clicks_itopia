/**
 * CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild  implements java.io.Serializable {
    private java.lang.String firstName;  // attribute

    private java.lang.String surname;  // attribute

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender gender;  // attribute

    private java.lang.String dateOfBirth;  // attribute

    public CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild() {
    }

    public CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild(
           java.lang.String firstName,
           java.lang.String surname,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender gender,
           java.lang.String dateOfBirth) {
           this.firstName = firstName;
           this.surname = surname;
           this.gender = gender;
           this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the firstName value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the surname value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @return surname
     */
    public java.lang.String getSurname() {
        return surname;
    }


    /**
     * Sets the surname value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @param surname
     */
    public void setSurname(java.lang.String surname) {
        this.surname = surname;
    }


    /**
     * Gets the gender value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @return gender
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @param gender
     */
    public void setGender(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChildGender gender) {
        this.gender = gender;
    }


    /**
     * Gets the dateOfBirth value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @return dateOfBirth
     */
    public java.lang.String getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.lang.String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild)) return false;
        CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild other = (CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.surname==null && other.getSurname()==null) || 
             (this.surname!=null &&
              this.surname.equals(other.getSurname()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) || 
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getSurname() != null) {
            _hashCode += getSurname().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyOptInsBabyClubAlreadyParentChildrenChild.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent>Children>Child"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("firstName");
        attrField.setXmlName(new javax.xml.namespace.QName("", "FirstName"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("surname");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Surname"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("gender");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Gender"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>>Customer>Loyalty>OptIns>BabyClub>AlreadyParent>Children>Child>Gender"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("dateOfBirth");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DateOfBirth"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
