/**
 * CustomerLoyaltyCBRStatementAccountsAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyCBRStatementAccountsAccount  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction[] transactions;

    private int accountId;  // attribute

    private java.math.BigDecimal CBRAvailableBalance;  // attribute

    public CustomerLoyaltyCBRStatementAccountsAccount() {
    }

    public CustomerLoyaltyCBRStatementAccountsAccount(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction[] transactions,
           int accountId,
           java.math.BigDecimal CBRAvailableBalance) {
           this.transactions = transactions;
           this.accountId = accountId;
           this.CBRAvailableBalance = CBRAvailableBalance;
    }


    /**
     * Gets the transactions value for this CustomerLoyaltyCBRStatementAccountsAccount.
     * 
     * @return transactions
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction[] getTransactions() {
        return transactions;
    }


    /**
     * Sets the transactions value for this CustomerLoyaltyCBRStatementAccountsAccount.
     * 
     * @param transactions
     */
    public void setTransactions(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction[] transactions) {
        this.transactions = transactions;
    }


    /**
     * Gets the accountId value for this CustomerLoyaltyCBRStatementAccountsAccount.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this CustomerLoyaltyCBRStatementAccountsAccount.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the CBRAvailableBalance value for this CustomerLoyaltyCBRStatementAccountsAccount.
     * 
     * @return CBRAvailableBalance
     */
    public java.math.BigDecimal getCBRAvailableBalance() {
        return CBRAvailableBalance;
    }


    /**
     * Sets the CBRAvailableBalance value for this CustomerLoyaltyCBRStatementAccountsAccount.
     * 
     * @param CBRAvailableBalance
     */
    public void setCBRAvailableBalance(java.math.BigDecimal CBRAvailableBalance) {
        this.CBRAvailableBalance = CBRAvailableBalance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyCBRStatementAccountsAccount)) return false;
        CustomerLoyaltyCBRStatementAccountsAccount other = (CustomerLoyaltyCBRStatementAccountsAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.transactions==null && other.getTransactions()==null) || 
             (this.transactions!=null &&
              java.util.Arrays.equals(this.transactions, other.getTransactions()))) &&
            this.accountId == other.getAccountId() &&
            ((this.CBRAvailableBalance==null && other.getCBRAvailableBalance()==null) || 
             (this.CBRAvailableBalance!=null &&
              this.CBRAvailableBalance.equals(other.getCBRAvailableBalance())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransactions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransactions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransactions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getAccountId();
        if (getCBRAvailableBalance() != null) {
            _hashCode += getCBRAvailableBalance().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyCBRStatementAccountsAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>CBRStatement>Accounts>Account"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("accountId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AccountId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("CBRAvailableBalance");
        attrField.setXmlName(new javax.xml.namespace.QName("", "CBRAvailableBalance"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Transactions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>CBRStatement>Accounts>Account>Transactions>Transaction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Transaction"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
