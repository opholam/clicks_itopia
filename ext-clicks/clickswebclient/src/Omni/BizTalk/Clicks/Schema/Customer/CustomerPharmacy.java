/**
 * CustomerPharmacy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerPharmacy  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScript[] scripts;

    private java.lang.String marconiId;  // attribute

    private java.lang.String collectionPharmacy;  // attribute

    private java.lang.String medicalSchemeProvider;  // attribute

    private java.lang.String medicalSchemeNumber;  // attribute

    private java.lang.String dependentCode;  // attribute

    private java.lang.String allergies;  // attribute

    private java.lang.String allergyDetails;  // attribute

    private int pharmacyAge;  // attribute

    public CustomerPharmacy() {
    }

    public CustomerPharmacy(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScript[] scripts,
           java.lang.String marconiId,
           java.lang.String collectionPharmacy,
           java.lang.String medicalSchemeProvider,
           java.lang.String medicalSchemeNumber,
           java.lang.String dependentCode,
           java.lang.String allergies,
           java.lang.String allergyDetails,
           int pharmacyAge) {
           this.scripts = scripts;
           this.marconiId = marconiId;
           this.collectionPharmacy = collectionPharmacy;
           this.medicalSchemeProvider = medicalSchemeProvider;
           this.medicalSchemeNumber = medicalSchemeNumber;
           this.dependentCode = dependentCode;
           this.allergies = allergies;
           this.allergyDetails = allergyDetails;
           this.pharmacyAge = pharmacyAge;
    }


    /**
     * Gets the scripts value for this CustomerPharmacy.
     * 
     * @return scripts
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScript[] getScripts() {
        return scripts;
    }


    /**
     * Sets the scripts value for this CustomerPharmacy.
     * 
     * @param scripts
     */
    public void setScripts(Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScript[] scripts) {
        this.scripts = scripts;
    }


    /**
     * Gets the marconiId value for this CustomerPharmacy.
     * 
     * @return marconiId
     */
    public java.lang.String getMarconiId() {
        return marconiId;
    }


    /**
     * Sets the marconiId value for this CustomerPharmacy.
     * 
     * @param marconiId
     */
    public void setMarconiId(java.lang.String marconiId) {
        this.marconiId = marconiId;
    }


    /**
     * Gets the collectionPharmacy value for this CustomerPharmacy.
     * 
     * @return collectionPharmacy
     */
    public java.lang.String getCollectionPharmacy() {
        return collectionPharmacy;
    }


    /**
     * Sets the collectionPharmacy value for this CustomerPharmacy.
     * 
     * @param collectionPharmacy
     */
    public void setCollectionPharmacy(java.lang.String collectionPharmacy) {
        this.collectionPharmacy = collectionPharmacy;
    }


    /**
     * Gets the medicalSchemeProvider value for this CustomerPharmacy.
     * 
     * @return medicalSchemeProvider
     */
    public java.lang.String getMedicalSchemeProvider() {
        return medicalSchemeProvider;
    }


    /**
     * Sets the medicalSchemeProvider value for this CustomerPharmacy.
     * 
     * @param medicalSchemeProvider
     */
    public void setMedicalSchemeProvider(java.lang.String medicalSchemeProvider) {
        this.medicalSchemeProvider = medicalSchemeProvider;
    }


    /**
     * Gets the medicalSchemeNumber value for this CustomerPharmacy.
     * 
     * @return medicalSchemeNumber
     */
    public java.lang.String getMedicalSchemeNumber() {
        return medicalSchemeNumber;
    }


    /**
     * Sets the medicalSchemeNumber value for this CustomerPharmacy.
     * 
     * @param medicalSchemeNumber
     */
    public void setMedicalSchemeNumber(java.lang.String medicalSchemeNumber) {
        this.medicalSchemeNumber = medicalSchemeNumber;
    }


    /**
     * Gets the dependentCode value for this CustomerPharmacy.
     * 
     * @return dependentCode
     */
    public java.lang.String getDependentCode() {
        return dependentCode;
    }


    /**
     * Sets the dependentCode value for this CustomerPharmacy.
     * 
     * @param dependentCode
     */
    public void setDependentCode(java.lang.String dependentCode) {
        this.dependentCode = dependentCode;
    }


    /**
     * Gets the allergies value for this CustomerPharmacy.
     * 
     * @return allergies
     */
    public java.lang.String getAllergies() {
        return allergies;
    }


    /**
     * Sets the allergies value for this CustomerPharmacy.
     * 
     * @param allergies
     */
    public void setAllergies(java.lang.String allergies) {
        this.allergies = allergies;
    }


    /**
     * Gets the allergyDetails value for this CustomerPharmacy.
     * 
     * @return allergyDetails
     */
    public java.lang.String getAllergyDetails() {
        return allergyDetails;
    }


    /**
     * Sets the allergyDetails value for this CustomerPharmacy.
     * 
     * @param allergyDetails
     */
    public void setAllergyDetails(java.lang.String allergyDetails) {
        this.allergyDetails = allergyDetails;
    }


    /**
     * Gets the pharmacyAge value for this CustomerPharmacy.
     * 
     * @return pharmacyAge
     */
    public int getPharmacyAge() {
        return pharmacyAge;
    }


    /**
     * Sets the pharmacyAge value for this CustomerPharmacy.
     * 
     * @param pharmacyAge
     */
    public void setPharmacyAge(int pharmacyAge) {
        this.pharmacyAge = pharmacyAge;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerPharmacy)) return false;
        CustomerPharmacy other = (CustomerPharmacy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.scripts==null && other.getScripts()==null) || 
             (this.scripts!=null &&
              java.util.Arrays.equals(this.scripts, other.getScripts()))) &&
            ((this.marconiId==null && other.getMarconiId()==null) || 
             (this.marconiId!=null &&
              this.marconiId.equals(other.getMarconiId()))) &&
            ((this.collectionPharmacy==null && other.getCollectionPharmacy()==null) || 
             (this.collectionPharmacy!=null &&
              this.collectionPharmacy.equals(other.getCollectionPharmacy()))) &&
            ((this.medicalSchemeProvider==null && other.getMedicalSchemeProvider()==null) || 
             (this.medicalSchemeProvider!=null &&
              this.medicalSchemeProvider.equals(other.getMedicalSchemeProvider()))) &&
            ((this.medicalSchemeNumber==null && other.getMedicalSchemeNumber()==null) || 
             (this.medicalSchemeNumber!=null &&
              this.medicalSchemeNumber.equals(other.getMedicalSchemeNumber()))) &&
            ((this.dependentCode==null && other.getDependentCode()==null) || 
             (this.dependentCode!=null &&
              this.dependentCode.equals(other.getDependentCode()))) &&
            ((this.allergies==null && other.getAllergies()==null) || 
             (this.allergies!=null &&
              this.allergies.equals(other.getAllergies()))) &&
            ((this.allergyDetails==null && other.getAllergyDetails()==null) || 
             (this.allergyDetails!=null &&
              this.allergyDetails.equals(other.getAllergyDetails()))) &&
            this.pharmacyAge == other.getPharmacyAge();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScripts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScripts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScripts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMarconiId() != null) {
            _hashCode += getMarconiId().hashCode();
        }
        if (getCollectionPharmacy() != null) {
            _hashCode += getCollectionPharmacy().hashCode();
        }
        if (getMedicalSchemeProvider() != null) {
            _hashCode += getMedicalSchemeProvider().hashCode();
        }
        if (getMedicalSchemeNumber() != null) {
            _hashCode += getMedicalSchemeNumber().hashCode();
        }
        if (getDependentCode() != null) {
            _hashCode += getDependentCode().hashCode();
        }
        if (getAllergies() != null) {
            _hashCode += getAllergies().hashCode();
        }
        if (getAllergyDetails() != null) {
            _hashCode += getAllergyDetails().hashCode();
        }
        _hashCode += getPharmacyAge();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerPharmacy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>Pharmacy"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("marconiId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MarconiId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("collectionPharmacy");
        attrField.setXmlName(new javax.xml.namespace.QName("", "CollectionPharmacy"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("medicalSchemeProvider");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MedicalSchemeProvider"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("medicalSchemeNumber");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MedicalSchemeNumber"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("dependentCode");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DependentCode"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("allergies");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Allergies"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>Pharmacy>Allergies"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("allergyDetails");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AllergyDetails"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("pharmacyAge");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PharmacyAge"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scripts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Scripts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Pharmacy>Scripts>Script"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Script"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
