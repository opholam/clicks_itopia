/**
 * CustomerNonLoyaltyOptIns.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerNonLoyaltyOptIns  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent[] marketingConsent;

    public CustomerNonLoyaltyOptIns() {
    }

    public CustomerNonLoyaltyOptIns(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent[] marketingConsent) {
           this.marketingConsent = marketingConsent;
    }


    /**
     * Gets the marketingConsent value for this CustomerNonLoyaltyOptIns.
     * 
     * @return marketingConsent
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent[] getMarketingConsent() {
        return marketingConsent;
    }


    /**
     * Sets the marketingConsent value for this CustomerNonLoyaltyOptIns.
     * 
     * @param marketingConsent
     */
    public void setMarketingConsent(Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltyOptInsMarketingConsentConsent[] marketingConsent) {
        this.marketingConsent = marketingConsent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerNonLoyaltyOptIns)) return false;
        CustomerNonLoyaltyOptIns other = (CustomerNonLoyaltyOptIns) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.marketingConsent==null && other.getMarketingConsent()==null) || 
             (this.marketingConsent!=null &&
              java.util.Arrays.equals(this.marketingConsent, other.getMarketingConsent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMarketingConsent() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMarketingConsent());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMarketingConsent(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerNonLoyaltyOptIns.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>NonLoyalty>OptIns"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketingConsent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "MarketingConsent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>NonLoyalty>OptIns>MarketingConsent>Consent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Consent"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
