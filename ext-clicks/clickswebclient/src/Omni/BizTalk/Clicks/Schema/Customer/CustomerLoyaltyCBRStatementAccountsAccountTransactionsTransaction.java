/**
 * CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction  implements java.io.Serializable {
    private java.math.BigDecimal amountIssued;  // attribute

    private java.math.BigDecimal availableAmount;  // attribute

    private java.util.Date issueDate;  // attribute

    private java.util.Date expiryDate;  // attribute

    public CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction() {
    }

    public CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction(
           java.math.BigDecimal amountIssued,
           java.math.BigDecimal availableAmount,
           java.util.Date issueDate,
           java.util.Date expiryDate) {
           this.amountIssued = amountIssued;
           this.availableAmount = availableAmount;
           this.issueDate = issueDate;
           this.expiryDate = expiryDate;
    }


    /**
     * Gets the amountIssued value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @return amountIssued
     */
    public java.math.BigDecimal getAmountIssued() {
        return amountIssued;
    }


    /**
     * Sets the amountIssued value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @param amountIssued
     */
    public void setAmountIssued(java.math.BigDecimal amountIssued) {
        this.amountIssued = amountIssued;
    }


    /**
     * Gets the availableAmount value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @return availableAmount
     */
    public java.math.BigDecimal getAvailableAmount() {
        return availableAmount;
    }


    /**
     * Sets the availableAmount value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @param availableAmount
     */
    public void setAvailableAmount(java.math.BigDecimal availableAmount) {
        this.availableAmount = availableAmount;
    }


    /**
     * Gets the issueDate value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @return issueDate
     */
    public java.util.Date getIssueDate() {
        return issueDate;
    }


    /**
     * Sets the issueDate value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @param issueDate
     */
    public void setIssueDate(java.util.Date issueDate) {
        this.issueDate = issueDate;
    }


    /**
     * Gets the expiryDate value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @return expiryDate
     */
    public java.util.Date getExpiryDate() {
        return expiryDate;
    }


    /**
     * Sets the expiryDate value for this CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.
     * 
     * @param expiryDate
     */
    public void setExpiryDate(java.util.Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction)) return false;
        CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction other = (CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amountIssued==null && other.getAmountIssued()==null) || 
             (this.amountIssued!=null &&
              this.amountIssued.equals(other.getAmountIssued()))) &&
            ((this.availableAmount==null && other.getAvailableAmount()==null) || 
             (this.availableAmount!=null &&
              this.availableAmount.equals(other.getAvailableAmount()))) &&
            ((this.issueDate==null && other.getIssueDate()==null) || 
             (this.issueDate!=null &&
              this.issueDate.equals(other.getIssueDate()))) &&
            ((this.expiryDate==null && other.getExpiryDate()==null) || 
             (this.expiryDate!=null &&
              this.expiryDate.equals(other.getExpiryDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmountIssued() != null) {
            _hashCode += getAmountIssued().hashCode();
        }
        if (getAvailableAmount() != null) {
            _hashCode += getAvailableAmount().hashCode();
        }
        if (getIssueDate() != null) {
            _hashCode += getIssueDate().hashCode();
        }
        if (getExpiryDate() != null) {
            _hashCode += getExpiryDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyCBRStatementAccountsAccountTransactionsTransaction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>CBRStatement>Accounts>Account>Transactions>Transaction"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("amountIssued");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AmountIssued"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("availableAmount");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AvailableAmount"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("issueDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "IssueDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("expiryDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "ExpiryDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
