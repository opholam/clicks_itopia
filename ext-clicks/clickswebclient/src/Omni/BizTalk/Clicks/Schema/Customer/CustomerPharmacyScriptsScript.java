/**
 * CustomerPharmacyScriptsScript.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerPharmacyScriptsScript  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScriptScriptImagesScriptImage[] scriptImages;

    private java.lang.String specialInstructions;  // attribute

    public CustomerPharmacyScriptsScript() {
    }

    public CustomerPharmacyScriptsScript(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScriptScriptImagesScriptImage[] scriptImages,
           java.lang.String specialInstructions) {
           this.scriptImages = scriptImages;
           this.specialInstructions = specialInstructions;
    }


    /**
     * Gets the scriptImages value for this CustomerPharmacyScriptsScript.
     * 
     * @return scriptImages
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScriptScriptImagesScriptImage[] getScriptImages() {
        return scriptImages;
    }


    /**
     * Sets the scriptImages value for this CustomerPharmacyScriptsScript.
     * 
     * @param scriptImages
     */
    public void setScriptImages(Omni.BizTalk.Clicks.Schema.Customer.CustomerPharmacyScriptsScriptScriptImagesScriptImage[] scriptImages) {
        this.scriptImages = scriptImages;
    }


    /**
     * Gets the specialInstructions value for this CustomerPharmacyScriptsScript.
     * 
     * @return specialInstructions
     */
    public java.lang.String getSpecialInstructions() {
        return specialInstructions;
    }


    /**
     * Sets the specialInstructions value for this CustomerPharmacyScriptsScript.
     * 
     * @param specialInstructions
     */
    public void setSpecialInstructions(java.lang.String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerPharmacyScriptsScript)) return false;
        CustomerPharmacyScriptsScript other = (CustomerPharmacyScriptsScript) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.scriptImages==null && other.getScriptImages()==null) || 
             (this.scriptImages!=null &&
              java.util.Arrays.equals(this.scriptImages, other.getScriptImages()))) &&
            ((this.specialInstructions==null && other.getSpecialInstructions()==null) || 
             (this.specialInstructions!=null &&
              this.specialInstructions.equals(other.getSpecialInstructions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScriptImages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScriptImages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScriptImages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSpecialInstructions() != null) {
            _hashCode += getSpecialInstructions().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerPharmacyScriptsScript.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>Pharmacy>Scripts>Script"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("specialInstructions");
        attrField.setXmlName(new javax.xml.namespace.QName("", "SpecialInstructions"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scriptImages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "ScriptImages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Pharmacy>Scripts>Script>ScriptImages>ScriptImage"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "ScriptImage"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
