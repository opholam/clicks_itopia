/**
 * CustomerCoreDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerCoreDetails  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress[] addresses;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail[] contactDetails;

    private int title;  // attribute

    private java.lang.String firstName;  // attribute

    private java.lang.String preferedName;  // attribute

    private java.lang.String lastName;  // attribute

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender gender;  // attribute

    private java.lang.String EMailAddress;  // attribute

    private boolean SAResident;  // attribute

    private java.lang.String IDNumber;  // attribute

    private java.util.Date dateOfBirth;  // attribute

    private java.lang.String primaryMemberIDNumber;  // attribute

    private java.util.Date primaryMemberDateOfBirth;  // attribute

    public CustomerCoreDetails() {
    }

    public CustomerCoreDetails(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress[] addresses,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail[] contactDetails,
           int title,
           java.lang.String firstName,
           java.lang.String preferedName,
           java.lang.String lastName,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender gender,
           java.lang.String EMailAddress,
           boolean SAResident,
           java.lang.String IDNumber,
           java.util.Date dateOfBirth,
           java.lang.String primaryMemberIDNumber,
           java.util.Date primaryMemberDateOfBirth) {
           this.addresses = addresses;
           this.contactDetails = contactDetails;
           this.title = title;
           this.firstName = firstName;
           this.preferedName = preferedName;
           this.lastName = lastName;
           this.gender = gender;
           this.EMailAddress = EMailAddress;
           this.SAResident = SAResident;
           this.IDNumber = IDNumber;
           this.dateOfBirth = dateOfBirth;
           this.primaryMemberIDNumber = primaryMemberIDNumber;
           this.primaryMemberDateOfBirth = primaryMemberDateOfBirth;
    }


    /**
     * Gets the addresses value for this CustomerCoreDetails.
     * 
     * @return addresses
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress[] getAddresses() {
        return addresses;
    }


    /**
     * Sets the addresses value for this CustomerCoreDetails.
     * 
     * @param addresses
     */
    public void setAddresses(Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsAddressesAddress[] addresses) {
        this.addresses = addresses;
    }


    /**
     * Gets the contactDetails value for this CustomerCoreDetails.
     * 
     * @return contactDetails
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail[] getContactDetails() {
        return contactDetails;
    }


    /**
     * Sets the contactDetails value for this CustomerCoreDetails.
     * 
     * @param contactDetails
     */
    public void setContactDetails(Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsContactDetailsContactDetail[] contactDetails) {
        this.contactDetails = contactDetails;
    }


    /**
     * Gets the title value for this CustomerCoreDetails.
     * 
     * @return title
     */
    public int getTitle() {
        return title;
    }


    /**
     * Sets the title value for this CustomerCoreDetails.
     * 
     * @param title
     */
    public void setTitle(int title) {
        this.title = title;
    }


    /**
     * Gets the firstName value for this CustomerCoreDetails.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this CustomerCoreDetails.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the preferedName value for this CustomerCoreDetails.
     * 
     * @return preferedName
     */
    public java.lang.String getPreferedName() {
        return preferedName;
    }


    /**
     * Sets the preferedName value for this CustomerCoreDetails.
     * 
     * @param preferedName
     */
    public void setPreferedName(java.lang.String preferedName) {
        this.preferedName = preferedName;
    }


    /**
     * Gets the lastName value for this CustomerCoreDetails.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this CustomerCoreDetails.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the gender value for this CustomerCoreDetails.
     * 
     * @return gender
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this CustomerCoreDetails.
     * 
     * @param gender
     */
    public void setGender(Omni.BizTalk.Clicks.Schema.Customer.CustomerCoreDetailsGender gender) {
        this.gender = gender;
    }


    /**
     * Gets the EMailAddress value for this CustomerCoreDetails.
     * 
     * @return EMailAddress
     */
    public java.lang.String getEMailAddress() {
        return EMailAddress;
    }


    /**
     * Sets the EMailAddress value for this CustomerCoreDetails.
     * 
     * @param EMailAddress
     */
    public void setEMailAddress(java.lang.String EMailAddress) {
        this.EMailAddress = EMailAddress;
    }


    /**
     * Gets the SAResident value for this CustomerCoreDetails.
     * 
     * @return SAResident
     */
    public boolean isSAResident() {
        return SAResident;
    }


    /**
     * Sets the SAResident value for this CustomerCoreDetails.
     * 
     * @param SAResident
     */
    public void setSAResident(boolean SAResident) {
        this.SAResident = SAResident;
    }


    /**
     * Gets the IDNumber value for this CustomerCoreDetails.
     * 
     * @return IDNumber
     */
    public java.lang.String getIDNumber() {
        return IDNumber;
    }


    /**
     * Sets the IDNumber value for this CustomerCoreDetails.
     * 
     * @param IDNumber
     */
    public void setIDNumber(java.lang.String IDNumber) {
        this.IDNumber = IDNumber;
    }


    /**
     * Gets the dateOfBirth value for this CustomerCoreDetails.
     * 
     * @return dateOfBirth
     */
    public java.util.Date getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this CustomerCoreDetails.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.util.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the primaryMemberIDNumber value for this CustomerCoreDetails.
     * 
     * @return primaryMemberIDNumber
     */
    public java.lang.String getPrimaryMemberIDNumber() {
        return primaryMemberIDNumber;
    }


    /**
     * Sets the primaryMemberIDNumber value for this CustomerCoreDetails.
     * 
     * @param primaryMemberIDNumber
     */
    public void setPrimaryMemberIDNumber(java.lang.String primaryMemberIDNumber) {
        this.primaryMemberIDNumber = primaryMemberIDNumber;
    }


    /**
     * Gets the primaryMemberDateOfBirth value for this CustomerCoreDetails.
     * 
     * @return primaryMemberDateOfBirth
     */
    public java.util.Date getPrimaryMemberDateOfBirth() {
        return primaryMemberDateOfBirth;
    }


    /**
     * Sets the primaryMemberDateOfBirth value for this CustomerCoreDetails.
     * 
     * @param primaryMemberDateOfBirth
     */
    public void setPrimaryMemberDateOfBirth(java.util.Date primaryMemberDateOfBirth) {
        this.primaryMemberDateOfBirth = primaryMemberDateOfBirth;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerCoreDetails)) return false;
        CustomerCoreDetails other = (CustomerCoreDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addresses==null && other.getAddresses()==null) || 
             (this.addresses!=null &&
              java.util.Arrays.equals(this.addresses, other.getAddresses()))) &&
            ((this.contactDetails==null && other.getContactDetails()==null) || 
             (this.contactDetails!=null &&
              java.util.Arrays.equals(this.contactDetails, other.getContactDetails()))) &&
            this.title == other.getTitle() &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.preferedName==null && other.getPreferedName()==null) || 
             (this.preferedName!=null &&
              this.preferedName.equals(other.getPreferedName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.EMailAddress==null && other.getEMailAddress()==null) || 
             (this.EMailAddress!=null &&
              this.EMailAddress.equals(other.getEMailAddress()))) &&
            this.SAResident == other.isSAResident() &&
            ((this.IDNumber==null && other.getIDNumber()==null) || 
             (this.IDNumber!=null &&
              this.IDNumber.equals(other.getIDNumber()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) || 
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth()))) &&
            ((this.primaryMemberIDNumber==null && other.getPrimaryMemberIDNumber()==null) || 
             (this.primaryMemberIDNumber!=null &&
              this.primaryMemberIDNumber.equals(other.getPrimaryMemberIDNumber()))) &&
            ((this.primaryMemberDateOfBirth==null && other.getPrimaryMemberDateOfBirth()==null) || 
             (this.primaryMemberDateOfBirth!=null &&
              this.primaryMemberDateOfBirth.equals(other.getPrimaryMemberDateOfBirth())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddresses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAddresses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAddresses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getContactDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContactDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContactDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getTitle();
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getPreferedName() != null) {
            _hashCode += getPreferedName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getEMailAddress() != null) {
            _hashCode += getEMailAddress().hashCode();
        }
        _hashCode += (isSAResident() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getIDNumber() != null) {
            _hashCode += getIDNumber().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        if (getPrimaryMemberIDNumber() != null) {
            _hashCode += getPrimaryMemberIDNumber().hashCode();
        }
        if (getPrimaryMemberDateOfBirth() != null) {
            _hashCode += getPrimaryMemberDateOfBirth().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerCoreDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">Customer>CoreDetails"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("title");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("firstName");
        attrField.setXmlName(new javax.xml.namespace.QName("", "FirstName"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>FirstName"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("preferedName");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PreferedName"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>PreferedName"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("lastName");
        attrField.setXmlName(new javax.xml.namespace.QName("", "LastName"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>LastName"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("gender");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Gender"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>Gender"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("EMailAddress");
        attrField.setXmlName(new javax.xml.namespace.QName("", "EMailAddress"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>EMailAddress"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("SAResident");
        attrField.setXmlName(new javax.xml.namespace.QName("", "SAResident"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("IDNumber");
        attrField.setXmlName(new javax.xml.namespace.QName("", "IDNumber"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>IDNumber"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("dateOfBirth");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DateOfBirth"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("primaryMemberIDNumber");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PrimaryMemberIDNumber"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>Customer>CoreDetails>PrimaryMemberIDNumber"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("primaryMemberDateOfBirth");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PrimaryMemberDateOfBirth"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addresses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Addresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>Addresses>Address"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Address"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "ContactDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>CoreDetails>ContactDetails>ContactDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "ContactDetail"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
