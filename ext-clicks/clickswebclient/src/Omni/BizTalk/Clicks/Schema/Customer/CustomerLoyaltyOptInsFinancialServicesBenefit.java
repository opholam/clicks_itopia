/**
 * CustomerLoyaltyOptInsFinancialServicesBenefit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyOptInsFinancialServicesBenefit  implements java.io.Serializable {
    private java.lang.String benefitId;  // attribute

    private java.math.BigDecimal benefitAmount;  // attribute

    private java.util.Date benefitDate;  // attribute

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefitStatus status;  // attribute

    public CustomerLoyaltyOptInsFinancialServicesBenefit() {
    }

    public CustomerLoyaltyOptInsFinancialServicesBenefit(
           java.lang.String benefitId,
           java.math.BigDecimal benefitAmount,
           java.util.Date benefitDate,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefitStatus status) {
           this.benefitId = benefitId;
           this.benefitAmount = benefitAmount;
           this.benefitDate = benefitDate;
           this.status = status;
    }


    /**
     * Gets the benefitId value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @return benefitId
     */
    public java.lang.String getBenefitId() {
        return benefitId;
    }


    /**
     * Sets the benefitId value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @param benefitId
     */
    public void setBenefitId(java.lang.String benefitId) {
        this.benefitId = benefitId;
    }


    /**
     * Gets the benefitAmount value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @return benefitAmount
     */
    public java.math.BigDecimal getBenefitAmount() {
        return benefitAmount;
    }


    /**
     * Sets the benefitAmount value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @param benefitAmount
     */
    public void setBenefitAmount(java.math.BigDecimal benefitAmount) {
        this.benefitAmount = benefitAmount;
    }


    /**
     * Gets the benefitDate value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @return benefitDate
     */
    public java.util.Date getBenefitDate() {
        return benefitDate;
    }


    /**
     * Sets the benefitDate value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @param benefitDate
     */
    public void setBenefitDate(java.util.Date benefitDate) {
        this.benefitDate = benefitDate;
    }


    /**
     * Gets the status value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @return status
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefitStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this CustomerLoyaltyOptInsFinancialServicesBenefit.
     * 
     * @param status
     */
    public void setStatus(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyOptInsFinancialServicesBenefitStatus status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyOptInsFinancialServicesBenefit)) return false;
        CustomerLoyaltyOptInsFinancialServicesBenefit other = (CustomerLoyaltyOptInsFinancialServicesBenefit) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.benefitId==null && other.getBenefitId()==null) || 
             (this.benefitId!=null &&
              this.benefitId.equals(other.getBenefitId()))) &&
            ((this.benefitAmount==null && other.getBenefitAmount()==null) || 
             (this.benefitAmount!=null &&
              this.benefitAmount.equals(other.getBenefitAmount()))) &&
            ((this.benefitDate==null && other.getBenefitDate()==null) || 
             (this.benefitDate!=null &&
              this.benefitDate.equals(other.getBenefitDate()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBenefitId() != null) {
            _hashCode += getBenefitId().hashCode();
        }
        if (getBenefitAmount() != null) {
            _hashCode += getBenefitAmount().hashCode();
        }
        if (getBenefitDate() != null) {
            _hashCode += getBenefitDate().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyOptInsFinancialServicesBenefit.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>OptIns>FinancialServices>Benefit"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("benefitId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "BenefitId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("benefitAmount");
        attrField.setXmlName(new javax.xml.namespace.QName("", "BenefitAmount"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("benefitDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "BenefitDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("status");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Status"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>Customer>Loyalty>OptIns>FinancialServices>Benefit>Status"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
