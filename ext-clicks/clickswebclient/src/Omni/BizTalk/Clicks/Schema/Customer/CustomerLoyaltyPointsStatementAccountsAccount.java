/**
 * CustomerLoyaltyPointsStatementAccountsAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerLoyaltyPointsStatementAccountsAccount  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction[] transactions;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPointsBucketsBucket[] pointsBuckets;

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPartnerPointsPartner[] partnerPoints;

    private int accountId;  // attribute

    private java.math.BigDecimal totalSpend;  // attribute

    private int pointsBalance;  // attribute

    private int pointsToQualify;  // attribute

    private int transactionsPointsBalance;  // attribute

    private int pointsBucketsBalance;  // attribute

    private int partnerPointsBalance;  // attribute

    public CustomerLoyaltyPointsStatementAccountsAccount() {
    }

    public CustomerLoyaltyPointsStatementAccountsAccount(
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction[] transactions,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPointsBucketsBucket[] pointsBuckets,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPartnerPointsPartner[] partnerPoints,
           int accountId,
           java.math.BigDecimal totalSpend,
           int pointsBalance,
           int pointsToQualify,
           int transactionsPointsBalance,
           int pointsBucketsBalance,
           int partnerPointsBalance) {
           this.transactions = transactions;
           this.pointsBuckets = pointsBuckets;
           this.partnerPoints = partnerPoints;
           this.accountId = accountId;
           this.totalSpend = totalSpend;
           this.pointsBalance = pointsBalance;
           this.pointsToQualify = pointsToQualify;
           this.transactionsPointsBalance = transactionsPointsBalance;
           this.pointsBucketsBalance = pointsBucketsBalance;
           this.partnerPointsBalance = partnerPointsBalance;
    }


    /**
     * Gets the transactions value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return transactions
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction[] getTransactions() {
        return transactions;
    }


    /**
     * Sets the transactions value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param transactions
     */
    public void setTransactions(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountTransactionsTransaction[] transactions) {
        this.transactions = transactions;
    }


    /**
     * Gets the pointsBuckets value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return pointsBuckets
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPointsBucketsBucket[] getPointsBuckets() {
        return pointsBuckets;
    }


    /**
     * Sets the pointsBuckets value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param pointsBuckets
     */
    public void setPointsBuckets(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPointsBucketsBucket[] pointsBuckets) {
        this.pointsBuckets = pointsBuckets;
    }


    /**
     * Gets the partnerPoints value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return partnerPoints
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPartnerPointsPartner[] getPartnerPoints() {
        return partnerPoints;
    }


    /**
     * Sets the partnerPoints value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param partnerPoints
     */
    public void setPartnerPoints(Omni.BizTalk.Clicks.Schema.Customer.CustomerLoyaltyPointsStatementAccountsAccountPartnerPointsPartner[] partnerPoints) {
        this.partnerPoints = partnerPoints;
    }


    /**
     * Gets the accountId value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the totalSpend value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return totalSpend
     */
    public java.math.BigDecimal getTotalSpend() {
        return totalSpend;
    }


    /**
     * Sets the totalSpend value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param totalSpend
     */
    public void setTotalSpend(java.math.BigDecimal totalSpend) {
        this.totalSpend = totalSpend;
    }


    /**
     * Gets the pointsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return pointsBalance
     */
    public int getPointsBalance() {
        return pointsBalance;
    }


    /**
     * Sets the pointsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param pointsBalance
     */
    public void setPointsBalance(int pointsBalance) {
        this.pointsBalance = pointsBalance;
    }


    /**
     * Gets the pointsToQualify value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return pointsToQualify
     */
    public int getPointsToQualify() {
        return pointsToQualify;
    }


    /**
     * Sets the pointsToQualify value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param pointsToQualify
     */
    public void setPointsToQualify(int pointsToQualify) {
        this.pointsToQualify = pointsToQualify;
    }


    /**
     * Gets the transactionsPointsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return transactionsPointsBalance
     */
    public int getTransactionsPointsBalance() {
        return transactionsPointsBalance;
    }


    /**
     * Sets the transactionsPointsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param transactionsPointsBalance
     */
    public void setTransactionsPointsBalance(int transactionsPointsBalance) {
        this.transactionsPointsBalance = transactionsPointsBalance;
    }


    /**
     * Gets the pointsBucketsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return pointsBucketsBalance
     */
    public int getPointsBucketsBalance() {
        return pointsBucketsBalance;
    }


    /**
     * Sets the pointsBucketsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param pointsBucketsBalance
     */
    public void setPointsBucketsBalance(int pointsBucketsBalance) {
        this.pointsBucketsBalance = pointsBucketsBalance;
    }


    /**
     * Gets the partnerPointsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @return partnerPointsBalance
     */
    public int getPartnerPointsBalance() {
        return partnerPointsBalance;
    }


    /**
     * Sets the partnerPointsBalance value for this CustomerLoyaltyPointsStatementAccountsAccount.
     * 
     * @param partnerPointsBalance
     */
    public void setPartnerPointsBalance(int partnerPointsBalance) {
        this.partnerPointsBalance = partnerPointsBalance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerLoyaltyPointsStatementAccountsAccount)) return false;
        CustomerLoyaltyPointsStatementAccountsAccount other = (CustomerLoyaltyPointsStatementAccountsAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.transactions==null && other.getTransactions()==null) || 
             (this.transactions!=null &&
              java.util.Arrays.equals(this.transactions, other.getTransactions()))) &&
            ((this.pointsBuckets==null && other.getPointsBuckets()==null) || 
             (this.pointsBuckets!=null &&
              java.util.Arrays.equals(this.pointsBuckets, other.getPointsBuckets()))) &&
            ((this.partnerPoints==null && other.getPartnerPoints()==null) || 
             (this.partnerPoints!=null &&
              java.util.Arrays.equals(this.partnerPoints, other.getPartnerPoints()))) &&
            this.accountId == other.getAccountId() &&
            ((this.totalSpend==null && other.getTotalSpend()==null) || 
             (this.totalSpend!=null &&
              this.totalSpend.equals(other.getTotalSpend()))) &&
            this.pointsBalance == other.getPointsBalance() &&
            this.pointsToQualify == other.getPointsToQualify() &&
            this.transactionsPointsBalance == other.getTransactionsPointsBalance() &&
            this.pointsBucketsBalance == other.getPointsBucketsBalance() &&
            this.partnerPointsBalance == other.getPartnerPointsBalance();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransactions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransactions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransactions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPointsBuckets() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPointsBuckets());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPointsBuckets(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPartnerPoints() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPartnerPoints());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPartnerPoints(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getAccountId();
        if (getTotalSpend() != null) {
            _hashCode += getTotalSpend().hashCode();
        }
        _hashCode += getPointsBalance();
        _hashCode += getPointsToQualify();
        _hashCode += getTransactionsPointsBalance();
        _hashCode += getPointsBucketsBalance();
        _hashCode += getPartnerPointsBalance();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerLoyaltyPointsStatementAccountsAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>Loyalty>PointsStatement>Accounts>Account"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("accountId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AccountId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("totalSpend");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TotalSpend"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("pointsBalance");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PointsBalance"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("pointsToQualify");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PointsToQualify"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("transactionsPointsBalance");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TransactionsPointsBalance"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("pointsBucketsBalance");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PointsBucketsBalance"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("partnerPointsBalance");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PartnerPointsBalance"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Transactions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>Transactions>Transaction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Transaction"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointsBuckets");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "PointsBuckets"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PointsBuckets>Bucket"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Bucket"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partnerPoints");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "PartnerPoints"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>>>Customer>Loyalty>PointsStatement>Accounts>Account>PartnerPoints>Partner"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", "Partner"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
