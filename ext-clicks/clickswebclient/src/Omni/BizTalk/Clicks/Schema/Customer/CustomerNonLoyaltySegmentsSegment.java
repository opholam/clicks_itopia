/**
 * CustomerNonLoyaltySegmentsSegment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.Customer;

public class CustomerNonLoyaltySegmentsSegment  implements java.io.Serializable {
    private int segmentId;  // attribute

    private Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegmentStatus status;  // attribute

    private java.util.Date startDate;  // attribute

    private java.util.Date endDate;  // attribute

    public CustomerNonLoyaltySegmentsSegment() {
    }

    public CustomerNonLoyaltySegmentsSegment(
           int segmentId,
           Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegmentStatus status,
           java.util.Date startDate,
           java.util.Date endDate) {
           this.segmentId = segmentId;
           this.status = status;
           this.startDate = startDate;
           this.endDate = endDate;
    }


    /**
     * Gets the segmentId value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @return segmentId
     */
    public int getSegmentId() {
        return segmentId;
    }


    /**
     * Sets the segmentId value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @param segmentId
     */
    public void setSegmentId(int segmentId) {
        this.segmentId = segmentId;
    }


    /**
     * Gets the status value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @return status
     */
    public Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegmentStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @param status
     */
    public void setStatus(Omni.BizTalk.Clicks.Schema.Customer.CustomerNonLoyaltySegmentsSegmentStatus status) {
        this.status = status;
    }


    /**
     * Gets the startDate value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @return startDate
     */
    public java.util.Date getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @return endDate
     */
    public java.util.Date getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this CustomerNonLoyaltySegmentsSegment.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Date endDate) {
        this.endDate = endDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerNonLoyaltySegmentsSegment)) return false;
        CustomerNonLoyaltySegmentsSegment other = (CustomerNonLoyaltySegmentsSegment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.segmentId == other.getSegmentId() &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSegmentId();
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerNonLoyaltySegmentsSegment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>Customer>NonLoyalty>Segments>Segment"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("segmentId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "SegmentId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("status");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Status"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/Customer", ">>>>Customer>NonLoyalty>Segments>Segment>Status"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("startDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "StartDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("endDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "EndDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
