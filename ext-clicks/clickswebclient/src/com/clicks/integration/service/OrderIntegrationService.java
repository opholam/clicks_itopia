/**
 *
 */
package com.clicks.integration.service;

import de.hybris.platform.core.model.order.OrderModel;


public interface OrderIntegrationService
{
	void publishNewOrder(OrderModel order);


}
