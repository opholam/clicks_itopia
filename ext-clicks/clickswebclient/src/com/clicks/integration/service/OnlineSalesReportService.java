/**
 *
 */
package com.clicks.integration.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;


/**
 * @author manish.bhargava
 *
 */
public interface OnlineSalesReportService
{
	void publishOnlineSalesReportData(List<OrderModel> orders);
}
