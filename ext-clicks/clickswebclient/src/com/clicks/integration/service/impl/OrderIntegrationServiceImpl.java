/**
 *
 */
package com.clicks.integration.service.impl;

import de.hybris.clicks.webservices.omniorder.schemas.Order;
import de.hybris.platform.core.enums.ExportStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;

import com.clicks.integration.populators.OmniOrderRequestPopulator;
import com.clicks.integration.service.OrderIntegrationService;



public class OrderIntegrationServiceImpl extends WebServiceGatewaySupport implements OrderIntegrationService
{






	final static Logger LOG = Logger.getLogger(OrderIntegrationServiceImpl.class.getName());


	private OmniOrderRequestPopulator omniOrderRequestPopulator;

	private ModelService modelService;

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param omniOrderRequestPopulator
	 *           the omniOrderRequestPopulator to set
	 */
	public void setOmniOrderRequestPopulator(final OmniOrderRequestPopulator omniOrderRequestPopulator)
	{
		this.omniOrderRequestPopulator = omniOrderRequestPopulator;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.clicks.integration.service.OrderService#getProductPricingData(com.clicks.integration.schema.order.Order)
	 */
	@Override
	public void publishNewOrder(final OrderModel order)
	{
		LOG.info("Inside OrderServiceImpl. Preparing JAXBElement");
		final Order orderReq = omniOrderRequestPopulator.createOrderRequest(order);

		try
		{

			getWebServiceTemplate().marshalSendAndReceive(orderReq, new WebServiceMessageCallback()
			{

				@Override
				public void doWithMessage(final WebServiceMessage webservicemessage) throws IOException, TransformerException
				{
					((SoapMessage) webservicemessage).setSoapAction("OnlineOrder");
					final ByteArrayOutputStream out = new ByteArrayOutputStream();
					webservicemessage.writeTo(out);
					LOG.info(out.toString());
				}
			});
			order.setExportStatus(ExportStatus.EXPORTED);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while publishing order", e);
			order.setExportStatus(ExportStatus.NOTEXPORTED);
		}
		modelService.save(order);
	}
}
