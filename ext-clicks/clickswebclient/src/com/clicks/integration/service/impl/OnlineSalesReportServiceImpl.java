/**
 *
 */
package com.clicks.integration.service.impl;

import de.hybris.clicks.webservices.onlineSalesReport.schemas.OnlineSalesReport;
import de.hybris.platform.core.model.order.OrderModel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;

import com.clicks.integration.populators.OnlineSalesReportRequestPopulator;
import com.clicks.integration.service.OnlineSalesReportService;


/**
 * @author manish.bhargava
 *
 */
public class OnlineSalesReportServiceImpl extends WebServiceGatewaySupport implements OnlineSalesReportService
{

	final static Logger LOG = Logger.getLogger(OnlineSalesReportServiceImpl.class.getName());

	private OnlineSalesReportRequestPopulator onlineSalesReportRequestPopulator;

	/**
	 * @param onlineSalesReportRequestPopulator
	 *           the onlineSalesReportRequestPopulator to set
	 */
	public void setOnlineSalesReportRequestPopulator(final OnlineSalesReportRequestPopulator onlineSalesReportRequestPopulator)
	{
		this.onlineSalesReportRequestPopulator = onlineSalesReportRequestPopulator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.clicks.integration.service.OnlineSalesReportService#publishOnlineSalesReportData(de.hybris.platform.core.model
	 * .order.OrderModel)
	 */
	@Override
	public void publishOnlineSalesReportData(final List<OrderModel> orders)
	{
		LOG.info("Inside OnlineSalesReportServiceImpl. Preparing JAXBElement");
		final OnlineSalesReport onlineSalesReportReq = onlineSalesReportRequestPopulator.createOnlineSalesReportRequest(orders);

		getWebServiceTemplate().marshalSendAndReceive(onlineSalesReportReq, new WebServiceMessageCallback()
		{
			@Override
			public void doWithMessage(final WebServiceMessage webservicemessage) throws IOException, TransformerException
			{
				((SoapMessage) webservicemessage).setSoapAction("PublishOnlineSalesReport");
				final ByteArrayOutputStream out = new ByteArrayOutputStream();
				webservicemessage.writeTo(out);
				LOG.info(out.toString());
			}
		});
	}
}