/**
 *
 */
package com.clicks.integration.constants;

/**
 * @author manikandan.r
 *
 */
public interface OmniOrderConstants
{
	String DATA_SOURCE = "Hybris";
	String EVENT_LOGID = "4d3942ea-b537-4c38-9141-fd8000829001";
	String EVENT_CODE = "OMNIORD";
}
