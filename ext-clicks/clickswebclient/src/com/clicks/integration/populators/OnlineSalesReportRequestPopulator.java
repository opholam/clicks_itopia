/**
 *
 */
package com.clicks.integration.populators;

import de.hybris.clicks.webservices.onlineSalesReport.schemas.ObjectFactory;
import de.hybris.clicks.webservices.onlineSalesReport.schemas.OnlineSalesReport;
import de.hybris.clicks.webservices.onlineSalesReport.schemas.OnlineSalesReport.Body.PaymentSummary;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.clicks.integration.constants.OnlineSalesReportConstants;


/**
 * @author manish.bhargava
 *
 */
public class OnlineSalesReportRequestPopulator
{
	static Logger log = Logger.getLogger(OnlineSalesReportRequestPopulator.class.getName());

	private static final ObjectFactory onlineSalesReportObjectFactory = new ObjectFactory();

	@Resource(name = "myNumberseriesGenerator")
	private PersistentKeyGenerator myNumberseriesGenerator;

	private CommonI18NService commonI18NService;

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public OnlineSalesReport createOnlineSalesReportRequest(final List<OrderModel> orders)
	{
		log.info("+++++++++++++++++++Inside OnlineSalesReportRequestPopulator++++++++++++++++++++");
		OnlineSalesReport onlineSalesReport = null;
		onlineSalesReport = onlineSalesReportObjectFactory.createOnlineSalesReport();
		final OnlineSalesReport.Header onlineSalesReportHeader = new OnlineSalesReport.Header();
		final OnlineSalesReport.Body onlineSalesReportBody = new OnlineSalesReport.Body();

		onlineSalesReportHeader.setCreatedDateTime(convertDateToXMLGregorianCalendar(new Date()));
		onlineSalesReportHeader.setDataSource(OnlineSalesReportConstants.DATA_SOURCE);
		onlineSalesReportHeader.setEventCode(OnlineSalesReportConstants.EVENT_CODE);
		onlineSalesReportHeader.setEventLogId((String) myNumberseriesGenerator.generate());

		BigDecimal totalDiscountForAllOrders = BigDecimal.ZERO;
		BigDecimal totalSalesForAllOrders = BigDecimal.ZERO;
		BigDecimal totalVATForAllOrders = BigDecimal.ZERO;

		BigDecimal totalSalesByCreditCard = BigDecimal.ZERO;
		BigDecimal totalSalesByDebitCard = BigDecimal.ZERO;
		BigDecimal totalSalesByEFT = BigDecimal.ZERO;
		for (final OrderModel orderModel : orders)
		{
			totalDiscountForAllOrders = totalDiscountForAllOrders.add(populateTotalDiscountsOnOrder(orderModel));

			totalSalesForAllOrders = totalSalesForAllOrders.add(populateTotalSaleForOrder(orderModel));

			totalVATForAllOrders = totalVATForAllOrders.add(populateTotalVatForOrder(orderModel));

			final OnlineSalesReport.Body.PaymentSummary.PaymentDetail paymentDetailForOrder = populateTotalSalesAmountByPaymentType(orderModel);
			if (null != paymentDetailForOrder)
			{
				//For Credit card
				if (StringUtils.equalsIgnoreCase(paymentDetailForOrder.getPaymentMethod(), "1"))
				{
					totalSalesByCreditCard = totalSalesByCreditCard.add(paymentDetailForOrder.getPaymentAmount());
				}
				else if (StringUtils.equalsIgnoreCase(paymentDetailForOrder.getPaymentMethod(), "2"))
				{//For Dedit card
					totalSalesByDebitCard = totalSalesByDebitCard.add(paymentDetailForOrder.getPaymentAmount());
				}
				else if (StringUtils.equalsIgnoreCase(paymentDetailForOrder.getPaymentMethod(), "3"))
				{
					//For EFT
					totalSalesByEFT = totalSalesByEFT.add(paymentDetailForOrder.getPaymentAmount());
				}
			}
		}
		onlineSalesReportBody.setPaymentSummary(populatePaymentSummary(totalSalesByCreditCard, totalSalesByDebitCard,
				totalSalesByEFT));
		onlineSalesReportBody.setStoreNumber(Config.getParameter("clickswebclient.order.storeid"));
		onlineSalesReportBody.setDateTime(convertDateToXMLGregorianCalendar(orders.get(0).getCreationtime()));
		onlineSalesReportBody.setTotalDiscount(totalDiscountForAllOrders);
		onlineSalesReportBody.setTotalSales(totalSalesForAllOrders);
		onlineSalesReportBody.setTotalVAT(totalVATForAllOrders);
		onlineSalesReportBody.setTransactionCount(orders.size());

		onlineSalesReport.setHeader(onlineSalesReportHeader);
		onlineSalesReport.setBody(onlineSalesReportBody);
		return onlineSalesReport;
	}

	private BigDecimal populateTotalDiscountsOnOrder(final OrderModel orderModel)
	{
		final double productsDiscountsAmount = getProductsDiscountsAmount(orderModel);
		final double orderDiscountsAmount = getOrderDiscountsAmount(orderModel);
		return roundOffPrice(productsDiscountsAmount + orderDiscountsAmount);
	}

	private BigDecimal populateTotalSaleForOrder(final OrderModel orderModel)
	{
		return roundOffPrice(orderModel.getTotalPrice());
	}

	private BigDecimal populateTotalVatForOrder(final OrderModel orderModel)
	{
		return roundOffPrice(orderModel.getTotalTax());
	}

	private double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}

	private double getOrderDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (value > 0.0d)
				{
					discounts += value;
				}
			}
		}

		return discounts;
	}

	private XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date)
	{
		log.info("+++++++++++++++++++Populate  XMLGregorianCalendar for OnlineSalesReportRequestPopulator++++++++++++++++++++");
		final GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		try
		{
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		}
		catch (final DatatypeConfigurationException e)
		{
			log.error("++++++++++Cannot convert date to GregorianCalendar++++++++++", e);
		}
		return null;
	}

	private BigDecimal roundOffPrice(final double value)
	{
		final double roundedVal = commonI18NService.roundCurrency(value, 2);
		return new BigDecimal(roundedVal).setScale(2, RoundingMode.HALF_UP);
	}

	private OnlineSalesReport.Body.PaymentSummary.PaymentDetail populateTotalSalesAmountByPaymentType(final OrderModel order)
	{
		PaymentTransactionEntryModel entry = null;
		OnlineSalesReport.Body.PaymentSummary.PaymentDetail paymentDetail = null;
		for (final PaymentTransactionModel paymentTransactionModel : order.getPaymentTransactions())
		{
			entry = (PaymentTransactionEntryModel) CollectionUtils.find(paymentTransactionModel.getEntries(),
					new BeanPropertyValueEqualsPredicate("type", PaymentTransactionType.AUTHORIZATION, true));
			if (null != entry && "ACCEPTED".equals(entry.getTransactionStatus())
					&& "Successful".equals(entry.getTransactionStatusDetails()) && null != entry.getAmount()
					&& entry.getAmount().doubleValue() == order.getTotalPrice().doubleValue())
			{
				paymentDetail = new OnlineSalesReport.Body.PaymentSummary.PaymentDetail();
				if (null != paymentTransactionModel.getInfo()
						&& paymentTransactionModel.getInfo() instanceof CreditCardPaymentInfoModel)
				{
					paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.creditcard"));
					paymentDetail.setPaymentAmount(entry.getAmount());
				}
				else if (null != paymentTransactionModel.getInfo()
						&& paymentTransactionModel.getInfo() instanceof DebitPaymentInfoModel)
				{
					final DebitPaymentInfoModel debitPaymentInfoModel = (DebitPaymentInfoModel) paymentTransactionModel.getInfo();
					if (("DEBIT CARD").equalsIgnoreCase(debitPaymentInfoModel.getBaOwner()))
					{
						paymentDetail = new OnlineSalesReport.Body.PaymentSummary.PaymentDetail();
						paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.debitcard"));
						paymentDetail.setPaymentAmount(entry.getAmount());
					}
					else if (("EFT").equalsIgnoreCase(debitPaymentInfoModel.getBaOwner()))
					{
						paymentDetail = new OnlineSalesReport.Body.PaymentSummary.PaymentDetail();
						paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.eft"));
						paymentDetail.setPaymentAmount(entry.getAmount());
					}
				}
				break;
			}
		}
		return paymentDetail;
	}

	private PaymentSummary populatePaymentSummary(final BigDecimal totalSalesByCreditCard, final BigDecimal totalSalesByDebitCard,
			final BigDecimal totalSalesByEFT)
	{
		final PaymentSummary paymentSummary = new PaymentSummary();
		final List<OnlineSalesReport.Body.PaymentSummary.PaymentDetail> paymentDetailList = new ArrayList<OnlineSalesReport.Body.PaymentSummary.PaymentDetail>();
		OnlineSalesReport.Body.PaymentSummary.PaymentDetail paymentDetail = null;
		paymentDetail = new OnlineSalesReport.Body.PaymentSummary.PaymentDetail();
		paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.creditcard"));
		paymentDetail.setPaymentAmount(roundOffPrice(totalSalesByCreditCard.doubleValue()));
		paymentDetailList.add(paymentDetail);

		paymentDetail = new OnlineSalesReport.Body.PaymentSummary.PaymentDetail();
		paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.debitcard"));
		paymentDetail.setPaymentAmount(roundOffPrice(totalSalesByDebitCard.doubleValue()));
		paymentDetailList.add(paymentDetail);

		paymentDetail = new OnlineSalesReport.Body.PaymentSummary.PaymentDetail();
		paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.eft"));
		paymentDetail.setPaymentAmount(roundOffPrice(totalSalesByEFT.doubleValue()));
		paymentDetailList.add(paymentDetail);

		paymentSummary.getPaymentDetail().addAll(paymentDetailList);
		return paymentSummary;
	}
}
