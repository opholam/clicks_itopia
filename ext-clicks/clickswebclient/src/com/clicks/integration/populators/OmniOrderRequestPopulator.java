/**
 *
 */
package com.clicks.integration.populators;

import de.hybris.clicks.core.model.ContactDetailsModel;
import de.hybris.clicks.webservices.omniorder.schemas.AddressType;
import de.hybris.clicks.webservices.omniorder.schemas.ContactDetailsType;
import de.hybris.clicks.webservices.omniorder.schemas.ContactDetailsType.ContactDetail;
import de.hybris.clicks.webservices.omniorder.schemas.DeliveryAddressType;
import de.hybris.clicks.webservices.omniorder.schemas.ObjectFactory;
import de.hybris.clicks.webservices.omniorder.schemas.Order;
import de.hybris.clicks.webservices.omniorder.schemas.Order.Body.PaymentInfo.PaymentDetail;
import de.hybris.clicks.webservices.omniorder.schemas.Order.Body.SalesTransaction.TransactionDetail.DiscountLineItem;
import de.hybris.clicks.webservices.omniorder.schemas.Order.Body.SalesTransaction.TransactionDetail.LineItem;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.jalo.ProductOneToOnePerfectPartnerPromotion;
import de.hybris.platform.promotions.jalo.ProductPerfectPartnerBundlePromotion;
import de.hybris.platform.promotions.jalo.ProductPerfectPartnerPromotion;
import de.hybris.platform.promotions.jalo.PromotionOrderEntryConsumed;
import de.hybris.platform.promotions.jalo.PromotionResult;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.PointOfServiceDao;
import de.hybris.platform.storelocator.exception.PointOfServiceDaoException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.voucher.VoucherModelService;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.clicks.integration.constants.OmniOrderConstants;


public class OmniOrderRequestPopulator
{
	static Logger log = Logger.getLogger(OmniOrderRequestPopulator.class.getName());

	private static final ObjectFactory orderObjectFactory = new ObjectFactory();

	PromotionsService promotionsService;

	private PointOfServiceDao pointOfServiceDao;


	private CommonI18NService commonI18NService;

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	protected PointOfServiceDao getPointOfServiceDao()
	{
		return this.pointOfServiceDao;
	}

	@Required
	public void setPointOfServiceDao(final PointOfServiceDao pointOfServiceDao)
	{
		this.pointOfServiceDao = pointOfServiceDao;
	}

	@Resource(name = "voucherModelService")
	private VoucherModelService voucherModelService;

	/**
	 * @return the promotionsService
	 */
	public PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * @param promotionsService
	 *           the promotionsService to set
	 */
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}

	public Order createOrderRequest(final OrderModel order)
	{
		log.info("+++++++++++++++++++Inside OrderRequestPopulator++++++++++++++++++++");
		Order orderReq = null;

		if (order != null)
		{
			final String storeId = Config.getParameter("clickswebclient.order.storeid");
			log.info("+++++++++++++++++++Store id for order----------> " + order.getClicksOrderCode() + "is: " + storeId
					+ "++++++++++++++++++++");
			orderReq = orderObjectFactory.createOrder();
			final Order.Body orderBody = new Order.Body();
			final Order.Header orderHeader = new Order.Header();
			final XMLGregorianCalendar xmlgregorianCalendar = convertDateToXMLGregorianCalendar(order.getCreationtime());

			orderHeader.setDataSource(OmniOrderConstants.DATA_SOURCE);
			orderHeader.setEventLogId(order.getClicksOrderCode());
			orderHeader.setEventCode(OmniOrderConstants.EVENT_CODE);
			orderHeader.setCreatedDateTime(xmlgregorianCalendar);
			orderReq.setHeader(orderHeader);

			orderBody.setOrderNumber(order.getClicksOrderCode());
			orderBody.setOrderDate(xmlgregorianCalendar);
			orderBody.setStoreId(storeId);
			orderBody.setDeliveryModeDescription(StringUtils.isNotBlank(order.getDeliveryMode().getDescription()) ? order
					.getDeliveryMode().getDescription() : "");
			try
			{
				if (order.getDeliveryAddress().getIsStore())
				{
					orderBody.setDeliveryMode(Integer.parseInt(order.getDeliveryMode().getCode()));
				}
				else
				{
					orderBody.setDeliveryMode(Integer.parseInt(order.getDeliveryMode().getCode()));
				}
			}
			catch (final Exception e)
			{
				log.error("Delivery code: " + order.getDeliveryMode().getCode() + "cannot be parsed into integer" + e);
				orderBody.setDeliveryMode(1);
			}
			orderBody.setDeliveryInstructions(order.getDeliveryInstructions());
			orderBody.setDeliveryAddress(populateDeliveryAddress(order.getDeliveryAddress()));
			try
			{
				if (null != order.getPaymentInfo())
				{
					final AddressType addressType = populateAddress(order.getPaymentAddress() != null ? order.getPaymentAddress()
							: order.getPaymentInfo().getBillingAddress() != null ? order.getPaymentInfo().getBillingAddress() : null);
					final Order.Body.PaymentInfo paymentInfo = new Order.Body.PaymentInfo();
					paymentInfo.getInvoicePaymentMethodDescription().add(
							order.getPaymentMode() != null ? order.getPaymentMode().getDescription() : "PAYU PAYMENT");
					paymentInfo.setBillingAddress(addressType);
					PaymentDetail paymentDetail = null;
					PaymentTransactionEntryModel entry = null;
					for (final PaymentTransactionModel paymentTransactionModel : order.getPaymentTransactions())
					{
						entry = (PaymentTransactionEntryModel) CollectionUtils.find(paymentTransactionModel.getEntries(),
								new BeanPropertyValueEqualsPredicate("type", PaymentTransactionType.AUTHORIZATION, true));
						if (null != entry && "ACCEPTED".equals(entry.getTransactionStatus())
								&& "Successful".equals(entry.getTransactionStatusDetails()) && null != entry.getAmount()
								&& entry.getAmount().doubleValue() == order.getTotalPrice().doubleValue())
						{
							paymentDetail = new PaymentDetail();
							paymentDetail
									.setPaymentAmount(CollectionUtils.isNotEmpty(paymentTransactionModel.getEntries()) ? roundOffPrice(
											paymentTransactionModel.getEntries().get(0).getAmount().doubleValue()).toPlainString() : order
											.getTotalPrice().toString());
							paymentDetail.setPaymentReference(paymentTransactionModel.getRequestId());
							if (null != paymentTransactionModel.getInfo()
									&& paymentTransactionModel.getInfo() instanceof CreditCardPaymentInfoModel)
							{
								paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.creditcard"));
							}
							else if (null != paymentTransactionModel.getInfo()
									&& paymentTransactionModel.getInfo() instanceof DebitPaymentInfoModel)
							{
								final DebitPaymentInfoModel debitPaymentInfoModel = (DebitPaymentInfoModel) paymentTransactionModel
										.getInfo();
								if (("DEBIT CARD").equalsIgnoreCase(debitPaymentInfoModel.getBaOwner()))
								{
									paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.debitcard"));
								}
								else if (("EFT").equalsIgnoreCase(debitPaymentInfoModel.getBaOwner()))
								{
									paymentDetail.setPaymentMethod(Config.getParameter("payU.paymentmethod.code.eft"));
								}
							}
							else
							{
								paymentDetail.setPaymentMethod(order.getPaymentMode() != null ? order.getPaymentMode().getDescription()
										: "PAYU PAYMENT");
							}
							paymentInfo.getPaymentDetail().add(paymentDetail);
							break;
						}
					}
					orderBody.setPaymentInfo(paymentInfo);
				}
			}
			catch (final Exception e)
			{
				log.error("Exception in sending payment details to ESB " + e.getMessage());
			}
			orderBody.setSalesTransaction(populateSalesTransactionDetail(order));
			orderBody.setCustomer(populateCustomer(order));
			orderReq.setBody(orderBody);
		}
		return orderReq;
	}

	private XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date)
	{
		log.info("+++++++++++++++++++Populate  XMLGregorianCalendar for Order++++++++++++++++++++");
		final GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		try
		{
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		}
		catch (final DatatypeConfigurationException e)
		{
			c.setTime(new Date());
			try
			{
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			}
			catch (final DatatypeConfigurationException e1)
			{
				log.error("Error in AbstractRegisterPageController class at convertDateToXMLGregorianCalendar() method : "
						+ e.getMessage());
			}
		}
		return null;
	}

	private AddressType populateAddress(final AddressModel address)
	{
		final AddressType addressType = new AddressType();
		log.info("+++++++++++++++++++Populate  AddressData for Order++++++++++++++++++++");
		final ContactDetailsType type = new ContactDetailsType();
		final List<ContactDetailsType.ContactDetail> contactDetailList = new ArrayList<ContactDetailsType.ContactDetail>();
		if (StringUtils.isNotBlank(address.getFirstname()))
		{
			addressType.setFirstName(replaceSpecialCharacters(address.getFirstname()));
		}
		if (StringUtils.isNotBlank(address.getLastname()))
		{
			addressType.setLastName(replaceSpecialCharacters(address.getLastname()));
		}
		if (StringUtils.isNotBlank(address.getEmail()))
		{
			addressType.setEmailAddress(replaceSpecialCharacters(address.getEmail()));
		}
		if (StringUtils.isNotBlank(address.getLine1()))
		{
			addressType.setAddressLine1(replaceSpecialCharacters(address.getLine1()));
		}
		if (StringUtils.isNotBlank(address.getLine2()))
		{
			addressType.setAddressLine2(replaceSpecialCharacters(address.getLine2()));
		}
		if (StringUtils.isNotBlank(address.getTown()))
		{
			addressType.setCity(replaceSpecialCharacters(address.getTown()));
		}
		if (StringUtils.isNotBlank(address.getTown()))
		{
			addressType.setSuburb(replaceSpecialCharacters(address.getTown()));
		}
		if (null != address.getCountry() && StringUtils.isNotBlank(address.getCountry().getName()))
		{
			addressType.setCountry(address.getCountry().getIsocode());
		}
		if (StringUtils.isNotBlank(address.getPostalcode()))
		{
			addressType.setPostalCode(address.getPostalcode());
		}
		if (StringUtils.isNotBlank(address.getSuburb()))
		{
			addressType.setSuburb(replaceSpecialCharacters(address.getSuburb()));
		}
		if (StringUtils.isNotBlank(address.getPhone1()))
		{
			final ContactDetail phnContactDetail = new ContactDetail();
			phnContactDetail.setTypeId("4");
			phnContactDetail.setNumber(address.getPhone1());
			contactDetailList.add(phnContactDetail);
		}
		if (StringUtils.isNotBlank(address.getCellphone()))
		{
			final ContactDetail mobContactDetail = new ContactDetail();
			mobContactDetail.setTypeId("2");
			mobContactDetail.setNumber(address.getCellphone());
			contactDetailList.add(mobContactDetail);
		}
		if (CollectionUtils.isNotEmpty(contactDetailList))
		{
			type.getContactDetail().addAll(contactDetailList);
			addressType.setContactDetails(type);
		}
		return addressType;
	}

	private DeliveryAddressType populateDeliveryAddress(final AddressModel address)
	{
		final DeliveryAddressType addressType = new DeliveryAddressType();
		log.info("+++++++++++++++++++Populate  AddressData for Order++++++++++++++++++++");
		final ContactDetailsType type = new ContactDetailsType();
		final List<ContactDetailsType.ContactDetail> contactDetailList = new ArrayList<ContactDetailsType.ContactDetail>();
		if (StringUtils.isNotBlank(address.getCompany()))
		{
			addressType.setDeliveryStoreId(address.getCompany());
			if (null != getPointOfServiceForName(address.getCompany())
					&& StringUtils.isNotBlank(getPointOfServiceForName(address.getCompany()).getDisplayName()))
			{
				addressType.setDeliveryStoreName(getPointOfServiceForName(address.getCompany()).getDisplayName());
			}
		}
		if (StringUtils.isNotBlank(address.getFirstname()))
		{
			addressType.setFirstName(replaceSpecialCharacters(address.getFirstname()));
		}
		if (StringUtils.isNotBlank(address.getLastname()))
		{
			addressType.setLastName(replaceSpecialCharacters(address.getLastname()));
		}
		if (StringUtils.isNotBlank(address.getEmail()))
		{
			addressType.setEmailAddress(replaceSpecialCharacters(address.getEmail()));
		}
		String line1 = "";
		String carriedOver = "";
		if (StringUtils.isNotBlank(address.getLine1()))
		{
			final StringBuilder buff = new StringBuilder();
			if (StringUtils.isNotBlank(address.getCompany()))
			{
				if (StringUtils.isNotBlank(addressType.getDeliveryStoreName()))
				{
					buff.append(addressType.getDeliveryStoreName());
					buff.append(",");
				}
				buff.append(address.getCompany());
				buff.append(",");
			}
			if (StringUtils.isNotBlank(address.getDepartment()) && !address.getLine1().contains(address.getDepartment()))
			{
				buff.append(address.getDepartment());
				buff.append(",");
			}
			if (StringUtils.isNotBlank(address.getAppartment()) && null != address.getLine2()
					&& !address.getLine2().contains(address.getAppartment()))
			{
				buff.append(address.getAppartment());
				buff.append(",");
			}
			buff.append(address.getLine1());
			line1 = buff.toString();
			line1 = replaceSpecialCharacters(line1);
			if (StringUtils.length(line1) > 50)
			{
				final int lastIndexOfSpace = StringUtils.substring(line1, 0, 50).lastIndexOf(" ");
				int carriedOverIndex = 50;
				if (line1.charAt(49) != ' ')
				{
					if (line1.charAt(50) != ' ')
					{
						addressType.setAddressLine1(StringUtils.substring(line1, 0, lastIndexOfSpace).trim());
						carriedOverIndex = lastIndexOfSpace + 1;

					}
					else
					{
						addressType.setAddressLine1(StringUtils.substring(line1, 0, 49));
						carriedOverIndex = 50;
					}

				}
				else if (line1.charAt(49) == ' ')
				{
					addressType.setAddressLine1(StringUtils.substring(line1, 0, 49).trim());
					carriedOverIndex = 50;
				}

				carriedOver = StringUtils.substring(line1, carriedOverIndex);
			}
			else
			{
				addressType.setAddressLine1(StringUtils.substring(line1, 0, 50));
			}
			//addressType.setAddressLine1(StringUtils.substring(line1, 0, 50));
		}
		//carriedOver = StringUtils.substring(line1, 50);

		if (StringUtils.isNotBlank(address.getLine2()))
		{
			addressType.setAddressLine2(carriedOver.concat(replaceSpecialCharacters(address.getLine2())));
		}
		else if (StringUtils.isNotEmpty(carriedOver))
		{
			addressType.setAddressLine2(carriedOver);
		}
		if (StringUtils.isNotBlank(address.getTown()))
		{
			addressType.setCity(replaceSpecialCharacters(address.getTown()));
		}
		if (StringUtils.isNotBlank(address.getSuburb()))
		{
			addressType.setSuburb(replaceSpecialCharacters(address.getSuburb()));
		}
		if (null != address.getCountry() && StringUtils.isNotBlank(address.getCountry().getName()))
		{
			addressType.setCountry(address.getCountry().getIsocode());
		}
		if (StringUtils.isNotBlank(address.getPostalcode()))
		{
			addressType.setPostalCode(address.getPostalcode());
		}
		if (StringUtils.isNotBlank(address.getPhone1()))
		{
			final ContactDetail phnContactDetail = new ContactDetail();
			phnContactDetail.setTypeId("4");
			phnContactDetail.setNumber(address.getPhone1());
			contactDetailList.add(phnContactDetail);
		}
		if (StringUtils.isNotBlank(address.getCellphone()))
		{
			final ContactDetail mobContactDetail = new ContactDetail();
			mobContactDetail.setTypeId("2");
			mobContactDetail.setNumber(address.getCellphone());
			contactDetailList.add(mobContactDetail);
		}
		if (CollectionUtils.isNotEmpty(contactDetailList))
		{
			type.getContactDetail().addAll(contactDetailList);
			addressType.setContactDetails(type);
		}
		return addressType;
	}

	private String replaceSpecialCharacters(final String value)
	{
		return StringUtils.isNotEmpty(value) ? value.replaceAll("\\p{Pd}", "-") : "";
	}

	private Order.Body.SalesTransaction populateSalesTransactionDetail(final OrderModel order)
	{
		log.info("+++++++++++++++++++Populate  SalesTransaction for Order++++++++++++++++++++");
		final List<AbstractOrderEntryModel> orderEntries = order.getEntries();
		final Order.Body.SalesTransaction.TransactionDetail transactionDetail = new Order.Body.SalesTransaction.TransactionDetail();
		final List<Object> lineItemList = new ArrayList<Object>();
		final Order.Body.SalesTransaction salesTransaction = new Order.Body.SalesTransaction();
		int lineItemId = 0;
		final PromotionOrderResults promotionOrderResults = promotionsService.getPromotionResults(order);
		final Map<Integer, PromotionResult> firedMessage = new HashMap<Integer, PromotionResult>();
		for (final PromotionResult prOrderResults : promotionOrderResults.getAppliedProductPromotions())
		{
			for (final PromotionOrderEntryConsumed entry : prOrderResults.getConsumedEntries())
			{
				firedMessage.put(entry.getOrderEntry().getEntryNumber(), prOrderResults);
			}
		}
		int discountLineId = 0;
		for (final AbstractOrderEntryModel orderEntry : orderEntries)
		{
			double orderEntryTaxVal = 14.00;
			if (CollectionUtils.isNotEmpty(orderEntry.getTaxValues()))
			{
				orderEntryTaxVal = orderEntry.getTaxValues().iterator().next().getValue();
			}
			int templineItemIdStart = lineItemId + 1;
			int templineItemIdEnd = 0;
			for (int item = 0; item < orderEntry.getQuantity().intValue(); item++)
			{
				lineItemId += 1;
				final LineItem lineItem = new LineItem();

				if (null != orderEntry.getProduct())
				{
					if (StringUtils.isNotBlank(orderEntry.getProduct().getCode()))
					{
						lineItem.setSKU(orderEntry.getProduct().getCode());
						lineItem.setId(lineItemId);
						lineItem.setProductTitle(orderEntry.getProduct().getName());
					}
					if (null != orderEntry.getBasePrice())
					{
						lineItem.setItemPrice(roundOffPrice(orderEntry.getBasePrice().doubleValue()));
					}
					if (null != orderEntry.getQuantity())
					{
						lineItem.setItemTax(calculcateItemTax(orderEntryTaxVal, orderEntry.getBasePrice()));
					}
					else
					{
						lineItem.setItemTax(roundOffPrice(0.00));
					}
					lineItemList.add(lineItem);
				}
				if ((orderEntry.getQuantity().intValue() == 2))
				{
					templineItemIdStart = lineItemId;
				}
				else if ((orderEntry.getQuantity().intValue() % 2 == 0) && item < (orderEntry.getQuantity().intValue() / 2))
				{
					templineItemIdStart = lineItemId;
				}
				else if ((orderEntry.getQuantity().intValue() % 2 != 0) && item < ((orderEntry.getQuantity().intValue() / 2) + 1))
				{
					templineItemIdStart = lineItemId;
				}

				if (item == (orderEntry.getQuantity().intValue() - 1))
				{
					templineItemIdEnd = lineItemId;
				}
			}
			int tempCount = 0;
			for (final DiscountValue discount : orderEntry.getDiscountValues())
			{
				final DiscountLineItem discountLineItem = new DiscountLineItem();
				final PromotionResult promotionResult = firedMessage.get(orderEntry.getEntryNumber());
				try
				{
					discountLineItem.setCorrelationId(promotionResult.getPromotion().getAttribute("correlationId").toString());
				}
				catch (final Exception e)
				{
					if (null != promotionResult.getPromotion())
					{
						discountLineItem.setCorrelationId(Config.getParameter("clicks.promotion.correlationId.prefix").concat(
								promotionResult.getPromotion().getPK().getLongValueAsString()));
					}
				}
				final PromotionOrderEntryConsumed promotionOrderEntryConsumed = ((PromotionOrderEntryConsumed) CollectionUtils.find(
						promotionResult.getConsumedEntries(),
						new BeanPropertyValueEqualsPredicate("orderEntry.entryNumber", orderEntry.getEntryNumber(), true)));
				final String promotionResultDescription = replacePartnerProductName(promotionOrderEntryConsumed,
						promotionResult.getDescription());

				//StringUtils.replace(promotionResultDescription, "ProductName", orderEntry.getProduct().getName());

				if (StringUtils.isNotBlank(promotionResultDescription))
				{
					discountLineItem.setDescription(promotionResultDescription);
				}

				if (CollectionUtils.isNotEmpty(orderEntry.getTaxValues()))
				{
					discountLineItem.setDiscountTax(calculcateItemTax(orderEntryTaxVal, discount.getAppliedValue()));
				}
				else
				{
					discountLineItem.setDiscountTax(roundOffPrice(0.00));
				}
				if (tempCount > 0)
				{
					//templineItemIdStart += 2;
					discountLineItem.setLineItemId(Integer.valueOf(templineItemIdEnd));
				}
				else
				{
					discountLineItem.setLineItemId(Integer.valueOf(templineItemIdStart));
				}

				discountLineItem.setDiscountValue(roundOffPrice(discount.getAppliedValue()));
				discountLineItem.setDiscountType(1);
				discountLineId += 1;
				discountLineItem.setId(discountLineId);
				lineItemList.add(discountLineItem);
				tempCount += 1;
			}
		}
		for (final PromotionResult prOrderResults : promotionOrderResults.getAppliedOrderPromotions())
		{
			final DiscountLineItem discountLineItem = new DiscountLineItem();
			discountLineItem.setDiscountValue(roundOffPrice(prOrderResults.getTotalDiscount()));
			discountLineItem.setDiscountType(2);
			try
			{
				discountLineItem.setCorrelationId(prOrderResults.getPromotion().getAttribute("correlationId").toString());
			}
			catch (final Exception e)
			{
				if (null != prOrderResults.getPromotion())
				{
					discountLineItem.setCorrelationId(Config.getParameter("clicks.promotion.correlationId.prefix").concat(
							prOrderResults.getPromotion().getPK().getLongValueAsString()));
				}
			}
			discountLineItem.setDiscountTax(roundOffPrice(prOrderResults.getTotalDiscount() * 0.14d / 1.14));
			discountLineItem.setDescription(prOrderResults.getDescription());
			discountLineId += 1;
			discountLineItem.setId(discountLineId);
			lineItemList.add(discountLineItem);
		}
		if (CollectionUtils.isNotEmpty(order.getDiscounts()))
		{
			try
			{
				for (final DiscountModel discount : order.getDiscounts())
				{
					if (discount instanceof PromotionVoucherModel)
					{
						final PromotionVoucherModel promotionVoucherModel = (PromotionVoucherModel) discount;
						final DiscountLineItem discountLineItem = new DiscountLineItem();
						discountLineItem.setDiscountValue(roundOffPrice(voucherModelService.getAppliedValue(promotionVoucherModel,
								order).getValue()));
						discountLineItem.setDiscountType(2);

						discountLineItem.setCorrelationId("VO1_" + discount.getPk().getLongValueAsString());

						discountLineItem.setDiscountTax(roundOffPrice(discount.getValue() * 0.14d / 1.14));
						discountLineItem.setDescription(promotionVoucherModel.getDescription());
						discountLineId += 1;
						discountLineItem.setId(discountLineId);
						lineItemList.add(discountLineItem);
					}
				}
			}
			catch (final Exception e)
			{
				//
			}
		}

		transactionDetail.getLineItemOrDiscountLineItem().addAll(lineItemList);
		salesTransaction.setTransactionDetail(transactionDetail);
		salesTransaction.setSubtotal(roundOffPrice(order.getSubtotal().doubleValue()));
		salesTransaction.setTotalDiscount(roundOffPrice(populateTotalDiscountsOnOrder(order)));
		salesTransaction.setDeliveryCost(roundOffPrice(order.getDeliveryCost().doubleValue()));
		salesTransaction.setTotalTax(roundOffPrice(order.getTotalTax().doubleValue()));
		salesTransaction.setTotal(roundOffPrice(order.getTotalPrice().doubleValue()));
		return salesTransaction;
	}

	private Order.Body.Customer populateCustomer(final OrderModel order)
	{
		log.info("+++++++++++++++++++Populate  Customer for Order++++++++++++++++++++");
		final Order.Body.Customer customer = new Order.Body.Customer();
		final Order.Body.Customer.CoreDetails coreDetails = new Order.Body.Customer.CoreDetails();
		final ContactDetailsType type = new ContactDetailsType();
		final Order.Body.Customer.Hybris hybrisData = new Order.Body.Customer.Hybris();
		final Order.Body.Customer.Loyalty loyaltyData = new Order.Body.Customer.Loyalty();
		final List<ContactDetailsType.ContactDetail> contactDetailList = new ArrayList<ContactDetailsType.ContactDetail>();
		AddressModel addressData = null;

		if (CustomerType.GUEST.equals(((CustomerModel) order.getUser()).getType()))
		{
			if (null != order.getPaymentAddress())
			{
				addressData = order.getPaymentAddress();
				if (null != addressData)
				{
					if (StringUtils.isNotBlank(addressData.getFirstname()))
					{
						coreDetails.setFirstName(addressData.getFirstname());
					}
					if (StringUtils.isNotBlank(addressData.getLastname()))
					{
						coreDetails.setLastName(addressData.getLastname());
					}
					if (StringUtils.isNotBlank(addressData.getEmail()))
					{
						coreDetails.setEmailAddress(addressData.getEmail());
					}
					if (StringUtils.isNotBlank(addressData.getPhone1()))
					{
						final ContactDetail phnContactDetail = new ContactDetail();
						phnContactDetail.setTypeId("4");
						phnContactDetail.setNumber(addressData.getPhone1());
						contactDetailList.add(phnContactDetail);
					}
					if (StringUtils.isNotBlank(addressData.getCellphone()))
					{
						final ContactDetail mobContactDetail = new ContactDetail();
						mobContactDetail.setTypeId("2");
						mobContactDetail.setNumber(addressData.getCellphone());
						contactDetailList.add(mobContactDetail);
					}
					if (CollectionUtils.isNotEmpty(contactDetailList))
					{
						type.getContactDetail().addAll(contactDetailList);
						coreDetails.setContactDetails(type);
					}
				}
			}
		}
		else
		{
			final CustomerModel customerModel = (CustomerModel) order.getUser();
			if (StringUtils.isNotBlank(customerModel.getCustomerID()))
			{
				hybrisData.setRetailerId(Config.getParameter("clickswebclient.order.retailerId"));
				hybrisData.setCustomerId(customerModel.getCustomerID());
				customer.setHybris(hybrisData);
			}
			if (StringUtils.isNotBlank(customerModel.getMemberID()))
			{
				loyaltyData.setRetailerId(Config.getParameter("clickswebclient.order.retailerId"));
				loyaltyData.setMemberId(customerModel.getMemberID());
				customer.setLoyalty(loyaltyData);
			}
			if (StringUtils.isNotBlank(customerModel.getFirstName()))
			{
				coreDetails.setFirstName(customerModel.getFirstName());
			}
			if (StringUtils.isNotBlank(customerModel.getLastName()))
			{
				coreDetails.setLastName(customerModel.getLastName());
			}
			if (StringUtils.isNotBlank(customerModel.getEmailID()))
			{
				coreDetails.setEmailAddress(customerModel.getEmailID());
			}
			if (CollectionUtils.isNotEmpty(customerModel.getContactDetails()))
			{
				ContactDetail contactDetail = null;
				for (final ContactDetailsModel cusContactDetail : customerModel.getContactDetails())
				{
					if (StringUtils.isNotEmpty(cusContactDetail.getNumber()))
					{
						contactDetail = new ContactDetail();
						contactDetail.setTypeId(cusContactDetail.getTypeID());
						contactDetail.setNumber(cusContactDetail.getNumber());
						contactDetailList.add(contactDetail);
					}
				}
			}
			if (CollectionUtils.isNotEmpty(contactDetailList))
			{
				type.getContactDetail().addAll(contactDetailList);
				coreDetails.setContactDetails(type);
			}
		}
		customer.setCoreDetails(coreDetails);
		return customer;
	}

	private double populateTotalDiscountsOnOrder(final OrderModel orderModel)
	{
		final double productsDiscountsAmount = getProductsDiscountsAmount(orderModel);
		final double orderDiscountsAmount = getOrderDiscountsAmount(orderModel);
		return productsDiscountsAmount + orderDiscountsAmount;
	}

	private double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}

	private double getOrderDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (value > 0.0d)
				{
					discounts += value;
				}
			}
		}

		return discounts;
	}

	private BigDecimal calculcateItemTax(final double orderEntryTaxVal, final double price)
	{
		final double itemTax = (price * (orderEntryTaxVal / 100) / (1 + (orderEntryTaxVal / 100)));
		return roundOffPrice(itemTax);
	}

	private BigDecimal roundOffPrice(final double value)
	{
		final double roundedVal = commonI18NService.roundCurrency(value, 2);
		return new BigDecimal(roundedVal).setScale(2, RoundingMode.HALF_UP);
	}

	public PointOfServiceModel getPointOfServiceForName(final String name) throws UnknownIdentifierException
	{
		ServicesUtil.validateParameterNotNull(name, "name");
		try
		{
			return getPointOfServiceDao().getPosByName(name);
		}
		catch (final PointOfServiceDaoException e)
		{
			throw new UnknownIdentifierException(e.getMessage(), e);
		}
	}

	private String replacePartnerProductName(final PromotionOrderEntryConsumed promotionOrderEntryConsumed,
			String promotionResultDescription)
	{
		try
		{
			String perfProdName = "";
			if (StringUtils.containsIgnoreCase(promotionResultDescription, "PartnerProductName"))
			{
				final StringBuilder buff = new StringBuilder();
				if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof ProductPerfectPartnerPromotion)
				{
					for (final Product product : ((ProductPerfectPartnerPromotion) (promotionOrderEntryConsumed.getPromotionResult()
							.getPromotion())).getPartnerProducts())
					{
						buff.append(product.getName());
						buff.append(",");
					}
					perfProdName = buff.toString();
				}
				else if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof ProductOneToOnePerfectPartnerPromotion)
				{
					perfProdName = ((ProductOneToOnePerfectPartnerPromotion) (promotionOrderEntryConsumed.getPromotionResult()
							.getPromotion())).getPartnerProduct().getName();
				}
				else if (promotionOrderEntryConsumed.getPromotionResult().getPromotion() instanceof ProductPerfectPartnerBundlePromotion)
				{
					for (final Product product : ((ProductPerfectPartnerBundlePromotion) (promotionOrderEntryConsumed
							.getPromotionResult().getPromotion())).getPartnerProducts())
					{
						buff.append(product.getName());
						buff.append(",");
					}
					perfProdName = buff.toString();
				}
				if (promotionResultDescription.contains("PartnerProductName, PartnerProductName, PartnerProductName"))
				{
					promotionResultDescription = promotionResultDescription.replaceAll(
							"PartnerProductName, PartnerProductName, PartnerProductName", perfProdName);
				}
				else
				{
					promotionResultDescription = promotionResultDescription.replaceAll("PartnerProductName", perfProdName);
				}
			}
			if (StringUtils.containsIgnoreCase(promotionResultDescription, "productname"))
			{
				for (final PromotionOrderEntryConsumed promotionConsumed : promotionOrderEntryConsumed.getPromotionResult()
						.getConsumedEntries())
				{
					if (StringUtils.length(perfProdName) > 0
							&& !perfProdName.equalsIgnoreCase(promotionConsumed.getProduct().getName()))
					{
						if (StringUtils.contains(promotionResultDescription, "productName"))
						{
							promotionResultDescription = StringUtils.replace(promotionResultDescription, "productName",
									promotionConsumed.getProduct().getName());
						}
						else
						{
							promotionResultDescription = StringUtils.replace(promotionResultDescription, "ProductName",
									promotionConsumed.getProduct().getName());
						}
						return promotionResultDescription;
					}
				}
				if (StringUtils.contains(promotionResultDescription, "productName"))
				{
					promotionResultDescription = StringUtils.replace(promotionResultDescription, "productName",
							promotionOrderEntryConsumed.getProduct().getName());
				}
				else
				{
					promotionResultDescription = StringUtils.replace(promotionResultDescription, "ProductName",
							promotionOrderEntryConsumed.getProduct().getName());
				}
			}
		}
		catch (final Exception e)
		{
			//
		}
		return promotionResultDescription;
	}
}
