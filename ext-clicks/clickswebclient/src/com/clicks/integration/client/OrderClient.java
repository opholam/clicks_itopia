/**
 *
 */
package com.clicks.integration.client;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.services.BaseStoreService;

import org.apache.log4j.Logger;

import com.clicks.integration.service.OrderIntegrationService;



public class OrderClient
{
	static Logger log = Logger.getLogger(OrderClient.class.getName());

	OrderIntegrationService orderIntegrationService;

	public void publishNewOrder(final OrderModel order)
	{

		orderIntegrationService.publishNewOrder(order);
	}

	public static void main(final String[] arg)
	{
		Registry.activateMasterTenant();
		final OrderClient client = new OrderClient();
		client.orderIntegrationService = Registry.getApplicationContext().getBean("orderIntegrationService",
				OrderIntegrationService.class);
		final CustomerAccountService customerAccountService = Registry.getApplicationContext().getBean("customerAccountService",
				CustomerAccountService.class);
		final BaseStoreService baseStoreService = Registry.getApplicationContext().getBean("baseStoreService",
				BaseStoreService.class);
		client.publishNewOrder(customerAccountService.getGuestOrderForGUID("fc12f71b-0212-43d5-8e40-99a0553ebd64",
				baseStoreService.getBaseStoreForUid("clicks")));
	}

}
