
package catalog.omni.biztalk.clicks.schema.onlinecatalogue;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DataSource" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreatedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Body">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice>
 *                     &lt;element name="Items" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="Item" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;all>
 *                                         &lt;element name="OnlineHierarchy" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="HierarchyLink" maxOccurs="unbounded">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="ProductImages" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="Image" maxOccurs="unbounded">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;attribute name="Url" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                           &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                           &lt;attribute name="Size" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                           &lt;attribute name="MainImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="Media" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="MediaItem" maxOccurs="unbounded">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                           &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/all>
 *                                       &lt;attribute name="SKU" use="required">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="18"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/attribute>
 *                                       &lt;attribute name="OnlineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                       &lt;attribute name="HasImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                       &lt;attribute name="DisplayOnline" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                       &lt;attribute name="HasCopywrite" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                       &lt;attribute name="CatalogueVersion">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="50"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/attribute>
 *                                       &lt;attribute name="Status">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="20"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/attribute>
 *                                       &lt;attribute name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                                       &lt;attribute name="LastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="GeneralTables" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="OnlineHierarchy" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ItemHierarchy" maxOccurs="unbounded">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                 &lt;attribute name="CategoryDescription" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                 &lt;attribute name="ParentCategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                 &lt;/sequence>
 *                 &lt;attribute name="RetailerId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "body"
})
@XmlRootElement(name = "OnlineCatalogue")
public class OnlineCatalogue {

    @XmlElement(name = "Header", required = true)
    protected OnlineCatalogue.Header header;
    @XmlElement(name = "Body", required = true)
    protected OnlineCatalogue.Body body;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link OnlineCatalogue.Header }
     *     
     */
    public OnlineCatalogue.Header getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnlineCatalogue.Header }
     *     
     */
    public void setHeader(OnlineCatalogue.Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link OnlineCatalogue.Body }
     *     
     */
    public OnlineCatalogue.Body getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnlineCatalogue.Body }
     *     
     */
    public void setBody(OnlineCatalogue.Body value) {
        this.body = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="Items" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="Item" maxOccurs="unbounded">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;all>
     *                               &lt;element name="OnlineHierarchy" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="HierarchyLink" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="ProductImages" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="Image" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;attribute name="Url" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                 &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                                 &lt;attribute name="Size" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                                 &lt;attribute name="MainImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="Media" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="MediaItem" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                                 &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/all>
     *                             &lt;attribute name="SKU" use="required">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="18"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/attribute>
     *                             &lt;attribute name="OnlineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                             &lt;attribute name="HasImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                             &lt;attribute name="DisplayOnline" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                             &lt;attribute name="HasCopywrite" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                             &lt;attribute name="CatalogueVersion">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="50"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/attribute>
     *                             &lt;attribute name="Status">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="20"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/attribute>
     *                             &lt;attribute name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *                             &lt;attribute name="LastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="GeneralTables" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="OnlineHierarchy" maxOccurs="unbounded">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element name="ItemHierarchy" maxOccurs="unbounded">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                       &lt;attribute name="CategoryDescription" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                       &lt;attribute name="ParentCategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *         &lt;/choice>
     *       &lt;/sequence>
     *       &lt;attribute name="RetailerId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "items",
        "generalTables"
    })
    public static class Body {

        @XmlElement(name = "Items")
        protected OnlineCatalogue.Body.Items items;
        @XmlElement(name = "GeneralTables")
        protected OnlineCatalogue.Body.GeneralTables generalTables;
        @XmlAttribute(name = "RetailerId", required = true)
        protected String retailerId;

        /**
         * Gets the value of the items property.
         * 
         * @return
         *     possible object is
         *     {@link OnlineCatalogue.Body.Items }
         *     
         */
        public OnlineCatalogue.Body.Items getItems() {
            return items;
        }

        /**
         * Sets the value of the items property.
         * 
         * @param value
         *     allowed object is
         *     {@link OnlineCatalogue.Body.Items }
         *     
         */
        public void setItems(OnlineCatalogue.Body.Items value) {
            this.items = value;
        }

        /**
         * Gets the value of the generalTables property.
         * 
         * @return
         *     possible object is
         *     {@link OnlineCatalogue.Body.GeneralTables }
         *     
         */
        public OnlineCatalogue.Body.GeneralTables getGeneralTables() {
            return generalTables;
        }

        /**
         * Sets the value of the generalTables property.
         * 
         * @param value
         *     allowed object is
         *     {@link OnlineCatalogue.Body.GeneralTables }
         *     
         */
        public void setGeneralTables(OnlineCatalogue.Body.GeneralTables value) {
            this.generalTables = value;
        }

        /**
         * Gets the value of the retailerId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRetailerId() {
            return retailerId;
        }

        /**
         * Sets the value of the retailerId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRetailerId(String value) {
            this.retailerId = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="OnlineHierarchy" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ItemHierarchy" maxOccurs="unbounded">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="CategoryDescription" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="ParentCategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "onlineHierarchy"
        })
        public static class GeneralTables {

            @XmlElement(name = "OnlineHierarchy", required = true)
            protected List<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy> onlineHierarchy;

            /**
             * Gets the value of the onlineHierarchy property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the onlineHierarchy property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOnlineHierarchy().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link OnlineCatalogue.Body.GeneralTables.OnlineHierarchy }
             * 
             * 
             */
            public List<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy> getOnlineHierarchy() {
                if (onlineHierarchy == null) {
                    onlineHierarchy = new ArrayList<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy>();
                }
                return this.onlineHierarchy;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ItemHierarchy" maxOccurs="unbounded">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="CategoryDescription" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="ParentCategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "itemHierarchy"
            })
            public static class OnlineHierarchy {

                @XmlElement(name = "ItemHierarchy", required = true)
                protected List<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy> itemHierarchy;

                /**
                 * Gets the value of the itemHierarchy property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the itemHierarchy property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getItemHierarchy().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy }
                 * 
                 * 
                 */
                public List<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy> getItemHierarchy() {
                    if (itemHierarchy == null) {
                        itemHierarchy = new ArrayList<OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy>();
                    }
                    return this.itemHierarchy;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="CategoryDescription" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="ParentCategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ItemHierarchy {

                    @XmlAttribute(name = "CategoryId", required = true)
                    protected String categoryId;
                    @XmlAttribute(name = "CategoryDescription", required = true)
                    protected String categoryDescription;
                    @XmlAttribute(name = "ParentCategoryId", required = true)
                    protected String parentCategoryId;

                    /**
                     * Gets the value of the categoryId property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCategoryId() {
                        return categoryId;
                    }

                    /**
                     * Sets the value of the categoryId property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCategoryId(String value) {
                        this.categoryId = value;
                    }

                    /**
                     * Gets the value of the categoryDescription property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCategoryDescription() {
                        return categoryDescription;
                    }

                    /**
                     * Sets the value of the categoryDescription property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCategoryDescription(String value) {
                        this.categoryDescription = value;
                    }

                    /**
                     * Gets the value of the parentCategoryId property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParentCategoryId() {
                        return parentCategoryId;
                    }

                    /**
                     * Sets the value of the parentCategoryId property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParentCategoryId(String value) {
                        this.parentCategoryId = value;
                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Item" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;all>
         *                   &lt;element name="OnlineHierarchy" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="HierarchyLink" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="ProductImages" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Image" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="Url" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                                     &lt;attribute name="Size" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                                     &lt;attribute name="MainImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Media" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="MediaItem" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                                     &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/all>
         *                 &lt;attribute name="SKU" use="required">
         *                   &lt;simpleType>
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                       &lt;maxLength value="18"/>
         *                     &lt;/restriction>
         *                   &lt;/simpleType>
         *                 &lt;/attribute>
         *                 &lt;attribute name="OnlineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="HasImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                 &lt;attribute name="DisplayOnline" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                 &lt;attribute name="HasCopywrite" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                 &lt;attribute name="CatalogueVersion">
         *                   &lt;simpleType>
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                       &lt;maxLength value="50"/>
         *                     &lt;/restriction>
         *                   &lt;/simpleType>
         *                 &lt;/attribute>
         *                 &lt;attribute name="Status">
         *                   &lt;simpleType>
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                       &lt;maxLength value="20"/>
         *                     &lt;/restriction>
         *                   &lt;/simpleType>
         *                 &lt;/attribute>
         *                 &lt;attribute name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
         *                 &lt;attribute name="LastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "item"
        })
        public static class Items {

            @XmlElement(name = "Item", required = true)
            protected List<OnlineCatalogue.Body.Items.Item> item;

            /**
             * Gets the value of the item property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the item property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getItem().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link OnlineCatalogue.Body.Items.Item }
             * 
             * 
             */
            public List<OnlineCatalogue.Body.Items.Item> getItem() {
                if (item == null) {
                    item = new ArrayList<OnlineCatalogue.Body.Items.Item>();
                }
                return this.item;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;all>
             *         &lt;element name="OnlineHierarchy" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="HierarchyLink" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="ProductImages" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Image" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="Url" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}int" />
             *                           &lt;attribute name="Size" type="{http://www.w3.org/2001/XMLSchema}int" />
             *                           &lt;attribute name="MainImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Media" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="MediaItem" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
             *                           &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/all>
             *       &lt;attribute name="SKU" use="required">
             *         &lt;simpleType>
             *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *             &lt;maxLength value="18"/>
             *           &lt;/restriction>
             *         &lt;/simpleType>
             *       &lt;/attribute>
             *       &lt;attribute name="OnlineDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="HasImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *       &lt;attribute name="DisplayOnline" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *       &lt;attribute name="HasCopywrite" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *       &lt;attribute name="CatalogueVersion">
             *         &lt;simpleType>
             *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *             &lt;maxLength value="50"/>
             *           &lt;/restriction>
             *         &lt;/simpleType>
             *       &lt;/attribute>
             *       &lt;attribute name="Status">
             *         &lt;simpleType>
             *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *             &lt;maxLength value="20"/>
             *           &lt;/restriction>
             *         &lt;/simpleType>
             *       &lt;/attribute>
             *       &lt;attribute name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
             *       &lt;attribute name="LastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {

            })
            public static class Item {

                @XmlElement(name = "OnlineHierarchy")
                protected OnlineCatalogue.Body.Items.Item.OnlineHierarchy onlineHierarchy;
                @XmlElement(name = "ProductImages")
                protected OnlineCatalogue.Body.Items.Item.ProductImages productImages;
                @XmlElement(name = "Media")
                protected OnlineCatalogue.Body.Items.Item.Media media;
                @XmlAttribute(name = "SKU", required = true)
                protected String sku;
                @XmlAttribute(name = "OnlineDescription")
                protected String onlineDescription;
                @XmlAttribute(name = "HasImage")
                protected Boolean hasImage;
                @XmlAttribute(name = "DisplayOnline")
                protected Boolean displayOnline;
                @XmlAttribute(name = "HasCopywrite")
                protected Boolean hasCopywrite;
                @XmlAttribute(name = "CatalogueVersion")
                protected String catalogueVersion;
                @XmlAttribute(name = "Status")
                protected String status;
                @XmlAttribute(name = "CreationDate")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar creationDate;
                @XmlAttribute(name = "LastUpdatedDate")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar lastUpdatedDate;

                /**
                 * Gets the value of the onlineHierarchy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OnlineCatalogue.Body.Items.Item.OnlineHierarchy }
                 *     
                 */
                public OnlineCatalogue.Body.Items.Item.OnlineHierarchy getOnlineHierarchy() {
                    return onlineHierarchy;
                }

                /**
                 * Sets the value of the onlineHierarchy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OnlineCatalogue.Body.Items.Item.OnlineHierarchy }
                 *     
                 */
                public void setOnlineHierarchy(OnlineCatalogue.Body.Items.Item.OnlineHierarchy value) {
                    this.onlineHierarchy = value;
                }

                /**
                 * Gets the value of the productImages property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OnlineCatalogue.Body.Items.Item.ProductImages }
                 *     
                 */
                public OnlineCatalogue.Body.Items.Item.ProductImages getProductImages() {
                    return productImages;
                }

                /**
                 * Sets the value of the productImages property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OnlineCatalogue.Body.Items.Item.ProductImages }
                 *     
                 */
                public void setProductImages(OnlineCatalogue.Body.Items.Item.ProductImages value) {
                    this.productImages = value;
                }

                /**
                 * Gets the value of the media property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OnlineCatalogue.Body.Items.Item.Media }
                 *     
                 */
                public OnlineCatalogue.Body.Items.Item.Media getMedia() {
                    return media;
                }

                /**
                 * Sets the value of the media property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OnlineCatalogue.Body.Items.Item.Media }
                 *     
                 */
                public void setMedia(OnlineCatalogue.Body.Items.Item.Media value) {
                    this.media = value;
                }

                /**
                 * Gets the value of the sku property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSKU() {
                    return sku;
                }

                /**
                 * Sets the value of the sku property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSKU(String value) {
                    this.sku = value;
                }

                /**
                 * Gets the value of the onlineDescription property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOnlineDescription() {
                    return onlineDescription;
                }

                /**
                 * Sets the value of the onlineDescription property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOnlineDescription(String value) {
                    this.onlineDescription = value;
                }

                /**
                 * Gets the value of the hasImage property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isHasImage() {
                    return hasImage;
                }

                /**
                 * Sets the value of the hasImage property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setHasImage(Boolean value) {
                    this.hasImage = value;
                }

                /**
                 * Gets the value of the displayOnline property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isDisplayOnline() {
                    return displayOnline;
                }

                /**
                 * Sets the value of the displayOnline property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setDisplayOnline(Boolean value) {
                    this.displayOnline = value;
                }

                /**
                 * Gets the value of the hasCopywrite property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isHasCopywrite() {
                    return hasCopywrite;
                }

                /**
                 * Sets the value of the hasCopywrite property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setHasCopywrite(Boolean value) {
                    this.hasCopywrite = value;
                }

                /**
                 * Gets the value of the catalogueVersion property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCatalogueVersion() {
                    return catalogueVersion;
                }

                /**
                 * Sets the value of the catalogueVersion property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCatalogueVersion(String value) {
                    this.catalogueVersion = value;
                }

                /**
                 * Gets the value of the status property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStatus() {
                    return status;
                }

                /**
                 * Sets the value of the status property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStatus(String value) {
                    this.status = value;
                }

                /**
                 * Gets the value of the creationDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getCreationDate() {
                    return creationDate;
                }

                /**
                 * Sets the value of the creationDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setCreationDate(XMLGregorianCalendar value) {
                    this.creationDate = value;
                }

                /**
                 * Gets the value of the lastUpdatedDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getLastUpdatedDate() {
                    return lastUpdatedDate;
                }

                /**
                 * Sets the value of the lastUpdatedDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setLastUpdatedDate(XMLGregorianCalendar value) {
                    this.lastUpdatedDate = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="MediaItem" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *                 &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "mediaItem"
                })
                public static class Media {

                    @XmlElement(name = "MediaItem", required = true)
                    protected List<OnlineCatalogue.Body.Items.Item.Media.MediaItem> mediaItem;

                    /**
                     * Gets the value of the mediaItem property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the mediaItem property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getMediaItem().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link OnlineCatalogue.Body.Items.Item.Media.MediaItem }
                     * 
                     * 
                     */
                    public List<OnlineCatalogue.Body.Items.Item.Media.MediaItem> getMediaItem() {
                        if (mediaItem == null) {
                            mediaItem = new ArrayList<OnlineCatalogue.Body.Items.Item.Media.MediaItem>();
                        }
                        return this.mediaItem;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
                     *       &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class MediaItem {

                        @XmlAttribute(name = "Type", required = true)
                        protected int type;
                        @XmlAttribute(name = "Value", required = true)
                        protected String value;

                        /**
                         * Gets the value of the type property.
                         * 
                         */
                        public int getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         */
                        public void setType(int value) {
                            this.type = value;
                        }

                        /**
                         * Gets the value of the value property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getValue() {
                            return value;
                        }

                        /**
                         * Sets the value of the value property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setValue(String value) {
                            this.value = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="HierarchyLink" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "hierarchyLink"
                })
                public static class OnlineHierarchy {

                    @XmlElement(name = "HierarchyLink", required = true)
                    protected List<OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink> hierarchyLink;

                    /**
                     * Gets the value of the hierarchyLink property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the hierarchyLink property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getHierarchyLink().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink }
                     * 
                     * 
                     */
                    public List<OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink> getHierarchyLink() {
                        if (hierarchyLink == null) {
                            hierarchyLink = new ArrayList<OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink>();
                        }
                        return this.hierarchyLink;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="CategoryId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class HierarchyLink {

                        @XmlAttribute(name = "CategoryId", required = true)
                        protected String categoryId;

                        /**
                         * Gets the value of the categoryId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCategoryId() {
                            return categoryId;
                        }

                        /**
                         * Sets the value of the categoryId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCategoryId(String value) {
                            this.categoryId = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Image" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="Url" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *                 &lt;attribute name="Size" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *                 &lt;attribute name="MainImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "image"
                })
                public static class ProductImages {

                    @XmlElement(name = "Image", required = true)
                    protected List<OnlineCatalogue.Body.Items.Item.ProductImages.Image> image;

                    /**
                     * Gets the value of the image property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the image property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getImage().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link OnlineCatalogue.Body.Items.Item.ProductImages.Image }
                     * 
                     * 
                     */
                    public List<OnlineCatalogue.Body.Items.Item.ProductImages.Image> getImage() {
                        if (image == null) {
                            image = new ArrayList<OnlineCatalogue.Body.Items.Item.ProductImages.Image>();
                        }
                        return this.image;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="Url" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}int" />
                     *       &lt;attribute name="Size" type="{http://www.w3.org/2001/XMLSchema}int" />
                     *       &lt;attribute name="MainImage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Image {

                        @XmlAttribute(name = "Url")
                        protected String url;
                        @XmlAttribute(name = "Type")
                        protected Integer type;
                        @XmlAttribute(name = "Size")
                        protected Integer size;
                        @XmlAttribute(name = "MainImage")
                        protected Boolean mainImage;

                        /**
                         * Gets the value of the url property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getUrl() {
                            return url;
                        }

                        /**
                         * Sets the value of the url property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setUrl(String value) {
                            this.url = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Integer getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setType(Integer value) {
                            this.type = value;
                        }

                        /**
                         * Gets the value of the size property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Integer getSize() {
                            return size;
                        }

                        /**
                         * Sets the value of the size property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setSize(Integer value) {
                            this.size = value;
                        }

                        /**
                         * Gets the value of the mainImage property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isMainImage() {
                            return mainImage;
                        }

                        /**
                         * Sets the value of the mainImage property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setMainImage(Boolean value) {
                            this.mainImage = value;
                        }

                    }

                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DataSource" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventLogId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreatedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataSource",
        "eventLogId",
        "eventCode",
        "createdDateTime"
    })
    public static class Header {

        @XmlElement(name = "DataSource", required = true)
        protected String dataSource;
        @XmlElement(name = "EventLogId", required = true)
        protected String eventLogId;
        @XmlElement(name = "EventCode", required = true)
        protected String eventCode;
        @XmlElement(name = "CreatedDateTime", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar createdDateTime;

        /**
         * Gets the value of the dataSource property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDataSource() {
            return dataSource;
        }

        /**
         * Sets the value of the dataSource property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDataSource(String value) {
            this.dataSource = value;
        }

        /**
         * Gets the value of the eventLogId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventLogId() {
            return eventLogId;
        }

        /**
         * Sets the value of the eventLogId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventLogId(String value) {
            this.eventLogId = value;
        }

        /**
         * Gets the value of the eventCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventCode() {
            return eventCode;
        }

        /**
         * Sets the value of the eventCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventCode(String value) {
            this.eventCode = value;
        }

        /**
         * Gets the value of the createdDateTime property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCreatedDateTime() {
            return createdDateTime;
        }

        /**
         * Sets the value of the createdDateTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCreatedDateTime(XMLGregorianCalendar value) {
            this.createdDateTime = value;
        }

    }

}
