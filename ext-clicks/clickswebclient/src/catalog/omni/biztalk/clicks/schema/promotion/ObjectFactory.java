
package catalog.omni.biztalk.clicks.schema.promotion;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the omni.biztalk.clicks.schema.promotion package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: omni.biztalk.clicks.schema.promotion
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PromotionMessage }
     * 
     */
    public PromotionMessage createPromotionMessage() {
        return new PromotionMessage();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body }
     * 
     */
    public PromotionMessage.Body createPromotionMessageBody() {
        return new PromotionMessage.Body();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions }
     * 
     */
    public PromotionMessage.Body.Promotions createPromotionMessageBodyPromotions() {
        return new PromotionMessage.Body.Promotions();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion createPromotionMessageBodyPromotionsPromotion() {
        return new PromotionMessage.Body.Promotions.Promotion();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Messages }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Messages createPromotionMessageBodyPromotionsPromotionMessages() {
        return new PromotionMessage.Body.Promotions.Promotion.Messages();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Media }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Media createPromotionMessageBodyPromotionsPromotionMedia() {
        return new PromotionMessage.Body.Promotions.Promotion.Media();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Restrictions }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Restrictions createPromotionMessageBodyPromotionsPromotionRestrictions() {
        return new PromotionMessage.Body.Promotions.Promotion.Restrictions();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction createPromotionMessageBodyPromotionsPromotionRestrictionsRestriction() {
        return new PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Buckets }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Buckets createPromotionMessageBodyPromotionsPromotionBuckets() {
        return new PromotionMessage.Body.Promotions.Promotion.Buckets();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Rewards }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Rewards createPromotionMessageBodyPromotionsPromotionRewards() {
        return new PromotionMessage.Body.Promotions.Promotion.Rewards();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Rewards.Reward }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Rewards.Reward createPromotionMessageBodyPromotionsPromotionRewardsReward() {
        return new PromotionMessage.Body.Promotions.Promotion.Rewards.Reward();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets createPromotionMessageBodyPromotionsPromotionRewardsRewardBuckets() {
        return new PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Triggers }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Triggers createPromotionMessageBodyPromotionsPromotionTriggers() {
        return new PromotionMessage.Body.Promotions.Promotion.Triggers();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger createPromotionMessageBodyPromotionsPromotionTriggersTrigger() {
        return new PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets createPromotionMessageBodyPromotionsPromotionTriggersTriggerBuckets() {
        return new PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets();
    }

    /**
     * Create an instance of {@link PromotionMessage.Header }
     * 
     */
    public PromotionMessage.Header createPromotionMessageHeader() {
        return new PromotionMessage.Header();
    }

    /**
     * Create an instance of {@link MonetaryValue }
     * 
     */
    public MonetaryValue createMonetaryValue() {
        return new MonetaryValue();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Properties }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Properties createPromotionMessageBodyPromotionsPromotionProperties() {
        return new PromotionMessage.Body.Promotions.Promotion.Properties();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Stickers }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Stickers createPromotionMessageBodyPromotionsPromotionStickers() {
        return new PromotionMessage.Body.Promotions.Promotion.Stickers();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Messages.Message }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Messages.Message createPromotionMessageBodyPromotionsPromotionMessagesMessage() {
        return new PromotionMessage.Body.Promotions.Promotion.Messages.Message();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Media.Item }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Media.Item createPromotionMessageBodyPromotionsPromotionMediaItem() {
        return new PromotionMessage.Body.Promotions.Promotion.Media.Item();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item createPromotionMessageBodyPromotionsPromotionRestrictionsRestrictionItem() {
        return new PromotionMessage.Body.Promotions.Promotion.Restrictions.Restriction.Item();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Buckets.Bucket }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Buckets.Bucket createPromotionMessageBodyPromotionsPromotionBucketsBucket() {
        return new PromotionMessage.Body.Promotions.Promotion.Buckets.Bucket();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket createPromotionMessageBodyPromotionsPromotionRewardsRewardBucketsBucket() {
        return new PromotionMessage.Body.Promotions.Promotion.Rewards.Reward.Buckets.Bucket();
    }

    /**
     * Create an instance of {@link PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket }
     * 
     */
    public PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket createPromotionMessageBodyPromotionsPromotionTriggersTriggerBucketsBucket() {
        return new PromotionMessage.Body.Promotions.Promotion.Triggers.Trigger.Buckets.Bucket();
    }

}
