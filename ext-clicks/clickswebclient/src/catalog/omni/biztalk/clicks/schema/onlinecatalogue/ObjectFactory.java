
package catalog.omni.biztalk.clicks.schema.onlinecatalogue;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the omni.biztalk.clicks.schema.onlinecatalogue package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: omni.biztalk.clicks.schema.onlinecatalogue
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OnlineCatalogue }
     * 
     */
    public OnlineCatalogue createOnlineCatalogue() {
        return new OnlineCatalogue();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body }
     * 
     */
    public OnlineCatalogue.Body createOnlineCatalogueBody() {
        return new OnlineCatalogue.Body();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.GeneralTables }
     * 
     */
    public OnlineCatalogue.Body.GeneralTables createOnlineCatalogueBodyGeneralTables() {
        return new OnlineCatalogue.Body.GeneralTables();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.GeneralTables.OnlineHierarchy }
     * 
     */
    public OnlineCatalogue.Body.GeneralTables.OnlineHierarchy createOnlineCatalogueBodyGeneralTablesOnlineHierarchy() {
        return new OnlineCatalogue.Body.GeneralTables.OnlineHierarchy();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items }
     * 
     */
    public OnlineCatalogue.Body.Items createOnlineCatalogueBodyItems() {
        return new OnlineCatalogue.Body.Items();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item }
     * 
     */
    public OnlineCatalogue.Body.Items.Item createOnlineCatalogueBodyItemsItem() {
        return new OnlineCatalogue.Body.Items.Item();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item.Media }
     * 
     */
    public OnlineCatalogue.Body.Items.Item.Media createOnlineCatalogueBodyItemsItemMedia() {
        return new OnlineCatalogue.Body.Items.Item.Media();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item.ProductImages }
     * 
     */
    public OnlineCatalogue.Body.Items.Item.ProductImages createOnlineCatalogueBodyItemsItemProductImages() {
        return new OnlineCatalogue.Body.Items.Item.ProductImages();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item.OnlineHierarchy }
     * 
     */
    public OnlineCatalogue.Body.Items.Item.OnlineHierarchy createOnlineCatalogueBodyItemsItemOnlineHierarchy() {
        return new OnlineCatalogue.Body.Items.Item.OnlineHierarchy();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Header }
     * 
     */
    public OnlineCatalogue.Header createOnlineCatalogueHeader() {
        return new OnlineCatalogue.Header();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy }
     * 
     */
    public OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy createOnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy() {
        return new OnlineCatalogue.Body.GeneralTables.OnlineHierarchy.ItemHierarchy();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item.Media.MediaItem }
     * 
     */
    public OnlineCatalogue.Body.Items.Item.Media.MediaItem createOnlineCatalogueBodyItemsItemMediaMediaItem() {
        return new OnlineCatalogue.Body.Items.Item.Media.MediaItem();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item.ProductImages.Image }
     * 
     */
    public OnlineCatalogue.Body.Items.Item.ProductImages.Image createOnlineCatalogueBodyItemsItemProductImagesImage() {
        return new OnlineCatalogue.Body.Items.Item.ProductImages.Image();
    }

    /**
     * Create an instance of {@link OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink }
     * 
     */
    public OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink createOnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink() {
        return new OnlineCatalogue.Body.Items.Item.OnlineHierarchy.HierarchyLink();
    }

}
