/**
 * BizTalkServiceInstanceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.clicksgroup.esb.OmniChannel.Catalogue;

public class BizTalkServiceInstanceLocator extends org.apache.axis.client.Service implements za.co.clicksgroup.esb.OmniChannel.Catalogue.BizTalkServiceInstance {

    public BizTalkServiceInstanceLocator() {
    }


    public BizTalkServiceInstanceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BizTalkServiceInstanceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_ITwoWayAsyncVoid
    private java.lang.String BasicHttpBinding_ITwoWayAsyncVoid_address = "https://esbqa.clicksgroup.co.za/OmniChannel/Catalogue/Service.svc";

    public java.lang.String getBasicHttpBinding_ITwoWayAsyncVoidAddress() {
        return BasicHttpBinding_ITwoWayAsyncVoid_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName = "BasicHttpBinding_ITwoWayAsyncVoid";

    public java.lang.String getBasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName() {
        return BasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName;
    }

    public void setBasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName(java.lang.String name) {
        BasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName = name;
    }

    public za.co.clicksgroup.esb.OmniChannel.Catalogue.Service getBasicHttpBinding_ITwoWayAsyncVoid() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_ITwoWayAsyncVoid_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_ITwoWayAsyncVoid(endpoint);
    }

    public za.co.clicksgroup.esb.OmniChannel.Catalogue.Service getBasicHttpBinding_ITwoWayAsyncVoid(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            za.co.clicksgroup.esb.OmniChannel.Catalogue.BasicHttpBinding_ITwoWayAsyncVoidStub _stub = new za.co.clicksgroup.esb.OmniChannel.Catalogue.BasicHttpBinding_ITwoWayAsyncVoidStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_ITwoWayAsyncVoidEndpointAddress(java.lang.String address) {
        BasicHttpBinding_ITwoWayAsyncVoid_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (za.co.clicksgroup.esb.OmniChannel.Catalogue.Service.class.isAssignableFrom(serviceEndpointInterface)) {
                za.co.clicksgroup.esb.OmniChannel.Catalogue.BasicHttpBinding_ITwoWayAsyncVoidStub _stub = new za.co.clicksgroup.esb.OmniChannel.Catalogue.BasicHttpBinding_ITwoWayAsyncVoidStub(new java.net.URL(BasicHttpBinding_ITwoWayAsyncVoid_address), this);
                _stub.setPortName(getBasicHttpBinding_ITwoWayAsyncVoidWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_ITwoWayAsyncVoid".equals(inputPortName)) {
            return getBasicHttpBinding_ITwoWayAsyncVoid();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("https://esb.clicksgroup.co.za/OmniChannel/Catalogue", "BizTalkServiceInstance");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("https://esb.clicksgroup.co.za/OmniChannel/Catalogue", "BasicHttpBinding_ITwoWayAsyncVoid"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_ITwoWayAsyncVoid".equals(portName)) {
            setBasicHttpBinding_ITwoWayAsyncVoidEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
