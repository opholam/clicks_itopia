/**
 * BizTalkServiceInstance.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package za.co.clicksgroup.esb.OmniChannel.Catalogue;

public interface BizTalkServiceInstance extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_ITwoWayAsyncVoidAddress();

    public za.co.clicksgroup.esb.OmniChannel.Catalogue.Service getBasicHttpBinding_ITwoWayAsyncVoid() throws javax.xml.rpc.ServiceException;

    public za.co.clicksgroup.esb.OmniChannel.Catalogue.Service getBasicHttpBinding_ITwoWayAsyncVoid(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
