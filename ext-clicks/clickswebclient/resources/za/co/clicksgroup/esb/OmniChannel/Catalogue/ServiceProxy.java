package za.co.clicksgroup.esb.OmniChannel.Catalogue;

public class ServiceProxy implements za.co.clicksgroup.esb.OmniChannel.Catalogue.Service {
  private String _endpoint = null;
  private za.co.clicksgroup.esb.OmniChannel.Catalogue.Service service = null;
  
  public ServiceProxy() {
    _initServiceProxy();
  }
  
  public ServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initServiceProxy();
  }
  
  private void _initServiceProxy() {
    try {
      service = (new za.co.clicksgroup.esb.OmniChannel.Catalogue.BizTalkServiceInstanceLocator()).getBasicHttpBinding_ITwoWayAsyncVoid();
      if (service != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)service)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)service)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (service != null)
      ((javax.xml.rpc.Stub)service)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public za.co.clicksgroup.esb.OmniChannel.Catalogue.Service getService() {
    if (service == null)
      _initServiceProxy();
    return service;
  }
  
  
}