/**
 * OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.OnlineCatalogue;

public class OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy  implements java.io.Serializable {
    private java.lang.String action;  // attribute

    private java.lang.String categoryId;  // attribute

    private java.lang.String categoryDescription;  // attribute

    private java.math.BigInteger level;  // attribute

    private java.lang.String parentCategoryId;  // attribute

    private java.math.BigInteger parentLevel;  // attribute

    public OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy() {
    }

    public OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy(
           java.lang.String action,
           java.lang.String categoryId,
           java.lang.String categoryDescription,
           java.math.BigInteger level,
           java.lang.String parentCategoryId,
           java.math.BigInteger parentLevel) {
           this.action = action;
           this.categoryId = categoryId;
           this.categoryDescription = categoryDescription;
           this.level = level;
           this.parentCategoryId = parentCategoryId;
           this.parentLevel = parentLevel;
    }


    /**
     * Gets the action value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }


    /**
     * Gets the categoryId value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @return categoryId
     */
    public java.lang.String getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @param categoryId
     */
    public void setCategoryId(java.lang.String categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the categoryDescription value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @return categoryDescription
     */
    public java.lang.String getCategoryDescription() {
        return categoryDescription;
    }


    /**
     * Sets the categoryDescription value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @param categoryDescription
     */
    public void setCategoryDescription(java.lang.String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }


    /**
     * Gets the level value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @return level
     */
    public java.math.BigInteger getLevel() {
        return level;
    }


    /**
     * Sets the level value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @param level
     */
    public void setLevel(java.math.BigInteger level) {
        this.level = level;
    }


    /**
     * Gets the parentCategoryId value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @return parentCategoryId
     */
    public java.lang.String getParentCategoryId() {
        return parentCategoryId;
    }


    /**
     * Sets the parentCategoryId value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @param parentCategoryId
     */
    public void setParentCategoryId(java.lang.String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }


    /**
     * Gets the parentLevel value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @return parentLevel
     */
    public java.math.BigInteger getParentLevel() {
        return parentLevel;
    }


    /**
     * Sets the parentLevel value for this OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.
     * 
     * @param parentLevel
     */
    public void setParentLevel(java.math.BigInteger parentLevel) {
        this.parentLevel = parentLevel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy)) return false;
        OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy other = (OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.categoryId==null && other.getCategoryId()==null) || 
             (this.categoryId!=null &&
              this.categoryId.equals(other.getCategoryId()))) &&
            ((this.categoryDescription==null && other.getCategoryDescription()==null) || 
             (this.categoryDescription!=null &&
              this.categoryDescription.equals(other.getCategoryDescription()))) &&
            ((this.level==null && other.getLevel()==null) || 
             (this.level!=null &&
              this.level.equals(other.getLevel()))) &&
            ((this.parentCategoryId==null && other.getParentCategoryId()==null) || 
             (this.parentCategoryId!=null &&
              this.parentCategoryId.equals(other.getParentCategoryId()))) &&
            ((this.parentLevel==null && other.getParentLevel()==null) || 
             (this.parentLevel!=null &&
              this.parentLevel.equals(other.getParentLevel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getCategoryId() != null) {
            _hashCode += getCategoryId().hashCode();
        }
        if (getCategoryDescription() != null) {
            _hashCode += getCategoryDescription().hashCode();
        }
        if (getLevel() != null) {
            _hashCode += getLevel().hashCode();
        }
        if (getParentCategoryId() != null) {
            _hashCode += getParentCategoryId().hashCode();
        }
        if (getParentLevel() != null) {
            _hashCode += getParentLevel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>>OnlineCatalogue>Body>GeneralTables>OnlineHierarchy>ItemHierarchy"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("action");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Action"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("categoryId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "CategoryId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("categoryDescription");
        attrField.setXmlName(new javax.xml.namespace.QName("", "CategoryDescription"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("level");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Level"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("parentCategoryId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "ParentCategoryId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("parentLevel");
        attrField.setXmlName(new javax.xml.namespace.QName("", "ParentLevel"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
