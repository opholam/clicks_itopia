/**
 * OnlineCatalogueBodyItemsItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.OnlineCatalogue;

public class OnlineCatalogueBodyItemsItem  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink[] onlineHierarchy;

    private Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemProductImagesImage[] productImages;

    private java.lang.String SKU;  // attribute

    private java.lang.String action;  // attribute

    private java.lang.String onlineDescription;  // attribute

    public OnlineCatalogueBodyItemsItem() {
    }

    public OnlineCatalogueBodyItemsItem(
           Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink[] onlineHierarchy,
           Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemProductImagesImage[] productImages,
           java.lang.String SKU,
           java.lang.String action,
           java.lang.String onlineDescription) {
           this.onlineHierarchy = onlineHierarchy;
           this.productImages = productImages;
           this.SKU = SKU;
           this.action = action;
           this.onlineDescription = onlineDescription;
    }


    /**
     * Gets the onlineHierarchy value for this OnlineCatalogueBodyItemsItem.
     * 
     * @return onlineHierarchy
     */
    public Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink[] getOnlineHierarchy() {
        return onlineHierarchy;
    }


    /**
     * Sets the onlineHierarchy value for this OnlineCatalogueBodyItemsItem.
     * 
     * @param onlineHierarchy
     */
    public void setOnlineHierarchy(Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink[] onlineHierarchy) {
        this.onlineHierarchy = onlineHierarchy;
    }


    /**
     * Gets the productImages value for this OnlineCatalogueBodyItemsItem.
     * 
     * @return productImages
     */
    public Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemProductImagesImage[] getProductImages() {
        return productImages;
    }


    /**
     * Sets the productImages value for this OnlineCatalogueBodyItemsItem.
     * 
     * @param productImages
     */
    public void setProductImages(Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItemProductImagesImage[] productImages) {
        this.productImages = productImages;
    }


    /**
     * Gets the SKU value for this OnlineCatalogueBodyItemsItem.
     * 
     * @return SKU
     */
    public java.lang.String getSKU() {
        return SKU;
    }


    /**
     * Sets the SKU value for this OnlineCatalogueBodyItemsItem.
     * 
     * @param SKU
     */
    public void setSKU(java.lang.String SKU) {
        this.SKU = SKU;
    }


    /**
     * Gets the action value for this OnlineCatalogueBodyItemsItem.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this OnlineCatalogueBodyItemsItem.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }


    /**
     * Gets the onlineDescription value for this OnlineCatalogueBodyItemsItem.
     * 
     * @return onlineDescription
     */
    public java.lang.String getOnlineDescription() {
        return onlineDescription;
    }


    /**
     * Sets the onlineDescription value for this OnlineCatalogueBodyItemsItem.
     * 
     * @param onlineDescription
     */
    public void setOnlineDescription(java.lang.String onlineDescription) {
        this.onlineDescription = onlineDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OnlineCatalogueBodyItemsItem)) return false;
        OnlineCatalogueBodyItemsItem other = (OnlineCatalogueBodyItemsItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.onlineHierarchy==null && other.getOnlineHierarchy()==null) || 
             (this.onlineHierarchy!=null &&
              java.util.Arrays.equals(this.onlineHierarchy, other.getOnlineHierarchy()))) &&
            ((this.productImages==null && other.getProductImages()==null) || 
             (this.productImages!=null &&
              java.util.Arrays.equals(this.productImages, other.getProductImages()))) &&
            ((this.SKU==null && other.getSKU()==null) || 
             (this.SKU!=null &&
              this.SKU.equals(other.getSKU()))) &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.onlineDescription==null && other.getOnlineDescription()==null) || 
             (this.onlineDescription!=null &&
              this.onlineDescription.equals(other.getOnlineDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOnlineHierarchy() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOnlineHierarchy());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOnlineHierarchy(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductImages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductImages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductImages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSKU() != null) {
            _hashCode += getSKU().hashCode();
        }
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getOnlineDescription() != null) {
            _hashCode += getOnlineDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OnlineCatalogueBodyItemsItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>OnlineCatalogue>Body>Items>Item"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("SKU");
        attrField.setXmlName(new javax.xml.namespace.QName("", "SKU"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>>OnlineCatalogue>Body>Items>Item>SKU"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("action");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Action"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("onlineDescription");
        attrField.setXmlName(new javax.xml.namespace.QName("", "OnlineDescription"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onlineHierarchy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "OnlineHierarchy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>>>OnlineCatalogue>Body>Items>Item>OnlineHierarchy>HierarchyLink"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "HierarchyLink"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productImages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "ProductImages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>>>OnlineCatalogue>Body>Items>Item>ProductImages>Image"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "Image"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
