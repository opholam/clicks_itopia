/**
 * OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.OnlineCatalogue;

public class OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink  implements java.io.Serializable {
    private java.math.BigInteger level;  // attribute

    private java.lang.String categoryId;  // attribute

    private java.lang.String action;  // attribute

    public OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink() {
    }

    public OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink(
           java.math.BigInteger level,
           java.lang.String categoryId,
           java.lang.String action) {
           this.level = level;
           this.categoryId = categoryId;
           this.action = action;
    }


    /**
     * Gets the level value for this OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.
     * 
     * @return level
     */
    public java.math.BigInteger getLevel() {
        return level;
    }


    /**
     * Sets the level value for this OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.
     * 
     * @param level
     */
    public void setLevel(java.math.BigInteger level) {
        this.level = level;
    }


    /**
     * Gets the categoryId value for this OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.
     * 
     * @return categoryId
     */
    public java.lang.String getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.
     * 
     * @param categoryId
     */
    public void setCategoryId(java.lang.String categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the action value for this OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink)) return false;
        OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink other = (OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.level==null && other.getLevel()==null) || 
             (this.level!=null &&
              this.level.equals(other.getLevel()))) &&
            ((this.categoryId==null && other.getCategoryId()==null) || 
             (this.categoryId!=null &&
              this.categoryId.equals(other.getCategoryId()))) &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLevel() != null) {
            _hashCode += getLevel().hashCode();
        }
        if (getCategoryId() != null) {
            _hashCode += getCategoryId().hashCode();
        }
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OnlineCatalogueBodyItemsItemOnlineHierarchyHierarchyLink.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>>>OnlineCatalogue>Body>Items>Item>OnlineHierarchy>HierarchyLink"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("level");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Level"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("categoryId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "CategoryId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("action");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Action"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
