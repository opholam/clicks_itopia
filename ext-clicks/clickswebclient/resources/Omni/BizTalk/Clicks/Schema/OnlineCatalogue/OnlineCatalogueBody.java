/**
 * OnlineCatalogueBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Omni.BizTalk.Clicks.Schema.OnlineCatalogue;

public class OnlineCatalogueBody  implements java.io.Serializable {
    private Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItem[] items;

    private Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy[][] generalTables;

    private java.lang.String retailerId;  // attribute

    private java.util.Date creationDate;  // attribute

    public OnlineCatalogueBody() {
    }

    public OnlineCatalogueBody(
           Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItem[] items,
           Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy[][] generalTables,
           java.lang.String retailerId,
           java.util.Date creationDate) {
           this.items = items;
           this.generalTables = generalTables;
           this.retailerId = retailerId;
           this.creationDate = creationDate;
    }


    /**
     * Gets the items value for this OnlineCatalogueBody.
     * 
     * @return items
     */
    public Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItem[] getItems() {
        return items;
    }


    /**
     * Sets the items value for this OnlineCatalogueBody.
     * 
     * @param items
     */
    public void setItems(Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyItemsItem[] items) {
        this.items = items;
    }


    /**
     * Gets the generalTables value for this OnlineCatalogueBody.
     * 
     * @return generalTables
     */
    public Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy[][] getGeneralTables() {
        return generalTables;
    }


    /**
     * Sets the generalTables value for this OnlineCatalogueBody.
     * 
     * @param generalTables
     */
    public void setGeneralTables(Omni.BizTalk.Clicks.Schema.OnlineCatalogue.OnlineCatalogueBodyGeneralTablesOnlineHierarchyItemHierarchy[][] generalTables) {
        this.generalTables = generalTables;
    }


    /**
     * Gets the retailerId value for this OnlineCatalogueBody.
     * 
     * @return retailerId
     */
    public java.lang.String getRetailerId() {
        return retailerId;
    }


    /**
     * Sets the retailerId value for this OnlineCatalogueBody.
     * 
     * @param retailerId
     */
    public void setRetailerId(java.lang.String retailerId) {
        this.retailerId = retailerId;
    }


    /**
     * Gets the creationDate value for this OnlineCatalogueBody.
     * 
     * @return creationDate
     */
    public java.util.Date getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this OnlineCatalogueBody.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.util.Date creationDate) {
        this.creationDate = creationDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OnlineCatalogueBody)) return false;
        OnlineCatalogueBody other = (OnlineCatalogueBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.items==null && other.getItems()==null) || 
             (this.items!=null &&
              java.util.Arrays.equals(this.items, other.getItems()))) &&
            ((this.generalTables==null && other.getGeneralTables()==null) || 
             (this.generalTables!=null &&
              java.util.Arrays.equals(this.generalTables, other.getGeneralTables()))) &&
            ((this.retailerId==null && other.getRetailerId()==null) || 
             (this.retailerId!=null &&
              this.retailerId.equals(other.getRetailerId()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGeneralTables() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGeneralTables());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGeneralTables(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRetailerId() != null) {
            _hashCode += getRetailerId().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OnlineCatalogueBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>OnlineCatalogue>Body"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("retailerId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "RetailerId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("creationDate");
        attrField.setXmlName(new javax.xml.namespace.QName("", "CreationDate"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("items");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "Items"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>OnlineCatalogue>Body>Items>Item"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "Item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("generalTables");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "GeneralTables"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", ">>>>OnlineCatalogue>Body>GeneralTables>OnlineHierarchy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Clicks.BizTalk.Omni/Schema/OnlineCatalogue", "OnlineHierarchy"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
